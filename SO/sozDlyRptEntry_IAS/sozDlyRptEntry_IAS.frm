VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#157.0#0"; "EntryLookupControls.ocx"
Object = "{9504980C-B928-4BF5-A5D0-13E1F649AECB}#35.0#0"; "SOTAVM.ocx"
Object = "{BC90D6A3-491E-451B-ADED-8FABA0B8EE36}#47.0#0"; "SOTADropDown.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#84.0#0"; "LookupView.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Begin VB.Form frmDailyReportEntry 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Daily Report Entry"
   ClientHeight    =   7530
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11280
   Icon            =   "sozDlyRptEntry_IAS.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7530
   ScaleWidth      =   11280
   Begin VB.CommandButton cmdNext 
      Caption         =   ">>"
      Height          =   255
      Left            =   1800
      TabIndex        =   81
      TabStop         =   0   'False
      ToolTipText     =   "Next Date"
      Top             =   960
      Width           =   855
   End
   Begin VB.CommandButton cmdPrevious 
      Caption         =   "<<"
      Height          =   255
      Left            =   840
      TabIndex        =   80
      TabStop         =   0   'False
      ToolTipText     =   "Previous Date"
      Top             =   960
      Width           =   855
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   300
      Left            =   6240
      Top             =   360
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save"
      Height          =   255
      Left            =   10200
      TabIndex        =   79
      TabStop         =   0   'False
      ToolTipText     =   "Save the default plant."
      Top             =   600
      Width           =   735
   End
   Begin LookupViewControl.LookupView navMain 
      Height          =   285
      Left            =   -3120
      TabIndex        =   67
      TabStop         =   0   'False
      Top             =   480
      Width           =   285
      _ExtentX        =   503
      _ExtentY        =   503
      LookupID        =   "soDlyRprtEntry_SGS"
   End
   Begin SOTACalendarControl.SOTACalendar tdDate 
      Height          =   300
      Left            =   840
      TabIndex        =   1
      Top             =   540
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   529
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Text            =   "  /  /    "
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5535
      Left            =   120
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   1440
      Width           =   11055
      _ExtentX        =   19500
      _ExtentY        =   9763
      _Version        =   393216
      Tabs            =   5
      TabsPerRow      =   4
      TabHeight       =   520
      TabCaption(0)   =   "Asphalt Receipts"
      TabPicture(0)   =   "sozDlyRptEntry_IAS.frx":23D2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblTruckTons"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblTrucks"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblRailTons"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblRailCars"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "lblNonIdealLoads"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "lblJBTAssignedTrucks"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "numJBTAssignedTrucks"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "numNonIdealLoads"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "numRailCars"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "numTrucks"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "numRailTons"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "numTruckTons"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).ControlCount=   12
      TabCaption(1)   =   "Daily Labor && Therms Metrics"
      TabPicture(1)   =   "sozDlyRptEntry_IAS.frx":23EE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "lblTherms"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "lblPaidTimeOff"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "lblOverTimeHours"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "lblOneXHours"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "lblSalaryEmployees"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "numSalaryEmployees"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "numTherms"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).Control(7)=   "numPaidTimeOff"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).Control(8)=   "numOverTimeHours"
      Tab(1).Control(8).Enabled=   0   'False
      Tab(1).Control(9)=   "numOneXHours"
      Tab(1).Control(9).Enabled=   0   'False
      Tab(1).ControlCount=   10
      TabCaption(2)   =   "Daily Comments"
      TabPicture(2)   =   "sozDlyRptEntry_IAS.frx":240A
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "lblTruckingLogisticsCmnt"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "lblOperationsCmnt"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "lblQualityCmnt"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).Control(3)=   "txtQualityCmnt"
      Tab(2).Control(3).Enabled=   0   'False
      Tab(2).Control(4)=   "txtOperationsCmnt"
      Tab(2).Control(4).Enabled=   0   'False
      Tab(2).Control(5)=   "txtTruckingLogisticsCmnt"
      Tab(2).Control(5).Enabled=   0   'False
      Tab(2).ControlCount=   6
      TabCaption(3)   =   "Winter Fill"
      TabPicture(3)   =   "sozDlyRptEntry_IAS.frx":2426
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "lblProducts3"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).Control(1)=   "lblRefinery3"
      Tab(3).Control(1).Enabled=   0   'False
      Tab(3).Control(2)=   "lblProducts2"
      Tab(3).Control(2).Enabled=   0   'False
      Tab(3).Control(3)=   "lblRefinery2"
      Tab(3).Control(3).Enabled=   0   'False
      Tab(3).Control(4)=   "lblProducts1"
      Tab(3).Control(4).Enabled=   0   'False
      Tab(3).Control(5)=   "lblRefinery1"
      Tab(3).Control(5).Enabled=   0   'False
      Tab(3).Control(6)=   "lblTargetLoads3"
      Tab(3).Control(6).Enabled=   0   'False
      Tab(3).Control(7)=   "lblTargetLoads2"
      Tab(3).Control(7).Enabled=   0   'False
      Tab(3).Control(8)=   "lblTargetLoads1"
      Tab(3).Control(8).Enabled=   0   'False
      Tab(3).Control(9)=   "Label1"
      Tab(3).Control(9).Enabled=   0   'False
      Tab(3).Control(10)=   "numTargetLoads3"
      Tab(3).Control(10).Enabled=   0   'False
      Tab(3).Control(11)=   "numTargetLoads2"
      Tab(3).Control(11).Enabled=   0   'False
      Tab(3).Control(12)=   "ddroducts3"
      Tab(3).Control(12).Enabled=   0   'False
      Tab(3).Control(13)=   "ddroducts2"
      Tab(3).Control(13).Enabled=   0   'False
      Tab(3).Control(14)=   "ddroducts1"
      Tab(3).Control(14).Enabled=   0   'False
      Tab(3).Control(15)=   "ddRefinery3"
      Tab(3).Control(15).Enabled=   0   'False
      Tab(3).Control(16)=   "ddRefinery2"
      Tab(3).Control(16).Enabled=   0   'False
      Tab(3).Control(17)=   "numTargetLoads1"
      Tab(3).Control(17).Enabled=   0   'False
      Tab(3).Control(18)=   "txtWinterFillCmnt"
      Tab(3).Control(18).Enabled=   0   'False
      Tab(3).Control(19)=   "ddRefinery1"
      Tab(3).Control(19).Enabled=   0   'False
      Tab(3).ControlCount=   20
      TabCaption(4)   =   "Summary"
      TabPicture(4)   =   "sozDlyRptEntry_IAS.frx":2442
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Label3"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).Control(1)=   "Label4"
      Tab(4).Control(1).Enabled=   0   'False
      Tab(4).Control(2)=   "Label5"
      Tab(4).Control(2).Enabled=   0   'False
      Tab(4).Control(3)=   "Label6"
      Tab(4).Control(3).Enabled=   0   'False
      Tab(4).Control(4)=   "lkuFilterPlant"
      Tab(4).Control(4).Enabled=   0   'False
      Tab(4).Control(5)=   "dtEndDate"
      Tab(4).Control(5).Enabled=   0   'False
      Tab(4).Control(6)=   "dtStartDate"
      Tab(4).Control(6).Enabled=   0   'False
      Tab(4).Control(7)=   "grdMain"
      Tab(4).Control(7).Enabled=   0   'False
      Tab(4).Control(8)=   "cmdRefresh"
      Tab(4).Control(8).Enabled=   0   'False
      Tab(4).ControlCount=   9
      Begin VB.CommandButton cmdRefresh 
         Caption         =   "Refresh"
         Height          =   255
         Left            =   -66480
         TabIndex        =   78
         TabStop         =   0   'False
         Top             =   960
         Width           =   1335
      End
      Begin FPSpreadADO.fpSpread grdMain 
         Height          =   4095
         Left            =   -74880
         TabIndex        =   70
         TabStop         =   0   'False
         Top             =   1320
         Width           =   10695
         _Version        =   524288
         _ExtentX        =   18865
         _ExtentY        =   7223
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         SpreadDesigner  =   "sozDlyRptEntry_IAS.frx":245E
      End
      Begin SOTADropDownControl.SOTADropDown ddRefinery1 
         Height          =   315
         Left            =   -73920
         TabIndex        =   34
         TabStop         =   0   'False
         Top             =   720
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Style           =   2
         Text            =   "ddRefinery1"
         SQLStatement    =   "select Refinery,  RefineryKey, OrderNo from tsoRefineries_SGS Order by OrderNo, Refinery"
         Sorted          =   0   'False
      End
      Begin VB.TextBox txtWinterFillCmnt 
         Height          =   1695
         Left            =   -73920
         MaxLength       =   1500
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   52
         TabStop         =   0   'False
         Top             =   2400
         Width           =   9855
      End
      Begin VB.TextBox txtTruckingLogisticsCmnt 
         Height          =   1215
         Left            =   -73200
         MaxLength       =   1500
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   32
         Top             =   3960
         Width           =   9015
      End
      Begin VB.TextBox txtOperationsCmnt 
         Height          =   1215
         Left            =   -73200
         MaxLength       =   1500
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   30
         Top             =   2640
         Width           =   9015
      End
      Begin VB.TextBox txtQualityCmnt 
         Height          =   1215
         Left            =   -73200
         MaxLength       =   1500
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   28
         Top             =   1290
         Width           =   9015
      End
      Begin NEWSOTALib.SOTANumber numTruckTons 
         Height          =   315
         Left            =   5220
         TabIndex        =   8
         Top             =   780
         Width           =   1500
         _Version        =   65536
         _ExtentX        =   2646
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "           0.00"
         sDecimalPlaces  =   2
      End
      Begin NEWSOTALib.SOTANumber numRailTons 
         Height          =   315
         Left            =   5220
         TabIndex        =   12
         Top             =   1260
         Width           =   1500
         _Version        =   65536
         _ExtentX        =   2646
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "           0.00"
         sDecimalPlaces  =   2
      End
      Begin NEWSOTALib.SOTANumber numOneXHours 
         Height          =   315
         Left            =   -70695
         TabIndex        =   20
         Top             =   720
         Width           =   1500
         _Version        =   65536
         _ExtentX        =   2646
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "           0.00"
         sDecimalPlaces  =   2
      End
      Begin NEWSOTALib.SOTANumber numOverTimeHours 
         Height          =   315
         Left            =   -67455
         TabIndex        =   22
         Top             =   750
         Width           =   1500
         _Version        =   65536
         _ExtentX        =   2646
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "           0.00"
         sDecimalPlaces  =   2
      End
      Begin NEWSOTALib.SOTANumber numPaidTimeOff 
         Height          =   315
         Left            =   -73215
         TabIndex        =   24
         Top             =   1185
         Width           =   1500
         _Version        =   65536
         _ExtentX        =   2646
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "           0.00"
         sDecimalPlaces  =   2
      End
      Begin NEWSOTALib.SOTANumber numTherms 
         Height          =   315
         Left            =   -73215
         TabIndex        =   26
         Top             =   1605
         Width           =   1500
         _Version        =   65536
         _ExtentX        =   2646
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "           0.00"
         sDecimalPlaces  =   2
      End
      Begin NEWSOTALib.SOTANumber numTargetLoads1 
         Height          =   315
         Left            =   -69360
         TabIndex        =   36
         TabStop         =   0   'False
         Top             =   720
         Width           =   1020
         _Version        =   65536
         _ExtentX        =   1799
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
         text            =   "           0"
         sDecimalPlaces  =   0
      End
      Begin SOTADropDownControl.SOTADropDown ddRefinery2 
         Height          =   315
         Left            =   -73920
         TabIndex        =   40
         TabStop         =   0   'False
         Top             =   1200
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Style           =   2
         Text            =   "ddRefinery2"
         SQLStatement    =   "select Refinery 'RefineryDisplay',  Refinery 'RefineryVlu', OrderNo from tsoRefineries_SGS Order by OrderNo, RefineryVlu"
         Sorted          =   0   'False
      End
      Begin SOTADropDownControl.SOTADropDown ddRefinery3 
         Height          =   315
         Left            =   -73920
         TabIndex        =   46
         TabStop         =   0   'False
         Top             =   1680
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Style           =   2
         Text            =   "ddRefinery3"
         SQLStatement    =   "select Refinery 'RefineryDisplay',  Refinery 'RefineryVlu', OrderNo from tsoRefineries_SGS Order by OrderNo, RefineryVlu"
         Sorted          =   0   'False
      End
      Begin SOTADropDownControl.SOTADropDown ddroducts1 
         Height          =   315
         Left            =   -67440
         TabIndex        =   38
         TabStop         =   0   'False
         Top             =   720
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Style           =   2
         Text            =   "ddroducts1"
         SQLStatement    =   "select Product, ProductKey from tsoProducts_SGS Order by Product"
         Sorted          =   0   'False
      End
      Begin SOTADropDownControl.SOTADropDown ddroducts2 
         Height          =   315
         Left            =   -67440
         TabIndex        =   44
         TabStop         =   0   'False
         Top             =   1200
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Style           =   2
         Text            =   "ddroducts2"
         SQLStatement    =   "select Product, ProductKey from tsoProducts_SGS Order by Product"
         Sorted          =   0   'False
      End
      Begin SOTADropDownControl.SOTADropDown ddroducts3 
         Height          =   315
         Left            =   -67440
         TabIndex        =   50
         TabStop         =   0   'False
         Top             =   1680
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Style           =   2
         Text            =   "ddroducts3"
         SQLStatement    =   "select Product, ProductKey from tsoProducts_SGS Order by Product"
         Sorted          =   0   'False
      End
      Begin NEWSOTALib.SOTANumber numTargetLoads2 
         Height          =   315
         Left            =   -69360
         TabIndex        =   42
         TabStop         =   0   'False
         Top             =   1200
         Width           =   1020
         _Version        =   65536
         _ExtentX        =   1799
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
         text            =   "           0"
         sDecimalPlaces  =   0
      End
      Begin NEWSOTALib.SOTANumber numTargetLoads3 
         Height          =   315
         Left            =   -69360
         TabIndex        =   48
         TabStop         =   0   'False
         Top             =   1680
         Width           =   1020
         _Version        =   65536
         _ExtentX        =   1799
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
         text            =   "           0"
         sDecimalPlaces  =   0
      End
      Begin NEWSOTALib.SOTANumber numSalaryEmployees 
         Height          =   315
         Left            =   -73200
         TabIndex        =   18
         Top             =   750
         Width           =   1500
         _Version        =   65536
         _ExtentX        =   2646
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
         text            =   "           0"
         sDecimalPlaces  =   0
      End
      Begin NEWSOTALib.SOTANumber numTrucks 
         Height          =   315
         Left            =   1680
         TabIndex        =   6
         Top             =   780
         Width           =   1500
         _Version        =   65536
         _ExtentX        =   2646
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
         text            =   "           0"
         sDecimalPlaces  =   0
      End
      Begin NEWSOTALib.SOTANumber numRailCars 
         Height          =   315
         Left            =   1680
         TabIndex        =   10
         Top             =   1260
         Width           =   1500
         _Version        =   65536
         _ExtentX        =   2646
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
         text            =   "           0"
         sDecimalPlaces  =   0
      End
      Begin SOTACalendarControl.SOTACalendar dtStartDate 
         Height          =   300
         Left            =   -72600
         TabIndex        =   71
         TabStop         =   0   'False
         Top             =   960
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaskedText      =   "  /  /    "
         Text            =   "  /  /    "
      End
      Begin SOTACalendarControl.SOTACalendar dtEndDate 
         Height          =   300
         Left            =   -70680
         TabIndex        =   74
         TabStop         =   0   'False
         Top             =   960
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaskedText      =   "  /  /    "
         Text            =   "  /  /    "
      End
      Begin EntryLookupControls.TextLookup lkuFilterPlant 
         Height          =   285
         Left            =   -68640
         TabIndex        =   76
         TabStop         =   0   'False
         Top             =   960
         WhatsThisHelpID =   60195
         Width           =   1995
         _ExtentX        =   3519
         _ExtentY        =   503
         ForeColor       =   -2147483640
         LookupID        =   "Warehouse_SGS"
         ParentIDColumn  =   "WhseID"
         ParentKeyColumn =   "Description"
         ParentTable     =   "vluWarehouseDesc_SGS"
         BoundColumn     =   "WhseID"
         BoundTable      =   "tsoDailyReportEntry_SGS"
         sSQLReturnCols  =   "WhseID,,;Description,,;"
      End
      Begin NEWSOTALib.SOTANumber numNonIdealLoads 
         Height          =   315
         Left            =   1680
         TabIndex        =   14
         Top             =   1695
         Width           =   1500
         _Version        =   65536
         _ExtentX        =   2646
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
         text            =   "           0"
         sDecimalPlaces  =   0
      End
      Begin NEWSOTALib.SOTANumber numJBTAssignedTrucks 
         Height          =   315
         Left            =   1680
         TabIndex        =   16
         Top             =   2160
         Width           =   1500
         _Version        =   65536
         _ExtentX        =   2646
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
         text            =   "           0"
         sDecimalPlaces  =   0
      End
      Begin VB.Label lblJBTAssignedTrucks 
         Caption         =   "JBT Assigned Trucks"
         Height          =   315
         Left            =   120
         TabIndex        =   15
         Top             =   2160
         Width           =   1620
      End
      Begin VB.Label lblNonIdealLoads 
         Caption         =   "Non Ideal Loads"
         Height          =   315
         Left            =   240
         TabIndex        =   13
         Top             =   1680
         Width           =   1500
      End
      Begin VB.Label Label6 
         Caption         =   "Filter By This Plant:"
         Height          =   300
         Left            =   -68640
         TabIndex        =   77
         Top             =   720
         Width           =   1500
      End
      Begin VB.Label Label5 
         Caption         =   "End Date"
         Height          =   255
         Left            =   -70680
         TabIndex        =   75
         Top             =   720
         Width           =   1815
      End
      Begin VB.Label Label4 
         Caption         =   "Start Date"
         Height          =   255
         Left            =   -72600
         TabIndex        =   73
         Top             =   720
         Width           =   1815
      End
      Begin VB.Label Label3 
         Caption         =   "Filter Between These Dates:"
         Height          =   300
         Left            =   -74760
         TabIndex        =   72
         Top             =   960
         Width           =   2100
      End
      Begin VB.Label Label1 
         Caption         =   "Comments"
         Height          =   195
         Left            =   -74760
         TabIndex        =   51
         Top             =   2400
         Width           =   1500
      End
      Begin VB.Label lblTargetLoads1 
         Caption         =   "Target Loads "
         Height          =   315
         Left            =   -70440
         TabIndex        =   35
         Top             =   720
         Width           =   1500
      End
      Begin VB.Label lblTargetLoads2 
         Caption         =   "Target Loads "
         Height          =   315
         Left            =   -70440
         TabIndex        =   41
         Top             =   1200
         Width           =   1500
      End
      Begin VB.Label lblTargetLoads3 
         Caption         =   "Target Loads "
         Height          =   315
         Left            =   -70440
         TabIndex        =   47
         Top             =   1680
         Width           =   1500
      End
      Begin VB.Label lblRefinery1 
         Caption         =   "Refinery 1"
         Height          =   315
         Left            =   -74760
         TabIndex        =   33
         Top             =   720
         Width           =   1500
      End
      Begin VB.Label lblProducts1 
         Caption         =   "Products"
         Height          =   315
         Left            =   -68160
         TabIndex        =   37
         Top             =   720
         Width           =   1500
      End
      Begin VB.Label lblRefinery2 
         Caption         =   "Refinery 2"
         Height          =   315
         Left            =   -74760
         TabIndex        =   39
         Top             =   1200
         Width           =   1500
      End
      Begin VB.Label lblProducts2 
         Caption         =   "Products"
         Height          =   315
         Left            =   -68160
         TabIndex        =   43
         Top             =   1200
         Width           =   1500
      End
      Begin VB.Label lblRefinery3 
         Caption         =   "Refinery 3"
         Height          =   315
         Left            =   -74760
         TabIndex        =   45
         Top             =   1680
         Width           =   1500
      End
      Begin VB.Label lblProducts3 
         Caption         =   "Products"
         Height          =   315
         Left            =   -68160
         TabIndex        =   49
         Top             =   1680
         Width           =   1500
      End
      Begin VB.Label lblQualityCmnt 
         Caption         =   "Quality "
         Height          =   315
         Left            =   -74760
         TabIndex        =   27
         Top             =   1290
         Width           =   1500
      End
      Begin VB.Label lblOperationsCmnt 
         Caption         =   "Operations "
         Height          =   195
         Left            =   -74760
         TabIndex        =   29
         Top             =   2640
         Width           =   1500
      End
      Begin VB.Label lblTruckingLogisticsCmnt 
         Caption         =   "Trucking Logistics"
         Height          =   315
         Left            =   -74760
         TabIndex        =   31
         Top             =   3960
         Width           =   1500
      End
      Begin VB.Label lblSalaryEmployees 
         Caption         =   "Salary Employees"
         Height          =   315
         Left            =   -74760
         TabIndex        =   17
         Top             =   750
         Width           =   1500
      End
      Begin VB.Label lblOneXHours 
         Caption         =   "1X Hours"
         Height          =   315
         Left            =   -71475
         TabIndex        =   19
         Top             =   750
         Width           =   780
      End
      Begin VB.Label lblOverTimeHours 
         Caption         =   "Over Time Hours"
         Height          =   315
         Left            =   -68955
         TabIndex        =   21
         Top             =   750
         Width           =   1500
      End
      Begin VB.Label lblPaidTimeOff 
         Caption         =   "Paid Time Off"
         Height          =   315
         Left            =   -74715
         TabIndex        =   23
         Top             =   1185
         Width           =   1500
      End
      Begin VB.Label lblTherms 
         Caption         =   "Therms"
         Height          =   315
         Left            =   -74715
         TabIndex        =   25
         Top             =   1605
         Width           =   1500
      End
      Begin VB.Label lblRailCars 
         Caption         =   "Rail Cars"
         Height          =   315
         Left            =   240
         TabIndex        =   9
         Top             =   1200
         Width           =   1500
      End
      Begin VB.Label lblRailTons 
         Caption         =   "Rail Tons"
         Height          =   315
         Left            =   3720
         TabIndex        =   11
         Top             =   1260
         Width           =   1500
      End
      Begin VB.Label lblTrucks 
         Caption         =   "Trucks"
         Height          =   315
         Left            =   240
         TabIndex        =   5
         Top             =   720
         Width           =   1500
      End
      Begin VB.Label lblTruckTons 
         Caption         =   "Truck Tons"
         Height          =   315
         Left            =   3720
         TabIndex        =   7
         Top             =   780
         Width           =   1500
      End
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   57
      Top             =   4320
      Visible         =   0   'False
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -10000
      TabIndex        =   56
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -10000
      TabIndex        =   55
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   63
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.ComboBox CustomCombo 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      ForeColor       =   &H80000012&
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   62
      TabStop         =   0   'False
      Text            =   "Combo1"
      Top             =   1530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   61
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin SOTAVM.SOTAValidationMgr valMgr 
      Left            =   -3000
      Top             =   180
      _ExtentX        =   741
      _ExtentY        =   661
      CtlsCount       =   0
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   7140
      Width           =   11280
      _ExtentX        =   19897
      _ExtentY        =   688
      BrowseVisible   =   0   'False
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   53
      TabStop         =   0   'False
      Top             =   0
      Width           =   11280
      _ExtentX        =   19897
      _ExtentY        =   741
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   58
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   59
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   60
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   64
      TabStop         =   0   'False
      Top             =   2880
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   615
      Index           =   0
      Left            =   -10000
      TabIndex        =   65
      TabStop         =   0   'False
      Top             =   720
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1085
      _StockProps     =   0
   End
   Begin EntryLookupControls.TextLookup lkuWhse 
      Height          =   285
      Left            =   3840
      TabIndex        =   3
      Top             =   540
      WhatsThisHelpID =   60195
      Width           =   1995
      _ExtentX        =   3519
      _ExtentY        =   503
      ForeColor       =   -2147483640
      LookupID        =   "Warehouse_SGS"
      ParentIDColumn  =   "WhseID"
      ParentKeyColumn =   "Description"
      ParentTable     =   "vluWarehouseDesc_SGS"
      BoundColumn     =   "WhseID"
      BoundTable      =   "tsoDailyReportEntry_SGS"
      sSQLReturnCols  =   "WhseID,lkuWhse,;Description,lblWhseDesc,;"
   End
   Begin EntryLookupControls.TextLookup lkuDefaultPlant 
      Height          =   285
      Left            =   8040
      TabIndex        =   68
      TabStop         =   0   'False
      Top             =   540
      WhatsThisHelpID =   60195
      Width           =   1995
      _ExtentX        =   3519
      _ExtentY        =   503
      ForeColor       =   -2147483640
      LookupID        =   "Warehouse_SGS"
      ParentIDColumn  =   "WhseID"
      ParentKeyColumn =   "Description"
      ParentTable     =   "vluWarehouseDesc_SGS"
      BoundColumn     =   "WhseID"
      BoundTable      =   "tsoDailyReportEntry_SGS"
      sSQLReturnCols  =   "WhseID,,;Description,,;"
   End
   Begin VB.Label Label2 
      Caption         =   "Default Plant:"
      Height          =   300
      Left            =   6960
      TabIndex        =   69
      Top             =   540
      Width           =   1500
   End
   Begin VB.Label lblWhseDesc 
      Height          =   255
      Left            =   3840
      TabIndex        =   66
      Top             =   900
      UseMnemonic     =   0   'False
      Width           =   6345
   End
   Begin VB.Label lblWhseKey 
      Caption         =   "Plant"
      Height          =   300
      Left            =   3000
      TabIndex        =   2
      Top             =   540
      Width           =   1500
   End
   Begin VB.Label lblEntryDate 
      Caption         =   "Date"
      Height          =   300
      Left            =   105
      TabIndex        =   0
      Top             =   540
      Width           =   1500
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10005
      TabIndex        =   54
      Top             =   -180
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmDailyReportEntry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Public WithEvents moDmGrid             As clsDmGrid
Attribute moDmGrid.VB_VarHelpID = -1

Private WithEvents moGM                As clsGridMgr
Attribute moGM.VB_VarHelpID = -1
'Main Table Constants
    Private Const kColEntryDate = 1

    Private Const kColWhseID = 2

    Private Const kColCompanyID = 3

    Private Const kColTrucks = 4

    Private Const kColTruckTons = 5

    Private Const kColRailCars = 6

    Private Const kColRailTons = 7

    Private Const kColSalaryEmployees = 8

    Private Const kColOneXHours = 9

    Private Const kColOverTimeHours = 10

    Private Const kColPaidTimeOff = 11

    Private Const kColTherms = 12

    Private Const kColNonIdealLoads = 13

    Private Const kColJBTAssignedTrucks = 14

    Private Const kColQualityCmnt = 15

    Private Const kColOperationsCmnt = 16

    Private Const kColTruckingLogisticsCmnt = 17

    Private Const kColRefinery1 = 18

    Private Const kColTargetLoads1 = 19

    Private Const kColProducts1 = 20

    Private Const kColRefinery2 = 21

    Private Const kColTargetLoads2 = 22

    Private Const kColProducts2 = 23

    Private Const kColRefinery3 = 24

    Private Const kColTargetLoads3 = 25

    Private Const kColProducts3 = 26

    Private Const kColWinterFillCmnts = 27




Private moClass             As Object

Private mlRunMode           As Long

Private mbSaved             As Boolean

Private mbCancelShutDown    As Boolean

Public moSotaObjects        As New Collection

Private miFilter            As Integer

Private moContextMenu        As clsContextMenu

Private mbEnterAsTab        As Boolean

Private msCompanyID         As String

Private msUserID            As String

Private mlLanguage          As Long

Private msLookupRestrict       As String

Private miSecurityLevel     As Integer

Private miOldFormHeight As Long

Private miMinFormHeight As Long

Private miOldFormWidth As Long

Private miMinFormWidth As Long

Private WithEvents moDmForm            As clsDmForm
Attribute moDmForm.VB_VarHelpID = -1

Const VBRIG_MODULE_ID_STRING = "frmDailyReportEntry.FRM"

Private mbLoadSuccess As Boolean

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function

Private Sub BindContextMenu()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig

    Set moContextMenu = New clsContextMenu
    With moContextMenu
        Set .Form = frmDailyReportEntry
        .Init
    End With
    
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) BindContextMenu - " & Err.Description    'Generic Error Rig
End Sub

Private Function bSetupGrid() As Boolean
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
Dim iRet As Integer
    
    '   Initialize Grid
' Add code here to format column headings, widths etc...

    bSetupGrid = False

    gGridSetProperties grdMain, 27, kGridDataSheet
    gGridSetColors grdMain
    gGridSetMaxRows grdMain, 10
    gGridSetMaxCols grdMain, 27
    
    grdMain.ScrollBars = ScrollBarsBoth
    
    gGridSetColumnType grdMain, kColEntryDate, SS_CELL_TYPE_DATE
    gGridSetHeader grdMain, kColEntryDate, "Entry Date"
    gGridSetColumnWidth grdMain, kColEntryDate, "10"
    gGridLockColumn grdMain, kColEntryDate
    
    gGridSetColumnType grdMain, kColWhseID, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColWhseID, "Whse ID"
    gGridSetColumnWidth grdMain, kColWhseID, "10"
    gGridLockColumn grdMain, kColWhseID
    
    gGridSetColumnType grdMain, kColCompanyID, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColCompanyID, "Company ID"
    gGridSetColumnWidth grdMain, kColCompanyID, "10"
    gGridLockColumn grdMain, kColCompanyID
    gGridHideColumn grdMain, kColCompanyID
    
    gGridSetColumnType grdMain, kColTrucks, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColTrucks, "Trucks"
    gGridSetColumnWidth grdMain, kColTrucks, "10"
    gGridLockColumn grdMain, kColTrucks
    
    gGridSetColumnType grdMain, kColTruckTons, SS_CELL_TYPE_FLOAT, 2
    gGridSetHeader grdMain, kColTruckTons, "Truck Tons"
    gGridSetColumnWidth grdMain, kColTruckTons, "10"
    gGridLockColumn grdMain, kColTruckTons
    
    gGridSetColumnType grdMain, kColRailCars, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColRailCars, "Rail Cars"
    gGridSetColumnWidth grdMain, kColRailCars, "10"
    gGridLockColumn grdMain, kColRailCars
    
    gGridSetColumnType grdMain, kColRailTons, SS_CELL_TYPE_FLOAT, 2
    gGridSetHeader grdMain, kColRailTons, "Rail Tons"
    gGridSetColumnWidth grdMain, kColRailTons, "10"
    gGridLockColumn grdMain, kColRailTons
    
    gGridSetColumnType grdMain, kColSalaryEmployees, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColSalaryEmployees, "Salary Employees"
    gGridSetColumnWidth grdMain, kColSalaryEmployees, "10"
    gGridLockColumn grdMain, kColSalaryEmployees
    
    gGridSetColumnType grdMain, kColOneXHours, SS_CELL_TYPE_FLOAT, 2
    gGridSetHeader grdMain, kColOneXHours, "One X Hours"
    gGridSetColumnWidth grdMain, kColOneXHours, "10"
    gGridLockColumn grdMain, kColOneXHours
    
    gGridSetColumnType grdMain, kColOverTimeHours, SS_CELL_TYPE_FLOAT, 2
    gGridSetHeader grdMain, kColOverTimeHours, "Over Time Hours"
    gGridSetColumnWidth grdMain, kColOverTimeHours, "10"
    gGridLockColumn grdMain, kColOverTimeHours
    
    gGridSetColumnType grdMain, kColPaidTimeOff, SS_CELL_TYPE_FLOAT, 2
    gGridSetHeader grdMain, kColPaidTimeOff, "Paid Time Off"
    gGridSetColumnWidth grdMain, kColPaidTimeOff, "10"
    gGridLockColumn grdMain, kColPaidTimeOff
    
    gGridSetColumnType grdMain, kColTherms, SS_CELL_TYPE_FLOAT, 2
    gGridSetHeader grdMain, kColTherms, "Therms"
    gGridSetColumnWidth grdMain, kColTherms, "10"
    gGridLockColumn grdMain, kColTherms
    
    gGridSetColumnType grdMain, kColNonIdealLoads, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColNonIdealLoads, "Non Ideal Loads"
    gGridSetColumnWidth grdMain, kColNonIdealLoads, "10"
    gGridLockColumn grdMain, kColNonIdealLoads
    
    gGridSetColumnType grdMain, kColJBTAssignedTrucks, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColJBTAssignedTrucks, "JBT Assigned Trucks"
    gGridSetColumnWidth grdMain, kColJBTAssignedTrucks, "10"
    gGridLockColumn grdMain, kColJBTAssignedTrucks
    
    gGridSetColumnType grdMain, kColQualityCmnt, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColQualityCmnt, "Quality Comment"
    gGridSetColumnWidth grdMain, kColQualityCmnt, "10"
    gGridLockColumn grdMain, kColQualityCmnt
    
    gGridSetColumnType grdMain, kColOperationsCmnt, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColOperationsCmnt, "Operations Comment"
    gGridSetColumnWidth grdMain, kColOperationsCmnt, "10"
    gGridLockColumn grdMain, kColOperationsCmnt
    
    gGridSetColumnType grdMain, kColTruckingLogisticsCmnt, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColTruckingLogisticsCmnt, "Trucking Logistics Comment"
    gGridSetColumnWidth grdMain, kColTruckingLogisticsCmnt, "10"
    gGridLockColumn grdMain, kColTruckingLogisticsCmnt
    
    gGridSetColumnType grdMain, kColRefinery1, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColRefinery1, "Refinery 1"
    gGridSetColumnWidth grdMain, kColRefinery1, "10"
    gGridLockColumn grdMain, kColRefinery1
    gGridHideColumn grdMain, kColRefinery1
    
    gGridSetColumnType grdMain, kColTargetLoads1, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColTargetLoads1, "Target Loads 1"
    gGridSetColumnWidth grdMain, kColTargetLoads1, "10"
    gGridLockColumn grdMain, kColTargetLoads1
    gGridHideColumn grdMain, kColTargetLoads1
    
    gGridSetColumnType grdMain, kColProducts1, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColProducts1, "Products 1"
    gGridSetColumnWidth grdMain, kColProducts1, "10"
    gGridLockColumn grdMain, kColProducts1
    gGridHideColumn grdMain, kColProducts1
    
    gGridSetColumnType grdMain, kColRefinery2, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColRefinery2, "Refinery 2"
    gGridSetColumnWidth grdMain, kColRefinery2, "10"
    gGridLockColumn grdMain, kColRefinery2
    gGridHideColumn grdMain, kColRefinery2
    
    gGridSetColumnType grdMain, kColTargetLoads2, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColTargetLoads2, "Target Loads 2"
    gGridSetColumnWidth grdMain, kColTargetLoads2, "10"
    gGridLockColumn grdMain, kColTargetLoads2
    gGridHideColumn grdMain, kColTargetLoads2
    
    gGridSetColumnType grdMain, kColProducts2, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColProducts2, "Products 2"
    gGridSetColumnWidth grdMain, kColProducts2, "10"
    gGridLockColumn grdMain, kColProducts2
    gGridHideColumn grdMain, kColProducts2
    
    gGridSetColumnType grdMain, kColRefinery3, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColRefinery3, "Refinery 3"
    gGridSetColumnWidth grdMain, kColRefinery3, "10"
    gGridLockColumn grdMain, kColRefinery3
    gGridHideColumn grdMain, kColRefinery3
    
    gGridSetColumnType grdMain, kColTargetLoads3, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColTargetLoads3, "Target Loads 3"
    gGridSetColumnWidth grdMain, kColTargetLoads3, "10"
    gGridLockColumn grdMain, kColTargetLoads3
    gGridHideColumn grdMain, kColTargetLoads3
    
    gGridSetColumnType grdMain, kColProducts3, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColProducts3, "Products 3"
    gGridSetColumnWidth grdMain, kColProducts3, "10"
    gGridLockColumn grdMain, kColProducts3
    gGridHideColumn grdMain, kColProducts3
    
    gGridSetColumnType grdMain, kColWinterFillCmnts, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColWinterFillCmnts, "Winter Fill Cmnts"
    gGridSetColumnWidth grdMain, kColWinterFillCmnts, "10"
    gGridLockColumn grdMain, kColWinterFillCmnts
    gGridHideColumn grdMain, kColWinterFillCmnts


    bSetupGrid = True

    Exit Function                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) bSetupGrid - " & Err.Description    'Generic Error Rig
End Function

Private Sub BindForm()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    '-- create a new data Manager Form class
    Set moDmForm = New clsDmForm

    With moDmForm
        Set .Form = frmDailyReportEntry
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        
        .AppName = Me.Caption
        .Table = "tsoDailyReportEntry_SGS"
        .UniqueKey = "WhseID, EntryDate"

        Set .ValidationMgr = valMgr
        
        .SaveOrder = 1
        
        .Bind Nothing, "CompanyID", SQL_CHAR
        .Bind lkuWhse, "WhseID", SQL_VARCHAR

        .Bind tdDate, "EntryDate", SQL_DATE
        
        .Bind numTrucks, "Trucks", SQL_INTEGER, kDmSetNull
        .Bind numTruckTons, "TruckTons", SQL_DECIMAL, kDmSetNull
        .Bind numRailCars, "RailCars", SQL_INTEGER, kDmSetNull
        .Bind numRailTons, "RailTons", SQL_DECIMAL, kDmSetNull
        .Bind numSalaryEmployees, "SalaryEmployees", SQL_INTEGER, kDmSetNull
        .Bind numOneXHours, "OneXHours", SQL_DECIMAL, kDmSetNull
        .Bind numOverTimeHours, "OverTimeHours", SQL_DECIMAL, kDmSetNull
        .Bind numPaidTimeOff, "PaidTimeOff", SQL_DECIMAL, kDmSetNull
        .Bind numTherms, "Therms", SQL_DECIMAL, kDmSetNull
        .Bind numNonIdealLoads, "NonIdealLoads", SQL_INTEGER, kDmSetNull
        .Bind numJBTAssignedTrucks, "JBTAssignedTrucks", SQL_INTEGER, kDmSetNull

        .Bind txtQualityCmnt, "QualityCmnt", SQL_VARCHAR, kDmSetNull
        .Bind txtOperationsCmnt, "OperationsCmnt", SQL_VARCHAR, kDmSetNull
        .Bind txtTruckingLogisticsCmnt, "TruckingLogisticsCmnt", SQL_VARCHAR, kDmSetNull
        
'        .BindComboBox ddRefinery1, "Refinery1", SQL_INTEGER, kDmSetNull
        .Bind ddRefinery1, "Refinery1", SQL_VARCHAR, kDmSetNull
        .Bind numTargetLoads1, "TargetLoads1", SQL_INTEGER, kDmSetNull
        .Bind ddroducts1, "Products1", SQL_VARCHAR, kDmSetNull

'        .BindComboBox ddRefinery2, "Refinery2", SQL_INTEGER, kDmSetNull
        .Bind ddRefinery2, "Refinery2", SQL_VARCHAR, kDmSetNull
        .Bind numTargetLoads2, "TargetLoads2", SQL_INTEGER, kDmSetNull
        .Bind ddroducts2, "Products2", SQL_VARCHAR, kDmSetNull

'        .BindComboBox ddRefinery3, "Refinery3", SQL_INTEGER, kDmSetNull
        .Bind ddRefinery3, "Refinery3", SQL_VARCHAR, kDmSetNull
        .Bind numTargetLoads3, "TargetLoads3", SQL_INTEGER, kDmSetNull
        .Bind ddroducts3, "Products3", SQL_VARCHAR, kDmSetNull

        .Bind txtWinterFillCmnt, "WinterFillCmnts", SQL_VARCHAR, kDmSetNull

        .Init
    End With

    Set moDmGrid = New clsDmGrid
    
    With moDmGrid
        Set .Form = frmDailyReportEntry
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        .OrderBy = "WhseID, EntryDate"
        .UniqueKey = "WhseID, EntryDate"
'        .Where = "CompanyID = '" & msCompanyID & "'"
        .Where = GetHistoryFilter()
        Set .Grid = grdMain
        
        .ReadOnly = True
        .NoAppend = True
        .NoInsert = True
        .NoDelete = True
        .NoMessages = True
        
        .Table = "tsoDailyReportEntry_SGS"
        .BindColumn "EntryDate", kColEntryDate, SQL_DATE
        .BindColumn "WhseID", kColWhseID, SQL_CHAR
        .BindColumn "CompanyID", kColCompanyID, SQL_CHAR
        .BindColumn "Trucks", kColTrucks, SQL_INTEGER
        .BindColumn "TruckTons", kColTruckTons, SQL_DECIMAL
        .BindColumn "RailCars", kColRailCars, SQL_INTEGER
        .BindColumn "RailTons", kColRailTons, SQL_DECIMAL
        .BindColumn "SalaryEmployees", kColSalaryEmployees, SQL_INTEGER
        .BindColumn "OneXHours", kColOneXHours, SQL_DECIMAL
        .BindColumn "OverTimeHours", kColOverTimeHours, SQL_DECIMAL
        .BindColumn "PaidTimeOff", kColPaidTimeOff, SQL_DECIMAL
        .BindColumn "Therms", kColTherms, SQL_DECIMAL
        .BindColumn "NonIdealLoads", kColNonIdealLoads, SQL_INTEGER
        .BindColumn "JBTAssignedTrucks", kColJBTAssignedTrucks, SQL_INTEGER
        .BindColumn "QualityCmnt", kColQualityCmnt, SQL_CHAR
        .BindColumn "OperationsCmnt", kColOperationsCmnt, SQL_CHAR
        .BindColumn "TruckingLogisticsCmnt", kColTruckingLogisticsCmnt, SQL_CHAR
        .BindColumn "Refinery1", kColRefinery1, SQL_CHAR
        .BindColumn "TargetLoads1", kColTargetLoads1, SQL_INTEGER
        .BindColumn "Products1", kColProducts1, SQL_CHAR
        .BindColumn "Refinery2", kColRefinery2, SQL_CHAR
        .BindColumn "TargetLoads2", kColTargetLoads2, SQL_INTEGER
        .BindColumn "Products2", kColProducts2, SQL_CHAR
        .BindColumn "Refinery3", kColRefinery3, SQL_CHAR
        .BindColumn "TargetLoads3", kColTargetLoads3, SQL_INTEGER
        .BindColumn "Products3", kColProducts3, SQL_CHAR
        .BindColumn "WinterFillCmnts", kColWinterFillCmnts, SQL_CHAR

        .Init
    End With

    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) BindForm - " & Err.Description    'Generic Error Rig
End Sub

Private Sub BindGM()
       
 'Bind the grid manager
    Set moGM = New clsGridMgr

    With moGM
        Set .Grid = grdMain
        Set .DM = moDmGrid
        Set .Form = frmDailyReportEntry
        If miSecurityLevel = kSecLevelDisplayOnly Then
            .GridType = kGridDataSheetNoAppend
        Else
            .GridType = kGridDataSheet
        End If
        
        .Init
    End With


End Sub


Public Sub HandleToolbarClick(sKey As String)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If
    Dim iActionCode As Integer
    Dim iConfirmUnload As Integer
    Dim lRet As Long
    Dim sNewKey As String
    Dim vParseRet As Variant
 
    iActionCode = kDmFailure
    
    Select Case sKey
        Case kTbFinish, kTbFinishExit
            If Not valMgr.ValidateForm Then Exit Sub
            moDmForm.Action (kDmFinish)
        
        Case kTbSave
            If Not valMgr.ValidateForm Then Exit Sub
            moDmForm.Save (True)

        Case kTbCancel, kTbCancelExit
            iActionCode = moDmForm.Action(kDmCancel)
        
        Case kTbDelete
            iActionCode = moDmForm.Action(kDmDelete)

        Case kTbCopyFrom
            '-- this is a future enhancement to Data Manager
            'iActionCode = moDmForm.CopyFrom

        Case kTbRenameId
            moDmForm.RenameID
            
        Case kTbPrint
            If moDmForm.ConfirmUnload(True) Then                 'Have user save changes
                'gPrintTask moClass.moFramework, ktskPrint
            End If
        
        Case kTbHelp
            gDisplayFormLevelHelp Me
        
        Case Else
            tbrMain.GenericHandler sKey, Me, moDmForm, moClass
            
    End Select

    Select Case iActionCode
        Case kDmSuccess
            moClass.lUIActive = kChildObjectInactive
            
        Case kDmFailure, kDmError
            Exit Sub
             
    End Select
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) HandleToolbarClick - " & Err.Description    'Generic Error Rig
End Sub

Private Sub cmdNext_Click()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    
    Dim msgrslt As VbMsgBoxResult
    Dim currState As DMStates
    Dim currDate As Date
    
    currState = moDmForm.State
    
    Select Case currState
        Case kDmStateAdd, kDmStateEdit
            If IsDate(tdDate.Value) = True Then
                currDate = tdDate.Value
            Else
                currDate = DateAdd("d", -1, Date)
            End If
            
            If moDmForm.IsDirty = True Then
                msgrslt = MsgBox("Do you want to save your changes?", vbQuestion + vbYesNoCancel, "MAS 500")
                
                If msgrslt = vbYes Then
                    'Save then advance
                    HandleToolbarClick kTbFinishExit
                ElseIf msgrslt = vbCancel Then
                    'Do nothing
                    Exit Sub
                ElseIf msgrslt = vbNo Then
                    'Don't Save just Cancel the current record then advance
                    HandleToolbarClick kTbCancel
                End If
            Else
                
                HandleToolbarClick kTbCancel
                
            End If
        
        Case Else
            If IsDate(tdDate.Value) = True Then
                currDate = tdDate.Value
            Else
                currDate = DateAdd("d", -1, Date)
            End If
            
        
    End Select
    
    tdDate.SetFocus
    tdDate.Value = DateAdd("d", 1, currDate)
    
    SendKeys "{TAB}"
    
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) cmdNext_Click - " & Err.Description    'Generic Error Rig

End Sub

Private Sub cmdPrevious_Click()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    
    Dim msgrslt As VbMsgBoxResult
    Dim currState As DMStates
    Dim currDate As Date
    
    currState = moDmForm.State
    
    Select Case currState
        Case kDmStateAdd, kDmStateEdit
            If IsDate(tdDate.Value) = True Then
                currDate = tdDate.Value
            Else
                currDate = DateAdd("d", 1, Date)
            End If
            
            If moDmForm.IsDirty = True Then
                msgrslt = MsgBox("Do you want to save your changes?", vbQuestion + vbYesNoCancel, "MAS 500")
                
                If msgrslt = vbYes Then
                    'Save then advance
                    HandleToolbarClick kTbFinishExit
                ElseIf msgrslt = vbCancel Then
                    'Do nothing
                    Exit Sub
                ElseIf msgrslt = vbNo Then
                    'Don't Save just Cancel the current record then advance
                    HandleToolbarClick kTbCancel
                End If
            Else
                
                HandleToolbarClick kTbCancel
                
            End If
        
        Case Else
            If IsDate(tdDate.Value) = True Then
                currDate = tdDate.Value
            Else
                currDate = DateAdd("d", 1, Date)
            End If
            
        
    End Select
    
    tdDate.SetFocus
    tdDate.Value = DateAdd("d", -1, currDate)
    
    SendKeys "{TAB}"
    
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) cmdPrevious_Click - " & Err.Description    'Generic Error Rig

End Sub

Private Sub cmdRefresh_Click()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    
    moDmGrid.Where = GetHistoryFilter
    moDmGrid.Init
    
    
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) cmdRefresh_Click - " & Err.Description    'Generic Error Rig
End Sub

Private Sub cmdSave_Click()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Dim sSQL As String
    
    If glGetValidLong(moClass.moAppDB.Lookup("Count(*)", "tsoDlyRptEntryDfltPlnt_SGS", "UserID = " & gsQuoted(msUserID))) > 0 Then
        'Record Exists just update
        sSQL = "Update tsoDlyRptEntryDfltPlnt_SGS Set DfltPlant = " & gsQuoted(lkuDefaultPlant.Text) & " Where UserID = " & gsQuoted(msUserID) & " "
        
    Else
        'Record Does Not Exist create
        sSQL = "Insert Into tsoDlyRptEntryDfltPlnt_SGS (UserID, DfltPlant) " & _
        "Values(" & gsQuoted(msUserID) & ", " & gsQuoted(lkuDefaultPlant.Text) & ") "
    
    End If
    
    moClass.moAppDB.ExecuteSQL sSQL
    
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) cmdSave_Click - " & Err.Description    'Generic Error Rig
End Sub

Private Sub ddRefinery1_GotFocus()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
'    SSTab1.Tab = 3
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) ddRefinery1_GotFocus - " & Err.Description    'Generic Error Rig
End Sub

Private Sub Form_Initialize()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    mbCancelShutDown = False
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) Form_Initialize - " & Err.Description    'Generic Error Rig
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig

    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
    End Select

    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) Form_KeyDown - " & Err.Description    'Generic Error Rig
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
               gProcessSendKeys "{TAB}"
               KeyAscii = 0
            End If
        
        Case Else
    
    End Select
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) Form_KeyPress - " & Err.Description    'Generic Error Rig
End Sub

Private Sub Form_Load()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig

    mbLoadSuccess = False
    msCompanyID = moClass.moSysSession.CompanyId
    msUserID = moClass.moSysSession.UserId
    mbEnterAsTab = moClass.moSysSession.EnterAsTab
    mlLanguage = moClass.moSysSession.Language
        
    miOldFormHeight = Me.Height
    miMinFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormWidth = Me.Width
    
    Me.Caption = "Daily Report Entry"
    
    'Hide the winter Fill Tab
    SSTab1.TabVisible(3) = False
    
    BindContextMenu
    msLookupRestrict = "CompanyID = " & gsQuoted(msCompanyID)
   
    SetupLookups
    SetupDropDowns
    
    'Get the Default Plant
    lkuDefaultPlant.Text = Trim(gsGetValidStr(moClass.moAppDB.Lookup("DfltPlant", "tsoDlyRptEntryDfltPlnt_SGS", "UserID = " & gsQuoted(msUserID))))
    lkuFilterPlant.Text = lkuDefaultPlant.Text
    
    'Default the history grid to filter by two weeks
    dtStartDate.Value = DateAdd("d", -14, Date)
    dtEndDate.Value = Date
    
    If Not bSetupGrid Then
        MsgBox "Error setting up the history grid.", vbExclamation, "MAS 500"
    End If
    
    BindForm
    BindGM
    
    With tbrMain
        .RemoveButton kTbMemo
        .LocaleID = mlLanguage
    End With
    
    sbrMain.MessageVisible = False
    Set sbrMain.Framework = oClass.moFramework
    Set moDmForm.SOTAStatusBar = sbrMain

    miSecurityLevel = giSetAppSecurity(moClass, tbrMain, moDmForm)
    
    miFilter = RSID_UNFILTERED
    
    moDmForm.SetUIStatusNone
    mbLoadSuccess = True
    
    Timer1.Enabled = True
    
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) Form_Load - " & Err.Description    'Generic Error Rig
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
    Dim iConfirmUnload As Integer
   
    '-- reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

    If moClass.mlError = 0 Then

        '-- if the form is dirty, prompt the user to save the record
        bConfirmUnload iConfirmUnload, True
        
        Select Case iConfirmUnload
            Case kDmSuccess
                'Do Nothing
                
            Case kDmFailure
                GoTo CancelShutDown
                
            Case kDmError
                GoTo CancelShutDown
                
            Case Else
                giSotaMsgBox Nothing, moClass.moSysSession, kmsgUnexpectedConfirmUnloadRV, iConfirmUnload
                
        End Select
      
        '-- Check all other forms that may have been loaded from this main form.
        '-- If there are any Visible forms, then this means the form is Active.
        '-- Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then GoTo CancelShutDown
    
        Select Case UnloadMode
            Case vbFormCode
                'Do Nothing
            
            Case Else
                '-- Most likely the user has requested to shut down the form.
                '-- If the context is normal or Drill-Around, have the object unload itself.
                Select Case mlRunMode
                    Case Else
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                        
                End Select
        End Select
    End If
    
    '-- If execution gets to this point, the form and class object of the form
    '-- will be shut down. Perform all operations necessary for a clean shutdown.
    PerformCleanShutDown
        
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
            
        Case Else
            'Do Nothing
            
    End Select

    Exit Sub
    
CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) Form_QueryUnload - " & Err.Description    'Generic Error Rig
End Sub

Public Sub PerformCleanShutDown()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig

#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    If Not moDmForm Is Nothing Then
        moDmForm.UnloadSelf
        Set moDmForm = Nothing
    End If
    
    If (Not (moGM Is Nothing)) Then
        moGM.UnloadSelf
        Set moGM = Nothing
    End If

    'Set Data Manager references to Nothing
    If Not moDmGrid Is Nothing Then
        moDmGrid.UnloadSelf
        Set moDmGrid = Nothing
    End If
 
    '-- Unload all forms loaded from this main form
    gUnloadChildForms Me
         
    '-- Remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1

   TerminateControls Me

   '-- If this form loads any other modal objects
    Set moSotaObjects = Nothing
    Set moContextMenu = Nothing

    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) PerformCleanShutDown - " & Err.Description    'Generic Error Rig
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Set moClass = Nothing
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) Form_Unload - " & Err.Description    'Generic Error Rig
End Sub




Private Sub lkuDefaultPlant_Validate(Cancel As Boolean)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    
    If Trim(lkuDefaultPlant.Text) <> Empty Then
        If glGetValidLong(moClass.moAppDB.Lookup("Count(*)", "vluWarehouseDesc_SGS", "WhseID = " & gsQuoted(lkuDefaultPlant.Text))) <= 0 Then
            Cancel = True
            MsgBox "This is not a valid plant.", vbExclamation, "MAS 500"
            Exit Sub
        End If
    End If
    
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) lkuDefaultPlant_Validate - " & Err.Description    'Generic Error Rig
End Sub

Private Sub lkuWhse_LookupClick(bCancel As Boolean)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    bCancel = Not bConfirmUnload(0, True)

    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) lkuWhse_LookupClick - " & Err.Description    'Generic Error Rig
End Sub

Private Sub lkuWhse_LostFocus()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    If Len(Trim$(lkuWhse.Text)) = 0 Then
        lblWhseDesc.Caption = ""
        Exit Sub
    End If

    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) lkuWhse_LostFocus - " & Err.Description    'Generic Error Rig
End Sub


Private Sub lkuWhse_Validate(Cancel As Boolean)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    
    If Trim(lkuWhse.Text) <> Empty Then
        If glGetValidLong(moClass.moAppDB.Lookup("Count(*)", "vluWarehouseDesc_SGS", "WhseID = " & gsQuoted(lkuWhse.Text))) <= 0 Then
            Cancel = True
            MsgBox "This is not a valid plant.", vbExclamation, "MAS 500"
            Exit Sub
        End If
    End If
    
    If ChangeInRec() = True Then
        
        valMgr_KeyChange
    End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) lkuWhse_Validate - " & Err.Description    'Generic Error Rig
End Sub

Private Sub moDmForm_DMBeforeInsert(bValid As Boolean)
'********************************************************************
' Description:
'    This routine will be called by Data Manager just before an
'    Insert. We need to get the next surrogate key for the table.
'********************************************************************
    Dim lKey As Long

    On Error GoTo CancelInsert

    bValid = True


'    With moClass.moAppDB
'        .SetInParam "tsoDailyReportEntry_SGS"
'        .SetOutParam lKey
'        .ExecuteSP ("spGetNextSurrogateKey")
'        lKey = .GetOutParam(2)
'        .ReleaseParams
'    End With

'    moDmForm.SetColumnValue "EntryDate", lKey

    If IsDate(tdDate.Value) = False Then
        tdDate.Value = Date
    End If
    
    moDmForm.SetColumnValue "EntryDate", tdDate.Value
    
    bValid = True

    Exit Sub
CancelInsert:
    bValid = False

End Sub

Private Sub moDmForm_DMStateChange(iOldState As Integer, iNewState As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    
    Select Case iNewState
        Case Is = kDmStateNone
            tbrMain.SetState sotaTB_NONE
            sbrMain.Status = SOTA_SB_START
            
            lblWhseDesc.Caption = Empty
            
        Case Is = kDmStateAdd
            moDmForm.SetDirty True

            
            
            tbrMain.SetState sotaTB_ADD
            sbrMain.Status = SOTA_SB_ADD
            
'            lkuItem.Enabled = False
            lkuWhse.Enabled = False
            
       
       Case Is = kDmStateEdit
            tbrMain.SetState sotaTB_EDIT
            sbrMain.Status = SOTA_SB_EDIT
            
            
'            lkuItem.Enabled = False
            lkuWhse.Enabled = False
            
    End Select
    
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) moDmForm_DMStateChange - " & Err.Description    'Generic Error Rig
End Sub

Private Sub moDmForm_DMValidate(bValid As Boolean)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    bValid = False

    'Prove Valid.  Invalid upon error

    bValid = True
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) moDmForm_DMValidate - " & Err.Description    'Generic Error Rig
End Sub

Private Sub navMain_Click()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Dim iActionCode As Integer
    Dim colRetVals As Collection


    SetupNavMain
    
    'Launch the Navigator.
'    mbF4Pressed = True
    DoEvents
'    mbF4Pressed = False

    Set colRetVals = gcLookupClick(Me, navMain, lkuWhse, "WhseID")
    
    SetHourglass True
    
    If colRetVals.Count <> 0 Then
        tdDate.Value = colRetVals("EntryDate")
'        lkuWhse.KeyValue = colRetVals("WhseKey")
        lkuWhse.Text = colRetVals("WhseID")
        
'        bIsValidID
    
        tdDate.Enabled = False
        lkuWhse.Enabled = False
    
'        tabInventory.SetFocus
    End If
    
    SetHourglass False

    moDmForm.SetDirty False

    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) navMain_Click - " & Err.Description    'Generic Error Rig
End Sub


Private Sub numSalaryEmployees_GotFocus()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    
    SSTab1.Tab = 1

    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) numSalaryEmployees_GotFocus - " & Err.Description    'Generic Error Rig
End Sub

Private Sub numTrucks_GotFocus()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    SSTab1.Tab = 0
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) numTrucks_GotFocus - " & Err.Description    'Generic Error Rig
End Sub


Private Sub sbrMain_ButtonClick(sButton As String)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    
    Dim lRet                  As Long
    Dim sNewKey          As String
    Dim iConfirmUnload  As Integer

    Select Case sButton

        Case kTbFilter

            miFilter = giToggleLookupFilter(miFilter)

        Case kTbFirst, kTbPrevious, kTbLast, kTbNext
            SetupNavMain

            bConfirmUnload iConfirmUnload, True
            If Not iConfirmUnload = kDmSuccess Then Exit Sub
          
            lRet = glLookupBrowse(navMain, sButton, miFilter, sNewKey)
            Select Case lRet
                Case Is = MS_SUCCESS
                    If navMain.ReturnColumnValues.Count = 0 Then
                        Exit Sub
                    End If
                    
                    tdDate.Value = CDate(gsGetValidStr(navMain.ReturnColumnValues("EntryDate")))
'                    lkuWhse.KeyValue = glGetValidLong(navMain.ReturnColumnValues("WhseKey"))
                    lkuWhse.Text = gsGetValidStr(navMain.ReturnColumnValues("WhseID"))
                    
'                    bIsValidID
                    
                    moDmForm.SetDirty False

                Case Else
                    gLookupBrowseError lRet, Me, moClass
            End Select

        End Select

    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) sbrMain_ButtonClick - " & Err.Description    'Generic Error Rig
End Sub

Private Sub tdDate_Validate(Cancel As Boolean)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    
    Dim MaxFutureDays As Long
    
    If Trim(gsGetValidStr(tdDate.Value)) = Empty Then
        'No date was entered
        Exit Sub
    End If
    
    If IsDate(tdDate.Value) = False And Trim(tdDate.Value) <> Empty Then
        MsgBox "Invalid Date", vbExclamation, "MAS 500"
        Cancel = True
        Exit Sub
    End If
    
    'Get Max Future Days
    MaxFutureDays = glGetValidLong(moClass.moAppDB.Lookup("Min(MaxFutureDays)", "tsoDailyReportMaxFutureDays_SGS", Empty))
    If MaxFutureDays < 0 Then
        MaxFutureDays = 0
    End If
    
    'Make sure the date is current or in the past
    If CDate(tdDate.Value) > DateAdd("d", MaxFutureDays, Date) Then
        If MaxFutureDays > 0 Then
            MsgBox "You can only select " & MaxFutureDays & " days in the future, or dates in the past.", vbExclamation, "MAS 500"
        Else
            MsgBox "You can only select the current date or dates in the past.", vbExclamation, "MAS 500"
        End If
        Cancel = True
        Exit Sub
    End If
    
    If ChangeInRec() = True Then
        valMgr_KeyChange
    End If

    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) tdDate_Validate - " & Err.Description    'Generic Error Rig
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    
    Timer1.Enabled = False
    
    'Load todays data
    tdDate.Value = Date

    SendKeys "{TAB}"
    
    Err.Clear
End Sub

Private Sub txtQualityCmnt_GotFocus()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    SSTab1.Tab = 2
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) txtQualityCmnt_GotFocus - " & Err.Description    'Generic Error Rig

End Sub

Private Sub valMgr_KeyChange()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    moDmForm.SetColumnValue "CompanyID", msCompanyID
    
'    If IsDate(tdDate.Value) = False Then
'        tdDate.Value = Date
'    End If
'    moDmForm.SetColumnValue "EntryDate", tdDate.Value
    
'    moDmForm.SetColumnValue "WhseKey", glGetValidLong(lkuWhse.KeyValue)
    
    
    moDmForm.KeyChange
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) valMgr_KeyChange - " & Err.Description    'Generic Error Rig
End Sub


Private Sub tbrMain_ButtonClick(Button As String)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    HandleToolbarClick Button
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) tbrMain_ButtonClick - " & Err.Description    'Generic Error Rig
End Sub

Private Sub Form_Activate()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig

    With valMgr
        Set .Framework = moClass.moFramework
        
        .Keys.Add tdDate
        .Keys.Add lkuWhse
        .Keys.Add navMain
        
        .Keys.Add lkuDefaultPlant
        .Keys.Add cmdSave
        .Keys.Add cmdPrevious
        .Keys.Add cmdNext

        .Init
    End With

#If CUSTOMIZER Then
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyDataBindings moDmForm
                moFormCust.ApplyFormCust
        End If
    End If
#End If

    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) Form_Activate - " & Err.Description    'Generic Error Rig
End Sub


Public Property Get FormHelpPrefix() As String
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    FormHelpPrefix = "SOZ"         '       Place your prefix here
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Property Get) FormHelpPrefix - " & Err.Description    'Generic Error Rig
End Property

Public Property Get WhatHelpPrefix() As String
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    WhatHelpPrefix = "SOZ"         '       Place your prefix here
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Property Get) WhatHelpPrefix - " & Err.Description    'Generic Error Rig
End Property

Public Property Get oClass() As Object
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Set oClass = moClass
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Property Get) oClass - " & Err.Description    'Generic Error Rig
End Property
Public Property Set oClass(oNewClass As Object)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Set moClass = oNewClass
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Property Set) oClass - " & Err.Description    'Generic Error Rig
End Property

Public Property Get lRunMode() As Long
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    lRunMode = mlRunMode
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Property Get) lRunMode - " & Err.Description    'Generic Error Rig
End Property
Public Property Let lRunMode(lNewRunMode As Long)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    mlRunMode = lNewRunMode
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Property Let) lRunMode - " & Err.Description    'Generic Error Rig
End Property

Public Property Get bCancelShutDown()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    bCancelShutDown = mbCancelShutDown
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Property Get) bCancelShutDown - " & Err.Description    'Generic Error Rig
End Property

Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Skip +++
    bLoadSuccess = mbLoadSuccess
End Property

Public Property Let bLoadSuccess(bNewLoadSuccess As Boolean)
'+++ VB/Rig Skip +++
    mbLoadSuccess = bNewLoadSuccess
End Property

Private Sub SetupLookups()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    'Setting up the warehouse look up.
    Set lkuWhse.Framework = moClass.moFramework
    Set lkuWhse.SysDB = moClass.moAppDB
    Set lkuWhse.AppDatabase = moClass.moAppDB
'    lkuWhse.RestrictClause = "WhseID IN('B', 'H', 'N', 'R', 'S', 'W') "

'SGS DEJ Setup Default Plant (START)
    Set lkuDefaultPlant.Framework = moClass.moFramework
    Set lkuDefaultPlant.SysDB = moClass.moAppDB
    Set lkuDefaultPlant.AppDatabase = moClass.moAppDB
'    lkuDefaultPlant.RestrictClause = "WhseID IN('B', 'H', 'N', 'R', 'S', 'W') "
'SGS DEJ Setup Default Plant (STOP)

'SGS DEJ Setup History Plant Filter (START)
    Set lkuFilterPlant.Framework = moClass.moFramework
    Set lkuFilterPlant.SysDB = moClass.moAppDB
    Set lkuFilterPlant.AppDatabase = moClass.moAppDB
'    lkuFilterPlant.RestrictClause = "WhseID IN('B', 'H', 'N', 'R', 'S', 'W') "
'SGS DEJ Setup History Plant Filter (STOP)

    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) SetupLookups - " & Err.Description    'Generic Error Rig
End Sub


Private Sub SetupDropDowns()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    
    '-- Set up drop down properties (see APZDA001 for reference)
    
    Dim lSQL As String
    
    lSQL = "select Refinery, RefineryKey, OrderNo from tsoRefineries_SGS Order by OrderNo Desc, Refinery"
    
    ddRefinery1.SQLStatement = lSQL
    Set ddRefinery1.DBObject = moClass.moAppDB

    ddRefinery2.SQLStatement = lSQL
    Set ddRefinery2.DBObject = moClass.moAppDB

    ddRefinery3.SQLStatement = lSQL
    Set ddRefinery3.DBObject = moClass.moAppDB



    lSQL = "select Product, ProductKey from tsoProducts_SGS Order by Product"
    
    ddroducts1.SQLStatement = lSQL
    Set ddroducts1.DBObject = moClass.moAppDB

    ddroducts2.SQLStatement = lSQL
    Set ddroducts2.DBObject = moClass.moAppDB

    ddroducts3.SQLStatement = lSQL
    Set ddroducts3.DBObject = moClass.moAppDB

    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) SetupDropDowns - " & Err.Description    'Generic Error Rig
End Sub

Private Function bConfirmUnload(iConfirmUnload As Integer, Optional ByVal bNoClear As Boolean = False) As Boolean
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig

    Dim bValid As Boolean
    
    bConfirmUnload = False
    
    If Not valMgr.ValidateForm() Then Exit Function
    
    iConfirmUnload = moDmForm.ConfirmUnload(bNoClear)

    If (iConfirmUnload = kDmSuccess) Then
        bConfirmUnload = True
    End If

    Exit Function                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) bConfirmUnload - " & Err.Description    'Generic Error Rig
End Function

Private Sub ProcessCancel()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig

    moDmForm.Action kDmCancel

    '-- Set the detail controls as valid so the old values are correct
    valMgr.Reset

    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) ProcessCancel - " & Err.Description    'Generic Error Rig
End Sub

Public Property Get MyApp() As Object
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Set MyApp = App
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Property Get) MyApp - " & Err.Description    'Generic Error Rig
End Property

Public Property Get MyForms() As Object
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Set MyForms = Forms
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Property Get) MyForms - " & Err.Description    'Generic Error Rig
End Property

Public Property Get UseHTMLHelp() As Boolean
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    UseHTMLHelp = True  ' Form uses HTML Help (True/False)
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Property Get) UseHTMLHelp - " & Err.Description    'Generic Error Rig
End Property


Private Sub CustomButton_Click(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomButton_Click - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomButton_GotFocus - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomCurrency_Change - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomFrame_DblClick - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomOption_Click(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomOption_Click - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomSpin_UpClick - " & Err.Description    'Generic Error Rig
End Sub

Private Sub picdrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseDown Index, Button, Shift, x, y
    End If
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) picdrag_MouseDown - " & Err.Description    'Generic Error Rig
End Sub

Private Sub picdrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseMove Index, Button, Shift, x, y
    End If
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) picdrag_MouseMove - " & Err.Description    'Generic Error Rig
End Sub

Private Sub picdrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseUp Index, Button, Shift, x, y
    End If
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) picdrag_MouseUp - " & Err.Description    'Generic Error Rig
End Sub

Private Sub picdrag_Paint(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_Paint Index
    End If
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) picdrag_Paint - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomButton_LostFocus - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomCheck_Click(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomCheck_Click - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomCheck_GotFocus - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomCheck_LostFocus - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomCombo_Change(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomCombo_Change - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomCombo_Click(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomCombo_Click - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomCombo_DblClick - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomCombo_GotFocus - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomCombo_KeyPress - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomCombo_LostFocus - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomCurrency_GotFocus - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomCurrency_KeyPress - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomCurrency_LostFocus - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomDate_Change(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomDate_Change - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomDate_Click(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomDate_Click - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomDate_DblClick - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomDate_GotFocus - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomDate_KeyPress - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomDate_LostFocus - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomFrame_Click(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomFrame_Click - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomLabel_Click(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomLabel_Click - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomLabel_DblClick - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomMask_Change(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomMask_Change - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomMask_GotFocus - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomMask_KeyPress - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomMask_LostFocus - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomNumber_Change(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomNumber_Change - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomNumber_GotFocus - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomNumber_KeyPress - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomNumber_LostFocus - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomOption_DblClick - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomOption_GotFocus - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomOption_LostFocus - " & Err.Description    'Generic Error Rig
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
#End If
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) CustomSpin_DownClick - " & Err.Description    'Generic Error Rig
End Sub

Private Sub valMgr_Validate(oControl As Object, iReturn As SOTAVM.SOTA_VALID_RETURN_TYPES, sMessage As String, ByVal iLevel As SOTAVM.SOTA_VALIDATION_LEVELS)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    iReturn = SOTA_VALID
    
    Select Case oControl.Name
        Case tdDate.Name
            If IsDate(tdDate.Value) = False Then
                iReturn = SOTA_INVALID
                MsgBox "Date is required", vbExclamation, "MAS 500"
                Exit Sub
            End If
        Case lkuWhse.Name
            If Len(Trim(lkuWhse.Text)) <= 0 Then
                iReturn = SOTA_INVALID
                MsgBox "Plant is required", vbExclamation, "MAS 500"
                Exit Sub
            End If
    End Select
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) valMgr_Validate - " & Err.Description    'Generic Error Rig
End Sub

Private Sub SetupNavMain()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    
    Dim sNavRestrict As String
    
     If Len(Trim(navMain.Tag)) = 0 Then
        'Setting the bottom navigator
        sNavRestrict = "CompanyID = " & gsQuoted(msCompanyID)
        Call gbLookupInit(navMain, moClass, moClass.moAppDB, "soDlyRprtEntry_SGS", sNavRestrict)
        navMain.Tag = navMain.LookupID
    End If

    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) SetupNavMain - " & Err.Description    'Generic Error Rig
End Sub


Private Function ChangeInRec() As Boolean
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Dim bDataValid As Boolean
    Dim bWhseValid As Boolean
    
    If tdDate.Enabled Then
        If IsDate(tdDate.Value) = True Then
            bDataValid = True
        End If
    End If

    If lkuWhse.EnabledText Then
        If Len(Trim(lkuWhse.Text)) > 0 Then
            bWhseValid = True
        ElseIf Len(Trim(lkuDefaultPlant.Text)) > 0 Then
            lkuWhse.Text = lkuDefaultPlant.Text
            bWhseValid = True
        End If
    End If
    
    If bWhseValid And bDataValid Then
        ChangeInRec = True
    End If
    

    Exit Function                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) ChangeInRec - " & Err.Description    'Generic Error Rig
End Function


Private Function GetHistoryFilter() As String
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Dim StartDate As Date
    Dim EndDate As Date
    Dim sFilter As String
    
    'Get Start Date
    If Trim(gsGetValidStr(dtStartDate.Value)) = Empty Then
        dtStartDate.Value = DateAdd("d", -14, Date)
        StartDate = DateAdd("d", -14, Date)
    Else
        If IsDate(dtStartDate.Value) Then
            StartDate = CDate(dtStartDate.Value)
        Else
            dtStartDate.Value = DateAdd("d", -14, Date)
            StartDate = DateAdd("d", -14, Date)
        End If
    End If
    
    'Get End Date
    If Trim(gsGetValidStr(dtEndDate.Value)) = Empty Then
        dtEndDate.Value = Date
        EndDate = Date
    Else
        If IsDate(dtEndDate.Value) Then
            EndDate = CDate(dtEndDate.Value)
        Else
            dtEndDate.Value = Date
            EndDate = Date
        End If
    End If
    
    sFilter = sFilter & " EntryDate Between " & gsQuoted(gsGetValidStr(StartDate)) & " and " & gsQuoted(gsGetValidStr(EndDate)) & " "
    
    If Trim(lkuFilterPlant.Text) <> Empty Then
        sFilter = sFilter & " And WhseID = " & gsQuoted(lkuFilterPlant.Text) & " "
    End If
    
    GetHistoryFilter = sFilter

    Exit Function                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmDailyReportEntry,  (Procedure) GetHistoryFilter - " & Err.Description    'Generic Error Rig
End Function



