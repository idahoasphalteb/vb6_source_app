Attribute VB_Name = "basSOCopy"
'*********************************************
'* NOTE: This module has NOT been VB/Rigged! *
'*********************************************

Option Explicit
'**********************************************************************
'     Name: baspurchord
'     Desc: Main Module of this Object containing "Sub Main ()".
' Original: 05-07-1997
'     Mods: mm/dd/yy XXX
'**********************************************************************

'-- Security Event Constants
    '-- if any security events are defined for this task, here is
    '-- where you would define the constant for the event
    'Public Const ksecChangeStatus As String = "CICHGSTAT"

'-- Object Task Constants
    '-- if this maintenance program has an associated report/listing,
    '-- here is where you would define the report/listing's Task ID so
    '-- that it may be launched when the Print button is pressed on the
    '-- toolbar
  
    'Public Const ktskPrint = 9998

Public mfrmMain                 As Form
Public mbEnterAsTab             As Boolean              ' use enter as tab?
Public msUserFld(3)             As String
Public mbDontRunGridClick       As Boolean

' tpoPurchOrder | ApprovalStatus
    Public Const kvApproveNone      As Integer = 0
    Public Const kvApproveSubmit    As Integer = 1
    Public Const kvApproveYes       As Integer = 2
    Public Const kvApproveNo        As Integer = 3

' tpoPurchOrder | CreateType
    Public Const kvCreateTypeOther  As Integer = 0
    Public Const kvCreateTypeStd    As Integer = 1
    Public Const kvCreateTypeImport As Integer = 2
    Public Const kvCreateTypeUpgrd  As Integer = 3

' tpoPurchOrder | RcvrTicketFormat
    Public Const kvRcvrTicketStd    As Integer = 0
    Public Const kvRcvrTicketBlind  As Integer = 1
    Public Const kvRcvrTicketMtrl   As Integer = 2

' tapVendor | Status
    Public Const kvActiveCust       As Integer = 1
    Public Const kvInactiveCust     As Integer = 2
    Public Const kvTemporaryCust    As Integer = 3
    Public Const kvDeletedCust      As Integer = 4

'-- Static List Constants
' timItem | Status
    Public Const kvActiveItem       As Integer = 1
    Public Const kvInactiveItem     As Integer = 2
    Public Const kvDiscontinuedItem As Integer = 3
    Public Const kvDeletedItem      As Integer = 4
    
    ' timItem | ItemType
    Public Const kvMiscItem         As Integer = 1
    Public Const kvService          As Integer = 2
    Public Const kvExpense          As Integer = 3
    Public Const kvCommentOnly      As Integer = 4
    Public Const kvFinishedGoods    As Integer = 5
    Public Const kvRawMaterial      As Integer = 6
    Public Const kvBTOKit           As Integer = 7
    Public Const kvAssembledKit     As Integer = 8
    Public Const kvCatalog          As Integer = 9

    Public moUF             As Object
    Public moSTax           As Object
    Public moRoundObj       As Object

    Public mlUniqueKey      As Long
 'Modified to invoke the spsoGetCustWID
 Public Type CustDflts
     'Customer
       iAllowInvtSubst            As Integer
       iBillingType               As Integer
       dCreditLimit               As Double
       iCredAgeCat                As Integer
       sCustID                    As String
       sCustName                  As String
       lBillToCustAddrKey         As Long
       sBillToCustAddrID          As String
       lDfltItemKey               As Long
       sDfltItemID                As String
       lDfltSalesAcctKey          As Long
       lShipToCustAddrKey         As Long
       sShipToCustAddrID          As String
       lShipToCustAddrUpdCtr      As String
       iCreditHold                As Integer
       lPrimaryAddrKey            As Long
       sPrimaryAddrLine1          As String
       sPrimaryCity               As String
       sPrimaryState              As String
       sPrimaryPostalCode         As String
       sPrimaryCountry            As String
       iReqPO                     As Integer
       dRetntPct                  As Double
       lDfltReturnAcctKey         As Long
       iStatus                    As Integer
       dTradeDiscPct              As Double
       lCustClassKey              As Long
       sCustClassID               As String
       sClassOvrdSegVal           As String
       iPriority                  As Integer
       lSource                    As Long
       sSource                    As String
     'Bill To
       lPmtTermsKey               As Long
       sPmtTermsID                As String
       lSOAckFormKey              As Long
       sSOAckFormID               As String
       lCurrExchSchdKey           As Long
       sCurrExchSchdID            As String
       sCurrID                    As String
       iPrintOrdAck               As Integer
       iReqAck                    As Integer
     'Ship To
       lDfltCntctKey              As Long
       sContactName               As String
       lPackListKey               As Long
       sPackListID                As String
       lShipMethKey               As Long
       sShipMethID                As String
       lShipZoneKey               As Long
       sShipZoneID                As String
       lSperKey                   As Long
       sSperID                    As String
       sSperName                  As String
       lSTaxSchdkey               As Long
       lShipToSTaxSchdKey         As Long
       sTaxSchedID                As String
       lFOBKey                    As Long
       sFOBID                     As String
       lCommPlanKey               As Long
       sCommPlanID                As String
       lWhseKey                   As Long
       sWhseID                    As String
       iShipDays                  As Integer
       iShipComplete              As Integer
       iTaxPoint                  As Integer
       iBillToNationalAcctParent  As Integer
       iPmtByNationalAcctParent   As Integer
       lNationalAcctParentCustKey As Long
       iNationalAcctHold          As Integer
       iWhseAllowImmedPick        As Integer
       iFreightMethod             As Integer
       dPrimaryLatitude           As Double	'Customer default Latitude 
       dPrimaryLongitude          As Double	'Customer default Longitude
    End Type
    
    Public Type ItemDflts
        lSOLineKey          As Long
        iSOLineNo           As Integer
        iPriceOvrd          As Integer
        iPromiseDateDirty   As Integer
        dQtyShipped         As Double
        dQtyInvcd           As Double
        dQtyRtrnCred        As Double
        dQtyRtrnRplc        As Double
        dAmtInvcd           As Double
        dOrigQtyOrd         As Double
        sOrigPromiseDate    As String
        lQuoteLineKey       As Long
        lSTaxTranKey        As Long
        lSTaxSchdkey        As Long
        lUpdateCounter      As Long
        lShipZoneKey        As Long
        lBlanketKey         As Long
        iCommentOnly        As Integer
        lSOLineDistKey      As Long
        sUserFld1           As String
        sUserFld2           As String
        dQTyBO              As Double
        lShipToAddrKey      As Long
        iTaxPoint           As Integer
        sAddrName           As String
        sAddr1              As String
        sAddr2              As String
        sAddr3              As String
        sAddr4              As String
        sAddr5              As String
        sCity               As String
        sState              As String
        sCountry            As String
        sZip                As String
        iInsertAddr         As Integer
        iUpdateAddr         As Integer
        lAddrUC             As Long
        iAddrForLine        As Integer
        lPOLineKey          As Long
    End Type
    
    Public Type ItemWasDflts
        lWhseKey            As Long
        iStatus             As Integer
        lItemKey            As Long
        sItemDesc           As String
        dQtyOrd             As Double
        dQtyOpen            As Double
        lUOMKey             As Long
        dUnitPrice          As Double
        dExtAmt             As Double
        lShipMethKey        As Long
        lFOBKey             As Long
        iPriority           As Integer
        lShipToCustAddrKey  As Long
        dFreightAmt         As Double
        iDropShip           As Integer
        lVendKey            As Long
        lVendAddrKey        As Long
        lCommPlanKey        As Long
        lCommClassKey       As Long
        lGLAcctKey          As Long
        lAcctRefKey         As Long
        sReqDate            As String
        sPromiseDate        As String
        sShipDate           As String
        lSTaxClassKey       As Long
        dSTaxAmt            As Double
        dTradeDiscAmt       As Double
        dTradeDiscPct       As Double
        sComment            As String
        iHold               As Integer
        sHoldRsn            As String
        lSOLineKey          As Long
        iSOLineNo           As Integer
        iPriceOvrd          As Integer
        iReqCert            As Integer
        iPromiseDateDirty   As Integer
        dQtyShipped         As Double
        dQtyInvcd           As Double
        dQtyRtrnCred        As Double
        dQtyRtrnRplc        As Double
        dAmtInvcd           As Double
        dOrigQtyOrd         As Double
        sOrigPromiseDate    As String
        lQuoteLineKey       As Long
        lSTaxTranKey        As Long
        lSTaxSchdkey        As Long
        lUpdateCounter      As Long
        lShipZoneKey        As Long
        lBlanketKey         As Long
        iCommentOnly        As Integer
        iInclOnPackList     As Integer
        iInclOnPickList     As Integer
        sCloseDate          As String
        lPOKey              As Long
        lSOLineDistKey      As Long
        sUserFld1           As String
        sUserFld2           As String
        dQTyBO              As Double
        lShipToAddrKey      As Long
        iTaxPoint           As Integer
        sAddrName           As String
        sAddr1              As String
        sAddr2              As String
        sAddr3              As String
        sAddr4              As String
        sAddr5              As String
        sCity               As String
        sState              As String
        sCountry            As String
        sZip                As String
        iInsertAddr         As Integer
        iUpdateAddr         As Integer
        lAddrUC             As Long
        iAddrForLine        As Integer
    End Type
    
    Public Const kColSOCompKey = 1
    Public Const kColSOCompItemKey = 2
    Public Const kColSOCompItemQty = 4
        
    Public Const kColSOCompItemID = 3
    

    Public Const kColSOSOLineNo = 1
    Public Const kColSOSOLineKey = 2
    Public Const kColSOItemKey = 3
    Public Const kColSOItemID = 4
    Public Const kColSODescription = 5
    Public Const kColSOQtyOrdered = 6
    Public Const kColSOUnitMeasKey = 7
    Public Const kColSOUnitMeasID = 8
    Public Const kColSOInclOnPick = 9
    Public Const kColSOUnitPrice = 10
    Public Const kColSOPriceOvrd = 11
    Public Const kColSONotUsed = 12
    Public Const kColSOCmntOnly = 13
    Public Const kColSOExtAmt = 14
    Public Const kColSOExtCmnt = 15
    Public Const kColSOQuoteLineKey = 16
    Public Const kColSOOrigPromiseDate = 17
    Public Const kColSOShipToAddrKey = 18
    Public Const kColSOShipToCustAddrKey = 19
    Public Const kColSOAddrForLine = 20
    Public Const kColSOAddrName = 21
    Public Const kColSOAddr1 = 22
    Public Const kColSOAddr2 = 23
    Public Const kColSOAddr3 = 24
    Public Const kColSOAddr4 = 25
    Public Const kColSOAddr5 = 26
    Public Const kColSOCity = 27
    Public Const kColSOState = 28
    Public Const kColSOCountry = 29
    Public Const kColSOZip = 30
    Public Const kColSOAddrUC = 31
    Public Const kColSOUpdateAddr = 32
    Public Const kColSOInsertAddr = 33
    Public Const kColSOQtyInvcd = 34
    Public Const kColSOFreightAmt = 35
    Public Const kColSOTradeDiscAmt = 36
    Public Const kColSOTradeDiscPct = 37
    Public Const kColSOReqCert = 38
    Public Const kColSOCommClassKey = 39
    Public Const kColSOCommPlanKey = 40
    Public Const kColSOQtyOpen = 41
    Public Const kColSOSTaxTranKey = 42
    Public Const kColSOSTaxClassKey = 43
    Public Const kColSOSTaxSchdKey = 44
    Public Const kColSOSTaxAmt = 45
    Public Const kColSOFOBKey = 46
    Public Const kColSOTaxPoint = 47
    Public Const kColSOShipMethKey = 48
    Public Const kColSOShipMethID = 49
    Public Const kColSOShipZoneKey = 50
    Public Const kColSOInclOnPack = 51
    Public Const kColSOQtyRtrnRplc = 52
    Public Const kColSOUserFld1 = 53
    Public Const kColSOUserFld2 = 54
    Public Const kColSORequestDate = 55
    Public Const kColSOPromiseDate = 56
    Public Const kColSOPromiseDateDirty = 57
    Public Const kColSOShipDate = 58
    Public Const kColSOOrigQtyOrd = 59
    Public Const kColSOQtyShipped = 60
    Public Const kColSOAmtInvcd = 61
    Public Const kColSOQtyRtrnCred = 62
    Public Const kColSOQtyBackOrd = 63
    Public Const kColSOGLAcctIDMsk = 64
    Public Const kColSOGLAcctDesc = 65
    Public Const kColSOCloseDate = 66
    Public Const kColSOHold = 67
    Public Const kColSOStatus = 68
    Public Const kColSOWhseKey = 69
    Public Const kColSOPONum = 70
    Public Const kColSOPOLineKey = 71
    Public Const kColSOStatusText = 72
    
    Public Const kChildSOMaxCols = 45
    Public Const kColChildSOLineDistKey = 1
    Public Const kColChildAmtInvcd = 2
    Public Const kColChildExtAmt = 3
    Public Const kColChildGLAcctKey = 4
    Public Const kColChildOrigOrdered = 5
    Public Const kColChildOrigPromiseDate = 6
    Public Const kColChildPromiseDate = 7
    Public Const kColChildQtyInvcd = 8
    Public Const kColChildQtyOnBO = 9
    Public Const kColChildQtyOpenShip = 10
    Public Const kColChildQtyOrd = 11
    Public Const kColChildQtyShipped = 12
    Public Const kColChildQtyRtrnCred = 13
    Public Const kColChildQtyRtrnRplc = 14
    Public Const kColChildRequestDate = 15
    Public Const kColChildShipMethKey = 16
    Public Const kColChildShipMethID = 17
    Public Const kColChildShipToCustAddrKey = 18
    Public Const kColChildVendKey = 19
    Public Const kColChildDropShip = 20
    Public Const kColChildWhseKey = 21
    Public Const kColChildShipZoneKey = 22
    Public Const kColChildSTaxTranKey = 23
    Public Const kColChildSTaxSchdKey = 24
    Public Const kColChildFrtAmt = 25
    Public Const kColChildFOBKey = 26
    Public Const kColChildPurchVendAddrKey = 27
    Public Const kColChildShipToAddrKey = 28
    Public Const kColChildStatus = 29
    Public Const kColChildAcctRefKey = 30
    Public Const kColChildPriority = 31
    Public Const kColChildBlnktKey = 32
    Public Const kColChildHold = 33
    Public Const kColChildHoldReason = 34
    Public Const kColChildPackListFormKey = 35
    Public Const kColChildTaxPoint = 36
    Public Const kColChildShipDate = 37
    Public Const kColChildSOLineKey = 38
    Public Const kColChildTradeDiscAmt = 39
    Public Const kColChildTradeDiscPct = 40
    
    Public msCurrSymbol         As String
    Public msHomeCurrSymbol     As String
    Public miDigAfterDec        As Integer
    Public miHomeDigAfterDec    As Integer
    Public miRoundPrec          As Integer
    Public miHomeRoundPrec      As Integer
    Public miRoundMeth          As Integer
    Public miHomeRoundMeth      As Integer
    Public mlCurrencyLocale     As Long
    Public mlHomeCurrencyLocale As Long

#If Prototype <> 1 Then
Const VBRIG_MODULE_ID_STRING = "PURCHORD.BAS"

Public Sub MyErrMsg(oClass As Object, sDesc As String, lErr As Long, sSub As String, sProc As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sText As String

    If lErr = guSotaErr.Number And Trim(guSotaErr.Description) <> Trim(sDesc) Then
        sDesc = sDesc & " " & guSotaErr.Description
    End If

    If oClass Is Nothing Then
        sText = sText & " AppName:  " & App.Title
        sText = sText & "    Error < " & Format(lErr)
        sText = sText & " >   occurred at < " & sSub
        sText = sText & " >  in Procedure < " & sProc
        sText = sText & " > " & sDesc
    Else
        sText = "Module: " & oClass.moSysSession.MenuModule
        sText = sText & "  Company: " & oClass.moSysSession.CompanyID
        sText = sText & "  AppName:  " & App.Title
        sText = sText & "    Error < " & Format(lErr)
        sText = sText & " >   occurred at < " & sSub
        sText = sText & " >  in Procedure < " & sProc
        sText = sText & " > " & sDesc
    End If
    
    MsgBox sText, vbExclamation, "Sage 500 ERP"

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MyErrMsg", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

#End If
Public Sub Main()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Main", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basSOCopy"
End Function

Public Function bOverrideSecEvent(sEventId As String, oClass As Object, Optional vPrompt As Variant = True)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
'***************************************************************
' Can the user override the security event passed in
'***************************************************************
Dim sUser       As String

    bOverrideSecEvent = False   'Set initial return code

  'User is Authorized set good return code
  'Check the user id typed in can override (no cancel hit)
    sUser = CStr(oClass.moSysSession.UserId)
    
    If oClass.moFramework.GetSecurityEventPerm(sEventId, sUser, vPrompt) <> 0 Then
        bOverrideSecEvent = True
    End If

Exit Function
                                                                                          'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bOverrideSecEvent", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
