VERSION 5.00
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#157.0#0"; "EntryLookupControls.ocx"
Begin VB.Form frmSOCopy 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Copy Sales Order"
   ClientHeight    =   4200
   ClientLeft      =   225
   ClientTop       =   465
   ClientWidth     =   4020
   HelpContextID   =   52826
   Icon            =   "soCopy.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4200
   ScaleWidth      =   4020
   Begin NEWSOTALib.SOTAMaskedEdit txtPurchOrder 
      Height          =   285
      Left            =   1920
      TabIndex        =   10
      Top             =   3840
      WhatsThisHelpID =   17775962
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      bAutoSelect     =   0   'False
      lMaxLength      =   15
   End
   Begin VB.CheckBox chkReprice 
      Alignment       =   1  'Right Justify
      Caption         =   "Re&price"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   1440
      WhatsThisHelpID =   52840
      Width           =   1965
   End
   Begin VB.CheckBox chkCopyDates 
      Alignment       =   1  'Right Justify
      Caption         =   "Copy &Date(s)"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   2160
      WhatsThisHelpID =   52841
      Width           =   1965
   End
   Begin VB.CheckBox chkCopyQuantities 
      Alignment       =   1  'Right Justify
      Caption         =   "Copy &Quantities"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   1800
      WhatsThisHelpID =   17777518
      Width           =   1965
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   4020
      _ExtentX        =   7091
      _ExtentY        =   741
      Style           =   4
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtNewSO 
      Height          =   285
      Left            =   1920
      TabIndex        =   1
      Top             =   720
      WhatsThisHelpID =   52838
      Width           =   1785
      _Version        =   65536
      _ExtentX        =   3149
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      lMaxLength      =   10
   End
   Begin SOTACalendarControl.SOTACalendar dteReqDate 
      CausesValidation=   0   'False
      Height          =   315
      Left            =   1920
      TabIndex        =   7
      Top             =   2760
      WhatsThisHelpID =   52837
      Width           =   1755
      _ExtentX        =   3096
      _ExtentY        =   556
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Text            =   "  /  /    "
      Object.CausesValidation=   0   'False
   End
   Begin SOTACalendarControl.SOTACalendar dtePromDate 
      CausesValidation=   0   'False
      Height          =   315
      Left            =   1920
      TabIndex        =   8
      Top             =   3120
      WhatsThisHelpID =   52836
      Width           =   1755
      _ExtentX        =   3096
      _ExtentY        =   556
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Text            =   "  /  /    "
      Object.CausesValidation=   0   'False
   End
   Begin SOTACalendarControl.SOTACalendar dteShipDate 
      CausesValidation=   0   'False
      Height          =   315
      Left            =   1920
      TabIndex        =   9
      Top             =   3480
      WhatsThisHelpID =   52835
      Width           =   1755
      _ExtentX        =   3096
      _ExtentY        =   556
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Text            =   "  /  /    "
      Object.CausesValidation=   0   'False
   End
   Begin SOTACalendarControl.SOTACalendar dteBegDate 
      CausesValidation=   0   'False
      Height          =   315
      Left            =   6240
      TabIndex        =   11
      Top             =   600
      WhatsThisHelpID =   52834
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   556
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Text            =   "  /  /    "
      Object.CausesValidation=   0   'False
   End
   Begin SOTACalendarControl.SOTACalendar dteEndDate 
      CausesValidation=   0   'False
      Height          =   315
      Left            =   6240
      TabIndex        =   12
      Top             =   990
      WhatsThisHelpID =   52833
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   556
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Text            =   "  /  /    "
      Object.CausesValidation=   0   'False
   End
   Begin SOTACalendarControl.SOTACalendar dteOrderDate 
      CausesValidation=   0   'False
      Height          =   315
      Left            =   1920
      TabIndex        =   6
      Top             =   2400
      WhatsThisHelpID =   17777520
      Width           =   1755
      _ExtentX        =   3096
      _ExtentY        =   556
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Text            =   "  /  /    "
      Object.CausesValidation=   0   'False
   End
   Begin EntryLookupControls.TextLookup lkuCustID 
      Height          =   285
      Left            =   1920
      TabIndex        =   2
      Top             =   1080
      WhatsThisHelpID =   17775971
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   503
      ForeColor       =   -2147483640
      MaxLength       =   12
      IsSurrogateKey  =   -1  'True
      LookupID        =   "ARCustomer  "
      ParentIDColumn  =   "CustID"
      ParentKeyColumn =   "CustKey"
      ParentTable     =   "tarCustomer"
      BoundColumn     =   "CustKey"
      BoundTable      =   "tsoSalesOrder"
      IsForeignKey    =   -1  'True
      sSQLReturnCols  =   "CustID,lkuCustID,;"
   End
   Begin VB.Label lblPurchOrder 
      AutoSize        =   -1  'True
      Caption         =   "Customer PO"
      Height          =   195
      Left            =   120
      TabIndex        =   21
      Top             =   3870
      Width           =   930
   End
   Begin VB.Label lblCustID 
      AutoSize        =   -1  'True
      Caption         =   "Customer"
      Height          =   195
      Left            =   120
      TabIndex        =   20
      Top             =   1080
      Width           =   660
   End
   Begin VB.Label lblOrderDate 
      AutoSize        =   -1  'True
      Caption         =   "&Order Date"
      Height          =   195
      Left            =   120
      TabIndex        =   19
      Top             =   2520
      Width           =   780
   End
   Begin VB.Label lblEndDate 
      AutoSize        =   -1  'True
      Caption         =   "New E&xpiration Date"
      Height          =   195
      Left            =   4500
      TabIndex        =   17
      Top             =   1035
      Width           =   1455
   End
   Begin VB.Label lblBegDate 
      AutoSize        =   -1  'True
      Caption         =   "New &Effective Date"
      Height          =   195
      Left            =   4500
      TabIndex        =   16
      Top             =   645
      Width           =   1395
   End
   Begin VB.Label lblNewSO 
      AutoSize        =   -1  'True
      Caption         =   "&New SO"
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   720
      Width           =   600
   End
   Begin VB.Label lblReqdate 
      AutoSize        =   -1  'True
      Caption         =   "&Request Date(s)"
      Height          =   195
      Left            =   120
      TabIndex        =   13
      Top             =   2880
      Width           =   1155
   End
   Begin VB.Label lblPromDate 
      AutoSize        =   -1  'True
      Caption         =   "Pro&mise Date(s)"
      Height          =   195
      Left            =   120
      TabIndex        =   14
      Top             =   3240
      Width           =   1110
   End
   Begin VB.Label lblShipDate 
      AutoSize        =   -1  'True
      Caption         =   "S&hip Date(s)"
      Height          =   195
      Left            =   120
      TabIndex        =   15
      Top             =   3600
      Width           =   870
   End
End
Attribute VB_Name = "frmSOCopy"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public moCM             As New clsContextMenu

Private moErrorLog           As New clsErrorLog
Public moOptions            As New clsOptions
Public moClass          As Object
Private muSOCust        As CustDflts                'Customer Defaults
'************************************************************************
'   oClass contains the reference to the parent class object.  The form
'   needs this reference to use the public variables created within the
'   class object.
'************************************************************************
Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    FormHelpPrefix = "SOZ"         '       Put your prefix here
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    WhatHelpPrefix = "SOZ"         '       Put your prefix here
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Private Function sGetNextSONo() As String
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    Dim sNextSONo       As String
    Dim iNumChars       As Integer
    Dim iRetVal         As Integer
    Dim sSQL            As String
    Dim lSOKey          As Long
    
On Error GoTo ExpectedErrorRoutine2
    
    
    If iNumChars = 0 Or iNumChars > 10 Then
        iNumChars = 10
    End If
    
    iRetVal = 0
    
    On Error GoTo ExpectedErrorRoutine
    With moClass.moAppDB
        .SetInParam moClass.sCompanyID
        .SetInParam iNumChars
        .SetInParam moClass.iTranType
        .SetInParam moClass.iUseSameRangeForBlanket
        .SetInParam moClass.iUseSameRangeForQuote
        .SetOutParam sNextSONo
        .SetOutParam iRetVal
        .ExecuteSP ("spsoGetNextSONo")
        sNextSONo = .GetOutParam(6)
        iRetVal = .GetOutParam(7)
        .ReleaseParams
    End With
    On Error GoTo ExpectedErrorRoutine2
    
    If iRetVal = 0 Then
        sNextSONo = ""
        giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
            "spsoGetNextSONo: " & "0"
        Exit Function
    End If
    
    If iRetVal = -1 Then
        sNextSONo = ""
        MsgBox "All Sales Order Numbers Used", , "Sage 500 ERP"
        Exit Function
    End If
    
    sSQL = "INSERT INTO tsoSalesOrdLog " & _
           "(CompanyID,          SOKey,          TranAmtHC, " & _
            "TranDate,           TranNo,         TranStatus, " & _
            "TranType,           TranNoRel)" & _
     "VALUES   (" & gsQuoted(moClass.sCompanyID) & "," & _
                 lSOKey & ",       0.00, GETDATE(), " & _
                 gsQuoted(sNextSONo) & ",       1, " & _
                 moClass.iTranType & ", " & _
                 gsQuoted(sNextSONo) & ")"

    moClass.moAppDB.ExecuteSQL sSQL
    
    sGetNextSONo = sNextSONo
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:
giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
    "spsoGetNextSONo: " & Err.Description
gClearSotaErr

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function

ExpectedErrorRoutine2:
MyErrMsg moClass, Err.Description, Err, sMyName, "sGetNextSONo"
gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetNextSONo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub chkCopyDates_Click()
    
    If chkCopyDates.Value = vbUnchecked Then
        dteOrderDate.Enabled = True
        dtePromDate.Enabled = True
        dteReqDate.Enabled = True
        dteShipDate.Enabled = True
    Else
        dteOrderDate.Enabled = False
        dtePromDate.Enabled = False
        dteReqDate.Enabled = False
        dteShipDate.Enabled = False
        dteOrderDate.Text = ""
        dtePromDate.Text = ""
        dteReqDate.Text = ""
        dteShipDate.Text = ""
        dteOrderDate.Tag = ""
        dtePromDate.Tag = ""
        dteReqDate.Tag = ""
        dteShipDate.Tag = ""
    End If

End Sub

Private Sub dteOrderDate_LostFocus()
    CalcDates
End Sub

Private Sub dtePromDate_LostFocus()
    CalcDate
End Sub

Private Sub dteReqDate_LostFocus()
    CalcDates
End Sub

Public Sub CalcDates(Optional bPrompt As Boolean = True)
Dim bCalcShip   As Boolean

    If dteOrderDate.Tag <> dteOrderDate.Text Then
        If Not dteOrderDate.IsValid Then
            Exit Sub
        End If
        
        If bPrompt Then
            If MsgBox("Do you wish to recalculate the dates?", vbYesNo, "Sage 500 ERP") = vbYes Then
                bCalcShip = True
            Else
                bCalcShip = False
            End If
        Else
            bCalcShip = True
        End If
        
        If bCalcShip Then
            dteReqDate.Text = DateAdd("d", -moClass.iShipDays, dteOrderDate.Text)
            dtePromDate.Text = dteReqDate.Text
            dteShipDate.Text = DateAdd("d", moClass.iShipDays, dtePromDate.Text)
            
            dteReqDate.Tag = dteReqDate.Text
            dtePromDate.Tag = dtePromDate.Text
            dteShipDate.Tag = dteShipDate.Text
            
        End If
              
        dteOrderDate.Tag = dteOrderDate.Text
    End If

    If dteReqDate.Tag <> dteReqDate.Text Then
        
        If Not dteReqDate.IsValid Then
            Exit Sub
        End If
        
        dteReqDate.Tag = dteReqDate.Text
        
      'Promise date
        If Len(Trim(dtePromDate.Text)) = 0 Or Not dtePromDate.IsValid Then
            dtePromDate.Text = dteReqDate.Text
            dtePromDate.Tag = dtePromDate.Text
            
            If Len(Trim(dtePromDate.Text)) > 0 Then
                If Len(Trim(dteShipDate.Text)) = 0 Then
                    bCalcShip = True
                Else
                    If MsgBox("Do you wish to recalculate the ship date?", vbYesNo, "Sage 500 ERP") = vbYes Then
                        bCalcShip = True
                    End If
                End If
            End If
        
        End If
        
      'Ship Date
        If bCalcShip Then
            dteShipDate.Text = DateAdd("d", moClass.iShipDays, dteReqDate.Text)
            dteShipDate.Tag = dteShipDate.Text
        End If
        
    End If

End Sub
Private Sub CalcDate()
Dim bCalcShip   As Boolean

    If dtePromDate.Tag <> dtePromDate.Text Then
        
        If Not dtePromDate.IsValid Then
            Exit Sub
        End If
        
        dtePromDate.Tag = dtePromDate.Text
        
      'Promise date
        If Len(Trim(dtePromDate.Text)) > 0 Then
            If Len(Trim(dteShipDate.Text)) = 0 Then
                bCalcShip = True
            Else
                If MsgBox("Do you wish to recalculate the ship date?", vbYesNo, "Sage 500 ERP") = vbYes Then
                    bCalcShip = True
                End If
            End If
        End If
        
      'Ship Date
        If bCalcShip Then
            dteShipDate.Text = DateAdd("d", moClass.iShipDays, dtePromDate.Text)
            dteShipDate.Tag = dteShipDate.Text
        End If
        
    End If

End Sub


Private Sub Form_Load()
    
    If moClass.iTranType = kTranTypeSOQO Then
        Me.Caption = "Copy Quotes"
        lblNewSO.Caption = "&New Quote"
    End If
    
    If moClass.iTranType = kTranTypeSOBO Then
        Me.Caption = "Copy Blanket Sales Orders"
        lblNewSO.Caption = "&New Blanket SO"
    End If
    
    If moClass.iTranType = kTranTypeSOBO Then
        Me.Width = dteEndDate.Left + dteEndDate.Width + 150
    End If

    SetupLookups        'Setup Customer Lookup
    
   'parameters are form, moduleID, title to appear on RPT
    moErrorLog.lInitErrorLog Me, "SO", Me.Caption
    
    tbrMain.AddButton kTbNextNumber, tbrMain.GetIndex(kTbHelp)
    tbrMain.AddSeparator tbrMain.GetIndex(kTbHelp)

    moClass.bViewRpt = False
    moClass.bGened = False

    Set moCM.Form = frmSOCopy
     moCM.Init

End Sub
Private Function bIsFormValid() As Boolean

    If Len(Trim(txtNewSO.Text)) = 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, gsStripChar(lblNewSO, "&")
        On Error Resume Next
        txtNewSO.SetFocus
        Exit Function
    End If
    
    'RKL DEJ 50-20-2016 (START)
    If Len(Trim(lkuCustID.Text)) = 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, gsStripChar(lblCustID.Caption, "&")
        On Error Resume Next
        lkuCustID.SetFocus
        Exit Function
    End If
    'RKL DEJ 50-20-2016 (STOP)
    
    If chkCopyDates = vbUnchecked Then
    
        CalcDates
        CalcDate
        
         If Len(Trim(dteOrderDate.Text)) = 0 Or Not dteOrderDate.IsValid Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, gsStripChar(lblOrderDate, "&")
            On Error Resume Next
            dteOrderDate.SetFocus
            Exit Function
        End If
                
        If Len(Trim(dteReqDate.Text)) = 0 Or Not dteReqDate.IsValid Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, gsStripChar(lblReqdate, "&")
            On Error Resume Next
            dteReqDate.SetFocus
            Exit Function
        End If
    
        If Len(Trim(dtePromDate.Text)) = 0 Or Not dtePromDate.IsValid Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, gsStripChar(lblPromDate, "&")
            On Error Resume Next
            dtePromDate.SetFocus
            Exit Function
        End If
    
        If Len(Trim(dteShipDate.Text)) = 0 Or Not dteShipDate.IsValid Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, gsStripChar(lblShipDate, "&")
            On Error Resume Next
            dteShipDate.SetFocus
            Exit Function
        End If

        If Len(Trim(lkuCustID.Text)) = 0 Or Not lkuCustID.IsValid Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, gsStripChar(lblCustID, "&")
            On Error Resume Next
            lkuCustID.SetFocus
            Exit Function
        End If
    End If
    
    If moClass.bContract Then
        If Len(Trim(dteBegDate.Text)) = 0 Or Not dteBegDate.IsValid Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, gsStripChar(lblBegDate, "&")
            On Error Resume Next
            dteBegDate.SetFocus
            Exit Function
        End If
        If Len(Trim(dteEndDate.Text)) = 0 Or Not dteEndDate.IsValid Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, gsStripChar(lblEndDate, "&")
            On Error Resume Next
            dteEndDate.SetFocus
            Exit Function
        End If
    End If
    
    bIsFormValid = True
    
End Function

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Me.Hide
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
Dim iMPtr As Integer

    Select Case Button
        Case kTbProceed
            SetHourglass True
            CopySO
            SetHourglass False
        Case kTbNextNumber
            txtNewSO.Text = sGetNextSONo
        Case kTbClose
            Unload Me
        Case kTbHelp
            gDisplayFormLevelHelp Me
       End Select
End Sub
Private Sub Form_Unload(Cancel As Integer)
    On Error Resume Next
    moClass.moAppDB.ExecuteSQL "DELETE FROM #tsoLines "
    
    If Not moClass.bViewRpt Then
        moClass.moAppDB.ExecuteSQL "DELETE FROM tciErrorLog WHERE SessionID = " & moClass.Spid
    End If
    Set moCM = Nothing
    
    'moErrorLog.moReport.bShutdownEngine
    moErrorLog.moReport.bShutdownErrorLog
    
    Set moOptions = Nothing
    Set moErrorLog = Nothing

End Sub
Private Sub CopySO()

    Dim sSQL            As String
    Dim lRow            As Long
    Dim iVal            As Integer
    Dim dQty            As Double
    Dim lSOLineKey      As Double
    Dim iQtyToGen       As Integer
    Dim iRetVal         As Integer
    Dim lErr            As Long
    Dim sErrDesc        As String
    Dim lSOKey          As Long
    
    moClass.bGened = False
    
    If bIsFormValid Then
        sSQL = "CREATE TABLE #tsoLines " & _
               "(SOLineKey int NOT NULL, " & _
               " ItemKey Int NULL, " & _
               " Qty dec(16,8) NOT NULL) "

        On Error Resume Next
        moClass.moAppDB.ExecuteSQL sSQL
        
        On Error GoTo ExpectedErrorRoutine
        moClass.moAppDB.ExecuteSQL "DELETE FROM #tsoLines "
        moClass.moAppDB.ExecuteSQL "DELETE FROM tciErrorLog WHERE SessionID = " & moClass.Spid
        
        sSQL = "INSERT INTO #tsoLines SELECT a.SOLineKey, NULL, "
        If chkCopyQuantities.Value = vbUnchecked Then
            sSQL = sSQL & "0 "
        Else
            sSQL = sSQL & "ISNULL(QtyOrd, 0) "
        End If
        sSQL = sSQL & " FROM tsoSOLine a WITH (NOLOCK) " & _
                    " LEFT OUTER JOIN tsoSOLineDist b WITH (NOLOCK) ON a.SOLineKey = b.SOLineKey" & _
                    " WHERE SOKey =  " & moClass.lSOKey
        moClass.moAppDB.ExecuteSQL sSQL
        
        On Error GoTo ExpectedErrorRoutine2
        With moClass.moAppDB
            .SetInParam moClass.sCompanyID
            .SetInParam moClass.lSOKey
            .SetInParam moClass.iTranType
            .SetInParam IIf(chkCopyDates.Value = vbChecked, gsFormatDateToDB(moClass.sSODate), gsFormatDateToDB(dteOrderDate.Text))
            If chkCopyDates.Value = vbChecked Then
                .SetInParamNull SQL_CHAR
                .SetInParamNull SQL_CHAR
                .SetInParamNull SQL_CHAR
            Else
                .SetInParam dteReqDate.SQLDate
                .SetInParam dtePromDate.SQLDate
                .SetInParam dteShipDate.SQLDate
            End If
            If Len(Trim(txtNewSO.Text)) = 0 Then
                .SetInParamNull SQL_CHAR
            Else
                .SetInParam CStr(txtNewSO.Text)
            End If
            .SetInParam moClass.sUserID
            If moClass.bUseBlnktRelNos Then
                .SetInParam 1
            Else
                .SetInParam 0
            End If
            .SetInParam moClass.lLanguage
            .SetInParam chkReprice.Value
            .SetOutParam iRetVal
            .SetOutParam moClass.Spid
            .SetOutParam lSOKey
            .SetInParam glGetValidLong(lkuCustID.KeyValue)      'New parameter to pass the CustomerKey
            .SetInParam gsGetValidStr(txtPurchOrder.Text)       'New parameter to pass the CustomerPONO
            .ExecuteSP "spsoCreateASalesOrder"
            iRetVal = giGetValidInt(.GetOutParam(13))
            moClass.Spid = glGetValidLong(.GetOutParam(14))
            lSOKey = glGetValidLong(.GetOutParam(15))
            .ReleaseParams
        End With
    
        On Error GoTo ExpectedErrorRoutine
        Select Case iRetVal
            Case 0
                MsgBox "unexpected errors occurred", , "Sage 500 ERP"
                Exit Sub
            Case 1
                Select Case moClass.iTranType
                    Case kTranTypeSOSO
                        MsgBox "Sales order created successfully", , "Sage 500 ERP"
                    Case kTranTypeSOBO
                        MsgBox "Blanket sales order created successfully", , "Sage 500 ERP"
                    Case kTranTypeSOQO
                        MsgBox "Quote created successfully", , "Sage 500 ERP"
                End Select
                moClass.bGened = True
            Case 2
                If MsgBox("Sales order created - warnings were raised.  Do you wish to view the Error Log?", vbYesNo, "Sage 500 ERP") = vbYes Then
                    moClass.bViewRpt = True
                Else
                    moClass.bViewRpt = False
                End If
                moClass.bGened = True
            Case 3, 4
                If MsgBox("Errors were raised.  Do you wish to view the Error Log?", vbYesNo, "Sage 500 ERP") = vbYes Then
                    moClass.bViewRpt = True
                Else
                    moClass.bViewRpt = False
                End If
        End Select
            
        
        If moClass.bGened Then
            
            sSQL = "INSERT INTO tsoBlnktSalesOrder (SOKey, Contract, ContractRef, CurrExchRateMeth, " & _
               "MaxAmountToGen, MaxSOToGen, NextRelNo, NoSOReleased, ReleasedToDate, StartDate, StopDate) " & _
            " SELECT " & lSOKey & ", Contract, ContractRef, CurrExchRateMeth, " & _
                    "MaxAmountToGen, MaxSOToGen, 1, 0, 0, "
                    
            If Len(Trim(dteBegDate.SQLDate)) = 0 Then
                sSQL = sSQL & "NULL, NULL "
            Else
                 sSQL = sSQL & gsQuoted(dteBegDate.SQLDate) & ", " & gsQuoted(dteEndDate.SQLDate)
            End If
                    
            sSQL = sSQL & " FROM tsoBlnktSalesOrder WHERE SOKey = " & moClass.lSOKey
                    
            moClass.moAppDB.ExecuteSQL sSQL
            moClass.sSONum = txtNewSO.Text
            
        End If
        
        Me.Hide
'        If moClass.bViewRpt Then
            'print the report and hide the copy form from SO main form so it shows up in front
            
        '        If moClass.bViewRpt Then
'            moErrorLog.ProcessSpidErrorLog frmSOCopy, 0, mlSpid
'            moErrorLog.PrintErrorLog kTbPreview, frmSOCopy
'            moClass.moAppDB.ExecuteSQL "DELETE FROM tciErrorLog WHERE SessionID = " & mlSpid
'        End If
               
    
    End If
  
Exit Sub

ExpectedErrorRoutine2:
lErr = Err
sErrDesc = Err.Description
SetHourglass False
giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
     "spsoCreateASalesOrder: " & sErrDesc
gClearSotaErr
Exit Sub

'Not a Stored Procedure error
ExpectedErrorRoutine:
lErr = Err
sErrDesc = Err.Description
SetHourglass False
MyErrMsg moClass, sErrDesc, lErr, sMyName, "CreateFromBlanket"
gClearSotaErr
Exit Sub


End Sub
Private Property Get sMyName() As String
    sMyName = "frmSOCopy"
End Property








Public Property Get MyApp() As Object
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property

Public Sub lkuCustID_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************
' Desc:  Validate customer id if lost focus
'************************************************************************
        
    If Len(Trim(lkuCustID.Text)) = 0 Then
        If Not Me.ActiveControl Is lkuCustID Then
                lkuCustID.SetFocus
        End If
    Else
        If Not bIsValidCustID Then
            lkuCustID.Text = ""
            lkuCustID.SetFocus
            Exit Sub
        End If
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuCustID_LostFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub SetupLookups()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

SetupLookup lkuCustID, "Customer", "CompanyID = " & gsQuoted(moClass.sCompanyID) & " AND Status IN (1, 3)"

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupLookups", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub SetupLookup(lu As Control, sLookupDef As String, sWhere As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set lu.Framework = moClass.moFramework
    Set lu.SysDB = moClass.moAppDB
    Set lu.AppDatabase = moClass.moAppDB

    lu.LookupID = sLookupDef

    If Len(Trim(sWhere)) > 0 Then
        lu.RestrictClause = sWhere
    End If
       

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupLookup", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Function bIsValidCustID() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++

    Dim sID                 As String
    Dim sUser               As String
    Dim vPrompt             As Variant
    Dim lMsg                As Long
    
    bIsValidCustID = False
    
    If Not bGetCustDflts Then
        Exit Function
    End If
       
    If ((muSOCust.iCreditHold = 1) Or (muSOCust.iNationalAcctHold = 1)) Then
        sID = "SOHOLDCUST"
        sUser = moClass.moSysSession.UserId
        vPrompt = False
        If moClass.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt) = 0 Then
            If Not bOverrideSecEvent(sID, moClass) Then
                Exit Function
            End If
        Else
            If muSOCust.iCreditHold = 1 Then
                'This customer is on hold.  Do you want to continue?
                lMsg = kSOmsgCustOnHold
            Else
                'The national account for this customer is on hold.  Do you want to continue?
                lMsg = kSOmsgNatlAcctOnHold
            End If
            If giSotaMsgBox(Me, moClass.moSysSession, lMsg) <> kretYes Then
                Exit Function
            End If
        End If
    End If
    
    bIsValidCustID = True

    Exit Function
                 
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsValidCustID", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bGetCustDflts() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lShipLabelKey As Long
Dim sShipLabelID As String
Dim iRetVal As Integer
Dim lAcctKey As Long
     
    bGetCustDflts = False
    
    On Error GoTo ExpectedErrorRoutine
    'Run the stored procedure to get defaults
    With moClass.moAppDB
        .SetInParam lkuCustID.Text '1
        .SetInParam moClass.sCompanyID  '2
        .SetOutParam muSOCust.iAllowInvtSubst '3
        .SetOutParam muSOCust.iBillingType '4
        .SetOutParam muSOCust.dCreditLimit '5
        .SetOutParam muSOCust.iCredAgeCat '6
        .SetOutParam muSOCust.sCustID '7
        .SetOutParam muSOCust.sCustName '8
        .SetOutParam muSOCust.lBillToCustAddrKey '9
        .SetOutParam muSOCust.sBillToCustAddrID '10
        .SetOutParam muSOCust.lDfltItemKey '11
        .SetOutParam muSOCust.sDfltItemID '12
        .SetOutParam muSOCust.lDfltSalesAcctKey '13
        .SetOutParam muSOCust.lShipToCustAddrKey '14
        .SetOutParam muSOCust.sShipToCustAddrID '15
        .SetOutParam muSOCust.lShipToCustAddrUpdCtr '16
        .SetOutParam muSOCust.iCreditHold '17
        .SetOutParam muSOCust.lPrimaryAddrKey '18
        .SetOutParam muSOCust.sPrimaryAddrLine1 '19
        .SetOutParam muSOCust.sPrimaryCity '20
        .SetOutParam muSOCust.sPrimaryState '21
        .SetOutParam muSOCust.sPrimaryPostalCode '22
        .SetOutParam muSOCust.sPrimaryCountry '23
        .SetOutParam muSOCust.iReqPO '24
        .SetOutParam muSOCust.dRetntPct '25
        .SetOutParam muSOCust.lDfltReturnAcctKey '26
        .SetOutParam muSOCust.iStatus '27
        .SetOutParam muSOCust.dTradeDiscPct / 100 'scopus #19341, convert the data back to database format
        .SetOutParam muSOCust.lCustClassKey '29
        .SetOutParam muSOCust.sCustClassID '30
        .SetOutParam muSOCust.sClassOvrdSegVal '31
        .SetOutParam muSOCust.iPriority '32
        .SetOutParam muSOCust.lPmtTermsKey '33
        .SetOutParam muSOCust.sPmtTermsID '34
        .SetOutParam muSOCust.lSOAckFormKey '35
        .SetOutParam muSOCust.sSOAckFormID '36
        .SetOutParam muSOCust.lCurrExchSchdKey '37
        .SetOutParam muSOCust.sCurrExchSchdID '38
        .SetOutParam muSOCust.sCurrID '39
        .SetOutParam muSOCust.iPrintOrdAck '40
        .SetOutParam muSOCust.lDfltCntctKey '41
        .SetOutParam muSOCust.sContactName '42
        .SetOutParam muSOCust.lPackListKey '43
        .SetOutParam muSOCust.sPackListID '44
        .SetOutParam muSOCust.lShipMethKey '45
        .SetOutParam muSOCust.sShipMethID '46
        .SetOutParam muSOCust.lShipZoneKey '47
        .SetOutParam muSOCust.sShipZoneID '48
        .SetOutParam muSOCust.lSperKey '49
        .SetOutParam muSOCust.sSperID '50
        .SetOutParam muSOCust.sSperName '51
        .SetOutParam muSOCust.lSTaxSchdkey '52
        .SetOutParam muSOCust.sTaxSchedID '53
        .SetOutParam muSOCust.lFOBKey '54
        .SetOutParam muSOCust.sFOBID '55
        .SetOutParam muSOCust.iTaxPoint '56
        .SetOutParam muSOCust.lCommPlanKey '57
        .SetOutParam muSOCust.sCommPlanID '58
        .SetOutParam muSOCust.lWhseKey '59
        .SetOutParam muSOCust.sWhseID '60
        .SetOutParam muSOCust.iShipDays '61
        .SetOutParam muSOCust.iShipComplete '62
        .SetOutParam lShipLabelKey '63
        .SetOutParam sShipLabelID '64
        .SetOutParam muSOCust.lSource '65
        .SetOutParam muSOCust.sSource '66
        .SetOutParam muSOCust.iBillToNationalAcctParent '67
        .SetOutParam muSOCust.iPmtByNationalAcctParent '68
        .SetOutParam muSOCust.lNationalAcctParentCustKey '69
        .SetOutParam muSOCust.iNationalAcctHold '70
        .SetOutParam muSOCust.iReqAck '71
        .SetOutParam muSOCust.iWhseAllowImmedPick '72
        .SetOutParam muSOCust.iFreightMethod '73
        .SetOutParam iRetVal '74
        .SetOutParam muSOCust.dPrimaryLatitude  '75
        .SetOutParam muSOCust.dPrimaryLongitude  '76
        .ExecuteSP "spsoGetCustWID"
        muSOCust.iAllowInvtSubst = giGetValidInt(.GetOutParam(3))
        muSOCust.iBillingType = giGetValidInt(.GetOutParam(4))
        muSOCust.dCreditLimit = gdGetValidDbl(.GetOutParam(5))
        muSOCust.iCredAgeCat = giGetValidInt(.GetOutParam(6))
        muSOCust.sCustID = gsGetValidStr(.GetOutParam(7))
        muSOCust.sCustName = gsGetValidStr(.GetOutParam(8))
        muSOCust.lBillToCustAddrKey = glGetValidLong(.GetOutParam(9))
        muSOCust.sBillToCustAddrID = gsGetValidStr(.GetOutParam(10))
        muSOCust.lDfltItemKey = glGetValidLong(.GetOutParam(11))
        muSOCust.sDfltItemID = gsGetValidStr(.GetOutParam(12))
        muSOCust.lDfltSalesAcctKey = glGetValidLong(.GetOutParam(13))
        muSOCust.lShipToCustAddrKey = glGetValidLong(.GetOutParam(14))
        muSOCust.sShipToCustAddrID = gsGetValidStr(.GetOutParam(15))
        muSOCust.lShipToCustAddrUpdCtr = glGetValidLong(.GetOutParam(16))
        muSOCust.iCreditHold = giGetValidInt(.GetOutParam(17))
        muSOCust.lPrimaryAddrKey = glGetValidLong(.GetOutParam(18))
        muSOCust.sPrimaryAddrLine1 = gsGetValidStr(.GetOutParam(19))
        muSOCust.sPrimaryCity = gsGetValidStr(.GetOutParam(20))
        muSOCust.sPrimaryState = gsGetValidStr(.GetOutParam(21))
        muSOCust.sPrimaryPostalCode = gsGetValidStr(.GetOutParam(22))
        muSOCust.sPrimaryCountry = gsGetValidStr(.GetOutParam(23))
        muSOCust.iReqPO = giGetValidInt(.GetOutParam(24))
        muSOCust.dRetntPct = gdGetValidDbl(.GetOutParam(25))
        muSOCust.lDfltReturnAcctKey = glGetValidLong(.GetOutParam(26))
        muSOCust.iStatus = giGetValidInt(.GetOutParam(27))
        muSOCust.dTradeDiscPct = 100 * gdGetValidDbl(.GetOutParam(28))
        muSOCust.lCustClassKey = glGetValidLong(.GetOutParam(29))
        muSOCust.sCustClassID = gsGetValidStr(.GetOutParam(30))
        muSOCust.sClassOvrdSegVal = gsGetValidStr(.GetOutParam(31))
        muSOCust.iPriority = giGetValidInt(.GetOutParam(32))
        muSOCust.lPmtTermsKey = glGetValidLong(.GetOutParam(33))
        muSOCust.sPmtTermsID = gsGetValidStr(.GetOutParam(34))
        muSOCust.lSOAckFormKey = glGetValidLong(.GetOutParam(35))
        muSOCust.sSOAckFormID = gsGetValidStr(.GetOutParam(36))
        muSOCust.lCurrExchSchdKey = glGetValidLong(.GetOutParam(37))
        muSOCust.sCurrExchSchdID = gsGetValidStr(.GetOutParam(38))
        muSOCust.sCurrID = gsGetValidStr(.GetOutParam(39))
        muSOCust.iPrintOrdAck = giGetValidInt(.GetOutParam(40))
        muSOCust.lDfltCntctKey = glGetValidLong(.GetOutParam(41))
        muSOCust.sContactName = gsGetValidStr(.GetOutParam(42))
        muSOCust.lPackListKey = glGetValidLong(.GetOutParam(43))
        muSOCust.sPackListID = gsGetValidStr(.GetOutParam(44))
        muSOCust.lShipMethKey = glGetValidLong(.GetOutParam(45))
        muSOCust.sShipMethID = gsGetValidStr(.GetOutParam(46))
        muSOCust.lShipZoneKey = glGetValidLong(.GetOutParam(47))
        muSOCust.sShipZoneID = gsGetValidStr(.GetOutParam(48))
        muSOCust.lSperKey = glGetValidLong(.GetOutParam(49))
        muSOCust.sSperID = gsGetValidStr(.GetOutParam(50))
        muSOCust.sSperName = gsGetValidStr(.GetOutParam(51))
        muSOCust.lSTaxSchdkey = glGetValidLong(.GetOutParam(52))
        muSOCust.sTaxSchedID = gsGetValidStr(.GetOutParam(53))
        muSOCust.lFOBKey = glGetValidLong(.GetOutParam(54))
        muSOCust.sFOBID = gsGetValidStr(.GetOutParam(55))
        muSOCust.iTaxPoint = giGetValidInt(.GetOutParam(56))
        muSOCust.lCommPlanKey = glGetValidLong(.GetOutParam(57))
        muSOCust.sCommPlanID = gsGetValidStr(.GetOutParam(58))
        muSOCust.lWhseKey = glGetValidLong(.GetOutParam(59))
        muSOCust.sWhseID = gsGetValidStr(.GetOutParam(60))
        muSOCust.iShipDays = giGetValidInt(.GetOutParam(61))
        muSOCust.iShipComplete = giGetValidInt(.GetOutParam(62))
        '63 - ship label - not used
        '64 - ship label ID - not used
        muSOCust.lSource = glGetValidLong(.GetOutParam(65))
        muSOCust.sSource = gsGetValidStr(.GetOutParam(66))
        muSOCust.iBillToNationalAcctParent = giGetValidInt(.GetOutParam(67))
        muSOCust.iPmtByNationalAcctParent = giGetValidInt(.GetOutParam(68))
        muSOCust.lNationalAcctParentCustKey = glGetValidLong(.GetOutParam(69))
        muSOCust.iNationalAcctHold = giGetValidInt(.GetOutParam(70))
        muSOCust.iReqAck = giGetValidInt(.GetOutParam(71))
        muSOCust.iWhseAllowImmedPick = giGetValidInt(.GetOutParam(72))
        muSOCust.iFreightMethod = giGetValidInt(.GetOutParam(73))
        iRetVal = giGetValidInt(.GetOutParam(74))
        .ReleaseParams
    End With
    
    If iRetVal <> 1 Then
        Exit Function
    End If
    
    bGetCustDflts = True
    
    Exit Function

ExpectedErrorRoutine:
giSotaMsgBox Me, moClass.moSysSession, kmsgSPBadReturn, "spsoGetCustDflts", Err.Description
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bGetCustDflts", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub txtPurchOrder_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************
' Desc:  Validate Customer Purchase Order if lost focus
'************************************************************************
    If Len(Trim(lkuCustID.Text)) = 0 Then
        If Not bIsValidCustPO Then
                txtPurchOrder.Text = ""
                txtPurchOrder.SetFocus
                Exit Sub
        End If
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtPurchOrder_LostFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Sub

Private Function bIsValidCustPO() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim lSOKey As Long
    Dim lCustKey As Long
    Dim lDupCount As Long
    Dim sSOTranNo As String
    Dim sPONum As String
    
    bIsValidCustPO = True
    
    sPONum = txtPurchOrder.Text
    lSOKey = moClass.lSOKey
    lCustKey = glGetValidLong(lkuCustID.KeyValue)
    
    If Not IsNull(sPONum) And Trim(sPONum) <> "" Then
        With moClass.moAppDB
            .SetInParam moClass.sCompanyID
            .SetInParam lSOKey
            .SetInParam sPONum
            .SetInParam lCustKey
            .SetOutParam lDupCount
            .SetOutParam sSOTranNo
            .ExecuteSP "spsoCheckDuplicatePO"
            lDupCount = .GetOutParam(5)
            sSOTranNo = .GetOutParam(6)
            .ReleaseParams
        End With
        
        If lDupCount = 1 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgSODuplPOonSO, sSOTranNo
        Else
            If lDupCount > 1 Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgSODuplPOonMultSO, lDupCount, sSOTranNo
            End If
        End If
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsValidCustPO", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
