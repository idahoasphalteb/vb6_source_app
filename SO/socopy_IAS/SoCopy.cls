VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsSOCopy"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'************************************************************************************
'     Name: clsAddr
'     Desc: Maintain Addresses Class
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: 12/11/95 AEW
'     Mods:
'************************************************************************************
Option Explicit
  'Sage MAS 500 Objects
    Public moSysSession     As Object   'Sage MAS 500 Session
    Public moFramework      As Object   'Sage MAS 500 Framework
    
  'databases
    Public moAppDB          As Object   'Application database
    
  'Form
    Public mfrmMain         As Object   'Form
    
  'Company ID / Enter as tab
    Public mbEnterAsTab     As Boolean  'Enter As Tab
    
    'Moved here from Init subroutine to correct crystal problems - 06/28 BDS
    Public moDasSession As Object
    
    Private miTranType                  As Integer
    Private miUseSameRangeForBlanket    As Integer
    Private miUseSameRangeForQuote      As Integer
    Private msSODate                    As String
    Private mbUseBlnktRelNos            As Boolean
    Private msCompanyID                 As String
    Private mlSOKey                     As Long
    Private msUserID                    As String
    Private mlLanguage                  As Long
    Private miShipDays                  As Integer
    Private mbContract                  As Boolean
    Private msSONum                     As String
    Private mbViewRpt                   As Boolean
    Private mbGened                     As Boolean
    Private mlSpid                      As Long

    
'**********************************************************************************
' Currency Symbol
'**********************************************************************************
Const VBRIG_MODULE_ID_STRING = "SOCopy.CLS"
Public Sub CopySO(sCompID As String, lSOKey As Long, sUserID As String, _
                  lLanguage As Long, iShipDays As Integer, iTranType As Integer, _
                  iUseSameRangeForBlanket As Integer, iUseSameRangeForQuote As Integer, _
                  bContract As Boolean, lSPid As Long, sSONum As String, bGened As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
'*************************************************************************************
' Desc:    Show the address form and allow the user to update addresses
' Parms:
'*************************************************************************************
    
    '***********************************************************
    'RKL DEJ 2016 (START)
    '***********************************************************
    Dim lcCustKey As Long
    Dim lcCustID As String
    '***********************************************************
    'RKL DEJ 2016 (STOP)
    '***********************************************************
    
    msCompanyID = sCompID
    mlSOKey = lSOKey
    msUserID = sUserID
    mlLanguage = lLanguage
    miShipDays = iShipDays
    miTranType = iTranType
    miUseSameRangeForBlanket = iUseSameRangeForBlanket
    miUseSameRangeForQuote = iUseSameRangeForQuote
    mbContract = bContract
    mlSpid = lSPid
    
    msSODate = gsGetValidStr(moAppDB.Lookup("TranDate", "tsoSalesOrder", _
                    "SOKey = " & mlSOKey))
    
    mfrmMain.dteBegDate.DisplayOnly = False
    mfrmMain.dteEndDate.DisplayOnly = False
    mfrmMain.dteOrderDate.DisplayOnly = False
    mfrmMain.dtePromDate.DisplayOnly = False
    mfrmMain.dteReqDate.DisplayOnly = False
    mfrmMain.dteShipDate.DisplayOnly = False

    If Not bContract Then
        mfrmMain.dteBegDate.DisplayOnly = True
        mfrmMain.dteEndDate.DisplayOnly = True
        mfrmMain.dteBegDate.Enabled = False
        mfrmMain.dteEndDate.Enabled = False

    Else
        mfrmMain.dteBegDate.DisplayOnly = False
        mfrmMain.dteEndDate.DisplayOnly = False
    End If
    
    mfrmMain.txtNewSO = ""
    
    '***********************************************************
    'RKL DEJ 2016 (START)
    'If the customer (on the BSO, QO, SO) is contractors Bidding (AAAA) then force user to select a new Customer
    'This was a request from Hal May of 2016
    '***********************************************************
    lcCustKey = glGetValidLong(moAppDB.Lookup("CustKey", "tsoSalesOrder (NOLOCK)", "SOKey = " & mlSOKey))
    lcCustID = UCase(gsGetValidStr(moAppDB.Lookup("CustID", "tarCustomer (NOLOCK)", "CustKey = " & lcCustKey)))
    
    If lcCustID <> "AAAA" Then
        mfrmMain.lkuCustID.SetTextAndKeyValue "", glGetValidLong(moAppDB.Lookup("CustKey", "tsoSalesOrder (NOLOCK)", "SOKey = " & mlSOKey)) 'To default the Customer from Sales Order.
    End If
    '***********************************************************
    'RKL DEJ 2016 (STOP)
    '***********************************************************
    
    mfrmMain.chkCopyDates.Value = vbUnchecked
    mfrmMain.chkCopyQuantities.Value = vbUnchecked
    mfrmMain.chkReprice.Value = vbUnchecked
    mfrmMain.txtPurchOrder = ""
    
    mfrmMain.dteBegDate.Value = ""
    mfrmMain.dteEndDate.Value = ""
    
    mfrmMain.dteOrderDate.Text = moSysSession.BusinessDate
    mfrmMain.dteOrderDate.Tag = ""
    
    mfrmMain.CalcDates False
    
'    mfrmMain.dteOrderDate.Text = ""
'    mfrmMain.dtePromDate.Text = ""
'    mfrmMain.dteReqDate.Text = ""
'    mfrmMain.dteShipDate.Text = ""
'
'    mfrmMain.dteOrderDate.Tag = ""
'    mfrmMain.dtePromDate.Tag = ""
'    mfrmMain.dteReqDate.Tag = ""
'    mfrmMain.dteShipDate.Tag = ""
    mfrmMain.txtNewSO.SetFocus
    mfrmMain.Show vbModal
    
    bGened = mbGened
    sSONum = msSONum
  
  'Setup Values to return

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "CalcAdjBal", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub
Public Sub UnloadMe()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Unload Objects associated with this class
'***********************************************************************
    
    Unload mfrmMain             'Unload The form
    Set mfrmMain = Nothing      'Set form to nothing
        
    Set moSysSession = Nothing  'Set session to nothing
    Set moAppDB = Nothing       'Set app database to nothing
    Set moAppDB = Nothing       'set system database to nothing
    Set moDasSession = Nothing
    
'    With moClass
'    If Not .moDasSession Is Nothing Then
'          .moDasSession.Close
'          Set .moDasSession = Nothing
'    End If
'    End With
      
    Set moFramework = Nothing   'set framework to nothing
    
    'Set to nothing, reqd. for context menu - BDS
    Set goClass = Nothing
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "UnloadMe", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub
Public Sub Init(oSysSession As Object, oAppDB As Object, sCompany As String, _
                bEnterAsTab As Boolean, oSysDB As Object, oFramework As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Desc:     load Objects/variables associated with this class
' Parms:    oSysSession     - Sage MAS 500 session object
'           oAppDB          - Application Database
'           sCompany        - Company ID
'           benterastab     - Enter as Tab
'           oSysDB          - system database
'           oSotaObjects    - Sage MAS 500 Objects
'           oFramework      - Sage MAS 500 Framework Object
'***********************************************************************
'    Dim moDasSession As Object
    Set moSysSession = oSysSession  'Sage MAS 500 session object
    Set moFramework = oFramework    'Sage MAS 500 Framework Object
    
 '   Set moAppDB = oAppDB            'Application Database
 '   Set moAppDB = oSysDB            'system database
      Set moDasSession = goCreateDasSession
      
      ' Open the Application database
      Set moAppDB = goOpenDatabase(moDasSession, moSysSession.AppODBCConnect, False, False, Me, _
                                   App.ProductName, App.Title)
      moAppDB.SetSysDBConnect moSysSession.SysODBCConnect
      DASSetAppRole moAppDB, moSysSession
    
    Set mfrmMain = frmSOCopy        'Credit Limit Form
    Set mfrmMain.oClass = Me        'Credit Limit form class
    
    msCompanyID = sCompany          'Company ID
    mbEnterAsTab = bEnterAsTab      'Enter as tab
    'Set moDasSession = Nothing
    
    'Required for context menu - BDS
    Set goClass = Me
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "Init", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub
Public Sub HideMe()

    frmSOCopy.Hide

End Sub

Public Property Get lSOKey() As Long
    lSOKey = mlSOKey
End Property
Public Property Get lLanguage() As Long
    lLanguage = mlLanguage
End Property
Public Property Get bContract() As Boolean
    bContract = mbContract
End Property
Public Property Let bViewRpt(bNewValue As Boolean)
    mbViewRpt = bNewValue
End Property
Public Property Get bViewRpt() As Boolean
    bViewRpt = mbViewRpt
End Property
Public Property Get bGened() As Boolean
    bGened = mbGened
End Property
Public Property Let bGened(bNewValue As Boolean)
    mbGened = bNewValue
End Property
Public Property Get bUseBlnktRelNos() As Boolean
    bUseBlnktRelNos = mbUseBlnktRelNos
End Property
Public Property Get sUserID() As String
    sUserID = msUserID
End Property
Public Property Let sSONum(sNewValue As String)
    msSONum = sNewValue
End Property
Public Property Get sSODate() As String
    sSODate = msSODate
End Property

Public Property Let Spid(lNewValue As Long)
    mlSpid = lNewValue
End Property

Public Property Get Spid() As Long
    Spid = mlSpid
End Property

Public Property Get iTranType() As Integer
    iTranType = miTranType
End Property
Public Property Get iUseSameRangeForBlanket() As Integer
    iUseSameRangeForBlanket = miUseSameRangeForBlanket
End Property
Public Property Get iUseSameRangeForQuote() As Integer
    iUseSameRangeForQuote = miUseSameRangeForQuote
End Property
Public Property Get sCompanyID() As String
    sCompanyID = msCompanyID
End Property
Public Property Get iShipDays() As Integer
    iShipDays = miShipDays
End Property
Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "clsSOCopy"
End Function
Public Function GetUIHandle(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mfrmMain Is Nothing Then
        GetUIHandle = mfrmMain.hwnd
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "GetUIHandle", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

