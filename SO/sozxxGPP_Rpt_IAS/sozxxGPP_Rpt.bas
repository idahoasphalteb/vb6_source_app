Attribute VB_Name = "basSOGPPReport"
Option Explicit


Const VBRIG_MODULE_ID_STRING = "sozxxGPP_Rpt.BAS"

Public gbSkipPrintDialog As Boolean     '   Show print dialog




Public Sub Main()
    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("sozxxGPP_Rpt.clsSOGPPReport")
        StartAppInStandaloneMode oApp, Command$()    ' ** The third parameter MUST be the
                                                     '    TaskID for this project in order for the
                                                     '    Standalone method to be activated for ActiveX Exe's
    End If
End Sub

Function PrepSelect(SessionID As String) As String
    Dim sSelect As String
    
    sSelect = _
    "Select " & SessionID & "'SessionID', " & _
    "vsoGPP_SGS.sokey, vsoGPP_SGS.solinekey, vsoGPP_SGS.CompanyID, vsoGPP_SGS.CompanyName, vsoGPP_SGS.[Origin-WhseKey], vsoGPP_SGS.Origin, " & _
    "vsoGPP_SGS.ContractNo, vsoGPP_SGS.Product, vsoGPP_SGS.CustName, vsoGPP_SGS.ProjectNumber, vsoGPP_SGS.ProjectDesc, vsoGPP_SGS.ContractTerm, " & _
    "vsoGPP_SGS.ContractYear, vsoGPP_SGS.BidQty, vsoGPP_SGS.PlanQty, vsoGPP_SGS.SellPerTon, vsoGPP_SGS.CostPerTon, vsoGPP_SGS.ProfitPerTon, " & _
    "vsoGPP_SGS.TotalSale, vsoGPP_SGS.TotalCost, vsoGPP_SGS.TotalProfit, vsoGPP_SGS.ProfitToDate, vsoGPP_SGS.TonsSoldToDate, " & _
    "vsoGPP_SGS.TonsRemaining , vsoGPP_SGS.ProjectComplete, vsoGPP_SGS.ContractType, vsoGPP_SGS.ContractStatusNbr, vsoGPP_SGS.ContractStatus, vsoGPP_SGS.TonsRemainingByStatus, " & _
    "vsoGPP_SGS.ItemID, vsoGPP_SGS.ItemKey, vsoGPP_SGS.RollingWAC, tsoSalesOrderExt_SGS.JobState " & _
    "From vsoGPP_SGS "
    sSelect = sSelect & " Left Outer Join tsoSalesOrderExt_SGS with(NoLock) "
    sSelect = sSelect & " on vsoGPP_SGS.sokey = tsoSalesOrderExt_SGS.SOKey "

    PrepSelect = sSelect
End Function

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basSOGPPReport"
End Function

Public Function lStartReport(sButton As String, frm As Form, bPeriodEnd As Boolean, _
    sPerEndDate, iPerNo, sPerYear, Optional iFileType As Variant, Optional sFileName As Variant, _
    Optional bUseStandardCost As Boolean = True, _
    Optional bUsePlannedTons As Boolean = True) As Long
    
    Dim sWhereClause As String
    Dim sTablesUsed As String
    Dim sSelect As String
    Dim sInsert As String
    Dim lRetval As Long
    Dim bValid As Boolean
    Dim iNumTablesUsed As Integer
    Dim RptFileName As String
    Dim lBadRow As Long
    Dim SelectObj As clsSelection
    Dim ReportObj As clsReportEngine
    Dim SortObj As clsSort
    Dim DBObj As Object
    Dim SettingsObj As clsSettings
    Dim InsColumns As String
    
    Dim sPermissions As String
    
    On Error GoTo badexit

    lStartReport = kFailure
    
    If Not bPeriodEnd Then ShowStatusBusy frm

    Set SelectObj = frm.moSelect
    Set ReportObj = frm.moReport
    Set SortObj = frm.moSort

    Set DBObj = frm.oClass.moAppDB
    Set SettingsObj = frm.moSettings
   
    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
    
    'Correct bad user input in the Selection grid.
    'lBadRow contains the failed row if the parameter passed is False
    lBadRow = SelectObj.lValidateGrid(True)
    
    'Create the WHERE clause based on user input in the Selection grid & Session ID.
    bValid = SelectObj.bGetWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True)

    'Append criteria to restrict data returned to the current company.
    'RKL DEJ 2018-08-24 Added If Statement
    If frm.chFilterByCompany.Value = vbChecked Then
        SelectObj.AppendToWhereClause sWhereClause, "vsoGPP_SGS.CompanyID=" & gsQuoted(frm.msCompanyID)
    End If
    
    'CUSTOMIZE:  Add join specifications to WHERE clause based on appearance of a given table
    'in the sTablesUsed string (built as a result of call to bGetWhereClause above).
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tciAddress", "tarCustomer.PrimaryAddrKey=tciAddress.AddrKey"
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustAddr", "tarCustomer.CustKey=tarCustAddr.CustKey"
    
'    'SGS DEJ Commented out because we do not need the sintax of 'Where'
'    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
'    If Not SelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
'        GoTo badexit
'    End If

    'Get User data/whse Permissions
    sPermissions = " vsoGPP_SGS.WhseID in( " & _
"    select distinct wp.WhseID " & _
"    From tsmUserCompanyGrp g with(NoLock) " & _
"    Inner Join tsmUserGroupWhseExt_SGS wp with(NoLock) " & _
"        on g.UserGroupID = wp.UserGroupID " & _
"    where g.UserID = " & gsQuoted(frm.msUserID) & " "
If frm.chFilterByCompany.Value = vbChecked Then
    'Only add the company Filter if specified
    sPermissions = sPermissions & "    and g.CompanyID = " & gsQuoted(frm.msCompanyID) & " "
End If
sPermissions = sPermissions & ") "

SelectObj.AppendToWhereClause sWhereClause, sPermissions

    'CUSTOMIZE:  Build a string for selection of key field and session ID.
'    sSelect = "SELECT DISTINCT timItem., " & ReportObj.SessionID & " FROM " & sTablesUsed
'    sSelect = sSelect & " " & sWhereClause

    'RKL DEJ 2016-11-22 added Insert Columns
    'RKL DEJ 2018-08-24 Added JobState
    InsColumns = "( SessionID, sokey, solinekey, CompanyID, CompanyName, [Origin-WhseKey], Origin, ContractNo, Product, CustName, ProjectNumber, ProjectDesc, ContractTerm, ContractYear, BidQty, PlanQty, SellPerTon, CostPerTon, ProfitPerTon, TotalSale, TotalCost, TotalProfit, ProfitToDate, TonsSoldToDate, TonsRemaining, ProjectComplete, ContractType, ContractStatusNbr, ContractStatus, TonsRemainingByStatus, ItemID, ItemKey, RollingWAC, JobState )"
    
Debug.Print sWhereClause
    #If RPTDEBUG Then
        sInsert = "INSERT INTO tsoGrossProfitProjectionWrk_SGS " & InsColumns & PrepSelect(ReportObj.SessionID) & " Where " & sWhereClause '& sPermissions
    #Else
        sInsert = "INSERT INTO #tsoGrossProfitProjectionWrk_SGS " & InsColumns & PrepSelect(ReportObj.SessionID) & " Where " & sWhereClause '& sPermissions
    #End If
Debug.Print sInsert
    
    On Error Resume Next
    
    ReportObj.ReportTitle2 = Empty
    
    '**********************************************************************************************************************
    '**********************************************************************************************************************
    '**********************************************************************************************************************
    'RkL DEJ 2017-07-26 (START) - Old Code and logic commented out
    '**********************************************************************************************************************
    '**********************************************************************************************************************
    '**********************************************************************************************************************
'    If bUseStandardCost = False Then
''        sInsert = Replace(UCase(sInsert), UCase("vsoGPP_SGS"), UCase("vsoGPPReplacementCost_SGS"))
'
'            'SGS DEJ 1/19/09
'            If bUsePlannedTons = False Then
'                sInsert = Replace(UCase(sInsert), UCase("vsoGPP_SGS"), UCase("vsoGPPReplacementCostTonsRemaining_SGS"))
'                ReportObj.ReportTitle2 = "Based On Replacement Cost Based & Tons Remaining"
'            Else
'                sInsert = Replace(UCase(sInsert), UCase("vsoGPP_SGS"), UCase("vsoGPPReplacementCost_SGS"))
'                ReportObj.ReportTitle2 = "Based On Replacement Cost"
'            End If
'    Else
'            'SGS DEJ 1/19/09
'            If bUsePlannedTons = False Then
'                sInsert = Replace(UCase(sInsert), UCase("vsoGPP_SGS"), UCase("vsoGPPTonsRemaining_SGS"))
'
'                ReportObj.ReportTitle2 = "Based On Tons Remaining"
'            End If
'    End If
    
    '**********************************************************************************************************************
    '**********************************************************************************************************************
    '**********************************************************************************************************************
    'RkL DEJ 2017-07-26 (START) - New Code logic with frm.optRollingWAC
    '**********************************************************************************************************************
    '**********************************************************************************************************************
    '**********************************************************************************************************************
    If frm.optReplacement.Value = True Then
        If frm.optPlanned.Value = False Then
'            sInsert = Replace(UCase(sInsert), UCase("vsoGPP_SGS"), UCase("vsoGPPReplacementCostTonsRemaining_SGS"))
            sInsert = Replace(UCase(sInsert), UCase("vsoGPP_SGS"), UCase("vsoGPPCalcReplacementCostTonsRemaining_RKL")) 'RKL DEJ 2018-11-06 - Change how Profit To Date is calculated
            ReportObj.ReportTitle2 = "Tons Remaining With Weighted Average Forecast"
        Else
'            sInsert = Replace(UCase(sInsert), UCase("vsoGPP_SGS"), UCase("vsoGPPReplacementCost_SGS"))
            sInsert = Replace(UCase(sInsert), UCase("vsoGPP_SGS"), UCase("vlusoGPPCalcReplacementCost_RKL"))    'RKL DEJ 2018-11-06 - Change how cost is calculated
            
            ReportObj.ReportTitle2 = "Sales Plus Tons Remaining With Weighted Average Forecast"
        End If
    ElseIf frm.optStandard.Value = True Then
        If frm.optPlanned.Value = False Then
            sInsert = Replace(UCase(sInsert), UCase("vsoGPP_SGS"), UCase("vsoGPPTonsRemaining_SGS"))
            
            ReportObj.ReportTitle2 = "Based On Tons Remaining"
        End If
    ElseIf frm.optRollingWAC.Value = True Then
        If frm.optPlanned.Value = False Then
            sInsert = Replace(UCase(sInsert), UCase("vsoGPP_SGS"), UCase("vsoGPPRollingWACCostTonsRemaining_SGS"))
            ReportObj.ReportTitle2 = "Based On Rolling Wgt Avg Cost & Tons Remaining"
        Else
            sInsert = Replace(UCase(sInsert), UCase("vsoGPP_SGS"), UCase("vsoGPPRollingWACCost_SGS"))
            ReportObj.ReportTitle2 = "Based On Rolling Wgt Avg Cost"
        End If
    Else
        'Should Never get to there...
        If frm.optPlanned.Value = False Then
            sInsert = Replace(UCase(sInsert), UCase("vsoGPP_SGS"), UCase("vsoGPPTonsRemaining_SGS"))
            
            ReportObj.ReportTitle2 = "Based On Tons Remaining"
        End If
    End If
    
    '**********************************************************************************************************************
    '**********************************************************************************************************************
    '**********************************************************************************************************************
    'RkL DEJ 2017-07-26 (STOP)
    '**********************************************************************************************************************
    '**********************************************************************************************************************
    '**********************************************************************************************************************
    
Debug.Print sInsert
    
    
    DBObj.ExecuteSQL sInsert
'        sInsert = "INSERT INTO tsoGrossProfitProjectionWrk_SGS " & PrepSelect(ReportObj.SessionID) & " And " & sWhereClause
'    DBObj.ExecuteSQL sInsert
    
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
    gClearSotaErr
    On Error GoTo badexit
    
    'Check whether any records were inserted; if not, go to error handler.
    If IsMissing(iFileType) Then
        #If RPTDEBUG Then
            If Not SelectObj.bRecsToPrintExist(frm.sWorkTableCollection(1), "SessionID", ReportObj.SessionID, True) Then
'            If Not SelectObj.bRecsToPrintExist(tsoGrossProfitProjectionWrk_SGS, "SessionID", ReportObj.SessionID, True) Then
        #Else
'            If Not SelectObj.bRecsToPrintExist("#tsoGrossProfitProjectionWrk_SGS", "SessionID", ReportObj.SessionID, True) Then
            If Not SelectObj.bRecsToPrintExist("#" & frm.sWorkTableCollection(1), "SessionID", ReportObj.SessionID, True) Then
'            If Not SelectObj.bRecsToPrintExist("#" & Left(frm.sWorkTableCollection(1), 19), "SessionID", ReportObj.SessionID, True) Then
        #End If
            GoTo badexit
        End If
    End If
   
'    'CUSTOMIZE:  Set input and output parameters as needed and execute appropriate
'    'custom stored procedure to populate remaining fields in primary work table and
'    'insert records into secondary work tables.
'    With DBObj
'        .SetInParam (ReportObj.SessionID)
'        .SetOutParam lRetval
'        On Error Resume Next
'        .ExecuteSP ("spSGSReportTest")
'        If Err.Number <> 0 Then
'            lRetval = Err.Number
'        Else
'            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
'            'errors after each attempt.  After storing all return values in local variables OR
'            'encountering an error, release parameters.
'            lRetval = DBObj.GetOutParam(2)
'        End If
'    End With
'
'    If lRetval <> 0 Then
'        ReportObj.ReportError kmsgProc, ""
'        DBObj.ReleaseParams
'        GoTo badexit
'    End If
        
    DBObj.ReleaseParams
    gClearSotaErr
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)
'    ReportObj.ReportFileName() = "GrossProfitProjection_SGS.rpt"
    ReportObj.ReportFileName() = "IAGrossProfitProjection.rpt"
    
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    ReportObj.Orientation() = vbPRORPortrait
    
    'CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    'to gsBuildString with a VB constant defined in LocalizationConst.bas. The subtitles should
    'not include the format selected by the user, i.e., "Detail" or "Summary".
    'ReportObj.ReportTitle2() = ""

    ReportObj.ReportTitle1() = "GROSS PROFIT PROJECTION"

    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    'using the "lbl" convention on formula field names so that label text will handled automatically
    ReportObj.UseSubTotalCaptions() = 1
    ReportObj.UseHeaderCaptions() = 1
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If

    'Set sort order in .RPT file according to user selections in the Sort grid.
    If (ReportObj.lSetSortCriteria(frm.moSort, 2) = kFailure) Then
        GoTo badexit
    End If
    
    '********* the following is specific to your report *************'
    Select Case RptFileName
        Case "XXZYY001.RPT"
        Case Else
    End Select
    '********* End of special processing *************'
    
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.SetSQL

    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    ReportObj.SetReportCaptions
    
    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
    ReportObj.SelectString = SelectObj.sGetUserReadableWhereClause(kLenPortrait)
    
    'If user has chosen to print report settings, set text for summary section.
    If (ReportObj.lSetSummarySection(frm.chkSummary.Value, frm.moOptions) = kFailure) Then
        GoTo badexit
    End If
    
    'CUSTOMIZE:  If using work tables, restrict report data to current Session ID.  If using
    'real tables, might restrict report data to current company or other criteria.
'    If (ReportObj.lRestrictBy("{.SessionID} = " & ReportObj.SessionID) = kFailure) Then
'SGS DEJ change to map to the actual table.
    If (ReportObj.lRestrictBy("{" & frm.sWorkTableCollection(1) & ".SessionID} = " & ReportObj.SessionID) = kFailure) Then

    'frm.sWorkTableCollection(1)
        GoTo badexit
    End If
    
    If bPeriodEnd Then
        gbSkipPrintDialog = True    '   don't show print dialog
    End If
    
    ReportObj.ProcessReport frm, sButton, iFileType, sFileName
    gbSkipPrintDialog = True    '   if multiple rpts, show print dialog one time only.
            
            
    If Not bPeriodEnd Then ShowStatusNone frm
    
    lStartReport = kSuccess
    
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    
    Exit Function
    
badexit:
    ReportObj.CleanupWorkTables
    If Not bPeriodEnd Then ShowStatusNone frm
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
    Exit Function
End Function

