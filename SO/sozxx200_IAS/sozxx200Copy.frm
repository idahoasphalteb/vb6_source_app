VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "NEWSOTA.OCX"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#157.0#0"; "ENTRYLOOKUPCONTROLS.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "SOTASbar.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpspr80.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{9504980C-B928-4BF5-A5D0-13E1F649AECB}#35.0#0"; "SOTAVM.OCX"
Begin VB.Form frmCopyCompMarketShare 
   Caption         =   "Competitor Market Share"
   ClientHeight    =   5910
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15675
   HelpContextID   =   16608
   Icon            =   "sozxx200Copy.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5910
   ScaleWidth      =   15675
   Begin VB.CommandButton cmdInvertSelect 
      Caption         =   "Invert Selection"
      Height          =   495
      Left            =   2640
      TabIndex        =   24
      Top             =   960
      Width           =   1095
   End
   Begin VB.CommandButton cmdUnSelectAll 
      Caption         =   "Un Select All"
      Height          =   495
      Left            =   1440
      TabIndex        =   23
      Top             =   960
      Width           =   1095
   End
   Begin VB.CommandButton cmdSelectAll 
      Caption         =   "Select All"
      Height          =   495
      Left            =   240
      TabIndex        =   22
      Top             =   960
      Width           =   1095
   End
   Begin VB.TextBox txtNewYear 
      Height          =   285
      Left            =   7560
      MaxLength       =   4
      TabIndex        =   4
      Top             =   480
      Width           =   1335
   End
   Begin EntryLookupControls.TextLookup lkuItemClass 
      Height          =   285
      Left            =   3360
      TabIndex        =   21
      Top             =   2280
      WhatsThisHelpID =   17374184
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   503
      ForeColor       =   -2147483640
      MaxLength       =   12
      IsSurrogateKey  =   -1  'True
      LookupID        =   "ItemClass"
      ParentIDColumn  =   "ItemClassID"
      ParentKeyColumn =   "ItemClassKey"
      ParentTable     =   "timItemClass"
      BoundColumn     =   "ItemClassKey"
      BoundTable      =   "timItem"
      IsForeignKey    =   -1  'True
      sSQLReturnCols  =   "ItemClassID,lkuItemClass,;"
   End
   Begin EntryLookupControls.TextLookup lkuCompetitor 
      Height          =   315
      Left            =   5520
      TabIndex        =   19
      Top             =   1680
      Width           =   1260
      _ExtentX        =   2223
      _ExtentY        =   556
      ForeColor       =   -2147483640
      MaxLength       =   50
      WildCardChar    =   "?"
      IsSurrogateKey  =   -1  'True
      LookupID        =   "soCompetitor"
      ParentIDColumn  =   "CompetitorID"
      ParentKeyColumn =   "CompetitorKey"
      ParentTable     =   "tsoCompetitor_SGS"
      BoundColumn     =   "CompetitorKey"
      BoundTable      =   "tsoCompetitor_SGS"
      IsForeignKey    =   -1  'True
      sSQLReturnCols  =   "CompetitorID,lkucompetitor,;CompetitorKey,,;"
   End
   Begin EntryLookupControls.TextLookup lkuWhse 
      Height          =   285
      Left            =   3240
      TabIndex        =   20
      Top             =   1680
      WhatsThisHelpID =   60195
      Width           =   1995
      _ExtentX        =   3519
      _ExtentY        =   503
      ForeColor       =   -2147483640
      MaxLength       =   6
      IsSurrogateKey  =   -1  'True
      LookupID        =   "Warehouse"
      ParentIDColumn  =   "WhseID"
      ParentKeyColumn =   "WhseKey"
      ParentTable     =   "timWarehouse"
      BoundColumn     =   "WhseKey"
      BoundTable      =   "timInventory"
      IsForeignKey    =   -1  'True
      sSQLReturnCols  =   "WhseID,lkuWhse,;Description,lblWhseDesc,;"
   End
   Begin VB.CommandButton cmdFilter 
      Caption         =   "Get Records"
      Height          =   255
      Left            =   3720
      TabIndex        =   2
      Top             =   480
      Width           =   1095
   End
   Begin VB.TextBox txtFilterYear 
      Height          =   285
      Left            =   2160
      MaxLength       =   4
      TabIndex        =   1
      Top             =   480
      Width           =   1335
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -10000
      TabIndex        =   8
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -10000
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.ComboBox CustomCombo 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      ForeColor       =   &H80000012&
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   15
      TabStop         =   0   'False
      Text            =   "Combo1"
      Top             =   1530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin FPSpreadADO.fpSpread grdMain 
      Height          =   3825
      Left            =   120
      TabIndex        =   5
      Top             =   1560
      WhatsThisHelpID =   16614
      Width           =   15420
      _Version        =   524288
      _ExtentX        =   27199
      _ExtentY        =   6747
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SpreadDesigner  =   "sozxx200Copy.frx":23D2
      AppearanceStyle =   0
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   0
      Width           =   15675
      _ExtentX        =   27649
      _ExtentY        =   741
      Style           =   7
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   5520
      Width           =   15675
      _ExtentX        =   27649
      _ExtentY        =   688
      BrowseVisible   =   0   'False
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   9
      Top             =   4320
      Visible         =   0   'False
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   2880
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   615
      Index           =   0
      Left            =   -10000
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   720
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1085
      _StockProps     =   0
   End
   Begin SOTAVM.SOTAValidationMgr valMgr 
      Left            =   -10000
      Top             =   180
      _ExtentX        =   741
      _ExtentY        =   661
      CtlsCount       =   0
   End
   Begin VB.Label Label1 
      Caption         =   "Copy To This Contract Year:"
      Height          =   255
      Left            =   5520
      TabIndex        =   3
      Top             =   480
      Width           =   2055
   End
   Begin VB.Label lblContractYearFilter 
      Caption         =   "Lookup Contract Year:"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   480
      Width           =   1815
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   10
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmCopyCompMarketShare"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private miOldFormHeight     As Long

Private miOldFormWidth      As Long

Private miMinFormHeight     As Long

Private miMinFormWidth      As Long

Private moClass             As Object

Private mlRunMode           As Long

Private mbSaved             As Boolean

Private mbCancelShutDown    As Boolean

Private miSecurityLevel     As Integer

Public moSotaObjects        As New Collection

Private mbEnterAsTab        As Boolean

Private msCompanyID         As String

'Main Table Constants
    Private Const kColMarketShareKey = 1
    Private Const kColCopy = 2
    Private Const kColCustName = 3
    Private Const kColDescription = 4
    Private Const kColContractYear = 5
    Private Const kColCreateDate = 6
    Private Const kColApplyToMonth = 7
    Private Const kColContractType = 8
    Private Const kColItemClassKey = 9
    Private Const kColItemClassID = 10
    Private Const kColWhseKey = 11
    Private Const kColWhseID = 12
    Private Const kColJobState = 13
    Private Const kColCompanyID = 14
    Private Const kColCompetitorKey = 15
    Private Const kColCompetitorID = 16
    
    Private Const kColCompetitorDetlKey = 17
    Private Const kColCompetitorOrigin = 18
    
    
    Private Const kColQty = 19

    Private Const kColMax = 19


'    Private Const kColMarketShareKey = 1
'    Private Const kColCustName = 2
'    Private Const kColDescription = 3
'    Private Const kColContractYear = 4
'    Private Const kColCreateDate = 5
'    Private Const kColApplyToMonth = 6
'    Private Const kColContractType = 7
'    Private Const kColItemClassKey = 8
'    Private Const kColItemClassID = 9
'    Private Const kColWhseKey = 10
'    Private Const kColWhseID = 11
'    Private Const kColJobState = 12
'    Private Const kColCompanyID = 13
'    Private Const kColCompetitorKey = 14
'    Private Const kColCompetitorID = 15
'    Private Const kColQty = 16

Private moContextMenu       As clsContextMenu

Public mlLanguage               As Long

Private mbLoadSuccess       As Boolean

Const VBRIG_MODULE_ID_STRING = "sozxx200Copy.frm"

Public WithEvents moDmGrid As clsDmGrid
Attribute moDmGrid.VB_VarHelpID = -1

Private WithEvents moGM                As clsGridMgr
Attribute moGM.VB_VarHelpID = -1

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If






Private Sub cmdFilter_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim lcSQL As String
    
    lcSQL = "Delete #tsoCompetitorMarketShare_sgs"
    
    moClass.moAppDB.ExecuteSQL lcSQL
    
    
    If IsNumeric(txtFilterYear.Text) = True Then
        lcSQL = "Insert Into #tsoCompetitorMarketShare_sgs Select 0, * From tsoCompetitorMarketShare_sgs Where 1=1 And CompanyID = " & gsQuoted(msCompanyID) & " And ContractYear = " & Trim(txtFilterYear.Text)
        moClass.moAppDB.ExecuteSQL lcSQL
        
    End If
    
    moDmGrid.Init
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cmdFilter_Click", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub



Private Sub cmdInvertSelect_Click()
    On Error GoTo Error
    
    Dim i As Long
    
    For i = 1 To grdMain.MaxRows
        
        If gsGridReadCell(grdMain, i, kColCopy) = "1" Then
            moDmGrid.SetCellValue i, kColCopy, SQL_INTEGER, "0"
        Else
            moDmGrid.SetCellValue i, kColCopy, SQL_INTEGER, "1"
        End If
        moDmGrid.SetRowDirty i
    Next

    moDmGrid.Save

    Exit Sub
Error:
    MsgBox Me.Name & ".cmdInvertSelect_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
End Sub

Private Sub cmdSelectAll_Click()
    On Error GoTo Error
        
    Dim i As Long
    
    For i = 1 To grdMain.MaxRows
        moDmGrid.SetCellValue i, kColCopy, SQL_INTEGER, "1"
        moDmGrid.SetRowDirty i
    Next
    
    moDmGrid.Save
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdSelectAll_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
End Sub


Private Sub cmdUnSelectAll_Click()
    On Error GoTo Error
    
    Dim i As Long
    
    For i = 1 To grdMain.MaxRows
        moDmGrid.SetCellValue i, kColCopy, SQL_INTEGER, "0"
        moDmGrid.SetRowDirty i
    Next

    moDmGrid.Save

    Exit Sub
Error:
    MsgBox Me.Name & ".cmdUnSelectAll_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mbCancelShutDown = False
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
    End Select
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{TAB}"
                KeyAscii = 0
            End If
        Case Else
            'Other KeyAscii routines.
    End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    mbLoadSuccess = False

  'Setup form level variables
    msCompanyID = moClass.moSysSession.CompanyID
    mbEnterAsTab = moClass.moSysSession.EnterAsTab
    mlLanguage = moClass.moSysSession.Language
    SetFormTitle
    SetupToolbar
    
    CreateTempTable
    
    SetupLookups
    
    If Not bSetupGrid Then
        Exit Sub
    End If
    BindForm
    SetupStatusbar
    BindGM
    BindContextMenu
            
    ' set form size variables
    miOldFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormHeight = miOldFormHeight
    miMinFormWidth = miOldFormWidth
    
    mbLoadSuccess = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
    Dim iConfirmUnload  As Integer

   'Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

    If moClass.mlError = 0 Then
        iConfirmUnload = moDmGrid.ConfirmUnload()
    
        Select Case iConfirmUnload
            Case kDmSuccess
                'Do Nothing
                
            Case kDmFailure
                GoTo CancelShutDown
                
            Case kDmError
                GoTo CancelShutDown
                'If you need Data Manager Error Value then you would
                'dimension a variable, such as lError as Long and assign
                'to the DataManager Error property.
                'lError = modmgrid.Error
                
            Case Else
                MsgBox "Unexpected Confirm Unload Return Value: " & iConfirmUnload
                
        End Select
    
    
        'Check all other forms  that may have been loaded from this main form.
        'If there are any visible forms, then this means the form is active.
        'Therefore, cancel the shutdown.
'        If gbActiveChildForms(Me) Then GoTo CancelShutDown
'
'        Select Case UnloadMode
'            Case vbFormCode
'            'Do Nothing.
'            'If the unload is caused by form code, then the form
'            'code should also have the miShutDownRequester set correctly.
'
'            Case Else
'           'Most likely the user has requested to shut down the form.
'            'If the context is Add-on-the-fly, hide the form
'            'and cancel the unload.
'            'If the context is Normal or Drill-Around, have the object unload itself.
'                 Select Case mlRunMode
'                    Case kContextAOF
'                        Me.Hide
'                        GoTo CancelShutDown
'
'                    Case Else
'                        moClass.miShutDownRequester = kUnloadSelfShutDown
'
'                End Select
'
'        End Select
'
    End If
    
    
    'If execution gets to this point, the form and class object of the form
    'will be shut down.  Perform all operations necessary for a clean shutdown.
    
    PerformCleanShutDown
    
'    Select Case moClass.miShutDownRequester
'        Case kUnloadSelfShutDown
'            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
'            Set moClass.moFramework = Nothing
'            Set moClass = Nothing               ' class object
'
'        Case Else 'kFrameworkShutDown
'            'Do nothing
'
'    End Select
    
Exit Sub

CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim i       As Integer
    Dim iLeft   As Integer
    
    'List of all the controls that can resize Down (grow in Height)
    gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, grdMain
               
    'List of all the controls that can resize right (grow in Width).
    gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, grdMain
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Unload(Cancel As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If
    On Error GoTo ExpectedErrorRoutine
    On Error Resume Next


    On Error GoTo ExpectedErrorRoutine
    Exit Sub
        
ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub HandleToolBarClick(sKey As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If

    frmCopyCompMarketShare.SetFocus
    DoEvents
    
    Select Case sKey
        
      'Save changes and close the form
        Case kTbFinishExit
            If IsNumeric(Trim(txtNewYear.Text)) = False Then
                MsgBox "You must specify a new contract year first.", vbExclamation, "MAS 500"
                Exit Sub
            End If
            
            If Len(Trim(txtNewYear.Text)) <> 4 Then
                MsgBox "The new contract year must be a 4 digit number.", vbExclamation, "MAS 500"
                Exit Sub
            End If
            
            moDmGrid.SetDirty True, True
            
            If (moDmGrid.Action(kDmFinish) = kDmSuccess) Then
                'Call proc to copy records
                moClass.moAppDB.SetInParamInt glGetValidLong(Trim(txtNewYear.Text))
                
                moClass.moAppDB.ExecuteSP ("spSOCopyCompetitorMrktData_SGS")
                
                
                moClass.moAppDB.ReleaseParams
                
                
                Unload Me
            End If
            
        Case kTbSave
            moDmGrid.Action kDmFinish
 
      'Cancel changes and close the form
        Case kTbCancelExit
            If (moDmGrid.Action(kDmCancel) = kDmSuccess) Then
'                moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
                Unload Me
            End If
                        
      'Display form level help
        Case kTbHelp
            gDisplayFormLevelHelp Me
            
      'Example code to save changes and display report form
        Case kTbPrint
            If moDmGrid.ConfirmUnload(True) Then                 'Have user save changes
                'Add associated Listing Task Id constant as second parameter
                'gPrintTask moClass.moFramework, <add constant here>
            End If
        
        Case Else
            
            tbrMain.GenericHandler sKey, Me, moDmGrid, moClass
    
    End Select
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "HandleToolBarClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuCompetitor_AfterValidate(bIsValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim lRow As Long
    Dim lcCompetitorKey As Long
    
    lRow = glGridGetActiveRow(grdMain)
    
    If bIsValid = True Then
        lcCompetitorKey = glGetValidLong(moClass.moAppDB.Lookup("CompetitorKey", "tsoCompetitor_sgs", "CompanyID = " & gsQuoted(msCompanyID) & " AND CompetitorID = " & gsQuoted(lkuCompetitor.Text)))
        moDmGrid.SetColumnValue lRow, "CompetitorKey", lcCompetitorKey
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuCompetitor_AfterValidate", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub lkuCompetitor_Validate(Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lRow As Long
    
    lRow = glGridGetActiveRow(grdMain)
    
    If Trim(lkuCompetitor.Text) = Empty Then
        moDmGrid.SetColumnValue lRow, "CompetitorKey", Empty
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuCompetitor_Validate", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuItemClass_AfterValidate(bIsValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lRow As Long
    Dim lcItemClassKey As Long
    
    lRow = glGridGetActiveRow(grdMain)
    
    If bIsValid = True Then
        lcItemClassKey = glGetValidLong(moClass.moAppDB.Lookup("ItemClassKey", "timItemClass", "CompanyID = " & gsQuoted(msCompanyID) & " AND ItemClassID = " & gsQuoted(lkuItemClass.Text)))
        moDmGrid.SetColumnValue lRow, "ItemClassKey", lcItemClassKey
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuItemClass_AfterValidate", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub lkuItemClass_Validate(Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lRow As Long
    
    lRow = glGridGetActiveRow(grdMain)
    
    If Trim(lkuItemClass.Text) = Empty Then
        moDmGrid.SetColumnValue lRow, "ItemClassKey", Empty
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuItemClass_Validate", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuWhse_AfterValidate(bIsValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim lRow As Long
    Dim lcWhseKey As Long
    
    lRow = glGridGetActiveRow(grdMain)
    
    If bIsValid = True Then
        lcWhseKey = glGetValidLong(moClass.moAppDB.Lookup("WhseKey", "timWarehouse", "CompanyID = " & gsQuoted(msCompanyID) & " AND WhseID = " & gsQuoted(lkuWhse.Text)))
        moDmGrid.SetColumnValue lRow, "WhseKey", lcWhseKey
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuWhse_AfterValidate", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub lkuWhse_Validate(Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim lRow As Long
    
    lRow = glGridGetActiveRow(grdMain)
    
    If Trim(lkuWhse.Text) = Empty Then
        moDmGrid.SetColumnValue lRow, "WhseKey", Empty
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuWhse_Validate", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmGrid_DMGridRowLoaded(lRow As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim lcCompetitorKey As Long
    Dim lcCompetitorID As String

    Dim lcWhseKey As Long
    Dim lcWhseID As String

    Dim lcItemClassKey As Long
    Dim lcItemClassID As String
    
    Dim lcCompetitorDetlKey As Long
    Dim lcOrigin As String

    lcCompetitorKey = glGetValidLong(moDmGrid.GetColumnValue(lRow, "CompetitorKey"))
    lcCompetitorID = gsGetValidStr(moClass.moAppDB.Lookup("CompetitorID", "tsoCompetitor_sgs", "CompanyID = " & gsQuoted(msCompanyID) & " AND CompetitorKey = " & lcCompetitorKey))

    lcWhseKey = glGetValidLong(moDmGrid.GetColumnValue(lRow, "WhseKey"))
    lcWhseID = gsGetValidStr(moClass.moAppDB.Lookup("WhseID", "timWarehouse", "CompanyID = " & gsQuoted(msCompanyID) & " AND WhseKey = " & lcWhseKey))

    lcItemClassKey = glGetValidLong(moDmGrid.GetColumnValue(lRow, "ItemClassKey"))
    lcItemClassID = gsGetValidStr(moClass.moAppDB.Lookup("ItemClassID", "timItemClass", "CompanyID = " & gsQuoted(msCompanyID) & " AND ItemClassKey = " & lcItemClassKey))

    lcCompetitorDetlKey = glGetValidLong(moDmGrid.GetColumnValue(lRow, "CompetitorDetlKey"))
    lcOrigin = gsGetValidStr(moClass.moAppDB.Lookup("Origin", "vsoCompetitorOrigins_SGS", "CompetitorDetlKey = " & lcCompetitorDetlKey))


    gGridUpdateCellText grdMain, lRow, kColCompetitorID, lcCompetitorID
    gGridUpdateCellText grdMain, lRow, kColWhseID, lcWhseID
    gGridUpdateCellText grdMain, lRow, kColItemClassID, lcItemClassID

    gGridUpdateCellText grdMain, lRow, kColCompetitorOrigin, lcOrigin

    moDmGrid.SetDirty False, True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmGrid_DMGridRowLoaded", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmGrid_DMGridValidate(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
'    Dim lcMarketShareKey As Long
'    Dim lcCompanyID As String
'    Dim lcContractYear As Long
'    Dim lcContractType As String
'    Dim lcJobState As String
'    Dim lcCustName As String
'    Dim lcDescription As String
'    Dim lcItemClassKey As Long
'    Dim lcWhseKey As Long
'    Dim lcQty As Double
'    Dim lcCompetitorKey As Long
'    Dim lcWhere As String
'    Static bInProcess As Boolean
'
'    If bInProcess = True Then Exit Sub
'
'    bInProcess = True
'
'    'Validate the row being saved.
'
'    lcMarketShareKey = glGetValidLong(moDmGrid.GetColumnValue(lRow, "MarketShareKey"))
'    lcCompanyID = gsGetValidStr(moDmGrid.GetColumnValue(lRow, "CompanyID"))
'    lcContractYear = glGetValidLong(moDmGrid.GetColumnValue(lRow, "ContractYear"))
'    lcContractType = gsGetValidStr(moDmGrid.GetColumnValue(lRow, "ContractType"))
'    lcJobState = gsGetValidStr(moDmGrid.GetColumnValue(lRow, "JobState"))
'    lcCustName = gsGetValidStr(moDmGrid.GetColumnValue(lRow, "CustName"))
'    lcDescription = gsGetValidStr(moDmGrid.GetColumnValue(lRow, "Description"))
'    lcItemClassKey = glGetValidLong(moDmGrid.GetColumnValue(lRow, "ItemClassKey"))
'    lcWhseKey = glGetValidLong(moDmGrid.GetColumnValue(lRow, "WhseKey"))
'    lcQty = gdGetValidDbl(moDmGrid.GetColumnValue(lRow, "Qty"))
'    lcCompetitorKey = glGetValidLong(moDmGrid.GetColumnValue(lRow, "CompetitorKey"))
'
'    lcWhere = "MarketShareKey <> " & lcMarketShareKey & " And CompanyID = " & gsQuoted(lcCompanyID) & _
'    " AND ContractYear = " & lcContractYear & " And ContractType  = " & gsQuoted(lcContractType) & _
'    " And JobState = " & gsQuoted(lcJobState) & " And CustName = " & gsQuoted(lcCustName) & _
'    " And ItemClassKey = " & lcItemClassKey & " And WhseKey = " & lcWhseKey
'
'    If glGetValidLong(moClass.moAppDB.Lookup("Count(*)", "tsoCompetitorMarketShare_sgs", lcWhere)) > 0 Then
'        'This record already exists.  Don't create a new one.
'        bValid = False
'        MsgBox "This Customer, Company, Contract Year, Contract Type, Job State, Item Class, and Warehouse already exist in the database.  The record will not be saved.", vbExclamation, "MAS 500"
'        bInProcess = False
'        Exit Sub
'    End If
'
'    bInProcess = False
    bValid = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmGrid_DMGridValidate", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub moGM_CellChange(ByVal lRow As Long, ByVal lCol As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Select Case lCol
        Case kColMarketShareKey
        Case kColCustName
        Case kColDescription
        Case kColContractYear
        Case kColContractType
        Case kColItemClassKey
        Case kColItemClassID
            lkuItemClass.Visible = True
        Case kColWhseKey
        Case kColWhseID
            lkuWhse.Visible = True
        Case kColJobState
        Case kColCompanyID
        Case kColCompetitorKey
        Case kColCompetitorID
            lkuCompetitor.Visible = True
        Case kColQty
    
        Case kColCreateDate
    
        Case kColApplyToMonth
    
    End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moGM_CellChange", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub moGM_LeaveGridRow(ByVal lLeaveRow As Long, ByVal lEnterRow As Long, ByVal eReason As GridMgrExt.LeaveGridRowReasons, bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If moDmGrid.IsDirty = True Then
        'Validate the data.
        If (moDmGrid.Action(kDmFinish) <> kDmSuccess) Then
            'Did not save so return to same row
            gGridSetSelectRow grdMain, lLeaveRow
        End If

'        lEnterRow
    Else
        'Not dirty so don't do anything
        
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moGM_LeaveGridRow", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub sbrMain_ButtonClick(sButton As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    HandleToolBarClick sButton

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "sbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    HandleToolBarClick Button
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function

Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    lRunMode = mlRunMode
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lRunMode", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlRunMode = lNewRunMode
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lRunMode", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Sub PerformCleanShutDown()
    
    On Error GoTo ExpectedErrorRoutine
    On Error Resume Next
    
'    'Unload all forms loaded from this main form
'    gUnloadChildForms Me
'
'    'Remove all child collections
'    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
         
    If (Not (moGM Is Nothing)) Then
        moGM.UnloadSelf
        Set moGM = Nothing
    End If

    'Set Data Manager references to Nothing
    If Not moDmGrid Is Nothing Then
        moDmGrid.UnloadSelf
        Set moDmGrid = Nothing
    End If
   
   TerminateControls Me
         

    Set moContextMenu = Nothing         ' context menu class
    Set moSotaObjects = Nothing         ' SOTA Child objects collection
    
    DropTempTable
   
'    'Set form-level object references to Nothing
'    Set moClass.moAppDB = Nothing
'    Set moClass.moSysSession = Nothing

    'Set Static List manager objects to Nothing


    'Exit this subroutine
    Exit Sub

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bCancelShutDown = mbCancelShutDown
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bCancelShutDown", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Function DMGridBeforeInsert(oDM As Object, lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    If oDM Is moDmGrid Then
        oDM.SetColumnValue lRow, "MarketShareKey", glGetNextSurrogateKey(moClass.moAppDB, "tsoCompetitorMarketShare_SGS")
    End If
    oDM.SetColumnValue lRow, "CompanyID", msCompanyID
    oDM.SetColumnValue lRow, "CreateDate", Date + Time


    DMGridBeforeInsert = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DMGridBeforeInsert", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub SetFormTitle()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Me.Caption = "Copy Competitor Market Share Records"
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetFormTitle", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    FormHelpPrefix = "SOZ"
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    WhatHelpPrefix = "SOZ"
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Sub DMGridRowLoaded(DMO As Object, Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}



'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DMGridRowLoaded", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function DMGridBeforeUpdate(oDM As Object, lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    DMGridBeforeUpdate = False



    DMGridBeforeUpdate = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DMGridBeforeUpdate", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bLoadSuccess = mbLoadSuccess
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bLoadSuccess", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Let bLoadSuccess(bNewLoadSuccess As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mbLoadSuccess = bNewLoadSuccess
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bLoadSuccess", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyDataBindings moDmGrid
                moFormCust.ApplyFormCust
        End If
    End If
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Property Get MyApp() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyApp = App
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyApp", VBRIG_IS_FORM                                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get MyForms() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyForms = Forms
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyForms", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get UseHTMLHelp() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    UseHTMLHelp = True   ' Form uses HTML Help (True/False)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "UseHTMLHelp", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property


Private Sub grdMain_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGM.Grid_Change Col, Row
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdMain_Change", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdMain_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGM.Grid_Click Col, Row
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdMain_Click", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdMain_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGM.Grid_KeyDown KeyCode, Shift
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdMain_KeyDown", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdMain_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGM.Grid_LeaveCell Col, Row, NewCol, NewRow
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdMain_LeaveCell", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdMain_LeaveRow(ByVal Row As Long, ByVal RowWasLast As Boolean, ByVal RowChanged As Boolean, ByVal AllCellsHaveData As Boolean, ByVal NewRow As Long, ByVal NewRowIsLast As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGM.Grid_LeaveRow Row, NewRow
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdMain_LeaveRow", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdMain_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moDmGrid.ProcessEventTopLeftChange OldTop, NewTop
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdMain_TopLeftChange", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub BindGM()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
       
 'Bind the grid manager
    Set moGM = New clsGridMgr

    With moGM
        Set .Grid = grdMain
        Set .DM = moDmGrid
        Set .Form = frmCopyCompMarketShare
        .GridType = kGridDataSheetNoAppend
        
        
        .BindColumn kColCompetitorID, lkuCompetitor
        .BindColumn kColWhseID, lkuWhse
        .BindColumn kColItemClassID, lkuItemClass
        
        .GridSortEnabled = True
        
        
        
        .Init
    End With


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindGM", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picdrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseDown Index, Button, Shift, x, y
    End If
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_MouseDown", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picdrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseMove Index, Button, Shift, x, y
    End If
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_MouseMove", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picdrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseUp Index, Button, Shift, x, y
    End If
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_MouseUp", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picdrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_Paint Index
    End If
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_Paint", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub BindForm()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Set moDmGrid = New clsDmGrid
    With moDmGrid
        Set .Form = frmCopyCompMarketShare
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
'        .OrderBy = "ContractType, ContractYear, CustName, ItemClassKey, JobState, WhseKey"
'        .UniqueKey = "ContractType, ContractYear, CustName, ItemClassKey, JobState, WhseKey"
        .UniqueKey = "MarketShareKey"
        .OrderBy = "CustName, ContractYear, JobState, ContractType, ItemClassKey, WhseKey"
        .Where = "CompanyID = '" & msCompanyID & "'"
        Set .Grid = grdMain
        
        .NoAppend = True
        .NoDelete = True
        .NoInsert = True
        
        .Table = "#tsoCompetitorMarketShare_sgs"
        .BindColumn "ContractType", kColContractType, SQL_CHAR
        .BindColumn "MarketShareKey", kColMarketShareKey, SQL_INTEGER
        .BindColumn "ContractYear", kColContractYear, SQL_INTEGER
        .BindColumn "CustName", kColCustName, SQL_CHAR
        .BindColumn "ItemClassKey", kColItemClassKey, SQL_INTEGER
        .BindColumn "JobState", kColJobState, SQL_CHAR
        .BindColumn "WhseKey", kColWhseKey, SQL_INTEGER
        .BindColumn "CompanyID", kColCompanyID, SQL_CHAR
        .BindColumn "Description", kColDescription, SQL_CHAR
        .BindColumn "Qty", kColQty, SQL_DECIMAL
        .BindColumn "CompetitorKey", kColCompetitorKey, SQL_INTEGER
        .BindColumn "CopyFlg", kColCopy, SQL_INTEGER

        .BindColumn "CompetitorDetlKey", kColCompetitorDetlKey, SQL_INTEGER

        .BindColumn "CreateDate", kColCreateDate, SQL_DATE
        .BindColumn "ApplyToMonth", kColApplyToMonth, SQL_CHAR
        
        .Init
    End With

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindForm", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetupToolbar()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    'Initialize toolbar.
    tbrMain.Init sotaTB_DATASHEET, mlLanguage
    With moClass.moFramework
        tbrMain.SecurityLevel = .GetTaskPermission(.GetTaskID())
    End With
    
    tbrMain.RemoveButton kTbPrint
    tbrMain.RemoveButton kTbHelp

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupToolbar", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetupStatusbar()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    '   Initialize status bar.
    Set sbrMain.Framework = moClass.moFramework
    sbrMain.StatusVisible = False

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupStatusbar", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
        
        Set moContextMenu = New clsContextMenu
        With moContextMenu
            .BindGrid moGM, grdMain.hWnd
            Set .Form = frmCopyCompMarketShare
            .Init
        End With
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bSetupGrid() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iRet As Integer
    
    '   Initialize Grid
' Add code here to format column headings, widths etc...

    bSetupGrid = False

    gGridSetProperties grdMain, kColMax, kGridDataSheet
    gGridSetColors grdMain
'    gGridSetMaxRows grdMain, 16
    gGridSetMaxCols grdMain, kColMax
    
    gGridSetColumnType grdMain, kColMarketShareKey, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColMarketShareKey, "Market Share Key"
    gGridSetColumnWidth grdMain, kColMarketShareKey, "10"
    gGridHideColumn grdMain, kColMarketShareKey
    
    gGridSetColumnType grdMain, kColCustName, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColCustName, "Customer Name"
    gGridSetColumnWidth grdMain, kColCustName, "20"
    
    gGridSetColumnType grdMain, kColCopy, SS_CELL_TYPE_CHECKBOX
    gGridSetHeader grdMain, kColCopy, "Copy"
    gGridSetColumnWidth grdMain, kColCopy, "7"
    
    gGridSetColumnType grdMain, kColDescription, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColDescription, "Description"
    gGridSetColumnWidth grdMain, kColDescription, "15"
    
    gGridSetColumnType grdMain, kColContractYear, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColContractYear, "Contract Year"
    gGridSetColumnWidth grdMain, kColContractYear, "10"
    
    gGridSetColumnType grdMain, kColContractType, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColContractType, "Contract Type"
    gGridSetColumnWidth grdMain, kColContractType, "10"
    
    gGridSetColumnType grdMain, kColItemClassKey, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColItemClassKey, "Item Class Key"
    gGridSetColumnWidth grdMain, kColItemClassKey, "10"
    gGridHideColumn grdMain, kColItemClassKey
    
    gGridSetColumnType grdMain, kColItemClassID, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColItemClassID, "Item Class ID"
    gGridSetColumnWidth grdMain, kColItemClassID, "14"
    
    gGridSetColumnType grdMain, kColWhseKey, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColWhseKey, "Whse Key"
    gGridSetColumnWidth grdMain, kColWhseKey, "10"
    gGridHideColumn grdMain, kColWhseKey
    
    gGridSetColumnType grdMain, kColWhseID, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColWhseID, "Whse ID"
    gGridSetColumnWidth grdMain, kColWhseID, "10"
    
    gGridSetColumnType grdMain, kColJobState, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColJobState, "Job State"
    gGridSetColumnWidth grdMain, kColJobState, "10"
    
    gGridSetColumnType grdMain, kColCompanyID, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColCompanyID, "Company ID"
    gGridSetColumnWidth grdMain, kColCompanyID, "10"
    gGridHideColumn grdMain, kColCompanyID
    
    gGridSetColumnType grdMain, kColCompetitorKey, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColCompetitorKey, "Competitor Key"
    gGridSetColumnWidth grdMain, kColCompetitorKey, "10"
    gGridHideColumn grdMain, kColCompetitorKey
    
    gGridSetColumnType grdMain, kColCompetitorID, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColCompetitorID, "Competitor ID"
    gGridSetColumnWidth grdMain, kColCompetitorID, "14"
    
    gGridSetColumnType grdMain, kColQty, SS_CELL_TYPE_FLOAT, 2
    gGridSetHeader grdMain, kColQty, "Qty"
    gGridSetColumnWidth grdMain, kColQty, "10"
    
    gGridSetColumnType grdMain, kColCreateDate, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColCreateDate, "Create Date"
    gGridSetColumnWidth grdMain, kColCreateDate, "10"
    gGridHideColumn grdMain, kColCreateDate
    
    gGridSetColumnType grdMain, kColApplyToMonth, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColApplyToMonth, "Apply To Month"
    gGridSetColumnWidth grdMain, kColApplyToMonth, "12"
    
    'Because this is a copy, don't allow edit except for the QTY, Contract Year, and Copy checkbox
    gGridLockColumn grdMain, kColMarketShareKey
    gGridLockColumn grdMain, kColCustName
    gGridLockColumn grdMain, kColDescription
    gGridLockColumn grdMain, kColContractType
    
    gGridLockColumn grdMain, kColCreateDate
    gGridLockColumn grdMain, kColApplyToMonth
    
    gGridLockColumn grdMain, kColItemClassKey
    gGridLockColumn grdMain, kColItemClassID
    gGridLockColumn grdMain, kColWhseKey
    gGridLockColumn grdMain, kColWhseID
    gGridLockColumn grdMain, kColJobState
    gGridLockColumn grdMain, kColCompanyID
    gGridLockColumn grdMain, kColCompetitorKey
    gGridLockColumn grdMain, kColCompetitorID
    
    gGridLockColumn grdMain, kColContractYear
'    gGridLockColumn grdMain, kColQty
'    gGridLockColumn grdMain, kColCopy
    
    gGridLockColumn grdMain, kColCompetitorDetlKey
    gGridLockColumn grdMain, kColCompetitorOrigin
    
    
    gGridSetColumnType grdMain, kColCompetitorDetlKey, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColCompetitorDetlKey, "CompetitorDetlKey"
    gGridSetColumnWidth grdMain, kColCompetitorDetlKey, "12"
    gGridHideColumn grdMain, kColCompetitorDetlKey
    
    gGridSetColumnType grdMain, kColCompetitorOrigin, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColCompetitorOrigin, "Competitor Origin"
    gGridSetColumnWidth grdMain, kColCompetitorOrigin, "12"
    
    
    grdMain.ScrollBars = ScrollBarsBoth
    
    bSetupGrid = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bSetupGrid", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub SetupLookups()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
        
    With lkuCompetitor
        Set .Framework = moClass.moFramework
        Set .AppDatabase = moClass.moAppDB
        .RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    End With
    
    With lkuWhse
        Set .Framework = moClass.moFramework
        Set .AppDatabase = moClass.moAppDB
        .RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    End With
    
    With lkuItemClass
        Set .Framework = moClass.moFramework
        Set .AppDatabase = moClass.moAppDB
        .RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    End With
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupLookups", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Sub CreateTempTable()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim lcSQL As String
    
    lcSQL = "Select 0 'CopyFlg', * Into #tsoCompetitorMarketShare_sgs From tsoCompetitorMarketShare_sgs Where 1=2"
    
    moClass.moAppDB.ExecuteSQL lcSQL
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CreateTempTable", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Sub DropTempTable()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim lcSQL As String
    
    lcSQL = "Drop Table #tsoCompetitorMarketShare_sgs "
    
    moClass.moAppDB.ExecuteSQL lcSQL
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DropTempTable", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


