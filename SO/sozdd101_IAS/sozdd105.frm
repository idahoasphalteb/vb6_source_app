VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#105.0#0"; "sotatbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#45.0#0"; "sotasbar.ocx"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#69.0#0"; "lookupview.ocx"
Begin VB.Form frmCompItem 
   Caption         =   "Pick Kit Components"
   ClientHeight    =   4785
   ClientLeft      =   60
   ClientTop       =   3930
   ClientWidth     =   9345
   HelpContextID   =   17774341
   Icon            =   "sozdd105.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4785
   ScaleWidth      =   9345
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdDist 
      Caption         =   "Dist..."
      Enabled         =   0   'False
      Height          =   375
      Left            =   8520
      TabIndex        =   4
      Top             =   480
      WhatsThisHelpID =   17774342
      Width           =   615
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtKitUOM 
      Height          =   285
      Left            =   7680
      TabIndex        =   3
      Top             =   555
      WhatsThisHelpID =   17774343
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      sBorder         =   0
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   9345
      _ExtentX        =   16484
      _ExtentY        =   741
      Style           =   6
   End
   Begin FPSpreadADO.fpSpread grdCompItem 
      Height          =   3390
      Left            =   120
      TabIndex        =   2
      Top             =   915
      WhatsThisHelpID =   17776643
      Width           =   9135
      _Version        =   524288
      _ExtentX        =   16113
      _ExtentY        =   5980
      _StockProps     =   64
      AllowCellOverflow=   -1  'True
      DisplayRowHeaders=   0   'False
      EditModeReplace =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ScrollBarExtMode=   -1  'True
      ScrollBars      =   2
      SpreadDesigner  =   "sozdd105.frx":23D2
      AppearanceStyle =   0
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtItemDesc 
      Height          =   285
      Left            =   2280
      TabIndex        =   5
      Top             =   580
      WhatsThisHelpID =   17774345
      Width           =   3375
      _Version        =   65536
      _ExtentX        =   5953
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      sBorder         =   0
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   4395
      WhatsThisHelpID =   73
      Width           =   9345
      _ExtentX        =   16484
      _ExtentY        =   688
      BrowseVisible   =   0   'False
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtItemID 
      Height          =   285
      Left            =   480
      TabIndex        =   7
      Top             =   580
      WhatsThisHelpID =   17774347
      Width           =   1575
      _Version        =   65536
      _ExtentX        =   2778
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      sBorder         =   0
   End
   Begin NEWSOTALib.SOTANumber nbrKitQty 
      Height          =   285
      Left            =   6600
      TabIndex        =   8
      Top             =   550
      WhatsThisHelpID =   17774348
      Width           =   915
      _Version        =   65536
      _ExtentX        =   1614
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<ILH>##|,###|,##<ILp0>#"
      text            =   "       0"
      sIntegralPlaces =   8
      sDecimalPlaces  =   0
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtlkuAutoDistBin 
      Height          =   255
      Left            =   360
      TabIndex        =   9
      Top             =   4500
      Visible         =   0   'False
      WhatsThisHelpID =   17774349
      Width           =   1215
      _Version        =   65536
      _ExtentX        =   2143
      _ExtentY        =   450
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin LookupViewControl.LookupView lkuAutoDistBin 
      Height          =   285
      Left            =   1605
      TabIndex        =   10
      TabStop         =   0   'False
      ToolTipText     =   "Lookup"
      Top             =   4500
      Visible         =   0   'False
      WhatsThisHelpID =   17774350
      Width           =   285
      _ExtentX        =   503
      _ExtentY        =   503
      LookupMode      =   1
   End
   Begin VB.Label lblReturnQty 
      AutoSize        =   -1  'True
      Caption         =   "Kit Pick Qty:"
      Height          =   285
      Left            =   5640
      TabIndex        =   6
      Top             =   580
      Width           =   1110
   End
   Begin VB.Label lblItem 
      AutoSize        =   -1  'True
      Caption         =   "Item:"
      Height          =   285
      Left            =   120
      TabIndex        =   1
      Top             =   580
      Width           =   345
   End
End
Attribute VB_Name = "frmCompItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************************************
'Date       Project     SE          Description of modification
'****************************************************************************************
Option Explicit

    'Public Form Variables
    Public moSotaObjects                As New Collection           'Collection of Loaded Objects
 
    Private WithEvents moGMKitComp      As clsGridMgr
Attribute moGMKitComp.VB_VarHelpID = -1
    Private moAppDB             As Object
    Private WithEvents moDMCompGrid    As clsDmGrid
Attribute moDMCompGrid.VB_VarHelpID = -1
    Private moSysSession        As Object
    Private moContextMenu       As New clsContextMenu
    Private moOptions           As clsModuleOptions
    Private moGridLkuAutoDistBin        As clsGridLookup
    Private moProcessPickList   As clsProcessPickList
    
    Private mbEnterAsTab        As Boolean
    Private mbCancelShutDown    As Boolean
    Private mlRunMode           As Long
    Private mlLanguage          As Long
    Private msCompanyID         As String
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long
    Private msSelectItem        As String
    Private mbIsValidating      As Boolean
    Private mbIMActivated       As Boolean
    Private mlKitShipLineKey    As Long
    Private mdKitQty            As Double
    Private mlKitItemKey        As Long
    Private msTranDate          As String
    Private mlRcvgWhseKey       As Long
    Private mlSOLineKey         As Long
    Private mbIsDirty           As Boolean
    Private mdCurrExchRate      As Double
    Private msCurrID            As String
    Private miCurrDigits        As Integer
    Private moClass             As Object
    Private moItemDist              As Object
    Private miLineStatus        As Integer
    Private mbTrackQtyAtBin     As Boolean
    Private miMousePointer      As Integer
    Private mlShipWhseKey       As Long
    Private mlTaskID            As Long
    Private mbShipmentGenerated As Boolean
    Private miEmptyBins         As Integer
    Private miEmptyRandomBins   As Integer
    Private mbCellChangeFired   As Boolean
    Private mdQtyOrdered        As Double
    Private miOverShipmentPolicy        As SOOverShipmentPolicy
    Private mlCurrentActiveColumn       As Long
    Private mlCurrentLineActiveRow          As Long
    Private mlCurrentRowShipLineKey     As Long
        
    Private Const kTypeBTOKit = IMS_BTO_KIT
    
    Private Const kOpen = 1
    Private Const kPosted = 2
    
    Private Const kPrimaryDistTmp = 1
    Private Const kSecondaryDistTmp = 2

    
    Public gbFormIsDirty        As Boolean
  
Public Property Get bIsDirty() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bIsDirty = mbIsDirty

'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsDirty", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
Public Property Let bIsDirty(bNewIsDirty As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbIsDirty = bNewIsDirty

'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsDirty", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moClass = oNewClass

'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
'************************************************************************
'   oClass contains the reference to the parent class object.  The form
'   needs this reference to use the public variables created within the
'   class object.
'************************************************************************
Public Property Get oProcessPickList() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oProcessPickList = moProcessPickList
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oProcessPickList", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Property Set oProcessPickList(oNewProcessPickList As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moProcessPickList = oNewProcessPickList

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oProcessPickList", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    FormHelpPrefix = "SOZ"

'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
Private Property Get sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Property
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    WhatHelpPrefix = "SOZ"

'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
Private Sub SetUpCompGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
     'Set general grid properties
    gGridSetProperties grdCompItem, kMaxLineCols, kGridDataSheetNoAppend
    gGridSetColors grdCompItem
        
    With grdCompItem
        .OperationMode = SS_OP_MODE_NORMAL
        .AllowMultiBlocks = False
        .SelectBlockOptions = SS_SELBLOCKOPT_ALL
        .NoBeep = True 'prevent beeps on locked cells
    End With
    
    'Set up the Component Item column & lock it
    gGridSetHeader grdCompItem, kColLineItem, gsBuildString(kIMColComponent, moAppDB, moClass.moSysSession)
    gGridSetColumnWidth grdCompItem, kColLineItem, 10
    gGridLockColumn grdCompItem, kColLineItem

    'Set up the Description column & lock it
    gGridSetHeader grdCompItem, kColLineItemDesc, gsBuildString(kSODescription, moAppDB, moSysSession)
    gGridSetColumnWidth grdCompItem, kColLineItemDesc, 16
    gGridLockColumn grdCompItem, kColLineItemDesc
    
    'Set up the Stock UOM column & lock it
    gGridSetHeader grdCompItem, kColLineShipUOM, gsBuildString(kSOUOM, moAppDB, moSysSession)
    gGridSetColumnWidth grdCompItem, kColLineShipUOM, 6
    gGridLockColumn grdCompItem, kColLineShipUOM
    
    'Set up the Quantity column.  Note: Number of decimal places allowed will vary according to timItem.AllowDecimalQty value.
    gGridSetHeader grdCompItem, kColLineCompItemQty, gsBuildString(kIMQtyPerKit, moAppDB, moClass.moSysSession)
    gGridSetColumnType grdCompItem, kColLineCompItemQty, SS_CELL_TYPE_FLOAT, moProcessPickList.iNbrDecPlaces, 7 'last two args are dec & integral places
    gGridSetColumnWidth grdCompItem, kColLineCompItemQty, 6
    gGridLockColumn grdCompItem, kColLineCompItemQty
    
    'Set up the Total Qty Required column & lock it
    gGridSetHeader grdCompItem, kColLineQtyPicked, gsBuildString(kSOTotalQtyReqd, moAppDB, moClass.moSysSession)
    gGridSetColumnType grdCompItem, kColLineQtyPicked, SS_CELL_TYPE_FLOAT, moProcessPickList.iNbrDecPlaces, 7 'last two args are dec & integral places
    gGridSetColumnWidth grdCompItem, kColLineQtyPicked, 12
    gGridLockColumn grdCompItem, kColLineQtyPicked
    
    'Set up the Total Qty Picked column & lock it
    gGridSetHeader grdCompItem, kColLineQtyDist, gsBuildString(kSOTotalQtyPicked, moAppDB, moClass.moSysSession)
    gGridSetColumnType grdCompItem, kColLineQtyDist, SS_CELL_TYPE_FLOAT, moProcessPickList.iNbrDecPlaces, 7  'last two args are dec & integral place
    gGridSetColumnWidth grdCompItem, kColLineQtyDist, 12
    gGridLockColumn grdCompItem, kColLineQtyDist
    
    'Set up the Bin column
    gGridSetHeader grdCompItem, kColLineAutoDistBinID, gsBuildString(kSOBin, moAppDB, moClass.moSysSession)
    gGridSetColumnType grdCompItem, kColLineAutoDistBinID, SS_CELL_TYPE_EDIT
    gGridSetColumnWidth grdCompItem, kColLineAutoDistBinID, 10

    'Set up Qty Available column
    gGridSetHeader grdCompItem, kColLineQtyAvail, msAvailToShip
    gGridSetColumnType grdCompItem, kColLineQtyAvail, SS_CELL_TYPE_EDIT
    gGridSetColumnWidth grdCompItem, kColLineQtyAvail, 10
    gGridLockColumn grdCompItem, kColLineQtyAvail

    'Hide columns
    gGridHideColumn grdCompItem, kColLineAllowDecimalQty
    gGridHideColumn grdCompItem, kColLineAllowImmedShipFromPick
    gGridHideColumn grdCompItem, kColLineAllowSubItem
    gGridHideColumn grdCompItem, kColLineAutoDist
    gGridHideColumn grdCompItem, kColLineAutoDistBinKey
    gGridHideColumn grdCompItem, kColLineAutoDistBinQtyAvail
    gGridHideColumn grdCompItem, kColLineAutoDistBinUOMKey
    gGridHideColumn grdCompItem, kColLineAutoDistLotExpDate
    gGridHideColumn grdCompItem, kColLineAutoDistLotKey
    gGridHideColumn grdCompItem, kColLineAutoDistLotNo
    gGridHideColumn grdCompItem, kColLineCrHold
    gGridHideColumn grdCompItem, kColLineCurrDisplayRow
    gGridHideColumn grdCompItem, kColLineCustReqShipComplete
    gGridHideColumn grdCompItem, kColLineDeliveryMethod
    gGridHideColumn grdCompItem, kColLineDeliveryMethodID
    gGridHideColumn grdCompItem, kColLineDistComplete
    gGridHideColumn grdCompItem, kColLineExtShipmentExists
    gGridHideColumn grdCompItem, kColLineInvtTranID
    gGridHideColumn grdCompItem, kColLineInvtTranKey
    gGridHideColumn grdCompItem, kColLineItemKey
    gGridHideColumn grdCompItem, kColLineItemReqShipComplete
    gGridHideColumn grdCompItem, kColLineItemType
    gGridHideColumn grdCompItem, kColLineKitShipLineKey
    gGridHideColumn grdCompItem, kColLineLine
    gGridHideColumn grdCompItem, kColLineLineHold
    gGridHideColumn grdCompItem, kColLineLineHoldReason
    gGridHideColumn grdCompItem, kColLineOrderLineUOMID
    gGridHideColumn grdCompItem, kColLineOrderLineUOMKey
    gGridHideColumn grdCompItem, kColLineOrderNo
    gGridHideColumn grdCompItem, kColLineOrdHold
    gGridHideColumn grdCompItem, kColLinePacked
    gGridHideColumn grdCompItem, kColLinePickCheck
    gGridHideColumn grdCompItem, kColLineQtyOrdered
    gGridHideColumn grdCompItem, kColLineQtyShort
    gGridHideColumn grdCompItem, kColLineQtyToPick
    gGridHideColumn grdCompItem, kColLineQtyToPickByOrdUOM
    gGridHideColumn grdCompItem, kColLineRcvgWarehouse
    gGridHideColumn grdCompItem, kColLineSASequence
    gGridHideColumn grdCompItem, kColLineShipDate
    gGridHideColumn grdCompItem, kColLineShipKey
    gGridHideColumn grdCompItem, kColLineShipLineDistKey
    gGridHideColumn grdCompItem, kColLineShipLineDistUpdateCounter
    gGridHideColumn grdCompItem, kColLineShipLineKey
    gGridHideColumn grdCompItem, kColLineShipLineUpdateCounter
    gGridHideColumn grdCompItem, kColLineShipmentCommitStatus
    gGridHideColumn grdCompItem, kColLineShipPriority
    gGridHideColumn grdCompItem, kColLineShiptoAddr
    gGridHideColumn grdCompItem, kColLineShiptoAddrID
    gGridHideColumn grdCompItem, kColLineShiptoName
    gGridHideColumn grdCompItem, kColLineShipUOMKey
    gGridHideColumn grdCompItem, kColLineShipVia
    gGridHideColumn grdCompItem, kColLineShipWhseID
    gGridHideColumn grdCompItem, kColLineShipWhseKey
    gGridHideColumn grdCompItem, kColLineShortPickFlag
    gGridHideColumn grdCompItem, kColLineStatus
    gGridHideColumn grdCompItem, kColLineStatusCustShipCompleteViolation
    gGridHideColumn grdCompItem, kColLineStatusDistQtyShort
    gGridHideColumn grdCompItem, kColLineStatusDistWarning
    gGridHideColumn grdCompItem, kColLineStatusItemShipCompLeteViolation
    gGridHideColumn grdCompItem, kColLineStatusOverShipment
    gGridHideColumn grdCompItem, kColLineSubFlag
    gGridHideColumn grdCompItem, kColLineSubItem
    gGridHideColumn grdCompItem, kColLineSubItemKey
    gGridHideColumn grdCompItem, kColLineTrackMeth
    gGridHideColumn grdCompItem, kColLineTrackQtyAtBin
    gGridHideColumn grdCompItem, kColLineTranKey
    gGridHideColumn grdCompItem, kColLineTranLineDistKey
    gGridHideColumn grdCompItem, kColLineTranLineKey
    gGridHideColumn grdCompItem, kColLineTranType
    gGridHideColumn grdCompItem, kColLineTranTypeText
    gGridHideColumn grdCompItem, kColLineWhseUseBin
    gGridHideColumn grdCompItem, kColLineShiptoAddrKey
    gGridHideColumn grdCompItem, kColLineShipMethKey
    gGridHideColumn grdCompItem, kColLineShipFOBKey
    
    'Hide warehouse column if IM is not interated
    If Not (mbIMIntegrated) Then
         'gGridHideColumn grdCompItem, kColLineShipWhseID
    End If
    
    'gGridSetMaxRows grdCompItem, 0
   

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetUpCompGrid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


'************************************************************************
'   Description:
'       create context menu object and bind controls to it
'
'   Param:
'       <none>
'
'   Returns:
'
'************************************************************************

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moContextMenu = New clsContextMenu      'Instantiate Context Menu Class

   With moContextMenu
        .BindGrid moGMKitComp, grdCompItem.hWnd 'define standard grid context menu (add/delete line)
        .Bind "*APPEND", grdCompItem.hWnd       'instructs context menu mgr to call CMAppendContextMenu for the grid
        .Bind "IMDA06", txtItemID.hWnd, kEntTypeIMItem
        Set .Form = frmCompItem
        .Init                                   'Init will set properties of Winhook control
    End With
    

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Private Sub cmdDist_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**********************************************************************
' Description:
'    This routine Loads the Distribution Form (imzde001)for user to
'    view or modify the distribution on a Shipline.
'
'**********************************************************************
On Error GoTo ExpectedErrorRoutine
Dim lRow            As Long
    
    'Get the active row
    lRow = glGridGetActiveRow(grdCompItem)
    
    'Store the Current cell value in the variable
    mlCurrentLineActiveRow = lRow
    mlCurrentRowShipLineKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColLineShipLineKey))

    SetMouse
    
    If Not moItemDist Is Nothing Then
        If moProcessPickList.bIsValidQtyToPick(Me, moDMCompGrid, grdCompItem, lRow) Then
        'If the QtyToPick is valid, load the distribution form
        'so use can enter new distribution or view and update exist distribution
            If Not moProcessPickList.bCreateManualDistribution(Me, grdCompItem, moDMCompGrid, lRow) Then
                grdCompItem_Click kColLineQtyToPick, lRow
                grdCompItem.SetFocus
            End If
        Else
        'QtyToPick is invalid, set the focus back the QtyToPick cell
            grdCompItem_Click kColLineQtyToPick, lRow
            grdCompItem.SetFocus
            Exit Sub
        End If
    End If
    
    mbIsDirty = True
    
    'Highlight the row where the user has selected before click the Dist button
    If mlCurrentLineActiveRow > 0 Then
        gGridSetSelectRow grdCompItem, mlCurrentLineActiveRow
    End If
    
    ResetMouse
    
    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdDist_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub



Private Sub Form_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Select Case keycode
        Case vbKeyF1 To vbKeyF16
'            If Shift = 0 And keycode = vbKeyF5 And grdCompItem.ActiveCol = kcollineitem And _
'(mbAllowAllMod Or mbAllowDecreaseOnly) Then
'                Call navGridKit_Click
'            Else
'                gProcessFKeys Me, keycode, Shift
'            End If
    End Select

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyPress traps the keys pressed when the KeyPreview
'         property of the form is set to True process the enter key
'         as if the user pressed the tab key.
'********************************************************************

    Select Case KeyAscii
        Case vbKeyReturn            'User pressed enter key
            If mbEnterAsTab Then    'enter as tab set in session
                DoEvents
                SendKeys "{Tab}"    'send tab key
                KeyAscii = 0        'cancel enter key
            End If
        Case Else
            'Other KeyAscii routines.
    End Select

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_Load()

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Save the form size variable to use for resize later
    miOldFormHeight = Me.Height
    miMinFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormWidth = Me.Width
    
    'Set up form level variables
    Set moSysSession = moClass.moSysSession
    With moSysSession
        msCompanyID = .CompanyId
        mbIMActivated = .IsModuleActivated(kModuleIM)
        mlLanguage = .Language
        mbEnterAsTab = .EnterAsTab
    End With
    
    'Set up toolbar
    tbrMain.LocaleID = moClass.moSysSession.Language
    tbrMain.RemoveButton kTbPrint
    
    'Set up grid
    SetUpCompGrid
    
    'Bind comp grid
    BindCompGrid
    
    'Bind the grid manager
    Set moGMKitComp = New clsGridMgr
    With moGMKitComp
        Set .Grid = grdCompItem
        Set .DM = moDMCompGrid
        Set .Form = frmCompItem
        .GridType = kGridDataSheetNoAppend
        .GridSortEnabled = True
        Set moGridLkuAutoDistBin = .BindColumn(kColLineAutoDistBinID, lkuAutoDistBin)
        Set moGridLkuAutoDistBin.ReturnControl = txtlkuAutoDistBin
        .Init
    End With
            
    BindToolbar

    BindContextMenu
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
'************************************************************************
'   Description:
'       Create the toolbar.
'   Param:
'       <none>
'   Returns:
'************************************************************************
Private Sub BindToolbar()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Set up the toolbar
    tbrMain.Build sotaTB_SINGLE_ROW
    tbrMain.RemoveButton kTbPrint
    tbrMain.RemoveButton kTbCancelExit
    
    With moClass.moFramework
        tbrMain.SecurityLevel = .GetTaskPermission(.GetTaskID())
        tbrMain.LocaleID = mlLanguage
    End With

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindToolbar", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub BindCompGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moDMCompGrid = New clsDmGrid
    With moDMCompGrid
        Set .Session = moSysSession
        Set .Database = moAppDB
        Set .Form = frmCompItem
        Set .Grid = grdCompItem
        .Table = "#tsoCreatePickWrk2"
        .UniqueKey = "ShipLineKey"
        .NoAppend = True
        .OrderBy = "ItemID"
        .Where = "CurrDisplayRow = 1 And KitShipLineKey IS NOT NULL"
        
        .BindColumn "AllowDecimalQty", kColLineAllowDecimalQty, SQL_SMALLINT
        .BindColumn "CompItemQty", kColLineCompItemQty, SQL_DECIMAL
        .BindColumn "CurrDisplayRow", kColLineCurrDisplayRow, SQL_INTEGER
        .BindColumn "CustReqShipComplete", kColLineCustReqShipComplete, SQL_SMALLINT
        .BindColumn "InvtLotNo", kColLineAutoDistLotNo, SQL_VARCHAR
        .BindColumn "InvtLotKey", kColLineAutoDistLotKey, SQL_INTEGER
        .BindColumn "InvtTranLinkKey", kColLineInvtTranID, SQL_INTEGER
        .BindColumn "InvtTranKey", kColLineInvtTranKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "IsDistComplete", kColLineDistComplete, SQL_SMALLINT
        .BindColumn "ItemDesc", kColLineItemDesc, SQL_VARCHAR
        .BindColumn "ItemID", kColLineItem, SQL_VARCHAR
        .BindColumn "ItemKey", kColLineItemKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ItemReqShipComplete", kColLineItemReqShipComplete, SQL_SMALLINT
        .BindColumn "ItemType", kColLineItemType, SQL_SMALLINT
        .BindColumn "KitShipLineKey", kColLineKitShipLineKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "OrderLineUnitMeasKey", kColLineOrderLineUOMKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "QtyAvail", kColLineQtyAvail, SQL_DECIMAL
        .BindColumn "QtyDist", kColLineQtyDist, SQL_DECIMAL
        .BindColumn "QtyPicked", kColLineQtyPicked, SQL_DECIMAL
        .BindColumn "QtyToPick", kColLineQtyToPick, SQL_DECIMAL
        .BindColumn "ShipLineKey", kColLineShipLineKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipLineUpdateCounter", kColLineShipLineUpdateCounter, SQL_INTEGER
        .BindColumn "ShipLineDistUpdateCounter", kColLineShipLineDistUpdateCounter, SQL_INTEGER
        .BindColumn "ShipUnitMeasID", kColLineShipUOM, SQL_VARCHAR
        .BindColumn "ShipUnitMeasKey", kColLineShipUOMKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "Status", kColLineStatus, SQL_VARCHAR
        .BindColumn "StatusCustShipCompleteViolation", kColLineStatusCustShipCompleteViolation, SQL_VARCHAR
        .BindColumn "StatusItemShipCompLeteViolation", kColLineStatusItemShipCompLeteViolation, SQL_VARCHAR
        .BindColumn "StatusDistWarning", kColLineStatusDistWarning, SQL_VARCHAR
        .BindColumn "StatusDistQtyShort", kColLineStatusDistQtyShort, SQL_VARCHAR
        .BindColumn "StatusOverShipment", kColLineStatusOverShipment, SQL_VARCHAR
        .BindColumn "TrackMeth", kColLineTrackMeth, SQL_SMALLINT
        .BindColumn "TrackQtyAtBin", kColLineTrackQtyAtBin, SQL_SMALLINT
        .BindColumn "TranType", kColLineTranType, SQL_INTEGER
        .BindColumn "ShipWhseKey", kColLineShipWhseKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "WhseBinID", kColLineAutoDistBinID, SQL_VARCHAR
        .BindColumn "WhseBinKey", kColLineAutoDistBinKey, SQL_INTEGER
        .BindColumn "WhseUseBin", kColLineWhseUseBin, SQL_SMALLINT
      
        .Init
    End With
    
    gbLookupInit lkuAutoDistBin, moClass, moAppDB, "DistBin"
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindCompGrid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If UnloadMode <> vbFormCode Then
        HandleToolbarClick (kTbFinishExit)
        Cancel = True
    Else
        PerformCleanShutDown
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub ProcessCancel()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Me.Hide
      
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ProcessCancel", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim i As Integer

    '-- Unload all forms loaded from this main form
    gUnloadChildForms Me
         
    '-- Remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
        
    If Not moContextMenu Is Nothing Then
        Set moContextMenu = Nothing
    End If
    
    If Not moItemDist Is Nothing Then
        moItemDist.Terminate
        Set moItemDist = Nothing
    End If
    
    If Not moGMKitComp Is Nothing Then
        moGMKitComp.UnloadSelf
        Set moGMKitComp = Nothing
    End If
    
    If Not moDMCompGrid Is Nothing Then
        moDMCompGrid.UnloadSelf
        Set moDMCompGrid = Nothing
    End If
    
    If Not moGridLkuAutoDistBin Is Nothing Then
        Set moGridLkuAutoDistBin = Nothing
    End If
    
    If Not moAppDB Is Nothing Then
        Set moAppDB = Nothing
    End If
   
    If Not moSysSession Is Nothing Then
        Set moSysSession = Nothing
    End If
    
    If Not moProcessPickList Is Nothing Then
        Set moProcessPickList = Nothing
    End If
       
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++


    'resize the form height and height all ctls on the screen (form height doesn't
    'get resize smaller than its original height)
    gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, grdCompItem
     
    'resize the form Width and Width all ctls on the screen (form Width doesn't
    'get resize smaller than its original Width)
    gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, grdCompItem
    
    

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub


Private Sub grdCompItem_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
       
    'moGMKitComp.Grid_Change Col, Row
    moGMKitComp_CellChange Row, Col
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdCompItem_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdCompItem_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Row > 0 Then
        'Enable the distribution button if needed.
        cmdDist.Enabled = bEnableCmdDistribution(Row)
    Else
        gGridSortByRow grdCompItem, Col
    End If
    
    'If the user click on any locked cell, highlight the grid row the cell is in
    With grdCompItem
        .Row = Row
        .Col = Col
        If .Lock = True Then
            gGridSetSelectRow grdCompItem, Row
        End If
    End With

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdCompItem_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdCompItem_EditChange(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'User changed the value on the cell.  Set the mbCellChangeFired to false.  It will be set to true
    'once the event fires.  However, if it doesn't fire, we can use this flag to tell if we have to
    'call the GM CellChange manually.
    mbCellChangeFired = False

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdCompItem_EditChange", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdCompItem_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    moGMKitComp.Grid_EditMode Col, Row, Mode, ChangeMade
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdCompItem_EditMode", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdCompItem_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moGMKitComp.Scroll

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdCompItem_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub grdCompItem_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Cancel = Not moGMKitComp.Grid_LeaveCell(Col, Row, NewCol, NewRow)

    'Enable the distribution button if needed.
    If Me.ActiveControl.Name = grdCompItem.Name Then
        cmdDist.Enabled = bEnableCmdDistribution(NewRow)
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdCompItem_LeaveCell", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Public Sub LoadCompForm(lKitItemKey As Long, sKitItemID As String, lKitShipLineKey As Long, _
                    lKitUOMKey As Long, sKitUOM As String, dKitQty As Double, dQtyOrdered As Double, lShipWhseKey As Long, _
                    oDist As Object, lTaskID As Long, bShipmentGenerated As Boolean, _
                        iEmptyBins As Integer, iEmptyRandomBins As Integer, iOverShipmentPolicy As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'Stores the Info for the BTOKit parent
    gbFormIsDirty = False
    mbIsDirty = False
    mlKitItemKey = lKitItemKey
    mlKitShipLineKey = lKitShipLineKey
    mlShipWhseKey = lShipWhseKey
    mlTaskID = lTaskID
    mbShipmentGenerated = bShipmentGenerated
    miEmptyBins = iEmptyBins
    miEmptyRandomBins = iEmptyRandomBins
    miOverShipmentPolicy = iOverShipmentPolicy
    mdQtyOrdered = dQtyOrdered
    
    'Set up the distribution objects
    Set moItemDist = oDist
    
    'Setup the Module Options Class.
    If moOptions Is Nothing Then
        Set moOptions = New clsModuleOptions
        Set moOptions.oAppDb = moAppDB
        Set moOptions.oSysSession = moClass.moSysSession
        moOptions.sCompanyID = moClass.moSysSession.CompanyId
    End If
    
    'Load the value for the BTOKit parent
    txtItemID.Text = sKitItemID
    txtItemDesc.Text = gsGetValidStr(moAppDB.Lookup("ShortDesc", "timItemDescription", "ItemKey=" & mlKitItemKey))
    txtKitUOM.Text = sKitUOM
    nbrKitQty.Value = dKitQty
    nbrKitQty.Tag = nbrKitQty.Value
      
    'Load the component grid
    grdCompItem.ReDraw = False
    moDMCompGrid.Refresh
    grdCompItem.ReDraw = True
        
    'Set the row to the next likely row that will be distributed.
    If grdCompItem.DataRowCnt > 0 Then
        gGridSetActiveCell grdCompItem, lGetNextRowToDistribute(1), kColLineQtyToPick
        If cmdDist.Enabled = True And cmdDist.Visible = True Then
            cmdDist.SetFocus
        End If
    End If

    Me.Show vbModal
   
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LoadComp", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    If Not moGMKitComp Is Nothing Then
        moGMKitComp.UnloadSelf
        Set moGMKitComp = Nothing
    End If

    Set moContextMenu = Nothing
    Set moOptions = Nothing
    

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub moDMCompGrid_DMGridRowLoaded(lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim iTrackQtyAtBin As Integer
    Dim iTrackMeth As Integer
    Dim lItemKey As Long
    Dim iItemType As Integer
    Dim lInvtTranKey As Long
    Dim lSubItemKey As Long
    Dim iAllowDecimalQty As Integer
    Dim dDMQtyPicked As Double
    Dim lShipLineKey As Long

    iAllowDecimalQty = giGetValidInt(gsGridReadCell(grdCompItem, lRow, kColLineAllowDecimalQty))
    iItemType = giGetValidInt(gsGridReadCell(grdCompItem, lRow, kColLineItemType))
    dDMQtyPicked = gdGetValidDbl(gsGridReadCell(grdCompItem, lRow, kColLineQtyPicked))

    'Format the Qty Columns
    moProcessPickList.SetGridNumericAttr grdCompItem, kColLineQtyPicked, moProcessPickList.iNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow
    moProcessPickList.SetGridNumericAttr grdCompItem, kColLineQtyDist, moProcessPickList.iNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow
    moProcessPickList.SetGridNumericAttr grdCompItem, kColLineQtyAvail, moProcessPickList.iNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow
   
    'Update the Display QtyPicked columns
    gGridUpdateCellText grdCompItem, lRow, kColLineQtyPicked, CStr(dDMQtyPicked)

    'Enable the auto-dist bin column if it can auto distributed from the bin lookup.
    'Lock it first by default.
    gGridLockCell grdCompItem, kColLineAutoDistBinID, lRow
    'Unlock it if the conditions are correct.
    lItemKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColLineItemKey))
    iTrackQtyAtBin = giGetValidInt(gsGridReadCell(grdCompItem, lRow, kColLineTrackQtyAtBin))
    iTrackMeth = giGetValidInt(gsGridReadCell(grdCompItem, lRow, kColLineTrackMeth))
    iItemType = giGetValidInt(gsGridReadCell(grdCompItem, lRow, kColLineItemType))
    lShipLineKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColLineShipLineKey))
    
    If iItemType = kFinishedGood Or iItemType = kPreAssembledKit Or iItemType = kRawMaterial Then
        If iTrackQtyAtBin = 1 And (iTrackMeth = kTM_None Or iTrackMeth = kTM_Lot) And mbIMIntegrated Then
            gGridUnlockCell grdCompItem, kColLineAutoDistBinID, lRow
        
'           'Get the bin info from the saved distributions.
'           lInvtTranKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColLineInvtTranKey))
'           If lInvtTranKey <> 0 Then
'                lSubItemKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColLineSubItemKey))
'                If lSubItemKey <> 0 Then
'                    lItemKey = lSubItemKey
'                End If
'
'
'                If Not moItemDist Is Nothing Then
'                    moProcessPickList.UpdateAutoDistBinCols grdCompItem, lRow, lItemKey, lShipLineKey, lInvtTranKey
'                End If
'           End If
        End If
    End If
    
    'Set the active cell
    If mlCurrentRowShipLineKey > 0 Then
        If lShipLineKey = mlCurrentRowShipLineKey Then
            mlCurrentLineActiveRow = lRow
        End If
    End If
    
    bEnableCmdDistribution lRow
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDMCompGrid_DMGridRowLoaded", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub moGMKitComp_CellChange(ByVal lRow As Long, ByVal lCol As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    On Error GoTo ExpectedErrorRoutine
    If (lCol <> kColLineAutoDistBinID) Then
        Exit Sub
    End If

    mbCellChangeFired = True
    
    SetMouse
    
    If Not moProcessPickList.bProcessGridChange(Me, moDMCompGrid, grdCompItem, lRow, lCol) Then
        ResetMouse
        grdCompItem_Click lCol, lRow
        Exit Sub
    End If
    
    ResetMouse
        Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse

'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moGMKitComp_CellChange", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub moGMKitComp_LeaveGridRow(ByVal lLeaveRow As Long, ByVal lEnterRow As Long, ByVal eReason As GridMgrExt.LeaveGridRowReasons, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
'    If bUpdateOrderLineStatus(moDMCompGrid, grdCompItem, lLeaveRow) Then
'        moDMCompGrid.SetRowDirty lLeaveRow
'        moDMCompGrid.Save
'    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moGMKitComp_LeaveGridRow", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


Private Sub nbrKitQty_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim lRowCount As Long
Dim dQtyPerKit As Double
Dim dQtyPicked As Double
Dim dQtyDist As Double
Dim dNewQtyPicked As Double
Dim bResult As Boolean
Dim sWhere As String
Dim sSQL As String
Dim lInvtTranKey As String
Dim iInvtTranKeyCount As Integer
Dim iItemType As Integer
Dim iTrackMeth As Integer
Dim iTrackQtyAtBin As Integer
Dim lShipLineKey As Long
Dim iUpdateCounterViolation As Integer

    'If there is no change of the kit qty, exit
    If nbrKitQty.Value = nbrKitQty.Tag Then Exit Sub

    'Negative Qty is not allowed
    If nbrKitQty.Value < 0 Then
        nbrKitQty.Value = nbrKitQty.Tag
        nbrKitQty.SetFocus
        Exit Sub
    End If
    
    If nbrKitQty.Value > mdQtyOrdered Then
        Select Case miOverShipmentPolicy
            Case kAllow
    
            Case kDisAllow
                giSotaMsgBox Me, moClass.moSysSession, kSOmsgQtyShipCantExceedQtyOpen
                nbrKitQty.Value = nbrKitQty.Tag
                nbrKitQty.SetFocus
                Exit Sub
            Case kAllowWithWarning
                giSotaMsgBox Me, moClass.moSysSession, kSOmsgQtyShiipExceedQtyOpen
        End Select
    End If
    
    If nbrKitQty.Value = 0 Then
        
        'Call the DeleteShipLines function to delete the IM distribution in the real tables
        'and udpate tsoShipLine, tsoShipLineDist, and the bound
        'temp tables.
        sWhere = " wrk.ShipLineKey = " & mlKitShipLineKey
        bResult = moProcessPickList.bDeleteShipLines(sWhere, kDeleteDistributionOnlyYes, iUpdateCounterViolation)
        
        If Not bResult Then
            Exit Sub
        End If
    Else
        'Loop through the component grid to update the  new QtyToPick value
        For lRowCount = 1 To grdCompItem.MaxRows
        
            'Recalculate the new QtyToPick for each Kit line
            dQtyPerKit = gdGetValidDbl(gsGridReadCell(grdCompItem, lRowCount, kColLineCompItemQty))
            dNewQtyPicked = dQtyPerKit * nbrKitQty.Value
            
            'Update the total qty required field
            gGridUpdateCellText grdCompItem, lRowCount, kColLineQtyPicked, CStr(dNewQtyPicked)
            
            moDMCompGrid.SetRowDirty lRowCount
        Next lRowCount
        
        moDMCompGrid.Save
    
    
        'Set the mbAutoPickSetValue to true since user manually overwrite the quantity
        mbAutoPickSetValue = True
    
        sWhere = "ShipLineKey = " & mlKitShipLineKey
    
        'Update temp table for the new Qty
        sSQL = "UPDATE #tsoCreatePickWrk2 SET QtyPicked = " & nbrKitQty.Value
        sSQL = sSQL & " WHERE ShipLineKey = " & mlKitShipLineKey
        
        moAppDB.ExecuteSQL sSQL
        
        If Len(Trim(sWhere)) > 0 Then
        
            bResult = moProcessPickList.bCreateAutoDistribution(sWhere, miEmptyBins, miEmptyRandomBins, iUpdateCounterViolation, kBTOKit)
            
            If Not bResult Then
                Exit Sub
            End If
        End If
    End If
    
    grdCompItem.ReDraw = False
    moDMCompGrid.Refresh
    grdCompItem.ReDraw = True
    
    mbIsDirty = True
    
    nbrKitQty.Tag = nbrKitQty.Value

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "nbrKitQty_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++

End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    HandleToolbarClick Button


'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim sSQL                As String
    Dim lRowCount           As Long
  
    Select Case sKey
        
        Case kTbFinishExit
        
            nbrKitQty_LostFocus
               
            'Update Order Header for Status
            For lRowCount = 1 To grdCompItem.MaxRows
                'Valid the grid data
                If Not bIsValidGridData(lRowCount) Then
                    Exit Sub
                End If
                'Update the grid status
                If moProcessPickList.bUpdateOrderLineStatus(moDMCompGrid, grdCompItem, lRowCount) Then
                    moDMCompGrid.SetRowDirty lRowCount
                End If
            Next lRowCount
            
            moDMCompGrid.Save
            
            'Update the status for the BTO Kit in the worktable
            sSQL = "UPDATE #tsoCreatePickWrk2 SET Status = tmp.Status, "
            sSQL = sSQL & " StatusCustShipCompleteViolation = tmp.StatusCustShipCompleteViolation,"
            sSQL = sSQL & " StatusDistWarning = tmp.StatusDistWarning,"
            sSQL = sSQL & " StatusItemShipCompLeteViolation = tmp.StatusItemShipCompLeteViolation,"
            sSQL = sSQL & " StatusDistQtyShort = tmp.StatusDistQtyShort,"
            sSQL = sSQL & " StatusOverShipment = tmp.StatusOverShipment"
            sSQL = sSQL & " FROM #tsoCreatePickWrk2 JOIN "
            sSQL = sSQL & " (SELECT MAX(COALESCE(Status, '')) Status, "
            sSQL = sSQL & "  MAX(COALESCE(StatusCustShipCompleteViolation, '')) StatusCustShipCompleteViolation, "
            sSQL = sSQL & "  MAX(COALESCE(StatusDistWarning, '')) StatusDistWarning, "
            sSQL = sSQL & "  MAX(COALESCE(StatusItemShipCompLeteViolation, '')) StatusItemShipCompLeteViolation, "
            sSQL = sSQL & "  MAX(COALESCE(StatusDistQtyShort, '')) StatusDistQtyShort, "
            sSQL = sSQL & "  MAX(COALESCE(StatusOverShipment, '')) StatusOverShipment, "
            sSQL = sSQL & " KitShipLineKey "
            sSQL = sSQL & " FROM #tsoCreatePickWrk2"
            sSQL = sSQL & " WHERE KitShipLineKey IS NOT NULL GROUP BY KitShipLineKey ) tmp"
            sSQL = sSQL & " ON #tsoCreatePickWrk2.ShipLineKey = tmp.KitShipLineKey "
            sSQL = sSQL & " WHERE #tsoCreatePickWrk2.ShipLineKey = " & mlKitShipLineKey
        
            moAppDB.ExecuteSQL sSQL
            
            Me.Hide

        Case kTbCancelExit
            Me.Hide
            
        Case kTbHelp
            gDisplayFormLevelHelp Me
            'Exit Sub
            
    End Select

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oClass = goClass

'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
Public Property Get MyApp() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set MyApp = App

'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MyApp", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
Public Property Get MyForms() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set MyForms = Forms

'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MyForms", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Private Function lGetNextRowToDistribute(ByVal lRow As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'This routine will check if the Lot/Serial/Bin has been cancelled.
    'If not, it will find the next row to distribute by checking which
    'row does not have a DistQty that equals the Qty to Distribute.
    
    Dim dQtyPicked As Double
    Dim dTranQty As Double
    Dim lStartingRow As Long

    'Default to the first row.
    lGetNextRowToDistribute = 1

    If moItemDist.CancelClicked = True Then
        'Just return the same row.
        lGetNextRowToDistribute = lRow
    Else
        'Remember the row we are starting from.
        lStartingRow = lRow

        'If the row is somehow set on the last row (non data row)
        'or the 0 Row, set the row back to 1.
        If lRow > grdCompItem.DataRowCnt Or lRow = 0 Then
            'You are in the last data row.  Set the lRow to start from
            'the top and set the flag to true so we know we've started over.
            lRow = 1
        End If

        'Get the Tran and Distributed quantities.
        dTranQty = gdGetValidDbl(gsGridReadCell(grdCompItem, lRow, kColLineQtyToPick))
        dQtyPicked = gdGetValidDbl(gsGridReadCell(grdCompItem, lRow, kColLineQtyPicked))

        'Seach for the next line that has not been fully distributed
        'and return that line.
        Do While dQtyPicked = dTranQty

            'Get the next row.
            lRow = lRow + 1

            'Check if the current row is data row.
            If lRow > grdCompItem.DataRowCnt Then
                'You have just completed checking the last data row.  Set the lRow
                'to start from the top if we did not start on the first row.
                If lStartingRow > 1 Then
                    lRow = 1
                    'Set the lStartingRow to 1 so we don't get in an endless loop.
                    lStartingRow = 1
                Else
                    lRow = 1
                    Exit Do
                End If
            End If

            'Get the Tran and Dist quantities off the grid.
            dTranQty = gsGridReadCell(grdCompItem, lRow, kColLineQtyToPick)
            dQtyPicked = gsGridReadCell(grdCompItem, lRow, kColLineQtyPicked)
        Loop

        lGetNextRowToDistribute = lRow
    End If
    'Enable the distribution button if needed.
    cmdDist.Enabled = bEnableCmdDistribution(lGetNextRowToDistribute)
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lGetNextRowToDistribute", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Function bEnableCmdDistribution(lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim iCompTrackMeth As Integer
    Dim iCompItemType As Integer
    Dim lItemKey As Long
    Dim iTrackQtyAtBin As Integer
    
    DoEvents

    bEnableCmdDistribution = False
    
    iCompTrackMeth = giGetValidInt(gsGridReadCell(grdCompItem, lRow, kColLineTrackMeth))
    iCompItemType = giGetValidInt(gsGridReadCell(grdCompItem, lRow, kColLineItemType))
    lItemKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColLineItemKey))
    iTrackQtyAtBin = giGetValidInt(gsGridReadCell(grdCompItem, lRow, kColLineTrackQtyAtBin))
    
    If lItemKey = 0 Or Not mbIMIntegrated Then
        bEnableCmdDistribution = False
    Else
        Select Case iCompItemType
            Case IMS_MISC_ITEM
                bEnableCmdDistribution = False
            Case IMS_FINISHED_GOOD, IMS_RAW_MATERIAL, IMS_ASSEMBLED_KIT
                If iTrackQtyAtBin = 0 And iCompTrackMeth = 0 Then
                    bEnableCmdDistribution = False
                Else
                    bEnableCmdDistribution = True
                End If
        End Select
    End If

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bEnableCmdDistribution", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
 Private Sub grdCompItem_ColWidthChange(ByVal Col1 As Long, ByVal Col2 As Long)
    moGMKitComp.Grid_ColWidthChange Col1
End Sub
 Private Sub grdCompItem_KeyDown(keycode As Integer, Shift As Integer)
    moGMKitComp.Grid_KeyDown keycode, Shift
End Sub
 Private Sub grdCompItem_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
    moGMKitComp.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop
End Sub

Public Property Get oAppDb() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oAppDb = moAppDB
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oAppDB", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Property Set oAppDb(oNewAppDB As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moAppDB = oNewAppDB

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oAppDB", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Private Sub SetMouse()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'    Desc: Sets Mouse to a Busy Value.
'   Parms: N/A
' Returns: N/A
'***********************************************************************
    If Me.MousePointer <> vbHourglass Then
        Me.MousePointer = vbHourglass
    End If
  
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetMouse", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub ResetMouse()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'    Desc: Resets Mouse to Previous Value.
'   Parms: N/A
' Returns: N/A
'***********************************************************************
    Me.MousePointer = miMousePointer

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ResetMouse", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub



Private Function bIsValidGridData(Optional ByVal lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
 
Dim dQtyPicked          As Double
Dim dQtyToPick          As Double
Dim dQtyDist            As Double
Dim lRowCount           As Long
Dim iItemType           As Integer

    bIsValidGridData = False ' assume all data is invalid
    
    If grdCompItem.MaxRows = 0 Then
        bIsValidGridData = True
        Exit Function
    End If
    
    iItemType = giGetValidInt(gsGridReadCell(grdCompItem, lRow, kColLineItemType))

    'Loop through Grid validating row by row
    If lRow > 0 And (iItemType = kFinishedGood Or iItemType = kRawMaterial Or iItemType = kPreAssembledKit) Then
        dQtyToPick = gdGetValidDbl(gsGridReadCell(grdCompItem, lRow, kColLineQtyToPick))
        dQtyPicked = gdGetValidDbl(gsGridReadCell(grdCompItem, lRow, kColLineQtyPicked))
        dQtyDist = gdGetValidDbl(gsGridReadCell(grdCompItem, lRow, kColLineQtyDist))

'        If dQtyPicked > dQtyDist Then
'            'The the shipline is under distributed, bring up the Distribution form for
'            'use to correct
'            giSotaMsgBox Me, moClass.moSysSession, kSOmsgDistQtyNotEqualToQtyPick
'            bIsValidGridData = True
'        End If
    
        If dQtyPicked < dQtyDist Then
            'The the shipline is over distributed, bring up the Distribution form for
            'use to correct
            giSotaMsgBox Me, moClass.moSysSession, kSOmsgQtyDistCantExceedQtyPick
            grdCompItem_Click kColLineQtyPicked, lRow
            gGridSetSelectRow grdCompItem, lRow
            Exit Function
        End If
    End If
    
    bIsValidGridData = True
    
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidGridData", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Sub lkuAutoDistBin_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lTranType As Long
    lTranType = glGetValidLong(gsGridReadCell(grdCompItem, grdCompItem.ActiveRow, kColLineTranType))
    If lTranType = kTranTypeSOSO Then
        lTranType = kTranTypeSOSH
    Else
        lTranType = kTranTypeSOTS
    End If
    
    'Initialize the Distribution object if not already initialized
    gGridSetActiveCell grdCompItem, grdCompItem.ActiveRow, kColLineAutoDistBinID
    moProcessPickList.ProcessAutoBinLkuClick Me, grdCompItem, moDMCompGrid, moGMKitComp, lkuAutoDistBin, lTranType
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuAutoDistBin_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub FireGMCellChangeIfNeeded()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'If the user clicks on the toolbar control immediately after typing a value on a
    'grid cell, sometimes the GM CellChange event does not fire.  Make sure the event
    'fires if it hasn't.  (Seems like the issue only applys for the toolbar control.
    'If any other control is clicked, the event still fires.)
    
    'The mbCellChangeFired variable is set to True when the event is called.
    'It is set to False on the Grid_EditChange event or when the user starts changes
    'the value on a cell.
    
    If mbCellChangeFired = False Then
        moGMKitComp_CellChange grdCompItem.ActiveRow, grdCompItem.ActiveCol
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FireGMCellChangeIfNeeded", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


