VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#105.0#0"; "sotatbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#45.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#47.0#0"; "sotacalendar.ocx"
Begin VB.Form frmLinesOnHold 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Lines On Hold"
   ClientHeight    =   4770
   ClientLeft      =   1170
   ClientTop       =   1440
   ClientWidth     =   10530
   HelpContextID   =   17774314
   Icon            =   "sozdd104.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4770
   ScaleWidth      =   10530
   Begin VB.Frame fraLinesOnHold 
      Caption         =   "0 Line On Hold"
      Height          =   3840
      Left            =   120
      TabIndex        =   12
      Top             =   480
      Width           =   10305
      Begin FPSpreadADO.fpSpread grdLineOnHold 
         Height          =   3375
         Left            =   120
         TabIndex        =   13
         Top             =   240
         WhatsThisHelpID =   17776616
         Width           =   10035
         _Version        =   524288
         _ExtentX        =   17701
         _ExtentY        =   5953
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         SpreadDesigner  =   "sozdd104.frx":23D2
         AppearanceStyle =   0
      End
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   11
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   5
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   6
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   0
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   10530
      _ExtentX        =   18574
      _ExtentY        =   741
      Style           =   0
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   4380
      WhatsThisHelpID =   73
      Width           =   10530
      _ExtentX        =   18574
      _ExtentY        =   688
      BrowseVisible   =   0   'False
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   10
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmLinesOnHold"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************************
'     Name: frmLinesOnHold
'     Desc: Pick Lines On Hold
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: JY 04-01/2004
'     Mods:
'************************************************************************************

Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

    Private mbUserSelectedItem As Boolean
    
    'Private Variables of Form Properties
    Private mbEnterAsTab                As Boolean
    Private moClass                     As Object
    Private moSysSession                As Object
    Private moAppDB                     As Object                   'Class Reference
    Private mbCancelShutDown            As Boolean                  'Cancel Shutdown Flag
    Private mbIsDirty                   As Boolean                  'Form dirty?
    Private msCompanyID                 As String
    Private msCurrencyID                As String
    Private mlLanguage                  As Long
    Private mbIMActivated               As Boolean
    Private WithEvents moDMLineGrid     As clsDmGrid
Attribute moDMLineGrid.VB_VarHelpID = -1
    Private WithEvents moGMLine         As clsGridMgr
Attribute moGMLine.VB_VarHelpID = -1
    Private mlRunMode                   As Long
    Private mbHoldReleaseFailed            As Boolean
    Private moItemDist                  As Object
    Private mlTaskID                    As Long
    Private mbShipmentGenerated         As Boolean
    Private miEmptyBins                 As Integer
    Private miEmptyRandomBins           As Integer
    Private moProcessPickList           As clsProcessPickList
    
    'Public Form Variables
    Public moSotaObjects                As New Collection           'Collection of Loaded Objects
       
    'Minimum Form Size
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long

    'Binding Object Variables
    Private moContextMenu               As clsContextMenu       'Context Menu

    'Miscellaneous Variables
    Private miMousePointer              As Integer
    
    'Message box return values
    Const kCancel = 2
    

Const VBRIG_MODULE_ID_STRING = "LinesOnHold.frm"

Private Sub ClearForm()
'if you add a control of a different type to the form, will need to modify
'this routine.
On Error GoTo ExpectedErrorRoutine
Dim ctrl As Object

    On Error Resume Next
    For Each ctrl In frmLinesOnHold.CONTROLS
    
        If TypeOf ctrl Is SOTACurrency Then
            ctrl.ClearData
            ctrl.Enabled = True
        ElseIf TypeOf ctrl Is SOTANumber Then
            ctrl.ClearData
            ctrl.Enabled = True
        ElseIf TypeOf ctrl Is CheckBox Then
            ctrl.Value = vbUnchecked
            ctrl.Tag = ""
            ctrl.Enabled = True
        ElseIf TypeOf ctrl Is TextLookup Then
            ctrl.ClearData
            ctrl.Enabled = True
        ElseIf TypeOf ctrl Is SOTADropDown Then
            ctrl.Clear
            ctrl.Enabled = True
        ElseIf TypeOf ctrl Is SOTAMaskedEdit Then
            ctrl.ClearData
            ctrl.Enabled = True
        End If
                
    Next ctrl
    On Error GoTo ExpectedErrorRoutine
    
    Exit Sub
    
ExpectedErrorRoutine:
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ClearForm", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Friend Sub LoadLinesOnHold(ByRef oDist As Object, lTaskID As Long, bShipmentGenerated As Boolean, _
                        iEmptyBins As Integer, iEmptyRandomBins As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Set moItemDist = oDist
    mlTaskID = lTaskID
    mbShipmentGenerated = bShipmentGenerated
    miEmptyBins = iEmptyBins
    miEmptyRandomBins = iEmptyRandomBins
    
    grdLineOnHold.ReDraw = False
    moDMLineGrid.Refresh
    
    'Refresh frame captions for display grids
    SetupRowsSelCaption
    grdLineOnHold.ReDraw = True

    mbIsDirty = False

    Me.Show vbModal

'    If IsDirty Then
'        uLineDetails = muLineDetails 'muLineDetails is updated with current values in HandleTookbarclick
'    End If
                  
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LoadLinesOnHold", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Public Property Get bIsDirty() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    bIsDirty = mbIsDirty
    
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsDirty", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Property Let bIsDirty(bFormDirty As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    mbIsDirty = bFormDirty

'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsDirty", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property



'************************************************************************
'   bCancelShutDown tells the framework whether the form has requested
'   the shutdown process to be cancelled.
'************************************************************************
Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancelShutDown = mbCancelShutDown

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCancelShutDown", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Private Sub SetMouse()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'    Desc: Sets Mouse to a Busy Value.
'   Parms: N/A
' Returns: N/A
'***********************************************************************
    If Me.MousePointer <> vbHourglass Then
        Me.MousePointer = vbHourglass
    End If
  
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetMouse", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub ResetMouse()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'    Desc: Resets Mouse to Previous Value.
'   Parms: N/A
' Returns: N/A
'***********************************************************************
    Me.MousePointer = miMousePointer

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ResetMouse", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub



'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & 'Z' & <FormType>
'
'   <Module>   is 'CI', 'AP', 'GL', . . .
'   'Z'        is the SOTA identifier.
'   <FormType> is 'M' = Maintenance, 'D' = data entry, 'I' = Inquiry,
'                 'P' = PeriodEnd, 'R' = Reports, 'L' = Listings, . . .
'************************************************************************
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    FormHelpPrefix = "SOZ"
    
    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & 'Z' & <FormType>
'
'   <Module>   is 'CI', 'AP', 'GL', . . .
'   'Z'        is the SOTA identifier.
'************************************************************************
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    WhatHelpPrefix = "SOZ"

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
 
Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***************************************************
'  Instantiate the Context Menu Class.
'***************************************************
    Set moContextMenu = New clsContextMenu

    With moContextMenu
        Set .Form = frmLinesOnHold
        .Init
    End With

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


Public Function bSetFocus(oControl As Object) As Boolean
    On Error GoTo ExpectedErrorRoutine

    bSetFocus = False

    If Not (oControl Is Nothing) Then
        If oControl.Enabled = True Then
            If oControl.Visible = True Then
                On Error Resume Next
                oControl.SetFocus
            End If
        End If
    End If
    
    bSetFocus = True

    'Exit this function
    Exit Function

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSetFocus", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Sub grdLineOnHold_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
On Error GoTo ExpectedErrorRoutine
    
    If mbHoldReleaseFailed Then
        mbHoldReleaseFailed = False
        Exit Sub
    End If
    
    SetMouse
    
    If Row > 0 And Col = kColLinePickCheck Then
        With grdLineOnHold
            .Col = kColLinePickCheck
            .Row = Row
                
            If bProcessHoldRelease(Row) Then
            
                'User checked the Pick check box, process security event checking
                'to check whether the user has priviliage to Release the hold
                'user passed the security event checking, go ahead pick/unpick all the lines belong to the order
                'that is not on line hold
    
                'Perform pick line update
                If moProcessPickList.bPerformPickLineUpdate(Me, grdLineOnHold, moDMLineGrid, Row, kSubItemNotUsed) Then
                    mbIsDirty = True
                Else
                    giSotaMsgBox moClass, moClass.moSysSession, kmsgCMUnexpectedSPError, "Update Pick List"
                End If
            Else
            'user failed the security event checking, can not Release the hold, undo the
            'check and exit
                If .Value = 1 Then
                    mbHoldReleaseFailed = True
                    .Value = 0
                End If
            End If
        End With
    End If
    
    ResetMouse
    
    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLineOnHold_ButtonClicked", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True.
'   Parameters:
'           KeyCode, Shift - see VB reference Manual.
'
'   NOTE: THIS ROUTINE SHOULD CALL A GLOBAL SUBROUTINE TO HANDLE ALL THE
'         AVAILABLE HOTKEYS.
'********************************************************************
    'Determine if the user used the form-level keys and process them
    Select Case keycode
        Case vbKeyF1 To vbKeyF16
            'Process the form-level key pressed
            gProcessFKeys Me, keycode, Shift
    End Select
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

'************************************************************************
'   Description:
'       Process Key Presses on the form.  NOTE: Key Preview of the form
'       should be set to True.
'   Param:
'       KeyAscii -  ASCII Key Code of Key Pressed.
'   Returns:
'************************************************************************
Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        
        Case Else
            'Other KeyAscii routines.
    End Select
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'******************************************
'* Set up the form.
'******************************************
   
    'Set the Status Bar object.
    Set sbrMain.Framework = moClass.moFramework
        
    'Get session defaults.
    Set moSysSession = moClass.moSysSession
    With moClass.moSysSession
        mlLanguage = .Language           'Language ID
        msCurrencyID = .CurrencyID       'Currency ID
        msCompanyID = .CompanyId         'Company ID
        mbEnterAsTab = .EnterAsTab
        mbIMActivated = .IsModuleActivated(kModuleIM)
    End With
   
    'Set the form's initial height and width.
    miOldFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormHeight = miOldFormHeight
    miMinFormWidth = miOldFormWidth
       
    BindContextMenu     'Bind the Context Menu.
    
    SetupLineGrid
    BindLineGrid
    BindGM
    BindToolbar

    ClearForm
        
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub SetupLineGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Set general grid properties
    gGridSetProperties grdLineOnHold, kMaxLineCols, kGridDataSheetNoAppend
    gGridSetColors grdLineOnHold
        
    With grdLineOnHold
        .OperationMode = SS_OP_MODE_NORMAL
        .AllowMultiBlocks = False
        .SelectBlockOptions = SS_SELBLOCKOPT_ALL
        .NoBeep = True 'prevent beeps on locked cells
    End With
  
    'set column captions
    gGridSetHeader grdLineOnHold, kColLinePickCheck, msPick
    gGridSetHeader grdLineOnHold, kColLineItem, msItemOrdered
    gGridSetHeader grdLineOnHold, kColLineLineHoldReason, msHoldReason
    gGridSetHeader grdLineOnHold, kColLineShiptoAddrID, msShipTo
    gGridSetHeader grdLineOnHold, kColLineQtyToPick, msQtyToPick
    gGridSetHeader grdLineOnHold, kColLineQtyAvail, msAvailToShip
    gGridSetHeader grdLineOnHold, kColLineShipUOM, msUOM
    gGridSetHeader grdLineOnHold, kColLineShipWhseID, msWhse
    gGridSetHeader grdLineOnHold, kColLineSubItem, msSubItem
    gGridSetHeader grdLineOnHold, kColLineShipDate, msShipDate
    gGridSetHeader grdLineOnHold, kColLineDeliveryMethodID, msDelivery
    gGridSetHeader grdLineOnHold, kColLineShipVia, msShipVia
    gGridSetHeader grdLineOnHold, kColLineShipPriority, msPriority
    gGridSetHeader grdLineOnHold, kColLineOrderNo, msOrderNo
    gGridSetHeader grdLineOnHold, kColLineLine, msLineNo
    gGridSetHeader grdLineOnHold, kColLineShiptoName, msShipToName
    gGridSetHeader grdLineOnHold, kColLineShiptoAddr, msShipToAddress
    
    'Hidden Columns
    gGridSetHeader grdLineOnHold, kColLineAllowDecimalQty, "AllowDecimalQty"
    gGridSetHeader grdLineOnHold, kColLineTranKey, "TranKey"
    gGridSetHeader grdLineOnHold, kColLineTranType, "TranType"
    gGridSetHeader grdLineOnHold, kColLineTranLineKey, "TranLineKey"
    gGridSetHeader grdLineOnHold, kColLineTranLineDistKey, "TranLineDistKey"

    'Set column widths
    gGridSetColumnWidth grdLineOnHold, kColLinePickCheck, 4
    gGridSetColumnWidth grdLineOnHold, kColLineItem, 12
    gGridSetColumnWidth grdLineOnHold, kColLineShiptoAddrID, 12
    gGridSetColumnWidth grdLineOnHold, kColLineQtyToPick, 8
    gGridSetColumnWidth grdLineOnHold, kColLineQtyAvail, 8
    gGridSetColumnWidth grdLineOnHold, kColLineShipUOM, 6
    gGridSetColumnWidth grdLineOnHold, kColLineShipPriority, 6
    gGridSetColumnWidth grdLineOnHold, kColLineShipDate, 8
    gGridSetColumnWidth grdLineOnHold, kColLineSubItem, 12
    gGridSetColumnWidth grdLineOnHold, kColLineDeliveryMethodID, 6
    gGridSetColumnWidth grdLineOnHold, kColLineShipVia, 10
    gGridSetColumnWidth grdLineOnHold, kColLineOrderNo, 12
    gGridSetColumnWidth grdLineOnHold, kColLineLine, 4
    gGridSetColumnWidth grdLineOnHold, kColLineShipWhseID, 8
    gGridSetColumnWidth grdLineOnHold, kColLineShiptoName, 14
    gGridSetColumnWidth grdLineOnHold, kColLineShiptoAddr, 14
    gGridSetColumnWidth grdLineOnHold, kColLineAllowDecimalQty, 8
    gGridSetColumnWidth grdLineOnHold, kColLineLineHoldReason, 12
    gGridSetColumnWidth grdLineOnHold, kColLineTranKey, 6
    gGridSetColumnWidth grdLineOnHold, kColLineTranType, 6
    gGridSetColumnWidth grdLineOnHold, kColLineTranLineKey, 6
    gGridSetColumnWidth grdLineOnHold, kColLineTranLineDistKey, 6

  'Set column types
    gGridSetColumnType grdLineOnHold, kColLineAllowDecimalQty, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineOnHold, kColLineDeliveryMethodID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineOnHold, kColLineLineHoldReason, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineOnHold, kColLineItem, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineOnHold, kColLineLine, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdLineOnHold, kColLineOrderNo, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineOnHold, kColLinePickCheck, SS_CELL_TYPE_CHECKBOX
    gGridSetColumnType grdLineOnHold, kColLineQtyAvail, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineOnHold, kColLineQtyToPick, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineOnHold, kColLineShipDate, SS_CELL_TYPE_DATE
    gGridSetColumnType grdLineOnHold, kColLineShipPriority, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineOnHold, kColLineShiptoAddr, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineOnHold, kColLineShiptoAddrID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineOnHold, kColLineShiptoName, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineOnHold, kColLineShipUOM, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineOnHold, kColLineShipVia, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineOnHold, kColLineShipWhseID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineOnHold, kColLineSubItem, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineOnHold, kColLineTranKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineOnHold, kColLineTranLineDistKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineOnHold, kColLineTranLineKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineOnHold, kColLineTranType, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineOnHold, kColLineTranTypeText, SS_CELL_TYPE_EDIT

    'lock protected columns
    gGridLockColumn grdLineOnHold, kColLineDeliveryMethodID
    gGridLockColumn grdLineOnHold, kColLineLineHoldReason
    gGridLockColumn grdLineOnHold, kColLineItem
    gGridLockColumn grdLineOnHold, kColLineLine
    gGridLockColumn grdLineOnHold, kColLineOrderNo
    gGridLockColumn grdLineOnHold, kColLineQtyAvail
    gGridLockColumn grdLineOnHold, kColLineQtyToPick
    gGridLockColumn grdLineOnHold, kColLineShipDate
    gGridLockColumn grdLineOnHold, kColLineShipPriority
    gGridLockColumn grdLineOnHold, kColLineShiptoAddr
    gGridLockColumn grdLineOnHold, kColLineShiptoAddrID
    gGridLockColumn grdLineOnHold, kColLineShiptoName
    gGridLockColumn grdLineOnHold, kColLineShipUOM
    gGridLockColumn grdLineOnHold, kColLineShipVia
    gGridLockColumn grdLineOnHold, kColLineShipWhseID
    gGridLockColumn grdLineOnHold, kColLineSubItem

    gGridHAlignColumn grdLineOnHold, kColLineShipDate, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
    gGridHAlignColumn grdLineOnHold, kColLineLine, SS_CELL_TYPE_INTEGER, SS_CELL_H_ALIGN_CENTER
    gGridHAlignColumn grdLineOnHold, kColLineShipPriority, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_LEFT
   
    'Hide warehouse column if IM is not interated
    If Not (mbIMIntegrated) Then
         gGridHideColumn grdLineOnHold, kColLineShipWhseID
    End If
    
    'hide columns
    gGridHideColumn grdLineOnHold, kColLineAllowDecimalQty
    gGridHideColumn grdLineOnHold, kColLineAllowImmedShipFromPick
    gGridHideColumn grdLineOnHold, kColLineAllowSubItem
    gGridHideColumn grdLineOnHold, kColLineAutoDist
    gGridHideColumn grdLineOnHold, kColLineAutoDistBinID
    gGridHideColumn grdLineOnHold, kColLineAutoDistBinKey
    gGridHideColumn grdLineOnHold, kColLineAutoDistBinQtyAvail
    gGridHideColumn grdLineOnHold, kColLineAutoDistBinUOMKey
    gGridHideColumn grdLineOnHold, kColLineAutoDistLotExpDate
    gGridHideColumn grdLineOnHold, kColLineAutoDistLotKey
    gGridHideColumn grdLineOnHold, kColLineAutoDistLotNo
    gGridHideColumn grdLineOnHold, kColLineCompItemQty
    gGridHideColumn grdLineOnHold, kColLineCrHold
    gGridHideColumn grdLineOnHold, kColLineCurrDisplayRow
    gGridHideColumn grdLineOnHold, kColLineCustReqShipComplete
    gGridHideColumn grdLineOnHold, kColLineDeliveryMethod
    gGridHideColumn grdLineOnHold, kColLineDistComplete
    gGridHideColumn grdLineOnHold, kColLineExtShipmentExists
    gGridHideColumn grdLineOnHold, kColLineInvtTranID
    gGridHideColumn grdLineOnHold, kColLineInvtTranKey
    gGridHideColumn grdLineOnHold, kColLineItemDesc
    gGridHideColumn grdLineOnHold, kColLineItemKey
    gGridHideColumn grdLineOnHold, kColLineItemReqShipComplete
    gGridHideColumn grdLineOnHold, kColLineItemType
    gGridHideColumn grdLineOnHold, kColLineKitShipLineKey
    gGridHideColumn grdLineOnHold, kColLineLineHold
    gGridHideColumn grdLineOnHold, kColLineOrderLineUOMID
    gGridHideColumn grdLineOnHold, kColLineOrderLineUOMKey
    gGridHideColumn grdLineOnHold, kColLineOrdHold
    gGridHideColumn grdLineOnHold, kColLinePacked
    gGridHideColumn grdLineOnHold, kColLineQtyDist
    gGridHideColumn grdLineOnHold, kColLineQtyOrdered
    gGridHideColumn grdLineOnHold, kColLineQtyPicked
    gGridHideColumn grdLineOnHold, kColLineQtyShort
    gGridHideColumn grdLineOnHold, kColLineQtyToPickByOrdUOM
    gGridHideColumn grdLineOnHold, kColLineRcvgWarehouse
    gGridHideColumn grdLineOnHold, kColLineSASequence
    gGridHideColumn grdLineOnHold, kColLineShipKey
    gGridHideColumn grdLineOnHold, kColLineShipLineDistKey
    gGridHideColumn grdLineOnHold, kColLineShipLineDistUpdateCounter
    gGridHideColumn grdLineOnHold, kColLineShipLineKey
    gGridHideColumn grdLineOnHold, kColLineShipLineUpdateCounter
    gGridHideColumn grdLineOnHold, kColLineShipmentCommitStatus
    gGridHideColumn grdLineOnHold, kColLineShipUOMKey
    gGridHideColumn grdLineOnHold, kColLineShipWhseKey
    gGridHideColumn grdLineOnHold, kColLineShortPickFlag
    gGridHideColumn grdLineOnHold, kColLineStatus
    gGridHideColumn grdLineOnHold, kColLineStatusCustShipCompleteViolation
    gGridHideColumn grdLineOnHold, kColLineStatusDistQtyShort
    gGridHideColumn grdLineOnHold, kColLineStatusDistWarning
    gGridHideColumn grdLineOnHold, kColLineStatusItemShipCompLeteViolation
    gGridHideColumn grdLineOnHold, kColLineStatusOverShipment
    gGridHideColumn grdLineOnHold, kColLineSubFlag
    gGridHideColumn grdLineOnHold, kColLineSubItemKey
    gGridHideColumn grdLineOnHold, kColLineTrackMeth
    gGridHideColumn grdLineOnHold, kColLineTrackQtyAtBin
    gGridHideColumn grdLineOnHold, kColLineTranKey
    gGridHideColumn grdLineOnHold, kColLineTranLineDistKey
    gGridHideColumn grdLineOnHold, kColLineTranLineKey
    gGridHideColumn grdLineOnHold, kColLineTranType
    gGridHideColumn grdLineOnHold, kColLineTranTypeText
    gGridHideColumn grdLineOnHold, kColLineWhseUseBin

    gGridFreezeCols grdLineOnHold, kColLineItem

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupLineGrid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Private Sub BindLineGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moDMLineGrid = New clsDmGrid
    With moDMLineGrid
        Set .Session = moSysSession
        Set .Database = moAppDB
        Set .Form = frmLinesOnHold
        Set .Grid = grdLineOnHold
        .Table = "#tsoCreatePickWrk2"
        .UniqueKey = "ShipLineKey"
        .NoAppend = True
        .OrderBy = "ItemID"
        .Where = "CurrDisplayRow = 1"   '"LineHold = 1 OR CrHold = 1 OR OrderHold = 1"
        
        .BindColumn "AllowDecimalQty", kColLineAllowDecimalQty, SQL_SMALLINT
        .BindColumn "AllowSubItem", kColLineAllowSubItem, SQL_SMALLINT
        .BindColumn "CrHold", kColLineCrHold, SQL_SMALLINT
        .BindColumn "CurrDisplayRow", kColLineCurrDisplayRow, SQL_SMALLINT
        .BindColumn "CustID", Nothing, SQL_VARCHAR
        .BindColumn "CustKey", Nothing, SQL_INTEGER, , kDmSetNull
        .BindColumn "CustName", Nothing, SQL_VARCHAR
        .BindColumn "CustReqShipComplete", kColLineCustReqShipComplete, SQL_SMALLINT
        .BindColumn "DeliveryMethod", kColLineDeliveryMethod, SQL_SMALLINT
        .BindColumn "DeliveryMethodID", kColLineDeliveryMethodID, SQL_VARCHAR
        .BindColumn "Expiration", Nothing, SQL_DATE
        .BindColumn "FreightAmt", Nothing, SQL_DECIMAL
        .BindColumn "InvtTranKey", kColLineInvtTranKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ItemDesc", kColLineItemDesc, SQL_VARCHAR
        .BindColumn "ItemID", kColLineItem, SQL_VARCHAR
        .BindColumn "ItemKey", kColLineItemKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ItemReqShipComplete", kColLineItemReqShipComplete, SQL_SMALLINT
        .BindColumn "ItemType", kColLineItemType, SQL_SMALLINT
        .BindColumn "KitShipLineKey", kColLineKitShipLineKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "LineDistKey", kColLineTranLineDistKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "LineHold", kColLineLineHold, SQL_SMALLINT
        .BindColumn "LineHoldReason", kColLineLineHoldReason, SQL_VARCHAR
        .BindColumn "LineKey", kColLineTranLineKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "LineNbr", kColLineLine, SQL_INTEGER
        .BindColumn "OrderHold", kColLineOrdHold, SQL_SMALLINT
        .BindColumn "OrderHoldReason", Nothing, SQL_VARCHAR
        .BindColumn "OrderKey", kColLineTranKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "OrderLineUnitMeasKey", kColLineOrderLineUOMKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "PickStatus", kColLinePickCheck, SQL_SMALLINT
        .BindColumn "QtyAvail", kColLineQtyAvail, SQL_DECIMAL
        .BindColumn "QtyDist", kColLineQtyDist, SQL_DECIMAL
        .BindColumn "QtyPendingShip", Nothing, SQL_DECIMAL
        .BindColumn "QtyPicked", kColLineQtyPicked, SQL_DECIMAL
        .BindColumn "QtyToPick", kColLineQtyToPick, SQL_DECIMAL
        .BindColumn "RcvgWhseID", kColLineRcvgWarehouse, SQL_VARCHAR
        .BindColumn "RcvgWhseKey", Nothing, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipKey", kColLineShipKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipmentCommitStatus", kColLineShipmentCommitStatus, SQL_SMALLINT
        .BindColumn "ShipDate", kColLineShipDate, SQL_DATE
        .BindColumn "ShipLineKey", kColLineShipLineKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipLineDistKey", kColLineShipLineDistKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipMethID", kColLineShipVia, SQL_VARCHAR
        .BindColumn "ShipMethKey", Nothing, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipPriority", kColLineShipPriority, SQL_VARCHAR, , kDmSetNull
        .BindColumn "ShipToAddrKey", Nothing, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipToCustAddress", kColLineShiptoAddr, SQL_VARCHAR
        .BindColumn "ShipToCustAddrID", kColLineShiptoAddrID, SQL_VARCHAR
        .BindColumn "ShipToCustAddrKey", Nothing, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipUnitMeasID", kColLineShipUOM, SQL_VARCHAR
        .BindColumn "ShipUnitMeasKey", kColLineShipUOMKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipWhseID", kColLineShipWhseID, SQL_VARCHAR
        .BindColumn "ShipWhseKey", kColLineShipWhseKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "Status", kColLineStatus, SQL_VARCHAR
        .BindColumn "StatusCustShipCompleteViolation", kColLineStatusCustShipCompleteViolation, SQL_VARCHAR
        .BindColumn "StatusItemShipCompLeteViolation", kColLineStatusItemShipCompLeteViolation, SQL_VARCHAR
        .BindColumn "StatusDistWarning", kColLineStatusDistWarning, SQL_VARCHAR
        .BindColumn "StatusDistQtyShort", kColLineStatusDistQtyShort, SQL_VARCHAR
        .BindColumn "StatusOverShipment", kColLineStatusOverShipment, SQL_VARCHAR
        .BindColumn "StockAllocSeq", kColLineSASequence, SQL_INTEGER
        .BindColumn "StockQtyAvail", Nothing, SQL_DECIMAL
        .BindColumn "StockUnitMeasKey", Nothing, SQL_INTEGER, , kDmSetNull
        .BindColumn "SubstiStatus", kColLineSubFlag, SQL_VARCHAR
        .BindColumn "SubstituteItemDesc", kColLineSubItem, SQL_VARCHAR
        .BindColumn "SubstituteItemKey", kColLineSubItemKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "TrackMeth", kColLineTrackMeth, SQL_SMALLINT
        .BindColumn "TrackQtyAtBin", kColLineTrackQtyAtBin, SQL_SMALLINT
        .BindColumn "TranNoRelChngOrd", kColLineOrderNo, SQL_VARCHAR
        .BindColumn "TranType", kColLineTranType, SQL_INTEGER
        .BindColumn "TranTypeText", kColLineTranTypeText, SQL_VARCHAR
        .BindColumn "WhseUseBin", kColLineWhseUseBin, SQL_SMALLINT

        .Init
    End With
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindLineGrid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Private Sub BindGM()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moGMLine = New clsGridMgr
    With moGMLine
        Set .Grid = grdLineOnHold
        Set .DM = moDMLineGrid
        Set .Form = frmLinesOnHold
        .GridType = kGridDataSheetNoAppend
        .GridSortEnabled = True
        .Init
    End With
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindGM", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
'************************************************************************
'   Description:
'       Create the toolbar.
'   Param:
'       <none>
'   Returns:
'************************************************************************
Private Sub BindToolbar()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
   
    With moClass.moFramework
        tbrMain.SecurityLevel = .GetTaskPermission(.GetTaskID())
        tbrMain.LocaleID = mlLanguage
    End With
    
    'Set up the toolbar
    tbrMain.AddButton kTbFinishExit
    If (moClass.moSysSession.ShowOfficeOnToolBar = 1) Then
        tbrMain.AddButton kTbOffice
        tbrMain.AddSeparator
        tbrMain.AddButton kTbHelp
    Else
        tbrMain.AddButton kTbHelp
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindToolbar", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


'************************************************************************
'   Description:
'       Standard Shutdown Procedure.  Unload all child forms
'       and remove any objects created within this app.
'   Param:
'       <none>
'   Returns:
'************************************************************************
Public Sub PerformCleanShutDown()
    On Error GoTo ExpectedErrorRoutine

         
    'Remove all child collections.
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    
    TerminateControls Me
    
    If Not moContextMenu Is Nothing Then
        Set moContextMenu = Nothing
    End If
    
    If Not moGMLine Is Nothing Then
        moGMLine.UnloadSelf
        Set moGMLine = Nothing
    End If
    
    If Not moDMLineGrid Is Nothing Then
        moDMLineGrid.UnloadSelf
        Set moDMLineGrid = Nothing
    End If
    
    If Not moItemDist Is Nothing Then
        Set moItemDist = Nothing
    End If
    
    If Not moAppDB Is Nothing Then
        Set moAppDB = Nothing
    End If
    
    If Not moSysSession Is Nothing Then
        Set moSysSession = Nothing
    End If
    
    If Not moProcessPickList Is Nothing Then
        Set moProcessPickList = Nothing
    End If
    
    'Exit this subroutine
    Exit Sub

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If

    If UnloadMode <> vbFormCode Then
        ProcessSave
        Cancel = True
    Else
        PerformCleanShutDown
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Me.WindowState = vbNormal Or Me.WindowState = vbMaximized Then
        'Resize Height
        gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, fraLinesOnHold, grdLineOnHold
        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, fraLinesOnHold, grdLineOnHold
               
        miOldFormHeight = Me.Height
        miOldFormWidth = Me.Width
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    If Not moClass Is Nothing Then
        Set moClass = Nothing
    End If
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

'************************************************************************
'   Description:
'       Returns Form Name for Debugging Information.
'   Param:
'       <none>
'   Returns:
'       Form Name
'************************************************************************
Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function

'************************************************************************
'   oClass contains the reference to the parent class object.  The form
'   needs this reference to use the public variables created within the
'   class object.
'************************************************************************
Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moClass = oNewClass

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
'************************************************************************
'   oClass contains the reference to the parent class object.  The form
'   needs this reference to use the public variables created within the
'   class object.
'************************************************************************
Public Property Get oProcessPickList() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oProcessPickList = moProcessPickList
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oProcessPickList", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Property Set oProcessPickList(oNewProcessPickList As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moProcessPickList = oNewProcessPickList

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oProcessPickList", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
Public Property Get oAppDb() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oAppDb = moAppDB
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oAppDB", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Property Set oAppDb(oNewAppDB As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moAppDB = oNewAppDB

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oAppDB", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Private Sub grdLineOnHold_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    If Row = 0 And Col <> 0 Then
        moDMLineGrid.Save
    End If
    moGMLine.Grid_Click Col, Row
    
'    If Row > 0 Then 'if event was not caused by a click in the heading area
'        SetRowValues Row
'    End If
    
    'If the user click on any locked cell, highlight the grid row the cell is in
    With grdLineOnHold
        .Row = Row
        .Col = Col
        If .Lock = True Then
            gGridSetSelectRow grdLineOnHold, Row
        End If
    End With
   
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLineOnHold_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub



Private Sub grdLineOnHold_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    moGMLine.Grid_EditMode Col, Row, Mode, ChangeMade

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLineOnHold_EditMode", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub grdLineOnHold_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    moGMLine.Grid_KeyDown keycode, Shift

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLineOnHold_KeyDown", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdLineOnHold_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    moGMLine.Grid_LeaveCell Col, Row, NewCol, NewRow

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLineOnHold_LeaveCell", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++

End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarClick Button

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub


'************************************************************************
'   Description:
'       Process all toolbar clicks (as well as HotKey shortcuts to
'       toolbar buttons).
'   Param:
'       sKey -  Token returned from toolbar.  Indicates what function
'               is to be executed.
'   Returns:
'************************************************************************
Private Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If

    Me.SetFocus
    DoEvents

    Select Case sKey
        Case kTbFinishExit
           
            ProcessSave
                                 
        Case kTbHelp
            'The Help button was pressed by the user.
            gDisplayFormLevelHelp Me

        Case Else
            'Error processing
            'Give an error message to the user.
            'Unexpected Operation Value: {0}
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation, _
                         CVar(sKey)
    End Select
    
    ResetMouse

    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub ProcessSave()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Me.Hide
    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ProcessSave", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub SetFocusCtrl()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'Set focus so that next time form is shown, focus will be at first control
'    lkuItem.Enabled = True
'    lkuItem.SetFocus
  
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetFocusCtrl", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
#End If

#If CUSTOMIZER Then
Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        
        If Not moFormCust Is Nothing Then
            moFormCust.Initialize Me, goClass
            Set moFormCust.CustToolbarMgr = tbrMain
            'moFormCust.ApplyDataBindings moDmForm
            moFormCust.ApplyFormCust
        End If
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
#End If



#If CUSTOMIZER And CONTROLS Then
Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomButton(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomButton(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomButton(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomCheck(Index)
    End If
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomCheck(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomCheck(Index)
    End If
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnChange CustomCombo(Index)
    End If
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomCombo(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnDblClick CustomCombo(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomCombo(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomCombo(Index)
    End If
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnChange CustomCurrency(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomCurrency(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomCurrency(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomFrame(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnDblClick CustomFrame(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomLabel(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnDblClick CustomLabel(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnChange CustomMask(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomMask(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnKeyPress CustomMask(Index), KeyAscii
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomMask(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnChange CustomNumber(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomNumber(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomNumber(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomOption(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnDblClick CustomOption(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomOption(Index)
    End If
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomOption(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnSpinDown CustomSpin(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnSpinUp CustomSpin(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
#End If

#If CUSTOMIZER And CONTROLS Then
Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomDate(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnDblClick CustomDate(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomDate(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomDate(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnKeyPress CustomDate(Index), KeyAscii
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnChange CustomDate(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
#End If

Public Property Get MyApp() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set MyApp = App

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MyApp", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Property Get MyForms() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set MyForms = Forms
    
    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MyForms", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
Public Sub DMGridRowLoaded(oDM As Object, lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim iAllowDecimalQty As Integer
    Dim iOrderHold As Integer
    Dim iCrHold As Integer

    If oDM Is moDMLineGrid Then
        'sSQLDate = Left(gsGridReadCellText(grdLinePicked, lRow, kColLineShipDate), 10) 'Remove time
        'gGridUpdateCellText grdLinePicked, lRow, kColLineShipDate, Format(sSQLDate, "SHORT DATE") 'Is Localized
        
        iAllowDecimalQty = giGetValidInt(gsGridReadCell(grdLineOnHold, lRow, kColLineAllowDecimalQty))
        iOrderHold = giGetValidInt(gsGridReadCell(grdLineOnHold, lRow, kColLineOrdHold))
        iCrHold = giGetValidInt(gsGridReadCell(grdLineOnHold, lRow, kColLineCrHold))
    
        moProcessPickList.SetGridNumericAttr grdLineOnHold, kColLineQtyDist, moProcessPickList.iNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow
        moProcessPickList.SetGridNumericAttr grdLineOnHold, kColLineQtyToPick, moProcessPickList.iNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow
        moProcessPickList.SetGridNumericAttr grdLineOnHold, kColLineQtyAvail, moProcessPickList.iNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow
        moProcessPickList.SetGridNumericAttr grdLineOnHold, kColLineQtyShort, moProcessPickList.iNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow
        moProcessPickList.SetGridNumericAttr grdLineOnHold, kColLineQtyPicked, moProcessPickList.iNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow
    
        With grdLineOnHold
            .Col = kColLinePickCheck
            .Row = lRow
            
            If iOrderHold = 1 Or iCrHold = 1 Then
                .Lock = True
            End If
        End With
    
    End If

    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMGridRowLoaded", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


Private Sub SetRowValues(ByVal lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim iAllowDecimalQty    As Integer
    
    iAllowDecimalQty = giGetValidInt(gsGridReadCell(grdLineOnHold, lRow, kColLineAllowDecimalQty))

    'setup numeric formatting
    moProcessPickList.SetGridNumericAttr grdLineOnHold, kColLineQtyDist, moProcessPickList.iNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow
    moProcessPickList.SetGridNumericAttr grdLineOnHold, kColLineQtyToPick, moProcessPickList.iNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow
    moProcessPickList.SetGridNumericAttr grdLineOnHold, kColLineQtyAvail, moProcessPickList.iNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow
    moProcessPickList.SetGridNumericAttr grdLineOnHold, kColLineQtyShort, moProcessPickList.iNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow
    moProcessPickList.SetGridNumericAttr grdLineOnHold, kColLineQtyPicked, moProcessPickList.iNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetRowValues", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


Private Sub SetupRowsSelCaption()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'********************************************************************
' Description:
'    This routine updates the select grids's frame caption for
'    the current row count in the grid
'********************************************************************
Dim iLineNbrSelected    As Integer

    iLineNbrSelected = glGridGetDataRowCnt(grdLineOnHold)
    
    If iLineNbrSelected = 0 Then
        fraLinesOnHold.Caption = iLineNbrSelected & " " & msLineOnHold
    Else
        If iLineNbrSelected > 1 Then
            fraLinesOnHold.Caption = iLineNbrSelected & " " & msLinesOnHold
        Else
            fraLinesOnHold.Caption = iLineNbrSelected & " " & msLineOnHold
        End If
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupRowsSelCaption", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function bProcessHoldRelease(ByVal lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'********************************************************************
' Description:
'    This function will perform the necessary check for release
'    hold on the orders to pick
'********************************************************************
On Error GoTo ExpectedErrorRoutine

    Dim lShipLineKey As Long
    Dim iLineHold As Integer
    Dim lRetVal As Long
    
    bProcessHoldRelease = False
    
    SetMouse
    
    If lRow > 0 Then
        lShipLineKey = glGetValidLong(gsGridReadCell(grdLineOnHold, lRow, kColLineShipLineKey))
        iLineHold = giGetValidInt(gsGridReadCell(grdLineOnHold, lRow, kColLineLineHold))

        If iLineHold = 1 And Not moProcessPickList.bOverrideSecEvent(kSecEventRemoveHold) Then
            'User does not have the security level at Normal and Higher, deny the request
            giSotaMsgBox Me, moClass.moSysSession, kSOmsgNoPermitToReleaseHold
            ResetMouse
            Exit Function
        End If
    
        'Call sp to release the hold and perform necessary update to the database
        With moAppDB
            .SetInParam 0                   '@_iReleaseCrHold
            .SetInParam 0                   '@_iReleaseOrderHold
            .SetInParam iLineHold           '@_iReleaseLineHold
            .SetInParam 0                   '@_iTranKey
            .SetInParam 0                   '@_iTranType
            .SetInParam lShipLineKey        '@_iShipLineKey
            .SetInParam oClass.moSysSession.UserId            '@_iUserID
            .SetOutParam lRetVal            '@_oRetVal
            .ExecuteSP "spsoReleaseSalesOrderHold"
            lRetVal = .GetOutParam(8)
            .ReleaseParams
        End With
            
        If lRetVal <> 1 Or Err.Number <> 0 Then
            giSotaMsgBox moClass, moClass.moSysSession, kmsgProc, "spsoReleaseSalesOrderHold"
            ResetMouse
            Exit Function
        End If
        
        'User has the security level at Normal and Higher, Release the hold
        If iLineHold = 1 Then
            gGridUpdateCell grdLineOnHold, lRow, kColLineLineHold, CStr(0)
            gGridUpdateCell grdLineOnHold, lRow, kColLineLineHoldReason, ""
        End If
    End If
    
    ResetMouse
    bProcessHoldRelease = True
    
    Exit Function
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    
    Exit Function
    
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "bProcessHoldRelease", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function



