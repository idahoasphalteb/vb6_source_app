VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Begin VB.Form frmOrdersOnHold 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Orders on Hold"
   ClientHeight    =   5220
   ClientLeft      =   1170
   ClientTop       =   1440
   ClientWidth     =   10530
   HelpContextID   =   17774327
   Icon            =   "sozdd103.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5220
   ScaleWidth      =   10530
   Begin VB.Frame fraOrdersOnHold 
      Caption         =   "0 Order On Hold"
      Height          =   4200
      Left            =   120
      TabIndex        =   12
      Top             =   480
      Width           =   10305
      Begin VB.CommandButton cmdOrderLines 
         Caption         =   "Order &Lines..."
         Height          =   375
         Index           =   0
         Left            =   8640
         TabIndex        =   15
         Top             =   240
         WhatsThisHelpID =   17774328
         Width           =   1455
      End
      Begin FPSpreadADO.fpSpread grdOrderOnHold 
         Height          =   3375
         Left            =   120
         TabIndex        =   13
         Top             =   720
         WhatsThisHelpID =   17776629
         Width           =   10035
         _Version        =   524288
         _ExtentX        =   17701
         _ExtentY        =   5953
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         SpreadDesigner  =   "sozdd103.frx":23D2
         AppearanceStyle =   0
      End
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   11
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   5
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   6
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   0
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   10530
      _ExtentX        =   18574
      _ExtentY        =   741
      Style           =   0
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   4830
      WhatsThisHelpID =   73
      Width           =   10530
      _ExtentX        =   18574
      _ExtentY        =   688
      BrowseVisible   =   0   'False
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   10
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmOrdersOnHold"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************************
'     Name: frmOrdersOnHold
'     Desc: Orders On Hold
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: JY 04-01/2004
'     Mods:
'************************************************************************************

Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

    Private mbUserSelectedItem As Boolean
    
    'Private Variables of Form Properties
    Private mbEnterAsTab                As Boolean
    Private moClass                     As Object
    Private moSysSession                As Object
    Private moAppDB                     As Object                   'Class Reference
    Private mbCancelShutDown            As Boolean                  'Cancel Shutdown Flag
    Private mbIsDirty                   As Boolean                  'Form dirty?
    Private msCurrencyID                As String
    Private msCompanyID                 As String
    Private mlLanguage                  As Long
    Private mbIMActivated               As Boolean
    Private WithEvents moDMOrderGrid    As clsDmGrid
Attribute moDMOrderGrid.VB_VarHelpID = -1
    Private WithEvents moGMOrder        As clsGridMgr
Attribute moGMOrder.VB_VarHelpID = -1
    Private moItemDist                  As Object
    Private mlRunMode                   As Long
    Private mbHoldReleaseFailed            As Boolean
    Private mlTaskID                    As Long
    Private mbShipmentGenerated         As Boolean
    Private miEmptyBins                 As Integer
    Private miEmptyRandomBins           As Integer
    Private mbIMIntegrated              As Boolean
    Private moProcessPickList           As clsProcessPickList
    
    'Public Form Variables
    Public moSotaObjects                As New Collection           'Collection of Loaded Objects
       
    'Minimum Form Size
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long

    'Binding Object Variables
    Private moDmForm                    As clsDmForm            'Parent Object
    Private moContextMenu               As clsContextMenu       'Context Menu

    'Miscellaneous Variables
    Private miMousePointer              As Integer
    
    'Message box return values
    Private Const kCancel = 2

Const VBRIG_MODULE_ID_STRING = "OrdersOnHold.frm"


Private Sub ClearForm()
'if you add a control of a different type to the form, will need to modify
'this routine.
On Error GoTo ExpectedErrorRoutine
Dim ctrl As Object

    On Error Resume Next
    For Each ctrl In frmOrdersOnHold.CONTROLS
    
        If TypeOf ctrl Is SOTACurrency Then
            ctrl.ClearData
            ctrl.Enabled = True
        ElseIf TypeOf ctrl Is SOTANumber Then
            ctrl.ClearData
            ctrl.Enabled = True
        ElseIf TypeOf ctrl Is CheckBox Then
            ctrl.Value = vbUnchecked
            ctrl.Tag = ""
            ctrl.Enabled = True
        ElseIf TypeOf ctrl Is TextLookup Then
            ctrl.ClearData
            ctrl.Enabled = True
        ElseIf TypeOf ctrl Is SOTADropDown Then
            ctrl.Clear
            ctrl.Enabled = True
        ElseIf TypeOf ctrl Is SOTAMaskedEdit Then
            ctrl.ClearData
            ctrl.Enabled = True
        End If
                
    Next ctrl
    On Error GoTo ExpectedErrorRoutine
    
    Exit Sub
    
ExpectedErrorRoutine:
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ClearForm", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Property Get bIsDirty() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    bIsDirty = mbIsDirty
    
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsDirty", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Property Let bIsDirty(bFormDirty As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    mbIsDirty = bFormDirty

'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsDirty", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
Friend Sub LoadOrdersOnHold(ByRef oDist As Object, lTaskID As Long, bShipmentGenerated As Boolean, _
                        iEmptyBins As Integer, iEmptyRandomBins As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Set moItemDist = oDist
    mlTaskID = lTaskID
    mbShipmentGenerated = bShipmentGenerated
    miEmptyBins = iEmptyBins
    miEmptyRandomBins = iEmptyRandomBins
    
    grdOrderOnHold.redraw = False
    moDMOrderGrid.Refresh
    
    'Refresh frame captions for display grids
    SetupRowsSelCaption
    grdOrderOnHold.redraw = True

    mbIsDirty = False

    Me.Show vbModal

'    If IsDirty Then
'        uLineDetails = muLineDetails 'muLineDetails is updated with current values in HandleTookbarclick
'    End If
                  
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LoadOrdersOnHold", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

'************************************************************************
'   bCancelShutDown tells the framework whether the form has requested
'   the shutdown process to be cancelled.
'************************************************************************
Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancelShutDown = mbCancelShutDown

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCancelShutDown", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Private Sub SetMouse()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'    Desc: Sets Mouse to a Busy Value.
'   Parms: N/A
' Returns: N/A
'***********************************************************************
    If Me.MousePointer <> vbHourglass Then
        Me.MousePointer = vbHourglass
    End If
  
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetMouse", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub ResetMouse()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'    Desc: Resets Mouse to Previous Value.
'   Parms: N/A
' Returns: N/A
'***********************************************************************
    Me.MousePointer = miMousePointer

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ResetMouse", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub



'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & 'Z' & <FormType>
'
'   <Module>   is 'CI', 'AP', 'GL', . . .
'   'Z'        is the SOTA identifier.
'   <FormType> is 'M' = Maintenance, 'D' = data entry, 'I' = Inquiry,
'                 'P' = PeriodEnd, 'R' = Reports, 'L' = Listings, . . .
'************************************************************************
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    FormHelpPrefix = "SOZ"
    
    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & 'Z' & <FormType>
'
'   <Module>   is 'CI', 'AP', 'GL', . . .
'   'Z'        is the SOTA identifier.
'************************************************************************
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    WhatHelpPrefix = "SOZ"

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
 

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***************************************************
'  Instantiate the Context Menu Class.
'***************************************************
    Set moContextMenu = New clsContextMenu

    With moContextMenu
        Set .Form = frmOrdersOnHold
        .Init
    End With

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub cmdOrderLines_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdOrderLines(Index), True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    
On Error GoTo ExpectedErrorRoutine
    SetMouse
    
    Dim lRow            As Long
    
    'Get the active row
    lRow = glGridGetActiveRow(grdOrderOnHold)
    
    If Not moItemDist Is Nothing Then
        If Not moProcessPickList.bLoadOrderPickLines(grdOrderOnHold, kPickLineOnHold, _
                    mbShipmentGenerated, miEmptyBins, miEmptyRandomBins) Then
            giSotaMsgBox moClass, moClass.moSysSession, kmsgCMUnexpectedSPError, "Loading Order Pick Line Form"
        End If
    End If
    
    If frmOrderPickLines.bIsDirty Then
        'Was some data changed on the Detail form?
        grdOrderOnHold.redraw = False
        moDMOrderGrid.Refresh
        grdOrderOnHold.redraw = True
    End If
    
    ResetMouse
    
    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse

'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdOrderLines_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True.
'   Parameters:
'           KeyCode, Shift - see VB reference Manual.
'
'   NOTE: THIS ROUTINE SHOULD CALL A GLOBAL SUBROUTINE TO HANDLE ALL THE
'         AVAILABLE HOTKEYS.
'********************************************************************
    'Determine if the user used the form-level keys and process them
    Select Case keycode
        Case vbKeyF1 To vbKeyF16
            'Process the form-level key pressed
            gProcessFKeys Me, keycode, Shift
    End Select
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

'************************************************************************
'   Description:
'       Process Key Presses on the form.  NOTE: Key Preview of the form
'       should be set to True.
'   Param:
'       KeyAscii -  ASCII Key Code of Key Pressed.
'   Returns:
'************************************************************************
Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        
        Case Else
            'Other KeyAscii routines.
    End Select
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'******************************************
'* Set up the form.
'******************************************
   
    'Set the Status Bar object.
    Set sbrMain.Framework = moClass.moFramework
        
    'Get session defaults.
    Set moSysSession = moClass.moSysSession
    With moClass.moSysSession
        mlLanguage = .Language           'Language ID
        msCurrencyID = .CurrencyID       'Currency ID
        msCompanyID = .CompanyId         'Company ID
        mbEnterAsTab = .EnterAsTab
        mbIMActivated = .IsModuleActivated(kModuleIM)
    End With
   
    'Set the form's initial height and width.
    miOldFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormHeight = miOldFormHeight
    miMinFormWidth = miOldFormWidth
       
    BindContextMenu     'Bind the Context Menu.
    
    SetupOrderGrid
    BindOrderGrid
    BindGM
    BindToolbar

    ClearForm
        
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub SetupOrderGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Set general grid properties
    gGridSetProperties grdOrderOnHold, kMaxOrderCols, kGridDataSheetNoAppend
    gGridSetColors grdOrderOnHold
        
    With grdOrderOnHold
        .OperationMode = SS_OP_MODE_NORMAL
        .AllowMultiBlocks = False
        .SelectBlockOptions = SS_SELBLOCKOPT_ALL
        .NoBeep = True 'prevent beeps on locked cells
    End With
    
    '****************************************************************************
    'SGS DEJ IA (START)
    '****************************************************************************
    gGridSetHeader grdOrderOnHold, kColOrdBOL, "BOL"
    gGridSetColumnWidth grdOrderOnHold, kColOrdBOL, 8
    gGridSetColumnType grdOrderOnHold, kColOrdBOL, SS_CELL_TYPE_EDIT
    gGridLockColumn grdOrderOnHold, kColOrdBOL
    
    '****************************************************************************
    'SGS DEJ IA (STOP)
    '****************************************************************************
    
    'set column captions
    gGridSetHeader grdOrderOnHold, kColOrdPickCheck, msPick
    gGridSetHeader grdOrderOnHold, kColOrdOrderNo, msOrderNumber
    gGridSetHeader grdOrderOnHold, kColOrdHold, msManualHold
    gGridSetHeader grdOrderOnHold, kColOrdCrHold, msCreditHold
    gGridSetHeader grdOrderOnHold, kColOrdHoldReason, "Hold Reason"
    gGridSetHeader grdOrderOnHold, kColOrdShipToAddrID, msShipTo
    gGridSetHeader grdOrderOnHold, kColOrdOrderDate, msOrderDate
    gGridSetHeader grdOrderOnHold, kColOrdExpiration, msExpireDate
    gGridSetHeader grdOrderOnHold, kColOrdShipVia, msShipVia
    gGridSetHeader grdOrderOnHold, kColOrdEarliestShip, msEarliestShip
    gGridSetHeader grdOrderOnHold, kColOrdShipPriority, msPriority
    gGridSetHeader grdOrderOnHold, kColOrdShipFromWhse, msShipFrom
    gGridSetHeader grdOrderOnHold, kColOrdTranKey, "Tran Key"
  
    'Set column widths
    gGridSetColumnWidth grdOrderOnHold, kColOrdPickCheck, 4
    gGridSetColumnWidth grdOrderOnHold, kColOrdSubFlag, 4
    gGridSetColumnWidth grdOrderOnHold, kColOrdStatus, 4
    gGridSetColumnWidth grdOrderOnHold, kColOrdOrderNo, 12
    gGridSetColumnWidth grdOrderOnHold, kcolOrdTranType, 6
    gGridSetColumnWidth grdOrderOnHold, kColOrdShipToAddrID, 12
    gGridSetColumnWidth grdOrderOnHold, kColOrdOrderDate, 8
    gGridSetColumnWidth grdOrderOnHold, kColOrdExpiration, 8
    gGridSetColumnWidth grdOrderOnHold, kColOrdShipVia, 9
    gGridSetColumnWidth grdOrderOnHold, kColOrdEarliestShip, 8
    gGridSetColumnWidth grdOrderOnHold, kColOrdShipPriority, 6
    gGridSetColumnWidth grdOrderOnHold, kColOrdShipFromWhse, 12
    gGridSetColumnWidth grdOrderOnHold, kColOrdTranKey, 4
    gGridSetColumnWidth grdOrderOnHold, kColOrdHold, 9
    gGridSetColumnWidth grdOrderOnHold, kColOrdCrHold, 8
    gGridSetColumnWidth grdOrderOnHold, kColOrdHoldReason, 12

    'Set column types
    gGridSetColumnType grdOrderOnHold, kColOrdPickCheck, SS_CELL_TYPE_CHECKBOX
    gGridSetColumnType grdOrderOnHold, kColOrdSubFlag, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderOnHold, kColOrdStatus, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderOnHold, kColOrdOrderNo, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderOnHold, kcolOrdTranType, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderOnHold, kColOrdShipToAddrID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderOnHold, kColOrdOrderDate, SS_CELL_TYPE_DATE
    gGridSetColumnType grdOrderOnHold, kColOrdExpiration, SS_CELL_TYPE_DATE
    gGridSetColumnType grdOrderOnHold, kColOrdShipVia, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderOnHold, kColOrdEarliestShip, SS_CELL_TYPE_DATE
    gGridSetColumnType grdOrderOnHold, kColOrdShipPriority, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderOnHold, kColOrdShipFromWhse, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderOnHold, kColOrdTranKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderOnHold, kColOrdHold, SS_CELL_TYPE_CHECKBOX
    gGridSetColumnType grdOrderOnHold, kColOrdCrHold, SS_CELL_TYPE_CHECKBOX
    gGridSetColumnType grdOrderOnHold, kColOrdHoldReason, SS_CELL_TYPE_EDIT

    'lock protected columns
    gGridLockColumn grdOrderOnHold, kColOrdOrderNo
    gGridLockColumn grdOrderOnHold, kColOrdShipToAddrID
    gGridLockColumn grdOrderOnHold, kColOrdOrderDate
    gGridLockColumn grdOrderOnHold, kColOrdExpiration
    gGridLockColumn grdOrderOnHold, kColOrdShipVia
    gGridLockColumn grdOrderOnHold, kColOrdEarliestShip
    gGridLockColumn grdOrderOnHold, kColOrdShipPriority
    gGridLockColumn grdOrderOnHold, kColOrdShipFromWhse
    gGridLockColumn grdOrderOnHold, kColOrdHold
    gGridLockColumn grdOrderOnHold, kColOrdCrHold
    gGridLockColumn grdOrderOnHold, kColOrdHoldReason
    
    gGridHAlignColumn grdOrderOnHold, kColOrdOrderDate, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
    gGridHAlignColumn grdOrderOnHold, kColOrdExpiration, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
    gGridHAlignColumn grdOrderOnHold, kColOrdEarliestShip, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
    
    'Hide warehouse column if IM is not interated
    If Not (mbIMIntegrated) Then
         gGridHideColumn grdOrderOnHold, kColOrdShipFromWhse
    End If
    
    'hide columns
    gGridHideColumn grdOrderOnHold, kColOrdTranKey
    gGridHideColumn grdOrderOnHold, kcolOrdTranType
    gGridHideColumn grdOrderOnHold, kColOrdShipToAddrKey
    gGridHideColumn grdOrderOnHold, kColOrdRcvgWhseKey
    gGridHideColumn grdOrderOnHold, kColOrdRcvgWhseID
    gGridHideColumn grdOrderOnHold, kColOrdRcvgWhseDesc
    gGridHideColumn grdOrderOnHold, kColOrdShortPickFlag
    gGridHideColumn grdOrderOnHold, kColOrdSubFlag
    gGridHideColumn grdOrderOnHold, kColOrdStatus
    gGridHideColumn grdOrderOnHold, kcolOrdTranTypeText
    gGridHideColumn grdOrderOnHold, kColOrdShipmentCommitStatus
    gGridHideColumn grdOrderOnHold, kColOrdExtShipmentExists
    gGridHideColumn grdOrderOnHold, kColOrdPacked
   
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupOrderGrid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Private Sub BindGM()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moGMOrder = New clsGridMgr
    With moGMOrder
        Set .Grid = grdOrderOnHold
        Set .DM = moDMOrderGrid
        Set .Form = frmOrdersOnHold
        .GridType = kGridDataSheetNoAppend
        .GridSortEnabled = True
        .Init
    End With
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindGM", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub BindOrderGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moDMOrderGrid = New clsDmGrid
    With moDMOrderGrid
        Set .Session = moSysSession
        Set .Database = moAppDB
        Set .Form = frmOrdersOnHold
        Set .Grid = grdOrderOnHold
        .Table = "#tsoCreatePickHdrWrk"
        .UniqueKey = "TranKey, TranType"
        .NoAppend = True
        .OrderBy = "TranID"
        .Where = "Hold = 1 Or CrHold = 1"
        
        .BindColumn "CrHold", kColOrdCrHold, SQL_SMALLINT
        .BindColumn "EarliestShipDate", kColOrdEarliestShip, SQL_DATE
        .BindColumn "ExpireDate", kColOrdExpiration, SQL_DATE
        .BindColumn "Hold", kColOrdHold, SQL_SMALLINT
        .BindColumn "HoldReason", kColOrdHoldReason, SQL_VARCHAR
        .BindColumn "OrderDate", kColOrdOrderDate, SQL_DATE
        .BindColumn "PickStatus", kColOrdPickCheck, SQL_SMALLINT
        .BindColumn "RcvgWhseKey", kColOrdRcvgWhseKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "RcvgWhseID", kColOrdRcvgWhseID, SQL_VARCHAR
        .BindColumn "RcvgWhseDesc", kColOrdRcvgWhseDesc, SQL_VARCHAR
        .BindColumn "ShipMethID", kColOrdShipVia, SQL_VARCHAR
        .BindColumn "ShipMethKey", Nothing, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipPriority", kColOrdShipPriority, SQL_VARCHAR, , kDmSetNull
        .BindColumn "ShipToAddrKey", kColOrdShipToAddrKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipToCustAddrID", kColOrdShipToAddrID, SQL_VARCHAR
        .BindColumn "ShipWhseID", kColOrdShipFromWhse, SQL_VARCHAR
        .BindColumn "ShipWhseKey", Nothing, SQL_INTEGER, , kDmSetNull
        .BindColumn "Status", kColOrdStatus, SQL_VARCHAR
        .BindColumn "SubstiStatus", kColOrdSubFlag, SQL_VARCHAR
        .BindColumn "TranID", kColOrdOrderNo, SQL_VARCHAR
        .BindColumn "TranKey", kColOrdTranKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "TranType", kcolOrdTranType, SQL_INTEGER
        .BindColumn "TranTypeText", kcolOrdTranTypeText, SQL_VARCHAR
        
        'SGS DEJ for IA (START)
        .LinkSource "tsoSalesOrderExt_SGS", .Table & ".TranKey=tsoSalesOrderExt_SGS.SOKey and " & .Table & ".TranType=801", kDmJoin, LeftOuter
        .Link giGetValidInt(kColOrdBOL), "BOLNo"
        'SGS DEJ for IA (STOP)
        
        .Init
    End With
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindOrderGrid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

'************************************************************************
'   Description:
'       Create the toolbar.
'   Param:
'       <none>
'   Returns:
'************************************************************************
Private Sub BindToolbar()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
  
    With moClass.moFramework
        tbrMain.SecurityLevel = .GetTaskPermission(.GetTaskID())
        tbrMain.LocaleID = mlLanguage
    End With
    
    'Set up the toolbar
    tbrMain.AddButton kTbFinishExit
    If (moClass.moSysSession.ShowOfficeOnToolBar = 1) Then
        tbrMain.AddButton kTbOffice
        tbrMain.AddSeparator
        tbrMain.AddButton kTbHelp
    Else
        tbrMain.AddButton kTbHelp
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindToolbar", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


'************************************************************************
'   Description:
'       Standard Shutdown Procedure.  Unload all child forms
'       and remove any objects created within this app.
'   Param:
'       <none>
'   Returns:
'************************************************************************
Public Sub PerformCleanShutDown()
    On Error GoTo ExpectedErrorRoutine

         
    'Remove all child collections.
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    
    TerminateControls Me
    
    If Not moGMOrder Is Nothing Then
        moGMOrder.UnloadSelf
        Set moGMOrder = Nothing
    End If
    
    If Not moDMOrderGrid Is Nothing Then
        moDMOrderGrid.UnloadSelf
        Set moDMOrderGrid = Nothing
    End If
    
    If Not moItemDist Is Nothing Then
        Set moItemDist = Nothing
    End If
    
    If Not moContextMenu Is Nothing Then
        Set moContextMenu = Nothing
    End If
    
    If Not moAppDB Is Nothing Then
        Set moAppDB = Nothing
    End If
    
    If Not moSysSession Is Nothing Then
        Set moSysSession = Nothing
    End If
    
    If Not moProcessPickList Is Nothing Then
        Set moProcessPickList = Nothing
    End If
    

    'Exit this subroutine
    Exit Sub

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If

    If UnloadMode <> vbFormCode Then
        ProcessSave
        Cancel = True
    Else
        PerformCleanShutDown
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Me.WindowState = vbNormal Or Me.WindowState = vbMaximized Then
        'Resize Height
        gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, fraOrdersOnHold, grdOrderOnHold
          
        'Resize Width
        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, fraOrdersOnHold, grdOrderOnHold
               
        miOldFormHeight = Me.Height
        miOldFormWidth = Me.Width
        
       
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    If Not moClass Is Nothing Then
        Set moClass = Nothing
    End If
    
    

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

'************************************************************************
'   Description:
'       Returns Form Name for Debugging Information.
'   Param:
'       <none>
'   Returns:
'       Form Name
'************************************************************************
Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function

'************************************************************************
'   oClass contains the reference to the parent class object.  The form
'   needs this reference to use the public variables created within the
'   class object.
'************************************************************************
Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moClass = oNewClass

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
'************************************************************************
'   oClass contains the reference to the parent class object.  The form
'   needs this reference to use the public variables created within the
'   class object.
'************************************************************************
Public Property Get oProcessPickList() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oProcessPickList = moProcessPickList
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oProcessPickList", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Property Set oProcessPickList(oNewProcessPickList As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moProcessPickList = oNewProcessPickList

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oProcessPickList", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
Public Property Get oAppDb() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oAppDb = moAppDB
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oAppDB", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Property Set oAppDb(oNewAppDB As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moAppDB = oNewAppDB

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oAppDB", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Private Sub grdOrderOnHold_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

On Error GoTo ExpectedErrorRoutine

    Dim lOrderTranKey As Long
    Dim lOrderTranType As Long
    Dim sWhere As String
    Dim iUpdateCounterViolation As Integer
    
    If mbHoldReleaseFailed Then
        mbHoldReleaseFailed = False
        Exit Sub
    End If
    
    SetMouse
    
    If Row > 0 And Col = kColOrdPickCheck Then
        'Get the TranKey and TranType for the order row clicked
        lOrderTranKey = glGetValidLong(gsGridReadCell(grdOrderOnHold, Row, kColOrdTranKey))
        lOrderTranType = glGetValidLong(gsGridReadCell(grdOrderOnHold, Row, kcolOrdTranType))

        With grdOrderOnHold
            .Col = kColOrdPickCheck
            .Row = Row
            
            If .Value = 1 Then
                'User checked the Pick check box, process security event checking
                'to check whether the user has priviliage to Release the hold
                
                If bProcessHoldRelease(Row) Then
                'user passed the security event checking, go ahead pick all the lines belong to the order
                'that is not on line hold
                
                    'Set up the Where Clause for auto picking
                    sWhere = " ShipLineKey IN (SELECT ShipLineKey FROM #tsoCreatePickWrk2 "
                    sWhere = sWhere & " WHERE OrderKey =" & lOrderTranKey & " AND TranType =" & lOrderTranType
                    sWhere = sWhere & " AND OrderHold = 0 AND LineHold = 0 AND CrHold = 0" & ")"
                
                    'Call the bCreateAutoDistribution function to create the line distribution
                    If Not moProcessPickList.bCreateAutoDistribution(sWhere, miEmptyBins, miEmptyRandomBins, iUpdateCounterViolation) Then
                        Exit Sub
                    End If
                    
                    'Perform update for special items
                    sWhere = " OrderKey =" & lOrderTranKey & " AND TranType =" & lOrderTranType
                    sWhere = sWhere & " AND ItemKey IS NULL"
                    If Not moProcessPickList.bUpdateShipLineForSpecialItems(sWhere) Then
                        giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation  'kmsgimspGeneralReserveInvtTran
                        ResetMouse
                        Exit Sub
                    End If
                Else
                'user failed the security event checking, can not Release the hold, undo the
                'check and exit
                    mbHoldReleaseFailed = True
                    .Value = 0
                    ResetMouse
                    Exit Sub
                End If
            Else
                'User unchecked the Pick check box, go ahead unpick all the lines belong to the order
                
                'Build the where clause for the update
                sWhere = " wrk.ShipLineKey IN (SELECT ShipLineKey FROM #tsoCreatePickWrk2 "
                sWhere = sWhere & " WHERE OrderKey =" & lOrderTranKey & " AND TranType =" & lOrderTranType & ")"
            
                'Call the DeleteShipLines function to delete the IM distribution in the real tables
                'and udpate tsoShipLine, tsoShipLineDist, and the bound
                'temp tables.
                If Not moProcessPickList.bDeleteShipLines(sWhere, kDeleteDistributionOnlyYes, iUpdateCounterViolation) Then
                    Exit Sub
                End If
            End If
        End With
            
        'Set the DM dirty flag for the order changed
        'moDMOrderGrid.SetRowDirty Row
        'moDMOrderGrid.Save
    End If
    
    mbIsDirty = True

    ResetMouse
    
    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    Exit Sub

'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "grdOrderOnHold_ButtonClicked", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdOrderOnHold_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    moGMOrder.Grid_Change Col, Row

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdOrderOnHold_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdOrderOnHold_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    If Row = 0 And Col <> 0 Then
        moDMOrderGrid.Save
    End If
    moGMOrder.Grid_Click Col, Row
    
    'If the user click on any locked cell, highlight the grid row the cell is in
    With grdOrderOnHold
        .Row = Row
        .Col = Col
        If .Lock = True Then
            gGridSetSelectRow grdOrderOnHold, Row
        End If
    End With

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdOrderPicked_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++

End Sub


Private Sub grdOrderOnHold_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    moGMOrder.Grid_EditMode Col, Row, Mode, ChangeMade

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdOrderOnHold_EditMode", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub grdOrderOnHold_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    moGMOrder.Grid_KeyDown keycode, Shift
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdOrderOnHold_KeyDown", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++

End Sub

Private Sub grdOrderOnHold_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    moGMOrder.Grid_LeaveCell Col, Row, NewCol, NewRow

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdOrderOnHold_LeaveCell", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++

End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarClick Button

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub


'************************************************************************
'   Description:
'       Process all toolbar clicks (as well as HotKey shortcuts to
'       toolbar buttons).
'   Param:
'       sKey -  Token returned from toolbar.  Indicates what function
'               is to be executed.
'   Returns:
'************************************************************************
Private Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If

    Me.SetFocus
    DoEvents
      

    Select Case sKey
        Case kTbFinishExit
            ProcessSave
            
        Case kTbHelp
            'The Help button was pressed by the user.
            gDisplayFormLevelHelp Me

        Case Else
            'Error processing
            'Give an error message to the user.
            'Unexpected Operation Value: {0}
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation, _
                         CVar(sKey)
    End Select
    
    ResetMouse
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub



Private Sub ProcessSave()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Me.Hide
    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ProcessSave", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


Private Sub SetFocusCtrl()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'Set focus so that next time form is shown, focus will be at first control
'    lkuItem.Enabled = True
'    lkuItem.SetFocus
  
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetFocusCtrl", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
#End If

#If CUSTOMIZER Then
Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        
        If Not moFormCust Is Nothing Then
            moFormCust.Initialize Me, goClass
            Set moFormCust.CustToolbarMgr = tbrMain
            moFormCust.ApplyDataBindings moDmForm
            moFormCust.ApplyFormCust
        End If
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
#End If




Private Sub cmdOrderLines_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdOrderLines(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdOrderLines_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdOrderLines_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdOrderLines(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdOrderLines_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then
Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomButton(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomButton(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomButton(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomCheck(Index)
    End If
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomCheck(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomCheck(Index)
    End If
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnChange CustomCombo(Index)
    End If
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomCombo(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnDblClick CustomCombo(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomCombo(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomCombo(Index)
    End If
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnChange CustomCurrency(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomCurrency(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomCurrency(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomFrame(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnDblClick CustomFrame(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomLabel(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnDblClick CustomLabel(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnChange CustomMask(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomMask(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnKeyPress CustomMask(Index), KeyAscii
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomMask(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnChange CustomNumber(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomNumber(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomNumber(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomOption(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnDblClick CustomOption(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomOption(Index)
    End If
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomOption(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnSpinDown CustomSpin(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnSpinUp CustomSpin(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
#End If

#If CUSTOMIZER And CONTROLS Then
Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomDate(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnDblClick CustomDate(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomDate(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomDate(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnKeyPress CustomDate(Index), KeyAscii
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnChange CustomDate(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
#End If

Public Property Get MyApp() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set MyApp = App

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MyApp", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Property Get MyForms() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set MyForms = Forms
    
    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MyForms", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Private Sub SetupRowsSelCaption()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'********************************************************************
' Description:
'    This routine updates the select grids's frame caption for
'    the current row count in the grid
'********************************************************************
Dim iOrdNbrSelected    As Integer

    iOrdNbrSelected = glGridGetDataRowCnt(grdOrderOnHold)
    
    If iOrdNbrSelected = 0 Then
        fraOrdersOnHold.Caption = iOrdNbrSelected & " " & msOrderOnHold
        'tbrMain.ButtonEnabled(kTbProceed) = False
    Else
        'tbrMain.ButtonEnabled(kTbProceed) = True
            
        If iOrdNbrSelected > 1 Then
            fraOrdersOnHold.Caption = iOrdNbrSelected & " " & msOrdersOnHold
        Else
            fraOrdersOnHold.Caption = iOrdNbrSelected & " " & msOrderOnHold
        End If
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupRowsSelCaption", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function bProcessHoldRelease(ByVal lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'********************************************************************
' Description:
'    This function will perform the necessary check for release
'    hold on the orders to pick
'********************************************************************
On Error GoTo ExpectedErrorRoutine

    Dim lOrderTranKey As Long
    Dim lOrderTranType As Long
    Dim iHold As Integer
    Dim iCrHold As Integer
    Dim lRetVal As Long
    Dim sSQL As String
    Dim vPrompt As Variant
    
    bProcessHoldRelease = False
    
    SetMouse
    
    If lRow > 0 Then
        
        lOrderTranKey = glGetValidLong(gsGridReadCell(grdOrderOnHold, lRow, kColOrdTranKey))
        lOrderTranType = glGetValidLong(gsGridReadCell(grdOrderOnHold, lRow, kcolOrdTranType))
        iHold = giGetValidInt(gsGridReadCell(grdOrderOnHold, lRow, kColOrdHold))
        iCrHold = giGetValidInt(gsGridReadCell(grdOrderOnHold, lRow, kColOrdCrHold))

        If iCrHold = 1 And Not moProcessPickList.bOverrideSecEvent(kSecEventRemoveCrHold) Then
            'User does not have the security level at Normal and Higher, deny the request
            giSotaMsgBox Me, moClass.moSysSession, kSOmsgNoPermitToReleaseHold
            ResetMouse
            Exit Function
        End If
        
        If iHold = 1 And Not moProcessPickList.bOverrideSecEvent(kSecEventRemoveHold) Then
            'User does not have the security level at Normal and Higher, deny the request
            giSotaMsgBox Me, moClass.moSysSession, kSOmsgNoPermitToReleaseHold
            ResetMouse
            Exit Function
        End If
    
        'Call sp to release the hold and perform necessary update to the database
        On Error Resume Next
        With moAppDB
            .SetInParam iCrHold             '@_iReleaseCrHold
            .SetInParam iHold               '@_iReleaseOrderHold
            .SetInParam 0                   '@_iReleaseLineHold
            .SetInParam lOrderTranKey       '@_iTranKey
            .SetInParam lOrderTranType      '@_iTranType
            .SetInParam 0                   '@_iShipLineKey
            .SetInParam oClass.moSysSession.UserId  '@_iUserID
            .SetOutParam lRetVal            '@_oRetVal
            .ExecuteSP "spsoReleaseSalesOrderHold"
            lRetVal = .GetOutParam(8)
            .ReleaseParams
        End With
        
        If lRetVal <> 1 Or Err.Number <> 0 Then
            giSotaMsgBox moClass, moClass.moSysSession, kmsgProc, "spsoReleaseSalesOrderHold"
            ResetMouse
            Exit Function
        End If
        
        On Error GoTo ExpectedErrorRoutine
        
        'User has the security level at Normal and Higher, Release the hold
        If iCrHold = 1 Then
            gGridUpdateCell grdOrderOnHold, lRow, kColOrdCrHold, CStr(0)
        End If
        
        If iHold = 1 Then
            gGridUpdateCell grdOrderOnHold, lRow, kColOrdHold, CStr(0)
            gGridUpdateCell grdOrderOnHold, lRow, kColOrdHoldReason, ""
        End If
        
    End If
    
    ResetMouse
    bProcessHoldRelease = True
    
    Exit Function
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    Exit Function
    
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bProcessHoldRelease", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function




