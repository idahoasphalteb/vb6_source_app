VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#84.0#0"; "LookupView.ocx"
Begin VB.Form frmOrderPickLines 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Order Pick Lines"
   ClientHeight    =   7665
   ClientLeft      =   1170
   ClientTop       =   1440
   ClientWidth     =   10860
   HelpContextID   =   17774287
   Icon            =   "sozdd102.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7665
   ScaleWidth      =   10860
   Begin VB.Frame fraLinePicked 
      Caption         =   "0 Lines Selected For Picking"
      Height          =   5275
      Left            =   240
      TabIndex        =   1
      Top             =   1920
      Width           =   10305
      Begin VB.Frame fraShow 
         Caption         =   "Show"
         Height          =   495
         Left            =   4080
         TabIndex        =   2
         Top             =   120
         Width           =   3135
         Begin VB.OptionButton OptLineShow 
            Caption         =   "O&nly Short Picks"
            Height          =   200
            Index           =   1
            Left            =   1080
            TabIndex        =   4
            Top             =   220
            WhatsThisHelpID =   17774288
            Width           =   1695
         End
         Begin VB.OptionButton OptLineShow 
            Caption         =   "&All"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   3
            Top             =   220
            Value           =   -1  'True
            WhatsThisHelpID =   17774289
            Width           =   975
         End
      End
      Begin VB.CommandButton cmdDist 
         Caption         =   "Di&st..."
         Enabled         =   0   'False
         Height          =   375
         Left            =   9435
         TabIndex        =   10
         Top             =   240
         WhatsThisHelpID =   17774290
         Width           =   615
      End
      Begin VB.CommandButton cmdDeleteAll 
         Caption         =   "&Delete All"
         Height          =   375
         Left            =   2520
         TabIndex        =   7
         Top             =   240
         WhatsThisHelpID =   17774291
         Width           =   1065
      End
      Begin VB.CommandButton cmdPickAll 
         Caption         =   "&Pick All"
         Height          =   375
         Left            =   1150
         TabIndex        =   6
         Top             =   240
         WhatsThisHelpID =   17774292
         Width           =   1000
      End
      Begin VB.CommandButton cmdClearAll 
         Caption         =   "&Clear All"
         Height          =   375
         Left            =   120
         TabIndex        =   5
         Top             =   240
         WhatsThisHelpID =   17774293
         Width           =   1000
      End
      Begin VB.CommandButton cmdComponents 
         Caption         =   "&Kit Components..."
         Enabled         =   0   'False
         Height          =   375
         Left            =   7920
         TabIndex        =   9
         Top             =   240
         WhatsThisHelpID =   17774294
         Width           =   1455
      End
      Begin VB.TextBox txtNavSubItem 
         Height          =   285
         Left            =   0
         TabIndex        =   24
         Text            =   "Text1"
         Top             =   0
         Visible         =   0   'False
         WhatsThisHelpID =   17774295
         Width           =   1425
      End
      Begin VB.TextBox txtNavUOM 
         Height          =   285
         Left            =   1860
         TabIndex        =   23
         Text            =   "Text1"
         Top             =   0
         Visible         =   0   'False
         WhatsThisHelpID =   17774296
         Width           =   1425
      End
      Begin FPSpreadADO.fpSpread grdLinePicked 
         Height          =   4455
         Left            =   120
         TabIndex        =   8
         Top             =   720
         WhatsThisHelpID =   17776603
         Width           =   10035
         _Version        =   524288
         _ExtentX        =   17701
         _ExtentY        =   7858
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         SpreadDesigner  =   "sozdd102.frx":23D2
         AppearanceStyle =   0
      End
      Begin LookupViewControl.LookupView navGrid 
         Height          =   285
         Index           =   1
         Left            =   11200
         TabIndex        =   25
         TabStop         =   0   'False
         ToolTipText     =   "Lookup"
         Top             =   240
         Visible         =   0   'False
         WhatsThisHelpID =   17774297
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   503
         LookupMode      =   1
      End
      Begin LookupViewControl.LookupView navGridUOM 
         Height          =   285
         Left            =   3300
         TabIndex        =   26
         TabStop         =   0   'False
         ToolTipText     =   "Lookup"
         Top             =   0
         Visible         =   0   'False
         WhatsThisHelpID =   17774298
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   503
         LookupMode      =   1
      End
      Begin LookupViewControl.LookupView navGridSubItem 
         Height          =   285
         Left            =   1440
         TabIndex        =   27
         TabStop         =   0   'False
         ToolTipText     =   "Lookup"
         Top             =   0
         Visible         =   0   'False
         WhatsThisHelpID =   17774299
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   503
         LookupMode      =   1
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtlkuAutoDistBin 
         Height          =   255
         Left            =   6750
         TabIndex        =   36
         Top             =   0
         Visible         =   0   'False
         WhatsThisHelpID =   17774300
         Width           =   1215
         _Version        =   65536
         _ExtentX        =   2143
         _ExtentY        =   450
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin LookupViewControl.LookupView lkuAutoDistBin 
         Height          =   285
         Left            =   8000
         TabIndex        =   37
         TabStop         =   0   'False
         ToolTipText     =   "Lookup"
         Top             =   0
         Visible         =   0   'False
         WhatsThisHelpID =   17774301
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   503
         LookupMode      =   1
      End
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   7275
      WhatsThisHelpID =   73
      Width           =   10860
      _ExtentX        =   19156
      _ExtentY        =   688
      BrowseVisible   =   0   'False
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   22
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   16
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   17
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   11
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   10860
      _ExtentX        =   19156
      _ExtentY        =   741
      Style           =   6
   End
   Begin VB.Label lblAddress 
      Height          =   1080
      Left            =   3480
      TabIndex        =   38
      Top             =   740
      Width           =   4335
   End
   Begin VB.Label lblShipToAddrName 
      Caption         =   "Ship To Name / Whse Desc"
      Height          =   240
      Left            =   5520
      TabIndex        =   35
      Top             =   480
      Width           =   4215
   End
   Begin VB.Label lblShipToID 
      Caption         =   "CustID / WhseID"
      Height          =   240
      Left            =   3480
      TabIndex        =   34
      Top             =   480
      Width           =   1815
   End
   Begin VB.Label lblShipToCaption 
      Caption         =   "Ship To:"
      Height          =   240
      Left            =   2400
      TabIndex        =   33
      Top             =   480
      Width           =   855
   End
   Begin VB.Label lblAddressCaption 
      Caption         =   "Address:"
      Height          =   240
      Left            =   2400
      TabIndex        =   32
      Top             =   740
      Width           =   855
   End
   Begin VB.Label lblTranType 
      Caption         =   "TranType"
      Height          =   240
      Left            =   840
      TabIndex        =   31
      Top             =   740
      Width           =   1215
   End
   Begin VB.Label lblTranNo 
      Caption         =   "TranNo"
      Height          =   240
      Left            =   840
      TabIndex        =   30
      Top             =   480
      Width           =   1215
   End
   Begin VB.Label lblTranTypeCaption 
      Caption         =   "Type:"
      Height          =   240
      Left            =   120
      TabIndex        =   29
      Top             =   740
      Width           =   615
   End
   Begin VB.Label lblOrderCaption 
      Caption         =   "Order:"
      Height          =   240
      Left            =   120
      TabIndex        =   28
      Top             =   480
      Width           =   615
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   21
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmOrderPickLines"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************************
'     Name: frmOrderPickLines
'     Desc: Order Pick Lines
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: JY 04-01/2004
'     Mods:
'************************************************************************************

Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

Private Declare Function AllowSetForegroundWindow Lib "user32" (ByVal dwProcessID As Long) As Boolean
Private Const ASFW_ANY = -1

    Private mbUserSelectedItem As Boolean
    
    'Private Variables of Form Properties
    Private mbEnterAsTab                As Boolean
    Private moClass                     As Object
    Private moSysSession                As Object
    Private moAppDB                     As Object                   'Class Reference
    Private mbCancelShutDown            As Boolean                  'Cancel Shutdown Flag
    Private mbIsDirty                   As Boolean                  'Form dirty?
    Private msCurrencyID                As String
    Private msCompanyID                 As String
    Private mlLanguage                  As Long
    Private mbIMActivated               As Boolean
    Private WithEvents moDMLineGrid     As clsDmGrid
Attribute moDMLineGrid.VB_VarHelpID = -1
    Private WithEvents moGMLine         As clsGridMgr
Attribute moGMLine.VB_VarHelpID = -1
    Private moGridNavUOM                As clsGridLookup
    Private moGridNavSubItem            As clsGridLookup
    Private moGridLkuAutoDistBin        As clsGridLookup
    Private mlTranKey                   As Long
    Private mlTranType                  As Long
    Private miCallingForm               As OrderLineCallingForm
    Private moItemDist                  As Object
    Private miMousePointer              As Integer
    Private mlTaskID                    As Long
    Private mbShipmentGenerated         As Boolean
    Private miEmptyBins                 As Integer
    Private miEmptyRandomBins           As Integer
    Private moProcessPickList           As clsProcessPickList
    Private mbCellChangeFired           As Boolean
    Private mlCurrentActiveColumn       As Long
    Private mlCurrentLineActiveRow          As Long
    Private mlCurrentRowShipLineKey     As Long
    
    'Public Form Variables
    Public moSotaObjects                As New Collection           'Collection of Loaded Objects
       
    'Minimum Form Size
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long

    'Binding Object Variables
    Private moContextMenu               As clsContextMenu       'Context Menu

    'Miscellaneous Variables
    Private m_LastEnterCol As Long
    Private m_LastEnterRow As Long
    
    'Message box return values
    Const kCancel = 2

Const VBRIG_MODULE_ID_STRING = "OrderPickLines.frm"
Private Sub ClearForm()
'if you add a control of a different type to the form, will need to modify
'this routine.
On Error GoTo ExpectedErrorRoutine
Dim ctrl As Object

    On Error Resume Next
    For Each ctrl In frmOrderPickLines.CONTROLS
    
        If TypeOf ctrl Is SOTACurrency Then
            ctrl.ClearData
            ctrl.Enabled = True
        ElseIf TypeOf ctrl Is SOTANumber Then
            ctrl.ClearData
            ctrl.Enabled = True
        ElseIf TypeOf ctrl Is CheckBox Then
            ctrl.Value = vbUnchecked
            ctrl.Tag = ""
            ctrl.Enabled = True
        ElseIf TypeOf ctrl Is TextLookup Then
            ctrl.ClearData
            ctrl.Enabled = True
        ElseIf TypeOf ctrl Is SOTADropDown Then
            ctrl.Clear
            ctrl.Enabled = True
        ElseIf TypeOf ctrl Is SOTAMaskedEdit Then
            ctrl.ClearData
            ctrl.Enabled = True
        End If
                
    Next ctrl
    On Error GoTo ExpectedErrorRoutine
    
    Exit Sub
    
ExpectedErrorRoutine:
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ClearForm", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Friend Sub LoadOrderDetails(uOrderDetail As OrderDetail, ByRef oDist As Object, _
                        iCaller As Integer, lTaskID As Long, bShipmentGenerated As Boolean, _
                        iEmptyBins As Integer, iEmptyRandomBins As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Set moItemDist = oDist
    miCallingForm = iCaller
    mlTaskID = lTaskID
    mbShipmentGenerated = bShipmentGenerated
    miEmptyBins = iEmptyBins
    miEmptyRandomBins = iEmptyRandomBins
    
    'Set up the controls base on OrderLineCallingForm
    If miCallingForm = kPickLineMain Then
    'The form is loaded by the "Pick Order Line" button from the Main
    'form Orders To Pick tab
        fraShow.Enabled = True
        OptLineShow(kShowAll).Enabled = True
        OptLineShow(kShowShortOnly).Enabled = True
        
        If mlTaskID = ktskReprintPickList Then
            cmdDeleteAll.Visible = False
        End If
        
        If mbShipmentGenerated Then
            cmdClearAll.Enabled = False
            cmdPickAll.Enabled = False
            
            If cmdDeleteAll.Visible = True Then
                cmdDeleteAll.Enabled = False
            End If
        Else
            cmdClearAll.Enabled = True
            cmdPickAll.Enabled = True
            
            If cmdDeleteAll.Visible = True Then
                cmdDeleteAll.Enabled = True
            End If
        End If
    Else
    'The form is loaded by the "Order Lines..." button from the Orders on Hold form
    'disable all the controls make the form a read-only form
        fraShow.Enabled = False
        OptLineShow(kShowAll).Enabled = False
        OptLineShow(kShowShortOnly).Enabled = False
        cmdClearAll.Enabled = False
        cmdPickAll.Enabled = False
        cmdDeleteAll.Enabled = False
    End If
    
    'Update value on the Order Pick Line Form
    UpdateControls uOrderDetail
    
    'Refresh frame captions for display grids
    SetupRowsSelCaption
    
    'Set focus to first grid row
    grdLinePicked_Click kColLineItem, 1

    bIsDirty = False

    Me.Show vbModal
                  
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LoadOrderDetails", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Public Property Get bIsDirty() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    bIsDirty = mbIsDirty
    
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsDirty", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Property Let bIsDirty(bFormDirty As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    mbIsDirty = bFormDirty

'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsDirty", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
Private Sub UpdateControls(uOrderDetail As OrderDetail)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Load form controls and module level variables from muLineDetails
    Dim sSQL As String
    Dim rs As Object
    Dim sShipToAddrLine1 As String
    Dim sShipToAddrLine2 As String
    Dim sShipToAddrLine3 As String
    Dim sShipToCity As String
    Dim sShipToState As String
    Dim sShipToPostalCode As String
    Dim sShipToLatLong As String
    Dim sAddress As String

    With uOrderDetail
        lblTranNo.Caption = .TranNo
        lblTranType.Caption = .TranTypeID
        lblShipToAddrName.Caption = .ShipToAddrName
        mlTranKey = .TranKey
        mlTranType = .TranType
        sShipToLatLong = ""
        If .TranType = kTranTypeIMTR Then
        'Transfer Order
            lblShipToID.Caption = .ShipToWhseID

            'Look up the RcvgWhse Address info
            sSQL = "SELECT AddrLine1, AddrLine2, AddrLine3, City, StateID, PostalCode, Latitude, Longitude "
            sSQL = sSQL & " FROM tciAddress ad WITH (NOLOCK) JOIN timWarehouse wh WITH (NOLOCK)"
            sSQL = sSQL & " ON ad.AddrKey = wh.ShipAddrKey AND wh.WhseKey = " & .ShipToWhseKey

            Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
            
            If Not rs.IsEOF Then
                sShipToAddrLine1 = gsGetValidStr(rs.Field("AddrLine1"))
                sShipToAddrLine2 = gsGetValidStr(rs.Field("AddrLine2"))
                sShipToAddrLine3 = gsGetValidStr(rs.Field("AddrLine3"))
                sShipToCity = gsGetValidStr(rs.Field("City"))
                sShipToState = gsGetValidStr(rs.Field("StateID"))
                sShipToPostalCode = gsGetValidStr(rs.Field("PostalCode"))
                If gsGetValidStr(rs.Field("Latitude")) <> "" And gsGetValidStr(rs.Field("Longitude")) <> "" Then
                    sShipToLatLong = "Latitude " & gsGetValidStr(rs.Field("Latitude")) & " Longitude " & gsGetValidStr(rs.Field("Longitude"))
                End If
            End If
            
            rs.Close
            Set rs = Nothing
           
        Else
        'Sales Order
            lblShipToID.Caption = .ShipToAddrID
            
            'Look up the ShipTo Customer Address info
            sSQL = "SELECT AddrLine1, AddrLine2, AddrLine3, City, StateID, PostalCode, AddrName, Latitude, Longitude "
            sSQL = sSQL & " FROM tciAddress WITH (NOLOCK) WHERE AddrKey = " & .ShipToAddrKey

            Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
            
            If Not rs.IsEOF Then
                sShipToAddrLine1 = gsGetValidStr(rs.Field("AddrLine1"))
                sShipToAddrLine2 = gsGetValidStr(rs.Field("AddrLine2"))
                sShipToAddrLine3 = gsGetValidStr(rs.Field("AddrLine3"))
                sShipToCity = gsGetValidStr(rs.Field("City"))
                sShipToState = gsGetValidStr(rs.Field("StateID"))
                sShipToPostalCode = gsGetValidStr(rs.Field("PostalCode"))
                lblShipToAddrName.Caption = gsGetValidStr(rs.Field("AddrName"))
                If gsGetValidStr(rs.Field("Latitude")) <> "" And gsGetValidStr(rs.Field("Longitude")) <> "" Then
                    sShipToLatLong = "Latitude " & gsGetValidStr(rs.Field("Latitude")) & " Longitude " & gsGetValidStr(rs.Field("Longitude"))
                End If
            End If
            
            rs.Close
            Set rs = Nothing
        End If
        
        sAddress = sShipToAddrLine1
        If Len(Trim(sShipToAddrLine2)) > 0 Then
            sAddress = sAddress + vbCrLf + sShipToAddrLine2
        End If
        If Len(Trim(sShipToAddrLine3)) > 0 Then
            sAddress = sAddress + vbCrLf + sShipToAddrLine3
        End If
        sAddress = sAddress + vbCrLf + sShipToCity + "," + "  " + sShipToState + "  " + sShipToPostalCode + IIf(sShipToLatLong <> "", vbCrLf + sShipToLatLong, "")
    
        lblAddress.Caption = sAddress
    End With
    
    'Setup the DM to TranKey to be displayed
    'moDMLineGrid. = "TranKey = " & mlTranKey & " AND TranType = " & mlTranType
    grdLinePicked.redraw = False
    moDMLineGrid.Refresh
    grdLinePicked.redraw = True
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "UpdateControls", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

'************************************************************************
'   bCancelShutDown tells the framework whether the form has requested
'   the shutdown process to be cancelled.
'************************************************************************
Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancelShutDown = mbCancelShutDown

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCancelShutDown", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Private Sub SetMouse()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'    Desc: Sets Mouse to a Busy Value.
'   Parms: N/A
' Returns: N/A
'***********************************************************************
    If Me.MousePointer <> vbHourglass Then
        Me.MousePointer = vbHourglass
    End If
  
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetMouse", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub ResetMouse()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'    Desc: Resets Mouse to Previous Value.
'   Parms: N/A
' Returns: N/A
'***********************************************************************
    Me.MousePointer = miMousePointer

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ResetMouse", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub



'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & 'Z' & <FormType>
'
'   <Module>   is 'CI', 'AP', 'GL', . . .
'   'Z'        is the SOTA identifier.
'   <FormType> is 'M' = Maintenance, 'D' = data entry, 'I' = Inquiry,
'                 'P' = PeriodEnd, 'R' = Reports, 'L' = Listings, . . .
'************************************************************************
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    FormHelpPrefix = "SOZ"
    
    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & 'Z' & <FormType>
'
'   <Module>   is 'CI', 'AP', 'GL', . . .
'   'Z'        is the SOTA identifier.
'************************************************************************
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    WhatHelpPrefix = "SOZ"

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Function CMMenuSelected(ctl As Control, lTaskID As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim lActiveRow As Long
Dim lShipLineKey As Long
Dim sWhere As String
Dim lLineTranKey As Long
Dim lLineTranType As Long
Dim lRowCount As Long
Dim sSQL As String
Dim lShipLineCount As Long
Dim oDDn As Object
Dim sTranNo As String
Dim sParam As String
Dim sItemID As String
Dim lItemKey As Long
Dim lWhseKey As Long
Dim iUpdateCounterViolation As Integer
Dim bRetVal As Boolean
Dim iCustShipComplete As Integer
Dim lOrderTranType As Long

    CMMenuSelected = False
    
    If ctl Is grdLinePicked Then
        lActiveRow = grdLinePicked.ActiveRow
        If lActiveRow <= 0 Then
            Exit Function
        Else
            lLineTranKey = glGetValidLong(gsGridReadCell(grdLinePicked, lActiveRow, kColLineTranKey))
            lLineTranType = glGetValidLong(gsGridReadCell(grdLinePicked, lActiveRow, kColLineTranType))
        
            Select Case lTaskID
            Case kMenuEnterSalesOrder
            
                On Error Resume Next
                ' For Vista OS (to ensure task comes to foreground)
                bRetVal = AllowSetForegroundWindow(ASFW_ANY)
                Err.Clear
                
                #If ERRORTRAPON Then
                On Error GoTo VBRigErrorRoutine
                #End If
            
                Set oDDn = moClass.moFramework.LoadSOTAObject(ktskSOEntry, kSOTA1RunFlags, kContextNormal)
                If oDDn Is Nothing Then
                    sParam = gsBuildString(kSOSalesOrder, moClass.moSysDB, moClass.moSysSession)
                    giSotaMsgBox Me, moClass.moSysSession, kmsgErrorLoadTask, sParam
                Else
                    oDDn.DrillAround lLineTranKey
                End If
                
            Case kMenuEnterTransferOrder
                sTranNo = gsGetValidStr(gsGridReadCell(grdLinePicked, lActiveRow, kColLineOrderNo))
            
                On Error Resume Next
                ' For Vista OS (to ensure task comes to foreground)
                bRetVal = AllowSetForegroundWindow(ASFW_ANY)
                Err.Clear
                
                #If ERRORTRAPON Then
                On Error GoTo VBRigErrorRoutine
                #End If
            
                Set oDDn = moClass.moFramework.LoadSOTAObject(ktskIMEnterTrnsfrOrder, kSOTA1RunFlags, kContextNormal)
                If oDDn Is Nothing Then
                    sParam = gsBuildString(kIMTrnsfrOrd, moClass.moSysDB, moClass.moSysSession)
                    giSotaMsgBox Me, moClass.moSysSession, kmsgErrorLoadTask, sParam
                Else
                    oDDn.DrillAround sTranNo
                End If
                
            Case kMenuMaintItem
                sItemID = gsGetValidStr(gsGridReadCell(grdLinePicked, lActiveRow, kColLineItem))
            
                On Error Resume Next
                ' For Vista OS (to ensure task comes to foreground)
                bRetVal = AllowSetForegroundWindow(ASFW_ANY)
                Err.Clear
                
                #If ERRORTRAPON Then
                On Error GoTo VBRigErrorRoutine
                #End If
            
                Set oDDn = moClass.moFramework.LoadSOTAObject(117506178, kSOTA1RunFlags, kContextNormal) 'ktskIMItemMaint
                If oDDn Is Nothing Then
                    sParam = "Maintain Item" 'gsBuildString(kIMTrnsfrOrd, moClass.moSysDB, moClass.moSysSession)
                    giSotaMsgBox Me, moClass.moSysSession, kmsgErrorLoadTask, sParam
                Else
                    oDDn.DrillAround sItemID
                End If
                
            Case kMenuMaintInventory
                lItemKey = glGetValidLong(gsGridReadCell(grdLinePicked, lActiveRow, kColLineItemKey))
                lWhseKey = glGetValidLong(gsGridReadCell(grdLinePicked, lActiveRow, kColLineShipWhseKey))
            
                On Error Resume Next
                ' For Vista OS (to ensure task comes to foreground)
                bRetVal = AllowSetForegroundWindow(ASFW_ANY)
                Err.Clear
                
                #If ERRORTRAPON Then
                On Error GoTo VBRigErrorRoutine
                #End If
            
                Set oDDn = moClass.moFramework.LoadSOTAObject(117506238, kSOTA1RunFlags, kContextNormal) 'ktskIMInventoryMain
                If oDDn Is Nothing Then
                    sParam = "Maintain Inventory" 'gsBuildString(kIMTrnsfrOrd, moClass.moSysDB, moClass.moSysSession)
                    giSotaMsgBox Me, moClass.moSysSession, kmsgErrorLoadTask, sParam
                Else
                    oDDn.DrillAround lItemKey, lWhseKey
                End If

            Case kMenuDeleteLine
                'Warn before line deletion if Cust requires ship complete and this is not the only line in the so
                lOrderTranType = glGetValidLong(gsGridReadCell(grdLinePicked, lActiveRow, kColLineTranType))
                iCustShipComplete = giGetValidInt(gsGridReadCell(grdLinePicked, lActiveRow, kColLineCustReqShipComplete))
                If lOrderTranType = kSOTranType Then
                    If iCustShipComplete = 1 Then
                        If moProcessPickList.bShipCompleteWarnOnDelete(grdLinePicked, lActiveRow) Then
                            If giSotaMsgBox(Me, moClass.moSysSession, kSOPartialPickWarn, "") = vbNo Then
                                Exit Function
                            End If
                        End If
                    End If
                End If
                
                'Delete the shipline associated with this ship line
                lShipLineKey = glGetValidLong(gsGridReadCell(grdLinePicked, lActiveRow, kColLineShipLineKey))
                sWhere = " wrk.ShipLineKey =" & lShipLineKey
                
                If Not moProcessPickList.bDeleteShipLines(sWhere, kDeleteDistributionOnlyNo, iUpdateCounterViolation) Then
                    Exit Function
                End If
                
                If iUpdateCounterViolation = 0 Then
                    moGMLine.GridDelete
                    moDMLineGrid.Save
                    mbIsDirty = True
                End If
                
                grdLinePicked.redraw = False
                moDMLineGrid.Refresh
                
                'Refresh frame captions for display grids
                SetupRowsSelCaption
                
                grdLinePicked.redraw = True
            End Select
        End If
    End If
    
    Err.Clear
    CMMenuSelected = True

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CMMenuSelected", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function CMAppendContextMenu(ctl As Control, hmenu As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    CMAppendContextMenu = False

    Dim sDeleteLine As String
    Dim lSOSO As Long
    Dim lTOTO As Long
    Dim lCUST As Long
    Dim lINVT As Long
    Dim lRow As Long
    Dim lTranType As Long
    Dim lOrderKey As Long
    Dim lItemType As Long
    Dim iShipmentCommitStatus As Integer
    
    lSOSO = MF_GRAYED
    lTOTO = MF_GRAYED
    lCUST = MF_GRAYED
    lINVT = MF_GRAYED

    If ctl Is grdLinePicked Then
    
        lRow = glGetValidLong(ctl.ActiveRow)
        
        If lRow > 0 Then
            lTranType = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineTranType))
            lOrderKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineTranKey))
            lItemType = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineItemType))
           
            If lOrderKey > 0 Then
                If lTranType = kTranTypeIMTR Then
                    lTOTO = MF_ENABLED
                Else
                    lSOSO = MF_ENABLED
                    lCUST = MF_ENABLED
                End If
            End If
            
            If (lItemType = IMS_RAW_MATERIAL Or lItemType = IMS_FINISHED_GOOD _
                    Or lItemType = IMS_ASSEMBLED_KIT) Then
                lINVT = MF_ENABLED
            End If
            
            AppendMenu hmenu, lSOSO, kMenuEnterSalesOrder, gsBuildString(kSOSalesOrder, moAppDB, moClass.moSysSession)  '"Enter SO"
            'AppendMenu hmenu, lSOSO, kMenuSOLineInq, gsBuildString(kSOSOLine, moappdb, moClass.moSysSession)  '"Enter SO"
            AppendMenu hmenu, MF_GRAYED, kMenuSOLineInq, gsBuildString(kSOSOLine, moAppDB, moClass.moSysSession)  '"Enter SO"
            AppendMenu hmenu, lTOTO, kMenuEnterTransferOrder, gsBuildString(kIMTrnsfrOrd, moAppDB, moClass.moSysSession)   '"SO Shippment Summary""Shippment Summary" '
            'AppendMenu hmenu, lTOTO, kMenuTOLineInq, gsBuildString(kIMTrnsfrOrdLine, moappdb, moClass.moSysSession)   '"SO Shippment Summary""Shippment Summary" '
            AppendMenu hmenu, MF_GRAYED, kMenuTOLineInq, gsBuildString(kIMTrnsfrOrdLine, moAppDB, moClass.moSysSession)   '"SO Shippment Summary""Shippment Summary" '
            AppendMenu hmenu, MF_ENABLED, kMenuMaintItem, "&" & gsBuildString(kIMItem, moAppDB, moClass.moSysSession)   '"SO Invoice Summary""Invoice Summary" '
            AppendMenu hmenu, lINVT, kMenuMaintInventory, gsBuildString(kInvent, moAppDB, moClass.moSysSession)   '"SO Shippment Summary""Shippment Summary" '
            'AppendMenu hmenu, lINVT, kMenuInventoryStatusInq, gsBuildString(kIMInventoryStatus, moappdb, moClass.moSysSession)   '"SO Shippment Summary""Shippment Summary" '
            AppendMenu hmenu, MF_GRAYED, kMenuInventoryStatusInq, gsBuildString(kIMInventoryStatus, moAppDB, moClass.moSysSession)   '"SO Shippment Summary""Shippment Summary" '
            'AppendMenu hmenu, lINVT, kMenuStockStatusInq, "&" & gsBuildString(kIMStokStatus, moappdb, moClass.moSysSession)   '"SO Invoice Summary""Invoice Summary" '
            AppendMenu hmenu, MF_GRAYED, kMenuStockStatusInq, "&" & gsBuildString(kIMStokStatus, moAppDB, moClass.moSysSession)   '"SO Invoice Summary""Invoice Summary" '
        
            iShipmentCommitStatus = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineShipmentCommitStatus))
            If iShipmentCommitStatus = 0 And (moProcessPickList.iSecurityLevel = kSecLevelNormal Or moProcessPickList.iSecurityLevel = kSecLevelSupervisory) Then
                sDeleteLine = gsBuildString(kDeleteLine, moAppDB, moClass.moSysSession)
                AppendMenu hmenu, MF_ENABLED, kMenuDeleteLine, sDeleteLine
            End If
        
        End If
    End If
    
    CMAppendContextMenu = True
    
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CMAppendContextMenu", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***************************************************
'  Instantiate the Context Menu Class.
'***************************************************
    Set moContextMenu = New clsContextMenu  'Instantiate Context Menu Class

    With moContextMenu
        
        Set .Form = frmOrderPickLines
        .BindGrid moGMLine, grdLinePicked.hwnd
        .Bind "*APPEND", grdLinePicked.hwnd
        .Init
    End With

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub



Private Sub cmdClearAll_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdClearAll, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'**********************************************************************
' Description:
'    This routine deletes the distributions for all the Short Pick
'    lines.
'**********************************************************************
On Error GoTo ExpectedErrorRoutine

    Dim sWhere As String
    Dim sShortPickSelected As String
    Dim iUpdateCounterViolation As Integer

    SetMouse
    
    If OptLineShow(kShowShortOnly).Value = True Then
    'If only short pick is displayed,  then only clear the short pick ship lines
        sShortPickSelected = " AND ShortPick = 1"
    Else
        sShortPickSelected = ""
    End If

    'Build the Where clause for the lines that need to delete distribution
    sWhere = " wrk.ShipLineKey IN (SELECT ShipLineKey FROM #tsoCreatePickWrk2 WHERE LineHold <> 1 AND CrHold <> 1 AND OrderHold <> 1"
    sWhere = sWhere & " AND OrderKey = " & mlTranKey & " AND TranType = " & mlTranType & sShortPickSelected & ")"
    
    'Call DeleteShipLines to delete the distribution for the select lines.
    If Not moProcessPickList.bDeleteShipLines(sWhere, kDeleteDistributionOnlyYes, iUpdateCounterViolation) Then
        Exit Sub
    End If
    
    If Err.Number <> 0 Then
        gClearSotaErr
    End If
    
    grdLinePicked.redraw = False
    moDMLineGrid.Refresh
    grdLinePicked.redraw = True
    
    ResetMouse

    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    Exit Sub

'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdClearAll_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++

End Sub

Private Sub cmdComponents_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdComponents, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++

On Error GoTo ExpectedErrorRoutine

    SetMouse
    
    Dim lRow            As Long
    
    'Get the active row
    lRow = glGridGetActiveRow(grdLinePicked)
    
    'Store the Current cell value in the variable
    mlCurrentLineActiveRow = lRow
    mlCurrentActiveColumn = glGridGetActiveCol(grdLinePicked)
    mlCurrentRowShipLineKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineShipLineKey))
    
    Me.Enabled = False
    
    If Not moItemDist Is Nothing Then
        If Not moProcessPickList.bLoadCompForm(grdLinePicked, lRow, mbShipmentGenerated, _
                        miEmptyBins, miEmptyRandomBins) Then
            giSotaMsgBox moClass, moClass.moSysSession, kmsgCMUnexpectedSPError, "Loading Component Form"
        End If
    End If
    
    ResetMouse
    
    Me.Enabled = True
    
    'Highlight the row where the user has selected before click the Dist button
    If mlCurrentLineActiveRow > 0 Then
        gGridSetSelectRow grdLinePicked, mlCurrentLineActiveRow
    End If
    
    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdComponents_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub cmdDeleteAll_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdDeleteAll, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'********************************************************************
' Description:
'    This routine deletes all the Short Pick Shiplines
'
'********************************************************************
On Error GoTo ExpectedErrorRoutine
Dim sWhere        As String
Dim sShortPickSelected As String
Dim iUpdateCounterViolation As Integer

    SetMouse
    
    If OptLineShow(kShowShortOnly).Value = True Then
    'If only short pick is displayed,  then only clear the short pick ship lines
        sShortPickSelected = " AND ShortPick = 1"
    Else
        sShortPickSelected = ""
    End If
    
    'Build where clause for the lines need to be deleted
    sWhere = " wrk.ShipLineKey IN (SELECT ShipLineKey FROM #tsoCreatePickWrk2 WHERE LineHold <> 1 AND CrHold <> 1 AND OrderHold <> 1"
    sWhere = sWhere & " AND OrderKey = " & mlTranKey & " AND TranType = " & mlTranType & sShortPickSelected & ")"
    
    'Call DeleteShipLines to delete the selected Shiplines
    If Not moProcessPickList.bDeleteShipLines(sWhere, kDeleteDistributionOnlyNo, iUpdateCounterViolation) Then
        ResetMouse
        Exit Sub
    End If
    
    If Err.Number <> 0 Then
        gClearSotaErr
    End If
    
    'Refresh frame captions for display grids
    grdLinePicked.redraw = False
    moDMLineGrid.Refresh
    grdLinePicked.redraw = True
 
    ResetMouse
    
    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    Exit Sub

'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdDeleteAll_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub cmdDist_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdDist, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
On Error GoTo ExpectedErrorRoutine

Dim lRow            As Long
    
    'Get the active row
    lRow = glGridGetActiveRow(grdLinePicked)
    
    'Store the Current cell value in the variable
    mlCurrentLineActiveRow = lRow
    mlCurrentActiveColumn = glGridGetActiveCol(grdLinePicked)
    mlCurrentRowShipLineKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineShipLineKey))

    SetMouse
    
    If Not moItemDist Is Nothing Then
        If moProcessPickList.bIsValidQtyToPick(Me, moDMLineGrid, grdLinePicked, lRow) Then
        'If the QtyToPick is valid, load the distribution form
        'so use can enter new distribution or view and update exist distribution
            If Not moProcessPickList.bCreateManualDistribution(Me, grdLinePicked, moDMLineGrid, lRow) Then
                grdLinePicked_Click kColLineQtyToPick, lRow
                grdLinePicked.SetFocus
            End If
        Else
        'QtyToPick is invalid, set the focus back the QtyToPick cell
            grdLinePicked_Click kColLineQtyToPick, lRow
            grdLinePicked.SetFocus
            Exit Sub
        End If
    End If
    
    'Highlight the row where the user has selected before click the Dist button
    If mlCurrentLineActiveRow > 0 Then
        gGridSetSelectRow grdLinePicked, mlCurrentLineActiveRow
    End If
    
    ResetMouse
    
    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "cmdDist_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub cmdPickAll_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdPickAll, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'**********************************************************************
' Description:
'    This routine automatic pick all the Short Pick Lines
'
'**********************************************************************
On Error GoTo ExpectedErrorRoutine

Dim sWhere    As String
Dim sShortPickSelected As String
Dim iUpdateCounterViolation As Integer
    
    SetMouse
    
    If OptLineShow(kShowShortOnly).Value = True Then
    'If only short pick is displayed,  then only clear the short pick ship lines
        sShortPickSelected = " AND ShortPick = 1"
    Else
        sShortPickSelected = ""
    End If
    
    'Build where clause for the lines need to be picked
    sWhere = " ShipLineKey IN (SELECT ShipLineKey FROM #tsoCreatePickWrk2 WHERE LineHold =0 AND CrHold =0 AND OrderHold =0"
    sWhere = sWhere & " AND OrderKey = " & mlTranKey & " AND TranType = " & mlTranType & sShortPickSelected & ")"
    
    'Call bCreateAutoDistribution to create distribution for the selected Shiplines
    If Not moProcessPickList.bCreateAutoDistribution(sWhere, miEmptyBins, miEmptyRandomBins, iUpdateCounterViolation) Then
        ResetMouse
        Exit Sub
    End If
    
    If Err.Number <> 0 Then
        gClearSotaErr
    End If
    
    'Refresh frame captions for display grids
    grdLinePicked.redraw = False
    moDMLineGrid.Refresh
    grdLinePicked.redraw = True
    
    ResetMouse

    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse

'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "cmdPickAll_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++

End Sub

Private Sub grdLinePicked_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
On Error GoTo ExpectedErrorRoutine
    
    'Only allow edit when this form is called by the Pick Order Line from
    'Main form
    If miCallingForm = kPickLineOnHold Then Exit Sub
    
    SetMouse
    
    'If the customer requires ship complete - check before delete
    If giGetValidInt(gsGridReadCell(grdLinePicked, Row, kColLinePickCheck)) = vbUnchecked And Not mbInIsvalidQty Then
        If giGetValidInt(gsGridReadCell(grdLinePicked, Row, kColLineCustReqShipComplete)) = 1 _
           And glGetValidLong(gsGridReadCell(grdLinePicked, Row, kColLineTranType)) = kSOTranType Then
            If moProcessPickList.bShipCompleteWarnOnDelete(grdLinePicked, Row) Then
                If giSotaMsgBox(Me, moClass.moSysSession, kSOPartialPickWarn, "") = vbNo Then
                    gGridUpdateCell grdLinePicked, Row, kColLinePickCheck, vbChecked
                    ResetMouse
                    Exit Sub
                End If
            End If
        End If
    End If
        
    'Perform pick line update
    If Not moProcessPickList.bPerformPickLineUpdate(Me, grdLinePicked, moDMLineGrid, Row) Then
        giSotaMsgBox moClass, moClass.moSysSession, kmsgCMUnexpectedSPError, "Update Pick List"
        ResetMouse
        Exit Sub
    End If
    
    grdLinePicked.SetActiveCell m_LastEnterCol, m_LastEnterRow
    DoEvents
    
    'Format the grid row
    SetRowValues Row
    
    mbIsDirty = True
    
    ResetMouse
    
    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "grdLinePicked_ButtonClicked", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub grdLinePicked_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Only allow edit when this form is called by the Pick Order Line from
    'Main form
    If miCallingForm = kPickLineOnHold Then Exit Sub
    
    'moGMLine.Grid_Change Col, Row
    moGMLine_CellChange Row, Col
    HideNavs
  
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdlinePicked_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub HidePicked(Optional ByVal lRow As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iShortPick    As Integer
    Dim lRowCount   As Long
    
    If IsMissing(lRow) Then
        For lRowCount = 1 To grdLinePicked.DataRowCnt
            iShortPick = giGetValidInt(gsGridReadCell(grdLinePicked, lRowCount, kColLineShortPickFlag))
            If iShortPick = 0 Then
                With grdLinePicked
                    .Row = lRowCount
                    .RowHidden = True
                End With
            End If
        Next lRowCount
    Else
        lRowCount = glGetValidLong(lRow)
        iShortPick = giGetValidInt(gsGridReadCell(grdLinePicked, lRowCount, kColLineShortPickFlag))
        If iShortPick = 0 Then
            With grdLinePicked
                .Row = lRowCount
                .RowHidden = True
            End With
        End If
    End If
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HideUnpicked", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


Private Sub HideNavs()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    navGridSubItem.Visible = False
    navGridUOM.Visible = False

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HideNavs", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True.
'   Parameters:
'           KeyCode, Shift - see VB reference Manual.
'
'   NOTE: THIS ROUTINE SHOULD CALL A GLOBAL SUBROUTINE TO HANDLE ALL THE
'         AVAILABLE HOTKEYS.
'********************************************************************
    'Determine if the user used the form-level keys and process them
    Select Case keycode
        Case vbKeyF1 To vbKeyF16
            'Process the form-level key pressed
            gProcessFKeys Me, keycode, Shift
    End Select
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

'************************************************************************
'   Description:
'       Process Key Presses on the form.  NOTE: Key Preview of the form
'       should be set to True.
'   Param:
'       KeyAscii -  ASCII Key Code of Key Pressed.
'   Returns:
'************************************************************************
Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        
        Case Else
            'Other KeyAscii routines.
    End Select
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'******************************************
'* Set up the form.
'******************************************
   
    'Set the Status Bar object.
    Set sbrMain.Framework = moClass.moFramework
        
    'Get session defaults.
    Set moSysSession = moClass.moSysSession
    With moClass.moSysSession
        mlLanguage = .Language           'Language ID
        msCurrencyID = .CurrencyID       'Currency ID
        msCompanyID = .CompanyId         'Company ID
        mbEnterAsTab = .EnterAsTab
        mbIMActivated = .IsModuleActivated(kModuleIM)
    End With
   
    'Set the form's initial height and width.
    miOldFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormHeight = miOldFormHeight
    miMinFormWidth = miOldFormWidth
       
    BindContextMenu     'Bind the Context Menu.
    
    SetupLineGrid
    BindLineGrid
    BindGM
    BindToolbar

    ClearForm
        
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub SetupLineGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Set general grid properties
    gGridSetProperties grdLinePicked, kMaxLineCols, kGridDataSheetNoAppend
    gGridSetColors grdLinePicked
        
    With grdLinePicked
        .OperationMode = SS_OP_MODE_NORMAL
        .AllowMultiBlocks = False
        .SelectBlockOptions = SS_SELBLOCKOPT_ALL
        .NoBeep = True 'prevent beeps on locked cells
    End With
    
    '************************************************************************
    'SGS DEJ 4/30/2010  (START)
    '************************************************************************
    gGridSetHeader grdLinePicked, kColLineBinFlag, "Tank"    'SGS DEJ 4/30/2010
    
    '************************************************************************
    'SGS DEJ 4/30/2010  (STOP)
    '************************************************************************
    
    'set column captions
    gGridSetHeader grdLinePicked, kColLinePickCheck, msPick
    gGridSetHeader grdLinePicked, kColLineLineHold, "Ln Hold"
    gGridSetHeader grdLinePicked, kColLineItem, msItemOrdered
    gGridSetHeader grdLinePicked, kColLineShiptoAddrID, msShipTo
    gGridSetHeader grdLinePicked, kColLineTranTypeText, msTranType
    gGridSetHeader grdLinePicked, kColLineQtyToPick, msQtyOpen 'msQtyToPick
    gGridSetHeader grdLinePicked, kColLineQtyPicked, msQtyPicked
    gGridSetHeader grdLinePicked, kColLineQtyShort, msShort
    gGridSetHeader grdLinePicked, kColLineQtyAvail, msAvailToShip
    gGridSetHeader grdLinePicked, kColLineShipUOM, msShipUOM
    gGridSetHeader grdLinePicked, kColLineStatus, msStat
    gGridSetHeader grdLinePicked, kColLineSubFlag, msSub
    gGridSetHeader grdLinePicked, kColLineShipPriority, msPriority
    gGridSetHeader grdLinePicked, kColLineShipDate, msShipDate
    gGridSetHeader grdLinePicked, kColLineSubItem, msSubItem
    gGridSetHeader grdLinePicked, kColLineDeliveryMethodID, msDelivery
    gGridSetHeader grdLinePicked, kColLineShipVia, msShipVia
    gGridSetHeader grdLinePicked, kColLineOrderNo, msOrderNo
    gGridSetHeader grdLinePicked, kColLineLine, msLineNo
    gGridSetHeader grdLinePicked, kColLineShipWhseID, msWhse
    gGridSetHeader grdLinePicked, kColLineShiptoName, msShipToName
    gGridSetHeader grdLinePicked, kColLineShiptoAddr, msShipToAddress
    gGridSetHeader grdLinePicked, kColLineItemDesc, msDescription
    gGridSetHeader grdLinePicked, kColLineSASequence, msStockAllocSeq
    gGridSetHeader grdLinePicked, kColLineAutoDistBinID, msBin
    gGridSetHeader grdLinePicked, kColLineQtyToPickByOrdUOM, msQtyOpen
    gGridSetHeader grdLinePicked, kColLineOrderLineUOMID, msOrdUOM
    
    'Column Catiptions for Hidden columns
    gGridSetHeader grdLinePicked, kColLineAllowDecimalQty, "AllowDecimalQty"
    gGridSetHeader grdLinePicked, kColLineRcvgWarehouse, "RcvgWarehouse"
    gGridSetHeader grdLinePicked, kColLineLineHoldReason, "Hold Reason"
    gGridSetHeader grdLinePicked, kColLineItemType, "ItemType"
    gGridSetHeader grdLinePicked, kColLineTranKey, "TranKey"
    gGridSetHeader grdLinePicked, kColLineTranType, "TranType"
    gGridSetHeader grdLinePicked, kColLineTranLineKey, "TranLineKey"
    gGridSetHeader grdLinePicked, kColLineTranLineDistKey, "TranLineDistKey"
    gGridSetHeader grdLinePicked, kColLineAllowSubItem, "AllowSubItem"
    gGridSetHeader grdLinePicked, kColLineKitShipLineKey, "KitLineKey"
    gGridSetHeader grdLinePicked, kColLineTrackMeth, "TrackMeth"
    gGridSetHeader grdLinePicked, kColLineTrackQtyAtBin, "TrackQtyAtBin"
    gGridSetHeader grdLinePicked, kColLineInvtTranKey, "InvtTranKey"
    gGridSetHeader grdLinePicked, kColLineShipLineDistKey, "ShipLineDistKey"
    gGridSetHeader grdLinePicked, kColLineOrderLineUOMKey, "OrderLineUOMKey"
    gGridSetHeader grdLinePicked, kColLineShipUOMKey, "ShipUOMKey"
    gGridSetHeader grdLinePicked, kColLineSubItemKey, "SubstituteItemKey"
    gGridSetHeader grdLinePicked, kColLineShortPickFlag, "Short Pick"
    gGridSetHeader grdLinePicked, kColLineCurrDisplayRow, "CurrDisplayRow"
    gGridSetHeader grdLinePicked, kColLineQtyDist, "Qty Dist"
    gGridSetHeader grdLinePicked, kColLineShiptoAddrKey, "ShipToAddrKey"
    gGridSetHeader grdLinePicked, kColLineShipMethKey, "ShipMethKey"
    gGridSetHeader grdLinePicked, kColLineShipFOBKey, "FOBKey"

    'Set column widths
    gGridSetColumnWidth grdLinePicked, kColLinePickCheck, 4
    gGridSetColumnWidth grdLinePicked, kColLineLineHold, 6
    gGridSetColumnWidth grdLinePicked, kColLineItem, 12
    gGridSetColumnWidth grdLinePicked, kColLineShiptoAddrID, 12
    gGridSetColumnWidth grdLinePicked, kColLineTranTypeText, 5
    gGridSetColumnWidth grdLinePicked, kColLineQtyDist, 8
    gGridSetColumnWidth grdLinePicked, kColLineQtyToPick, 8
    gGridSetColumnWidth grdLinePicked, kColLineQtyShort, 8
    gGridSetColumnWidth grdLinePicked, kColLineQtyAvail, 10
    gGridSetColumnWidth grdLinePicked, kColLineShipUOM, 6
    gGridSetColumnWidth grdLinePicked, kColLineStatus, 6
    gGridSetColumnWidth grdLinePicked, kColLineSubFlag, 4
    gGridSetColumnWidth grdLinePicked, kColLineShipPriority, 6
    gGridSetColumnWidth grdLinePicked, kColLineShipDate, 8
    gGridSetColumnWidth grdLinePicked, kColLineSubItem, 12
    gGridSetColumnWidth grdLinePicked, kColLineDeliveryMethodID, 6
    gGridSetColumnWidth grdLinePicked, kColLineShipVia, 10
    gGridSetColumnWidth grdLinePicked, kColLineOrderNo, 12
    gGridSetColumnWidth grdLinePicked, kColLineLine, 4
    gGridSetColumnWidth grdLinePicked, kColLineShipWhseID, 8
    gGridSetColumnWidth grdLinePicked, kColLineShiptoName, 14
    gGridSetColumnWidth grdLinePicked, kColLineShiptoAddr, 12
    gGridSetColumnWidth grdLinePicked, kColLineItemDesc, 14
    gGridSetColumnWidth grdLinePicked, kColLineSASequence, 8
    gGridSetColumnWidth grdLinePicked, kColLineAllowDecimalQty, 8
    gGridSetColumnWidth grdLinePicked, kColLineRcvgWarehouse, 8
    gGridSetColumnWidth grdLinePicked, kColLineLineHoldReason, 6
    gGridSetColumnWidth grdLinePicked, kColLineItemType, 6
    gGridSetColumnWidth grdLinePicked, kColLineTranKey, 6
    gGridSetColumnWidth grdLinePicked, kColLineTranType, 6
    gGridSetColumnWidth grdLinePicked, kColLineTranLineKey, 6
    gGridSetColumnWidth grdLinePicked, kColLineTranLineDistKey, 6
    gGridSetColumnWidth grdLinePicked, kColLineAllowSubItem, 4
    gGridSetColumnWidth grdLinePicked, kColLineKitShipLineKey, 4
    gGridSetColumnWidth grdLinePicked, kColLineTrackMeth, 4
    gGridSetColumnWidth grdLinePicked, kColLineTrackQtyAtBin, 4
    gGridSetColumnWidth grdLinePicked, kColLineQtyPicked, 8
    gGridSetColumnWidth grdLinePicked, kColLineInvtTranKey, 4
    gGridSetColumnWidth grdLinePicked, kColLineShipLineDistKey, 4
    gGridSetColumnWidth grdLinePicked, kColLineOrderLineUOMKey, 4
    gGridSetColumnWidth grdLinePicked, kColLineShipUOMKey, 4
    gGridSetColumnWidth grdLinePicked, kColLineSubItemKey, 4
    gGridSetColumnWidth grdLinePicked, kColLineShortPickFlag, 4
    gGridSetColumnWidth grdLinePicked, kColLineCurrDisplayRow, 4
    gGridSetColumnWidth grdLinePicked, kColLineAutoDistBinID, 10

  'Set column types
    gGridSetColumnType grdLinePicked, kColLinePickCheck, SS_CELL_TYPE_CHECKBOX
    gGridSetColumnType grdLinePicked, kColLineLineHold, SS_CELL_TYPE_CHECKBOX
    gGridSetColumnType grdLinePicked, kColLineItem, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShiptoAddrID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineTranTypeText, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShipUOM, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineStatus, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineSubFlag, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShipPriority, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShipDate, SS_CELL_TYPE_DATE
    gGridSetColumnType grdLinePicked, kColLineSubItem, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineDeliveryMethodID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShipVia, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineOrderNo, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineLine, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdLinePicked, kColLineShipWhseID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShiptoName, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShiptoAddr, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineItemDesc, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineSASequence, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdLinePicked, kColLineAllowDecimalQty, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineRcvgWarehouse, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineLineHoldReason, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineItemType, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineTranKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineTranType, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineTranLineKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineTranLineDistKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineAllowSubItem, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineKitShipLineKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineTrackMeth, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineTrackQtyAtBin, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineInvtTranKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShipLineDistKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineOrderLineUOMKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShipUOMKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineInvtTranID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineSubItemKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShortPickFlag, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineCurrDisplayRow, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineAutoDistBinID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineQtyDist, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShiptoAddrKey, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdLinePicked, kColLineShipMethKey, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdLinePicked, kColLineShipFOBKey, SS_CELL_TYPE_INTEGER

    'lock protected columns
    'gGridLockColumn grdLinePicked, kColLinePickCheck
    gGridLockColumn grdLinePicked, kColLineItem
    gGridLockColumn grdLinePicked, kColLineShiptoAddrID
    gGridLockColumn grdLinePicked, kColLineTranTypeText
    gGridLockColumn grdLinePicked, kColLineQtyToPick
    gGridLockColumn grdLinePicked, kColLineQtyToPick
    gGridLockColumn grdLinePicked, kColLineQtyShort
    gGridLockColumn grdLinePicked, kColLineQtyAvail
    'gGridLockColumn grdLinePicked, kcollineShipUOM
    gGridLockColumn grdLinePicked, kColLineStatus
    gGridLockColumn grdLinePicked, kColLineSubFlag
    gGridLockColumn grdLinePicked, kColLineShipPriority
    gGridLockColumn grdLinePicked, kColLineShipDate
    gGridLockColumn grdLinePicked, kColLineSubItem
    gGridLockColumn grdLinePicked, kColLineDeliveryMethodID
    gGridLockColumn grdLinePicked, kColLineShipVia
    gGridLockColumn grdLinePicked, kColLineOrderNo
    gGridLockColumn grdLinePicked, kColLineLine
    gGridLockColumn grdLinePicked, kColLineShipWhseID
    gGridLockColumn grdLinePicked, kColLineShiptoName
    gGridLockColumn grdLinePicked, kColLineShiptoAddr
    gGridLockColumn grdLinePicked, kColLineItemDesc
    gGridLockColumn grdLinePicked, kColLineSASequence
    gGridLockColumn grdLinePicked, kColLineLineHold
    gGridLockColumn grdLinePicked, kColLineQtyToPickByOrdUOM
    gGridLockColumn grdLinePicked, kColLineOrderLineUOMID
    gGridLockColumn grdLinePicked, kColLineShiptoAddrKey
    gGridLockColumn grdLinePicked, kColLineShipMethKey
    gGridLockColumn grdLinePicked, kColLineShipFOBKey

    gGridHAlignColumn grdLinePicked, kColLineShipDate, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
    gGridHAlignColumn grdLinePicked, kColLineSubFlag, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
    gGridHAlignColumn grdLinePicked, kColLineLine, SS_CELL_TYPE_INTEGER, SS_CELL_H_ALIGN_CENTER
    gGridHAlignColumn grdLinePicked, kColLineShipPriority, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
   
    'Hide warehouse column if IM is not interated
    If Not (mbIMIntegrated) Then
         gGridHideColumn grdLinePicked, kColLineShipWhseID
         gGridHideColumn grdLinePicked, kColLineRcvgWarehouse
    End If
    
    'hide columns
    gGridHideColumn grdLinePicked, kColLineAllowDecimalQty
    gGridHideColumn grdLinePicked, kColLineAllowImmedShipFromPick
    gGridHideColumn grdLinePicked, kColLineAllowSubItem
    gGridHideColumn grdLinePicked, kColLineAutoDist
    gGridHideColumn grdLinePicked, kColLineAutoDistBinKey
    gGridHideColumn grdLinePicked, kColLineAutoDistBinQtyAvail
    gGridHideColumn grdLinePicked, kColLineAutoDistBinUOMKey
    gGridHideColumn grdLinePicked, kColLineAutoDistLotExpDate
    gGridHideColumn grdLinePicked, kColLineAutoDistLotKey
    gGridHideColumn grdLinePicked, kColLineAutoDistLotNo
    gGridHideColumn grdLinePicked, kColLineCompItemQty
    gGridHideColumn grdLinePicked, kColLineCrHold
    gGridHideColumn grdLinePicked, kColLineCurrDisplayRow
    gGridHideColumn grdLinePicked, kColLineCustReqShipComplete
    gGridHideColumn grdLinePicked, kColLineDeliveryMethod
    gGridHideColumn grdLinePicked, kColLineDistComplete
    gGridHideColumn grdLinePicked, kColLineExtShipmentExists
    gGridHideColumn grdLinePicked, kColLineInvtTranID
    gGridHideColumn grdLinePicked, kColLineInvtTranKey
    gGridHideColumn grdLinePicked, kColLineItemDesc
    gGridHideColumn grdLinePicked, kColLineItemKey
    gGridHideColumn grdLinePicked, kColLineItemReqShipComplete
    gGridHideColumn grdLinePicked, kColLineItemType
    gGridHideColumn grdLinePicked, kColLineKitShipLineKey
    'gGridHideColumn grdLinePicked, kColLineLineHold
    gGridHideColumn grdLinePicked, kColLineLineHoldReason
    'gGridHideColumn grdLinePicked, kColLineOrderLineUOMID
    gGridHideColumn grdLinePicked, kColLineOrderLineUOMKey
    gGridHideColumn grdLinePicked, kColLineOrderNo
    gGridHideColumn grdLinePicked, kColLineOrdHold
    gGridHideColumn grdLinePicked, kColLinePacked
    gGridHideColumn grdLinePicked, kColLineQtyDist
    gGridHideColumn grdLinePicked, kColLineQtyOrdered
    gGridHideColumn grdLinePicked, kColLineRcvgWarehouse
    gGridHideColumn grdLinePicked, kColLineSASequence
    gGridHideColumn grdLinePicked, kColLineShipKey
    gGridHideColumn grdLinePicked, kColLineShipLineDistKey
    gGridHideColumn grdLinePicked, kColLineShipLineDistUpdateCounter
    gGridHideColumn grdLinePicked, kColLineShipLineKey
    gGridHideColumn grdLinePicked, kColLineShipLineUpdateCounter
    gGridHideColumn grdLinePicked, kColLineShipmentCommitStatus
    gGridHideColumn grdLinePicked, kColLineShiptoAddr
    gGridHideColumn grdLinePicked, kColLineShiptoAddrID
    gGridHideColumn grdLinePicked, kColLineShiptoName
    gGridHideColumn grdLinePicked, kColLineShipUOMKey
    gGridHideColumn grdLinePicked, kColLineShipWhseKey
    gGridHideColumn grdLinePicked, kColLineShortPickFlag
    gGridHideColumn grdLinePicked, kColLineStatusCustShipCompleteViolation
    gGridHideColumn grdLinePicked, kColLineStatusDistQtyShort
    gGridHideColumn grdLinePicked, kColLineStatusDistWarning
    gGridHideColumn grdLinePicked, kColLineStatusItemShipCompLeteViolation
    gGridHideColumn grdLinePicked, kColLineStatusOverShipment
    gGridHideColumn grdLinePicked, kColLineSubItemKey
    gGridHideColumn grdLinePicked, kColLineTrackMeth
    gGridHideColumn grdLinePicked, kColLineTrackQtyAtBin
    gGridHideColumn grdLinePicked, kColLineTranKey
    gGridHideColumn grdLinePicked, kColLineTranLineDistKey
    gGridHideColumn grdLinePicked, kColLineTranLineKey
    gGridHideColumn grdLinePicked, kColLineTranType
    gGridHideColumn grdLinePicked, kColLineTranTypeText
    gGridHideColumn grdLinePicked, kColLineWhseUseBin
    gGridHideColumn grdLinePicked, kColLineShiptoAddrKey
    gGridHideColumn grdLinePicked, kColLineShipMethKey
    gGridHideColumn grdLinePicked, kColLineShipFOBKey

    gGridFreezeCols grdLinePicked, kColLineItem
    gGridFreezeCols grdLinePicked, kColLineShiptoAddrID

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupLineGrid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Private Sub BindLineGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moDMLineGrid = New clsDmGrid
    With moDMLineGrid
        Set .Session = moSysSession
        Set .Database = moAppDB
        Set .Form = frmOrderPickLines
        Set .Grid = grdLinePicked
        .Table = "#tsoCreatePickWrk2"
        .UniqueKey = "ShipLineKey"
        .NoAppend = True
        .OrderBy = "LineNbr"
        .Where = "CurrDisplayRow = 1 AND KitShipLineKey IS NULL"
        
        .BindColumn "AllowAutoSub", Nothing, SQL_SMALLINT
        .BindColumn "AllowDecimalQty", kColLineAllowDecimalQty, SQL_SMALLINT
        .BindColumn "AllowSubItem", kColLineAllowSubItem, SQL_SMALLINT
        .BindColumn "CrHold", kColLineCrHold, SQL_SMALLINT
        .BindColumn "CustReqShipComplete", kColLineCustReqShipComplete, SQL_SMALLINT
        .BindColumn "CurrDisplayRow", kColLineCurrDisplayRow, SQL_SMALLINT
        .BindColumn "DeliveryMethod", kColLineDeliveryMethod, SQL_SMALLINT
        .BindColumn "DeliveryMethodID", kColLineDeliveryMethodID, SQL_VARCHAR
        .BindColumn "ExtShipmentExists", kColLineExtShipmentExists, SQL_SMALLINT
        .BindColumn "FreightAmt", Nothing, SQL_DECIMAL
        .BindColumn "InvtLotNo", kColLineAutoDistLotNo, SQL_VARCHAR
        .BindColumn "InvtLotKey", kColLineAutoDistLotKey, SQL_INTEGER
        .BindColumn "InvtTranLinkKey", kColLineInvtTranID, SQL_INTEGER
        .BindColumn "InvtTranKey", kColLineInvtTranKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "IsDistComplete", kColLineDistComplete, SQL_SMALLINT
        .BindColumn "ItemDesc", kColLineItemDesc, SQL_VARCHAR
        .BindColumn "ItemID", kColLineItem, SQL_VARCHAR
        .BindColumn "ItemKey", kColLineItemKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ItemReqShipComplete", kColLineItemReqShipComplete, SQL_SMALLINT
        .BindColumn "ItemType", kColLineItemType, SQL_SMALLINT
        .BindColumn "KitShipLineKey", kColLineKitShipLineKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "LineDistKey", kColLineTranLineDistKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "LineKey", kColLineTranLineKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "LineNbr", kColLineLine, SQL_INTEGER
        .BindColumn "LineHold", kColLineLineHold, SQL_SMALLINT
        .BindColumn "OrderHold", kColLineOrdHold, SQL_SMALLINT
        .BindColumn "OrderKey", kColLineTranKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "OrderLineUnitMeasKey", kColLineOrderLineUOMKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "OrderLineUnitMeasID", kColLineOrderLineUOMID, SQL_VARCHAR
        .BindColumn "Packed", kColLinePacked, SQL_SMALLINT
        .BindColumn "PickStatus", kColLinePickCheck, SQL_SMALLINT
        .BindColumn "QtyAvail", kColLineQtyAvail, SQL_DECIMAL
        .BindColumn "QtyDist", kColLineQtyDist, SQL_DECIMAL
        .BindColumn "QtyPicked", kColLineQtyPicked, SQL_DECIMAL
        .BindColumn "QtyShort", kColLineQtyShort, SQL_DECIMAL
        .BindColumn "QtyToPick", kColLineQtyToPick, SQL_DECIMAL
        .BindColumn "QtyToPickByOrderUOM", kColLineQtyToPickByOrdUOM, SQL_DECIMAL
        .BindColumn "QtyOrdered", kColLineQtyOrdered, SQL_DECIMAL
        .BindColumn "ShipKey", kColLineShipKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipmentCommitStatus", kColLineShipmentCommitStatus, SQL_SMALLINT
        .BindColumn "ShipDate", kColLineShipDate, SQL_DATE
        .BindColumn "ShipLineKey", kColLineShipLineKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipLineDistKey", kColLineShipLineDistKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipLineUpdateCounter", kColLineShipLineUpdateCounter, SQL_INTEGER
        .BindColumn "ShipLineDistUpdateCounter", kColLineShipLineDistUpdateCounter, SQL_INTEGER
        .BindColumn "ShipMethID", kColLineShipVia, SQL_VARCHAR
        .BindColumn "ShipMethKey", kColLineShipMethKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipPriority", kColLineShipPriority, SQL_VARCHAR, , kDmSetNull
        .BindColumn "ShipUnitMeasID", kColLineShipUOM, SQL_VARCHAR
        .BindColumn "ShipUnitMeasKey", kColLineShipUOMKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipWhseID", kColLineShipWhseID, SQL_VARCHAR
        .BindColumn "ShipWhseKey", kColLineShipWhseKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShortPick", kColLineShortPickFlag, SQL_SMALLINT
        .BindColumn "Status", kColLineStatus, SQL_VARCHAR
        .BindColumn "StatusCustShipCompleteViolation", kColLineStatusCustShipCompleteViolation, SQL_VARCHAR
        .BindColumn "StatusItemShipCompLeteViolation", kColLineStatusItemShipCompLeteViolation, SQL_VARCHAR
        .BindColumn "StatusDistWarning", kColLineStatusDistWarning, SQL_VARCHAR
        .BindColumn "StatusDistQtyShort", kColLineStatusDistQtyShort, SQL_VARCHAR
        .BindColumn "StatusOverShipment", kColLineStatusOverShipment, SQL_VARCHAR
        .BindColumn "StockAllocSeq", kColLineSASequence, SQL_INTEGER
        .BindColumn "StockQtyAvail", Nothing, SQL_DECIMAL
        .BindColumn "StockUnitMeasKey", Nothing, SQL_INTEGER, , kDmSetNull
        .BindColumn "SubstiStatus", kColLineSubFlag, SQL_VARCHAR
        .BindColumn "SubstituteItemDesc", kColLineSubItem, SQL_VARCHAR
        .BindColumn "SubstituteItemKey", kColLineSubItemKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "TrackMeth", kColLineTrackMeth, SQL_SMALLINT
        .BindColumn "TrackQtyAtBin", kColLineTrackQtyAtBin, SQL_SMALLINT
        .BindColumn "TranType", kColLineTranType, SQL_INTEGER
        .BindColumn "WhseBinID", kColLineAutoDistBinID, SQL_VARCHAR
        .BindColumn "WhseBinKey", kColLineAutoDistBinKey, SQL_INTEGER
        .BindColumn "WhseUseBin", kColLineWhseUseBin, SQL_SMALLINT
        .BindColumn "ShipToCustAddrKey", kColLineShiptoAddrKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipFOBKey", kColLineShipFOBKey, SQL_INTEGER

        .Init
    End With
    
    'Bind the grid navigator for the UOM column.
    gbLookupInit navGridUOM, moClass, moAppDB, "UnitOfMeasure"

    gbLookupInit navGridSubItem, moClass, moAppDB, "ItemSubstitute"
    
    gbLookupInit lkuAutoDistBin, moClass, moAppDB, "DistBin"
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindLineGrid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Private Sub BindGM()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moGMLine = New clsGridMgr
    With moGMLine
        Set .Grid = grdLinePicked
        Set .DM = moDMLineGrid
        Set .Form = frmOrderPickLines
        .GridType = kGridDataSheetNoAppend
        .GridSortEnabled = True
        Set moGridNavSubItem = .BindColumn(kColLineSubItem, navGridSubItem)
        Set moGridNavSubItem.ReturnControl = txtNavSubItem
        Set moGridNavUOM = .BindColumn(kColLineShipUOM, navGridUOM)
        Set moGridNavUOM.ReturnControl = txtNavUOM
        Set moGridLkuAutoDistBin = .BindColumn(kColLineAutoDistBinID, lkuAutoDistBin)
        Set moGridLkuAutoDistBin.ReturnControl = txtlkuAutoDistBin
        .Init
    End With
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindGM", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
'************************************************************************
'   Description:
'       Create the toolbar.
'   Param:
'       <none>
'   Returns:
'************************************************************************
Private Sub BindToolbar()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Set up the toolbar
    tbrMain.Build sotaTB_SINGLE_ROW
    tbrMain.RemoveButton kTbPrint
    tbrMain.RemoveButton kTbCancelExit
    
    With moClass.moFramework
        tbrMain.SecurityLevel = .GetTaskPermission(.GetTaskID())
        tbrMain.LocaleID = mlLanguage
    End With

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindToolbar", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


'************************************************************************
'   Description:
'       Standard Shutdown Procedure.  Unload all child forms
'       and remove any objects created within this app.
'   Param:
'       <none>
'   Returns:
'************************************************************************
Public Sub PerformCleanShutDown()
    On Error GoTo ExpectedErrorRoutine

         
    'Remove all child collections.
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    TerminateControls Me
    
    If Not moItemDist Is Nothing Then
        Set moItemDist = Nothing
    End If
    
    If Not moGridNavUOM Is Nothing Then
        Set moGridNavUOM = Nothing
    End If
    
    If Not moGridNavSubItem Is Nothing Then
        Set moGridNavSubItem = Nothing
    End If
    
    If Not moGridLkuAutoDistBin Is Nothing Then
        Set moGridLkuAutoDistBin = Nothing
    End If
    
    If Not moGMLine Is Nothing Then
        moGMLine.UnloadSelf
        Set moGMLine = Nothing
    End If
    
    If Not moDMLineGrid Is Nothing Then
        moDMLineGrid.UnloadSelf
        Set moDMLineGrid = Nothing
    End If
    
    If Not moAppDB Is Nothing Then
        Set moAppDB = Nothing
    End If
    
    If Not moSysSession Is Nothing Then
        Set moSysSession = Nothing
    End If
    
    If Not moContextMenu Is Nothing Then
        Set moContextMenu = Nothing
    End If
    
    If Not moProcessPickList Is Nothing Then
        Set moProcessPickList = Nothing
    End If
    

    'Exit this subroutine
    Exit Sub

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If

    If UnloadMode <> vbFormCode Then
        ProcessCancel
        Cancel = True
    Else
        PerformCleanShutDown
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Me.WindowState = vbNormal Or Me.WindowState = vbMaximized Then
        'Resize Height
        gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, fraLinePicked, grdLinePicked
        'Resize Width
        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, fraLinePicked, grdLinePicked
        miOldFormHeight = Me.Height
        miOldFormWidth = Me.Width
       
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    If Not moClass Is Nothing Then
        Set moClass = Nothing
    End If
    
    

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

'************************************************************************
'   Description:
'       Returns Form Name for Debugging Information.
'   Param:
'       <none>
'   Returns:
'       Form Name
'************************************************************************
Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function

'************************************************************************
'   oClass contains the reference to the parent class object.  The form
'   needs this reference to use the public variables created within the
'   class object.
'************************************************************************
Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moClass = oNewClass

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
'************************************************************************
'   oClass contains the reference to the parent class object.  The form
'   needs this reference to use the public variables created within the
'   class object.
'************************************************************************
Public Property Get oProcessPickList() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oProcessPickList = moProcessPickList
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oProcessPickList", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Property Set oProcessPickList(oNewProcessPickList As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moProcessPickList = oNewProcessPickList

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oProcessPickList", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
Public Property Get oAppDb() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oAppDb = moAppDB
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oAppDB", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Property Set oAppDb(oNewAppDB As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moAppDB = oNewAppDB

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oAppDB", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Private Sub grdLinePicked_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
   
    'Only allow edit when this form is called by the Pick Order Line from
    'Main form
    If miCallingForm = kPickLineOnHold Then Exit Sub
   
    If Row = 0 And Col <> 0 Then
        moDMLineGrid.Save
        HideNavs
    End If
    moDMLineGrid.ProcessEventClick Col, Row
    'moGMLine.Grid_Click Col, Row
   
    If Row > 0 Then 'if event was not caused by a click in the heading area
        If moProcessPickList.bSetNavRestrict(grdLinePicked, navGridUOM, navGridSubItem, Row) Then
            SetRowValues Row
        End If
    End If
    
    'If the user click on any locked cell, highlight the grid row the cell is in
    With grdLinePicked
        .Row = Row
        .Col = Col
        If .Lock = True Then
            gGridSetSelectRow grdLinePicked, Row
        End If
    End With
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLinePicked_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdLinePicked_EditChange(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'User changed the value on the cell.  Set the mbCellChangeFired to false.  It will be set to true
    'once the event fires.  However, if it doesn't fire, we can use this flag to tell if we have to
    'call the GM CellChange manually.
    mbCellChangeFired = False

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLinePicked_EditChange", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdLinePicked_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Only allow edit when this form is called by the Pick Order Line from
    'Main form
    If miCallingForm = kPickLineOnHold Then Exit Sub
    moGMLine.Grid_EditMode Col, Row, Mode, ChangeMade
    If Mode = 1 Then
        m_LastEnterCol = Col
        m_LastEnterRow = Row
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLinePicked_EditMode", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub grdLinePicked_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'Only allow edit when this form is called by the Pick Order Line from
    'Main form
    If miCallingForm = kPickLineOnHold Then Exit Sub
    moGMLine.Grid_KeyDown keycode, Shift

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLinePicked_KeyDown", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++

End Sub

Private Sub grdLinePicked_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

     'Only allow edit when this form is called by the Pick Order Line from
    'Main form
    If miCallingForm = kPickLineOnHold Then Exit Sub

    'moGMLine.Grid_LeaveCell Col, Row, NewCol, NewRow
    Cancel = Not moGMLine.Grid_LeaveCell(Col, Row, NewCol, NewRow)
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLinePickedPicked_LeaveCell", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdLinePicked_TextTipFetch(ByVal Col As Long, ByVal Row As Long, MultiLine As FPSpreadADO.TextTipFetchMultilineConstants, TipWidth As Long, TipText As String, ShowTip As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If

Dim sSubItemID As String
Dim lSubItemKey As Long
Dim lWhseKey As Long
Dim dSubQtyAvail As Double

    'Only allow edit when this form is called by the Pick Order Line from
    'Main form
    If miCallingForm = kPickLineOnHold Then Exit Sub

    sSubItemID = gsGetValidStr(gsGridReadCell(grdLinePicked, Row, kColLineSubItem))
    lSubItemKey = glGetValidLong(gsGridReadCell(grdLinePicked, Row, kColLineSubItemKey))
    lWhseKey = glGetValidLong(gsGridReadCell(grdLinePicked, Row, kColLineShipWhseKey))

    If Col = kColLineQtyAvail Then
        If lSubItemKey > 0 And Len(Trim(sSubItemID)) > 0 Then
            'dSubQtyAvail = dGetSubAvailQty(lSubItemKey)
            dSubQtyAvail = 100
            ShowTip = True
            TipText = "Sub " & Trim(sSubItemID) & " Available = " & dSubQtyAvail
        End If
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLinePicked_TextTipFetch", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++

End Sub

Private Sub grdLinePicked_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Only allow edit when this form is called by the Pick Order Line from
    'Main form
    DoEvents
    If miCallingForm = kPickLineOnHold Then Exit Sub
    moGMLine.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLinePicked_TopLeftChange", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub



Private Sub moGMLine_CellChange(ByVal lRow As Long, ByVal lCol As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If (lCol <> kColLineShipUOM) And (lCol <> kColLineQtyPicked) And (lCol <> kColLineSubItem) And (lCol <> kColLineAutoDistBinID) And (lCol <> kColLineQtyToPick) Then
        Exit Sub
    End If
    
    mbCellChangeFired = True

    SetMouse
    If Not moProcessPickList.bProcessGridChange(Me, moDMLineGrid, grdLinePicked, lRow, lCol) Then
        ResetMouse
        'grdLinePicked_Click lCol, lRow
        Exit Sub
    End If
    
    ResetMouse
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "moGM_CellChange", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Private Sub moGMLine_LeaveGridRow(ByVal lLeaveRow As Long, ByVal lEnterRow As Long, ByVal eReason As GridMgrExt.LeaveGridRowReasons, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
   
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moGMLine_LeaveGridRow", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


Private Sub OptLineShow_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick OptLineShow(Index), True
    #End If
'+++ End Customizer Code Push +++

    If Index = kShowAll Then
        ShowPicked
    Else
        HidePicked
    End If
    
    SetupRowsSelCaption
    
    grdLinePicked_Click kColLineItem, 1
    
    OptLineShow(Index).Tag = OptLineShow(Index).Value

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "OptLineShow_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub ShowPicked(Optional ByVal lRow As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lRowCount        As Long
    
    If IsMissing(lRow) Then
        For lRowCount = 1 To grdLinePicked.DataRowCnt
            With grdLinePicked
                .Row = lRowCount
                .RowHidden = False
            End With
        Next lRowCount
    Else
        With grdLinePicked
            .Row = lRow
            .RowHidden = False
        End With
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ShowUnpicked", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarClick Button

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub


'************************************************************************
'   Description:
'       Process all toolbar clicks (as well as HotKey shortcuts to
'       toolbar buttons).
'   Param:
'       sKey -  Token returned from toolbar.  Indicates what function
'               is to be executed.
'   Returns:
'************************************************************************
Private Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If

    'Force Focus event when pressing toolbar.
    DoEvents

    Select Case sKey
        Case kTbFinishExit
        
            'Make sure the GM CellChange event fires.
            FireGMCellChangeIfNeeded
            Me.Hide
                                 
        Case kTbCancelExit
           
            ProcessCancel
            
        Case kTbHelp
            'The Help button was pressed by the user.
            gDisplayFormLevelHelp Me

        Case Else
            'Error processing
            'Give an error message to the user.
            'Unexpected Operation Value: {0}
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation, _
                         CVar(sKey)
    End Select
    
    ResetMouse

    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub ProcessCancel()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    SetFocusCtrl
    
    Me.Hide
      
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ProcessCancel", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Private Sub SetFocusCtrl()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'Set focus so that next time form is shown, focus will be at first control
'    lkuItem.Enabled = True
'    lkuItem.SetFocus
  
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetFocusCtrl", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
#End If

#If CUSTOMIZER Then
Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyFormCust
        End If
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
#End If




Private Sub cmdDist_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdDist, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDist_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDist_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdDist, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDist_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDeleteAll_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdDeleteAll, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDeleteAll_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDeleteAll_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdDeleteAll, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDeleteAll_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdPickAll_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdPickAll, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPickAll_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdPickAll_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdPickAll, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPickAll_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdClearAll_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdClearAll, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdClearAll_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdClearAll_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdClearAll, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdClearAll_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdComponents_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdComponents, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdComponents_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdComponents_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdComponents, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdComponents_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub txtNavSubItem_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtNavSubItem, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavSubItem_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavSubItem_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtNavSubItem, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavSubItem_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavSubItem_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtNavSubItem, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavSubItem_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavSubItem_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtNavSubItem, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavSubItem_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavUOM_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtNavUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavUOM_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavUOM_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtNavUOM, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavUOM_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavUOM_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtNavUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavUOM_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavUOM_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtNavUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavUOM_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtlkuAutoDistBin_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtlkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtlkuAutoDistBin_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtlkuAutoDistBin_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtlkuAutoDistBin, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtlkuAutoDistBin_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtlkuAutoDistBin_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtlkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtlkuAutoDistBin_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtlkuAutoDistBin_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtlkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtlkuAutoDistBin_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub OptLineShow_DblClick(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnDblClick OptLineShow(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "OptLineShow_DblClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub OptLineShow_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus OptLineShow(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "OptLineShow_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub OptLineShow_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus OptLineShow(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "OptLineShow_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then
Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomButton(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomButton(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomButton(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomCheck(Index)
    End If
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomCheck(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomCheck(Index)
    End If
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnChange CustomCombo(Index)
    End If
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomCombo(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnDblClick CustomCombo(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomCombo(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomCombo(Index)
    End If
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnChange CustomCurrency(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomCurrency(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomCurrency(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomFrame(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnDblClick CustomFrame(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomLabel(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnDblClick CustomLabel(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnChange CustomMask(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomMask(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnKeyPress CustomMask(Index), KeyAscii
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomMask(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnChange CustomNumber(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomNumber(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomNumber(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomOption(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnDblClick CustomOption(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomOption(Index)
    End If
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomOption(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnSpinDown CustomSpin(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnSpinUp CustomSpin(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
#End If

#If CUSTOMIZER And CONTROLS Then
Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.onClick CustomDate(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnDblClick CustomDate(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomDate(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomDate(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnKeyPress CustomDate(Index), KeyAscii
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.OnChange CustomDate(Index)
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
#End If

Public Property Get MyApp() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set MyApp = App

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MyApp", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Property Get MyForms() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set MyForms = Forms
    
    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MyForms", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Private Sub navGridSubItem_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim lRow As Long

    lRow = grdLinePicked.ActiveRow
    gGridSetActiveCell grdLinePicked, lRow, kColLineSubItem
    txtNavSubItem.Text = ""

    'Perform nav_click
    gcLookupClick Me, navGridSubItem, txtNavSubItem, "ItemID"
    moGMLine.LookupClicked

    mbIsDirty = True
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navGridSubItem_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub


Private Sub SetRowValues(ByVal lRow As Long, Optional bDataRefresh As Boolean = False)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**********************************************************************
' Description:
'    This routine formats the line grid rows based on the ItemType
'    UOM options, etc.
'
'**********************************************************************

Dim iAllowSubItem       As Integer
Dim iTrackMeth          As Integer
Dim iTrackQtyAtBin      As Integer
Dim dQty                As Double
Dim lKitShipLineKey     As Long
Dim lSubItemKey         As Long
Dim sSubItemID          As String
Dim lWhseKey            As Long
Dim lUOMKey             As Long
Dim lItemKey            As Long
Dim iItemType           As Integer
Dim iAllowDecimalQty    As Integer
Dim sSQL                As String
Dim iOrderChecked       As Integer
Dim iLineChecked        As Integer
Dim dDMQtyPicked        As Double
Dim dGridQtyPicked      As Double
Dim lOrderTranKey       As Long
Dim lOrderTranType      As Long
Dim lLineTranKey        As Long
Dim lLineTranType       As Long
Dim lRowCount           As Long
Dim iLineHold           As Integer
Dim iOrderHold          As Integer
Dim iCrHold             As Integer
Dim iShipmentCommitStatus    As Integer
Dim iExtShipmentExists  As Integer
Dim iPacked             As Integer
Dim iWhseUseBin         As Integer

    iLineHold = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineLineHold))
    iOrderHold = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineOrdHold))
    iCrHold = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineCrHold))

    If iLineHold = 0 And iOrderHold = 0 And iCrHold = 0 Then
    'Format the grid row when a ship line is not on line hold
        iAllowSubItem = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineAllowSubItem))
        iTrackMeth = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineTrackMeth))
        iTrackQtyAtBin = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineTrackQtyAtBin))
        iWhseUseBin = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineWhseUseBin))
        dQty = gdGetValidDbl(gsGridReadCell(grdLinePicked, lRow, kColLineQtyToPick))
        lKitShipLineKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineKitShipLineKey))
        sSubItemID = gsGetValidStr(gsGridReadCell(grdLinePicked, lRow, kColLineSubItem))
        lSubItemKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineSubItemKey))
        lWhseKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineShipWhseKey))
        lItemKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineSubItemKey))
        iItemType = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineItemType))
        iAllowDecimalQty = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineAllowDecimalQty))
        iShipmentCommitStatus = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineShipmentCommitStatus))
        iExtShipmentExists = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineExtShipmentExists))
        iPacked = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLinePacked))
        
        If iShipmentCommitStatus = 1 Or iExtShipmentExists = 1 Or iPacked = 1 Or moProcessPickList.iSecurityLevel = kSecLevelDisplayOnly Then
            gGridLockRow grdLinePicked, lRow
        Else
        
            If lItemKey = 0 Then
                lItemKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineItemKey))
            Else
                iTrackMeth = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineTrackMeth))
            End If
            
            lUOMKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineShipUOMKey))
        
            If iAllowSubItem = 0 Or dQty <= 0 Or lKitShipLineKey > 0 Then
                gGridLockCell grdLinePicked, kColLineSubItem, lRow
            Else
                gGridUnlockCell grdLinePicked, kColLineSubItem, lRow
            End If
            
            'UOM changes are not allowed for:
            'Serialized Items            Kit Component Items
            'Substituted Items           Items not allowing Deciaml Qtys
            If iAllowDecimalQty = 0 Or (iTrackMeth > 1) Or (lKitShipLineKey > 0) Or _
                 (iItemType < 5) Then
                 'Or (Len(Trim(sSubItemID))) > 0 Or (iAllowDecimalQty = 0) Then
                gGridLockCell grdLinePicked, kColLineShipUOM, lRow
            Else
                gGridUnlockCell grdLinePicked, kColLineShipUOM, lRow
            End If
            
            'Enable the auto-dist bin column if it can auto distributed from the bin lookup.
            'Lock it first by default.
            gGridLockCell grdLinePicked, kColLineAutoDistBinID, lRow
            'Unlock it if the conditions are correct.
            If iItemType = kFinishedGood Or iItemType = kPreAssembledKit Or iItemType = kRawMaterial Then
                If iTrackQtyAtBin = 1 And (iTrackMeth = kTM_None Or iTrackMeth = kTM_Lot) And mbIMIntegrated Then
                    gGridUnlockCell grdLinePicked, kColLineAutoDistBinID, lRow
                End If
            End If
            
            'Setup the distribution button for item needed to be distributed.
            'Skip the following code when the grid is being refreshed since this
            'does not applied to a group of lines
            If Not bDataRefresh Then
                If iItemType > 4 And mbIMIntegrated Then
                    If (iTrackMeth > 0 Or iTrackQtyAtBin = 1) Then
                        If iItemType = IMS_BTO_KIT Then
                            cmdComponents.Enabled = True
                            cmdDist.Enabled = False
                        Else
                            cmdDist.Enabled = True
                            cmdComponents.Enabled = False
                        End If
                    Else
                        If iItemType = IMS_BTO_KIT Then
                            cmdComponents.Enabled = True
                        Else
                            cmdComponents.Enabled = False
                        End If
                        cmdDist.Enabled = False
                    End If
                Else
                    cmdComponents.Enabled = False
                    cmdDist.Enabled = False
                End If
            End If
        End If
    Else
        'Format the grid row when a ship line is on hold
        'Lock the line from being edited
        cmdComponents.Enabled = False
        cmdDist.Enabled = False
        
        gGridLockRow grdLinePicked, lRow
        
    End If
        
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetRowValues", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub navGridUOM_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim lRow As Long

    lRow = grdLinePicked.ActiveRow
    gGridSetActiveCell grdLinePicked, lRow, kColLineShipUOM
    txtNavUOM.Text = ""
    
    'Perform nav_click
    gcLookupClick Me, navGridUOM, txtNavUOM, "UnitMeasID"
    moGMLine.LookupClicked
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navGridUOM_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub SetupRowsSelCaption()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'********************************************************************
' Description:
'    This routine updates the select grids's frame caption for
'    the current row count in the grid
'********************************************************************
Dim lLineNbrSelected    As Long
Dim lLineNbrSelectedDisplay As Long


    'First, get the count for all the lines that is not on hold
    lLineNbrSelected = glGridGetDataRowCnt(grdLinePicked)
    
    If lLineNbrSelected = 0 Then
        fraLinePicked.Caption = vbNullString
    Else
        'Now, according to the ShowAll and ShowShortOnly option recalculate the display row count
        If OptLineShow(kShowAll).Value = True Then
            lLineNbrSelectedDisplay = lLineNbrSelected
        Else
            lLineNbrSelectedDisplay = glGetValidLong(moAppDB.Lookup("Count(*)", "#tsoCreatePickWrk2", "KitShipLineKey IS NULL AND LineHold = 0 AND ShortPick = 1"))
        End If
    
        If lLineNbrSelectedDisplay > 1 Then
            fraLinePicked.Caption = lLineNbrSelectedDisplay & " " & msLinesToPick
        Else
            fraLinePicked.Caption = lLineNbrSelectedDisplay & " " & msLineToPick
        End If
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupRowsSelCaption", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub DMGridRowLoaded(oDM As Object, lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim iAllowDecimalQty As Integer
    Dim lInvtTranKey As Long
    Dim lItemKey As Long
    Dim lSubItemKey As Long
    Dim lShipLineKey As Long

    If oDM Is moDMLineGrid Then
        'sSQLDate = Left(gsGridReadCellText(grdLinePicked, lRow, kColLineShipDate), 10) 'Remove time
        'gGridUpdateCellText grdLinePicked, lRow, kColLineShipDate, Format(sSQLDate, "SHORT DATE") 'Is Localized
        
        'Always display the locked Qty column with decimal place
        moProcessPickList.SetGridNumericAttr grdLinePicked, kColLineQtyDist, moProcessPickList.iNbrDecPlaces, kMaxQtyLen, kAllowDecimalPlaces, lRow
        moProcessPickList.SetGridNumericAttr grdLinePicked, kColLineQtyToPick, moProcessPickList.iNbrDecPlaces, kMaxQtyLen, kAllowDecimalPlaces, lRow
        moProcessPickList.SetGridNumericAttr grdLinePicked, kColLineQtyAvail, moProcessPickList.iNbrDecPlaces, kMaxQtyLen, kAllowDecimalPlaces, lRow
        moProcessPickList.SetGridNumericAttr grdLinePicked, kColLineQtyShort, moProcessPickList.iNbrDecPlaces, kMaxQtyLen, kAllowDecimalPlaces, lRow
        moProcessPickList.SetGridNumericAttr grdLinePicked, kColLineQtyToPickByOrdUOM, moProcessPickList.iNbrDecPlaces, kMaxQtyLen, kAllowDecimalPlaces, lRow
        
        'For QtyPicked column, apply the AllowDecimalPlace rule defined for the item
        'in the grid row since this field can be edited by the user. Related UOM
        'convertion rules will be enforced in the data validation.
        iAllowDecimalQty = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineAllowDecimalQty))
        moProcessPickList.SetGridNumericAttr grdLinePicked, kColLineQtyPicked, moProcessPickList.iNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow
        
        SetRowValues lRow, True
       
'        'Get the bin info from the saved distributions.
'        lShipLineKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineShipLineKey))
'        lInvtTranKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineInvtTranKey))
'        If lInvtTranKey <> 0 Then
'            lSubItemKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineSubItemKey))
'            If lSubItemKey <> 0 Then
'                lItemKey = lSubItemKey
'            Else
'                lItemKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineItemKey))
'            End If
'
'            moProcessPickList.UpdateAutoDistBinCols grdLinePicked, lRow, lItemKey, lShipLineKey, lInvtTranKey
'        End If
       
        If OptLineShow(kShowShortOnly).Value = True Then
            HidePicked lRow
        End If
        
        'Set the active cell
        If mlCurrentRowShipLineKey > 0 And mlCurrentActiveColumn > 0 Then
            If lShipLineKey = mlCurrentRowShipLineKey Then
                mlCurrentLineActiveRow = lRow
            End If
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMGridRowLoaded", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub lkuAutoDistBin_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lTranType As Long
    lTranType = glGetValidLong(gsGridReadCell(grdLinePicked, grdLinePicked.ActiveRow, kColLineTranType))
    If lTranType = kTranTypeSOSO Then
        lTranType = kTranTypeSOSH
    Else
        lTranType = kTranTypeSOTS
    End If
    
    'Initialize the Distribution object if not already initialized
    gGridSetActiveCell grdLinePicked, grdLinePicked.ActiveRow, kColLineAutoDistBinID
    moProcessPickList.ProcessAutoBinLkuClick Me, grdLinePicked, moDMLineGrid, moGMLine, lkuAutoDistBin, lTranType
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuAutoDistBin_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub FireGMCellChangeIfNeeded()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'If the user clicks on the toolbar control immediately after typing a value on a
    'grid cell, sometimes the GM CellChange event does not fire.  Make sure the event
    'fires if it hasn't.  (Seems like the issue only applys for the toolbar control.
    'If any other control is clicked, the event still fires.)
    
    'The mbCellChangeFired variable is set to True when the event is called.
    'It is set to False on the Grid_EditChange event or when the user starts changes
    'the value on a cell.
    
    If mbCellChangeFired = False Then
        moGMLine_CellChange grdLinePicked.ActiveRow, grdLinePicked.ActiveCol
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FireGMCellChangeIfNeeded", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


