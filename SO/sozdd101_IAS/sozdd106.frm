VERSION 5.00
Begin VB.Form frmExitCreatePick 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Create Pick List"
   ClientHeight    =   2940
   ClientLeft      =   60
   ClientTop       =   330
   ClientWidth     =   6210
   HelpContextID   =   17774351
   Icon            =   "sozdd106.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2940
   ScaleWidth      =   6210
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdExitWithoutShipments 
      Caption         =   "E&xit without Shipments"
      Height          =   400
      Index           =   0
      Left            =   2040
      TabIndex        =   1
      Top             =   1560
      WhatsThisHelpID =   17774352
      Width           =   1965
   End
   Begin VB.CommandButton cmdReturnToPicking 
      Caption         =   "&Return to Picking"
      Default         =   -1  'True
      Height          =   400
      Index           =   1
      Left            =   2040
      TabIndex        =   0
      Top             =   2040
      WhatsThisHelpID =   17774353
      Width           =   1965
   End
   Begin VB.Label Label1 
      Caption         =   "Warning!       Shipments for this pick list have not been generated."
      Height          =   255
      Left            =   360
      TabIndex        =   4
      Top             =   360
      Width           =   5415
   End
   Begin VB.Label Label2 
      Caption         =   "If you wish to generate shipments, you must return to picking and either print or"
      Height          =   255
      Left            =   360
      TabIndex        =   3
      Top             =   840
      Width           =   6135
   End
   Begin VB.Label Label3 
      Caption         =   "preview the pick list.  Or, generate shipments from the menu at a later time."
      Height          =   255
      Left            =   360
      TabIndex        =   2
      Top             =   1080
      Width           =   5535
   End
End
Attribute VB_Name = "frmExitCreatePick"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Public Form Variables
Public moSotaObjects                As New Collection           'Collection of Loaded Objects
Private moClass                     As Object
'Private moSysSession        As Object
'Private moContextMenu   As clsContextMenu

Private miExitOption    As ExitOption

Const VBRIG_MODULE_ID_STRING = "sozdd106.FRM"

Private Sub cmdExitWithoutShipments_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    iExitOption = kExitWithoutShipment
    Unload Me

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdExitWithoutShipments_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdReturnToPicking_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    iExitOption = kReturnToPicking
    Me.Hide
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdExitWithoutShipments_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Activate()
    On Error Resume Next
End Sub
Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' center the form
    Me.Left = (Screen.Width - Me.Width) / 2
    Me.Top = (Screen.Height - Me.Height) / 2

    'Set up form level variables
    'Set moSysSession = moClass.moSysSession

    'BindContextMenu

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    If UnloadMode = vbFormControlMenu Then
        iExitOption = kReturnToPicking
    End If
    
    If UnloadMode <> vbFormCode Then
        Unload Me
        Cancel = True
    Else
        Me.Hide
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
    
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
        sMyName = "frmExitCreatePick"
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sMyName", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    WhatHelpPrefix = "SOZ"
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Property


'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
'    Set moContextMenu = New clsContextMenu  'Instantiate Context Menu Class
'
'    With moContextMenu
'    'Add any special .Bind or .BindGrid commands here for Drill Around/ Drill Down
'    'or for Grids - Here
'
'        ' Set .Hook = Me.WinHook3    'Assign Winhook control to Context Menu Class
'        Set .Form = frmExitCreatePick
'                .Init                      'Init will set properties of Winhook to intercept WM_RBUTTONDOWN
'    End With
'
''+++ VB/Rig Begin Pop +++
''+++ VB/Rig End +++
'        Exit Sub


'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Property Get oClass() As Object
    Set oClass = goClass
End Property
Public Property Get MyApp() As Object
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property

Public Property Get iExitOption() As ExitOption
'+++ VB/Rig Skip +++
'********************************************************************
' Description:
'    Return the exit option user pick
'********************************************************************
    iExitOption = miExitOption
End Property

Public Property Let iExitOption(iNewExitOption As ExitOption)
'+++ VB/Rig Skip +++
    miExitOption = iNewExitOption
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moClass = oNewClass

    'Exit this property
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim i As Integer

    '-- Unload all forms loaded from this main form
    gUnloadChildForms Me
         
    '-- Remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
        
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Unload Me
End Sub


