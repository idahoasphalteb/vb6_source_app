VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#157.0#0"; "EntryLookupControls.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#84.0#0"; "LookupView.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmCreatePick 
   Caption         =   "Create Pick List"
   ClientHeight    =   7875
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11475
   HelpContextID   =   17774219
   Icon            =   "sozdd101.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7875
   ScaleWidth      =   11475
   Begin VB.CheckBox chkPrintRptSetting 
      Caption         =   "Print &Report Settings"
      Height          =   285
      Left            =   7650
      TabIndex        =   3
      Top             =   550
      WhatsThisHelpID =   17774220
      Width           =   2085
   End
   Begin VB.CommandButton cmdSelectOrders 
      Caption         =   "&Select Orders..."
      Height          =   375
      Left            =   9960
      TabIndex        =   4
      Top             =   480
      WhatsThisHelpID =   17774221
      Width           =   1335
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   39570
      TabIndex        =   62
      Top             =   8835
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -10000
      TabIndex        =   65
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   69
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.ComboBox CustomCombo 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      ForeColor       =   &H80000012&
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   68
      TabStop         =   0   'False
      Text            =   "Combo1"
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   70
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   61
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   11475
      _ExtentX        =   20241
      _ExtentY        =   741
      Style           =   4
   End
   Begin VB.ComboBox cboSetting 
      Height          =   315
      Left            =   3800
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   540
      WhatsThisHelpID =   17774227
      Width           =   3585
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   7485
      WhatsThisHelpID =   73
      Width           =   11475
      _ExtentX        =   20241
      _ExtentY        =   688
      BrowseVisible   =   0   'False
      MessageVisible  =   -1  'True
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   64
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   71
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   67
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   63
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   66
      Top             =   2880
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   73
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin TabDlg.SSTab tabPickOrder 
      Height          =   6465
      Left            =   120
      TabIndex        =   5
      Top             =   960
      WhatsThisHelpID =   17774235
      Width           =   11295
      _ExtentX        =   19923
      _ExtentY        =   11404
      _Version        =   393216
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   556
      TabCaption(0)   =   "&Orders to Pick"
      TabPicture(0)   =   "sozdd101.frx":23D2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraOrdersToPick"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "&Lines to Pick"
      TabPicture(1)   =   "sozdd101.frx":23EE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraLinesToPick"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Opt&ions"
      TabPicture(2)   =   "sozdd101.frx":240A
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraOptions"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Custom Tab"
      TabPicture(3)   =   "sozdd101.frx":2426
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "fraCustomizer"
      Tab(3).ControlCount=   1
      Begin VB.Frame fraCustomizer 
         BorderStyle     =   0  'None
         Height          =   6000
         Left            =   -74880
         TabIndex        =   88
         Top             =   360
         Width           =   11055
      End
      Begin VB.Frame fraLinesToPick 
         BorderStyle     =   0  'None
         Height          =   6015
         Left            =   -74880
         TabIndex        =   17
         Top             =   360
         Width           =   11055
         Begin VB.Frame fraLinePicked 
            Caption         =   "0 Lines Selected For Picking"
            Height          =   5400
            Left            =   0
            TabIndex        =   18
            Top             =   555
            Width           =   11025
            Begin VB.Frame fraShow 
               Caption         =   "Show"
               Height          =   495
               Index           =   1
               Left            =   4080
               TabIndex        =   22
               Top             =   120
               Width           =   3135
               Begin VB.OptionButton OptLineShow 
                  Caption         =   "O&nly Short Picks"
                  Height          =   200
                  Index           =   1
                  Left            =   1080
                  TabIndex        =   24
                  Top             =   220
                  WhatsThisHelpID =   17774236
                  Width           =   1695
               End
               Begin VB.OptionButton OptLineShow 
                  Caption         =   "&All"
                  Height          =   195
                  Index           =   0
                  Left            =   120
                  TabIndex        =   23
                  Top             =   220
                  Value           =   -1  'True
                  WhatsThisHelpID =   17774237
                  Width           =   975
               End
            End
            Begin VB.CommandButton cmdDist 
               Caption         =   "Di&st..."
               Enabled         =   0   'False
               Height          =   375
               Left            =   10200
               TabIndex        =   27
               Top             =   240
               WhatsThisHelpID =   17774238
               Width           =   615
            End
            Begin VB.CommandButton cmdDeleteAll 
               Caption         =   "&Delete All"
               Enabled         =   0   'False
               Height          =   375
               Index           =   1
               Left            =   2520
               TabIndex        =   21
               Top             =   240
               WhatsThisHelpID =   17774239
               Width           =   1065
            End
            Begin VB.CommandButton cmdPickAll 
               Caption         =   "&Pick All"
               Enabled         =   0   'False
               Height          =   375
               Index           =   1
               Left            =   1150
               TabIndex        =   20
               Top             =   240
               WhatsThisHelpID =   17774240
               Width           =   1000
            End
            Begin VB.CommandButton cmdClearAll 
               Caption         =   "&Clear All"
               Enabled         =   0   'False
               Height          =   375
               Index           =   1
               Left            =   120
               TabIndex        =   19
               Top             =   240
               WhatsThisHelpID =   17774241
               Width           =   1000
            End
            Begin VB.CommandButton cmdComponents 
               Caption         =   "&Kit Components..."
               Enabled         =   0   'False
               Height          =   375
               Left            =   8640
               TabIndex        =   26
               Top             =   240
               WhatsThisHelpID =   17774242
               Width           =   1455
            End
            Begin VB.TextBox txtNavSubItem 
               Height          =   285
               Left            =   480
               TabIndex        =   79
               Text            =   "Text1"
               Top             =   0
               Visible         =   0   'False
               WhatsThisHelpID =   17774243
               Width           =   945
            End
            Begin VB.TextBox txtNavUOM 
               Height          =   285
               Left            =   2340
               TabIndex        =   78
               Text            =   "Text1"
               Top             =   0
               Visible         =   0   'False
               WhatsThisHelpID =   17774244
               Width           =   945
            End
            Begin FPSpreadADO.fpSpread grdLinePicked 
               Height          =   4575
               Left            =   120
               TabIndex        =   25
               Top             =   690
               WhatsThisHelpID =   17776360
               Width           =   10755
               _Version        =   524288
               _ExtentX        =   18971
               _ExtentY        =   8070
               _StockProps     =   64
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               SpreadDesigner  =   "sozdd101.frx":2442
               TextTip         =   2
               AppearanceStyle =   0
            End
            Begin LookupViewControl.LookupView navGrid 
               Height          =   285
               Index           =   1
               Left            =   11200
               TabIndex        =   80
               TabStop         =   0   'False
               ToolTipText     =   "Lookup"
               Top             =   240
               Visible         =   0   'False
               WhatsThisHelpID =   17774245
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin LookupViewControl.LookupView navGridUOM 
               Height          =   285
               Left            =   3300
               TabIndex        =   81
               TabStop         =   0   'False
               ToolTipText     =   "Lookup"
               Top             =   0
               Visible         =   0   'False
               WhatsThisHelpID =   17774246
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin LookupViewControl.LookupView navGridSubItem 
               Height          =   285
               Left            =   1440
               TabIndex        =   82
               TabStop         =   0   'False
               ToolTipText     =   "Lookup"
               Top             =   0
               Visible         =   0   'False
               WhatsThisHelpID =   17774247
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtlkuAutoDistBin 
               Height          =   255
               Left            =   6960
               TabIndex        =   83
               Top             =   120
               Visible         =   0   'False
               WhatsThisHelpID =   17774248
               Width           =   1215
               _Version        =   65536
               _ExtentX        =   2143
               _ExtentY        =   450
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin LookupViewControl.LookupView lkuAutoDistBin 
               Height          =   285
               Left            =   8160
               TabIndex        =   84
               TabStop         =   0   'False
               ToolTipText     =   "Lookup"
               Top             =   120
               Visible         =   0   'False
               WhatsThisHelpID =   17774249
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
         End
         Begin VB.CommandButton cmdLinesOnHold 
            Caption         =   "Lines On &Hold..."
            Height          =   375
            Left            =   9240
            TabIndex        =   28
            Top             =   120
            WhatsThisHelpID =   17774250
            Width           =   1455
         End
      End
      Begin VB.Frame fraOrdersToPick 
         BorderStyle     =   0  'None
         Height          =   6000
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   11055
         Begin VB.Frame fraOrderPicked 
            Caption         =   "0 Orders Selected For Picking"
            Height          =   5400
            Left            =   0
            TabIndex        =   7
            Top             =   555
            Width           =   11025
            Begin VB.CommandButton cmdClearAll 
               Caption         =   "&Clear All"
               Enabled         =   0   'False
               Height          =   375
               Index           =   0
               Left            =   120
               TabIndex        =   8
               Top             =   240
               WhatsThisHelpID =   17774251
               Width           =   1000
            End
            Begin VB.CommandButton cmdPickAll 
               Caption         =   "&Pick All"
               Enabled         =   0   'False
               Height          =   375
               Index           =   0
               Left            =   1150
               TabIndex        =   9
               Top             =   240
               WhatsThisHelpID =   17774252
               Width           =   1000
            End
            Begin VB.CommandButton cmdDeleteAll 
               Caption         =   "&Delete All"
               Enabled         =   0   'False
               Height          =   375
               Index           =   0
               Left            =   2520
               TabIndex        =   10
               Top             =   240
               WhatsThisHelpID =   17774253
               Width           =   1065
            End
            Begin VB.CommandButton cmdOrderPickLines 
               Caption         =   "O&rder Pick Lines"
               Height          =   375
               Left            =   9240
               TabIndex        =   15
               Top             =   240
               WhatsThisHelpID =   17774254
               Width           =   1455
            End
            Begin VB.Frame fraShow 
               Caption         =   "Show"
               Height          =   495
               Index           =   0
               Left            =   4080
               TabIndex        =   11
               Top             =   120
               Width           =   3135
               Begin VB.OptionButton OptOrderShow 
                  Caption         =   "&All"
                  Height          =   195
                  Index           =   0
                  Left            =   120
                  TabIndex        =   13
                  Top             =   220
                  Value           =   -1  'True
                  WhatsThisHelpID =   17774255
                  Width           =   975
               End
               Begin VB.OptionButton OptOrderShow 
                  Caption         =   "O&nly Short Picks"
                  Height          =   200
                  Index           =   1
                  Left            =   1080
                  TabIndex        =   12
                  Top             =   220
                  WhatsThisHelpID =   17774256
                  Width           =   1695
               End
            End
            Begin FPSpreadADO.fpSpread grdOrderPicked 
               Height          =   4575
               Left            =   120
               TabIndex        =   14
               Top             =   690
               WhatsThisHelpID =   17776361
               Width           =   10755
               _Version        =   524288
               _ExtentX        =   18971
               _ExtentY        =   8070
               _StockProps     =   64
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               SpreadDesigner  =   "sozdd101.frx":2856
               AppearanceStyle =   0
            End
            Begin LookupViewControl.LookupView navGrid 
               Height          =   285
               Index           =   0
               Left            =   11200
               TabIndex        =   77
               TabStop         =   0   'False
               ToolTipText     =   "Lookup"
               Top             =   0
               Visible         =   0   'False
               WhatsThisHelpID =   17774257
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
         End
         Begin VB.CommandButton cmdOrdersOnHold 
            Caption         =   "Orders On &Hold..."
            Height          =   375
            Left            =   9240
            TabIndex        =   16
            Top             =   120
            WhatsThisHelpID =   17774258
            Width           =   1455
         End
      End
      Begin VB.Frame fraOptions 
         BorderStyle     =   0  'None
         Height          =   5895
         Left            =   -74880
         TabIndex        =   74
         Top             =   400
         Width           =   11055
         Begin VB.Frame fraInclude 
            Caption         =   "In&clude"
            Height          =   2415
            Left            =   4320
            TabIndex        =   40
            Top             =   120
            Width           =   2805
            Begin VB.CheckBox chkInclude 
               Caption         =   "Extended Line Comments"
               Height          =   314
               Index           =   0
               Left            =   180
               TabIndex        =   41
               Top             =   270
               WhatsThisHelpID =   17774259
               Width           =   2295
            End
            Begin VB.CheckBox chkInclude 
               Caption         =   "Abbreviated Ship To Address"
               Height          =   314
               Index           =   2
               Left            =   180
               TabIndex        =   43
               Top             =   990
               WhatsThisHelpID =   17774260
               Width           =   2535
            End
            Begin VB.CheckBox chkInclude 
               Caption         =   "BTO Kit List"
               Height          =   314
               Index           =   3
               Left            =   180
               TabIndex        =   44
               Top             =   1350
               WhatsThisHelpID =   17774261
               Width           =   2295
            End
            Begin VB.CheckBox chkInclude 
               Caption         =   "Non-Inventory"
               Height          =   314
               Index           =   4
               Left            =   180
               TabIndex        =   45
               Top             =   1710
               WhatsThisHelpID =   17774262
               Width           =   2295
            End
            Begin VB.CheckBox chkInclude 
               Caption         =   "Ship To Address"
               Height          =   314
               Index           =   1
               Left            =   180
               TabIndex        =   42
               Top             =   630
               WhatsThisHelpID =   17774263
               Width           =   2295
            End
         End
         Begin VB.Frame fraFormat 
            Caption         =   "Pick &By"
            Height          =   1755
            Left            =   240
            TabIndex        =   29
            Top             =   120
            Width           =   3885
            Begin VB.Frame fraSortBy 
               Caption         =   "Sort By"
               Height          =   1395
               Left            =   1440
               TabIndex        =   33
               Top             =   240
               Width           =   2295
               Begin VB.ComboBox cboBinSort 
                  Height          =   315
                  HelpContextID   =   101996
                  ItemData        =   "sozdd101.frx":2C6A
                  Left            =   720
                  List            =   "sozdd101.frx":2C6C
                  Style           =   2  'Dropdown List
                  TabIndex        =   37
                  Top             =   840
                  WhatsThisHelpID =   17776591
                  Width           =   1395
               End
               Begin VB.ComboBox cboZoneSort 
                  Height          =   315
                  HelpContextID   =   101996
                  ItemData        =   "sozdd101.frx":2C6E
                  Left            =   720
                  List            =   "sozdd101.frx":2C70
                  Style           =   2  'Dropdown List
                  TabIndex        =   35
                  Top             =   255
                  WhatsThisHelpID =   17776592
                  Width           =   1395
               End
               Begin VB.Label lblBinSort 
                  Caption         =   "Bin"
                  Height          =   375
                  Left            =   225
                  TabIndex        =   36
                  Top             =   885
                  Width           =   615
               End
               Begin VB.Label lblZoneSort 
                  Caption         =   "Zone"
                  Height          =   255
                  Left            =   225
                  TabIndex        =   34
                  Top             =   300
                  Width           =   735
               End
            End
            Begin VB.OptionButton optFormat 
               Caption         =   "Wave"
               Height          =   314
               Index           =   0
               Left            =   135
               TabIndex        =   30
               Top             =   360
               WhatsThisHelpID =   17774264
               Width           =   1680
            End
            Begin VB.OptionButton optFormat 
               Caption         =   "Order"
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   32
               Top             =   1200
               WhatsThisHelpID =   17774265
               Width           =   1440
            End
            Begin VB.CheckBox chkWaveSummary 
               Caption         =   "Summary"
               Height          =   314
               Left            =   360
               TabIndex        =   31
               Top             =   720
               WhatsThisHelpID =   17774267
               Width           =   1215
            End
         End
         Begin VB.Frame fraSort 
            Caption         =   "Sor&t"
            Height          =   1860
            Left            =   240
            TabIndex        =   54
            Top             =   2715
            Width           =   10695
            Begin FPSpreadADO.fpSpread grdSort 
               Height          =   1440
               Left            =   120
               TabIndex        =   55
               Top             =   240
               WhatsThisHelpID =   25
               Width           =   10350
               _Version        =   524288
               _ExtentX        =   18256
               _ExtentY        =   2540
               _StockProps     =   64
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               SpreadDesigner  =   "sozdd101.frx":2C72
               AppearanceStyle =   0
            End
         End
         Begin VB.Frame fraIncludeInDetail 
            Caption         =   "Include In &Detail"
            Height          =   2415
            Left            =   7320
            TabIndex        =   46
            Top             =   120
            Width           =   3615
            Begin VB.CheckBox chkIncludeInDetail 
               Caption         =   "Ship Date"
               Height          =   314
               Index           =   0
               Left            =   180
               TabIndex        =   47
               Top             =   270
               WhatsThisHelpID =   17774268
               Width           =   2040
            End
            Begin VB.CheckBox chkIncludeInDetail 
               Caption         =   "Ship Complete Indicator"
               Height          =   314
               Index           =   1
               Left            =   180
               TabIndex        =   48
               Top             =   630
               WhatsThisHelpID =   17774269
               Width           =   2040
            End
            Begin VB.CheckBox chkIncludeInDetail 
               Caption         =   "Substitution Indicator"
               Height          =   314
               Index           =   2
               Left            =   180
               TabIndex        =   49
               Top             =   990
               WhatsThisHelpID =   17774270
               Width           =   2040
            End
            Begin VB.CheckBox chkIncludeInDetail 
               Caption         =   "Kit Component Indicator"
               Height          =   314
               Index           =   3
               Left            =   180
               TabIndex        =   50
               Top             =   1350
               WhatsThisHelpID =   17774271
               Width           =   2400
            End
            Begin VB.CheckBox chkIncludeInDetail 
               Caption         =   "Bin Location"
               Height          =   314
               Index           =   4
               Left            =   2280
               TabIndex        =   51
               Top             =   270
               WhatsThisHelpID =   17774272
               Width           =   1215
            End
            Begin VB.CheckBox chkIncludeInDetail 
               Caption         =   "Priority"
               Height          =   314
               Index           =   5
               Left            =   2280
               TabIndex        =   52
               Top             =   630
               WhatsThisHelpID =   17774273
               Width           =   1215
            End
            Begin VB.CheckBox chkIncludeInDetail 
               Caption         =   "Ship Via"
               Height          =   314
               Index           =   6
               Left            =   2280
               TabIndex        =   53
               Top             =   990
               WhatsThisHelpID =   17774274
               Width           =   1215
            End
         End
         Begin VB.Frame fraShipping 
            Caption         =   "S&hipping"
            Height          =   615
            Left            =   240
            TabIndex        =   38
            Top             =   1920
            Width           =   3885
            Begin VB.CheckBox chkGenShip 
               Caption         =   "&Generate Shipments"
               Height          =   285
               Left            =   240
               TabIndex        =   39
               Top             =   220
               WhatsThisHelpID =   17774275
               Width           =   1815
            End
         End
         Begin VB.Frame fraMessage 
            Caption         =   "Messa&ge"
            Height          =   1230
            Left            =   240
            TabIndex        =   56
            Top             =   4645
            Width           =   10695
            Begin VB.PictureBox pctMessage 
               Height          =   825
               Left            =   135
               ScaleHeight     =   765
               ScaleWidth      =   10305
               TabIndex        =   57
               TabStop         =   0   'False
               Top             =   240
               Width           =   10365
               Begin VB.TextBox txtMessageHeader 
                  Appearance      =   0  'Flat
                  Height          =   285
                  Index           =   0
                  Left            =   0
                  MaxLength       =   50
                  TabIndex        =   58
                  Top             =   0
                  WhatsThisHelpID =   18
                  Width           =   10335
               End
               Begin VB.TextBox txtMessageHeader 
                  Appearance      =   0  'Flat
                  Height          =   285
                  Index           =   1
                  Left            =   0
                  MaxLength       =   50
                  TabIndex        =   59
                  Top             =   240
                  WhatsThisHelpID =   18
                  Width           =   10335
               End
               Begin VB.TextBox txtMessageHeader 
                  Appearance      =   0  'Flat
                  Height          =   285
                  Index           =   2
                  Left            =   0
                  MaxLength       =   50
                  TabIndex        =   60
                  Top             =   510
                  WhatsThisHelpID =   18
                  Width           =   10335
               End
               Begin VB.TextBox txtMessageHeader 
                  Appearance      =   0  'Flat
                  Height          =   285
                  Index           =   3
                  Left            =   0
                  MaxLength       =   50
                  TabIndex        =   76
                  Top             =   765
                  WhatsThisHelpID =   18
                  Width           =   9015
               End
               Begin VB.TextBox txtMessageHeader 
                  Appearance      =   0  'Flat
                  Height          =   285
                  Index           =   4
                  Left            =   0
                  MaxLength       =   50
                  TabIndex        =   75
                  Top             =   1020
                  WhatsThisHelpID =   18
                  Width           =   8770
               End
               Begin VB.Line Line1 
                  Index           =   2
                  X1              =   0
                  X2              =   8835
                  Y1              =   765
                  Y2              =   765
               End
               Begin VB.Line Line1 
                  Index           =   1
                  X1              =   0
                  X2              =   8835
                  Y1              =   510
                  Y2              =   510
               End
               Begin VB.Line Line1 
                  Index           =   4
                  X1              =   0
                  X2              =   8835
                  Y1              =   240
                  Y2              =   240
               End
               Begin VB.Line Line1 
                  Index           =   0
                  X1              =   0
                  X2              =   8835
                  Y1              =   1020
                  Y2              =   1020
               End
            End
         End
      End
   End
   Begin EntryLookupControls.TextLookup lkuPickListNo 
      Height          =   285
      Left            =   1000
      TabIndex        =   1
      Top             =   540
      WhatsThisHelpID =   17774281
      Width           =   1800
      _ExtentX        =   3175
      _ExtentY        =   503
      ForeColor       =   -2147483640
      LookupMode      =   0
      MaxLength       =   10
      LookupID        =   "SOReprntPick"
      BoundColumn     =   "PickListNo"
      BoundTable      =   "tsoPickList"
      sSQLReturnCols  =   "PickListNo,lkuPickListNo,;"
   End
   Begin EntryLookupControls.TextLookup TextLookup1 
      Height          =   285
      Left            =   0
      TabIndex        =   86
      Top             =   0
      WhatsThisHelpID =   17774282
      Width           =   1800
      _ExtentX        =   3175
      _ExtentY        =   503
      ForeColor       =   -2147483640
      LookupMode      =   0
      MaxLength       =   10
      LookupID        =   "SOReprntPick"
      BoundColumn     =   "PickListNo"
      BoundTable      =   "tsoPickList"
      sSQLReturnCols  =   "PickListNo,lkuPickListNo,;"
   End
   Begin MSComDlg.CommonDialog dlgCreateRPTFile 
      Left            =   2640
      Top             =   360
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label currPickListNo 
      Height          =   255
      Left            =   1080
      TabIndex        =   87
      Top             =   600
      Width           =   1695
   End
   Begin VB.Label lblPickListNo 
      AutoSize        =   -1  'True
      Caption         =   "Pick List"
      Height          =   225
      Left            =   240
      TabIndex        =   85
      Top             =   585
      Width           =   600
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   72
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Label lblSetting 
      Caption         =   "Se&tting"
      Height          =   225
      Left            =   3050
      TabIndex        =   0
      Top             =   585
      Width           =   615
   End
End
Attribute VB_Name = "frmCreatePick"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************************
'     Name: sozdd101
'     Desc: Create Pick List
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: JY 04/01/2004
'     Mods: RS 09/13/2010 Clean up standard errorhandling and WA removal
'                       add bak in bBuildReport - iReprintFlag flag
'************************************************************************************

Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

'Public Form Variables
Public moSotaObjects                As New Collection
Public moMapSrch                    As Collection    'Search Buttons

'Private Object Variables
Public moClass                      As Object
Private moAppDB                     As Object
Private msCompanyID                 As String
Private moSysSession                As Object
Private WithEvents moDMLineGrid     As clsDmGrid
Attribute moDMLineGrid.VB_VarHelpID = -1
Private WithEvents moDMOrderGrid    As clsDmGrid
Attribute moDMOrderGrid.VB_VarHelpID = -1
Private WithEvents moDMPickListHeader As clsDmForm
Attribute moDMPickListHeader.VB_VarHelpID = -1
Private WithEvents moGMLine         As clsGridMgr
Attribute moGMLine.VB_VarHelpID = -1
Private WithEvents moGMOrder        As clsGridMgr
Attribute moGMOrder.VB_VarHelpID = -1
Private moContextMenu               As clsContextMenu
Private moModuleOptions             As clsModuleOptions
Private moGridNavUOM                As clsGridLookup
Private moGridNavSubItem            As clsGridLookup
Private moGridLkuAutoDistBin        As clsGridLookup
Private moItemDist                  As Object
Private moProcessPickList           As clsProcessPickList

'Private Miscellaneous Variables
Private mbEnterAsTab                As Boolean
Private msHomeCurrID                As String
Private mlLanguage                  As Long
Private mbLoading                   As Boolean
Private mlRunMode                   As Long
Private mbCancelShutDown            As Boolean
Private mbLoadSuccess               As Boolean
Private bMaskCtrlLostFocus          As Boolean
Private miSPID                      As Integer
Private miMousePointer              As Integer
Private mlPickListKey               As Long
Public mbLineGridRefreshNeeded     As Boolean
Public mbOrderGridRefreshNeeded    As Boolean
Private mbCheckCanceled             As Boolean
Private mbManualClick               As Boolean
Private miMaxSerialNbrsOnPickList   As Integer
Private mbLoadSetting               As Boolean
Private miCallingTask               As CallingTask
Private msPickListNo                As String
Private mbLeaveCell                 As Boolean
Private SortSettings()              As SortSetting
Private miNbrDecPlaces              As Integer
Private mbCellChangeFired           As Boolean
Private mlCurrentActiveColumn       As Long
Private mlCurrentLineActiveRow      As Long
Private mlCurrentOrderActiveRow     As Long
Private mlCurrentRowShipLineKey     As Long
Private mlCurrentRowOrderKey        As Long

Private miOverShipmentPolicy        As SOOverShipmentPolicy
Private miCreditCheckAtPicking      As Integer
Private miCreditHoldAutoRelease     As Integer
Private miPickOrdQty                As Integer
Private miEmptyBins                  As Integer
Private miEmptyRandomBins            As Integer
Private miSecurityLevel              As Integer
Private msUserID                     As String
Private mlSessionID                  As Long
Private mbShipmentGenerated          As Boolean

'Public Variable for reporting
Public moReport                     As clsReportEngine
Public moSettings                   As clsSettings
Public moSort                       As clsSort
Public moDDData                     As clsDDData
Public moOptions                    As clsOptions
Public moPrinter                    As Printer
Public sRealTableCollection         As Collection
Public sWorkTableCollection         As Collection
Private miRptFormat                 As Integer
Private miPrintCount                As Integer
Private miDeliveryMeth              As Integer
Private miBinSortOrder              As OptionsBinSortOrder
Private miZoneSortOrder             As OptionsZoneSortOrder
Private mlTaskID                    As Long
Private mlLogicalLockKey            As Long
Private mbIsWaveSelected            As Boolean

Private Const kNoMoreEntries = 999

'Grid Edit Variables
Private miAllowNegQty               As Integer
Private miItemType                  As Integer
Private miAllowDecimalQty           As Integer
Private miSOSelectedCount           As Integer
Private miTOSelectedCount           As Integer
Private miSOLSelectedCount          As Integer
Private miTOLSelectedCount          As Integer
          
'Private Form Resize Variables
Private miMinFormHeight As Long
Private miMinFormWidth As Long
Private miOldFormHeight As Long
Private miOldFormWidth As Long

'Private Constants for Loading Task for picklist
Private Const kLoadingTaskImmediatePick = 1
Private Const kLoadingTaskReprintPickList = 2

'Private Constants for Tab Order
Private Const kTabOrder             As Integer = 0
Private Const kTabLine              As Integer = 1
Private Const kTabOptions           As Integer = 2
Private Const kTabCustomizer        As Integer = 3
Private Const kSetupCaptionForOrderGridOnly As Integer = 0
Private Const kSetupCaptionForLineGridOnly  As Integer = 1
Private Const kSetupCaptionForBothGrid      As Integer = 2

Private Const kFinishProceeded      As Integer = 1
Private Const kFinishCancelled      As Integer = 0

'Private Constants for tran type
Private Const kSalesOrder           As Integer = 0
Private Const kTransfer             As Integer = 1

Private m_LastEnterCol As Long
Private m_LastEnterRow As Long


'Private Constants for LogicLockType
Private Const LOCKTYPE_PICKLIST_RECOVERY  As Integer = 4

Const VBRIG_MODULE_ID_STRING = "SOZDD101.FRM"

Public Property Get lRunMode() As Long
'+++ VB/Rig Skip +++
'********************************************************************
' Description:
'    lRunMode contains the user-defined run mode context in which the
'    class object was Initialzed. The run mode is extracted from the
'    context passed in through the InitializeObject.
'********************************************************************
    lRunMode = mlRunMode
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Skip +++
    mlRunMode = lNewRunMode
End Property

Public Property Get oClass() As Object
'+++ VB/Rig Skip +++
'***********************************************************************
' Description:
'    oClass contains the reference to the parent class object. The form
'    needs this reference to use the public variables created within the
'    class object.
'***********************************************************************
    Set oClass = moClass
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Skip +++
    Set moClass = oNewClass
End Property

Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function

Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Skip +++
    bLoadSuccess = mbLoadSuccess
End Property

Public Property Get bCancelShutDown()
'+++ VB/Rig Skip +++
'**********************************************************************
' Description:
'    bCancelShutDown tells the framework whether the form has requested
'    the shutdown process to be cancelled.
'**********************************************************************
    bCancelShutDown = mbCancelShutDown
End Property

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Skip +++
    '-- the three-letter abbreviation used below is built as follows:
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> + "Z"
    WhatHelpPrefix = "SOZ"
End Property

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Skip +++
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> + "Z"
    FormHelpPrefix = "SOZ"
End Property
Public Property Get lPickListKey() As Long
'+++ VB/Rig Skip +++
    
    lPickListKey = mlPickListKey
End Property

Public Property Let lPickListKey(lNewPickListKey As Long)
'+++ VB/Rig Skip +++
    
    mlPickListKey = lNewPickListKey
End Property


Private Sub cboBinSort_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboBinSort, True
    #End If
'+++ End Customizer Code Push +++

    miBinSortOrder = cboBinSort.ItemData(cboBinSort.ListIndex)
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "cboBinSort_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub cboZoneSort_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboZoneSort, True
    #End If
'+++ End Customizer Code Push +++

    miZoneSortOrder = cboZoneSort.ItemData(cboZoneSort.ListIndex)
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "cboZoneSort_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub chkInclude_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkInclude(Index), True
    #End If
'+++ End Customizer Code Push +++

    If Index = kInclAbbrvShipToAddr Then
        If chkInclude(kInclAbbrvShipToAddr).Value = vbChecked Then
            chkInclude(kInclShipToAddr).Enabled = False
        ElseIf chkWaveSummary.Value = vbUnchecked Then
            chkInclude(kInclShipToAddr).Enabled = True
        End If
    End If
    
    If Index = kInclShipToAddr Then
        If chkInclude(kInclShipToAddr).Value = vbChecked Then
            chkInclude(kInclAbbrvShipToAddr).Enabled = False
        ElseIf chkWaveSummary.Value = vbUnchecked Then
            chkInclude(kInclAbbrvShipToAddr).Enabled = True
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkInclude_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub chkWaveSummary_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkWaveSummary, True
    #End If
'+++ End Customizer Code Push +++

Dim i As OptionsInclude
Dim j As OptionsIncludeInDetail

        'Don't execute the following logic if option are not set by user
        'If mbManualClick = True Then Exit Sub

        If chkWaveSummary.Value = vbChecked Then
        'When suumary is checked, uncheck and disable all include checkboxes except
        'Non-inventory.
            For i = 0 To chkInclude.UBound
                If i = iInclNonInvt Then
                    chkInclude(i).Enabled = True
                Else
                    chkInclude(i).Value = vbUnchecked
                    chkInclude(i).Enabled = False
                End If
            Next i
            
            For j = 0 To chkIncludeInDetail.UBound
                If j = iInclDetlBinLocation Then
                    chkIncludeInDetail(j).Enabled = True
                Else
                    chkIncludeInDetail(j).Value = vbUnchecked
                    chkIncludeInDetail(j).Enabled = False
                End If
            Next j
                        
            miRptFormat = kPickRptTypeSummary
        Else
        'When summary is unchecked, enable all include checkboxes.
            For i = 0 To chkInclude.UBound
                chkInclude(i).Enabled = True
            Next i
            
            For j = 0 To chkIncludeInDetail.UBound
                chkIncludeInDetail(j).Enabled = True
            Next j
            
            If optFormat(kPickFormatOrder).Value = True Then
                miRptFormat = kPickRptTypeOrder
            Else
                miRptFormat = kPickRptTypeDetail
            End If
        End If
        
        'Re-initial the sort object with corresponding format
        If Not bInitSort(miRptFormat) Then Exit Sub
        
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "chkWaveSummary_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub chkWaveZone_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'    If chkWaveSummary.Value = vbChecked Then
'    'if the zone picking checkbox is checked, the wave pick will be sorted first by warehouse and then by
'    'zone and then by bin with zone.  If checked, set second sort grid row to be 'zone'
'    'and do not let the user change it.  if unchecked, remove the zone sort from the sort grid.
'        If Not bInitSort(kPickRptTypeSummary) Then Exit Sub
'    Else
'        If Not bInitSort(kPickRptTypeDetail) Then Exit Sub
'    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "chkWaveZone_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub cmdClearAll_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdClearAll(Index), True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'**********************************************************************
' Description:
'    This routine deletes the distributions for all the Short Pick
'    lines.
'**********************************************************************
On Error GoTo ExpectedErrorRoutine
    
  
    SetMouse
    
    
    'Call DeleteShipLines to delete the distribution for the select lines.
    If Not bClearAllRowsPicked Then
        ResetMouse
        Exit Sub
    End If
    
    If Err.Number <> 0 Then
        gClearSotaErr
    End If
    On Error GoTo VBRigErrorRoutine
    
    'Refresh frame captions for display grids
    If Index = kOrder Then
        grdOrderPicked.redraw = False
        moDMOrderGrid.Refresh
        grdOrderPicked.redraw = True
        'SetupRowsSelCaption kSetupCaptionForOrderGridOnly
        
        'Set the Line Refreshed Flag to Ture so when user click the line tab
        'the line grid will display the updated data
        mbLineGridRefreshNeeded = True
    Else
        grdLinePicked.redraw = False
        moDMLineGrid.Refresh
        grdLinePicked.redraw = True
        
        'SetupRowsSelCaption kSetupCaptionForLineGridOnly
        
        'Set the Order Refreshed Flag to Ture so when user click the order tab
        'the order grid will display the updated data
        mbOrderGridRefreshNeeded = True
    End If
    
    ResetMouse

    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    
    Exit Sub
'+++ VB/Rig Begin Pop +++

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdClearAll_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub cmdComponents_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdComponents, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'********************************************************************
' Description:
'    This routine calls LoadCompForm to load the Component form
'    for a shipline that has a BTOKit
'********************************************************************
    
On Error GoTo ExpectedErrorRoutine
    
    SetMouse
    
    Dim lRow            As Long
    
    'Get the active row
    lRow = glGridGetActiveRow(grdLinePicked)
    
    'Store the Current cell value in the variable
    mlCurrentLineActiveRow = lRow
    mlCurrentActiveColumn = glGridGetActiveCol(grdLinePicked)
    mlCurrentRowShipLineKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineShipLineKey))
    
    'Initialize the Distribution object if not already initialized
    CreateDistForm lRow
    
    Me.Enabled = False
    
    If Not moItemDist Is Nothing Then
        If Not moProcessPickList.bLoadCompForm(grdLinePicked, lRow, mbShipmentGenerated, _
                        miEmptyBins, miEmptyRandomBins) Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgCMUnexpectedSPError, "Load Component Form"
            ResetMouse
            Me.Enabled = True
            Exit Sub
        End If
    End If
    
    If frmCompItem.bIsDirty Then
        'Was some data changed on the Detail form?
        grdLinePicked.redraw = False
        moDMLineGrid.Refresh
        grdLinePicked.redraw = True
        'Set the Line Refreshed Flag to Ture so when user click the line tab
        'the line grid will display the updated data
        mbOrderGridRefreshNeeded = True
    End If
    
    ResetMouse
    
    Me.Enabled = True
    
    'Highlight the row where the user has selected before click the Dist button
    If mlCurrentLineActiveRow > 0 Then
        gGridSetSelectRow grdLinePicked, mlCurrentLineActiveRow
    End If
    
    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdComponents_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub cmdDeleteAll_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdDeleteAll(Index), True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'********************************************************************
' Description:
'    This routine deletes all the Short Pick Shiplines
'
'********************************************************************
On Error GoTo ExpectedErrorRoutine

Dim sWhere        As String
Dim sShortPickSelected As String
Dim iUpdateCounterViolation As Integer

    SetMouse
    
    If OptOrderShow(kShowShortOnly).Value = True Then
    'If only short pick is displayed,  then only clear the short pick ship lines
        sShortPickSelected = " AND ShortPick = 1"
        
        'Build where clause for the lines need to be deleted
        sWhere = " wrk.ShipLineKey IN (SELECT ShipLineKey FROM #tsoCreatePickWrk2 WHERE LineHold <> 1 AND CrHold <> 1 AND OrderHold <> 1" & sShortPickSelected & ")"
    Else
        sShortPickSelected = ""
        sWhere = ""
        
    End If
    
    'Call DeleteShipLines to delete the selected Shiplines
    If Not moProcessPickList.bDeleteShipLines(sWhere, kDeleteDistributionOnlyNo, iUpdateCounterViolation) Then
        ResetMouse
        Exit Sub
    End If
    
    If Err.Number <> 0 Then
        gClearSotaErr
    End If
    
    On Error GoTo VBRigErrorRoutine
    
    'Refresh frame captions for display grids
    If Index = kOrder Then
        grdOrderPicked.redraw = False
        moDMOrderGrid.Refresh
        SetupRowsSelCaption kSetupCaptionForOrderGridOnly
        grdOrderPicked.redraw = True
        
        'Set the Line Refreshed Flag to Ture so when user click the line tab
        'the line grid will display the updated data
        mbLineGridRefreshNeeded = True
    Else
        grdLinePicked.redraw = False
        moDMLineGrid.Refresh
        SetupRowsSelCaption kSetupCaptionForLineGridOnly
        grdLinePicked.redraw = True
        
        'Set the Order Refreshed Flag to Ture so when user click the order tab
        'the order grid will display the updated data
        mbOrderGridRefreshNeeded = True
    End If
 
    ResetMouse
    
        Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    Exit Sub

'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdDeleteAll_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++

End Sub

Private Sub cmdDist_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdDist, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'**********************************************************************
' Description:
'    This routine Loads the Distribution Form (imzde001)for user to
'    view or modify the distribution on a Shipline.
'
'**********************************************************************
On Error GoTo ExpectedErrorRoutine

Dim lRow            As Long
    
    'Get the active row
    lRow = glGridGetActiveRow(grdLinePicked)

    'Store the Current cell value in the variable
    mlCurrentLineActiveRow = lRow
    mlCurrentActiveColumn = glGridGetActiveCol(grdLinePicked)
    mlCurrentRowShipLineKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineShipLineKey))
   
    SetMouse
    
    'Initialize the Distribution object if not already initialized
    CreateDistForm lRow
    
    If Not moItemDist Is Nothing Then
        If moProcessPickList.bIsValidQtyToPick(Me, moDMLineGrid, grdLinePicked, lRow) Then
        'If the QtyToPick is valid, load the distribution form
        'so use can enter new distribution or view and update exist distribution
            If moProcessPickList.bCreateManualDistribution(Me, grdLinePicked, moDMLineGrid, lRow) Then
                'moProcessPickList.UpdateGridLine grdLinePicked, moDMLineGrid, lRow
                moProcessPickList.bUpdateOrderLineStatus moDMLineGrid, grdLinePicked, lRow
                
                'Set the Order Refreshed Flag to Ture so when user click the Order tab
                'the order grid will display the updated data
                mbOrderGridRefreshNeeded = True
            Else
                grdLinePicked_Click kColLineQtyToPick, lRow
                grdLinePicked.SetFocus
            End If
        Else
        'QtyToPick is invalid, set the focus back the QtyToPick cell
            grdLinePicked_Click kColLineQtyToPick, lRow
            grdLinePicked.SetFocus
            Exit Sub
        End If
    End If
    
    'Highlight the row where the user has selected before click the Dist button
    If mlCurrentLineActiveRow > 0 Then
        gGridSetSelectRow grdLinePicked, mlCurrentLineActiveRow
    End If

    ResetMouse
    
    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "cmdDist_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub cmdLinesOnHold_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdLinesOnHold, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'**********************************************************************
' Description:
'    This routine loads the LinesOnHold form (frmLinesOnHold)
'
'**********************************************************************
    
On Error GoTo ExpectedErrorRoutine
    
    Dim sSQL As String
    Dim lRow As Long
    
    Static bLinesOnHoldLoaded As Boolean

    Set frmLinesOnHold.oClass = moClass
    Set frmLinesOnHold.oAppDb = moAppDB
    Set frmLinesOnHold.oProcessPickList = moProcessPickList

    If Not bLinesOnHoldLoaded Then
        Load frmLinesOnHold
        bLinesOnHoldLoaded = True
    End If
    
    'Get the active row
    lRow = glGridGetActiveRow(grdLinePicked)
    
    'Initialize the Distribution object if not already initialized
    CreateDistForm lRow
    
    'Setup the CurrDisplayRow flag in #tsoCreatePickWrk2 so only the selected Kit Components
    'lines will be displayed in the CompItem Form
    sSQL = "UPDATE #tsoCreatePickWrk2"
    sSQL = sSQL & " SET CurrDisplayRow = CASE WHEN LineHold = 1 "
    sSQL = sSQL & " THEN 1 ELSE 0 END"
    
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        Exit Sub
    End If
    
    If Not moItemDist Is Nothing Then
        frmLinesOnHold.LoadLinesOnHold moItemDist, mlTaskID, mbShipmentGenerated, miEmptyBins, miEmptyRandomBins
    End If
    
    If frmLinesOnHold.bIsDirty Then
        'Data changed on LinesOnHold Form
        
        grdLinePicked.redraw = False
        
        moDMLineGrid.Refresh
        'Refresh frame captions for display grids
        SetupRowsSelCaption kSetupCaptionForLineGridOnly
        
        grdLinePicked.redraw = True
        'Set the Line Refreshed Flag to Ture so when user click the line tab
        'the line grid will display the updated data
        mbOrderGridRefreshNeeded = True
    End If
    
    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdLinesOnHold_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub cmdOrderPickLines_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdOrderPickLines, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
 '**********************************************************************
' Description:
'    This routine loads the OrderPickLines form (frmOrderPickLines)
'
'**********************************************************************
On Error GoTo ExpectedErrorRoutine
    
    Dim lRow            As Long
    
    SetMouse
    
    'Get the active row
    lRow = glGridGetActiveRow(grdLinePicked)
    
    'Store the Current cell value in the variable
    mlCurrentOrderActiveRow = glGridGetActiveRow(grdOrderPicked)
    mlCurrentActiveColumn = glGridGetActiveCol(grdOrderPicked)
    mlCurrentRowOrderKey = glGetValidLong(gsGridReadCell(grdOrderPicked, glGridGetActiveRow(grdOrderPicked), kColOrdTranKey))
    
    'Initialize the Distribution object if not already initialized
    CreateDistForm lRow
    
    If Not moItemDist Is Nothing Then
        If Not moProcessPickList.bLoadOrderPickLines(grdOrderPicked, kPickLineMain, _
                    mbShipmentGenerated, miEmptyBins, miEmptyRandomBins) Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgCMUnexpectedSPError, "Load Order Pick Line Form"
            ResetMouse
            Exit Sub
        End If
    End If
        
    grdOrderPicked.redraw = False

    'synchronize #tsoCreatePickHdrWrk to #tsoCreatePickWrk2
    'before refresh the order grid
    SyncDetailToHeader
    
    moDMOrderGrid.Refresh
    grdOrderPicked.redraw = True
    'Set the Line Refreshed Flag to Ture so when user click the line tab
    'the line grid will display the updated data
    mbLineGridRefreshNeeded = True
    
    'Highlight the row where the user has selected before click the Dist button
    If mlCurrentOrderActiveRow > 0 Then
        gGridSetSelectRow grdOrderPicked, mlCurrentOrderActiveRow
    End If
    
    ResetMouse
    
    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdOrderPickLines_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub cmdOrdersOnHold_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdOrdersOnHold, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'**********************************************************************
' Description:
'    This routine loads the OrdersOnHold form (frmOrdersOnHold)
'
'**********************************************************************
    
On Error GoTo ExpectedErrorRoutine
    Dim lRow   As Long
    
    Static bOrderOnHoldLoaded As Boolean

    Set frmOrdersOnHold.oClass = moClass
    Set frmOrdersOnHold.oAppDb = moAppDB
    Set frmOrdersOnHold.oProcessPickList = moProcessPickList

    If Not bOrderOnHoldLoaded Then
        Load frmOrdersOnHold
        bOrderOnHoldLoaded = True
    End If
    
    'Get the active row
    lRow = glGridGetActiveRow(grdLinePicked)
    
    'Initialize the Distribution object if not already initialized
    CreateDistForm lRow
    
    If Not moItemDist Is Nothing Then
        frmOrdersOnHold.LoadOrdersOnHold moItemDist, mlTaskID, mbShipmentGenerated, _
                        miEmptyBins, miEmptyRandomBins
    End If
    
    If frmOrdersOnHold.bIsDirty Then
        'Was some data changed on the Detail form?
        grdOrderPicked.redraw = False
        
        'synchronize #tsoCreatePickHdrWrk to #tsoCreatePickWrk2
        'before refresh the order grid
        SyncDetailToHeader
        
        moDMOrderGrid.Refresh
        'Refresh frame captions for display grids
        SetupRowsSelCaption kSetupCaptionForOrderGridOnly
        
        grdOrderPicked.redraw = True
        
        'Set the Line Refreshed Flag to Ture so when user click the line tab
        'the line grid will display the updated data
        mbLineGridRefreshNeeded = True
    End If
    
    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse

'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdPickAll_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub cmdPickAll_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdPickAll(Index), True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'**********************************************************************
' Description:
'    This routine automatic pick all the Short Pick Lines
'
'**********************************************************************
On Error GoTo ExpectedErrorRoutine
Dim sWhere    As String
Dim sShortPickSelected As String
Dim iUpdateCounterViolation As Integer
    
    If bChkAllRowsSelected Then Exit Sub 'Check all rows are already selected then exit routine
    
    SetMouse
    
    If Not bClearAllRowsPicked Then 'Clear all rows
        ResetMouse
        Exit Sub
    End If

    If OptOrderShow(kShowShortOnly).Value = True Then
    'If only short pick is displayed,  then only clear the short pick ship lines
        sShortPickSelected = " AND ShortPick = 1"
    Else
        sShortPickSelected = ""
    End If
    
    'Build where clause for the lines need to be picked
    sWhere = " ShipLineKey IN (SELECT ShipLineKey FROM #tsoCreatePickWrk2 WHERE LineHold = 0 AND CrHold = 0 AND OrderHold = 0" & sShortPickSelected & ")"
   
    'Call bCreateAutoDistribution to create distribution for the selected Shiplines
    If Not moProcessPickList.bCreateAutoDistribution(sWhere, miEmptyBins, miEmptyRandomBins, iUpdateCounterViolation) Then
        ResetMouse
        Exit Sub
    End If
    
    If Err.Number <> 0 Then
        gClearSotaErr
    End If
    On Error GoTo VBRigErrorRoutine
    
    'Refresh frame captions for display grids
    If Index = kOrder Then
        grdOrderPicked.redraw = False
        moDMOrderGrid.Refresh
        grdOrderPicked.redraw = True
        
        'SetupRowsSelCaption kSetupCaptionForOrderGridOnly
        
        'Set the Line Refreshed Flag to Ture so when user click the line tab
        'the line grid will display the updated data
        mbLineGridRefreshNeeded = True
    Else
        grdLinePicked.redraw = False
        moDMLineGrid.Refresh
        grdLinePicked.redraw = True
        'SetupRowsSelCaption kSetupCaptionForLineGridOnly
        
        'Set the Order Refreshed Flag to Ture so when user click the order tab
        'the order grid will display the updated data
        mbOrderGridRefreshNeeded = True
    End If

    ResetMouse
    
    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    
    Exit Sub
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "cmdPickAll_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub HidePicked(ByVal iCallGrid As Integer, Optional ByVal lRow As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'**********************************************************************
' Description:
'    This routine hides all the non short pick lines when the Show Only
'    short pick checkbox is checked.
'
'**********************************************************************
    Dim iShortPick    As Integer
    Dim lRowCount   As Long

    If IsMissing(lRow) Then
    'No specific row has been passed in, so we will hide all the
    'non-Short pick rows
        If iCallGrid = kLine Then
        'Hide the non-short pick rows in the Linepicked grid
            For lRowCount = 1 To grdLinePicked.DataRowCnt
                iShortPick = giGetValidInt(gsGridReadCell(grdLinePicked, lRowCount, kColLineShortPickFlag))
                If iShortPick = 0 Then
                    With grdLinePicked
                        .Row = lRowCount
                        .RowHidden = True
                    End With
                End If
            Next lRowCount
        Else
        'Hide the non-short pick rows in the Orderpicked grid
            For lRowCount = 1 To grdOrderPicked.DataRowCnt
                iShortPick = giGetValidInt(gsGridReadCell(grdOrderPicked, lRowCount, kColOrdShortPickFlag))
                If iShortPick = 0 Then
                    With grdOrderPicked
                        .Row = lRowCount
                        .RowHidden = True
                    End With
                End If
            Next lRowCount
        End If
    Else
    'A specific row has been passed in, so we will hide that row
    'if it is short picked
        lRowCount = glGetValidLong(lRow)
        If iCallGrid = kLine Then
        'Hide the row in the Linepicked grid if it is not short picked
            If lRow > 0 Then
                iShortPick = giGetValidInt(gsGridReadCell(grdLinePicked, lRowCount, kColLineShortPickFlag))
                If iShortPick = 0 Then
                    With grdLinePicked
                        .Row = lRowCount
                        .RowHidden = True
                    End With
                End If
            End If
        Else
        'Hide the row in the Orderpicked grid if it is not short picked
            If lRow > 0 Then
                iShortPick = giGetValidInt(gsGridReadCell(grdOrderPicked, lRowCount, kColOrdShortPickFlag))
                If iShortPick = 0 Then
                    With grdOrderPicked
                        .Row = lRowCount
                        .RowHidden = True
                    End With
                End If
            End If
        End If
    End If
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HideUnpicked", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub ShowPicked(ByVal iCallGrid As Integer, Optional ByVal lRow As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**********************************************************************
' Description:
'    This routine unhides all the non short pick lines when the Show
'    all picks checkbox is checked.
'
'**********************************************************************
    Dim lRowCount        As Long

    If IsMissing(lRow) Then
    'No specific row has been passed in, so we will show all the
    'pick rows
        If iCallGrid = kLine Then
            For lRowCount = 1 To grdLinePicked.DataRowCnt
                With grdLinePicked
                    .Row = lRowCount
                    .RowHidden = False
                End With
            Next lRowCount
        Else
        'Show all pick rows in the Orderpicked grid
            For lRowCount = 1 To grdOrderPicked.DataRowCnt
                With grdOrderPicked
                    .Row = lRowCount
                    .RowHidden = False
                End With
            Next lRowCount
        End If
    Else
    'A specific row has been passed in, so we will show that pick row
        If iCallGrid = kLine Then
        'Show the pick row in the Linepicked grid
            With grdLinePicked
                .Row = lRow
                .RowHidden = False
            End With
        Else
        'Show the pick row in the Orderpicked grid
            With grdOrderPicked
                .Row = lRowCount
                .RowHidden = False
            End With
        End If
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ShowUnpicked", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub CreateDistForm(ByVal lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**********************************************************************
' Description:
'    This routine creates the Distribution Objects if the object is not
'    alreay created.
'
'**********************************************************************
Dim lWhseKey    As Long

    'This sub was required since the Distribution form requires a warehousekey, we don't have warehouse key until an item is chosen
    If moItemDist Is Nothing Then
        '-- Setup Item Distribution object
       Set moItemDist = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
"imzde001.clsimzde001", 117833828, kAOFRunFlags, kContextAOF)

        lWhseKey = glGetValidLong(moDMLineGrid.GetColumnValue(lRow, "ShipWhseKey"))

        If Not moItemDist Is Nothing Then
            If Not moItemDist.InitDistribution(moAppDB, moAppDB, lWhseKey) Then
                giSotaMsgBox Me, moSysSession, kmsgErrInitItemDist
                Exit Sub
            End If
        End If
    End If
    
    moProcessPickList.oItemDist = moItemDist

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CreateDistForm", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub SetRowValues(ByVal lRow As Long, Optional bDataRefresh As Boolean = False)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'**********************************************************************
' Description:
'    This routine formats the line grid rows based on the ItemType
'    UOM options, etc.
'
'**********************************************************************

Dim iAllowSubItem       As Integer
Dim iTrackMeth          As Integer
Dim iTrackQtyAtBin      As Integer
Dim dQty                As Double
Dim lKitShipLineKey     As Long
Dim lSubItemKey         As Long
Dim sSubItemID          As String
Dim lWhseKey            As Long
Dim lUOMKey             As Long
Dim lItemKey            As Long
Dim iItemType           As Integer
Dim iAllowDecimalQty    As Integer
Dim sSQL                As String
Dim iOrderChecked       As Integer
Dim iLineChecked        As Integer
Dim dDMQtyPicked        As Double
Dim dGridQtyPicked      As Double
Dim lOrderTranKey       As Long
Dim lOrderTranType      As Long
Dim lLineTranKey        As Long
Dim lLineTranType       As Long
Dim lRowCount           As Long
Dim iShipmentCommitStatus As Integer
Dim iExtShipmentExists As Integer
Dim iPacked             As Integer
Dim iWhseUseBin         As Integer

    iAllowSubItem = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineAllowSubItem))
    iTrackMeth = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineTrackMeth))
    iTrackQtyAtBin = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineTrackQtyAtBin))
    iWhseUseBin = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineWhseUseBin))
    dQty = gdGetValidDbl(gsGridReadCell(grdLinePicked, lRow, kColLineQtyToPick))
    lKitShipLineKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineKitShipLineKey))
    sSubItemID = gsGetValidStr(gsGridReadCell(grdLinePicked, lRow, kColLineSubItem))
    lSubItemKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineSubItemKey))
    lWhseKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineShipWhseKey))
    lItemKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineSubItemKey))
    iItemType = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineItemType))
    iAllowDecimalQty = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineAllowDecimalQty))
    iShipmentCommitStatus = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineShipmentCommitStatus))
    iExtShipmentExists = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineExtShipmentExists))
    iPacked = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLinePacked))
    
    If iShipmentCommitStatus = 1 Or iExtShipmentExists = 1 Or iPacked = 1 Or miSecurityLevel = kSecLevelDisplayOnly Then
        gGridLockRow grdLinePicked, lRow
    Else
        If lItemKey = 0 Then
            lItemKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineItemKey))
        Else
            iTrackMeth = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineTrackMeth))
        End If

        lUOMKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineShipUOMKey))

        If iAllowSubItem = 0 Or dQty <= 0 Or lKitShipLineKey > 0 Then
            gGridLockCell grdLinePicked, kColLineSubItem, lRow
        Else
            gGridUnlockCell grdLinePicked, kColLineSubItem, lRow
        End If

        'UOM changes are not allowed for:
        'Serialized Items            Kit Component Items
        'Substituted Items           Items not allowing Deciaml Qtys
        If iAllowDecimalQty = 0 Or (iTrackMeth > 1) Or (lKitShipLineKey > 0) Or _
            (iItemType < 5) Or iItemType = IMS_BTO_KIT Then
            'Or (Len(Trim(sSubItemID)) > 0) Or (iAllowDecimalQty = 0) Then
            gGridLockCell grdLinePicked, kColLineShipUOM, lRow
        Else
            gGridUnlockCell grdLinePicked, kColLineShipUOM, lRow
        End If

        'Enable the auto-dist bin column if it can auto distributed from the bin lookup.
        'Lock it first by default.
        gGridLockCell grdLinePicked, kColLineAutoDistBinID, lRow
        'Unlock it if the conditions are correct.
        If iItemType = kFinishedGood Or iItemType = kPreAssembledKit Or iItemType = kRawMaterial Then
            If iTrackQtyAtBin = 1 And (iTrackMeth = kTM_None Or iTrackMeth = kTM_Lot) And mbIMIntegrated Then
                gGridUnlockCell grdLinePicked, kColLineAutoDistBinID, lRow
            End If
        End If

        'Setup the distribution button for item needed to be distributed.
        'Skip the following code when the grid is being refreshed since this
        'does not applied to a group of lines
        If Not bDataRefresh Then
            If iItemType > 4 And mbIMIntegrated Then
                If (iTrackMeth > 0 Or iTrackQtyAtBin = 1) Then
                    If iItemType = IMS_BTO_KIT Then
                        cmdComponents.Enabled = True
                        cmdDist.Enabled = False
                    Else
                        cmdDist.Enabled = True
                        cmdComponents.Enabled = False
                    End If
                Else
                    If iItemType = IMS_BTO_KIT Then
                        cmdComponents.Enabled = True
                    Else
                        cmdComponents.Enabled = False
                    End If
                    cmdDist.Enabled = False
                End If
            Else
                cmdComponents.Enabled = False
                cmdDist.Enabled = False
            End If
        End If
    End If
       
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetRowValues", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


Private Sub SetMouse()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'    Desc: Sets Mouse to a Busy Value.
'   Parms: N/A
' Returns: N/A
'***********************************************************************
    If Me.MousePointer <> vbHourglass Then
        Me.MousePointer = vbHourglass
    End If
  
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetMouse", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub ResetMouse()
'+++ VB/Rig Skip +++
'***********************************************************************
'    Desc: Resets Mouse to Previous Value.
'   Parms: N/A
' Returns: N/A
' Mod: rs 10/10  Remove errorhandling in reset mouse routine or it will clear
'               errors when called from expected error routines.
'***********************************************************************
    Me.MousePointer = miMousePointer

End Sub

Private Sub SetupRowsSelCaption(iSetupOption As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
' Description:
'    This routine updates the select grids's frame caption for
'    the current row count in the grid
'
'    Parameters:
'           iSetupOption <in>
'                - 0 (kSetupCaptionForOrderGridOnly) update caption for
'                    the grid frame for Orderpicked grid
'                - 1 (kSetupCaptionForLineGridOnly) update caption for
'                    the grid frame for Linepicked grid
'                - 2 (kSetupCaptionForBothGrid) update captions for
'                    the grid frames for Orderpicked grid and Linepicked
'                    grid
'
'********************************************************************
Dim lOrdNbrSelected     As Long
Dim lLineNbrSelected    As Long
Dim lOrdersOnHoldCount  As Long
Dim lLinesOnHoldCount   As Long
Dim lLineNbrSelectedDisplay    As Long
Dim lOrdNbrSelectedDisplay    As Long

lOrdNbrSelected = 0
lLineNbrSelected = 0
lOrdersOnHoldCount = 0
lLinesOnHoldCount = 0

    'Get the order on hold count
    lOrdersOnHoldCount = glGetValidLong(moAppDB.Lookup("Count(*)", "#tsoCreatePickHdrWrk", "Hold = 1 OR CrHold = 1"))
    'Enable the OrderOnHold button if there are orders on hold
    If lOrdersOnHoldCount > 0 Then
        cmdOrdersOnHold.Enabled = True
    'Otherwise disable the button
    Else
        cmdOrdersOnHold.Enabled = False
    End If
    
    'Get the lines on hold count
    lLinesOnHoldCount = glGetValidLong(moAppDB.Lookup("Count(*)", "#tsoCreatePickWrk2", "LineHold = 1"))
    'Enable the LinesOnHold button if there are lines on hold
    If lLinesOnHoldCount > 0 Then
        cmdLinesOnHold.Enabled = True
    Else
    'Otherwise disable the button
        cmdLinesOnHold.Enabled = False
    End If
    
    'Now check the selected pick line count in the grids
    'First, get the count for all the lines that is not on hold
    lOrdNbrSelected = glGetValidLong(moAppDB.Lookup("Count(*)", "#tsoCreatePickHdrWrk", "Hold = 0 AND CrHold = 0"))
    lLineNbrSelected = glGetValidLong(moAppDB.Lookup("Count(*)", "#tsoCreatePickWrk2", "KitShipLineKey IS NULL AND OrderHold = 0 AND CrHold = 0 AND LineHold = 0"))
    
    'Now, according to the ShowAll and ShowShortOnly option recalculate the display row count
    If OptOrderShow(kShowAll).Value = True Then
        lOrdNbrSelectedDisplay = lOrdNbrSelected
        lLineNbrSelectedDisplay = lLineNbrSelected
    Else
        lOrdNbrSelectedDisplay = glGetValidLong(moAppDB.Lookup("Count(*)", "#tsoCreatePickHdrWrk", "Hold = 0 AND CrHold = 0 AND ShortPick = 1"))
        lLineNbrSelectedDisplay = glGetValidLong(moAppDB.Lookup("Count(*)", "#tsoCreatePickWrk2", "KitShipLineKey IS NULL AND LineHold = 0 AND ShortPick = 1"))
    End If
   
    If lOrdNbrSelectedDisplay <= 1 Then
        fraOrderPicked.Caption = lOrdNbrSelectedDisplay & " " & msOrderToPick
    Else
        fraOrderPicked.Caption = lOrdNbrSelectedDisplay & " " & msOrdersToPick
    End If

    If lLineNbrSelectedDisplay <= 1 Then
        fraLinePicked.Caption = lLineNbrSelectedDisplay & " " & msLineToPick
    Else
        fraLinePicked.Caption = lLineNbrSelectedDisplay & " " & msLinesToPick
    End If
    
    If lOrdNbrSelected = 0 Or lLineNbrSelected = 0 Then
    'If lines picked, disable the Print, Preview in the toolbar
        tbrMain.ButtonEnabled(kTbPrint) = False
        tbrMain.ButtonEnabled(kTbPreview) = False
        If miCallingTask = kReprintPickList Then
            tbrMain.ButtonEnabled(kTbDefer) = False
        End If
        'tbrMain.ButtonEnabled(kTbFinish) = False
        'tbrMain.ButtonEnabled(kTbFinishExit) = False
        cmdDist.Enabled = False
        cmdComponents.Enabled = False
        cmdOrderPickLines.Enabled = False
    Else
    'There are lines picked, enable the Print, Preview, Finish, and finishExit button in the toolbar
        tbrMain.ButtonEnabled(kTbPrint) = True
        tbrMain.ButtonEnabled(kTbPreview) = True
        If miCallingTask = kReprintPickList Then
            tbrMain.ButtonEnabled(kTbDefer) = True
        End If
        tbrMain.ButtonEnabled(kTbFinish) = True
        tbrMain.ButtonEnabled(kTbFinishExit) = True
        cmdOrderPickLines.Enabled = True
    End If
    
    'If the grd is empty, disable all the controls in the form other than select order button
    If lOrdNbrSelected + lLineNbrSelected + lOrdersOnHoldCount + lLinesOnHoldCount = 0 Then
        
        cmdSelectOrders.Enabled = True
        
        fraShow(kOrder).Enabled = False
        fraShow(kLine).Enabled = False
        OptOrderShow(kShowAll).Enabled = False
        OptOrderShow(kShowShortOnly).Enabled = False
        OptLineShow(kShowAll).Enabled = False
        OptLineShow(kShowShortOnly).Enabled = False
        
        cmdClearAll(kOrder).Enabled = False
        cmdPickAll(kOrder).Enabled = False
        cmdClearAll(kLine).Enabled = False
        cmdPickAll(kLine).Enabled = False
        cmdDeleteAll(kOrder).Enabled = False
        cmdDeleteAll(kLine).Enabled = False
        
        'delete the pick list when all the ship line in the pick list is deleted
        If mlPickListKey > 0 Then
            If Not moProcessPickList.bDeletePickList(mlPickListKey) Then
               giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation  'kmsgimspGeneralReserveInvtTran
            Else
            'Remove the logical lock
                 bRemoveLogicalLock
            End If
            'clear the controls
            mlPickListKey = 0
            currPickListNo.Caption = ""
        End If
        
        'Default the display table to order picked tab
        grdOrderPicked.redraw = False
        grdLinePicked.redraw = False
        moDMLineGrid.Refresh
        moDMOrderGrid.Refresh
        grdOrderPicked.redraw = True
        grdLinePicked.redraw = True
        
        tabPickOrder.Tab = kTabOrder
    Else
        cmdSelectOrders.Enabled = False
        tabPickOrder.Enabled = True
        
        fraShow(kOrder).Enabled = True
        fraShow(kLine).Enabled = True
        OptOrderShow(kShowAll).Enabled = True
        OptOrderShow(kShowShortOnly).Enabled = True
        OptLineShow(kShowAll).Enabled = True
        OptLineShow(kShowShortOnly).Enabled = True
        
        If Not mbShipmentGenerated Then
            cmdClearAll(kOrder).Enabled = True
            cmdPickAll(kOrder).Enabled = True
            cmdDeleteAll(kOrder).Enabled = True
            cmdClearAll(kLine).Enabled = True
            cmdPickAll(kLine).Enabled = True
            cmdDeleteAll(kLine).Enabled = True
        Else
            cmdClearAll(kOrder).Enabled = False
            cmdPickAll(kOrder).Enabled = False
            cmdDeleteAll(kOrder).Enabled = False
            cmdClearAll(kLine).Enabled = False
            cmdPickAll(kLine).Enabled = False
            cmdDeleteAll(kLine).Enabled = False
        End If
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, "frmSelectPick", "SetupRowsSelCaption", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function bInitWorkTables() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**********************************************************************
' Description:
'    This routine creates all the temp table which were used in the later
'    process to avoid unnecessary recompilation in store procedures.
'
'**********************************************************************
    Dim oSOTmpTbls As New clsSOTempTable

    bInitWorkTables = False
    
    If Not oSOTmpTbls Is Nothing Then
        oSOTmpTbls.gbCreateTmpTblSOCreatePickList moAppDB
        oSOTmpTbls.gbCreateTmpTblSOGenShipment moAppDB
        oSOTmpTbls.gbCreateTmpTblSOPrintPickList moAppDB
    End If

    If Err.Number <> 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kSOmsgPickListWrkTable  'inform user that there are problems
        Exit Function
    End If
        
    Set oSOTmpTbls = Nothing
    
    bInitWorkTables = True

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        If Not oSOTmpTbls Is Nothing Then
            Set oSOTmpTbls = Nothing
        End If
        gSetSotaErr Err, sMyName, "bInitWorkTables", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Sub cmdSelectOrders_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdSelectOrders, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'**********************************************************************
' Description:
'    This routine calls the SelectOrder DLL to Create Pick Lines
'
'**********************************************************************
On Error GoTo ExpectedErrorRoutine
    
    Dim oSelectPick As Object
    
    Me.Enabled = False
        
    'Initial the select order object
    Set oSelectPick = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                            kclsSelectOrdToPick, ktskSelectOrdToPick, kAOFRunFlags, kContextAOF)
    
    
    'Select the pick lines
    If Not oSelectPick Is Nothing Then
    
        'Set up properties
        oSelectPick.bDisplayUI = True
        oSelectPick.oAppDb = moAppDB
        
        'Populate pick option temp table
        GetPickListOption
       
        'Load the object
        oSelectPick.SelectOrders
        
        'Get the necessary key and options for future deletion
        'and auto pick processing in the main form
        mlPickListKey = oSelectPick.lPickListKey
        miEmptyBins = oSelectPick.iEmptyBins
        miEmptyRandomBins = oSelectPick.iEmptyRandomBins
        mlLogicalLockKey = glGetValidLong(oSelectPick.lLogicalLockKey)
        miPickOrdQty = oSelectPick.iPickOrdQty
        
        moProcessPickList.iPickQtyOrd = miPickOrdQty
    End If
    
    If mlPickListKey > 0 Then
        If bPerformPickListKeyChange(mlPickListKey) Then
            currPickListNo.Caption = msPickListNo
        End If
    End If
    
    'Refresh the Grid in the main form
    grdOrderPicked.redraw = False
    grdLinePicked.redraw = False
    moDMOrderGrid.Refresh
    moDMLineGrid.Refresh
    
    'Refresh frame captions for display grids
    SetupRowsSelCaption kSetupCaptionForBothGrid
    
    grdOrderPicked.redraw = True
    grdLinePicked.redraw = True
   
    If Not oSelectPick Is Nothing Then
        Set oSelectPick = Nothing
    End If
    
    ResetMouse
    
    Me.Enabled = True
    
    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub


Private Sub MapControls()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
'   Description:
'       MapControls will map the controls on the form to the buttons
'       that have Function key equivalents.  The general procedure
'       giCollectionAdd is used to add the button control as
'       an object Item to the collection and use the mapped
'       entry control's window handle as the key into the collection.
'*********************************************************************
    Set moMapSrch = New Collection
    'giCollectionAdd moMapSrch, navControl, mskControl(kSalesOrder).hwnd
    'giCollectionAdd moMapSrch, navControlTrnsfr, mskControl(kTransfer).hwnd
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MapControls", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True process predefined function keys.
'*********************************************************************
    Select Case keycode 'normal function key processing
        Case vbKeyF1 To vbKeyF16              'Function Keys pressed
            gProcessFKeys Me, keycode, Shift  'Run Sage MAS 500 Function Key Processing
    End Select
    

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub


Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyPress traps the keys pressed when the KeyPreview
'         property of the form is set to True process the enter key
'         as if the user pressed the tab key.
'********************************************************************

    Select Case KeyAscii
        Case vbKeyReturn            'User pressed enter key
            If mbEnterAsTab Then    'enter as tab set in session
                DoEvents
                SendKeys "{Tab}"    'send tab key
                KeyAscii = 0        'cancel enter key
            End If
        Case Else
            'Other KeyAscii routines.
    End Select

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'default to Form_Load error

    mbLoadSuccess = False
    mbLoading = True
    
    Set moAppDB = moClass.moAppDB
    
    'The server process ID is used when deselecting or clearing the grid so only those rows
    'in the locking table associated with this user's session are deleted.  Prevents the following
    'situation from occuring:
    'User 1 selects SO#1
    'User 2 selects ALL SO's.  SO#1 will be excluded.  User 2 then clears their selection.  Since the initial selection
    'included SO#1, the locking row for User 1's SO#1 will get deleted unless it is restricted to the SPID for this
    'session, making it possible for SO#1 to be subsequently selected and picked by another user.
    miSPID = moAppDB.Lookup("spid", "master..sysprocesses", "spid = @@spid")
            
    Set moSysSession = moClass.moSysSession
    With moSysSession
        msCompanyID = .CompanyId
        msUserID = .UserId
        mbEnterAsTab = .EnterAsTab
        mlLanguage = .Language
        msHomeCurrID = .CurrencyID
    End With
    
   
    '-- Set up the form resizing variables
    With Me
        miMinFormHeight = .Height
        miMinFormWidth = .Width
        miOldFormHeight = .Height
        miOldFormWidth = .Width
    End With
    
    'Get caller TaskID
    mlTaskID = moClass.moFramework.GetTaskID()
    
    'Setup form according to the calling task
    If mlTaskID = ktskCreatePick Then
            
        'Setup Form Caption
        Me.Caption = gsBuildString(kSOCreatePickList, moAppDB, moSysSession)
        miCallingTask = kCreatePickList
        
        'Enable controls that apply to this task
        cmdSelectOrders.Visible = True
        cmdOrdersOnHold.Visible = True
        cmdLinesOnHold.Visible = True
        'lblPickListNo.Visible = False
        lkuPickListNo.Visible = False
        currPickListNo.Visible = True
        
        'Justifed control position
        'lblSetting.Left = 210
        'cboSetting.Left = 870
        'chkPrintRptSetting.Left = 4650
    Else
        If mlTaskID = ktskReprintPickList Then
            'Setup Form Caption
            Me.Caption = "Reprint Pick List" 'gsBuildString(kSOCreatePickList, moAppDB, moSysSession)
            miCallingTask = kReprintPickList
            
            'Disable controls that do not apply to this task
            cmdSelectOrders.Visible = False
            cmdOrdersOnHold.Visible = False
            cmdLinesOnHold.Visible = False
            cmdDeleteAll(kOrder).Visible = False
            cmdDeleteAll(kLine).Visible = False
            fraShipping.Visible = False
            currPickListNo.Visible = False
            lblSetting.Visible = False
            cboSetting.Visible = False
            
            'Justifed control position
            chkPrintRptSetting.Left = 3800
            fraFormat.Height = 2295
            fraOrderPicked.Top = 120
            fraOrderPicked.Height = 5835
            grdOrderPicked.Height = 5010
            fraLinePicked.Top = 120
            fraLinePicked.Height = 5835
            grdLinePicked.Height = 5010
            
            CleanupLogicalLocks
            
            With lkuPickListNo
            Set .Framework = moClass.moFramework
            Set .SysDB = moAppDB
            Set .AppDatabase = moAppDB
                .RestrictClause = "tsoPickList.CompanyID = " & gsQuoted(msCompanyID) & _
                        " AND tsoPickList.PickListKey IN (SELECT PickListKey FROM tsoShipLine WITH (NOLOCK) WHERE PickingComplete = 1)"
            End With
        End If
    End If
    
    'Set up security level
    miSecurityLevel = giSetAppSecurity(moClass, tbrMain, moDMOrderGrid, _
                        moDMLineGrid)
                        
    If CBool(moClass.moSysSession.IsModuleActivated(kModuleWM)) = True Then
        mbWMIsLicensed = moClass.moSysSession.IsModuleLicensed(kModuleWM)
    Else
        mbWMIsLicensed = False
    End If
    

    'Create Work Tables In TempDB
    If Not bInitWorkTables Then
        'Can't initialize Work Tables, so exit
        Exit Sub
    End If
    
    'Get whatever Localized Strings are needed.
    If Not bGetLocalStrings() Then

        'An error occurred while retrieving text for messages used in this function.
        ResetMouse
        giSotaMsgBox Me, moClass.moSysSession, kmsgGetLocalStringsError
        Exit Sub
    End If
  
    'Initialize the ProcessPickList Object
    If moProcessPickList Is Nothing Then
        Set moProcessPickList = New clsProcessPickList
        moProcessPickList.oClass = moClass
        moProcessPickList.oAppDb = moAppDB
        moProcessPickList.sCompanyID = msCompanyID
        moProcessPickList.lTaskID = mlTaskID
        moProcessPickList.iSecurityLevel = miSecurityLevel
    End If
    
    'Get Module Options
    GetModuleOptions
  
    'Setup Grids for the Orders Picked and Lines Picked
    SetupOrderGrid
    SetupLineGrid
    
    'Bind Grids
    BindOrderGrid
    BindLineGrid
    BindGM
    BindPickListHeader
    
    'Setup Context Menu
    BindContextMenu
   
    'Setup Toolbar
    SetupBars
        
    'Setup Report
    SetupSortCombos
    SetupReports
    
    'map controls for function keys
    MapControls
    
    'clear form
    ClearForm
    
    
    SetTags
    
    'Default to the Order Tab
    tabPickOrder.Tab = kTabOrder
    'tabPickOrder.Enabled = False

#If CUSTOMIZER Then
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
            tabPickOrder.TabVisible(kTabCustomizer) = False
            tabPickOrder.TabsPerRow = tabPickOrder.TabsPerRow - 1
            
            moFormCust.Initialize Me, goClass, tabPickOrder.Name & ";" & kTabCustomizer
            Set moFormCust.CustToolbarMgr = tbrMain
            moFormCust.ApplyFormCust
        End If
    End If
#End If
    
    'Form_Load completed successfully
    sbrMain.Status = SOTA_SB_START
    mbLoading = False
    mbLoadSuccess = True
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub SetupOptions()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim i As Integer

    Set moOptions = New clsOptions
    With moOptions
        For i = 0 To chkInclude.UBound
            .Add chkInclude(i)
        Next i
        
        For i = 0 To chkIncludeInDetail.UBound
            .Add chkIncludeInDetail(i)
        Next i
        
        For i = 0 To optFormat.UBound
            .Add optFormat(i)
        Next i
        
        .Add cboBinSort
        .Add cboZoneSort
        .Add chkWaveSummary
        
        For i = 0 To OptOrderShow.UBound
            .Add OptOrderShow(i)
        Next i
        
        For i = 0 To OptLineShow.UBound
            .Add OptLineShow(i)
        Next i
        
        .Add chkPrintRptSetting
        
        If miCallingTask = kCreatePickList Then
            .Add chkGenShip
        End If
    End With
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupOptions", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++End Sub
End Sub

Private Function bChkAllRowsSelected() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim lRowCount   As Long
'**********************************************************************
' Description:
'    bChkAllRowsSelected return whether any row in the grid is unchecked.
'**********************************************************************
bChkAllRowsSelected = False

    For lRowCount = 1 To grdOrderPicked.DataRowCnt
        If giGetValidInt(gsGridReadCell(grdOrderPicked, lRowCount, kColOrdPickCheck)) = 0 Then Exit Function  'Check whether the row is selected
    Next

bChkAllRowsSelected = True
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bChkAllRowsSelected", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Function bClearAllRowsPicked() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sWhere As String
    Dim sShortPickSelected As String
    Dim iUpdateCounterViolation As Integer
'**********************************************************************
' Description:
'    bClearAllRowsPicked delete the rows already picked.
'**********************************************************************
    bClearAllRowsPicked = False
    'If only short pick is displayed,  then only clear the short pick ship lines
    If OptOrderShow(kShowShortOnly).Value = True Then
        If tabPickOrder.Tab = kTabOrder Then
            sShortPickSelected = " AND OrderKey IN (SELECT DISTINCT OrderKey FROM #tsoCreatePickWrk2 WHERE ShortPick = 1)"
        Else
            sShortPickSelected = " AND ShortPick = 1"
        End If
    Else
        sShortPickSelected = ""
    End If

    'Build the Where clause for the lines that need to delete distribution
    sWhere = " wrk.ShipLineKey IN (SELECT ShipLineKey FROM #tsoCreatePickWrk2 WHERE LineHold <> 1 AND CrHold <> 1 AND OrderHold <> 1" & sShortPickSelected & ")"
    
    'Call DeleteShipLines to delete the distribution for the select lines.
    bClearAllRowsPicked = moProcessPickList.bDeleteShipLines(sWhere, kDeleteDistributionOnlyYes, iUpdateCounterViolation)
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bClearAllRowsPicked", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Function bGetLocalStrings() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**********************************************************************
' Description:
'    This routine gets all the local strings needed for the process
'
'**********************************************************************
    
    bGetLocalStrings = False

    msLineToPick = gsBuildString(kSOLineSelForPick, moAppDB, moSysSession)
    msLinesToPick = gsBuildString(kSOLinesSelForPick, moAppDB, moSysSession)
    msOrderToPick = gsBuildString(kSOOrderToPick, moAppDB, moSysSession)
    msOrdersToPick = gsBuildString(kSOOrdersToPick, moAppDB, moSysSession)
    msLineOnHold = gsBuildString(kSOLineOnHold, moAppDB, moSysSession)
    msLinesOnHold = gsBuildString(kSOLinesOnHold, moAppDB, moSysSession)
    msOrderOnHold = gsBuildString(kSOOrderOnHold, moAppDB, moSysSession)
    msOrdersOnHold = gsBuildString(kSOOrdersOnHold, moAppDB, moSysSession)

    msOrderNumber = gsBuildString(kSOOrderNumber, moAppDB, moSysSession)
    msOrderDate = gsBuildString(kSOOrderDate, moAppDB, moSysSession)
    msExpireDate = gsBuildString(kDD321167ExpireDate, moAppDB, moSysSession)
    msEarliestShip = gsBuildString(kSOEarliestOrderShipDate, moAppDB, moSysSession)
    msShipFrom = gsBuildString(kSOShipFrom, moAppDB, moSysSession)
    msManualHold = gsBuildString(kSOManualHold, moAppDB, moSysSession)
    msCreditHold = gsBuildString(kSOCreditHold, moAppDB, moSysSession)
    msHoldReason = gsBuildString(kSOHoldRsn, moAppDB, moSysSession)

    msPick = gsBuildString(kSOPick, moAppDB, moSysSession)
    msItemOrdered = gsBuildString(kSOItemOrder, moAppDB, moSysSession)
    msShipTo = gsBuildString(kSOShipTo, moAppDB, moSysSession)
    msTranType = gsBuildString(kTypeCol, moAppDB, moSysSession)
    msQtyToPick = gsBuildString(kSOQtyToPick, moAppDB, moSysSession)
    msShort = gsBuildString(kSOShort, moAppDB, moSysSession)
    msAvailToShip = gsBuildString(kSOAvailToShip, moAppDB, moSysSession)
    msUOM = gsBuildString(kSOUOM, moAppDB, moSysSession)
    msStat = gsBuildString(kSOStatusAbbrev, moAppDB, moSysSession)
    msSub = gsBuildString(kSOSub, moAppDB, moSysSession)
    msPriority = gsBuildString(kSOShpPriority, moAppDB, moSysSession)
    msShipDate = gsBuildString(kSOShipDate, moAppDB, moSysSession)
    msSubItem = gsBuildString(kSOSRegSubItem, moAppDB, moSysSession)
    msDelivery = gsBuildString(kSODelivery, moAppDB, moSysSession)
    msShipVia = gsBuildString(kSOShipvia, moAppDB, moSysSession)
    msOrderNo = gsBuildString(kSOOrderNo, moAppDB, moSysSession)
    msLineNo = gsBuildString(kLineNoCol, moAppDB, moSysSession)
    msWhse = gsBuildString(kSOWhse, moAppDB, moSysSession)
    msShipToName = gsBuildString(kSOShipToName, moAppDB, moSysSession)
    msShipToAddress = gsBuildString(kSOShipToAddress1, moAppDB, moSysSession)
    msDescription = gsBuildString(kSODescription, moAppDB, moSysSession)
    msStockAllocSeq = gsBuildString(kSOStockAllocSeq, moAppDB, moSysSession)
    msBin = gsBuildString(kSOBin, moAppDB, moSysSession)
    msQtyPicked = gsBuildString(kSOQtyPicked, moAppDB, moSysSession)
    msOrdUOM = gsBuildString(kSOOrdUOM, moAppDB, moSysSession)
    msShipUOM = gsBuildString(kSOShipUOM, moAppDB, moSysSession)
    msQtyToShip = gsBuildString(kSOQtyToShip, moAppDB, moSysSession)
    msQtyOpen = gsBuildString(kSOQtyOpen, moAppDB, moSysSession)
    
    bGetLocalStrings = True

    'Exit this function
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bGetLocalStrings", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Private Function bInitSort(iRptType As Integer) As Boolean
'NOTE: A custom bInitSort exists here for two reasons: one, the work table collection needs to
'be repopulated so that the primary work table can be changed; two, so that when the report format
'is changed, sorts that are valid for the new format can "stay" in the sort grid (eliminating the need
'for the user to start over).

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim sStr As String
    Dim i As Integer
    
    bInitSort = True
    
'    'Remove all the exist data in sWorkTableCollection
'    For i = sWorkTableCollection.Count To 1 Step -1
'        sWorkTableCollection.Remove i
'    Next i
'
'    'Add data to sWorkTableCollection based on the report format
'    With sWorkTableCollection
'        Select Case iRptType
'            Case kPickRptTypeSummary
'                .Add "tsoPickListHdrWrk"
'                .Add "tsoPickListDistWrk"
'                .Add "tciAddressWrk"
'            Case kPickRptTypeDetail
'                .Add "tsoPickListHdrWrk"
'                .Add "tsoPickListDistWrk"
'                .Add "tciAddressWrk"
'                .Add "tsoPickListCompListWrk"
'            Case kPickRptTypeOrder
'                .Add "tsoPickListHdrWrk"
'                .Add "tsoPickListDistWrk"
'                .Add "tciAddressWrk"
'                .Add "tsoPickListCompListWrk"
'        End Select
'    End With
    
    If Not moSort Is Nothing Then
        moSort.CleanSortGrid
    Else
        Set moSort = New clsSort
    End If
    
    With moSort
        If iRptType = kPickRptTypeOrder Then
            If (.lInitSort(Me, moDDData, 1, 0, True) = kFailure) Then
                bInitSort = False
                Exit Function
            End If
        End If
        If iRptType = kPickRptTypeDetail Then
            If (.lInitSort(Me, moDDData, 0, 5, True) = kFailure) Then
                bInitSort = False
                Exit Function
            End If
        End If
        If iRptType = kPickRptTypeSummary Then
            If (.lInitSort(Me, moDDData, 0, 0, True) = kFailure) Then
                bInitSort = False
                Exit Function
            End If
        End If
        
        'Insert sorts used by various formats
        If (iRptType = kPickRptTypeOrder) Then
            .lInsSort "ItemID", "timItem", moDDData, "ItemID" 'gsBuildString(kSOSoItem, oSysDB, oSysSession)
            .lInsSort "SOLineNo", "tsoSOLine", moDDData, "Order Line No"
        Else
            .lDeleteSort "ItemID", "timItem"
            .lDeleteSort "SOLineNo", "tsoSOLine"
        End If
        If (iRptType = kPickRptTypeDetail) Then
            .lInsSort "OrderTranNo", "tsoSalesOrder", moDDData, "Order No" 'gsBuildString(kIMOrdrNo, oSysDB, oSysSession)
            .lInsSort "CustID", "tarCustomer", moDDData, "CustID" 'gsBuildString(kSOCustomer, oSysDB, oSysSession)
            .lInsSort "CustName", "tarCustomer", moDDData, "CustName" 'gsBuildString(kSOCustomerName, oSysDB, oSysSession)
            .lInsSort "RcvgWhseID", "timWarehouse", moDDData, "Rcvg Warehouse" 'gsBuildString(kIMReceivingWhse, oSysDB, oSysSession)
        Else
            .lDeleteSort "OrderTranNo", "tsoSalesOrder"
            .lDeleteSort "CustID", "tarCustomer"
            .lDeleteSort "CustName", "tarCustomer"
            .lDeleteSort "RcvgWhseID", "timWarehouse"
        End If
        
        If (iRptType = kPickRptTypeOrder) Or (iRptType = kPickRptTypeDetail) Then
            .lInsSort "ShipDate", "tsoSOLineDist", moDDData, "Ship Date" 'gsBuildString(kSOShipDate, oSysDB, oSysSession)
            .lInsSort "ShipPriority", "tsoSOLineDist", moDDData, "Ship Priority" 'gsBuildString(kSOShipPriority, oSysDB, oSysSession)"
            .lInsSort "ShipMethID", "tciShipMethod", moDDData, "Ship Via" 'moDDData, gsBuildString(kSOShipvia, oSysDB, oSysSession)
            .lInsSort "AddrName", "tciAddress", moDDData, "Ship To Addr Name" 'gsBuildString(kSOShipToAddress, oSysDB, oSysSession)
            .lInsSort "CustAddrID", "tarCustAddr", moDDData, "Ship To Cust Addr" 'gsBuildString(kSOShipToAddress, oSysDB, oSysSession)
           
        Else
            'Remove sorts when Summary Format is selected
            .lDeleteSort "ShipDate", "tsoSOLineDist"
            '.lDeleteSort "RcvgWhseID", "timWarehouse"
            .lDeleteSort "ShipPriority", "tsoSOLineDist"
            .lDeleteSort "ShipMethID", "tciShipMethod"
            .lDeleteSort "AddrName", "tciAddress"
            .lDeleteSort "CustAddrID", "tarCustAddr"
        End If


        If (iRptType = kPickRptTypeOrder) Then
            .AutoSubtotal "timItem", "ItemID"
            .AutoSubtotal "tsoSOLine", "SOLineNo"
            .AutoSubtotal "tsoSOLineDist", "ShipDate"
            .AutoSubtotal "timWarehouse", "RcvgWhseID"
            .AutoSubtotal "tsoSOLineDist", "ShipPriority"
            .AutoSubtotal "tciShipMethod", "ShipMethID"
            .AutoSubtotal "tciAddress", "AddrName"
            .AutoSubtotal "tarCustAddr", "CustAddrID"

        End If

        
        'Delete un-needed sorts
        .lDeleteSort "QtyToPick", "tsoShipLineDist"

    End With
    
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bInitSort", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub SetDefaultSorts(iRptType)

On Error GoTo ExpectedErrorRoutine

    Dim sStr As String

    'Set the focus to grdSort so the drop down boxes in the
    'grids will be set up correctly
    On Error Resume Next
    moSort.SortGridGotFocus
    On Error GoTo ExpectedErrorRoutine
    
    With moSort
        If mbIMIntegrated Then
            If iRptType = kPickRptTypeOrder Then
                sStr = gsBuildString(kSOItemID, moAppDB, moClass.moSysSession)
                .bRestoreRow 1, "ItemID", "timItem", 1, 0, 0
            End If
        Else
            If iRptType = kPickRptTypeOrder Then
                .bRestoreRow 1, "SOLineNo", "tsoSOLine", 1, 0, 0
            End If
        End If
        
        If (iRptType = kPickRptTypeDetail) Then
            .bRestoreRow 1, "TranNoRelChngOrd", "tsoSalesOrder", 0, 0, 0
        End If
        
    End With

    If (iRptType = kPickRptTypeDetail) Then
        gGridLockCell grdSort, 3, 1
        gGridLockCell grdSort, 4, 1
    End If
    
    Exit Sub
    
ExpectedErrorRoutine:

End Sub
Private Function bRestoreSort(iRptType) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim Row As Long
    Dim sTableColumnName As String
    Dim sDescription As String
    Dim iSubtotal As Integer
    Dim iPageBreak As Integer
    Dim iSortOrder As Integer
    Dim sColumnName As String
    Dim iSeqNo As Integer
    Dim sTableName As String
    Dim sParentTableName As String
    Dim iPos As Integer
    Dim i As Integer
    
    bRestoreSort = False
    
    For i = 1 To UBound(SortSettings)
        iPos = moSort.lSetRow(SortSettings(i).Row, SortSettings(i).sTableColumnName, _
                      SortSettings(i).sDescription, SortSettings(i).iSubtotal, _
                      SortSettings(i).iPageBreak, SortSettings(i).iSortOrder, _
                      SortSettings(i).sColumnName, SortSettings(i).iSeqNo, _
                      SortSettings(i).sTableName, SortSettings(i).sParentTableName)
        If (iPos = Not (kSuccess)) Then
            Exit Function
        End If
    Next i

'    If (iRptType = kPickRptTypeOrder) Then
'        moSort.SetRowSort 1, "tsoSalesOrder", "TranNoRelChngOrd"
'        moSort.SetRowSubtotal 1, True
'        gGridLockRow grdSort, 3
'
'    ElseIf (iRptType = kPickRptTypeSummary) Or (iRptType = kPickRptTypeDetail) Then
'        If mbIMIntegrated Then
'            moSort.SetRowSort 1, "timWarehouse", "WhseID"
'            moSort.SetRowSubtotal 1, True
'            moSort.SetRowPageBreak 1, True
'            gGridLockRow grdSort, 1
'
'            If miZoneSortOrder = kSortByZoneID Then
'                moSort.SetRowSort 2, "timWhseZone", "WhseZoneID"
'                moSort.SetRowSubtotal 1, True
'                moSort.SetRowPageBreak 1, True
'                gGridLockRow grdSort, 2
'            End If
'
'        End If
'    End If

    bRestoreSort = True
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bRestoreSort", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bSaveSort() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iSubtotal As Integer
    Dim iPageBreak As Integer
    Dim sTableColumnName As String
    Dim sLastTableColumnName As String
    Dim sDescription As String
    Dim i As Long
    Dim lRet As Long
    Dim sColumnName As String
    Dim iSeqNo As Integer
    Dim sTableName As String
    Dim iSortOrder As Integer
    Dim sParentTableName As String
    Dim iPos As Integer
    Dim iEntries As Integer
    
    bSaveSort = False
    
    iEntries = 0
    
    ReDim SortSettings(iEntries)
    
    For i = 1 To moSort.MaxRows
        lRet = moSort.lGetRow(i, sTableColumnName, sDescription, iSubtotal, _
                    iPageBreak, iSortOrder, sColumnName, iSeqNo, sTableName, sParentTableName)
        
        If (lRet = kNoMoreEntries) Then
            Exit For
        End If
        
        If sTableName <> "timWhseZone" Then
        'Do not save the sorting definition for WhseZone since it is only
        'a default value for WavePicking with Zone
            If (lRet = kSuccess) Then
                iEntries = iEntries + 1
                ReDim Preserve SortSettings(iEntries)
                SortSettings(iEntries).Row = i
                SortSettings(iEntries).sTableColumnName = sTableColumnName
                SortSettings(iEntries).sDescription = sDescription
                SortSettings(iEntries).iSubtotal = iSubtotal
                SortSettings(iEntries).iPageBreak = iPageBreak
                SortSettings(iEntries).iSortOrder = iSortOrder
                SortSettings(iEntries).sColumnName = sColumnName
                SortSettings(iEntries).iSeqNo = iSeqNo
                SortSettings(iEntries).sTableName = sTableName
                SortSettings(iEntries).sParentTableName = sParentTableName
            Else
                Exit Function
            End If
        End If
    Next i

    bSaveSort = True
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSaveSort", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub GetModuleOptions()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moModuleOptions = New clsModuleOptions
    With moModuleOptions
        Set .oSysSession = moSysSession
        Set .oAppDb = moAppDB
        .sCompanyID = msCompanyID
    End With
    
    'Get Qty Decimal Places
    miNbrDecPlaces = giGetValidInt(moModuleOptions.CI("QtyDecPlaces"))
    moProcessPickList.iNbrDecPlaces = miNbrDecPlaces
    
    'Look up integrate with IM Flag and AD module activation flag
    mbIMIntegrated = moAppDB.Lookup("IntegrateWithIM", "tsoOptions", "CompanyID = " & gsQuoted(msCompanyID))
    
    'Only get miAllowNegQty when IM enabled
    If mbIMIntegrated Then
        miAllowNegQty = giGetValidInt(moModuleOptions.IM("AllowNegQtyOnHand"))
    End If
    
    'Get CreditCheckAtPicking and CreditHoldAutoRelease flag
    miCreditCheckAtPicking = giGetValidInt(moModuleOptions.SO("CreditCheckAtPicking"))
    miCreditHoldAutoRelease = giGetValidInt(moModuleOptions.SO("CreditHoldAutoRelease"))
    miOverShipmentPolicy = giGetValidInt(moModuleOptions.SO("OverShipmentPolicy"))
    miMaxSerialNbrsOnPickList = giGetValidInt(moModuleOptions.SO("MaxSerialNbrsOnPickList"))
                                                              
    'Set the related property in moclass
    moProcessPickList.iOverShipmentPolicy = miOverShipmentPolicy
    moProcessPickList.lSysBatchKey = giGetValidInt(moModuleOptions.SO("ShipmentHiddenBatchKey"))
                                                              
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetModuleOptions", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub cboSetting_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboSetting, True
    #End If
'+++ End Customizer Code Push +++
    Dim i As Integer
    If Not moSettings.gbSkipClick Then
        'mbLoadSetting = True
        mbManualClick = True
        moSettings.bLoadReportSettings True
        mbManualClick = False
        'mbLoadSetting = False
    End If
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboSetting_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub BindLineGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moDMLineGrid = New clsDmGrid
    With moDMLineGrid
        Set .Session = moSysSession
        Set .Database = moAppDB
        Set .Form = frmCreatePick
        Set .Grid = grdLinePicked
        #If RPTDEBUG Then
            .Table = "tsoCreatePickWrk"
        #Else
            .Table = "#tsoCreatePickWrk2"
        #End If
        .UniqueKey = "ShipLineKey"
        .NoAppend = True
        .OrderBy = "StockAllocSeq"
        .Where = "KitShipLineKey IS NULL AND LineHold <> 1 AND CrHold <> 1 AND OrderHold <> 1"
        
        .BindColumn "AllowAutoSub", Nothing, SQL_SMALLINT
        .BindColumn "AllowDecimalQty", kColLineAllowDecimalQty, SQL_SMALLINT
        .BindColumn "AllowSubItem", kColLineAllowSubItem, SQL_SMALLINT
        .BindColumn "AllowImmedShipFromPick", kColLineAllowImmedShipFromPick, SQL_SMALLINT
        .BindColumn "CrHold", kColLineCrHold, SQL_SMALLINT
        .BindColumn "CustID", Nothing, SQL_VARCHAR
        .BindColumn "CustKey", Nothing, SQL_INTEGER, , kDmSetNull
        .BindColumn "CustName", kColLineShiptoName, SQL_VARCHAR
        .BindColumn "CustReqShipComplete", kColLineCustReqShipComplete, SQL_SMALLINT
        .BindColumn "DeliveryMethod", kColLineDeliveryMethod, SQL_SMALLINT
        .BindColumn "DeliveryMethodID", kColLineDeliveryMethodID, SQL_VARCHAR
        .BindColumn "ExtShipmentExists", kColLineExtShipmentExists, SQL_SMALLINT
        .BindColumn "Expiration", Nothing, SQL_DATE
        .BindColumn "FreightAmt", Nothing, SQL_DECIMAL
        .BindColumn "InvtLotNo", kColLineAutoDistLotNo, SQL_VARCHAR
        .BindColumn "InvtLotKey", kColLineAutoDistLotKey, SQL_INTEGER
        .BindColumn "InvtTranLinkKey", kColLineInvtTranID, SQL_INTEGER
        .BindColumn "InvtTranKey", kColLineInvtTranKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "InvtLotExpirationDate", kColLineAutoDistLotExpDate, SQL_DATE
        .BindColumn "IsDistComplete", kColLineDistComplete, SQL_SMALLINT
        .BindColumn "ItemDesc", kColLineItemDesc, SQL_VARCHAR
        .BindColumn "ItemID", kColLineItem, SQL_VARCHAR
        .BindColumn "ItemKey", kColLineItemKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ItemReqShipComplete", kColLineItemReqShipComplete, SQL_SMALLINT
        .BindColumn "ItemType", kColLineItemType, SQL_SMALLINT
        .BindColumn "KitShipLineKey", kColLineKitShipLineKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "LineDistKey", kColLineTranLineDistKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "LineHold", kColLineLineHold, SQL_SMALLINT
        .BindColumn "LineHoldReason", kColLineLineHoldReason, SQL_VARCHAR
        .BindColumn "LineKey", kColLineTranLineKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "LineNbr", kColLineLine, SQL_INTEGER
        .BindColumn "OrderHold", kColLineOrdHold, SQL_SMALLINT
        .BindColumn "OrderHoldReason", Nothing, SQL_VARCHAR
        .BindColumn "OrderKey", kColLineTranKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "OrderLineUnitMeasKey", kColLineOrderLineUOMKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "OrderLineUnitMeasID", kColLineOrderLineUOMID, SQL_VARCHAR
        .BindColumn "Packed", kColLinePacked, SQL_SMALLINT
        .BindColumn "PickStatus", kColLinePickCheck, SQL_SMALLINT
        .BindColumn "QtyAvail", kColLineQtyAvail, SQL_DECIMAL
        .BindColumn "QtyDist", kColLineQtyDist, SQL_DECIMAL
        .BindColumn "QtyToPickByOrderUOM", kColLineQtyToPickByOrdUOM, SQL_DECIMAL
        .BindColumn "QtyPendingShip", Nothing, SQL_DECIMAL
        .BindColumn "QtyPicked", kColLineQtyPicked, SQL_DECIMAL
        .BindColumn "QtyShort", kColLineQtyShort, SQL_DECIMAL
        .BindColumn "QtyToPick", kColLineQtyToPick, SQL_DECIMAL
        .BindColumn "QtyOrdered", kColLineQtyOrdered, SQL_DECIMAL
        .BindColumn "RcvgWhseID", kColLineRcvgWarehouse, SQL_VARCHAR
        .BindColumn "RcvgWhseKey", Nothing, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipKey", kColLineShipKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipmentCommitStatus", kColLineShipmentCommitStatus, SQL_SMALLINT
        .BindColumn "ShipDate", kColLineShipDate, SQL_DATE
        .BindColumn "ShipLineKey", kColLineShipLineKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipLineDistKey", kColLineShipLineDistKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipLineUpdateCounter", kColLineShipLineUpdateCounter, SQL_INTEGER
        .BindColumn "ShipLineDistUpdateCounter", kColLineShipLineDistUpdateCounter, SQL_INTEGER
        .BindColumn "ShipMethID", kColLineShipVia, SQL_VARCHAR
        .BindColumn "ShipMethKey", kColLineShipMethKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipPriority", kColLineShipPriority, SQL_VARCHAR, , kDmSetNull
        .BindColumn "ShipToAddrKey", Nothing, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipToCustAddress", kColLineShiptoAddr, SQL_VARCHAR
        .BindColumn "ShipToCustAddrID", kColLineShiptoAddrID, SQL_VARCHAR
        .BindColumn "ShipToCustAddrKey", kColLineShiptoAddrKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipUnitMeasID", kColLineShipUOM, SQL_VARCHAR
        .BindColumn "ShipUnitMeasKey", kColLineShipUOMKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipWhseID", kColLineShipWhseID, SQL_VARCHAR
        .BindColumn "ShipWhseKey", kColLineShipWhseKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShortPick", kColLineShortPickFlag, SQL_SMALLINT
        .BindColumn "Status", kColLineStatus, SQL_VARCHAR
        .BindColumn "StatusCustShipCompleteViolation", kColLineStatusCustShipCompleteViolation, SQL_VARCHAR
        .BindColumn "StatusItemShipCompLeteViolation", kColLineStatusItemShipCompLeteViolation, SQL_VARCHAR
        .BindColumn "StatusDistWarning", kColLineStatusDistWarning, SQL_VARCHAR
        .BindColumn "StatusDistQtyShort", kColLineStatusDistQtyShort, SQL_VARCHAR
        .BindColumn "StatusOverShipment", kColLineStatusOverShipment, SQL_VARCHAR
        .BindColumn "StockAllocSeq", kColLineSASequence, SQL_INTEGER
        .BindColumn "StockQtyAvail", Nothing, SQL_DECIMAL
        .BindColumn "StockUnitMeasKey", Nothing, SQL_INTEGER, , kDmSetNull
        .BindColumn "SubstiStatus", kColLineSubFlag, SQL_VARCHAR
        .BindColumn "SubstituteItemDesc", kColLineSubItem, SQL_VARCHAR
        .BindColumn "SubstituteItemKey", kColLineSubItemKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "TrackMeth", kColLineTrackMeth, SQL_SMALLINT
        .BindColumn "TrackQtyAtBin", kColLineTrackQtyAtBin, SQL_SMALLINT
        .BindColumn "TranNoRelChngOrd", kColLineOrderNo, SQL_VARCHAR
        .BindColumn "TranType", kColLineTranType, SQL_INTEGER
        .BindColumn "TranTypeText", kColLineTranTypeText, SQL_VARCHAR
        .BindColumn "WhseBinID", kColLineAutoDistBinID, SQL_VARCHAR
        .BindColumn "WhseBinKey", kColLineAutoDistBinKey, SQL_INTEGER
        .BindColumn "WhseUseBin", kColLineWhseUseBin, SQL_SMALLINT
        .BindColumn "ShipFOBKey", kColLineShipFOBKey, SQL_INTEGER

        'SGS PTC IA (START)
        .LinkSource "vsoBin_SGS", .Table & ".ShipLineKey=vsoBin_SGS.ShipLineKey", kDmJoin, LeftOuter
        .Link giGetValidInt(kColLineBinFlag), "WhseBinID"
        'SGS PTC IA (STOP)

        .Init
    End With
    
    'Bind the grid navigator for the UOM column.
    gbLookupInit navGridUOM, moClass, moAppDB, "UnitOfMeasure"

    gbLookupInit navGridSubItem, moClass, moAppDB, "ItemSubstitute"
    
    gbLookupInit lkuAutoDistBin, moClass, moAppDB, "DistBin"
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindLineGrid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


Private Sub grdLinePicked_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

On Error GoTo ExpectedErrorRoutine
    If Row > 0 Then
        SetMouse
        
        'Initialize the Distribution object if not already initialized
        CreateDistForm Row
        
        'If the customer requires ship complete - check before delete
        If giGetValidInt(gsGridReadCell(grdLinePicked, Row, kColLinePickCheck)) = vbUnchecked And Not mbInIsvalidQty Then
            If giGetValidInt(gsGridReadCell(grdLinePicked, Row, kColLineCustReqShipComplete)) = 1 _
               And glGetValidLong(gsGridReadCell(grdLinePicked, Row, kColLineTranType)) = kSOTranType Then
                If moProcessPickList.bShipCompleteWarnOnDelete(grdLinePicked, Row) Then
                    If giSotaMsgBox(Me, moClass.moSysSession, kSOPartialPickWarn, "") = vbNo Then
                        gGridUpdateCell grdLinePicked, Row, kColLinePickCheck, vbChecked
                        ResetMouse
                        Exit Sub
                    End If
                End If
            End If
        End If
        
        'Perform pick line update
        If Not moProcessPickList.bPerformPickLineUpdate(Me, grdLinePicked, moDMLineGrid, Row) Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgCMUnexpectedSPError, "Update Pick List"
            ResetMouse
            Exit Sub
        End If
        
         grdLinePicked.SetActiveCell m_LastEnterCol, m_LastEnterRow
       DoEvents
        'Format the grid row
        SetRowValues Row
        
        ResetMouse
        
        'Set the Order Refreshed Flag to Ture so when user click the Order tab
        'the order grid will display the updated data
        mbOrderGridRefreshNeeded = True
    End If
    
    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    Exit Sub

'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "grdLinePicked_ButtonClicked", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub grdLinePicked_EditChange(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'User changed the value on the cell.  Set the mbCellChangeFired to false.  It will be set to true
    'once the event fires.  However, if it doesn't fire, we can use this flag to tell if we have to
    'call the GM CellChange manually.
    mbCellChangeFired = False

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLinePicked_EditChange", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdLinePicked_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    
    moGMLine.Grid_EditMode Col, Row, Mode, ChangeMade
    If Mode = 1 Then
        m_LastEnterCol = Col
        m_LastEnterRow = Row
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLinePicked_EditMode", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub grdLinePicked_TextTipFetch(ByVal Col As Long, ByVal Row As Long, MultiLine As FPSpreadADO.TextTipFetchMultilineConstants, TipWidth As Long, TipText As String, ShowTip As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If

Dim sSubItemID As String
Dim lSubItemKey As Long
Dim lWhseKey As Long
Dim dSubQtyAvail As Double

    sSubItemID = gsGetValidStr(gsGridReadCell(grdLinePicked, Row, kColLineSubItem))
    lSubItemKey = glGetValidLong(gsGridReadCell(grdLinePicked, Row, kColLineSubItemKey))
    lWhseKey = glGetValidLong(gsGridReadCell(grdLinePicked, Row, kColLineShipWhseKey))

    If Col = kColLineQtyAvail Then
        If lSubItemKey > 0 And Len(Trim(sSubItemID)) > 0 Then
            'dSubQtyAvail = dGetSubAvailQty(lSubItemKey)
            dSubQtyAvail = 100
            ShowTip = True
            TipText = "Sub " & Trim(sSubItemID) & " Available = " & dSubQtyAvail
        End If
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLinePicked_TextTipFetch", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdOrderPicked_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

On Error GoTo ExpectedErrorRoutine

    Dim lOrderTranKey As Long
    Dim lOrderTranType As Long
    Dim sWhere As String
    Dim sStatus As String
    Dim sSubstiStatus As String
    Dim rs As Object
    Dim iPickStatus As Integer
    Dim iPickStatusInGrid As Integer
    Dim iUpdateCounterViolation As Integer
    
    SetMouse

    If mbCheckCanceled = True Then
    'Button is clicked by the program, skip the following logic.
        mbCheckCanceled = False
        ResetMouse
        Exit Sub
    End If
    
    If Row > 0 Then
        'Get the TranKey and TranType for the order row clicked
        lOrderTranKey = glGetValidLong(gsGridReadCell(grdOrderPicked, Row, kColOrdTranKey))
        lOrderTranType = glGetValidLong(gsGridReadCell(grdOrderPicked, Row, kcolOrdTranType))

        With grdOrderPicked
            .Col = kColOrdPickCheck
            .Row = Row
            
            If .Value = 1 Then
            'User checked the Pick check box, go ahead pick all the lines belong to the order
                'Set up the Where Clause for auto picking
                sWhere = " ShipLineKey IN (SELECT ShipLineKey FROM #tsoCreatePickWrk2 "
                sWhere = sWhere & " WHERE OrderKey =" & lOrderTranKey & " AND TranType =" & lOrderTranType & ")"
                sWhere = sWhere & " AND LineHold = 0 AND OrderHold = 0 And CrHold = 0 "
        
                'Call the bCreateAutoDistribution function to create the line distribution
                If Not moProcessPickList.bCreateAutoDistribution(sWhere, miEmptyBins, miEmptyRandomBins, iUpdateCounterViolation) Then
                    ResetMouse
                    Exit Sub
                End If
                
                'Check whether update counter violation has been encountered
                If iUpdateCounterViolation = 1 Then
                    'yes, data has beed reloaded, refresh the grid
                    'Data has been reloaded, refresh the grid
                    grdOrderPicked.redraw = False
                    moDMOrderGrid.Refresh
                    grdOrderPicked.redraw = True
                        
                    'Set the Line Refreshed Flag to Ture so when user click the line tab
                    'the line grid will display the updated data
                    mbLineGridRefreshNeeded = True
                    Exit Sub
                End If
                
                'Perform update for special items
                sWhere = " OrderKey =" & lOrderTranKey & " AND TranType =" & lOrderTranType
                sWhere = sWhere & " AND ItemKey IS NULL"
                If Not moProcessPickList.bUpdateShipLineForSpecialItems(sWhere) Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation  'kmsgimspGeneralReserveInvtTran
                    ResetMouse
                    Exit Sub
                Else
                    mbLineGridRefreshNeeded = True
                End If
            Else
                'User unchecked the Pick check box, go ahead unpick all the lines belong to the order
                
                'Build the where clause for the update
                lOrderTranKey = glGetValidLong(gsGridReadCell(grdOrderPicked, Row, kColOrdTranKey))
                lOrderTranType = glGetValidLong(gsGridReadCell(grdOrderPicked, Row, kcolOrdTranType))
                sWhere = " wrk.ShipLineKey IN (SELECT ShipLineKey FROM #tsoCreatePickWrk2 "
                sWhere = sWhere & " WHERE OrderKey =" & lOrderTranKey & " AND TranType =" & lOrderTranType & ")"
            
                'Call the DeleteShipLines function to delete the IM distribution in the real tables
                'and udpate tsoShipLine, tsoShipLineDist, and the bound
                'temp tables.
                If Not moProcessPickList.bDeleteShipLines(sWhere, kDeleteDistributionOnlyYes, iUpdateCounterViolation) Then
                    ResetMouse
                    Exit Sub
                End If
                
                
                'Check whether update counter violation has been encountered
                If iUpdateCounterViolation = 1 Then
                    'yes, data has beed reloaded, refresh the grid
                    'Data has been reloaded, refresh the grid
                    grdOrderPicked.redraw = False
                    moDMOrderGrid.Refresh
                    grdOrderPicked.redraw = True
                        
                    'Set the Line Refreshed Flag to Ture so when user click the line tab
                    'the line grid will display the updated data
                    mbLineGridRefreshNeeded = True
                End If
            End If
            
            'Update the Stauts and SubStatus column
            sWhere = "SELECT PickStatus, Status, SubstiStatus FROM #tsoCreatePickHdrWrk"
            sWhere = sWhere & " WHERE TranKey =" & lOrderTranKey & " AND TranType =" & lOrderTranType

            Set rs = moAppDB.OpenRecordset(sWhere, kSnapshot, kOptionNone)
            If Not rs Is Nothing Then
                If Not rs.IsEOF Then
                    iPickStatus = giGetValidInt(rs.Field("PickStatus"))
                    sStatus = gsGetValidStr(rs.Field("Status"))
                    sSubstiStatus = gsGetValidStr(rs.Field("SubstiStatus"))
                End If
            End If
            rs.Close
            Set rs = Nothing

            gGridUpdateCell grdOrderPicked, Row, kColOrdSubFlag, sSubstiStatus
            gGridUpdateCell grdOrderPicked, Row, kColOrdStatus, sStatus

            With grdOrderPicked
            'Check whether the auto pick is successful or not
                .Col = kColOrdPickCheck
                .Row = Row
                
                iPickStatusInGrid = giGetValidInt(.Value)
                
                If iPickStatus = 0 And iPickStatusInGrid = 1 Then
                'The auto pick is unable to pick any quantity, uncheck the
                'check box
                    mbCheckCanceled = True
                    .Value = 0
                    mbCheckCanceled = False
                Else
                    If iPickStatus = 1 And iPickStatusInGrid = 0 And mlTaskID = ktskReprintPickList Then
                    'In reprint pick list mode, user wants to cancel the delete ship line action
                    'recheck the checkbox
                        mbCheckCanceled = True
                        .Value = 1
                        mbCheckCanceled = False
                    End If
                End If
                
            End With

            'Set the Line Refreshed Flag to Ture so when user click the line tab
            'the line grid will display the updated data
            mbLineGridRefreshNeeded = True
        End With
            
        'Set the DM dirty flag for the order changed
        moDMOrderGrid.SetRowDirty Row
        moDMOrderGrid.Save
    End If

    ResetMouse
    
        Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    Exit Sub

'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "grdOrderPicked_ButtonClicked", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdOrderPicked_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    moGMOrder.Grid_Change Col, Row

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdOrderPicked_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub


Private Sub grdOrderPicked_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    moGMOrder.Grid_EditMode Col, Row, Mode, ChangeMade

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdOrderPicked_EditMode", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub



Private Sub grdSort_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSort.GridButtonClicked Col, Row, ButtonDown

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_ButtonClicked", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSort_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim sSortValue As Variant
    Dim Retval As Boolean
    Dim iZoneSort As Integer
    
    sSortValue = ""
    
    If Not mbLoading Then
        
        moSort.GridChange Col, Row
        
    End If
    'Don't allow user to change subtotal or page break flags
    gGridLockCell grdSort, 3, Row
    gGridLockCell grdSort, 4, Row
    If (miRptFormat = kPickRptTypeOrder And _
            (moSort.lSortGridPosition("tarCustAddr", "CustAddrID") = 1 Or _
            moSort.lSortGridPosition("tciAddress", "AddrName") = 1)) Then
        gGridUnlockCell grdSort, 4, Row
    End If

    
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub grdSort_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'    If Not mbLoading Then moSort.SortGridGotFocus
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSort_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSort.SortGridKeyDown keycode, Shift
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSort_KeyUp(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSort.GridKey keycode, Shift
        
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_KeyUp", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSort_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
   
    If Not mbLoading Then moSort.GridLeaveCell Col, Row, NewCol, NewRow
       
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_LeaveCell", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub lkuAutoDistBin_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lTranType As Long
    lTranType = glGetValidLong(gsGridReadCell(grdLinePicked, grdLinePicked.ActiveRow, kColLineTranType))
    If lTranType = kTranTypeSOSO Then
        lTranType = kTranTypeSOSH
    Else
        lTranType = kTranTypeSOTS
    End If
    
    'Initialize the Distribution object if not already initialized
    CreateDistForm grdLinePicked.ActiveRow
    gGridSetActiveCell grdLinePicked, grdLinePicked.ActiveRow, kColLineAutoDistBinID
    moProcessPickList.ProcessAutoBinLkuClick Me, grdLinePicked, moDMLineGrid, moGMLine, lkuAutoDistBin, lTranType
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuAutoDistBin_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub lkuPickListNo_BeforeValidate(Text As String, iReturn As EntryLookupControls.ENTRY_LKU_CTRL_ValidateTypes)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

On Error GoTo VBRigErrorRoutine
    '-- To work around a problem with the lookup control button keeping
    '-- focus if the next control in the tab order is disabled
    
    If Not lkuPickListNo.EnabledText Then
        SetCtrlFocus chkPrintRptSetting
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPickListNo_BeforeValidate", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
End Sub


Private Sub moGMLine_CellChange(ByVal lRow As Long, ByVal lCol As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
  
    'Debug.Print "moGMLine_CellChange"
    If (lCol <> kColLineShipUOM) And (lCol <> kColLineQtyPicked) And (lCol <> kColLineSubItem) And (lCol <> kColLineAutoDistBinID) And (lCol <> kColLineQtyToPick) Then
        Exit Sub
    End If
    
    mbCellChangeFired = True
    
    SetMouse
    
    CreateDistForm lRow
    If Not moItemDist Is Nothing Then
        If Not moProcessPickList.bProcessGridChange(Me, moDMLineGrid, grdLinePicked, lRow, lCol) Then
            ResetMouse
            'grdLinePicked_Click lCol, lRow
            mbLeaveCell = False
            Exit Sub
        End If
    End If
    
    ResetMouse
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "moGM_CellChange", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub



Private Sub moGMLine_LeaveGridRow(ByVal lLeaveRow As Long, ByVal lEnterRow As Long, ByVal eReason As GridMgrExt.LeaveGridRowReasons, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
   
  
   
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moGMLine_LeaveGridRow", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub navGridUOM_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
  
    Dim lRow As Long

    lRow = grdLinePicked.ActiveRow
    gGridSetActiveCell grdLinePicked, lRow, kColLineShipUOM
    txtNavUOM.Text = ""
    
    'Perform nav_click
    gcLookupClick Me, navGridUOM, txtNavUOM, "UnitMeasID"
    moGMLine.LookupClicked

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "navGridUOM_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub navGridSubItem_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lRow As Long

    lRow = grdLinePicked.ActiveRow
    gGridSetActiveCell grdLinePicked, lRow, kColLineSubItem
    txtNavSubItem.Text = ""
    
    'Perform nav_click
    gcLookupClick Me, navGridSubItem, txtNavSubItem, "ItemID" '"ShortDesc"
    moGMLine.LookupClicked
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "navGridSubItem_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub BindOrderGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moDMOrderGrid = New clsDmGrid
    With moDMOrderGrid
        Set .Session = moSysSession
        Set .Database = moAppDB
        Set .Form = frmCreatePick
        Set .Grid = grdOrderPicked
        #If RPTDEBUG Then
            .Table = "tsoCreatePickHdrWrk"
        #Else
            .Table = "#tsoCreatePickHdrWrk"
        #End If
        .UniqueKey = "TranKey, TranType"
        .NoAppend = True
        .OrderBy = "TranID"
        .Where = "Hold <> 1 AND CrHold <> 1"
        
        .BindColumn "CrHold", kColOrdCrHold, SQL_SMALLINT
        .BindColumn "EarliestShipDate", kColOrdEarliestShip, SQL_DATE
        .BindColumn "ExpireDate", kColOrdExpiration, SQL_DATE
        .BindColumn "ExtShipmentExists", kColOrdExtShipmentExists, SQL_SMALLINT
        .BindColumn "Hold", kColOrdHold, SQL_SMALLINT
        .BindColumn "HoldReason", kColOrdHoldReason, SQL_VARCHAR
        .BindColumn "OrderDate", kColOrdOrderDate, SQL_DATE
        .BindColumn "Packed", kColOrdPacked, SQL_SMALLINT
        .BindColumn "PickStatus", kColOrdPickCheck, SQL_SMALLINT
        .BindColumn "RcvgWhseKey", kColOrdRcvgWhseKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "RcvgWhseID", kColOrdRcvgWhseID, SQL_VARCHAR
        .BindColumn "RcvgWhseDesc", kColOrdRcvgWhseDesc, SQL_VARCHAR
        .BindColumn "ShipmentCommitStatus", kColOrdShipmentCommitStatus, SQL_SMALLINT
        .BindColumn "ShipMethID", kColOrdShipVia, SQL_VARCHAR
        .BindColumn "ShipMethKey", Nothing, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipPriority", kColOrdShipPriority, SQL_VARCHAR, , kDmSetNull
        .BindColumn "ShipToAddrKey", kColOrdShipToAddrKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipToCustAddrID", kColOrdShipToAddrID, SQL_VARCHAR
        .BindColumn "ShipWhseID", kColOrdShipFromWhse, SQL_VARCHAR
        .BindColumn "ShipWhseKey", Nothing, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShortPick", kColOrdShortPickFlag, SQL_SMALLINT
        .BindColumn "Status", kColOrdStatus, SQL_VARCHAR
        .BindColumn "SubstiStatus", kColOrdSubFlag, SQL_VARCHAR
        .BindColumn "TranID", kColOrdOrderNo, SQL_VARCHAR
        .BindColumn "TranKey", kColOrdTranKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "TranType", kcolOrdTranType, SQL_INTEGER
        .BindColumn "TranTypeText", kcolOrdTranTypeText, SQL_VARCHAR
        
        'SGS DEJ for IA (START)
        .LinkSource "vluBOLTran_SGS", .Table & ".TranKey=vluBOLTran_SGS.TranKey and " & .Table & ".TranType=vluBOLTran_SGS.TranType", kDmJoin, LeftOuter
        .Link giGetValidInt(kColOrdBOL), "BOLNo"
        'SGS DEJ for IA (STOP)
        
        .Init
    End With

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindOrderGrid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Private Sub BindPickListHeader()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moDMPickListHeader = New clsDmForm
    With moDMPickListHeader
        Set .Form = frmCreatePick
        Set .Session = moClass.moSysSession
        Set .Database = moAppDB
        Set .Toolbar = tbrMain
        Set .SOTAStatusBar = sbrMain
        .AppName = Me.Caption
        .Table = "tsoPickList"
        .UniqueKey = "PickListKey"
        .OrderBy = "PickListNo"
        .AccessType = kDmBuildQueries
        
        '-- Bind pick list columns
        .Bind Nothing, "CompanyID", SQL_VARCHAR
        .Bind Nothing, "DeliveryMeth", SQL_SMALLINT
        .Bind chkInclude(kInclAbbrvShipToAddr), "InclAbbrShipToAddr", SQL_SMALLINT
        .Bind chkIncludeInDetail(kInclDetlBinLocation), "InclBinLoc", SQL_SMALLINT
        .Bind chkInclude(kInclBTOKitLine), "InclBTOKitList", SQL_SMALLINT
        .Bind chkInclude(kInclExtLineComm), "InclComments", SQL_SMALLINT
        .Bind chkIncludeInDetail(kInclDetlKitComp), "InclKitCompInd", SQL_SMALLINT
        .Bind chkInclude(kInclNonInvt), "InclNonStock", SQL_SMALLINT
        .Bind chkIncludeInDetail(kInclDetlPriority), "InclPriority", SQL_SMALLINT
        .Bind chkIncludeInDetail(kInclDetlShipComplete), "InclShipCompIeteInd", SQL_SMALLINT
        .Bind chkIncludeInDetail(kInclDetlShipDate), "InclShipDate", SQL_SMALLINT
        .Bind chkIncludeInDetail(kInclDetlShipVia), "InclShipMethod", SQL_SMALLINT
        .Bind chkInclude(kInclShipToAddr), "InclShipToAddr", SQL_SMALLINT
        .Bind chkIncludeInDetail(kInclDetlSubstitution), "InclSubInd", SQL_SMALLINT
        .Bind Nothing, "PickListKey", SQL_INTEGER
        .Bind lkuPickListNo, "PickListNo", SQL_VARCHAR, kDmDisplayColumn
        .Bind Nothing, "PrintCount", SQL_SMALLINT
        .Bind Nothing, "RptFormat", SQL_SMALLINT
        .Bind Nothing, "SortByZone", SQL_SMALLINT
        .Bind Nothing, "BinSortOrder", SQL_SMALLINT
        
        .Init
    End With

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindPickListHeader", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub SetupLineGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Set general grid properties
    gGridSetProperties grdLinePicked, kMaxLineCols, kGridDataSheetNoAppend
    gGridSetColors grdLinePicked
        
    With grdLinePicked
        .OperationMode = SS_OP_MODE_NORMAL
        .AllowMultiBlocks = False
        .SelectBlockOptions = SS_SELBLOCKOPT_ALL
        .RowHeaderDisplay = DispNumbers
        .DisplayRowHeaders = False 'true
        .NoBeep = True 'prevent beeps on locked cells
    End With
    
  'set column captions
    gGridSetHeader grdLinePicked, kColLinePickCheck, msPick
    gGridSetHeader grdLinePicked, kColLineItem, msItemOrdered
    gGridSetHeader grdLinePicked, kColLineShiptoAddrID, msShipTo
    gGridSetHeader grdLinePicked, kColLineTranTypeText, msTranType
    gGridSetHeader grdLinePicked, kColLineQtyPicked, msQtyPicked
    gGridSetHeader grdLinePicked, kColLineQtyToPick, msQtyOpen 'msQtyToPick
    gGridSetHeader grdLinePicked, kColLineQtyShort, msShort
    gGridSetHeader grdLinePicked, kColLineQtyAvail, msAvailToShip
    gGridSetHeader grdLinePicked, kColLineShipUOM, msShipUOM
    gGridSetHeader grdLinePicked, kColLineStatus, msStat
    gGridSetHeader grdLinePicked, kColLineSubFlag, msSub
    gGridSetHeader grdLinePicked, kColLineShipPriority, msPriority
    gGridSetHeader grdLinePicked, kColLineShipDate, msShipDate
    gGridSetHeader grdLinePicked, kColLineSubItem, msSubItem
    gGridSetHeader grdLinePicked, kColLineDeliveryMethodID, msDelivery
    gGridSetHeader grdLinePicked, kColLineShipVia, msShipVia
    gGridSetHeader grdLinePicked, kColLineOrderNo, msOrderNo
    gGridSetHeader grdLinePicked, kColLineLine, msLineNo
    gGridSetHeader grdLinePicked, kColLineShipWhseID, msWhse
    gGridSetHeader grdLinePicked, kColLineShiptoName, msShipToName
    gGridSetHeader grdLinePicked, kColLineShiptoAddr, msShipToAddress
    gGridSetHeader grdLinePicked, kColLineItemDesc, msDescription
    gGridSetHeader grdLinePicked, kColLineSASequence, msStockAllocSeq
    gGridSetHeader grdLinePicked, kColLineAutoDistBinID, msBin
    gGridSetHeader grdLinePicked, kColLineQtyToPickByOrdUOM, msQtyOpen
    gGridSetHeader grdLinePicked, kColLineOrderLineUOMID, msOrdUOM
        
    'SGS PTC IA
    gGridSetHeader grdLinePicked, kColLineBinFlag, "Tank"
        
    'Hidden Columns
    gGridSetHeader grdLinePicked, kColLineAllowDecimalQty, "AllowDecimalQty"
    gGridSetHeader grdLinePicked, kColLineRcvgWarehouse, "RcvgWarehouse"
    gGridSetHeader grdLinePicked, kColLineLineHoldReason, "Hold Reason"
    gGridSetHeader grdLinePicked, kColLineItemType, "ItemType"
    gGridSetHeader grdLinePicked, kColLineTranKey, "TranKey"
    gGridSetHeader grdLinePicked, kColLineTranType, "TranType"
    gGridSetHeader grdLinePicked, kColLineTranLineKey, "TranLineKey"
    gGridSetHeader grdLinePicked, kColLineTranLineDistKey, "TranLineDistKey"
    gGridSetHeader grdLinePicked, kColLineAllowSubItem, "AllowSubItem"
    gGridSetHeader grdLinePicked, kColLineKitShipLineKey, "KitLineKey"
    gGridSetHeader grdLinePicked, kColLineTrackMeth, "TrackMeth"
    gGridSetHeader grdLinePicked, kColLineTrackQtyAtBin, "TrackQtyAtBin"
    gGridSetHeader grdLinePicked, kColLineInvtTranKey, "InvtTranKey"
    gGridSetHeader grdLinePicked, kColLineShipLineDistKey, "ShipLineDistKey"
    gGridSetHeader grdLinePicked, kColLineOrderLineUOMKey, "OrderLineUOMKey"
    gGridSetHeader grdLinePicked, kColLineShipUOMKey, "ShipUOMKey"
    gGridSetHeader grdLinePicked, kColLineSubItemKey, "SubstituteItemKey"
    gGridSetHeader grdLinePicked, kColLineLineHold, "Hold"
    gGridSetHeader grdLinePicked, kColLineShortPickFlag, "ShortPick"
    gGridSetHeader grdLinePicked, kColLineQtyDist, "Qty Dist"
    gGridSetHeader grdLinePicked, kColLineShipKey, "Ship Key"
    gGridSetHeader grdLinePicked, kColLineShipmentCommitStatus, "ShipmentCommitStatus"
    gGridSetHeader grdLinePicked, kColLineDeliveryMethod, "DeliveryMethod"
    gGridSetHeader grdLinePicked, kColLineShiptoAddrKey, "ShipToAddrKey"
    gGridSetHeader grdLinePicked, kColLineShipMethKey, "ShipMethKey"
    gGridSetHeader grdLinePicked, kColLineShipFOBKey, "ShipFOBKey"
    
    'Set column widths
    gGridSetColumnWidth grdLinePicked, kColLineAllowDecimalQty, 8
    gGridSetColumnWidth grdLinePicked, kColLineAllowSubItem, 4
    gGridSetColumnWidth grdLinePicked, kColLineDeliveryMethodID, 6
    gGridSetColumnWidth grdLinePicked, kColLineInvtTranKey, 4
    gGridSetColumnWidth grdLinePicked, kColLineItem, 12
    gGridSetColumnWidth grdLinePicked, kColLineItemDesc, 11
    gGridSetColumnWidth grdLinePicked, kColLineItemType, 6
    gGridSetColumnWidth grdLinePicked, kColLineKitShipLineKey, 4
    gGridSetColumnWidth grdLinePicked, kColLineLine, 4
    gGridSetColumnWidth grdLinePicked, kColLineLineHold, 4
    gGridSetColumnWidth grdLinePicked, kColLineLineHoldReason, 6
    gGridSetColumnWidth grdLinePicked, kColLineOrderLineUOMKey, 4
    gGridSetColumnWidth grdLinePicked, kColLineOrderNo, 12
    gGridSetColumnWidth grdLinePicked, kColLinePickCheck, 4
    gGridSetColumnWidth grdLinePicked, kColLineQtyAvail, 8
    gGridSetColumnWidth grdLinePicked, kColLineQtyDist, 8
    gGridSetColumnWidth grdLinePicked, kColLineQtyPicked, 8
    gGridSetColumnWidth grdLinePicked, kColLineQtyShort, 8
    gGridSetColumnWidth grdLinePicked, kColLineQtyToPick, 8
    gGridSetColumnWidth grdLinePicked, kColLineRcvgWarehouse, 6
    gGridSetColumnWidth grdLinePicked, kColLineSASequence, 12
    gGridSetColumnWidth grdLinePicked, kColLineShipDate, 8
    gGridSetColumnWidth grdLinePicked, kColLineShipLineDistKey, 4
    gGridSetColumnWidth grdLinePicked, kColLineShipPriority, 4
    gGridSetColumnWidth grdLinePicked, kColLineShiptoAddr, 12
    gGridSetColumnWidth grdLinePicked, kColLineShiptoAddrID, 11
    gGridSetColumnWidth grdLinePicked, kColLineShiptoName, 14
    gGridSetColumnWidth grdLinePicked, kColLineShipUOM, 7
    gGridSetColumnWidth grdLinePicked, kColLineShipUOMKey, 4
    gGridSetColumnWidth grdLinePicked, kColLineShipVia, 10
    gGridSetColumnWidth grdLinePicked, kColLineShipWhseID, 6
    gGridSetColumnWidth grdLinePicked, kColLineStatus, 5
    gGridSetColumnWidth grdLinePicked, kColLineSubFlag, 4
    gGridSetColumnWidth grdLinePicked, kColLineSubItem, 12
    gGridSetColumnWidth grdLinePicked, kColLineSubItemKey, 4
    gGridSetColumnWidth grdLinePicked, kColLineTrackMeth, 4
    gGridSetColumnWidth grdLinePicked, kColLineTrackQtyAtBin, 4
    gGridSetColumnWidth grdLinePicked, kColLineTranKey, 6
    gGridSetColumnWidth grdLinePicked, kColLineTranLineDistKey, 6
    gGridSetColumnWidth grdLinePicked, kColLineTranLineKey, 6
    gGridSetColumnWidth grdLinePicked, kColLineTranType, 6
    gGridSetColumnWidth grdLinePicked, kColLineTranTypeText, 4
    gGridSetColumnWidth grdLinePicked, kColLineShortPickFlag, 4
    gGridSetColumnWidth grdLinePicked, kColLineAutoDistBinID, 10
    gGridSetColumnWidth grdLinePicked, kColLineShiptoAddrKey, 10
    gGridSetColumnWidth grdLinePicked, kColLineShipMethKey, 10
    gGridSetColumnWidth grdLinePicked, kColLineShipFOBKey, 10

    'SGS PTC IA
    gGridSetColumnWidth grdLinePicked, kColLineBinFlag, 5

  'Set column types
    gGridSetColumnType grdLinePicked, kColLineAllowDecimalQty, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineAllowSubItem, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineDeliveryMethodID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineInvtTranID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineInvtTranKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineItem, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineItemDesc, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineItemType, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineKitShipLineKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineLine, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdLinePicked, kColLineLineHoldReason, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineOrderLineUOMKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineOrderNo, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLinePickCheck, SS_CELL_TYPE_CHECKBOX
    gGridSetColumnType grdLinePicked, kColLineRcvgWarehouse, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineSASequence, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdLinePicked, kColLineShipDate, SS_CELL_TYPE_DATE
    gGridSetColumnType grdLinePicked, kColLineShipLineDistKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShipPriority, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShiptoAddr, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShiptoAddrID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShiptoName, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShipUOM, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShipUOMKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShipVia, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShipWhseID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineStatus, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineSubFlag, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineSubItem, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineTrackMeth, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineTrackQtyAtBin, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineTranKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineTranLineDistKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineTranLineKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineTranType, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineTranTypeText, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShortPickFlag, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineAutoDistBinID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShiptoAddrKey, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdLinePicked, kColLineShipMethKey, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdLinePicked, kColLineShipFOBKey, SS_CELL_TYPE_INTEGER
    
    'SGS PTC IA
    gGridSetColumnType grdLinePicked, kColLineBinFlag, SS_CELL_TYPE_EDIT
    
    'lock protected columns
    'gGridLockColumn grdLinePicked, kColLinePickCheck
    gGridLockColumn grdLinePicked, kColLineDeliveryMethodID
    gGridLockColumn grdLinePicked, kColLineItem
    gGridLockColumn grdLinePicked, kColLineItemDesc
    gGridLockColumn grdLinePicked, kColLineLine
    gGridLockColumn grdLinePicked, kColLineOrderNo
    gGridLockColumn grdLinePicked, kColLineQtyToPick
    gGridLockColumn grdLinePicked, kColLineQtyAvail
    gGridLockColumn grdLinePicked, kColLineQtyDist
    gGridLockColumn grdLinePicked, kColLineQtyShort
    gGridLockColumn grdLinePicked, kColLineSASequence
    gGridLockColumn grdLinePicked, kColLineShipDate
    gGridLockColumn grdLinePicked, kColLineShipPriority
    gGridLockColumn grdLinePicked, kColLineShiptoAddr
    gGridLockColumn grdLinePicked, kColLineShiptoAddrID
    gGridLockColumn grdLinePicked, kColLineShiptoName
    gGridLockColumn grdLinePicked, kColLineShipVia
    gGridLockColumn grdLinePicked, kColLineShipWhseID
    gGridLockColumn grdLinePicked, kColLineStatus
    gGridLockColumn grdLinePicked, kColLineSubFlag
    gGridLockColumn grdLinePicked, kColLineSubItem
    gGridLockColumn grdLinePicked, kColLineTranTypeText
    gGridLockColumn grdLinePicked, kColLineOrderLineUOMID
    gGridLockColumn grdLinePicked, kColLineQtyToPickByOrdUOM
    gGridLockColumn grdLinePicked, kColLineShiptoAddrKey
    gGridLockColumn grdLinePicked, kColLineShipMethKey
    gGridLockColumn grdLinePicked, kColLineShipFOBKey
    
    'SGS PTC IA
    gGridLockColumn grdLinePicked, kColLineBinFlag
    
    gGridHAlignColumn grdLinePicked, kColLineShipDate, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
    gGridHAlignColumn grdLinePicked, kColLineSubFlag, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
    gGridHAlignColumn grdLinePicked, kColLineLine, SS_CELL_TYPE_INTEGER, SS_CELL_H_ALIGN_CENTER
    gGridHAlignColumn grdLinePicked, kColLineShipPriority, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
   
    'Hide warehouse column if IM is not interated
    If Not (mbIMIntegrated) Then
         gGridHideColumn grdLinePicked, kColLineShipWhseID
         gGridHideColumn grdLinePicked, kColLineRcvgWarehouse
    End If
    
    'hide columns
    gGridHideColumn grdLinePicked, kColLineAllowDecimalQty
    gGridHideColumn grdLinePicked, kColLineAllowImmedShipFromPick
    gGridHideColumn grdLinePicked, kColLineAllowSubItem
    gGridHideColumn grdLinePicked, kColLineAutoDist
    gGridHideColumn grdLinePicked, kColLineAutoDistBinKey
    gGridHideColumn grdLinePicked, kColLineAutoDistBinQtyAvail
    gGridHideColumn grdLinePicked, kColLineAutoDistBinUOMKey
    gGridHideColumn grdLinePicked, kColLineAutoDistLotExpDate
    gGridHideColumn grdLinePicked, kColLineAutoDistLotKey
    gGridHideColumn grdLinePicked, kColLineAutoDistLotNo
    gGridHideColumn grdLinePicked, kColLineCompItemQty
    gGridHideColumn grdLinePicked, kColLineCrHold
    gGridHideColumn grdLinePicked, kColLineCurrDisplayRow
    gGridHideColumn grdLinePicked, kColLineCustReqShipComplete
    gGridHideColumn grdLinePicked, kColLineDeliveryMethod
    gGridHideColumn grdLinePicked, kColLineDistComplete
    gGridHideColumn grdLinePicked, kColLineExtShipmentExists
    gGridHideColumn grdLinePicked, kColLineInvtTranID
    gGridHideColumn grdLinePicked, kColLineInvtTranKey
    gGridHideColumn grdLinePicked, kColLineItemKey
    gGridHideColumn grdLinePicked, kColLineItemReqShipComplete
    gGridHideColumn grdLinePicked, kColLineItemType
    gGridHideColumn grdLinePicked, kColLineKitShipLineKey
    gGridHideColumn grdLinePicked, kColLineKitShipLineKey
    gGridHideColumn grdLinePicked, kColLineLineHold
    gGridHideColumn grdLinePicked, kColLineLineHoldReason
    'gGridHideColumn grdLinePicked, kColLineOrderLineUOMID
    gGridHideColumn grdLinePicked, kColLineOrderLineUOMKey
    gGridHideColumn grdLinePicked, kColLineOrdHold
    gGridHideColumn grdLinePicked, kColLinePacked
    gGridHideColumn grdLinePicked, kColLineQtyDist
    gGridHideColumn grdLinePicked, kColLineQtyOrdered
    gGridHideColumn grdLinePicked, kColLineRcvgWarehouse
    gGridHideColumn grdLinePicked, kColLineShipKey
    gGridHideColumn grdLinePicked, kColLineShipLineDistKey
    gGridHideColumn grdLinePicked, kColLineShipLineDistUpdateCounter
    gGridHideColumn grdLinePicked, kColLineShipLineKey
    gGridHideColumn grdLinePicked, kColLineShipLineUpdateCounter
    gGridHideColumn grdLinePicked, kColLineShipmentCommitStatus
    gGridHideColumn grdLinePicked, kColLineShipUOMKey
    gGridHideColumn grdLinePicked, kColLineShipWhseKey
    gGridHideColumn grdLinePicked, kColLineShortPickFlag
    gGridHideColumn grdLinePicked, kColLineStatusCustShipCompleteViolation
    gGridHideColumn grdLinePicked, kColLineStatusDistQtyShort
    gGridHideColumn grdLinePicked, kColLineStatusDistWarning
    gGridHideColumn grdLinePicked, kColLineStatusItemShipCompLeteViolation
    gGridHideColumn grdLinePicked, kColLineStatusOverShipment
    gGridHideColumn grdLinePicked, kColLineSubItemKey
    gGridHideColumn grdLinePicked, kColLineTrackMeth
    gGridHideColumn grdLinePicked, kColLineTrackQtyAtBin
    gGridHideColumn grdLinePicked, kColLineTranKey
    gGridHideColumn grdLinePicked, kColLineTranLineDistKey
    gGridHideColumn grdLinePicked, kColLineTranLineKey
    gGridHideColumn grdLinePicked, kColLineTranType
    gGridHideColumn grdLinePicked, kColLineWhseUseBin
    gGridHideColumn grdLinePicked, kColLineShiptoAddrKey
    gGridHideColumn grdLinePicked, kColLineShipMethKey
    gGridHideColumn grdLinePicked, kColLineShipFOBKey
    
    gGridFreezeCols grdLinePicked, kColLinePickCheck
    gGridFreezeCols grdLinePicked, kColLineItem
    gGridFreezeCols grdLinePicked, kColLineShiptoAddrID

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupLineGrid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Private Sub SetupOrderGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Set general grid properties
    gGridSetProperties grdOrderPicked, kMaxOrderCols, kGridDataSheetNoAppend
    gGridSetColors grdOrderPicked
        
    With grdOrderPicked
        .OperationMode = SS_OP_MODE_NORMAL
        .AllowMultiBlocks = False
        .DisplayRowHeaders = False
        '.SelectBlockOptions = SS_SELBLOCKOPT_ALL
        .NoBeep = True 'prevent beeps on locked cells
    End With
    
    '***********************************************************************
    'SGS DEJ IA (START)
    '***********************************************************************
    gGridSetHeader grdOrderPicked, kColOrdBOL, "BOL"
    gGridSetColumnWidth grdOrderPicked, kColOrdBOL, 8
    gGridSetColumnType grdOrderPicked, kColOrdBOL, SS_CELL_TYPE_EDIT
    gGridLockColumn grdOrderPicked, kColOrdBOL
    '***********************************************************************
    'SGS DEJ IA (STOP)
    '***********************************************************************
    
  'set column captions
    gGridSetHeader grdOrderPicked, kColOrdPickCheck, msPick
    gGridSetHeader grdOrderPicked, kColOrdSubFlag, msSub
    gGridSetHeader grdOrderPicked, kColOrdStatus, msStat
    gGridSetHeader grdOrderPicked, kColOrdOrderNo, msOrderNumber
    gGridSetHeader grdOrderPicked, kcolOrdTranTypeText, msTranType
    gGridSetHeader grdOrderPicked, kColOrdShipToAddrID, msShipTo
    gGridSetHeader grdOrderPicked, kColOrdOrderDate, msOrderDate
    gGridSetHeader grdOrderPicked, kColOrdExpiration, msExpireDate
    gGridSetHeader grdOrderPicked, kColOrdShipVia, msShipVia
    gGridSetHeader grdOrderPicked, kColOrdEarliestShip, msEarliestShip
    gGridSetHeader grdOrderPicked, kColOrdShipPriority, msPriority
    gGridSetHeader grdOrderPicked, kColOrdShipFromWhse, msShipFrom
    gGridSetHeader grdOrderPicked, kColOrdTranKey, "Tran Key"
    gGridSetHeader grdOrderPicked, kColOrdShortPickFlag, "Short Pick"
    gGridSetHeader grdOrderPicked, kColOrdShipmentCommitStatus, "ShipmentCommitStatus"
    gGridSetHeader grdOrderPicked, kColOrdExtShipmentExists, "ExtShipmentExists"
    
    
  'Set column widths
    gGridSetColumnWidth grdOrderPicked, kColOrdPickCheck, 4
    gGridSetColumnWidth grdOrderPicked, kColOrdSubFlag, 4
    gGridSetColumnWidth grdOrderPicked, kColOrdStatus, 5
    gGridSetColumnWidth grdOrderPicked, kColOrdOrderNo, 10
    gGridSetColumnWidth grdOrderPicked, kcolOrdTranTypeText, 6
    gGridSetColumnWidth grdOrderPicked, kColOrdShipToAddrID, 11
    gGridSetColumnWidth grdOrderPicked, kColOrdOrderDate, 8
    gGridSetColumnWidth grdOrderPicked, kColOrdExpiration, 8
    gGridSetColumnWidth grdOrderPicked, kColOrdShipVia, 7
    gGridSetColumnWidth grdOrderPicked, kColOrdEarliestShip, 8
    gGridSetColumnWidth grdOrderPicked, kColOrdShipPriority, 6
    gGridSetColumnWidth grdOrderPicked, kColOrdShipFromWhse, 10
    gGridSetColumnWidth grdOrderPicked, kColOrdTranKey, 4
    gGridSetColumnWidth grdOrderPicked, kColOrdShortPickFlag, 4
    gGridSetColumnWidth grdOrderPicked, kColOrdShipmentCommitStatus, 4
    gGridSetColumnWidth grdOrderPicked, kColOrdExtShipmentExists, 4


  'Set column types
    gGridSetColumnType grdOrderPicked, kColOrdPickCheck, SS_CELL_TYPE_CHECKBOX
    gGridSetColumnType grdOrderPicked, kColOrdSubFlag, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderPicked, kColOrdStatus, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderPicked, kColOrdOrderNo, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderPicked, kcolOrdTranType, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderPicked, kColOrdShipToAddrID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderPicked, kColOrdOrderDate, SS_CELL_TYPE_DATE
    gGridSetColumnType grdOrderPicked, kColOrdExpiration, SS_CELL_TYPE_DATE
    gGridSetColumnType grdOrderPicked, kColOrdShipVia, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderPicked, kColOrdEarliestShip, SS_CELL_TYPE_DATE
    gGridSetColumnType grdOrderPicked, kColOrdShipPriority, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderPicked, kColOrdShipFromWhse, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderPicked, kColOrdTranKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderPicked, kColOrdHold, SS_CELL_TYPE_CHECKBOX
    gGridSetColumnType grdOrderPicked, kColOrdCrHold, SS_CELL_TYPE_CHECKBOX
    gGridSetColumnType grdOrderPicked, kColOrdHoldReason, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderPicked, kColOrdShortPickFlag, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderPicked, kColOrdShipmentCommitStatus, SS_CELL_TYPE_EDIT


    'lock protected columns
    'gGridLockColumn grdOrderPicked, kColOrdPickCheck
    gGridLockColumn grdOrderPicked, kColOrdSubFlag
    gGridLockColumn grdOrderPicked, kColOrdStatus
    gGridLockColumn grdOrderPicked, kColOrdOrderNo
    gGridLockColumn grdOrderPicked, kcolOrdTranTypeText
    gGridLockColumn grdOrderPicked, kColOrdShipToAddrID
    gGridLockColumn grdOrderPicked, kColOrdOrderDate
    gGridLockColumn grdOrderPicked, kColOrdExpiration
    gGridLockColumn grdOrderPicked, kColOrdShipVia
    gGridLockColumn grdOrderPicked, kColOrdEarliestShip
    gGridLockColumn grdOrderPicked, kColOrdShipPriority
    gGridLockColumn grdOrderPicked, kColOrdShipFromWhse
    
    
    gGridHAlignColumn grdOrderPicked, kColOrdOrderDate, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
    gGridHAlignColumn grdOrderPicked, kColOrdShipPriority, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
    gGridHAlignColumn grdOrderPicked, kColOrdExpiration, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
    gGridHAlignColumn grdOrderPicked, kColOrdEarliestShip, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
    gGridHAlignColumn grdOrderPicked, kColOrdSubFlag, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
    
    'Hide warehouse column if IM is not interated
    If Not (mbIMIntegrated) Then
         gGridHideColumn grdOrderPicked, kColOrdShipFromWhse
    End If
    
    'hide columns
    gGridHideColumn grdOrderPicked, kColOrdTranKey
    gGridHideColumn grdOrderPicked, kcolOrdTranType
    gGridHideColumn grdOrderPicked, kColOrdShipToAddrKey
    gGridHideColumn grdOrderPicked, kColOrdRcvgWhseKey
    gGridHideColumn grdOrderPicked, kColOrdRcvgWhseID
    gGridHideColumn grdOrderPicked, kColOrdRcvgWhseDesc
    gGridHideColumn grdOrderPicked, kColOrdHold
    gGridHideColumn grdOrderPicked, kColOrdCrHold
    gGridHideColumn grdOrderPicked, kColOrdHoldReason
    gGridHideColumn grdOrderPicked, kColOrdShortPickFlag
    gGridHideColumn grdOrderPicked, kColOrdShipmentCommitStatus
    gGridHideColumn grdOrderPicked, kColOrdExtShipmentExists
    gGridHideColumn grdOrderPicked, kColOrdPacked
    
    gGridFreezeCols grdOrderPicked, kColOrdOrderNo

    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupOrderGrid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub DMGridRowLoaded(oDM As Object, lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim iAllowDecimalQty As Integer
Dim lInvtTranKey As Long
Dim lItemKey As Long
Dim lSubItemKey As Long
Dim iShipmentCommitStatus As Integer
Dim iExtShipmentExists As Integer
Dim iPacked As Integer
Dim lShipLineKey As Long
Dim lOrderKey As Long

    If oDM Is moDMLineGrid Then
        'sSQLDate = Left(gsGridReadCellText(grdLinePicked, lRow, kColLineShipDate), 10) 'Remove time
        'gGridUpdateCellText grdLinePicked, lRow, kColLineShipDate, Format(sSQLDate, "SHORT DATE") 'Is Localized
    
        'Always display the locked Qty column with decimal place
        moProcessPickList.SetGridNumericAttr grdLinePicked, kColLineQtyDist, miNbrDecPlaces, kMaxQtyLen, kAllowDecimalPlaces, lRow
        moProcessPickList.SetGridNumericAttr grdLinePicked, kColLineQtyToPick, miNbrDecPlaces, kMaxQtyLen, kAllowDecimalPlaces, lRow
        moProcessPickList.SetGridNumericAttr grdLinePicked, kColLineQtyAvail, miNbrDecPlaces, kMaxQtyLen, kAllowDecimalPlaces, lRow
        moProcessPickList.SetGridNumericAttr grdLinePicked, kColLineQtyShort, miNbrDecPlaces, kMaxQtyLen, kAllowDecimalPlaces, lRow
        moProcessPickList.SetGridNumericAttr grdLinePicked, kColLineQtyToPickByOrdUOM, miNbrDecPlaces, kMaxQtyLen, kAllowDecimalPlaces, lRow
        
        'For QtyPicked column, apply the AllowDecimalPlace rule defined for the item
        'in the grid row since this field can be edited by the user. Related UOM
        'convertion rules will be enforced in the data validation.
        iAllowDecimalQty = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineAllowDecimalQty))
        moProcessPickList.SetGridNumericAttr grdLinePicked, kColLineQtyPicked, miNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow
        
        lShipLineKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineShipLineKey))
       
        SetRowValues lRow, True
        
        'Get the bin info from the saved distributions.
'        lInvtTranKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineInvtTranKey))
'        If lInvtTranKey <> 0 Then
'            lSubItemKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineSubItemKey))
'            If lSubItemKey <> 0 Then
'                lItemKey = lSubItemKey
'            Else
'                lItemKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineItemKey))
'            End If
'            CreateDistForm lRow
'            moProcessPickList.UpdateAutoDistBinCols grdLinePicked, lRow, lItemKey, lShipLineKey, lInvtTranKey
'        End If
       
        If OptLineShow(kShowShortOnly).Value = True Then
            HidePicked kLine, lRow
        End If
        
        'Get active row
        If mlCurrentRowShipLineKey > 0 And mlCurrentActiveColumn > 0 Then
            If lShipLineKey = mlCurrentRowShipLineKey Then
                mlCurrentLineActiveRow = lRow
            End If
        End If
    Else
        iShipmentCommitStatus = giGetValidInt(gsGridReadCell(grdOrderPicked, lRow, kColOrdShipmentCommitStatus))
        iExtShipmentExists = giGetValidInt(gsGridReadCell(grdOrderPicked, lRow, kColOrdExtShipmentExists))
        iPacked = giGetValidInt(gsGridReadCell(grdOrderPicked, lRow, kColOrdPacked))
        lOrderKey = glGetValidLong(gsGridReadCell(grdOrderPicked, lRow, kColOrdTranKey))
       
        If iShipmentCommitStatus = 1 Or iExtShipmentExists = 1 Or iPacked = 1 Or miSecurityLevel = kSecLevelDisplayOnly Then
            gGridLockRow grdOrderPicked, lRow
        End If
        
        If OptOrderShow(kShowShortOnly).Value = True Then
            HidePicked kOrder, lRow
        End If
        
        'Get active row
        If mlCurrentRowOrderKey > 0 Then
            If lOrderKey = mlCurrentRowOrderKey Then
                mlCurrentOrderActiveRow = lRow
            End If
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMGridRowLoaded", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub BindGM()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moGMLine = New clsGridMgr
    With moGMLine
        Set .Grid = grdLinePicked
        Set .DM = moDMLineGrid
        Set .Form = frmCreatePick
        .GridType = kGridDataSheetNoAppend
        '.HighlightActiveGridRow = True
        .GridSortEnabled = True
        Set moGridNavSubItem = .BindColumn(kColLineSubItem, navGridSubItem)
        Set moGridNavSubItem.ReturnControl = txtNavSubItem
        Set moGridNavUOM = .BindColumn(kColLineShipUOM, navGridUOM)
        Set moGridNavUOM.ReturnControl = txtNavUOM
        Set moGridLkuAutoDistBin = .BindColumn(kColLineAutoDistBinID, lkuAutoDistBin)
        Set moGridLkuAutoDistBin.ReturnControl = txtlkuAutoDistBin
        .Init
    End With
    
    Set moGMOrder = New clsGridMgr
    With moGMOrder
        Set .Grid = grdOrderPicked
        Set .DM = moDMOrderGrid
        Set .Form = frmCreatePick
        .GridType = kGridDataSheetNoAppend
        '.HighlightActiveGridRow = True
        .GridSortEnabled = True
        .Init
    End With
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindGM", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moContextMenu = New clsContextMenu
    
    With moContextMenu
        Set .Form = frmCreatePick
        
        .BindGrid moGMLine, grdLinePicked.hwnd
        .BindGrid moGMOrder, grdOrderPicked.hwnd
        
        .Bind "*APPEND", grdLinePicked.hwnd, kEntTypeSOPicking
        .Bind "*DYNAMIC", grdOrderPicked.hwnd
        .Init
    End With
    
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Function CMAppendContextMenu(ctl As Control, hmenu As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************************
'      Desc: Called to append menu items to right mouse popup menu.
'            Called because form_load did a .Bind *APPEND.
'     Parms: Ctl:   The control that received the right mouse button down message.
'            hmenu: The handle to the popup menu
'   Returns: True if menu item added; False if menu item not added.
'************************************************************************************
    CMAppendContextMenu = False

    Dim sDeleteLine As String
    Dim lRow As Long
    Dim lTranType As Long
    Dim lOrderKey As Long
    Dim iShipmentCommitStatus As Integer
    Dim iExtShipmentExists As Integer
    
    If ctl Is grdLinePicked Then
    
        lRow = glGetValidLong(ctl.ActiveRow)
        
        If lRow > 0 Then
            iShipmentCommitStatus = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineShipmentCommitStatus))
            iExtShipmentExists = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineExtShipmentExists))
            If iShipmentCommitStatus = 0 And iExtShipmentExists = 0 And (miSecurityLevel = kSecLevelSupervisory Or miSecurityLevel = kSecLevelNormal) Then
                sDeleteLine = gsBuildString(kDeleteLine, moAppDB, moClass.moSysSession)
                AppendMenu hmenu, MF_ENABLED, kMenuDeleteLine, sDeleteLine
            End If
        End If
    End If
    
    If ctl Is grdOrderPicked Then
        
        lRow = glGetValidLong(ctl.ActiveRow)
        
        If lRow > 0 Then
            lTranType = glGetValidLong(gsGridReadCell(grdOrderPicked, lRow, kcolOrdTranType))
        End If
            
        ' Point control to different context menu
        If (lTranType = kTranTypeIMTR) Then
            moContextMenu.AppendDrillMenu hmenu, "IMOUTORDER", _
                                          ctl, Me, (lRow > 0), kEntTypeIMTransOrdOut
        Else
            moContextMenu.AppendDrillMenu hmenu, "SOORDER", _
                                          ctl, Me, (lRow > 0), kEntTypeSOSalesOrder
        End If
    
        If lRow > 0 Then
            iShipmentCommitStatus = giGetValidInt(gsGridReadCell(grdOrderPicked, lRow, kColOrdShipmentCommitStatus))
            iExtShipmentExists = giGetValidInt(gsGridReadCell(grdOrderPicked, lRow, kColOrdExtShipmentExists))
            If iShipmentCommitStatus = 0 And iExtShipmentExists = 0 And (miSecurityLevel = kSecLevelNormal Or miSecurityLevel = kSecLevelSupervisory) Then
                sDeleteLine = "Delete Order" 'gsBuildString(kDeleteorder, moappdb, moClass.moSysSession)
                AppendMenu hmenu, MF_ENABLED, kMenuDeleteOrder, sDeleteLine
            End If
        End If
    End If
    
    CMAppendContextMenu = True
        
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CMAppendContextMenu", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function CMMenuSelected(ctl As Control, lTaskID As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************************
'      Desc: Called when a popup context menu item is selected.
'            Called because menu item was added by CMAppendContextMenu event.
'     Parms: Ctl:   The control that received the right mouse button down message.
'            lTaskID: The Task ID of the selected menu item.
'   Returns: True if successful; False if unsuccessful.
'************************************************************************************
    Dim lActiveRow As Long
    Dim sWhere As String
    Dim lShipLineKey As Long
    Dim lOrderTranKey As Long
    Dim lOrderTranType As Long
    Dim iUpdateCounterViolation As Integer
    Dim iCustShipComplete As Integer
    
    CMMenuSelected = False
      
    'User selected to delete a shipline from the LinePicked grid
    If ctl Is grdLinePicked Then
        lActiveRow = grdLinePicked.ActiveRow
        
        Select Case lTaskID
                
            Case kMenuDeleteLine
                'Warn before line deletion if Cust requires ship complete and this is not the only line in the so
                lOrderTranType = glGetValidLong(gsGridReadCell(grdLinePicked, lActiveRow, kColLineTranType))
                iCustShipComplete = giGetValidInt(gsGridReadCell(grdLinePicked, lActiveRow, kColLineCustReqShipComplete))
                If lOrderTranType = kSOTranType And iCustShipComplete = 1 Then
                    If moProcessPickList.bShipCompleteWarnOnDelete(grdLinePicked, lActiveRow) Then
                        If giSotaMsgBox(Me, moClass.moSysSession, kSOPartialPickWarn, "") = vbNo Then
                            Exit Function
                        End If
                    End If
                End If
                
                'Delete the shipline associated with this ship line
                lShipLineKey = glGetValidLong(gsGridReadCell(grdLinePicked, lActiveRow, kColLineShipLineKey))
                sWhere = " wrk.ShipLineKey =" & lShipLineKey
  
                If Not moProcessPickList.bDeleteShipLines(sWhere, kDeleteDistributionOnlyNo, iUpdateCounterViolation) Then
                    Exit Function
                End If
               
                If iUpdateCounterViolation = 0 Then
                    'Delete the line from the LinePicked grid
                    moGMLine.GridDelete
                    'moDMLineGrid.Save
                End If
                
                'Refresh the grid to get the latest QtyAvail
                grdLinePicked.redraw = False
                moDMLineGrid.Refresh
                
                'Refresh frame captions for display grids
                SetupRowsSelCaption kSetupCaptionForLineGridOnly
                'Delete a shipline might cause order pick status to change
                'To save time, don't refresh the line grid till the line tab
                'is clicked
                
                grdLinePicked.redraw = True
                mbOrderGridRefreshNeeded = True
                
        End Select
    End If
    
    'User selected to delete a picked order from the OrderPicked grid
    If ctl Is grdOrderPicked Then
        lActiveRow = grdOrderPicked.ActiveRow
        lOrderTranKey = glGetValidLong(gsGridReadCell(grdOrderPicked, lActiveRow, kColOrdTranKey))
        lOrderTranType = glGetValidLong(gsGridReadCell(grdOrderPicked, lActiveRow, kcolOrdTranType))
        
        Select Case lTaskID
            Case kMenuDeleteOrder
                'Delete the pendshipment and all the shiplines
                'associated with this order
                sWhere = " wrk.ShipLineKey IN (SELECT ShipLineKey FROM #tsoCreatePickWrk2 "
                sWhere = sWhere & " WHERE OrderKey =" & lOrderTranKey & " AND TranType =" & lOrderTranType & ")"
                
                If Not moProcessPickList.bDeleteShipLines(sWhere, kDeleteDistributionOnlyNo, iUpdateCounterViolation) Then
                    Exit Function
                End If
                
                If iUpdateCounterViolation = 0 Then
                    'Delete the order from the Order Picked grid
                    moGMOrder.GridDelete
                    'moDMOrderGrid.Save
                Else
                    grdOrderPicked.redraw = False
                    moDMOrderGrid.Refresh
                    grdOrderPicked.redraw = True
                End If
                
                'Refresh frame captions for display grids
                SetupRowsSelCaption kSetupCaptionForOrderGridOnly
                'Set the Line Refreshed Flag to Ture so when user click the line tab
                'the line grid will display the updated data
                mbLineGridRefreshNeeded = True
        End Select
    End If
    
    CMMenuSelected = True
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CMMenuSelected", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function ETWhereClause(ByVal ctl As Object, _
                              ByVal taskID As Long, _
                              ByVal EntityType As Integer, _
                              ByVal naturalKeyCols As String, _
                              ByVal TaskGroup As EntityTask_Groups) As String
'+++ VB/Rig Skip +++
'*******************************************************************************
'   Description: OPTIONAL
'                Entity Task Where Clause. This routine allows the application
'                to specify the where clause to be used against the
'                Host Data View for the specified entity.
'
'                If a where clause is not specified, the where clause will be
'                built using the Natural Key Columns specified for the entity.
'
'                Surrogate Keys as natural keys and other criteria make it
'                impossible without supporting metadata to get these values from
'                the application.
'
'   Parameters:
'                ctl <in> - Control that selected the task
'                taskID <in> - Task ID for the Entity Task
'                entityType <in> - Entity Type bound to the control that selected the task
'                naturalKeyCols <in> - Key columns used to specify where clause (FYI)
'                TaskGroup <in> - Task Group 100-500 (FYI)
'
'   Returns:
'                A specified where clause WITHOUT the 'WHERE' verb. (i.e. "1 = 2")
'*******************************************************************************
On Error Resume Next

    Dim lActiveRow As Long
    Dim lOrderKey As Long
    Dim lTranType As Long
    
    Select Case True
        Case ctl Is grdLinePicked
            lActiveRow = glGetValidLong(grdLinePicked.ActiveRow)
            ETWhereClause = "ShipLineKey = " & CStr(glGetValidLong(gsGridReadCell(grdLinePicked, lActiveRow, kColLineShipLineKey)))
        
        Case ctl Is grdOrderPicked
            lActiveRow = glGetValidLong(grdOrderPicked.ActiveRow)
            
            lOrderKey = glGetValidLong(gsGridReadCell(grdOrderPicked, lActiveRow, kColOrdTranKey))
            lTranType = glGetValidLong(gsGridReadCell(grdOrderPicked, lActiveRow, kcolOrdTranType))
        
            If (lTranType = kTranTypeIMTR) Then
                ETWhereClause = "TrnsfrOrderKey = " & CStr(glGetValidLong(gsGridReadCell(grdOrderPicked, lActiveRow, kColOrdTranKey)))
            Else
                ETWhereClause = "SOKey = " & CStr(glGetValidLong(gsGridReadCell(grdOrderPicked, lActiveRow, kColOrdTranKey)))
            End If
        
        Case Else
            ETWhereClause = ""
    End Select

    Err.Clear

End Function

Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If
    Dim i As Integer

    '-- Unload all forms loaded from this main form
    gUnloadChildForms Me
         
    '-- Remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
        
    If Not moContextMenu Is Nothing Then
        Set moContextMenu = Nothing
    End If
    
    If Not moGridNavUOM Is Nothing Then
        Set moGridNavUOM = Nothing
    End If
    
    If Not moGridNavSubItem Is Nothing Then
        Set moGridNavSubItem = Nothing
    End If
    
    If Not moGridLkuAutoDistBin Is Nothing Then
        Set moGridLkuAutoDistBin = Nothing
    End If
    
    If Not moItemDist Is Nothing Then
        moItemDist.Terminate
        Set moItemDist = Nothing
    End If
    
    If Not moGMLine Is Nothing Then
        moGMLine.UnloadSelf
        Set moGMLine = Nothing
    End If
    
    If Not moDMLineGrid Is Nothing Then
        moDMLineGrid.UnloadSelf
        Set moDMLineGrid = Nothing
    End If
    
    If Not moGMOrder Is Nothing Then
        moGMOrder.UnloadSelf
        Set moGMOrder = Nothing
    End If
    
    If Not moDMOrderGrid Is Nothing Then
        moDMOrderGrid.UnloadSelf
        Set moDMOrderGrid = Nothing
    End If
    
    If Not moDMPickListHeader Is Nothing Then
        moDMPickListHeader.UnloadSelf
        Set moDMPickListHeader = Nothing
    End If
    
    If Not moProcessPickList Is Nothing Then
        Set moProcessPickList = Nothing
    End If
    
    If Not moOptions Is Nothing Then
        Set moOptions = Nothing
    End If
        
    If Not moModuleOptions Is Nothing Then
        Set moModuleOptions = Nothing
    End If
    
    If Not sRealTableCollection Is Nothing Then
        Set sRealTableCollection = Nothing
    End If
    
    If Not sWorkTableCollection Is Nothing Then
        Set sWorkTableCollection = Nothing
    End If
    
    If Not moDDData Is Nothing Then
        Set moDDData = Nothing
    End If
    
    If Not moSort Is Nothing Then
        Set moSort = Nothing
    End If
    
    If Not moReport Is Nothing Then
        Set moReport = Nothing
    End If
    
    If Not moPrinter Is Nothing Then
        Set moPrinter = Nothing
    End If
   
    If Not moAppDB Is Nothing Then
        Set moAppDB = Nothing
    End If
   
    If Not moSysSession Is Nothing Then
        Set moSysSession = Nothing
    End If
    
    If Not moSettings Is Nothing Then
        Set moSettings = Nothing
    End If
    
    If Not moMapSrch Is Nothing Then
        Set moMapSrch = Nothing
    End If
    
    If Not moSotaObjects Is Nothing Then
        Set moSotaObjects = Nothing
    End If
    
       
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub SetupBars()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'****************************************************************
' Description:
'    This routine will setup the toolbar with buttons appropriate
'    for the current project.
'****************************************************************
    '-- Setup the status bar
    With sbrMain
        Set .Framework = moClass.moFramework
        .BrowseVisible = False
        '.StatusVisible = False
    End With
    
    '-- Setup the Toolbar
    If miCallingTask = kCreatePickList Then
        With tbrMain
            .Style = sotaTB_NO_STYLE
            .AddSeparator
            .AddButton kTbPrint
            .AddButton kTbPreview
            .AddSeparator
            .AddButton kTbFinish
            .AddButton kTbFinishExit
            .AddButton kTbCancel
            .AddButton kTbCancelExit
            .AddSeparator
            .AddButton kTbSave
            .AddButton kTbDelete
            .AddSeparator
            If (moClass.moSysSession.ShowOfficeOnToolBar = 1) Then
                .AddButton kTbOffice
                .AddSeparator
                .AddButton kTbHelp
            Else
                .AddButton kTbHelp
            End If
            .AddSeparator
            .AddButton kTbCustomizer
            .LocaleID = mlLanguage
        End With
    Else
        With tbrMain
            .Style = sotaTB_NO_STYLE
            .AddSeparator
            .AddButton kTbPrint
            .AddButton kTbPreview
            .AddButton kTbDefer
            .AddSeparator
            .AddButton kTbFinish
            .AddButton kTbFinishExit
'            .AddSeparator
'            .AddButton kTbSave
'            .AddButton kTbDelete
            .AddSeparator
            If (moClass.moSysSession.ShowOfficeOnToolBar = 1) Then
                .AddButton kTbOffice
                .AddSeparator
                .AddButton kTbHelp
            Else
                .AddButton kTbHelp
            End If
            .AddSeparator
            .AddButton kTbCustomizer
            .LocaleID = mlLanguage
        End With
    End If
    
    '+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupBars", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim iConfirmUnload As Integer
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
   
    '-- Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

        '-- Check all other forms that may have been loaded from this main form.
        '-- If there are any Visible forms, then this means the form is Active.
        '-- Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then GoTo CancelShutDown
    
        Select Case UnloadMode
            Case vbFormCode
                'Do Nothing
            Case Else
                '-- Most likely the user has requested to shut down the form.
                '-- If the context is normal or Drill-Around, have the object unload itself.
                 Select Case mlRunMode
                    Case kContextAOF
                        Me.Hide
                        GoTo CancelShutDown
                    Case Else
                        If Not PartialPickWarn Then
                            GoTo CancelShutDown
                        End If

                        If bIsValidGridData Then
            
                            'Save any un-saved changes
                            moDMOrderGrid.Save
                            moDMLineGrid.Save
                            If iProcessFinish = kFinishCancelled Then
                                GoTo CancelShutDown
                            Else
                                moClass.miShutDownRequester = kUnloadSelfShutDown
                            End If
                        Else
                            GoTo CancelShutDown
                        End If
                End Select
        End Select
    
    '-- If execution gets to this point, the form and class object of the form
    '-- will be shut down. Perform all operations necessary for a clean shutdown.
    PerformCleanShutDown
        
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
        Case Else
            'Do Nothing
    End Select

    Exit Sub
    
CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Resize the grid and associated controls
    If (Me.WindowState = vbNormal Or Me.WindowState = vbMaximized) Then
        '-- Move the controls down
        gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, tabPickOrder, fraOrdersToPick, fraOrderPicked, grdOrderPicked, fraLinesToPick, fraLinePicked, grdLinePicked, fraOptions, fraCustomizer
        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, tabPickOrder, fraOrdersToPick, fraOrderPicked, grdOrderPicked, fraLinesToPick, fraLinePicked, grdLinePicked, fraOptions, fraCustomizer
        
        miOldFormWidth = Me.Width
        miOldFormHeight = Me.Height
    End If
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Set the Class object to Nothing
    Set moClass = Nothing
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdLinePicked_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
On Error GoTo ExpectedErrorRoutine
    'Debug.Print "grdLinePicked_Change"

    If (Col <> kColLineShipUOM) And (Col <> kColLineQtyPicked) And (Col <> kColLineSubItem) And (Col <> kColLineAutoDistBinID) And (Col <> kColLineQtyToPick) Then
        Exit Sub
    End If

    SetMouse
    
    'moGMLine.Grid_Change Col, Row
    moGMLine_CellChange Row, Col
    
    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    Exit Sub
        
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdlinePicked_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub HideNavs()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    navGridSubItem.Visible = False
    navGridUOM.Visible = False

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HideNavs", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub grdLinePicked_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Debug.Print "grdLinePicked_Click"
    
    If Row = 0 And Col <> 0 Then
        moDMLineGrid.Save
        HideNavs
    End If
    moGMLine.Grid_Click Col, Row
  
    If Row > 0 Then 'if event was not caused by a click in the heading area
        If Not moProcessPickList.bSetNavRestrict(grdLinePicked, navGridUOM, navGridSubItem, Row) Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgCMUnexpectedSPError, "Set up lookup control"
        End If
        SetRowValues Row
    End If
    
    'If the user click on any locked cell, highlight the grid row the cell is in
    With grdLinePicked
        .Row = Row
        .Col = Col
        If .Lock = True Then
            gGridSetSelectRow grdLinePicked, Row
        End If
    End With

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLinePicked_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdLinePicked_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    moGMLine.Grid_KeyDown keycode, Shift

'    If bContextMenu Then
'        bContextMenu = False
'        Exit Sub
'    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLinePicked_KeyDown", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdLinePicked_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Debug.Print "grdlinepicked_leavecell"
    
    If Not mbLeaveCell Then
        mbLeaveCell = True
        Cancel = True
        Exit Sub
    End If
    
    Cancel = Not moGMLine.Grid_LeaveCell(Col, Row, NewCol, NewRow)

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLinePickedPicked_LeaveCell", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdLinePicked_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    DoEvents
    moGMLine.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLinePicked_TopLeftChange", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub grdOrderPicked_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    If Row = 0 And Col <> 0 Then
        moDMOrderGrid.Save
    End If
    moGMOrder.Grid_Click Col, Row
    
    'If the user click on any locked cell, highlight the grid row the cell is in
    With grdOrderPicked
        .Row = Row
        .Col = Col
        If .Lock = True Then
            gGridSetSelectRow grdOrderPicked, Row
        End If
    End With

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdOrderPicked_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdOrderPicked_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    moGMOrder.Grid_KeyDown keycode, Shift
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdOrderPicked_KeyDown", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdOrderPicked_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    moGMOrder.Grid_LeaveCell Col, Row, NewCol, NewRow

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdOrderPicked_LeaveCell", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdOrderPicked_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    moGMOrder.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdOrderPicked_TopLeftChange", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Public Function CMRightMouseDown(lWndHandle As Long, lParam As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim y As Long
    Dim x As Long
    Dim lRow As Long
    Dim lCol As Long
    
    CMRightMouseDown = True
    
    '-- Select the row that was right-clicked on
    If lWndHandle = grdLinePicked.hwnd Then
        y = ((lParam And &HFFFF0000) / &H10000) * Screen.TwipsPerPixelY
        x = (lParam And &HFFFF&) * Screen.TwipsPerPixelX
        
        grdLinePicked.GetCellFromScreenCoord lCol, lRow, x, y
                     
        Select Case lRow
            Case 0, -1, -2  '-- RightClick on Column Header or Gray area
                'Do Nothing
             
            Case Else       '-- RightClick on grid proper
                     
                glRC = SendMessage(lWndHandle, WM_LBUTTONDOWN, 0, lParam)
                glRC = SendMessage(lWndHandle, WM_LBUTTONUP, 0, lParam)   ' Simulate click on grid
                gGridSetSelectRow grdLinePicked, lRow
        End Select
    End If

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CMRightMouseDown", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function


Private Sub HandleToolbarClick(sKey As String, Optional lSettingKey As Variant, Optional iFileType As Variant, Optional sFileName As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If
'********************************************************************
' Description:
'    This routine is the central routine for processing events when a
'    button on the Toolbar is clicked.
'********************************************************************
    Dim sWhere As String
    Dim iRet As Integer
    
    Select Case sKey
        Case kTbDefer, kTbPrint, kTbPreview
            If bIsValidGridData Then
                If PartialPickWarn Then
                    'Save any un-saved changes
                    moDMOrderGrid.Save
                    moDMLineGrid.Save
                    ProcessPrint sKey, lSettingKey, iFileType, sFileName
                End If
            End If
        Case kTbFinishExit
            If bIsValidGridData Then
                If PartialPickWarn Then
                    'Save any un-saved changes
                    moDMOrderGrid.Save
                    moDMLineGrid.Save
                    If iProcessFinish = kFinishProceeded Then
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                        Unload Me
                    End If
                End If
            End If
            
        Case kTbFinish
            If bIsValidGridData Then
                If PartialPickWarn Then
                    'Save any un-saved changes
                    moDMOrderGrid.Save
                    moDMLineGrid.Save
                    If iProcessFinish = kFinishProceeded Then
                        ClearForm
                        If lkuPickListNo.Visible = True Then
                            gbSetFocus Me, lkuPickListNo
                        End If
                    End If
                End If
            End If
            
        Case kTbCancelExit
            
            If mlPickListKey > 0 Then
                iRet = giSotaMsgBox(Me, moSysSession, kSOmsgConfirmCancelPick)
                
                If (iRet = kretYes) Then
                    If Not bProcessCancel Then
                        Exit Sub
                    End If
                Else
                    Exit Sub
                End If
            End If
            
            moClass.miShutDownRequester = kUnloadSelfShutDown
            Unload Me
            
        Case kTbCancel
            
            If mlPickListKey > 0 Then
                iRet = giSotaMsgBox(Me, moSysSession, kSOmsgConfirmCancelPick)
            
                If (iRet = kretYes) Then
                    If Not bProcessCancel Then
                        Exit Sub
                    End If
                    ClearForm
                Else
                    Exit Sub
                End If
            Else
                ClearForm
            End If
            
        Case kTbSave
            moSettings.ReportSettingsSaveAs
        Case kTbDelete
            moSettings.ReportSettingsDelete
        Case kTbHelp
            gDisplayFormLevelHelp Me
        Case Else
            tbrMain.GenericHandler sKey, Me, moDMLineGrid, moClass
    End Select
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandletoolbarClick", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function bIsValidGridData(Optional ByVal lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
 
Dim lRowCount           As Long
Dim iChoice             As Integer
Dim iNbrZeroQtyLines    As Integer
Dim iShipCommitStatus   As Integer
Dim iExtShipExists      As Integer
Dim iPacked             As Integer

    bIsValidGridData = False ' assume all data is invalid
    
    If grdLinePicked.MaxRows = 0 Then
        bIsValidGridData = True
        Exit Function
    End If
    
    'Make sure the GM CellChange event fires.
    mbLeaveCell = True
    FireGMCellChangeIfNeeded
    If Not mbLeaveCell Then
        Exit Function
    End If
    
    'Loop through Grid validating row by row
    If lRow = 0 Then
        For lRowCount = 1 To (glGridGetDataRowCnt(grdLinePicked))
            
            'No need to do validation if the shipment is in a commited state
            iShipCommitStatus = giGetValidInt(gsGridReadCell(grdLinePicked, lRowCount, kColLineShipmentCommitStatus))
            iExtShipExists = giGetValidInt(gsGridReadCell(grdLinePicked, lRowCount, kColLineExtShipmentExists))
            iPacked = giGetValidInt(gsGridReadCell(grdLinePicked, lRowCount, kColLinePacked))
            If iShipCommitStatus = 1 Or iExtShipExists = 1 Or iPacked = 1 Or miSecurityLevel = kSecLevelDisplayOnly Then
                bIsValidGridData = True
                Exit Function
            End If
                
            'No need to do validation on Qty or Distribution is QtyPicked is 0
            If (gdGetValidDbl(gsGridReadCell(grdLinePicked, lRowCount, kColLineQtyPicked)) <> 0) Then
                If bAmtToPickInvalid(lRowCount) Then
                    gGridSetTopRow grdLinePicked, lRowCount
                    grdLinePicked.LeftCol = kColLineQtyToPick 'Else scrolls back only to QtyPicked col
                    grdLinePicked_Click kColLineQtyPicked, lRowCount
                    gGridSetActiveCell grdLinePicked, lRowCount, kColLineQtyPicked
                    Exit Function
                End If
                If Not bIsValidDistribution(lRowCount) Then
                    gGridSetTopRow grdLinePicked, lRowCount
                    grdLinePicked.LeftCol = kColLineQtyToPick 'Else scrolls back only to QtyPicked col
                    grdLinePicked_Click kColLineQtyPicked, lRowCount
                    gGridSetActiveCell grdLinePicked, lRowCount, kColLineQtyPicked
                    Exit Function
                End If
            End If
        Next lRowCount
    Else
        'No need to do validation on Qty or Distribution is QtyPicked is 0
        If (gdGetValidDbl(gsGridReadCell(grdLinePicked, lRow, kColLineQtyPicked)) <> 0) Then
            If bAmtToPickInvalid(lRow) Then
                gGridSetTopRow grdLinePicked, lRow
                grdLinePicked.LeftCol = kColLineQtyToPick 'Else scrolls back only to QtyPicked col
                grdLinePicked_Click kColLineQtyPicked, lRow
                gGridSetActiveCell grdLinePicked, lRow, kColLineQtyPicked
                Exit Function
            End If
            If Not bIsValidDistribution(lRow) Then
                gGridSetTopRow grdLinePicked, lRow
                grdLinePicked.LeftCol = kColLineQtyToPick 'Else scrolls back only to QtyPicked col
                grdLinePicked_Click kColLineQtyPicked, lRow
                gGridSetActiveCell grdLinePicked, lRow, kColLineQtyPicked
                Exit Function
            End If
        End If
        moDMOrderGrid.SetRowDirty lRow
        moDMLineGrid.SetRowDirty lRow
    End If
    
    bIsValidGridData = True
    
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidGridData", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Function bAmtToPickInvalid(lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
Dim dQtyPicked As Double
Dim dQtyAvail As Double
Dim dQtyToPick As Double
Dim dQtyDist As Double
Dim lSubItemKey As Long
Dim iItemType As Integer
Dim iAllowDecimalQty  As Integer

    bAmtToPickInvalid = False

    dQtyPicked = gdGetValidDbl(gsGridReadCell(grdLinePicked, lRow, kColLineQtyPicked))
    dQtyAvail = gdGetValidDbl(gsGridReadCell(grdLinePicked, lRow, kColLineQtyAvail))
    dQtyToPick = gdGetValidDbl(gsGridReadCell(grdLinePicked, lRow, kColLineQtyToPick))
    dQtyDist = gdGetValidDbl(gsGridReadCell(grdLinePicked, lRow, kColLineQtyDist))
    lSubItemKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineSubItemKey))
    iItemType = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineItemType))
    iAllowDecimalQty = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineAllowDecimalQty))
    
    
    If iItemType > 4 Then 'Only perform this edit for those items that are stocked
        If (dQtyPicked > dQtyAvail + dQtyDist) And lSubItemKey = 0 Then
            If miAllowNegQty = 0 Then
                giSotaMsgBox Me, moSysSession, kSOmsgQtyToPickQtyAvail
                bAmtToPickInvalid = True
                Exit Function
            End If
        End If
        'Validate Decimal Qty Indicator
        If iAllowDecimalQty = 0 Then
            If (CLng(dQtyPicked) <> dQtyPicked) Then
                giSotaMsgBox Me, moClass.moSysSession, kSOmsgItemDontAllowDesQty
                bAmtToPickInvalid = True
                Exit Function
            End If
        End If
    End If
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bAmtToPickInvalid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function


Private Sub optFormat_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick optFormat(Index), True
    #End If
'+++ End Customizer Code Push +++

Dim i As Integer
    
    If Index = kPickFormatWave Then
    'Enable Summary and Zone Picking checkbox if Wave Option is selected
        chkWaveSummary.Enabled = True
        
        If mbWMIsLicensed Then
            cboZoneSort.Enabled = True
            lblZoneSort.Enabled = True
        Else
            cboZoneSort.Enabled = False
            lblZoneSort.Enabled = False
        End If
        
        If chkWaveSummary.Value = vbChecked Then
            miRptFormat = kPickRptTypeSummary
        Else
            miRptFormat = kPickRptTypeDetail
            
            If Not mbManualClick Then
            'Don't execute the following logic if option are not set by user
                For i = 0 To chkInclude.UBound
                    chkInclude(i).Enabled = True
                    chkInclude(i).Value = vbUnchecked
                Next i
                        
                For i = 0 To chkIncludeInDetail.UBound
                    chkIncludeInDetail(i).Enabled = True
                    chkIncludeInDetail(i).Value = vbUnchecked
                Next i
            End If
            
            fraSortBy.Enabled = True
            mbIsWaveSelected = True
            cboBinSort.Enabled = True
            lblBinSort.Enabled = True
        End If
    Else
        miRptFormat = kPickRptTypeOrder
        'Disable Summary and Zone Picking checkbox if Wave Option is not selected
        chkWaveSummary.Value = vbUnchecked
        chkWaveSummary.Enabled = False
        If cboZoneSort.Enabled Then
            cboZoneSort.ListIndex = 0
        End If
        cboZoneSort.Enabled = False
        lblZoneSort.Enabled = False
        cboBinSort.Enabled = False
        lblBinSort.Enabled = False
        miBinSortOrder = kBinID
        
        miZoneSortOrder = kNotUseZone
        fraSortBy.Enabled = False
        mbIsWaveSelected = False
        
        If Not mbManualClick Then
        'Don't execute the following logic if option are not set by user
            For i = 0 To chkInclude.UBound
                chkInclude(i).Enabled = True
                chkInclude(i).Value = vbUnchecked
            Next i

            For i = 0 To chkIncludeInDetail.UBound
                chkIncludeInDetail(i).Enabled = True
                chkIncludeInDetail(i).Value = vbUnchecked
            Next i
        End If
    End If
    
    If Not bInitSort(miRptFormat) Then Exit Sub
    
    SetDefaultSorts miRptFormat

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "optFormat_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub OptOrderShow_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick OptOrderShow(Index), True
    #End If
'+++ End Customizer Code Push +++
'**********************************************************************
' Description:
'    This routine toggles the controls where OptOrderShow is changed
'
'**********************************************************************

    OptLineShow(Index).Value = OptOrderShow(Index).Value
    
    grdOrderPicked.redraw = False
    
    If Index = kShowAll Then
        ShowPicked kOrder
    Else
        HidePicked kOrder
    End If
    
    grdOrderPicked.redraw = True
    
    'Refresh frame captions for display grids
    SetupRowsSelCaption kSetupCaptionForOrderGridOnly
    
    grdOrderPicked_Click kColOrdOrderNo, 1
    
    OptOrderShow(Index).Tag = OptOrderShow(Index).Value
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "OptOrderShow_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub OptLineShow_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick OptLineShow(Index), True
    #End If
'+++ End Customizer Code Push +++
'**********************************************************************
' Description:
'    This routine toggles the controls where OptOrderShow is changed
'
'**********************************************************************
    OptOrderShow(Index).Value = OptLineShow(Index).Value
    
    grdLinePicked.redraw = False
    
    If Index = kShowAll Then
        ShowPicked kLine
    Else
        HidePicked kLine
    End If
    
    grdLinePicked.redraw = True
    grdLinePicked_Click kColLineItem, 1
    
    'Refresh frame captions for display grids
    SetupRowsSelCaption kSetupCaptionForLineGridOnly
    
    OptLineShow(Index).Tag = OptLineShow(Index).Value

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "OptLineShow_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub tabPickOrder_Click(PreviousTab As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim sSQL As String

    Select Case tabPickOrder.Tab
    Case kTabLine
    
        fraOrdersToPick.Enabled = False
        fraLinesToPick.Enabled = True
        fraOptions.Enabled = False
        fraCustomizer.Enabled = False
        
        'If there is change in Linegrid caused by Ordergrid
        'Refresh the line grid.
        If mbLineGridRefreshNeeded Then
            'Reset the flag
            mbLineGridRefreshNeeded = False
            
            grdLinePicked.redraw = False
            
            'If the grid display option set to Show only
            'short pick, hide the non-short pick rows
            If OptLineShow(kShowShortOnly).Value = True Then
                HidePicked kLine
            End If
            
            'Refresh the Grid Data
            moDMLineGrid.Refresh
            
            'Clear the Form
            If grdLinePicked.MaxRows = 0 Then
                moGMLine.Clear
            End If
            
            'Update the frame caption of the grid
            SetupRowsSelCaption kSetupCaptionForLineGridOnly
            
            grdLinePicked.redraw = True
        End If
        
        grdLinePicked_Click kColLineItem, 1
        
    Case kTabOrder
    
        fraOrdersToPick.Enabled = True
        fraLinesToPick.Enabled = False
        fraOptions.Enabled = False
        fraCustomizer.Enabled = False
    
        'If there is change in Linegrid caused by Ordergrid
        'Refresh the line grid.
        If mbOrderGridRefreshNeeded Then
            'Reset the flag
            mbOrderGridRefreshNeeded = False
            
            grdOrderPicked.redraw = False
            
            'synchronize #tsoCreatePickHdrWrk to #tsoCreatePickWrk2
            'before refresh the order grid
            SyncDetailToHeader
    
            'If the grid display option set to Show only
            'short pick, hide the non-short pick rows
            If OptOrderShow(kShowShortOnly).Value = True Then
                HidePicked kOrder
            End If
    
            'Refresh the Grid Data
            moDMOrderGrid.Refresh
            
            'Update the frame caption of the grid
            SetupRowsSelCaption kSetupCaptionForOrderGridOnly
            
            grdOrderPicked.redraw = True
        End If
        
    Case kTabOptions
        
        fraOrdersToPick.Enabled = False
        fraLinesToPick.Enabled = False
        fraOptions.Enabled = True
        fraCustomizer.Enabled = False
        
    Case kTabCustomizer
    
        fraOrdersToPick.Enabled = False
        fraLinesToPick.Enabled = False
        fraOptions.Enabled = False
        fraCustomizer.Enabled = True
   
    End Select
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tabPickOrder_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    HandleToolbarClick Button
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If


Private Sub cmdSelectOrders_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdSelectOrders, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSelectOrders_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSelectOrders_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdSelectOrders, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSelectOrders_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDist_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdDist, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDist_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDist_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdDist, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDist_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDeleteAll_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdDeleteAll(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDeleteAll_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDeleteAll_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdDeleteAll(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDeleteAll_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdPickAll_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdPickAll(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPickAll_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdPickAll_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdPickAll(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPickAll_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdClearAll_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdClearAll(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdClearAll_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdClearAll_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdClearAll(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdClearAll_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdComponents_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdComponents, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdComponents_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdComponents_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdComponents, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdComponents_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdLinesOnHold_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdLinesOnHold, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdLinesOnHold_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdLinesOnHold_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdLinesOnHold, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdLinesOnHold_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdOrderPickLines_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdOrderPickLines, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdOrderPickLines_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdOrderPickLines_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdOrderPickLines, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdOrderPickLines_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdOrdersOnHold_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdOrdersOnHold, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdOrdersOnHold_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdOrdersOnHold_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdOrdersOnHold, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdOrdersOnHold_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub txtNavSubItem_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtNavSubItem, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavSubItem_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavSubItem_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtNavSubItem, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavSubItem_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavSubItem_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtNavSubItem, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavSubItem_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavSubItem_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtNavSubItem, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavSubItem_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavUOM_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtNavUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavUOM_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavUOM_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtNavUOM, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavUOM_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavUOM_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtNavUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavUOM_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavUOM_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtNavUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavUOM_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtlkuAutoDistBin_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtlkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtlkuAutoDistBin_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtlkuAutoDistBin_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtlkuAutoDistBin, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtlkuAutoDistBin_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtlkuAutoDistBin_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtlkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtlkuAutoDistBin_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtlkuAutoDistBin_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtlkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtlkuAutoDistBin_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMessageHeader_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtMessageHeader(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMessageHeader_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtMessageHeader(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMessageHeader_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtMessageHeader(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMessageHeader_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtMessageHeader(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPickListNo_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuPickListNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPickListNo_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPickListNo_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuPickListNo, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPickListNo_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPickListNo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuPickListNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPickListNo_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPickListNo_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuPickListNo, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPickListNo_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPickListNo_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuPickListNo, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPickListNo_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPickListNo_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuPickListNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPickListNo_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub TextLookup1_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange TextLookup1, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "TextLookup1_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub TextLookup1_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress TextLookup1, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "TextLookup1_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub TextLookup1_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus TextLookup1, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "TextLookup1_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub TextLookup1_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus TextLookup1, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "TextLookup1_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub TextLookup1_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(TextLookup1, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "TextLookup1_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub TextLookup1_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(TextLookup1, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "TextLookup1_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub TextLookup1_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked TextLookup1, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "TextLookup1_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub chkPrintRptSetting_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkPrintRptSetting, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPrintRptSetting_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPrintRptSetting_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkPrintRptSetting, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPrintRptSetting_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPrintRptSetting_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkPrintRptSetting, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPrintRptSetting_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInclude_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkInclude(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInclude_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInclude_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkInclude(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInclude_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkWaveSummary_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkWaveSummary, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkWaveSummary_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkWaveSummary_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkWaveSummary, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkWaveSummary_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkIncludeInDetail_Click(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkIncludeInDetail(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkIncludeInDetail_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkIncludeInDetail_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkIncludeInDetail(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkIncludeInDetail_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkIncludeInDetail_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkIncludeInDetail(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkIncludeInDetail_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkGenShip_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkGenShip, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkGenShip_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkGenShip_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkGenShip, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkGenShip_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkGenShip_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkGenShip, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkGenShip_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub OptLineShow_DblClick(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnDblClick OptLineShow(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "OptLineShow_DblClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub OptLineShow_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus OptLineShow(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "OptLineShow_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub OptLineShow_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus OptLineShow(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "OptLineShow_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub OptOrderShow_DblClick(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnDblClick OptOrderShow(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "OptOrderShow_DblClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub OptOrderShow_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus OptOrderShow(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "OptOrderShow_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub OptOrderShow_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus OptOrderShow(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "OptOrderShow_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optFormat_DblClick(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnDblClick optFormat(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optFormat_DblClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optFormat_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus optFormat(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optFormat_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optFormat_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus optFormat(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optFormat_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSetting_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboSetting, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSetting_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSetting_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboSetting, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSetting_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSetting_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboSetting, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSetting_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboBinSort_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboBinSort, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboBinSort_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboBinSort_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboBinSort, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboBinSort_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboBinSort_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboBinSort, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboBinSort_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboZoneSort_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboZoneSort, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboZoneSort_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboZoneSort_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboZoneSort, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboZoneSort_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboZoneSort_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboZoneSort, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboZoneSort_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then
Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If
Public Property Get MyApp() As Object
'+++ VB/Rig Skip +++
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
'+++ VB/Rig Skip +++
    Set MyForms = Forms
End Property

Private Sub SetTags()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim Index As Integer
    
    For Index = 0 To OptOrderShow.UBound
        OptOrderShow(Index).Tag = OptOrderShow(Index).Value
    Next Index
    
    For Index = 0 To OptLineShow.UBound
        OptLineShow(Index).Tag = OptLineShow(Index).Value
    Next Index
  
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetTags", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub ClearForm()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*************************************************************************
' Desc:    Cleans up form.
'*************************************************************************

    Dim i As Integer
    
    'Delete worktable
    DeleteWorkTables
    
    'Refresh the grids
    moDMOrderGrid.Refresh
    moDMLineGrid.Refresh
    
    'Refresh frame captions for display grids
    SetupRowsSelCaption kSetupCaptionForBothGrid

    'Clear other module level variables.
    miSOSelectedCount = 0
    miTOSelectedCount = 0
    miSOLSelectedCount = 0
    miTOLSelectedCount = 0
    mbLineGridRefreshNeeded = False
    mbOrderGridRefreshNeeded = False
    mlPickListKey = 0
    msPickListNo = ""
    miPrintCount = 0
    mbShipmentGenerated = False
    miEmptyBins = 0
    miEmptyRandomBins = 0
    miPickOrdQty = 0
    currPickListNo.Caption = ""
    mbLeaveCell = True
    mbCheckCanceled = False
    
    'Clear all the report set if the program is
    'in Reprint mode
    'Default the setting Wave Detail
    If miCallingTask = kReprintPickList Then
        If lkuPickListNo.Visible = True Then
            lkuPickListNo.ClearData
            lkuPickListNo.Tag = ""
            lkuPickListNo.EnabledText = True
        End If
    
        'mbManualClick = True
        optFormat(kPickFormatWave) = True
        chkWaveSummary.Value = vbUnchecked
        miRptFormat = kPickRptTypeDetail
        For i = 0 To chkInclude.UBound
            chkInclude(i).Enabled = True
            chkInclude(i).Value = vbUnchecked
        Next i
                
        For i = 0 To chkIncludeInDetail.UBound
            chkIncludeInDetail(i).Enabled = True
            chkIncludeInDetail(i).Value = vbUnchecked
        Next i
    Else
        currPickListNo.Caption = ""
        'mbManualClick = False
    End If
    
     'Default to the Order Tab
    tabPickOrder.Tab = kTabOrder
    
    'Disable command controls
    ResetMouse

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "ClearForm", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Private Function bIsValidDistribution(lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim sInvtTranID     As String
Dim lInvtTranKey    As Long
Dim iTrackMethod    As Integer
Dim iTrackByBin     As Integer
Dim lItemKey        As Long
Dim lWhseKey        As Long
Dim lOrigItemKey    As Long
Dim iConfirmed      As Integer
Dim dQtyToPick      As Double
Dim dQtyPicked      As Double
Dim dQtyDist        As Double
Dim iItemType       As Integer
Dim lSOUOMKey       As Long
Dim lUOMKey         As Long
Dim bResult         As Boolean
Dim lShipLineKey    As Long
Dim lLineTranType   As Long
Dim iWhseUseBin     As Integer
    
    bIsValidDistribution = False

    'Skip previously confirmed lines that are to be unconfirmed
    dQtyPicked = gdGetValidDbl(gsGridReadCell(grdLinePicked, lRow, kColLineQtyPicked))
    dQtyDist = gdGetValidDbl(gsGridReadCell(grdLinePicked, lRow, kColLineQtyDist))
    iItemType = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineItemType))
    lInvtTranKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineInvtTranKey))
    iWhseUseBin = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineWhseUseBin))
    
    'If previosly confirmed or user is cancelling pick updates
    If dQtyPicked = 0 Then
        bIsValidDistribution = True
        Exit Function
    End If
    
'    'If whse does not use bin, no distribution is required
'    If iWhseUseBin = 0 Then
'        bIsValidDistribution = True
'        Exit Function
'    End If
    
    'Only distribute Finished Goods, Raw Materials and Assembled Kits (needed for kit components that are not these three types)
    Select Case iItemType
        Case 5, 6, 8
        Case Else
            bIsValidDistribution = True
            Exit Function
    End Select

    CreateDistForm lRow
    
    iTrackMethod = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineTrackMeth))
    iTrackByBin = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineTrackQtyAtBin))
    dQtyToPick = gdGetValidDbl(gsGridReadCell(grdLinePicked, lRow, kColLineQtyToPick))
    lWhseKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineShipWhseKey))
    lSOUOMKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineOrderLineUOMKey))
    lUOMKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineShipUOMKey))
    lItemKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineSubItemKey))
    
    If lItemKey = 0 Then
        lOrigItemKey = 0
        lItemKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineItemKey))
    Else
        lOrigItemKey = glGetValidLong(gsGridReadCell(grdLinePicked, lRow, kColLineItemKey))
    End If

    If iTrackByBin = 0 And lWhseKey = 0 Then 'Item does not require distributions
        bIsValidDistribution = True
        Exit Function
    End If
    
    ' Item does require distributions but defaults can be used
    If (lUOMKey = lSOUOMKey) And (dQtyPicked = dQtyDist) _
        And (lOrigItemKey = 0) Then
        bIsValidDistribution = True
        Exit Function
    End If

'    'Check to see if the distribution is valid by comparing the quantities
'    'for QtyToPick and QtyPicked
'    If dQtyPicked > dQtyDist Then
'        'The the shipline is under distributed, bring up the Distribution form for
'        'use to correct
'        giSotaMsgBox Me, moClass.moSysSession, kSOmsgDistQtyNotEqualToQtyPick
'        If Not bCreateManualDistribution(Me, grdLinePicked, moItemDist, moDMLineGrid, lRow) Then
'            Exit Function
'        End If
'    End If
    
    If dQtyDist > dQtyPicked Then
        'The the shipline is over distributed, bring up the Distribution form for
        'use to correct
        giSotaMsgBox Me, moClass.moSysSession, kSOmsgQtyDistCantExceedQtyPick
        grdLinePicked_Click kColLineQtyToPick, lRow
        Exit Function
    End If

    bIsValidDistribution = True
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidDistribution", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function


Public Sub DeleteWorkTables()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
On Error GoTo ExpectedErrorRoutine

Dim sSQL        As String

    SetMouse

    On Error Resume Next
    'Delete pick line from work table for the lines deleted
    sSQL = "DELETE  #tsoCreatePickWrk2"
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        gClearSotaErr
    End If

   'Delete pick order from work table if all the lines for the order
   're deleted
    sSQL = "DELETE  #tsoCreatePickHdrWrk"
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        gClearSotaErr
    End If
    
  
   ResetMouse
   
       Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    Exit Sub

'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DeleteWorkTables", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
End Sub

Private Function bProcessCancel() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
        
Dim sWhere As String
Dim iUpdateCounterViolation As Integer

    bProcessCancel = False

        sWhere = ""
        If Not moProcessPickList.bDeleteShipLines(sWhere, kDeleteDistributionOnlyNo, iUpdateCounterViolation) Then
            Exit Function
        End If
                    
        If mlPickListKey > 0 And iUpdateCounterViolation = 0 Then
            If Not moProcessPickList.bDeletePickList(mlPickListKey) Then
               giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation  'kmsgimspGeneralReserveInvtTran
            End If
            mlPickListKey = 0
            currPickListNo.Caption = ""
            
            If Not bReversePickCompleteFlag Then
                Exit Function
            End If
        End If
        
        moDMOrderGrid.Refresh
        moDMLineGrid.Refresh
        
        'Refresh frame captions for display grids
        SetupRowsSelCaption kSetupCaptionForBothGrid
        
        HideNavs
        
    bProcessCancel = True

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bProcessCancel", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Function iProcessFinish() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
        
Dim iPrintFlag As Integer
Static bExitFormLoaded As Boolean
Dim lRetVal As Long
Dim sSQL As String
Dim lShipLineKey As Long
Dim lRow As Long

    iProcessFinish = kFinishCancelled
    
    If mlPickListKey = 0 Then
        iProcessFinish = kFinishProceeded
        Exit Function
    End If
    
    iPrintFlag = giGetValidInt(moAppDB.Lookup("PrintCount", "tsoPickList", "PickListKey = " & mlPickListKey))
    
    '--Update tsoPickList for the pick list being processed
    If Not moProcessPickList.bUpdShipLinePriorPrint(mlPickListKey) Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgCMUnexpectedSPError, "Update ShipLine"
        Exit Function
    End If
    
    '--Update tsoPickList.DeliveryMeth for the pick list being processed
    '--Update all other tsoPickList include settings as specified by individual controls
    '  in this document
    bSavePickList
    
    If Err.Number <> 0 Then
        ResetMouse
        Exit Function
    End If
            
    'If user didn't print or preview the pick list and they have the 'genereate shipments'
    'option set, they haven't generated their shipments yet and we must warn them and give them a chance
    'to perform the shipment generation.
    If iPrintFlag = 0 And chkGenShip.Value = vbChecked And Not mbShipmentGenerated And mlPickListKey > 0 Then
        If Not bExitFormLoaded Then
            Load frmExitCreatePick
            bExitFormLoaded = True
        End If
        
        If Not frmExitCreatePick Is Nothing Then
            frmExitCreatePick.Show vbModal
        End If
        
        If frmExitCreatePick.iExitOption = kReturnToPicking Then
            'User select to return to picking
            Exit Function
        Else
            'User selected to Exit without Shipments
        End If
    End If
    
    'If user has deleted all the pick lines for a pick list, upon exiting the task
    'delete the empty pick list
    If mlPickListKey > 0 Then
        If Not moProcessPickList.bDeletePickList(mlPickListKey) Then
           giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation  'kmsgimspGeneralReserveInvtTran
        End If
        mlPickListKey = 0
            
        If miCallingTask = kCreatePickList Then
            currPickListNo.Caption = ""
        Else
            lkuPickListNo.Text = ""
        End If
    End If
    
    'If user finished editing a pick list from create pick task, upon exiting change
    'tsoShipLine.PickingComplete for the shiplines in the pick list to true (1) so
    'other task can have access to it.
    If miCallingTask = kCreatePickList Then
        If Not bReversePickCompleteFlag Then
            Exit Function
        End If
    End If
    
    iProcessFinish = kFinishProceeded

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iProcessFinish", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Sub ProcessPrint(sKey As String, Optional lSettingKey As Variant, _
                        Optional iFileType As Variant, Optional sFileName As Variant, _
                        Optional sPrinterName As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
On Error GoTo ExpectedErrorRoutine

    Dim sSQL As String
    Dim lResult As Long
    Dim bIsBTOKitList As Boolean
    Dim iBTOKitExists As Integer
    
    If mlPickListKey = 0 Then Exit Sub
    
    SetMouse
    
    'Update tsoShipLine and tsoShipLineDist for the pick list being processed
    If Not moProcessPickList.bUpdShipLinePriorPrint(mlPickListKey) Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgCMUnexpectedSPError, "Update ShipLine"
        Exit Sub
    End If
   
    '--Update tsoPickList.DeliveryMeth for the pick list being processed
    '--Update all other tsoPickList include settings as specified by individual controls
    '  in this document
    bSavePickList
    
    If Err.Number <> 0 Then
        ResetMouse
        Exit Sub
    End If
    
    'Now check whether the Generate shipment option is checked
    If chkGenShip.Value = vbChecked And Not mbShipmentGenerated Then
    'Yes, the option is checked
        If moProcessPickList.bOverrideSecEvent(kSecEventGenerateShipment) Then
            If Not bProcessShipment Then
                mbShipmentGenerated = False
                ResetMouse
                Exit Sub
            End If
        Else
            giSotaMsgBox Me, moClass.moSysSession, kSOmsgWarnNoShipmtGenerated
            ResetMouse
            Exit Sub
        End If
    End If
    
    'Pupulate the report data and build the reports
    If bBuildReport(sKey, lSettingKey, iFileType, sFileName, sPrinterName) Then
        'Refresh the grid
        grdOrderPicked.redraw = False
        grdLinePicked.redraw = False
        moDMOrderGrid.Refresh
        moDMLineGrid.Refresh
        grdOrderPicked.redraw = True
        grdLinePicked.redraw = True

        'Refresh frame captions for display grids
        SetupRowsSelCaption kSetupCaptionForBothGrid
    End If
    
    ResetMouse

    Exit Sub
    
ExpectedErrorRoutine:

    If Me.Enabled = False Then
        Me.Enabled = True
    End If
  
    ResetMouse
    'Sub Allow to raise error

'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ProcessPrint", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function bProcessShipment() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
        
Dim sWhere As String
Dim sSQL As String
Dim sWhseDisAllowShipFromPick As String
Dim rs As Object
Dim iRet As Integer
Dim bResult As Boolean
Dim sGeneShipConfirmation As String

    bProcessShipment = False
    
    'Check whether there are any warehouse disallow immediate shipment
    'from picking process
    sSQL = "SELECT DISTINCT ShipWhseID From #tsoCreatePickWrk2 WHERE "
    sSQL = sSQL & " AllowImmedShipFromPick = 0 And ShipWhseID IS NOT NULL"
    
    Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If Not rs Is Nothing Then
        'build the string for the offending warehouse
        If Not rs.IsBOF Then
            rs.MoveFirst
        End If
        Do While Not rs.IsEOF
            If Len(Trim(sWhseDisAllowShipFromPick)) = 0 Then
                sWhseDisAllowShipFromPick = gsGetValidStr(rs.Field("ShipWhseID"))
            Else
                sWhseDisAllowShipFromPick = sWhseDisAllowShipFromPick & ", " & gsGetValidStr(rs.Field("ShipWhseID"))
            End If
            rs.MoveNext
        Loop
    End If
    rs.Close
    Set rs = Nothing
    
    If Len(Trim(sWhseDisAllowShipFromPick)) > 0 Then
        ' if Setting name already exists, ask if user wants to update
        iRet = giSotaMsgBox(Me, moClass.moSysSession, kSOmsgWhseDisallowShipFromPick, sWhseDisAllowShipFromPick)
        
        'if yes, then update existing setting
        If iRet = kretCancel Then
            'go back to cancel pick
            Exit Function
        End If
    End If
    
    'Go ahead generate the shipment
    'Call the GenShip API
    sbrMain.Status = SOTA_SB_BUSY
    
    'sGeneShipConfirmation = "***Generating Shipments***"
    sbrMain.Message = "***Generating Shipments***"
    If Not moProcessPickList.bGenerateShipment(mlSessionID) Then
        'Message
        sbrMain.Message = ""
        sbrMain.Status = SOTA_SB_START
        Exit Function
    Else
    
        'update the shipment generate flag
        mbShipmentGenerated = True
        
        'Refresh the grids
        grdOrderPicked.redraw = False
        grdLinePicked.redraw = False
        moDMOrderGrid.Refresh
        moDMLineGrid.Refresh
        grdOrderPicked.redraw = True
        grdLinePicked.redraw = True
        
        'Update the status bar
        sbrMain.Message = ""
        sbrMain.Status = SOTA_SB_START
        

    End If
    
    bProcessShipment = True

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        If Not rs Is Nothing Then Set rs = Nothing
        gSetSotaErr Err, sMyName, "bProcessShipment", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Property Get oReport() As clsReportEngine
'+++ VB/Rig Skip +++
    Set oReport = moReport
End Property

Public Property Set oReport(oClass As clsReportEngine)
'+++ VB/Rig Skip +++
    Set moReport = oClass
End Property

Public Function lInitializeReport(ModuleID As String, DefaultReport As String, ProjectName As String) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iRptType As Integer

    lInitializeReport = -1

    Set moPrinter = Printer
    Set moSotaObjects = New Collection
    Set moOptions = New clsOptions
    Set moReport = New clsReportEngine
    Set moDDData = New clsDDData
    Set moSettings = New clsSettings

    If Not moDDData.lInitDDData(sRealTableCollection, moAppDB, moAppDB, msCompanyID) = kSuccess Then
        lInitializeReport = kmsgCantLoadDDData
        Exit Function
    End If

'add maprealtowork
    moReport.MapRealToWork "tsoSalesOrder", "TranNoRelChgOrd", sWorkTableCollection(1), "OrderTranNo"


    If Not moDDData.lDDBuildData(App.EXEName) = kSuccess Then
        lInitializeReport = kmsgCantLoadDDData
        Exit Function
    End If

    moReport.AppOrSysDB = kAppDB

    If (moReport.lInitReport(ModuleID, DefaultReport, Me, moDDData, , False) = kFailure) Then
        lInitializeReport = kmsgFatalReportInit
        Exit Function
    End If
    
    'Setup the default setting if no setting is saved
    optFormat(kPickFormatWave) = True
    mbIsWaveSelected = True
    miRptFormat = kPickRptTypeDetail
    miBinSortOrder = kBinID
    miZoneSortOrder = kNotUseZone

    'Setup Option for Form save settings
    SetupOptions

    mbManualClick = True
    moSettings.Initialize Me, cboSetting, moSort, , moOptions, True, tbrMain, sbrMain, "sozdd101", ktskCreatePick, True
    mbManualClick = False

    '-- CUSTOMIZE:  Add all controls on Opt                   ions page for which you will want to save settings
    '-- to the moOptions collection, using its Add method.
    If (Trim$(cboSetting.Text) <> "(None)") And cboSetting.Visible = True Then
        mbLoadSetting = True
        moSettings.bLoadReportSettings True
        mbLoadSetting = False
    End If

    mlSessionID = moReport.SessionID

    lInitializeReport = 0
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lInitializeReport", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub SetupReports()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim sRptProgramName As String   'Stores base filename for identification purposes.
    Dim sModule As String           'The report's module: AR, AP, SM, etc.
    Dim sDefaultReport As String    'The default .RPT file name
    Dim lRetVal As Long

    '-- Setup RPT defaults
    sRptProgramName = "SOZDD101"
    sModule = "SO"
    sDefaultReport = "sozrd002.rpt"

    Set sRealTableCollection = New Collection
    With sRealTableCollection
         .Add "tsoShipLineDist"
    End With

    Set sWorkTableCollection = New Collection
    With sWorkTableCollection
        .Add "tsoPickListHdrWrk"    'changed from DetWrk
        .Add "tsoPickListDistWrk"
        .Add "tciAddressWrk"
        .Add "tsoPickListCompListWrk"
    End With

    lRetVal = lInitializeReport(sModule, sDefaultReport, sRptProgramName)
    If (lRetVal <> 0) Then
        moReport.ReportError lRetVal
        Exit Sub
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:

        gSetSotaErr Err, sMyName, "SetupReports", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub lkuPickListNo_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuPickListNo, True
    #End If
'+++ End Customizer Code Push +++
    
Dim sPickListNo As String
    
    'Is the PickList No populated?
    If Len(Trim$(lkuPickListNo.Text)) = 0 Then
        'No, the PickList No is NOT populated.
        gbSetFocus Me, lkuPickListNo
        Exit Sub
    Else
        If bIsValidPickListNo Then lkuPickListNo.EnabledText = False
    End If

''+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPickListNo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function bIsValidPickListNo() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iRetVal As Integer
    Dim bValid As Boolean
    Dim iRet As Integer
    Dim iShipmentGenerated As Integer

    bIsValidPickListNo = False

    If lkuPickListNo.Text = lkuPickListNo.Tag Then
        bIsValidPickListNo = True
        Exit Function
    End If
    
    '-- Zero-fill the Pick List Number if appropriate
    lkuPickListNo = sZeroFillNumber(lkuPickListNo.MaskedText, lkuPickListNo.MaxLength)
        
    'Look up the PickListKey
    mlPickListKey = glGetValidLong(moAppDB.Lookup("PickListKey", "tsoPickList", _
                       "CompanyID = " & gsQuoted(msCompanyID) & _
                       " AND PickListNo = " & gsQuoted(lkuPickListNo.Text) & _
                       " AND PickListKey NOT IN (SELECT COALESCE(PickListKey, -1) FROM tsoShipLine WITH (NOLOCK) WHERE PickingComplete = 0)"))

                         
    If (mlPickListKey = 0) Then
        '-- Pick List Is Invalid
        giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidEntity, "PickList No", lkuPickListNo.Text
        lkuPickListNo.Text = lkuPickListNo.Tag
        lkuPickListNo.EnabledText = True
        gbSetFocus Me, lkuPickListNo
        Exit Function
    Else
        '-- Pick List is valid
        'Update the PickListNo value
        lkuPickListNo.Tag = lkuPickListNo.Text
        
        'Perform key change event to update the value for report options controls
        
        'Since in this routine we loading the saved setting for an existing picklist
        'list, we need to only load the saved setting value to the bond controls
        'but skip any logic that will be fired if user had click on them
        mbManualClick = True
        If bPerformPickListKeyChange(mlPickListKey) Then
            'Now populate the work tables for the data grids
            With moAppDB
                .SetInParam msCompanyID
                .SetInParam mlPickListKey
                .SetInParam kLoadingTaskReprintPickList
                .SetInParam gsFormatDateToDB(moClass.moSysSession.BusinessDate)
                .SetOutParam iShipmentGenerated
                .SetOutParam iRetVal
                .ExecuteSP "spsoLoadPickList"
                iShipmentGenerated = giGetValidInt(.GetOutParam(5))
                iRetVal = giGetValidInt(.GetOutParam(6))
                .ReleaseParams
            End With
            
            If iRetVal <> 1 Or Err.Number <> 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgProc, "spsoLoadPickList"
                ResetMouse
                Exit Function
            End If
            
            If iShipmentGenerated = 1 Then
                mbShipmentGenerated = True
            Else
                mbShipmentGenerated = False
            End If
            
            'Refresh the Grid in the main form
            grdOrderPicked.redraw = False
            grdLinePicked.redraw = False
            moDMOrderGrid.Refresh
            moDMLineGrid.Refresh
            
            'Refresh frame captions for display grids
            SetupRowsSelCaption kSetupCaptionForBothGrid
            
            grdOrderPicked.redraw = True
            grdLinePicked.redraw = True
        End If
        mbManualClick = False
    End If
    
    'lkuPickListNo.Tag = lkuPickListNo.Text
    
    bIsValidPickListNo = True
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidPickListNo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function bPerformPickListKeyChange(ByVal lPickListKey As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim iRet As Integer

    bPerformPickListKeyChange = False
    
    '-- Set the keyvalue for the moDMPickListHeader
    moDMPickListHeader.SetColumnValue "PickListKey", lPickListKey
    '-- Refresh the DM data
    
    iRet = moDMPickListHeader.KeyChange()

    If (iRet = kDmNotAllowed) Then
        '-- Pick List Is Invalid
        giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidEntity, "PickList No", lkuPickListNo.Text
        Exit Function
    End If

    If (moDMPickListHeader.State = kDmStateEdit) Then
        '-- Set the data manager object and all children to clean
        '-- (the header object was becoming dirty somewhere in the display process)
        moDMPickListHeader.SetDirty False, True
    End If
    
    'We need to manually set the variable values that is not directly bond to
    'moDMPickListHeader
    miDeliveryMeth = giGetValidInt(moDMPickListHeader.GetColumnValue("DeliveryMeth"))
    msPickListNo = gsGetValidStr(moDMPickListHeader.GetColumnValue("PickListNo"))
    miPrintCount = giGetValidInt(moDMPickListHeader.GetColumnValue("PrintCount"))
    
    'If mlTaskID = ktskNewReprintPickList Then
        miRptFormat = giGetValidInt(moDMPickListHeader.GetColumnValue("RptFormat"))
        miBinSortOrder = giGetValidInt(moDMPickListHeader.GetColumnValue("BinSortOrder"))
        miZoneSortOrder = giGetValidInt(moDMPickListHeader.GetColumnValue("SortByZone"))
    
        Select Case miRptFormat
            Case kPickRptTypeSummary
                    optFormat(kPickFormatWave).Value = True
                    chkWaveSummary.Value = vbChecked
            Case kPickRptTypeDetail
                    optFormat(kPickFormatWave).Value = True
                    chkWaveSummary.Value = vbUnchecked
            Case kPickRptTypeOrder
                    optFormat(kPickFormatOrder).Value = True
                    chkWaveSummary.Value = vbUnchecked
        End Select

        cboBinSort.ListIndex = Max(giListIndexFromItemData(cboBinSort, miBinSortOrder), 0)
        cboZoneSort.ListIndex = Max(giListIndexFromItemData(cboZoneSort, miZoneSortOrder), 0)

    'End If
   
    bPerformPickListKeyChange = True


'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bPerformPickListKeyChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function sZeroFillNumber(sNum As String, iLen As Integer) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sTempNum As String

    sTempNum = Trim$(sNum)
    
    '-- Make sure it's numeric before zero filling
    If IsNumeric(sTempNum) Then
        '-- Use the mask to zero-fill the Voucher Number
        sTempNum = Format$(sTempNum, String$(iLen, "0"))
    End If

    sZeroFillNumber = sTempNum
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sZeroFillNumber", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Private Function bReversePickCompleteFlag() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim lRetVal As Long

    On Error Resume Next
    
    bReversePickCompleteFlag = False

    With moAppDB
        .SetOutParam lRetVal
        .ExecuteSP "spsoReversePickCompleteFlag"
        lRetVal = glGetValidLong(.GetOutParam(1))
        .ReleaseParams
    End With
    
    If lRetVal <> 1 Or Err.Number <> 0 Then
        giSotaMsgBox moClass, moClass.moSysSession, kmsgProc, "spsoReversePickCompleteFlag"
        Exit Function
    End If

    'Remove the logical lock
    If Not bRemoveLogicalLock Then
        Exit Function
    End If
    
    bReversePickCompleteFlag = True
                            
'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bReversePickCompleteFlag", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub DeleteLockData()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim sSQL As String

    'Delete the deleted lines from the lock table
    sSQL = "Delete tsoSOLineDistPick"
    sSQL = sSQL & " WHERE SOLineDistKey IN"
    sSQL = sSQL & " (SELECT LineDistKey FROM #tsoCreatePickWrk2"
    sSQL = sSQL & " WHERE #tsoCreatePickWrk2.TranType = " & kTranTypeSOSO
    sSQL = sSQL & " AND #tsoCreatePickWrk2.LineDistKey IS NOT NULL)"
    sSQL = sSQL & " AND SPID = " & miSPID
    
    moAppDB.ExecuteSQL sSQL
    
    sSQL = sSQL & " Delete timTrnsfrOrdLinePick"
    sSQL = sSQL & " WHERE TrnsfrOrderLineKey IN"
    sSQL = sSQL & " (SELECT LineKey FROM #tsoCreatePickWrk2"
    sSQL = sSQL & " WHERE TranType = " & kTranTypeIMTR
    sSQL = sSQL & " AND LineKey IS NOT NULL)"
    sSQL = sSQL & " AND SPID = " & miSPID
    
    moAppDB.ExecuteSQL sSQL
                            
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DeleteLockData", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function bSavePickList() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iRptType As Integer
    
    bSavePickList = False
        
    moDMPickListHeader.SetColumnValue "RptFormat", miRptFormat
    moDMPickListHeader.SetColumnValue "PrintCount", miPrintCount
    moDMPickListHeader.SetColumnValue "DeliveryMeth", miDeliveryMeth
    moDMPickListHeader.SetColumnValue "BinSortOrder", miBinSortOrder
    moDMPickListHeader.SetColumnValue "SortByZone", miZoneSortOrder
    
    'moDMPickListHeader.SetDirty True, False
    
    If (moDMPickListHeader.Save(True) = kDmSuccess) Then
        bSavePickList = True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSavePickList", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub SyncDetailToHeader()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim sSQL As String

    'synchronize #tsoCreatePickHdrWrk to #tsoCreatePickWrk2
    'before refresh the order grid
    sSQL = "UPDATE  #tsoCreatePickHdrWrk "
    sSQL = sSQL & " SET SubstiStatus = tmp.SubstiStatus,"
    sSQL = sSQL & " PickStatus = tmp.PickStatus,"
    sSQL = sSQL & " ShortPick = tmp.ShortPick,"
    sSQL = sSQL & " Status = tmp.Status"
    sSQL = sSQL & " FROM    #tsoCreatePickHdrWrk"
    sSQL = sSQL & " JOIN    (SELECT MAX(COALESCE(SubstiStatus,'')) SubstiStatus,"
    sSQL = sSQL & " MAX(COALESCE(PickStatus,'0'))PickStatus,"
    sSQL = sSQL & " MAX(StatusCustShipCompleteViolation) + MAX(StatusDistWarning) + MAX(StatusItemShipCompLeteViolation) "
    sSQL = sSQL & "   + MAX(StatusDistQtyShort) + MAX(StatusOverShipment) Status,"
    sSQL = sSQL & " MAX(COALESCE(ShortPick, '')) ShortPick,"
    sSQL = sSQL & " TranType , OrderKey"
    sSQL = sSQL & " FROM #tsoCreatePickWrk2"
    sSQL = sSQL & " GROUP BY OrderKey, TranType) tmp"
    sSQL = sSQL & " ON  #tsoCreatePickHdrWrk.TranType = tmp.TranType"
    sSQL = sSQL & " AND #tsoCreatePickHdrWrk.TranKey = tmp.OrderKey"
    
    moAppDB.ExecuteSQL sSQL
        
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SyncDetailToHeader", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
        
End Sub

Public Function bBuildReport(ByVal sKey As String, Optional lSettingKey As Variant, _
            Optional iFileType As Variant, Optional sFileName As Variant, _
            Optional sPrinterName As Variant) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim iBTOKitExists As Integer
Dim bIsBTOKitList As Boolean
Dim lResult As Long
Dim iReprintFlag As Integer

    bBuildReport = False
    
    If miPrintCount > 0 Or miCallingTask = kReprintPickList Then
        iReprintFlag = 1
    Else
        iReprintFlag = 0
    End If

    
    'First build the Main Pick List Report
    bIsBTOKitList = False
    lResult = lStartReport(sKey, Me, moClass, moReport, miRptFormat, iReprintFlag, miNbrDecPlaces, _
                    miMaxSerialNbrsOnPickList, mlSessionID, bIsBTOKitList, _
                    iBTOKitExists, mlPickListKey, miZoneSortOrder, miBinSortOrder, lSettingKey, iFileType, sFileName, sPrinterName, mbIsWaveSelected)

    'If there is no data to be printed, exit
    If lResult = kNoRepData Then
        bBuildReport = True
        Exit Function
    End If
    
    'If user has check the Include BTO Kit List, then build the BTO Kit List
    If chkInclude(kInclBTOKitLine).Value = vbChecked And iBTOKitExists > 0 Then
        bIsBTOKitList = True
        lResult = lStartReport(sKey, Me, moClass, moReport, miRptFormat, iReprintFlag, miNbrDecPlaces, _
                    miMaxSerialNbrsOnPickList, mlSessionID, bIsBTOKitList, _
                    iBTOKitExists, lPickListKey, miZoneSortOrder, miBinSortOrder, lSettingKey, iFileType, sFileName, sPrinterName, mbIsWaveSelected)
    End If

    'Update print flag in tsoPickList table
    If lResult = kSuccess And miCallingTask = kReprintPickList Then
        miPrintCount = miPrintCount + 1
        moAppDB.ExecuteSQL ("UPDATE tsoPickList Set PrintCount = (COALESCE(PrintCount,0) + 1)  WHERE PickListKey = " & mlPickListKey)
    End If
        
    bBuildReport = True

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bBuildReport", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function bSetupRptSetting(ByVal lRptSettingKey As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    bSetupRptSetting = False
  
    'Set the combo index to the value designated
    cboSetting.ListIndex = Max(giListIndexFromItemData(cboSetting, lRptSettingKey), 0)
    
    'Force a combo box click to get all the related control set up
    moSettings.gbSkipClick = False
    cboSetting_Click
    
    bSetupRptSetting = True


'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSetupRptSetting", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub FireGMCellChangeIfNeeded()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'If the user clicks on the toolbar control immediately after typing a value on a
    'grid cell, sometimes the GM CellChange event does not fire.  Make sure the event
    'fires if it hasn't.  (Seems like the issue only applys for the toolbar control.
    'If any other control is clicked, the event still fires.)
    
    'The mbCellChangeFired variable is set to True when the event is called.
    'It is set to False on the Grid_EditChange event or when the user starts changes
    'the value on a cell.
    
    If mbCellChangeFired = False Then
        moGMLine_CellChange grdLinePicked.ActiveRow, grdLinePicked.ActiveCol
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FireGMCellChangeIfNeeded", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub ForceNavigation()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    lkuPickListNo_LostFocus
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ForceNavigation", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub SetupSortCombos()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim arrStaticValues()   As String
    Dim sString             As String
    Dim sString1             As String
    Dim i                   As Integer
    
    'Build Static List for the Zone Sort Combo Box
    ReDim arrStaticValues(3)
    sString = gsBuildString(kSONone, moClass.moAppDB, moClass.moSysSession)
    arrStaticValues(1) = sString
    
    'sString1 = gsBuildString(kSOWhseZone, moClass.moAppDB, moClass.moSysSession)
    
    sString = gsBuildString(kIMID, moClass.moAppDB, moClass.moSysSession)
    arrStaticValues(2) = sString
    
    sString = gsBuildString(kIMWaveOrder, moClass.moAppDB, moClass.moSysSession)
    arrStaticValues(3) = sString
    
    For i = 0 To 2
        cboZoneSort.AddItem arrStaticValues(i + 1)
        cboZoneSort.ItemData(i) = i
    Next i
    cboZoneSort.ListIndex = 0
    
    'Build Static List for the Bin Sort Combo Box
    ReDim arrStaticValues(3)

'    sString = gsBuildString(kIMBinID, moClass.moAppDB, moClass.moSysSession)
'    arrStaticValues(1) = sString
'
'    sString = gsBuildString(kSOSRegBinLocation, moClass.moAppDB, moClass.moSysSession)
'    arrStaticValues(2) = sString
'
'    sString = gsBuildString(kBinSortOrderCol, moClass.moAppDB, moClass.moSysSession)
'    arrStaticValues(3) = sString
    
    sString = gsBuildString(kIMID, moClass.moAppDB, moClass.moSysSession)
    arrStaticValues(1) = sString
    
    sString = gsBuildString(kSOLocation, moClass.moAppDB, moClass.moSysSession)
    arrStaticValues(2) = sString
    
    sString = gsBuildString(kIMWaveOrder, moClass.moAppDB, moClass.moSysSession)
    arrStaticValues(3) = sString
   
    For i = 0 To 2
        cboBinSort.AddItem arrStaticValues(i + 1)
        cboBinSort.ItemData(i) = i + 1
    Next i
    cboBinSort.ListIndex = 0

    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupSortCombos", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub CleanupLogicalLocks()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*************************************************************************
' Desc:   This routine will clean up any orphaned ship line due to any
'         abnormal exiting from the Create Pick List Task which left the
'         picking complete flag to be false.  By reversing the picking complete
'         flag to the correct stage (true) the ship lines can be edited
'         again by the reprint pick list task or the cancel pick task.
'*************************************************************************
    
Dim lRetVal As Long
    
    On Error Resume Next
    With moAppDB
        .SetInParam LOCKTYPE_PICKLIST_RECOVERY
        .SetOutParam lRetVal
        .ExecuteSP "spsmLogicalLockCleanup"
        lRetVal = glGetValidLong(.GetOutParam(2))
        .ReleaseParams
    End With
    
    If lRetVal <> 1 Or Err.Number <> 0 Then
        giSotaMsgBox moClass, moClass.moSysSession, kmsgProc, "spsmLogicalLockCleanup"
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CleanupLogicalLocks", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Private Function bRemoveLogicalLock() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*************************************************************************
' Desc:   This routine will remove the logical lock created by the
'         create pick process
'*************************************************************************
    
Dim lRetVal As Long

    bRemoveLogicalLock = False
    
    On Error Resume Next
    With moAppDB
        .SetInParam mlLogicalLockKey
        .SetOutParam lRetVal
        .ExecuteSP "spsmLogicalLockRemove"
        lRetVal = glGetValidLong(.GetOutParam(2))
        .ReleaseParams
    End With
    
    If lRetVal <> 1 Or Err.Number <> 0 Then
        giSotaMsgBox moClass, moClass.moSysSession, kmsgProc, "spsmLogicalLockRemove"
        Exit Function
    End If
    
    bRemoveLogicalLock = True

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bRemoveLogicalLock", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Sub GetPickListOption()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*************************************************************************
' Desc:   This routine will populate #PickListOption
'         with the options specified in the Create Pick Form
'*************************************************************************
    
Dim sSQL As String
    
    On Error Resume Next
    sSQL = "TRUNCATE TABLE #PickListOption"
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        giSotaMsgBox moClass, moClass.moSysSession, kmsgUnexpectedOperation
        Exit Sub
    End If
    
    sSQL = "INSERT INTO #PickListOption ("
    sSQL = sSQL & "BinSortOrder,"
    sSQL = sSQL & "InclAbbrShipToAddr,"
    sSQL = sSQL & "InclBinLoc,"
    sSQL = sSQL & "InclBTOKitList,"
    sSQL = sSQL & "InclComments,"
    sSQL = sSQL & "InclKitCompInd,"
    sSQL = sSQL & "InclNonStock,"
    sSQL = sSQL & "InclPriority,"
    sSQL = sSQL & "InclShipCompIeteInd,"
    sSQL = sSQL & "InclShipDate,"
    sSQL = sSQL & "InclShipMethod,"
    sSQL = sSQL & "InclShipToAddr,"
    sSQL = sSQL & "InclSubInd,"
    sSQL = sSQL & "RptFormat,"
    sSQL = sSQL & "SessionID,"
    sSQL = sSQL & "SortByZone)"
    sSQL = sSQL & " SELECT "
    sSQL = sSQL & miBinSortOrder & ","
    sSQL = sSQL & chkInclude(kInclAbbrvShipToAddr).Value & ","
    sSQL = sSQL & chkIncludeInDetail(kInclDetlBinLocation).Value & ","
    sSQL = sSQL & chkInclude(kInclBTOKitLine).Value & ","
    sSQL = sSQL & chkInclude(kInclExtLineComm).Value & ","
    sSQL = sSQL & chkIncludeInDetail(kInclDetlKitComp).Value & ","
    sSQL = sSQL & chkInclude(kInclNonInvt).Value & ","
    sSQL = sSQL & chkIncludeInDetail(kInclDetlPriority).Value & ","
    sSQL = sSQL & chkIncludeInDetail(kInclDetlShipComplete).Value & ","
    sSQL = sSQL & chkIncludeInDetail(kInclDetlShipDate).Value & ","
    sSQL = sSQL & chkIncludeInDetail(kInclDetlShipVia).Value & ","
    sSQL = sSQL & chkInclude(kInclShipToAddr).Value & ","
    sSQL = sSQL & chkIncludeInDetail(kInclDetlSubstitution).Value & ","
    sSQL = sSQL & miRptFormat & ","
    sSQL = sSQL & "NULL,"
    sSQL = sSQL & miZoneSortOrder
    
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        giSotaMsgBox moClass, moClass.moSysSession, kmsgUnexpectedOperation
        Exit Sub
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetPickListOption", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function PartialPickWarn() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sWhere      As String
    Dim sOrderNo    As String
    
    PartialPickWarn = True
    
    'moAppDB.ExecuteSQL "INSERT INTO tsoCreatePickWrk SELECT * FROM #tsoCreatePickWrk2"
    sWhere = "TranType = " & kSOTranType
    sWhere = sWhere & " AND (CHARINDEX('C',Status) <> 0 OR CHARINDEX('I',Status) <> 0)"
    sOrderNo = gsGetValidStr(moAppDB.Lookup("MIN(TranNoRelChngOrd)", "#tsoCreatePickWrk2", sWhere))
    If sOrderNo <> "" Then
        If giSotaMsgBox(Me, moClass.moSysSession, kSOPartialPickWarn, "Sales Order " & sOrderNo) = vbNo Then
            PartialPickWarn = False
        End If
    End If

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PartialPickWarn", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

