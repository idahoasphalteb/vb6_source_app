Attribute VB_Name = "basCreatePick"

'**********************************************************************
'     Name: basCreatePick
'     Desc: Main Module of this Object containing "Sub Main ()".
' Original: 04/01/04
'     Mods: 10/13/10 RS - lStartReport - redo errorhandling so any fail will call std error routine,
'                           so errors won't be hidden. remove WA logic 7.4
'**********************************************************************
Option Explicit

    Public mfrmMain            As Form
        
    Public Enum ExitOption
        kExitWithoutShipment = 1
        kReturnToPicking = 2
    End Enum
    
    Public Enum OptionsInclude
        iInclExtLineComm = 0
        iInclShipToAddr = 1
        iInclAbbrvShipToAddr = 2
        iInclBTOKitLine = 3
        iInclNonInvt = 4
    End Enum
    
    Public Enum CallingTask
        kCreatePickList = 1
        kReprintPickList = 2
    End Enum

    Public Enum OptionsIncludeInDetail
        iInclDetlShipDate = 0
        iInclDetlShipComplete = 1
        iInclDetlSubstitution = 2
        iInclDetlKitComp = 3
        iInclDetlBinLocation = 4
        iInclDetlPriority = 5
        iInclDetlShipVia = 6
    End Enum
    
    Public Enum OptionsBinSortOrder
        kNotSortByBin = 0
        kBinID = 1
        kBinLocation = 2
        kBinSortOrder = 3
    End Enum
    
    Public Enum OptionsZoneSortOrder
        kNotUseZone = 0
        kSortByZoneID = 1
        kSortByZoneSortOrder = 2
    End Enum
    
    'Public Constants for pick report format
    Public Const kPickRptTypeSummary   As Integer = 1
    Public Const kPickRptTypeDetail    As Integer = 2
    Public Const kPickRptTypeOrder     As Integer = 3

    Public Enum SOOverShipmentPolicy
        kDisAllow = 0
        kAllow = 1
        kAllowWithWarning = 2
    End Enum
        
    Public Enum OrderLineCallingForm
        kPickLineMain = 1
        kPickLineOnHold = 2
    End Enum
        
    Public Type OrderDetail
        TranKey             As Long
        TranNo              As String
        TranType            As Long
        TranTypeID          As String
        ShipToWhseKey       As Long
        ShipToWhseID        As String
        ShipToAddrKey       As Long
        ShipToAddrID        As String
        ShipToAddrName      As String
    End Type

    '-- Typedef to Store Sort Grid Settings
    Public Type SortSetting
        Row                 As Long
        sTableColumnName    As String
        sDescription        As String
        iSubtotal           As Integer
        iPageBreak          As Integer
        iSortOrder          As Integer
        sColumnName         As String
        iSeqNo              As Integer
        sTableName          As String
        sParentTableName    As String
    End Type
    
    Public Enum DeliveryMethod
        DeliveryMeth_Ship = 1
        DeliveryMeth_DropShip = 2
        DeliveryMeth_CounterSale = 3
        DeliveryMeth_WillCall = 4
        DeliveryMeth_Mixed = 9
    End Enum
    
    'String variables
    Public msLineToPick     As String
    Public msLinesToPick    As String
    Public msOrderToPick    As String
    Public msOrdersToPick   As String
    Public msLineOnHold     As String
    Public msLinesOnHold    As String
    Public msOrderOnHold    As String
    Public msOrdersOnHold   As String
    
    Public msOrderNumber    As String
    Public msOrderDate      As String
    Public msExpireDate     As String
    Public msEarliestShip   As String
    Public msShipFrom       As String
    Public msManualHold     As String
    Public msCreditHold     As String
    Public msHoldReason     As String
    
    Public msPick           As String
    Public msItemOrdered    As String
    Public msShipTo         As String
    Public msTranType       As String
    Public msQtyToPick      As String
    Public msShort          As String
    Public msAvailToShip    As String
    Public msUOM            As String
    Public msStat           As String
    Public msSub            As String
    Public msPriority       As String
    Public msShipDate       As String
    Public msSubItem        As String
    Public msDelivery       As String
    Public msShipVia        As String
    Public msOrderNo        As String
    Public msLineNo         As String
    Public msWhse           As String
    Public msShipToName     As String
    Public msShipToAddress  As String
    Public msDescription    As String
    Public msStockAllocSeq  As String
    Public msBin            As String
    Public msQtyPicked      As String
    Public msShipUOM        As String
    Public msOrdUOM         As String
    Public msQtyToShip      As String
    Public msQtyOpen        As String
    
    Public mbInIsvalidQty   As Boolean
    
    'Modole Option variables
    Public mbIMIntegrated   As Boolean
    Public mbWMIsLicensed   As Boolean
    Public mbAutoPickSetValue   As Boolean
    
    'Constants for context menu
    Public Const kMenuDeleteOrder = 5001
    Public Const kMenuEnterSalesOrder = 5002
    Public Const kMenuEnterTransferOrder = 5003
    Public Const kMenuMaintCustomer = 5004
    Public Const kMenuSOLineInq = 5005
    Public Const kMenuTOLineInq = 5006
    Public Const kMenuMaintItem = 5007
    Public Const kMenuMaintInventory = 5008
    Public Const kMenuInventoryStatusInq = 5009
    Public Const kMenuStockStatusInq = 5010
    Public Const kMenuDeleteLine = 6001
 
    'Misc constants
    Public Const kAllowDecimalPlaces = 1
    Public Const kDisAllowDecimalPlaces = 0
    Public Const kDeleteTempTableYes = 1
    Public Const kDeleteTempTableNo = 0
    Public Const kDeleteDistributionOnlyYes = 1
    Public Const kDeleteDistributionOnlyNo = 0
    Public Const kOrder = 0
    Public Const kLine = 1
    Public Const kNoRepData = 99
    Public Const kSOTranType = 801
    
    'Constants for pick result grids
    Public Const kShowAll              As Integer = 0
    Public Const kShowShortOnly        As Integer = 1
    Public Const kiEffectOnInventory   As Integer = 1   'Effect On Inventory Counters, Plus/Minus

    'Order Grid Column Constants
    Public Const kColOrdPickCheck = 1
    Public Const kColOrdSubFlag = 2
    Public Const kColOrdStatus = 3
    Public Const kColOrdOrderNo = 4
    '*************************************************************************
    'SGS DEJ IA Added BOL (Thus reordered constants) (START)
    '*************************************************************************
    Public Const kColOrdBOL = 5 'SGS DEJ New Column
    
    Public Const kColOrdCrHold = 6 '5
    Public Const kColOrdHold = 7 '6
    Public Const kColOrdHoldReason = 8 '7
    Public Const kcolOrdTranTypeText = 9 '8
    Public Const kColOrdShipToAddrID = 10 '9
    Public Const kColOrdOrderDate = 11 '10
    Public Const kColOrdExpiration = 12 '11
    Public Const kColOrdShipVia = 13 '12
    Public Const kColOrdEarliestShip = 14 '13
    Public Const kColOrdShipPriority = 15 '14
    Public Const kColOrdShipFromWhse = 16 '15
    Public Const kColOrdTranKey = 17 '16
    Public Const kcolOrdTranType = 18 '17
    Public Const kColOrdShipToAddrKey = 19 '18
    Public Const kColOrdRcvgWhseKey = 20 '19
    Public Const kColOrdRcvgWhseID = 21 '20
    Public Const kColOrdRcvgWhseDesc = 22 '21
    Public Const kColOrdShortPickFlag = 23 '22
    Public Const kColOrdShipmentCommitStatus = 24 '23
    Public Const kColOrdExtShipmentExists = 25 '24
    Public Const kColOrdPacked = 26 '25
    
    'Maximum columns to display in grid
    Public Const kMaxOrderCols = 26 '25 'SGS DEJ IA Changed Nbr of columns
    '*************************************************************************
    'SGS DEJ IA Added BOL (Thus reordered constants) (STOP)
    '*************************************************************************
    
    'Line Grid Column Constants
    Public Const kColLinePickCheck = 1
    Public Const kColLineLineHold = 2
    Public Const kColLineItem = 3
    '*************************************************************************
    'SGS DEJ IA (Thus reordered constants) (START)
    '*************************************************************************
    Public Const kColLineLineHoldReason = 5 '4
    Public Const kColLineShiptoAddrID = 6 '5
    Public Const kColLineTranTypeText = 7 '6
    
    Public Const kColLineItemDesc = 4 '27   --SGS DEJ Changed the order For IA
    
    Public Const kColLineAutoDistBinID = 8 '19  --SGS DEJ Changed the order For IA
    Public Const kColLineBinFlag = 9  '--SGS PTC Added to Line Grid For IA
    
    Public Const kColLineShipUOM = 10 '7
    Public Const kColLineCompItemQty = 11 '8
    Public Const kColLineQtyToPick = 12 '9
    Public Const kColLineQtyPicked = 13 '10
    Public Const kColLineQtyShort = 14 '11
    Public Const kColLineQtyDist = 15 '12
    Public Const kColLineQtyAvail = 16 '13
    Public Const kColLineStatus = 17 '14
    Public Const kColLineSubFlag = 18 '15
    Public Const kColLineShipPriority = 19 '16
    Public Const kColLineShipDate = 20 '17
    Public Const kColLineSubItem = 21 '18
    Public Const kColLineDeliveryMethodID = 22 '20
    Public Const kColLineShipVia = 23 '21
    Public Const kColLineOrderNo = 24 '22
    Public Const kColLineLine = 25 '23
    Public Const kColLineShipWhseID = 26 '24
    Public Const kColLineShiptoName = 27 '25
    Public Const kColLineShiptoAddr = 28 '26
    Public Const kColLineSASequence = 29 '28
    
    Public Const kColLineOrderLineUOMID = 30 '29
    Public Const kColLineQtyToPickByOrdUOM = 31 '30
    
    Public Const kColLineItemType = 32 '31
    Public Const kColLineTranKey = 33 '32
    Public Const kColLineTranType = 34 '33
    Public Const kColLineTranLineKey = 35 '34
    Public Const kColLineTranLineDistKey = 36 '35
    Public Const kColLineAllowSubItem = 37 '36
    Public Const kColLineTrackMeth = 38 '37
    Public Const kColLineTrackQtyAtBin = 39 '38
    Public Const kColLineInvtTranKey = 40 '39
    Public Const kColLineShipLineDistKey = 41 '40
    Public Const kColLineOrderLineUOMKey = 42 '41
    Public Const kColLineShipUOMKey = 43 '42
    Public Const kColLineInvtTranID = 44 '43
    Public Const kColLineSubItemKey = 45 '44
    Public Const kColLineKitShipLineKey = 46 '45
    Public Const kColLineCrHold = 47 '46
    Public Const kColLineOrdHold = 48 '47
    Public Const kColLineItemKey = 49 '48
    Public Const kColLineShipWhseKey = 50 '49
    Public Const kColLineShipLineKey = 51 '50
    Public Const kColLineShortPickFlag = 52 '51
    Public Const kColLineCurrDisplayRow = 53 '52
    Public Const kColLineCustReqShipComplete = 54 '53
    Public Const kColLineItemReqShipComplete = 55 '54
    Public Const kColLineAllowImmedShipFromPick = 56 '55
    Public Const kColLineAutoDist = 57 '56
    Public Const kColLineAutoDistBinQtyAvail = 58 '57
    Public Const kColLineAutoDistBinUOMKey = 59 '58
    Public Const kColLineAutoDistLotNo = 60 '59
    Public Const kColLineAutoDistLotExpDate = 61 '60
    Public Const kColLineDistComplete = 62 '61
    Public Const kColLineShipKey = 63 '62
    Public Const kColLineShipmentCommitStatus = 64 '63
    Public Const kColLineShipLineUpdateCounter = 65 '64
    Public Const kColLineShipLineDistUpdateCounter = 66 '65
    Public Const kColLineExtShipmentExists = 67 '66
    Public Const kColLinePacked = 68 '67
    Public Const kColLineStatusCustShipCompleteViolation = 69 '68
    Public Const kColLineStatusItemShipCompLeteViolation = 70 '69
    Public Const kColLineStatusDistWarning = 71 '70
    Public Const kColLineStatusDistQtyShort = 72 '71
    Public Const kColLineStatusOverShipment = 73 '72
    
    Public Const kColLineAllowDecimalQty = 74 '73
    Public Const kColLineRcvgWarehouse = 75 '74
    
    Public Const kColLineQtyOrdered = 76 '75
    Public Const kColLineWhseUseBin = 77 '76
    Public Const kColLineDeliveryMethod = 78 '77
    Public Const kColLineAutoDistLotKey = 79 '78
    Public Const kColLineAutoDistBinKey = 80 '79
    Public Const kColLineShiptoAddrKey = 81 '80
    Public Const kColLineShipMethKey = 82 '81
    Public Const kColLineShipFOBKey = 83 '82
    '*************************************************************************
    'SGS DEJ IA (Thus reordered constants) (STOP)
    '*************************************************************************
    
    'Constants for Security Event
    Public Const kSecEventRemoveCrHold = "SOCREDHOLD"
    Public Const kSecEventRemoveHold = "SORMVHOLD"
    Public Const kSecEventGenerateShipment = "SOGENSHIP"
    
    'Maximum columns to display in grid
    'SGS DEJ IA Changed Nbr of columns (START)
    'Public Const kMaxLineCols = 82
    Public Const kMaxLineCols = 83
    'SGS DEJ IA Changed Nbr of columns (STOP)
    
    Public Const kOpen = 1       '1 = open tsoSOLine, tsoSOLineDist, or timTrnsfrOrderLine lines available for picking
    Public Const kMaxQtyLen = "99999999.99999999"
    Public Const kNoHold = 0
    
    Public Const kSubItemUsed = True
    Public Const kSubItemNotUsed = False
    Public Const kAutoSubstitutionUsed = 1
    Public Const kManualSubstitutionUsed = 0

    Public Const kTM_None      As Integer = 0
    Public Const kTM_Lot       As Integer = 1
    Public Const kTM_Serial    As Integer = 2
    Public Const kTM_Both      As Integer = 3

    Public Const kFinishedGood As Integer = 5
    Public Const kRawMaterial As Integer = 6
    Public Const kBTOKit As Integer = 7
    Public Const kPreAssembledKit As Integer = 8
    
    Public Const kDistBin_LkuRetCol_BinID As Integer = 1
    Public Const kDistBin_LkuRetCol_BinKey As Integer = 2
    Public Const kDistBin_LkuRetCol_QtyOnHand As Integer = 3
    Public Const kDistBin_LkuRetCol_AvailQty As Integer = 4
    Public Const kDistBin_LkuRetCol_UOMKey As Integer = 5
    
    Public Const kDistBinLot_LkuRetCol_BinID As Integer = 1
    Public Const kDistBinLot_LkuRetCol_BinKey As Integer = 2
    Public Const kDistBinLot_LkuRetCol_LotNo As Integer = 3
    Public Const kDistBinLot_LkuRetCol_LotKey As Integer = 4
    Public Const kDistBinLot_LkuRetCol_ExpDate As Integer = 5
    Public Const kDistBinLot_LkuRetCol_QtyOnHand As Integer = 6
    Public Const kDistBinLot_LkuRetCol_AvailQty As Integer = 7
    Public Const kDistBinLot_LkuRetCol_UOMKey As Integer = 8
    
    'Public Constants for pick format
    Public Const kPickFormatWave       As Integer = 0
    Public Const kPickFormatOrder      As Integer = 1
    
    'Public Constants for Include
    Public Const kInclExtLineComm      As Integer = 0
    Public Const kInclShipToAddr       As Integer = 1
    Public Const kInclAbbrvShipToAddr  As Integer = 2
    Public Const kInclBTOKitLine       As Integer = 3
    Public Const kInclNonInvt          As Integer = 4
    
    'Public Constants for IncludeInDetail
    Public Const kInclDetlShipDate     As Integer = 0
    Public Const kInclDetlShipComplete As Integer = 1
    Public Const kInclDetlSubstitution As Integer = 2
    Public Const kInclDetlKitComp      As Integer = 3
    Public Const kInclDetlBinLocation  As Integer = 4
    Public Const kInclDetlPriority     As Integer = 5
    Public Const kInclDetlShipVia      As Integer = 6
    
    'Public Constants for BinSortOption
    Public Const kSortByBinID           As Integer = 0
    Public Const kSortByBinLocation     As Integer = 1
    Public Const kSortByBinSortOrder    As Integer = 2

Const VBRIG_MODULE_ID_STRING = "SOZDD101.BAS"

Public Sub Main()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("sozdd101.clsCreatePick")
        StartAppInStandaloneMode oApp, Command$()
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, "basCreatePick", "Main", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basCreatePick"
End Function

Public Function lStartReport(ByVal sButton As String, ByRef frm As Object, ByRef oClass As Object, _
                            ByRef oReport As Object, iRptFormat As Integer, iReprintFlag As Integer, _
                            iQtyDecPlaces As Integer, iMaxSerialNbrsOnPickList As Integer, _
                            lSessionID As Long, bIsBTOKitList As Boolean, iBTOKitExists As Integer, _
                            lPickListKey As Long, iZoneSort As OptionsZoneSortOrder, iBinSort As OptionsBinSortOrder, _
                            Optional lSettingKey As Variant, Optional iFileType As Variant, _
                            Optional sFileName As Variant, Optional sPrinterName As Variant, Optional bIsSortBy As Boolean = False) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
On Error GoTo ExpectedErrorRoutine

    Dim iRptType As Integer
    Dim sHdrWrkTbl As String
    Dim sDtlWrkTbl As String
    Dim lRecCount As Long
    Dim lRetVal As Long
    Dim sTitle3 As String
    Dim iPageBreakCol As Integer
    Dim bSuppressPrinterDialog As Boolean
    
    lStartReport = kFailure
        
    'Populate Report Data
    'We will run both the pick list report and BTO Kit list report (if applicable) from
    'the same set of data, so the data only need to be populated one time
    If Not bIsBTOKitList Then
        lRetVal = lPopulateReportData(oClass, oClass.moAppDB, frm.moSort, frm.chkInclude(kInclNonInvt).Value, _
                         frm.chkInclude(kInclBTOKitLine).Value, lSessionID, iBTOKitExists, iRptFormat, _
                         lPickListKey, iPageBreakCol)
                         
        Select Case lRetVal
            Case -1
                GoTo ExpectedErrorRoutine
            Case 0
                giSotaMsgBox oClass, oClass.moSysSession, kmsgNoRepData
                lStartReport = kNoRepData
                Exit Function
        End Select
    End If

    '-- Determine the selected report format and set some report engine properties
    If bIsBTOKitList Then
        oReport.ReportFileName = "sozrd004.rpt"
        oReport.Orientation = vbPRORLandscape
    Else
        Select Case iRptFormat
            Case kPickRptTypeSummary
                iRptType = kPickRptTypeSummary
                oReport.ReportFileName = "sozrd002.rpt"
                oReport.Orientation = vbPRORLandscape
            Case kPickRptTypeDetail
                iRptType = kPickRptTypeDetail
                oReport.ReportFileName = "sozrd002.rpt"
                oReport.Orientation = vbPRORLandscape
            Case kPickRptTypeOrder
                iRptType = kPickRptTypeOrder
                If iPageBreakCol > 0 Then
                    oReport.ReportFileName = "sozrd005.rpt"
                Else
                    oReport.ReportFileName = "sozrd003.rpt"
                End If
                oReport.Orientation = vbPRORPortrait
        End Select
    End If
    
    'Overwrite the defalut printer if user has specified one
    If Not IsMissing(sPrinterName) Then
        oReport.PrinterName = sPrinterName
    End If

    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    '-- Start Crystal print engine, open a print job, and get localized strings from
    '-- tsmLocalString table.
    If (oReport.lSetupReport = kFailure) Then
        GoTo ExpectedErrorRoutine
    End If

    '-- CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    '-- to gsBuildString with a VB constant defined in StrConst.bas. The subtitles should
    '-- not include the format selected by the user, i.e., "Detail" or "Summary".
    If bIsBTOKitList Then
        oReport.ReportTitle1 = gsBuildString(kSOBTOKitListing, frm.oClass.moAppDB, frm.oClass.moSysSession)
        oReport.ReportTitle2 = gsBuildString(kSOSRegWHouse, frm.oClass.moAppDB, frm.oClass.moSysSession)
    Else
        If (iRptType = kPickRptTypeOrder) Then
            oReport.ReportTitle1 = gsBuildString(kSOPickList, frm.oClass.moAppDB, frm.oClass.moSysSession)
        Else
            oReport.ReportTitle1 = gsBuildString(kSOPickList, frm.oClass.moAppDB, frm.oClass.moSysSession)
            oReport.ReportTitle2 = gsBuildString(kSOSRegWHouse, frm.oClass.moAppDB, frm.oClass.moSysSession)
        End If
    End If
    
    '-- CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    '-- using the "lbl" convention on formula field names so that label text will handled automatically
    oReport.UseSubTotalCaptions = 1
    oReport.UseHeaderCaptions = 1
        
    '-- Set standard formulas, business date, run time, company name etc.
    '-- as defined in the template
    If (oReport.lSetStandardFormulas(frm) = kFailure) Then
        GoTo ExpectedErrorRoutine
    End If
    
    If Not bIsBTOKitList Then
        'Special handling of Module formula for Order format - Should say "Sales Order" only for SOs,
        '"Transfer Order" for transfers.
        If iRptType = kPickRptTypeOrder Then
            If oReport.bSetReportFormula("Module", "If {tsoPickListHdrWrk.TranType} = " & kTranTypeSOSO & " Then " & """" & "Sales Order" & """" _
                & " Else " & """" & "Transfer Order" & """") = 0 Then
                    GoTo ExpectedErrorRoutine
            End If
        End If
    
        Select Case iRptType
            Case kPickRptTypeOrder
                'Skip first 2 groups
                If (oReport.lSetSortCriteria(frm.moSort, 2) = kFailure) Then
                    GoTo ExpectedErrorRoutine
                End If
            Case Else
                'Skip all groups
                If (oReport.lSetSortCriteria(frm.moSort, 5) = kFailure) Then
                    GoTo ExpectedErrorRoutine
                End If
        End Select
         
        '********* the following is specific to your report *************'
        If oReport.bSetReportFormula("IncludeNonStockItem", CStr(frm.chkInclude(kInclNonInvt).Value)) = 0 Then
            GoTo ExpectedErrorRoutine
        End If
        
        If oReport.bSetReportFormula("IncludeBinLocation", CStr(frm.chkIncludeInDetail(kInclDetlBinLocation).Value)) = 0 Then
            GoTo ExpectedErrorRoutine
        End If
        
        If oReport.bSetReportFormula("QtyDecimals", CStr(iQtyDecPlaces)) = 0 Then
            GoTo ExpectedErrorRoutine
        End If
        
        If oReport.bSetReportFormula("MaxSerialNbrsOnPickList", CStr(iMaxSerialNbrsOnPickList)) = 0 Then
            GoTo ExpectedErrorRoutine
        End If
        
        If bIsSortBy Then
            If oReport.bSetReportFormula("ZoneUsed", CStr(iZoneSort)) = 0 Then
                GoTo ExpectedErrorRoutine
            End If
            If oReport.bSetReportFormula("BinSortSelected", CStr(iBinSort)) = 0 Then
                GoTo ExpectedErrorRoutine
            End If
        End If
        
        If (iRptType <> kPickRptTypeSummary) Then
            If oReport.bSetReportFormula("IncludeExtComm", CStr(frm.chkInclude(kInclExtLineComm).Value)) = 0 Then
                GoTo ExpectedErrorRoutine
            End If
            
            If oReport.bSetReportFormula("IncludeKitCompIndicator", CStr(frm.chkIncludeInDetail(kInclDetlKitComp).Value)) = 0 Then
                GoTo ExpectedErrorRoutine
            End If
            
            If oReport.bSetReportFormula("IncludePriority", CStr(frm.chkIncludeInDetail(kInclDetlPriority).Value)) = 0 Then
                GoTo ExpectedErrorRoutine
            End If
            
            If oReport.bSetReportFormula("IncludeShipCompleteIndicator", CStr(frm.chkIncludeInDetail(kInclDetlShipComplete).Value)) = 0 Then
                GoTo ExpectedErrorRoutine
            End If
            
            If oReport.bSetReportFormula("IncludeShipDate", CStr(frm.chkIncludeInDetail(kInclDetlShipDate).Value)) = 0 Then
                GoTo ExpectedErrorRoutine
            End If
            
            If oReport.bSetReportFormula("IncludeShipToAddr", CStr(frm.chkInclude(kInclShipToAddr).Value)) = 0 Then
                GoTo ExpectedErrorRoutine
            End If
            
            If oReport.bSetReportFormula("IncludeAbbrShipToAddr", CStr(frm.chkInclude(kInclAbbrvShipToAddr).Value)) = 0 Then
                GoTo ExpectedErrorRoutine
            End If
            
            If oReport.bSetReportFormula("IncludeShipVia", CStr(frm.chkIncludeInDetail(kInclDetlShipVia).Value)) = 0 Then
                GoTo ExpectedErrorRoutine
            End If
            
            If oReport.bSetReportFormula("IncludeSubsIndicator", CStr(frm.chkIncludeInDetail(kInclDetlSubstitution).Value)) = 0 Then
                GoTo ExpectedErrorRoutine
            End If
            If iRptType = kPickRptTypeOrder And iPageBreakCol > 0 Then
                If oReport.bSetReportFormula("ResetAfterPageBreak", CStr(iPageBreakCol)) = 0 Then
                    GoTo ExpectedErrorRoutine
                End If
            End If

        Else
        
            'Print Summary
            If oReport.bSetReportFormula("PrintSummary", CStr(1)) = 0 Then
                GoTo ExpectedErrorRoutine
            End If
            'BTOKitExists
            If oReport.bSetReportFormula("BTOKitExists", CStr(iBTOKitExists)) = 0 Then
                GoTo ExpectedErrorRoutine
            End If
        End If
    End If
    
    If iReprintFlag = 1 Then
        If oReport.bSetReportFormula("ReprintFlag", CStr(1)) = 0 Then
            GoTo ExpectedErrorRoutine
        End If
    Else
        If oReport.bSetReportFormula("ReprintFlag", CStr(0)) = 0 Then
            GoTo ExpectedErrorRoutine
        End If
    End If
    '********* End of special processing *************'
    
    '-- Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    oReport.BuildSQL
    
    '-- Appending the sort for PickListLineNo on top of user's selection.
    If bIsBTOKitList Then
        oReport.AppendSort "tsoPickListCompListWrk.SOLineNo"
        oReport.AppendSort "tsoPickListCompListWrk.KitShipLineKey"
    Else
        If (iRptType = kPickRptTypeDetail Or iRptType = kPickRptTypeOrder) Then
            oReport.AppendSort "tsoPickListHdrWrk.PickListLineNo"
        End If
    End If
    
    oReport.SetSQL

    '-- CUSTOMIZE:  Include this call if you have named column labels on the report
    '-- using the "lbl" convention and wish label text to be handled automatically for you.
    oReport.SetReportCaptions
    
    If Not bIsBTOKitList Then
        '-- If user has chosen to print report settings, set text for summary section.
        If (oReport.lSetSummarySection(frm.chkPrintRptSetting.Value, frm.moOptions) = kFailure) Then
            GoTo ExpectedErrorRoutine
        End If
        
        '-- CUSTOMIZE:  If using work tables, restrict report data to current Session ID.  If using
        '-- real tables, might restrict report data to current company or other criteria.
        If (oReport.lRestrictBy("{tsoPickListHdrWrk.SessionID} = " & lSessionID) = kFailure) Then
            GoTo ExpectedErrorRoutine
        End If
    Else
        If (oReport.lSetSummarySection(frm.chkPrintRptSetting.Value, frm.moOptions) = kFailure) Then
            GoTo ExpectedErrorRoutine
        End If
        
        If (oReport.lRestrictBy("{tsoPickListCompListWrk.SessionID} = " & lSessionID) = kFailure) Then
            GoTo ExpectedErrorRoutine
        End If
    End If
    
        
    If oClass.PrintNoUI = True Then
        bSuppressPrinterDialog = True
    End If
    
    oReport.ProcessReport frm, sButton, iFileType, sFileName, bSuppressPrinterDialog
   
    lStartReport = kSuccess
   
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function

ExpectedErrorRoutine:
    'Do only necessary clean up
    CleanupMyWorkTables frm.oClass, lSessionID
    Set oReport = Nothing
    SetHourglass False
    
    'And let errorroutine handle rest
    'if comming from a GOTO ExpectedErrorRoutine, error will be 0 but module/routine name will be displayed.
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lStartReport", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Sub CleanupMyWorkTables(ByRef oAppDb As Object, ByVal lSessionID As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim sHdrWrkTbl As String
    Dim sDtlWrkTbl As String
    Dim sSQL As String

#If RPTDEBUG = 1 Then
    sHdrWrkTbl = "tsoPickListHdrWrk"
    sDtlWrkTbl = "tsoPickListDistWrk"
#Else
    sHdrWrkTbl = "#tsoPickListHdrWrk"
    sDtlWrkTbl = "tsoPickListDistWrk"
#End If

    '-- Delete records from work tables matching the current session ID
    sSQL = "DELETE " & sHdrWrkTbl & " WHERE SessionID = " & lSessionID
    oAppDb.ExecuteSQL sSQL

    sSQL = "DELETE " & sDtlWrkTbl & " WHERE SessionID = " & lSessionID
    oAppDb.ExecuteSQL sSQL

 '+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CleanupMyWorkTables", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Function lPopulateReportData(ByRef oClass As Object, ByRef oAppDb As Object, _
                                ByRef oSort As Object, ByVal iInclNonInvt As Integer, _
                                ByVal iInclBTOKitLine As Integer, _
                                ByVal lSessionID As Long, ByRef iBTOKitExists As Integer, _
                                ByVal iRptType As Integer, ByVal lPickListKey As Long, ByRef iPageBreakCol As Integer) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
        
 Dim sOrderBy As String
 Dim sDtlWrkTbl As String
 Dim sSelect As String
 Dim sInsert As String
 Dim lRecCount As Long
 Dim iRetVal As Integer
 Dim iPrintFormat As Integer


 
    sDtlWrkTbl = "#tsoPickListHdrWrk"
    
    lPopulateReportData = -1

    '-- Extract order by clause from sort grid
    sOrderBy = sBuildOrderBy(oSort, iRptType, iPageBreakCol)
       
    '-- Delete all records from work tables that have the same session ID as current session ID.
    CleanupMyWorkTables oAppDb, lSessionID

    '-- Insert keys for the selected pick list into work tables
    sSelect = "SELECT SL.ShipLineKey, SLD.ShipLineDistKey, PickListKey, " & lSessionID
    sSelect = sSelect & " FROM tsoShipLine SL WITH (NOLOCK)"
    sSelect = sSelect & " JOIN tsoShipLineDist SLD WITH (NOLOCK)"
    sSelect = sSelect & " ON SL.ShipLineKey = SLD.ShipLineKey "
    sSelect = sSelect & " WHERE SL.PickListKey = " & lPickListKey

    sInsert = "INSERT INTO " & sDtlWrkTbl & " (ShipLineKey, ShipLineDistKey, PickListKey, SessionID) " & sSelect
    
    oAppDb.ExecuteSQL sInsert
        
    '-- Check whether any records were inserted; if not, go to error handler.
    lRecCount = glGetValidLong(oAppDb.Lookup("COUNT(*)", sDtlWrkTbl, "SessionID = " & lSessionID))
    If (lRecCount = 0) Then
        lPopulateReportData = 0
        Exit Function
    End If

    
    '-- insert records into secondary work tables.
    With oAppDb
        .SetInParam lSessionID
        .SetInParam oClass.moSysSession.CompanyId
        .SetInParam lPickListKey
        .SetInParam gsFormatDateToDB(oClass.moSysSession.BusinessDate)
        .SetInParam iInclNonInvt
        .SetInParam iInclBTOKitLine
        .SetInParam iRptType
        .SetInParam iPageBreakCol
        .SetOutParam iBTOKitExists
        .SetOutParam iRetVal
        .ExecuteSP "spsoPrintPickList"
        iBTOKitExists = .GetOutParam(9)
        iRetVal = .GetOutParam(10)
        .ReleaseParams
    End With
     
    If (iRetVal <> 1) Then
        '-- Error in SP execution
        giSotaMsgBox oClass, oClass.moSysSession, kmsgProc, "spsoPrintPickList"
        Exit Function
    End If
     
    lPopulateReportData = lRecCount

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lPopulateReportData", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function sBuildOrderBy(ByRef oSort As Object, ByVal iRptType As Integer, _
                            ByRef iPageBreakCol As Integer) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************
' Desc: This routine is a copy of the Sort object's BuildOrderBy
'       routine. We call this here so that we can pass the proper
'       ORDER BY clause to the Create Pick List process.  This
'       unusual handling is required so that pick list line numbers
'       can be assigned according to the order requested.
'*****************************************************************
    Dim iSubtotal As Integer
    Dim iPageBreak As Integer
    Dim sTableColumnName As String
    Dim sDescription As String
    Dim i As Long
    Dim lRet As Long
    Dim sColumnName As String
    Dim iSeqNo As Integer
    Dim sTableName As String
    Dim iSortOrder As Integer
    Dim sParentTableName As String
    Dim iPos As Integer
    Dim sSQL As String

    Const kNoMoreEntries = 999

    sSQL = ""
    iPageBreakCol = 0
    
    For i = 1 To oSort.MaxRows
        lRet = oSort.lGetRow(i, sTableColumnName, sDescription, iSubtotal, _
                             iPageBreak, iSortOrder, sColumnName, iSeqNo, sTableName, sParentTableName)

        If (lRet = kNoMoreEntries) Then
            Exit For
        End If

        If (lRet = kSuccess) Then
            If (iRptType = kPickRptTypeOrder) Then
                iPos = InStr(1, sSQL, "SalesOrder", vbTextCompare)
                If (iPos = 0) And sSQL = "ORDER BY " Then
                    sSQL = sSQL & sBuildOrderBy & sParentTableName & "." & sColumnName
                Else
                    sSQL = sSQL & sBuildOrderBy & ", " & sParentTableName & "." & sColumnName
                End If
                If iPageBreak Then
                    Select Case sColumnName
                        Case "AddrName"
                            iPageBreakCol = 1
                        Case "CustAddrID"
                            iPageBreakCol = 2
                        Case Else
                            iPageBreakCol = 0
                    End Select
                End If
            Else
                iPos = InStr(1, sSQL, "WhseID", vbTextCompare)
                If (iPos = 0) And sSQL = "ORDER BY " Then
                    sSQL = sSQL & sBuildOrderBy & sParentTableName & "." & sColumnName
                Else
                    sSQL = sSQL & sBuildOrderBy & ", " & sParentTableName & "." & sColumnName
                End If
            End If
        Else
            sBuildOrderBy = ""
            Exit Function
        End If

    Next i
    
    If Len(Trim(sSQL)) > 0 Then
        sSQL = " ORDER BY " & sSQL
    End If

    sBuildOrderBy = sSQL
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sBuildOrderBy", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function






