Attribute VB_Name = "basSOCommitTrans"
'************************************************************************************
'     Name: basTemplate
'     Desc: Main Module of this Object containing "Sub Main ()".
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: 07/23/95 KMS
'     Mods: mm/dd/yy XXX
'************************************************************************************
Option Explicit


Const VBRIG_MODULE_ID_STRING = "SOCommitTrans.BAS"

Public Sub Main()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("soCommitTrans.clsSOCommitTrans")
        StartAppInStandaloneMode oApp, Command$()
    End If


'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Main", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "basSOCommitTrans"
End Function





