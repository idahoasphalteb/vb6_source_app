VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{9504980C-B928-4BF5-A5D0-13E1F649AECB}#35.0#0"; "SOTAVM.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.ocx"
Begin VB.Form frmSOCustReturn 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Customer Return Info"
   ClientHeight    =   10755
   ClientLeft      =   3690
   ClientTop       =   2325
   ClientWidth     =   10950
   FillColor       =   &H00C0C0C0&
   HelpContextID   =   34402
   Icon            =   "sozxxCustReturn.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   10755
   ScaleWidth      =   10950
   WhatsThisHelp   =   -1  'True
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -10000
      TabIndex        =   79
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -10000
      TabIndex        =   78
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   87
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.ComboBox CustomCombo 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      ForeColor       =   &H80000012&
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   86
      TabStop         =   0   'False
      Text            =   "Combo1"
      Top             =   1530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   85
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   10365
      Width           =   10950
      _ExtentX        =   19315
      _ExtentY        =   688
      BrowseVisible   =   0   'False
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   77
      TabStop         =   0   'False
      Top             =   0
      Width           =   10950
      _ExtentX        =   19315
      _ExtentY        =   741
      Style           =   6
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   82
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   84
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   83
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   80
      Top             =   4320
      Visible         =   0   'False
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   88
      TabStop         =   0   'False
      Top             =   2880
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   615
      Index           =   0
      Left            =   -10000
      TabIndex        =   89
      TabStop         =   0   'False
      Top             =   720
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1085
      _StockProps     =   0
   End
   Begin Threed.SSPanel pnlData 
      Height          =   9975
      Index           =   0
      Left            =   60
      TabIndex        =   90
      Top             =   330
      Width           =   10845
      _Version        =   65536
      _ExtentX        =   19129
      _ExtentY        =   17595
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelOuter      =   0
      Begin VB.Frame Frame4 
         Caption         =   "Disposition of Product"
         Height          =   1380
         Left            =   90
         TabIndex        =   66
         Top             =   8150
         Width           =   10665
         Begin VB.ComboBox cmboRetestPassFail 
            Enabled         =   0   'False
            Height          =   315
            ItemData        =   "sozxxCustReturn.frx":23D2
            Left            =   3240
            List            =   "sozxxCustReturn.frx":23DC
            Sorted          =   -1  'True
            TabIndex        =   68
            Text            =   "cmboOffSpecValue"
            Top             =   270
            Width           =   1335
         End
         Begin VB.CheckBox chkProdRestocked 
            Caption         =   "Product Restocked"
            Enabled         =   0   'False
            Height          =   285
            Left            =   4980
            TabIndex        =   72
            TabStop         =   0   'False
            Top             =   600
            Visible         =   0   'False
            WhatsThisHelpID =   17776018
            Width           =   2190
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtProdRetestedResult 
            Height          =   285
            Left            =   5760
            TabIndex        =   70
            Top             =   270
            Width           =   3075
            _Version        =   65536
            _ExtentX        =   5424
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
            lMaxLength      =   20
         End
         Begin VB.CheckBox chkProdRetested 
            Caption         =   "Product Retested on Return"
            Height          =   285
            Left            =   480
            TabIndex        =   67
            Top             =   300
            WhatsThisHelpID =   17776018
            Width           =   2460
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtOtherDispDesc 
            Height          =   285
            Left            =   2370
            TabIndex        =   74
            Top             =   900
            Width           =   4785
            _Version        =   65536
            _ExtentX        =   8440
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
            lMaxLength      =   100
         End
         Begin VB.CheckBox chkBlended 
            Caption         =   "Blended With Other Product"
            Height          =   285
            Left            =   510
            TabIndex        =   71
            Top             =   615
            WhatsThisHelpID =   17776018
            Width           =   2460
         End
         Begin VB.CheckBox chkOtherDisp 
            Caption         =   "Other (Describe)"
            Height          =   285
            Left            =   810
            TabIndex        =   73
            Top             =   900
            WhatsThisHelpID =   17776018
            Width           =   1620
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Result"
            Height          =   255
            Index           =   15
            Left            =   4830
            TabIndex        =   69
            Top             =   330
            Width           =   795
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Reason for Return / Non-Conformance"
         Height          =   3135
         Left            =   90
         TabIndex        =   40
         Top             =   4980
         Width           =   10665
         Begin VB.CheckBox chkPaperSysCorrect 
            Caption         =   "Paperwrok/System Corrections"
            Height          =   285
            Left            =   7920
            TabIndex        =   57
            Top             =   720
            WhatsThisHelpID =   17776018
            Width           =   2580
         End
         Begin VB.ComboBox cmboOffSpecValue 
            Enabled         =   0   'False
            Height          =   315
            ItemData        =   "sozxxCustReturn.frx":23EC
            Left            =   1440
            List            =   "sozxxCustReturn.frx":23FF
            Sorted          =   -1  'True
            TabIndex        =   45
            Text            =   "cmboOffSpecValue"
            Top             =   1080
            Width           =   1575
         End
         Begin VB.CheckBox chkOtherReasonAcco 
            Caption         =   "Other (Describe)"
            Height          =   285
            Left            =   4260
            TabIndex        =   54
            Top             =   1700
            WhatsThisHelpID =   17776018
            Width           =   1500
         End
         Begin VB.CheckBox chInvstgtd 
            Caption         =   "Audited by TQD"
            Height          =   285
            Left            =   240
            TabIndex        =   41
            Top             =   240
            WhatsThisHelpID =   17776018
            Width           =   2580
         End
         Begin VB.CheckBox chIASPayFreightNo 
            Caption         =   "No"
            Height          =   195
            Left            =   3000
            TabIndex        =   64
            Top             =   2880
            Width           =   735
         End
         Begin VB.CheckBox chIASPayFreightYes 
            Caption         =   "Yes"
            Height          =   195
            Left            =   2160
            TabIndex        =   63
            Top             =   2880
            Width           =   735
         End
         Begin VB.CheckBox chkNoRestockFee 
            Caption         =   "No Restocking Fee"
            Height          =   285
            Left            =   4500
            TabIndex        =   61
            Top             =   2490
            WhatsThisHelpID =   17776018
            Width           =   2040
         End
         Begin NEWSOTALib.SOTACurrency curRestockFeeAmt 
            Height          =   315
            Left            =   3120
            TabIndex        =   60
            Top             =   2460
            Width           =   975
            _Version        =   65536
            _ExtentX        =   1720
            _ExtentY        =   556
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.26
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin VB.CheckBox chkChargeRestockFee 
            Caption         =   "Charge Restocking Fee"
            Height          =   285
            Left            =   600
            TabIndex        =   59
            Top             =   2490
            WhatsThisHelpID =   17776018
            Width           =   2040
         End
         Begin VB.CheckBox chkOtherReason 
            Caption         =   "Other (Describe)"
            Height          =   285
            Left            =   270
            TabIndex        =   47
            Top             =   1700
            WhatsThisHelpID =   17776018
            Width           =   1500
         End
         Begin VB.CheckBox chkEquipBreakdown 
            Caption         =   "Equipment Breakdown"
            Height          =   285
            Left            =   4260
            TabIndex        =   53
            Top             =   1470
            WhatsThisHelpID =   17776018
            Width           =   2580
         End
         Begin VB.CheckBox chkCustOverOrdered 
            Caption         =   "Overload (Customer Over-Ordered)"
            Height          =   285
            Left            =   4260
            TabIndex        =   52
            Top             =   1230
            WhatsThisHelpID =   17776018
            Width           =   2895
         End
         Begin VB.CheckBox chkWeather 
            Caption         =   "Weather Related Delays"
            Height          =   285
            Left            =   4260
            TabIndex        =   51
            Top             =   990
            WhatsThisHelpID =   17776018
            Width           =   2580
         End
         Begin VB.CheckBox chkWrongMatOrdered 
            Caption         =   "Wrong Material Ordered"
            Height          =   285
            Left            =   4260
            TabIndex        =   50
            Top             =   750
            WhatsThisHelpID =   17776018
            Width           =   2580
         End
         Begin VB.CheckBox chkTemperature 
            Caption         =   "Temperature / Late Delivery"
            Height          =   285
            Left            =   270
            TabIndex        =   46
            Top             =   1470
            WhatsThisHelpID =   17776018
            Width           =   2580
         End
         Begin VB.CheckBox chkMoreThanOrdered 
            Caption         =   "Overload (More than Ordered)"
            Height          =   285
            Left            =   7920
            TabIndex        =   58
            Top             =   990
            WhatsThisHelpID =   17776018
            Width           =   2580
         End
         Begin VB.CheckBox chkViscosity 
            Caption         =   "Off-spec"
            Height          =   285
            Left            =   270
            TabIndex        =   44
            Top             =   990
            WhatsThisHelpID =   17776018
            Width           =   1500
         End
         Begin VB.CheckBox chkWrongMatShipped 
            Caption         =   "Wrong Material Shipped"
            Height          =   285
            Left            =   270
            TabIndex        =   43
            Top             =   750
            WhatsThisHelpID =   17776018
            Width           =   2580
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtOtherReasonDesc 
            Height          =   285
            Left            =   270
            TabIndex        =   48
            Top             =   2000
            Width           =   3345
            _Version        =   65536
            _ExtentX        =   5900
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
            lMaxLength      =   100
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtOtherReasonDescAcco 
            Height          =   285
            Left            =   4260
            TabIndex        =   55
            Top             =   2000
            Width           =   3345
            _Version        =   65536
            _ExtentX        =   5900
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
            lMaxLength      =   100
         End
         Begin VB.CheckBox chIASPaysFreight 
            Caption         =   "IAS Pays Freight (Hide this object)"
            Height          =   195
            Left            =   3840
            TabIndex        =   65
            Top             =   2880
            Visible         =   0   'False
            Width           =   3735
         End
         Begin VB.Line Line6 
            X1              =   7920
            X2              =   8220
            Y1              =   600
            Y2              =   600
         End
         Begin VB.Line Line5 
            X1              =   9480
            X2              =   9780
            Y1              =   600
            Y2              =   600
         End
         Begin VB.Label Label5 
            Caption         =   "Internal Issues"
            Height          =   285
            Left            =   8340
            TabIndex        =   56
            Top             =   480
            Width           =   1065
         End
         Begin VB.Label Label4 
            Caption         =   "We Pay Freight"
            Height          =   195
            Left            =   840
            TabIndex        =   62
            Top             =   2880
            Width           =   1215
         End
         Begin VB.Line Line4 
            X1              =   6630
            X2              =   6930
            Y1              =   630
            Y2              =   630
         End
         Begin VB.Line Line3 
            X1              =   4230
            X2              =   4530
            Y1              =   630
            Y2              =   630
         End
         Begin VB.Label Label3 
            Caption         =   "Customer Accommodation"
            Height          =   285
            Left            =   4680
            TabIndex        =   49
            Top             =   510
            Width           =   2025
         End
         Begin VB.Line Line2 
            X1              =   2040
            X2              =   2610
            Y1              =   630
            Y2              =   630
         End
         Begin VB.Line Line1 
            X1              =   240
            X2              =   720
            Y1              =   630
            Y2              =   630
         End
         Begin VB.Label Label2 
            Caption         =   "Product Quality"
            Height          =   285
            Left            =   840
            TabIndex        =   42
            Top             =   510
            Width           =   1155
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Details of Return / Customer Complaint / Observations / Comments"
         Height          =   1515
         Left            =   120
         TabIndex        =   15
         Top             =   1150
         Width           =   7785
         Begin VB.CheckBox chHeldOvernight 
            Caption         =   "Truck was retained for overnight hold"
            Height          =   255
            Left            =   4680
            TabIndex        =   18
            Top             =   1200
            Width           =   3015
         End
         Begin VB.CheckBox chEmptyTruck 
            Caption         =   "Truck went out empty and brought back a return"
            Height          =   255
            Left            =   150
            TabIndex        =   17
            Top             =   1200
            Width           =   3855
         End
         Begin VB.TextBox txtRetComments 
            Height          =   825
            Left            =   150
            MaxLength       =   500
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   16
            Top             =   300
            WhatsThisHelpID =   17776024
            Width           =   7485
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Original Load Info"
         Height          =   2325
         Left            =   90
         TabIndex        =   19
         Top             =   2655
         Width           =   7785
         Begin VB.CheckBox chSpreader 
            Alignment       =   1  'Right Justify
            Caption         =   "Include Spreader In Price"
            Enabled         =   0   'False
            Height          =   255
            Left            =   240
            TabIndex        =   92
            Top             =   1920
            Width           =   2175
         End
         Begin VB.CheckBox chFreight 
            Alignment       =   1  'Right Justify
            Caption         =   "Include Freight In Price"
            Enabled         =   0   'False
            Height          =   255
            Left            =   2520
            TabIndex        =   91
            Top             =   1920
            Width           =   2175
         End
         Begin VB.ComboBox cmboOrigBOL 
            Height          =   315
            Left            =   930
            Sorted          =   -1  'True
            TabIndex        =   21
            Text            =   "Combo1"
            Top             =   300
            Width           =   1575
         End
         Begin NEWSOTALib.SOTANumber nbrTonsShipped 
            Height          =   285
            Left            =   6180
            TabIndex        =   35
            TabStop         =   0   'False
            Top             =   960
            Width           =   795
            _Version        =   65536
            _ExtentX        =   1402
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            bLocked         =   -1  'True
            mask            =   "<ILH>##<ILp0>#<IRp0>|.##"
            text            =   "  0.00"
            sIntegralPlaces =   3
            sDecimalPlaces  =   2
         End
         Begin SOTACalendarControl.SOTACalendar calShippedDate 
            Height          =   285
            Left            =   6180
            TabIndex        =   31
            TabStop         =   0   'False
            Top             =   300
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   503
            BackColor       =   -2147483633
            Enabled         =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Locked          =   -1  'True
            MaskedText      =   "  /  /    "
            Protected       =   -1  'True
            Text            =   "  /  /    "
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtProduct 
            Height          =   285
            Left            =   930
            TabIndex        =   25
            TabStop         =   0   'False
            Top             =   960
            Width           =   3735
            _Version        =   65536
            _ExtentX        =   6588
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            bLocked         =   -1  'True
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtAdditive 
            Height          =   285
            Left            =   930
            TabIndex        =   27
            TabStop         =   0   'False
            Top             =   1290
            Width           =   3735
            _Version        =   65536
            _ExtentX        =   6588
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            bLocked         =   -1  'True
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtCarrier 
            Height          =   285
            Left            =   930
            TabIndex        =   29
            TabStop         =   0   'False
            Top             =   1620
            Width           =   3735
            _Version        =   65536
            _ExtentX        =   6588
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            bLocked         =   -1  'True
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtTruck 
            Height          =   285
            Left            =   6180
            TabIndex        =   39
            TabStop         =   0   'False
            Top             =   1620
            Width           =   1455
            _Version        =   65536
            _ExtentX        =   2566
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            bLocked         =   -1  'True
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtTank 
            Height          =   285
            Left            =   6180
            TabIndex        =   33
            TabStop         =   0   'False
            Top             =   630
            Width           =   1455
            _Version        =   65536
            _ExtentX        =   2566
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            bLocked         =   -1  'True
            lMaxLength      =   15
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtLot 
            Height          =   285
            Left            =   6180
            TabIndex        =   37
            TabStop         =   0   'False
            Top             =   1290
            Width           =   1455
            _Version        =   65536
            _ExtentX        =   2566
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            bLocked         =   -1  'True
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtContract 
            Height          =   285
            Left            =   930
            TabIndex        =   23
            TabStop         =   0   'False
            Top             =   630
            Width           =   1575
            _Version        =   65536
            _ExtentX        =   2778
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            bLocked         =   -1  'True
         End
         Begin VB.Label Label1 
            Caption         =   "Contract"
            Height          =   255
            Index           =   13
            Left            =   210
            TabIndex        =   22
            Top             =   690
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "Tons Shipped"
            Height          =   255
            Index           =   12
            Left            =   5040
            TabIndex        =   34
            Top             =   990
            Width           =   1305
         End
         Begin VB.Label Label1 
            Caption         =   "Lot"
            Height          =   255
            Index           =   11
            Left            =   5040
            TabIndex        =   36
            Top             =   1350
            Width           =   1305
         End
         Begin VB.Label Label1 
            Caption         =   "Shipped Date"
            Height          =   255
            Index           =   8
            Left            =   5040
            TabIndex        =   30
            Top             =   360
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "Tank"
            Height          =   255
            Index           =   10
            Left            =   5040
            TabIndex        =   32
            Top             =   690
            Width           =   1305
         End
         Begin VB.Label Label1 
            Caption         =   "Truck#"
            Height          =   255
            Index           =   9
            Left            =   5040
            TabIndex        =   38
            Top             =   1680
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "Carrier"
            Height          =   255
            Index           =   7
            Left            =   210
            TabIndex        =   28
            Top             =   1680
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "Additive"
            Height          =   255
            Index           =   6
            Left            =   210
            TabIndex        =   26
            Top             =   1350
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "Product"
            Height          =   255
            Index           =   5
            Left            =   210
            TabIndex        =   24
            Top             =   1020
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "BOL#"
            Height          =   255
            Index           =   1
            Left            =   210
            TabIndex        =   20
            Top             =   360
            Width           =   615
         End
      End
      Begin SOTAVM.SOTAValidationMgr valMgr 
         Left            =   3930
         Top             =   3390
         _ExtentX        =   741
         _ExtentY        =   661
         CtlsCount       =   0
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtCustomerID 
         Height          =   285
         Left            =   1260
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   450
         Width           =   1575
         _Version        =   65536
         _ExtentX        =   2778
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         bLocked         =   -1  'True
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtCustomerName 
         Height          =   285
         Left            =   2940
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   450
         Width           =   4935
         _Version        =   65536
         _ExtentX        =   8705
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         bLocked         =   -1  'True
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtContactName 
         Height          =   285
         Left            =   1260
         TabIndex        =   12
         Top             =   780
         Width           =   3945
         _Version        =   65536
         _ExtentX        =   6959
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         lMaxLength      =   20
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtContactPhone 
         Height          =   285
         Left            =   6150
         TabIndex        =   14
         Top             =   780
         Width           =   1725
         _Version        =   65536
         _ExtentX        =   3043
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         lMaxLength      =   20
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtTons 
         Height          =   285
         Left            =   3210
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   120
         Width           =   525
         _Version        =   65536
         _ExtentX        =   926
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         bLocked         =   -1  'True
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtCompletedBy 
         Height          =   285
         Left            =   1230
         TabIndex        =   76
         TabStop         =   0   'False
         Top             =   9600
         Width           =   1875
         _Version        =   65536
         _ExtentX        =   3307
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         bLocked         =   -1  'True
         lMaxLength      =   100
      End
      Begin SOTACalendarControl.SOTACalendar calReturnDate 
         Height          =   285
         Left            =   6420
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   120
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   503
         BackColor       =   -2147483633
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Locked          =   -1  'True
         MaskedText      =   "  /  /    "
         Protected       =   -1  'True
         Text            =   "  /  /    "
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtBOL 
         Height          =   285
         Left            =   1260
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   120
         Width           =   1275
         _Version        =   65536
         _ExtentX        =   2249
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         bLocked         =   -1  'True
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtProdRestockedBinID 
         Height          =   285
         Left            =   4410
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   120
         Width           =   1005
         _Version        =   65536
         _ExtentX        =   1773
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         bLocked         =   -1  'True
         lMaxLength      =   15
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Tank"
         Height          =   255
         Index           =   16
         Left            =   3870
         TabIndex        =   4
         Top             =   150
         Width           =   405
      End
      Begin VB.Label Label1 
         Caption         =   "Completed By"
         Height          =   255
         Index           =   17
         Left            =   150
         TabIndex        =   75
         Top             =   9660
         Width           =   1185
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Tons"
         Height          =   255
         Index           =   14
         Left            =   2610
         TabIndex        =   2
         Top             =   150
         Width           =   465
      End
      Begin VB.Label Label1 
         Caption         =   "Phone"
         Height          =   255
         Index           =   4
         Left            =   5550
         TabIndex        =   13
         Top             =   840
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Contact Name"
         Height          =   255
         Index           =   2
         Left            =   150
         TabIndex        =   11
         Top             =   840
         Width           =   1185
      End
      Begin VB.Label Label1 
         Caption         =   "Customer"
         Height          =   255
         Index           =   3
         Left            =   150
         TabIndex        =   8
         Top             =   510
         Width           =   1185
      End
      Begin VB.Label Label1 
         Caption         =   "Return Dt"
         Height          =   255
         Index           =   0
         Left            =   5580
         TabIndex        =   6
         Top             =   150
         Width           =   765
      End
      Begin VB.Label Label1 
         Caption         =   "Return BOL#"
         Height          =   255
         Index           =   54
         Left            =   150
         TabIndex        =   0
         Top             =   150
         Width           =   1125
      End
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   81
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmSOCustReturn"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private moClass             As Object

Private mlRunMode           As Long

Private mbSaved             As Boolean

Private mbCancelShutDown    As Boolean

Private mbValidating        As Boolean

Private mbResizing          As Boolean

Private miFilter            As Integer

Public moSotaObjects        As New Collection

Public moMapSrch            As New Collection

Public moMapMemo            As New Collection

Public WithEvents moDmForm                 As clsDmForm
Attribute moDmForm.VB_VarHelpID = -1

Public moDMGrid    As clsDmGrid

Private moContextMenu       As clsContextMenu

Private moVM                As clsValidationManager

Private mbEnterAsTab        As Boolean

Private msCompanyID         As String

Private miMinFormHeight     As Integer

Private miOldFormHeight     As Integer

Private miOldFormWidth      As Integer

Private moAppDAS            As Object

Private moFrmSysSession     As Object

Private miSecurityLevel     As Integer

Private miSecurityAuditTQD     As Integer       'SGS DEJ 1/6/2011 Kevin only wants the Lab to have permission for the Audited by TQD field.

Private msTableName         As String

Const VBRIG_MODULE_ID_STRING = "sozxxCustReturn.frm"

Public mlLanguage               As Long

Private mbLoadSuccess       As Boolean

Private mbClearingCh As Boolean

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    FormHelpPrefix = "SOZ"
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    WhatHelpPrefix = "SOZ"
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    lRunMode = mlRunMode
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lRunMode", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
 mlRunMode = lNewRunMode
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lRunMode", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get bSaved() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bSaved = mbSaved
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bSaved", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Let bSaved(bNewSaved As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mbSaved = bNewSaved
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bSaved", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
   bCancelShutDown = mbCancelShutDown
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bCancelShutDown", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Private Sub chIASPayFreightNo_Click()
    On Error GoTo Error
    
    If mbClearingCh = True Then Exit Sub
    
    If chIASPayFreightNo.Value = vbChecked Then
        chIASPayFreightYes.Value = vbUnchecked
        chIASPaysFreight.Value = vbUnchecked
    Else
        chIASPayFreightYes.Value = vbChecked
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".chIASPayFreightNo_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear

End Sub

Private Sub chIASPayFreightYes_Click()
    On Error GoTo Error
    
    If mbClearingCh = True Then Exit Sub
    
    If chIASPayFreightYes.Value = vbChecked Then
        chIASPayFreightNo.Value = vbUnchecked
        chIASPaysFreight.Value = vbChecked
    Else
        chIASPayFreightNo.Value = vbChecked
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".chIASPayFreightYes_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
End Sub

Private Sub chIASPaysFreight_Click()
    On Error GoTo Error
    
    If mbClearingCh = True Then Exit Sub
    
    If chIASPaysFreight.Value = vbChecked Then
        chIASPayFreightNo.Value = vbUnchecked
        chIASPayFreightYes.Value = vbChecked
    Else
        chIASPayFreightYes.Value = vbUnchecked
        chIASPayFreightNo.Value = vbChecked
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".chIASPaysFreight_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear

End Sub

Private Sub chkChargeRestockFee_Click()
    If chkChargeRestockFee.Value = vbChecked Then '
        chkNoRestockFee.Value = vbUnchecked
        curRestockFeeAmt.Enabled = True
        curRestockFeeAmt.Amount = 250
    Else
        curRestockFeeAmt.Enabled = False
        curRestockFeeAmt.Amount = 0
    End If
End Sub

Private Sub chkNoRestockFee_Click()
    If chkNoRestockFee.Value = vbChecked Then
        chkChargeRestockFee.Value = vbUnchecked
        curRestockFeeAmt.Enabled = False
        curRestockFeeAmt.Amount = 0
    End If
End Sub

Private Sub chkOtherDisp_Click()
    If chkOtherDisp.Value = vbChecked Then
        txtOtherDispDesc.Enabled = True
    Else
        txtOtherDispDesc.Enabled = False
        txtOtherDispDesc.Text = ""
    End If
End Sub

Private Sub chkOtherReason_Click()
    If chkOtherReason.Value = vbChecked Then
        txtOtherReasonDesc.Enabled = True
    Else
        txtOtherReasonDesc.Enabled = False
        txtOtherReasonDesc.Text = ""
    End If
End Sub

'RKL DEJ 1/23/14 (START)
Private Sub chkOtherReasonAcco_Click()
    If chkOtherReasonAcco.Value = vbChecked Then
        txtOtherReasonDescAcco.Enabled = True
    Else
        txtOtherReasonDescAcco.Enabled = False
        txtOtherReasonDescAcco.Text = ""
    End If

End Sub
'RKL DEJ 1/23/14 (STOP)

'Private Sub chkProdRestocked_Click()
'    If chkProdRestocked.Value = vbChecked Then
'        txtProdRestockedBinID.Enabled = True
'    Else
'        txtProdRestockedBinID.Enabled = False
'        txtProdRestockedBinID.Text = ""
'    End If
'End Sub

Private Sub chkProdRetested_Click()
    If chkProdRetested.Value = vbChecked Then
        txtProdRetestedResult.Enabled = True
        cmboRetestPassFail.Enabled = True       'RKL DEJ 2017-10-02
    Else
        txtProdRetestedResult.Enabled = False
        txtProdRetestedResult.Text = ""
        
        cmboRetestPassFail.Enabled = False      'RKL DEJ 2017-10-02
        cmboRetestPassFail.Text = Empty         'RKL DEJ 2017-10-02
    End If
End Sub

Private Sub chkViscosity_Click()
    On Error GoTo Error
    
    If chkViscosity.Value = vbChecked Then
        cmboOffSpecValue.Enabled = True
    Else
        cmboOffSpecValue.Enabled = False
        cmboOffSpecValue.Text = Empty
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".chkViscosity_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
End Sub

Private Sub chkViscosity_LostFocus()
    On Error GoTo Error
    
    If chkViscosity.Value = vbChecked Then
        MsgBox "The product needs to be internally tested and the results need to be entered in this form under the Disposition section.", vbInformation, "MAS 500"
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".chkViscosity_LostFocus()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear

End Sub


Private Sub cmboOrigBOL_LostFocus()
    On Error GoTo Error
    
    Dim sSQL As String
    Dim RS  As Object
    
    If Trim(cmboOrigBOL.Text) = Empty Then
        txtContract.Text = Empty
        txtProduct.Text = Empty
        txtAdditive.Text = Empty
        txtCarrier.Text = Empty
        calShippedDate.Text = Empty
        txtTank.Text = Empty
        nbrTonsShipped.Value = 0
        txtLot.Text = Empty
        txtTruck.Text = Empty
        
        'RKL DEJ 2016-10-06 (start)
        chSpreader.Value = vbUnchecked
        chFreight.Value = vbUnchecked
        'RKL DEJ 2016-10-06 (stop)
        
        Exit Sub
    End If
    
    If UCase(Trim(cmboOrigBOL.Text)) = UCase(Trim(txtBOL.Text)) Then
        MsgBox "The Return BOL cannot be the same as the Origianl BOL.", vbExclamation, "SAGE 500"
        Exit Sub
    End If
    
    'Statement below should find all BOL's but those that have been Hand-Written
'    sSQL = "SELECT * FROM tsoBOL_SGS where Lading = " & Val(cmboOrigBOL.Text)  'RKL DEJ 2016-10-07 Replaced with the script below.
    
    sSQL = "SELECT bol.*, "
    sSQL = sSQL & "Coalesce(slx.IncludeFreightInPrice,0) 'IncludeFreightInPrice', Coalesce(slx.IncludeSpreaderInPrice,0) 'IncludeSpreaderInPrice' "
    sSQL = sSQL & "FROM tsoBOL_SGS bol with(NoLock) "
    sSQL = sSQL & "Left Outer Join tsoSalesOrderExt_SGS sox with(NoLock) "
    sSQL = sSQL & "    on cast(bol.Facility as varchar) + '-' + Cast(bol.lading as varchar) = sox.BOLNo "
    sSQL = sSQL & "Left Outer Join tsoSalesOrder so with(NoLock) "
    sSQL = sSQL & "    on sox.SOKey = so.SOKey "
    sSQL = sSQL & "Left Outer Join tsoSOLine sl with(NoLock) "
    sSQL = sSQL & "    on so.sokey = sl.sokey "
    sSQL = sSQL & "Left outer Join timItem i with(NoLock) "
    sSQL = sSQL & "    on sl.ItemKey = i.ItemKey "
    sSQL = sSQL & "Left Outer Join tsoSOLineExt_SGS slx with(NoLock) "
    sSQL = sSQL & "    on sl.SOLineKey = slx.SOlineKey "
    sSQL = sSQL & "    and bol.item = i.ItemID "
    sSQL = sSQL & "where bol.Lading = " & Val(cmboOrigBOL.Text)

    
    Set RS = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)

    If Not RS.IsEmpty Then
        txtContract.Text = RS.Field("Contract")
        txtProduct.Text = RS.Field("Item")
        txtAdditive.Text = IIf(IsNull(RS.Field("Additive")), "", RS.Field("Additive"))
        txtCarrier.Text = RS.Field("Carrier")
        calShippedDate.Text = RS.Field("LoadTimeOut")
        txtTank.Text = RS.Field("BinID")
        nbrTonsShipped.Value = Round(((RS.Field("GrossWeight1") - RS.Field("TareWeight1")) + (RS.Field("GrossWeight2") - RS.Field("TareWeight2"))) / 2000, 2)
        txtLot.Text = "" 'rs.Field("Lot")
        txtTruck.Text = RS.Field("Truck")
    
        chSpreader.Value = RS.Field("IncludeSpreaderInPrice")
        chFreight.Value = RS.Field("IncludeFreightInPrice")
    Else
        RS.Close
        'Statement below should find the Hand-Written BOL's missed by SQL above
'        sSQL = "SELECT * FROM tsoScalePassBOLPrinting WHERE MASLoadID = " & Val(cmboOrigBOL.Text)  'RKL DEJ 2016-10-07 Replaced with the script below.
        
        sSQL = "SELECT bol.*, "
        sSQL = sSQL & "Coalesce(slx.IncludeFreightInPrice,0) 'IncludeFreightInPrice', Coalesce(slx.IncludeSpreaderInPrice,0) 'IncludeSpreaderInPrice' "
        sSQL = sSQL & "FROM tsoScalePassBOLPrinting bol with(NoLock) "
        sSQL = sSQL & "Left Outer Join tsoSalesOrderExt_SGS sox with(NoLock) "
        sSQL = sSQL & "    on bol.BOLNbr = sox.BOLNo "
        sSQL = sSQL & "Left Outer Join tsoSalesOrder so with(NoLock) "
        sSQL = sSQL & "    on sox.SOKey = so.SOKey "
        sSQL = sSQL & "Left Outer Join tsoSOLine sl with(NoLock) "
        sSQL = sSQL & "    on so.sokey = sl.sokey "
        sSQL = sSQL & "Left outer Join timItem i with(NoLock) "
        sSQL = sSQL & "    on sl.ItemKey = i.ItemKey "
        sSQL = sSQL & "Left Outer Join tsoSOLineExt_SGS slx with(NoLock) "
        sSQL = sSQL & "    on sl.SOLineKey = slx.SOlineKey "
        sSQL = sSQL & "    and bol.Product = i.ItemID "
        sSQL = sSQL & "where bol.MASLoadID = " & Val(cmboOrigBOL.Text)
        
        Set RS = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
    
        If Not RS.IsEmpty Then
            txtContract.Text = RS.Field("ContractNbr")
            txtProduct.Text = RS.Field("Product")
            txtAdditive.Text = IIf(IsNull(RS.Field("Additive")), "", RS.Field("Additive"))
            txtCarrier.Text = RS.Field("Hauler")
            calShippedDate.Text = RS.Field("ScaleOutTime")
            txtTank.Text = RS.Field("TankNumber")
            nbrTonsShipped.Value = Round(RS.Field("NetTons"), 2)
            txtLot.Text = "" 'rs.Field("Lot")
            txtTruck.Text = RS.Field("TruckNo")
            
            chSpreader.Value = RS.Field("IncludeSpreaderInPrice")
            chFreight.Value = RS.Field("IncludeFreightInPrice")
        Else
            'SGS DEJ 7/26/11 added for SO manually entered
            RS.Close
            'Statement below should find the Hand-Written BOL's missed by SQL above
'            sSQL = "SELECT * FROM vluSalesOrderBOLContract_SGS WHERE Lading = " & gsQuoted(Trim(gsGetValidStr(cmboOrigBOL.Text)))   'RKL DEJ 2016-10-07 Replaced with the script below.
            
            sSQL = "SELECT so.* "
            sSQL = sSQL + ", Coalesce(slx.IncludeFreightInPrice,0) 'IncludeFreightInPrice', Coalesce(slx.IncludeSpreaderInPrice,0) 'IncludeSpreaderInPrice' "
            sSQL = sSQL + "FROM vluSalesOrderBOLContract_SGS so with(NoLock) "
            sSQL = sSQL + "Left Outer Join tsoSOLineExt_SGS slx with(NoLock) "
            sSQL = sSQL + "    on so.SOLineKey = slx.SOLineKey "
            sSQL = sSQL + "WHERE Lading = " & gsQuoted(Trim(gsGetValidStr(cmboOrigBOL.Text))) + " "
            
            
            Set RS = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
        
            If Not RS.IsEmpty Then
                txtContract.Text = gsGetValidStr(RS.Field("ContractNo"))
                nbrTonsShipped.Value = Round(gdGetValidDbl(RS.Field("QtyOrd")), 2)
                
                If glGetValidLong(RS.Field("IsAdditive")) = 0 Then
                    txtProduct.Text = gsGetValidStr(RS.Field("Product"))
                Else
                    txtAdditive.Text = gsGetValidStr(RS.Field("Additive"))
                End If
                
                RS.MoveNext
                
                If Not RS.IsBOF And Not RS.IsEOF Then
                    If glGetValidLong(RS.Field("IsAdditive")) = 0 Then
                        txtProduct.Text = gsGetValidStr(RS.Field("Product"))
                    Else
                        txtAdditive.Text = gsGetValidStr(RS.Field("Additive"))
                    End If
                End If
            
                chSpreader.Value = RS.Field("IncludeSpreaderInPrice")
                chFreight.Value = RS.Field("IncludeFreightInPrice")
            
            Else
                MsgBox "Invalid Lading Number", vbOKOnly + vbExclamation, "Sage MAS 500"
                cmboOrigBOL.Text = ""
                cmboOrigBOL.SetFocus
            End If
        End If
    End If
    
    RS.Close
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmboOrigBOL_LostFocus()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear

End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
        mbCancelShutDown = False
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
    End Select



'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{TAB}"
                KeyAscii = 0
            End If
        Case Else
            'Other KeyAscii routines.
            'If txtCompletedBy.Text = "" Then
                txtCompletedBy.Text = CStr(oClass.moSysSession.UserId)
            'End If
    End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    'SGS DEJ 1/6/2012 (START)
    Dim vPrompt As Variant
    Dim sID As String
    Dim sUser As String
    'SGS DEJ 1/6/2012 (STOP)
    
    mbLoadSuccess = False

    msCompanyID = moClass.moSysSession.CompanyId
    mbEnterAsTab = moClass.moSysSession.EnterAsTab
    mlLanguage = moClass.moSysSession.Language

    SetupLookups
    SetupDropDowns
 
    BindVM
    BindForm
    
'    miSecurityLevel = giSetAppSecurity(moClass, tbrMain, moDmForm)

    With moClass.moFramework
        tbrMain.SecurityLevel = .GetTaskPermission(.GetTaskID())
        
        'SGS DEJ 1/6/2012 (START)
        sID = "SGSRtnAuditTQD"
        sUser = moClass.moSysSession.UserId
        vPrompt = False
        
        miSecurityAuditTQD = moClass.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt)
        If miSecurityAuditTQD = 0 Then
            'Disable the chInvstgtd checkbox (Only the Lab should have permissions to this.
            chInvstgtd.Enabled = False
        End If
        'SGS DEJ 1/6/2012 (STOP)
    End With

    'tbrMain.Init sotaTB_SINGLE_ROW, moClass.moSysSession.Language
    SetupBars

    BindContextMenu     'winhook binding of context menu
    
    Dim i As Integer
    
    
    pnlData(0).Enabled = True
    For i = 1 To pnlData.Count - 1
        pnlData(i).Enabled = False
    Next i
    
    mbLoadSuccess = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
'***********************************************************************
'Desc: If the data has changed then prompt to save changes
'***********************************************************************
    
    Dim iConfirmUnload  As Integer
    Dim sFinishKey As String
    
   'Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False


    If moClass.mlError = 0 Then  'mbUnloadMe specific to this prog
        'Call bUnloadconfirmed function
        If Not bUnloadConfirmed() Then GoTo CancelShutDown

    
        'Check all other forms  that may have been loaded from this main form.

        'If there are any visible forms, then this means the form is active.
        'Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then GoTo CancelShutDown
    
        Select Case UnloadMode
            Case vbFormCode
            'Do Nothing.
            'If the unload is caused by form code, then the form
            'code should also have the miShutDownRequester set correctly.
            
            Case Else
           'Most likely the user has requested to shut down the form.
            'If the context is Add-on-the-fly, hide the form
            'and cancel the unload.
            'If the context is Normal or Drill-Around, have the object unload itself.
                 Select Case mlRunMode
                    Case kContextAOF
                        Me.Hide
                        GoTo CancelShutDown
                    Case Else
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                End Select
        End Select
    End If
    
    
    'If execution gets to this point, the form and class object of the form
    'will be shut down.  Perform all operations necessary for a clean shutdown.
    
    PerformCleanShutDown
    
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
        Case Else 'kFrameworkShutDown
            'Do nothing
    End Select
    
Exit Sub
CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub PerformCleanShutDown()

    On Error GoTo ExpectedErrorRoutine
    On Error Resume Next

#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If
    
    'Unload all forms loaded from this main form
    gUnloadChildForms Me
         
    'remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    '**** Data Manager is being used
    If Not moDmForm Is Nothing Then ' Make sure the Data Manager is not Nothing
        moDmForm.UnloadSelf    ' Call the UnloadSelf Method of the DM
        Set moDmForm = Nothing  ' Set the DM reference to Nothing
    End If
    
    TerminateControls Me

    '**** If this form loads any other modal objects.
    Set moSotaObjects = Nothing
    
    '***** If a Search, DrillDown, Memo Button exist on the form
     Set moMapSrch = Nothing
    
    'Set the ContextMenu object to nothing
    Set moContextMenu = Nothing
 
   
    '**** If a Modal Dialog Form exists in the project
    'Set mfrmModalDialog = Nothing


Exit Sub
ExpectedErrorRoutine:
'Nothing
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Unload(Cancel As Integer)


On Error GoTo ExpectedErrorRoutine
On Error Resume Next
    
    Set moClass = Nothing        ' Class Object
    
    Exit Sub
    
ExpectedErrorRoutine:
'Nothing
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function DMValidate(oDm As clsDmForm) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'********************************************************************
'   Description:
'       DMValidate is called from the DataManager before it attempts
'       to save the record.  Here, entries will be checked for validity.
'       If DMValidate returns true, the Data Manager will save the
'       record.  Otherwise, it will stop processing, and the form
'       will remain in the same state as if the use had not pressed
'       the finish button.
'
'   Parameters:
'       oDM <in>    - is the DataManager Object that caused the
'                     DMValidate to be called.
'   Return Values:
'       True    - the entry is valid
'       False   - the Entry is invalid because anyone of the components
'                 tested for validity is invalid.
'********************************************************************
    
    DMValidate = False
    
'SGS DEJ 7/1/2011 Kevin sent email on 6/30 asking to not have the Pay Freight required
'    If chIASPayFreightYes.Value <> vbChecked And chIASPayFreightNo.Value <> vbChecked Then
'        MsgBox "IAS Pays Freight required", vbExclamation + vbOKOnly, "Sage MAS 500"
'        chIASPayFreightYes.SetFocus
'        Exit Function
'    End If
    
    If txtRetComments = "" Then
        MsgBox "Details/Comments required.", vbExclamation + vbOKOnly, "Sage MAS 500"
        txtRetComments.SetFocus
        Exit Function
    End If
    
    If cmboOrigBOL.Text = "" Then
        MsgBox "Original BOL# required.", vbExclamation + vbOKOnly, "Sage MAS 500"
        cmboOrigBOL.SetFocus
        Exit Function
    End If
    
    'RKL DEJ 2016-09-12 (Start)
    'Check that the Orig Lading is not the same and the return lading
    If UCase(Trim(cmboOrigBOL.Text)) = UCase(Trim(txtBOL.Text)) Then
        MsgBox "The Return BOL cannot be the same as the Origianl BOL.", vbExclamation, "SAGE 500"
        cmboOrigBOL.SetFocus
        Exit Function
    End If
    
    'Check that the return contract is the same as the Orig contract
    If Trim(UCase(moDmForm.GetColumnValue("ReturnContract"))) <> Trim(UCase(txtContract.Text)) Then
        MsgBox "The Return Contract " & Trim(UCase(moDmForm.GetColumnValue("ReturnContract"))) & " is not the same as the Original Shipment Contract " & Trim(UCase(txtContract.Text)) & ".", vbExclamation, "SAGE 500"
        cmboOrigBOL.SetFocus
        Exit Function
    End If
    'RKL DEJ 2016-09-12 (Stop)
    
    'RKL DEJ 1/23/14 added chkOtherReasonAcco to if statement
    If chkWrongMatShipped = vbUnchecked And chkViscosity = vbUnchecked And chkMoreThanOrdered = vbUnchecked _
    And chkTemperature = vbUnchecked And chkWrongMatOrdered = vbUnchecked And chkWeather = vbUnchecked _
    And chkCustOverOrdered = vbUnchecked And chkEquipBreakdown = vbUnchecked And chkOtherReason = vbUnchecked _
    And chkOtherReasonAcco = vbUnchecked And chkPaperSysCorrect.Value = vbUnchecked Then
    
        MsgBox "Reason For Return required.  At least one reason should be checked.", vbExclamation + vbOKOnly, "Sage MAS 500"
        chkWrongMatShipped.SetFocus
        Exit Function
    End If
    
    If chkProdRetested.Value = vbChecked Then
        If txtProdRetestedResult = "" Then
            MsgBox "Result required when Product Retested on Return is checked.", vbExclamation + vbOKOnly, "Sage MAS 500"
            txtProdRetestedResult.SetFocus
            Exit Function
        End If
    
        'RKL DEJ 2017-10-02 (START)
        If Trim(cmboRetestPassFail.Text) = Empty Then
            MsgBox "Don't forget to enter the Product Retested (Pass/Fail) value.", vbExclamation + vbOKOnly, "Sage MAS 500"
'            cmboRetestPassFail.SetFocus
'            Exit Function
        Else
            Select Case UCase(Trim(cmboRetestPassFail.Text))
                Case "PASS"
                    'Valid
                Case "FAIL"
                    'Valid
                Case Else
                    'Not Valid
                    MsgBox "Product Retested (Pass/Fail) Value is not valid.  Please select a valid value.", vbExclamation + vbOKOnly, "Sage MAS 500"
                    cmboRetestPassFail.SetFocus
                    Exit Function
            End Select
        End If
        'RKL DEJ 2017-10-02 (STOP)
    End If
    
'    If chkProdRestocked.Value = vbChecked Then
'        If txtProdRestockedBinID = "" Then
'            MsgBox "Tank# required when Product Restocked is checked.", vbExclamation + vbOKOnly, "Sage MAS 500"
'            txtProdRestockedBinID.SetFocus
'            Exit Function
'        End If
'    End If
    
    If chkOtherReason.Value = vbChecked Then
        If txtOtherReasonDesc = "" Then
            MsgBox "Other Description for Product Quality required when Other is checked.", vbExclamation + vbOKOnly, "Sage MAS 500"
            txtOtherReasonDesc.SetFocus
            Exit Function
        End If
    End If
    
    If chkOtherReasonAcco.Value = vbChecked Then
        If txtOtherReasonDescAcco = "" Then
            MsgBox "Other Description for Customer Accommodation required when Other is checked.", vbExclamation + vbOKOnly, "Sage MAS 500"
            txtOtherReasonDesc.SetFocus
            Exit Function
        End If
    End If
    
    If chkChargeRestockFee.Value = vbChecked Then
        If curRestockFeeAmt = 0 Then
            MsgBox "Restocking Fee required when Charge Restocking Fee is checked.", vbExclamation + vbOKOnly, "Sage MAS 500"
            curRestockFeeAmt.SetFocus
            Exit Function
        End If
    Else
        If chkNoRestockFee.Value = vbUnchecked Then
            MsgBox "Restocking Fee determination required.", vbExclamation + vbOKOnly, "Sage MAS 500"
            chkChargeRestockFee.SetFocus
            Exit Function
        End If
    End If
    
    If chkOtherDisp.Value = vbChecked Then
        If txtOtherDispDesc = "" Then
            MsgBox "Other Description required when Other is checked.", vbExclamation + vbOKOnly, "Sage MAS 500"
            txtOtherDispDesc.SetFocus
            Exit Function
        End If
    End If
   
    'RKL DEJ 2017-10-02 (START)
    If chkViscosity.Value = vbChecked Then
        If Trim(cmboOffSpecValue.Text) = Empty Then
            MsgBox "Off-spec Value is require.", vbExclamation + vbOKOnly, "Sage MAS 500"
            cmboOffSpecValue.SetFocus
            Exit Function
        Else
            Select Case UCase(Trim(cmboOffSpecValue.Text))
                Case "DEMULSIBILITY"
                    'Valid
                Case "OTHER"
                    'Valid
                Case "RESIDUE"
                    'Valid
                Case "SIEVE"
                    'Valid
                Case "VISCOSITY"
                    'Valid
                Case Else
                    'Not Valid
                    MsgBox "Off-spec Value is not valid.  Please select a valid value.", vbExclamation + vbOKOnly, "Sage MAS 500"
                    cmboOffSpecValue.SetFocus
                    Exit Function
            End Select
        End If
    End If
    
    
    'RKL DEJ 2017-10-02 (STOP)

    DMValidate = True


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DMValidate", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Sub DMReposition(oDm As clsDmForm)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'********************************************************************
'   The DMReposition is called by the Data Manager from within the
'   KeyChange method, after attempting to select a recordset from
'   the database.  By the time this subroutine is called, the
'   DataManager's state has changed to either kDmStateAdd or
'   kDmStateEdit.  This routine is called whether or not the
'   record exists (in either add or edit state).
'
'   Parameters:
'       oDM <in>    - is the DataManager Object that caused the
'                     DMValidate to be called.
'********************************************************************
    
    Select Case oDm.State
        Case DataMgr.DMStates.kDmStateAdd
            oDm.SetColumnValue "Lading", glGetNextSurrogateKey(moClass.moAppDB, "tsoReturnLoadInfo_SGS")
            oDm.SetColumnValue "CompanyID", msCompanyID
            oDm.SetDirty True
            
        Case DataMgr.DMStates.kDmStateEdit
        Case DataMgr.DMStates.kDmStateLocked
        Case DataMgr.DMStates.kDmStateNone
        Case Else
            'Should never get here
    End Select
    
    moVM.SetAllValid

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DMReposition", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub DMDataDisplayed(oDm As clsDmForm)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'********************************************************************
'   The DMDataDisplayed is called by the Data Manager from within the
'   KeyChange method, after displaying the contents of the recordset
'   from the database in the form's controls.
'   This routine is called ONLY if the record exists (edit state).
'
'   Parameters:
'       oDM <ini>   - is the DataManager Object that caused the
'                     DMDataDisplayed to be called.
'********************************************************************

'Set the values for non-DM attached fields

    moVM.SetAllValid

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DMDataDisplayed", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function DMBeforeUpdate(oDm As clsDmForm)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'**************************************************************************
'   Description: Called from DM.RowUpdate
'                when a ( existing ) record is saved
'
'        Params: Calling DataManager Object
'       Returns: Success(True) or Failure(False)to calling routine
'Note: Part of the big oDMForm.Save Transaction no need to commit etc.
'
'//* KMC -  For any messages that you need to give the user before a save,
'           should be done out of the DM transaction.  This is because you
'           can cause others to be locked out.  Use a PreSave function which
'           returns true if the save is allowed to contiinue, false otherwise.
'           In this PreSave function, put all checks required which may require
'           user interaction before the record can be saved.
'
'
'**************************************************************************
    
    DMBeforeUpdate = False
    

    DMBeforeUpdate = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DMBeforeUpdate", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bRequiredIsValid(ByVal ctlReqdControl As Control, ByVal sAssosLabel As String, Optional iPage As Variant) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'***********************************************************************
'   Desc: Called from DMValidate.
'         bRequiredIsValid will return True if the ctlReqdControl
'         is not empty.
'         If Control is empty diplay message with assigned label and
'         set focus back to control.
'
'  Parms: ctlReqdControl - Required control to be verified
'         sAssosLabel   - Label associated to control to be displayed
'         iPage         - tabPage where control is
'***********************************************************************
    bRequiredIsValid = True

    'If ctl is invisible due to Option changes then it is not required
    If ctlReqdControl.Visible Then
        If Trim(ctlReqdControl) = Empty Then
            '100078, kmsgEntryRequired, You must specify a value for {0}
            '//* KMC -  make sure the cancel the action before displaying
            '           a message box.
            moDmForm.CancelAction
            Call giSotaMsgBox(Me, moClass.moSysSession, kmsgEntryRequired, CVar(sAssosLabel))
            bRequiredIsValid = False
            ''tabAPOpts.Tab = iPage    'Set correct tabPage before Setting focus to control
            If ctlReqdControl.Enabled Then ctlReqdControl.SetFocus
        End If
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bRequiredIsValid", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bUnloadConfirmed() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'***********************************************************************
'     Desc: Called from hlpPrint_Click; navMain_Click; Form_QueryUnload
'           bUnloadConfirmed confirms the unloading of the current record
'           with the DM.
'
'     Note: For Any changes that apply don't apply to ALL the calling routines
'           pull this entire code into the appropriate routine and customize
'           as neccessary
'**********************************************************************
Dim iConfirmUnload      As Integer 'frm JT
    
    bUnloadConfirmed = False

        If moDmForm.State = kDmStateNone Then
            bUnloadConfirmed = True
            Exit Function
        End If
        
        'check for dirty form here with message box
        If Not moVM.IsValidDirtyCheck() Then Exit Function
        
        iConfirmUnload = moDmForm.ConfirmUnload(mlRunMode <> kContextAOF) 'Pass True if Not AOF
        
        Select Case iConfirmUnload
            Case kDmSuccess
                'see DMAfterUpdate, DMAfterInsert
                'rs ix Reset Form back to kDMStateNone (For AOF only otherwise it clears rec when going to NavMode)
                If moDmForm.State <> kDmStateNone And mlRunMode = kContextAOF Then moDmForm.Action kDmCancel
            Case kDmFailure
                Exit Function
            Case kDmError
                Exit Function
                'If you need Data Manager Error Value then you would
                'dimension a variable, such as lError as Long and assign
                'to the DataManager Error property.
                'lError = moDmForm.Error
            Case Else
                Call giSotaMsgBox(Me, moClass.moSysSession, kmsgUnexpectedConfirmUnloadRV, CVar(iConfirmUnload))
                Exit Function
        End Select
    
    bUnloadConfirmed = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bUnloadConfirmed", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub BindForm()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moDmForm = New clsDmForm

    With moDmForm
        .UIType = kDmUISingle
        .Table = "tsoReturnLoadInfo_SGS"
        .UniqueKey = "Lading"
'        .CompanyId = msCompanyID
        .Where = "CompanyID = '" & msCompanyID & "'"
        Set .Form = frmSOCustReturn
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        .GenerateSPs = False
        
        .Bind Nothing, "Lading", SQL_INTEGER
        .Bind Nothing, "CompanyID", SQL_VARCHAR
        .Bind txtTons, "RetTons", SQL_DECIMAL
        .Bind calReturnDate, "RetDate", SQL_DATE
        .Bind txtCustomerID, "Customer", SQL_VARCHAR
        .Bind txtCustomerName, "CustomerName", SQL_VARCHAR
        .Bind txtContactName, "CustContactName", SQL_VARCHAR
        .Bind txtContactPhone, "CustContactPhone", SQL_VARCHAR
        .Bind txtRetComments, "ReturnComments", SQL_VARCHAR
        .Bind cmboOrigBOL, "OrigLading", SQL_INTEGER
        .Bind txtContract, "Contract", SQL_INTEGER
        .Bind txtProduct, "Item", SQL_VARCHAR
        .Bind txtAdditive, "Additive", SQL_VARCHAR
        .Bind txtCarrier, "Carrier", SQL_VARCHAR
        .Bind calShippedDate, "OrigShipDate", SQL_DATE
        .Bind txtTank, "OrigBinID", SQL_VARCHAR
        .Bind nbrTonsShipped, "OrigTons", SQL_DECIMAL
        .Bind txtLot, "OrigLotNumber", SQL_VARCHAR
        .Bind txtTruck, "Truck", SQL_VARCHAR
        .Bind chkWrongMatShipped, "WrongMatShippedFg", SQL_SMALLINT, kDmSetNull
        .Bind chkViscosity, "ViscosityFg", SQL_SMALLINT, kDmSetNull
        .Bind chkMoreThanOrdered, "MoreThanOrderedFg", SQL_SMALLINT, kDmSetNull
        .Bind chkTemperature, "TemperatureFg", SQL_SMALLINT, kDmSetNull
        .Bind chkWrongMatOrdered, "WrongMatOrderedFg", SQL_SMALLINT, kDmSetNull
        .Bind chkWeather, "WeatherFg", SQL_SMALLINT, kDmSetNull
        .Bind chkCustOverOrdered, "CustOverOrderedFg", SQL_SMALLINT, kDmSetNull
        .Bind chkEquipBreakdown, "EquipBreakdownFg", SQL_SMALLINT, kDmSetNull
        .Bind chkOtherReason, "OtherReasonFg", SQL_SMALLINT, kDmSetNull
        .Bind txtOtherReasonDesc, "OtherReasonDesc", SQL_VARCHAR
        
        .Bind chkOtherReasonAcco, "OtherReasonAccmFg", SQL_SMALLINT, kDmSetNull
        .Bind txtOtherReasonDescAcco, "OtherReasonAccmDesc", SQL_VARCHAR
        
        .Bind chkChargeRestockFee, "ChargeRestockingFg", SQL_SMALLINT, kDmSetNull
        .Bind curRestockFeeAmt, "RestockAmt", SQL_DECIMAL
        .Bind chkNoRestockFee, "NoRestockingFg", SQL_SMALLINT, kDmSetNull
        .Bind chkProdRetested, "ProdRetestedFg", SQL_SMALLINT, kDmSetNull
        .Bind txtProdRetestedResult, "ProdRetestResult", SQL_VARCHAR
        .Bind chkProdRestocked, "ProdRestockedFg", SQL_SMALLINT, kDmSetNull
        .Bind txtProdRestockedBinID, "ProdRestockedBinID", SQL_VARCHAR
        .Bind chkBlended, "BlendedFg", SQL_SMALLINT, kDmSetNull
        .Bind chkOtherDisp, "OtherDispFg", SQL_SMALLINT, kDmSetNull
        .Bind txtOtherDispDesc, "OtherDispDesc", SQL_VARCHAR
        .Bind txtCompletedBy, "CompletedBy", SQL_VARCHAR
        .Bind chIASPaysFreight, "IASPaysFreightFlg", SQL_INTEGER, kDmSetNull
        
        .Bind chInvstgtd, "BeingInvestigated", SQL_INTEGER, kDmSetNull
        
        .Bind chEmptyTruck, "TruckLeftEmpty", SQL_INTEGER, kDmSetNull
        
        'RKL DEJ 2016-09-12 (START)
        .Bind Nothing, "ReturnContract", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "ReturnContractNo", SQL_VARCHAR, kDmSetNull
        .Bind Nothing, "ReturnItem", SQL_VARCHAR, kDmSetNull
        .Bind Nothing, "ReturnAdditive", SQL_VARCHAR, kDmSetNull
        .Bind Nothing, "ReturnCarrier", SQL_VARCHAR, kDmSetNull
        .Bind Nothing, "ReturnCarrierID", SQL_VARCHAR, kDmSetNull
        .Bind Nothing, "ReturnCarrier_Name", SQL_VARCHAR, kDmSetNull
        .Bind Nothing, "ReturnWeightTktNo", SQL_VARCHAR, kDmSetNull
        .Bind Nothing, "ReturnPurchase_Order", SQL_VARCHAR, kDmSetNull
        
        .Bind chFreight, "IncludeFreightInPrice", SQL_INTEGER, kDmSetNull
        .Bind chSpreader, "IncludeSpreaderInPrice", SQL_INTEGER, kDmSetNull
        
        'RKL DEJ 2016-09-12 (STOP)
        
        'RKL DEJ 2017-10-02 (START)
        .Bind cmboOffSpecValue, "OffSpecValue", SQL_VARCHAR, kDmSetNull
        .Bind chkPaperSysCorrect, "PaperWorkSystemCorrections", SQL_INTEGER, kDmSetNull
        .Bind cmboRetestPassFail, "ProdRetestedPassFail", SQL_VARCHAR, kDmSetNull
        
        .Bind chHeldOvernight, "TruckHeldOverNight", SQL_INTEGER, kDmSetNull
        'RKL DEJ 2017-10-02 (STOP)

        
        .Init
    End With
'
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindForm", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Set moContextMenu = New clsContextMenu
    
    With moContextMenu
            Set .Form = frmSOCustReturn
            .Init
    End With
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If
'***********************************************************
' Desc: Called from tbrMain_ButtonClick, tbarBrowse_ButtonClick
'       Carries out the actions that determines what to
'       do with the current record being maintained or viewed
'       depending on the Key selected
'
'   Parameters: sKey - Button Selected
'
'***********************************************************
    Select Case sKey
        Case kTbFinishExit

            cmdMaintClick kcmdFinish
        

        Case kTbCancelExit

            cmdMaintClick kcmdCancel

        Case Else
                '  IMPORTANT:  This is to handle future toolbar buttons
    End Select
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub BindVM()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim i   As Integer
    
    
    
    Set moVM = New clsValidationManager
    
    With moVM
        Set .Form = frmSOCustReturn
        Set .Class = moClass
        'Set .TabControl = tabXxxxxz
        
        'Main Page

        .Bind txtTons
        .Bind calReturnDate
        .Bind txtCustomerID
        .Bind txtCustomerName
        .Bind txtContactName
        .Bind txtContactPhone
        .Bind txtRetComments
        .Bind cmboOrigBOL
        .Bind txtContract
        .Bind txtProduct
        .Bind txtAdditive
        .Bind txtCarrier
        .Bind calShippedDate
        .Bind txtTank
        .Bind nbrTonsShipped
        .Bind txtLot
        .Bind txtTruck
        .Bind chkWrongMatShipped
        .Bind chkViscosity
        .Bind chkMoreThanOrdered
        .Bind chkTemperature
        .Bind chkWrongMatOrdered
        .Bind chkWeather
        .Bind chkCustOverOrdered
        .Bind chkEquipBreakdown
        .Bind chkOtherReason
        .Bind txtOtherReasonDesc
        
        .Bind chkOtherReasonAcco
        .Bind txtOtherReasonDescAcco
        
        .Bind chkProdRetested
        .Bind txtProdRetestedResult
        .Bind chkProdRestocked
        .Bind txtProdRestockedBinID
        .Bind chkBlended
        .Bind chkOtherDisp
        .Bind txtOtherDispDesc
        
        .Init
    End With
    
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindVM", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function VMIsValidControl(ctlLostFocus As Control) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    VMIsValidControl = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "VMIsValidControl", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = Me.Name
End Function

Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

#If CUSTOMIZER Then
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyDataBindings moDmForm
                moFormCust.ApplyFormCust
        End If
    End If
#End If
    
    moDmForm.Init

    Exit Sub


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub cmdMaintClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'**************************************************************************
'   Description:
'       cmdMaint is a control array for the user interface command buttons
'       that determine what to do with the current record being maintained
'       or viewed.
'
'   Parameters:
'       index <in> - which command button was pressed.  The choices are
'                    Finish/OK, Cancel, Delete.  Depending on the state
'                    of the form, some of these buttons may be disabled
'                    or invisible.
'
'**************************************************************************
    Dim iActionCode As Integer

    iActionCode = kDmFailure
    

    Select Case Index

        Case kcmdFinish
            
            If Not moVM.IsValidDirtyCheck() Then Exit Sub
            
            iActionCode = moDmForm.Action(kDmFinish)

        Case kcmdCancel
            moDmForm.Action kDmCancel
            moClass.lUIActive = kChildObjectInactive
            moClass.miShutDownRequester = kUnloadSelfShutDown
            Unload Me
        Case Else
            'New Command Button not programmed for
            Call giSotaMsgBox(Me, moClass.moSysSession, kmsgUnexpectedCtrlArryIndex)
    End Select


    Select Case iActionCode
        Case kDmSuccess
            moClass.lUIActive = kChildObjectInactive
            'Esp for One-Rec Maint programs
            moClass.miShutDownRequester = kUnloadSelfShutDown
            Unload Me
            
        Case kDmFailure
            Exit Sub

        Case kDmError
            '======================================================
            '   **** SPECIAL NOTE *****
            'If you need Data Manager Error Value then you would
            'dimension a variable, such as lError as Long and assign
            'to the DataManager Error property.
            'lError = moDmForm.Error
            '======================================================

             Exit Sub
    End Select

'rs
 '   If mlRunMode = kContextAOF Then
 '       Me.Hide
 '   End If

 
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cmdMaintClick", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetupBars()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    tbrMain.LocaleID = mlLanguage
    Set sbrMain.FrameWork = moClass.moFramework
    sbrMain.BrowseVisible = False

    With tbrMain
        .Style = sotaTB_NO_STYLE
        .AddButton kTbFinishExit
        .AddButton kTbCancelExit
        .LocaleID = mlLanguage
    End With


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupBars", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmForm_DMReposition(oChild As Object)
    On Error GoTo Error
    
    Dim tmp As Variant
    
    'Always clear the checkboxes
    Call ClearCheckboxes
    
    tmp = moDmForm.GetColumnValue("IASPaysFreightFlg")
    If IsNull(tmp) = True Then
'        MsgBox "IASPaysFreightFlg = Null"
'        'Clear Checkboxes
'        Call ClearCheckboxes
    Else
        If IsNumeric(tmp) = True Then
            If Abs(CInt(tmp)) > 0 Then
                chIASPayFreightYes.Value = vbChecked
            Else
                chIASPayFreightNo.Value = vbChecked
            End If
'        Else
'            'Clear Checkboxes
'            Call ClearCheckboxes
        End If
    End If
    
'    If chIASPaysFreight.Value = vbChecked Then
'        chIASPayFreightYes.Value = vbChecked
'    ElseIf chIASPaysFreight.Value = vbUnchecked Then
'        chIASPayFreightNo.Value = vbChecked
'    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".moDmForm_DMReposition()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    HandleToolbarClick Button
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    bLoadSuccess = mbLoadSuccess

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bLoadSuccess", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Private Sub SetupLookups()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Initialize the task's Lookup Controls here
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupLookups", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetupDropDowns()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Initialize the task's Dropdown Controls here
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupDropDowns", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Property Get MyApp() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyApp = App
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyApp", VBRIG_IS_FORM                                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get MyForms() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyForms = Forms
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyForms", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picdrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseDown Index, Button, Shift, x, y
    End If
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_MouseDown", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picdrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseMove Index, Button, Shift, x, y
    End If
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_MouseMove", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picdrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseUp Index, Button, Shift, x, y
    End If
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_MouseUp", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picdrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_Paint Index
    End If
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_Paint", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Property Let Lading(lLading As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moDmForm.Where = "CompanyID = '" & msCompanyID & "' AND lading = " & lLading
    moDmForm.Init
    txtBOL.Text = lLading
    
    Call GetListOfBOL(lLading)   'RKL DEJ 2016-09-07
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Lading", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Sub Init()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    DoEvents
    
    valMgr.Reset
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Init", VBRIG_IS_FORM                                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

'Private Sub txtOrigBOL_LostFocus()
'    On Error GoTo Error
'
'    Dim sSQL As String
'    Dim RS  As Object
'
'    'Statement below should find all BOL's but those that have been Hand-Written
'    sSQL = "SELECT * FROM tsoBOL_SGS where Lading = " & Val(txtOrigBOL.Text)
'    Set RS = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
'
'    If Not RS.IsEmpty Then
'        txtContract.Text = RS.Field("Contract")
'        txtProduct.Text = RS.Field("Item")
'        txtAdditive.Text = IIf(IsNull(RS.Field("Additive")), "", RS.Field("Additive"))
'        txtCarrier.Text = RS.Field("Carrier")
'        calShippedDate.Text = RS.Field("LoadTimeOut")
'        txtTank.Text = RS.Field("BinID")
'        nbrTonsShipped.Value = Round(((RS.Field("GrossWeight1") - RS.Field("TareWeight1")) + (RS.Field("GrossWeight2") - RS.Field("TareWeight2"))) / 2000, 2)
'        txtLot.Text = "" 'rs.Field("Lot")
'        txtTruck.Text = RS.Field("Truck")
'    Else
'        RS.Close
'        'Statement below should find the Hand-Written BOL's missed by SQL above
'        sSQL = "SELECT * FROM tsoScalePassBOLPrinting WHERE MASLoadID = " & Val(txtOrigBOL.Text)
'        Set RS = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
'
'        If Not RS.IsEmpty Then
'            txtContract.Text = RS.Field("ContractNbr")
'            txtProduct.Text = RS.Field("Product")
'            txtAdditive.Text = IIf(IsNull(RS.Field("Additive")), "", RS.Field("Additive"))
'            txtCarrier.Text = RS.Field("Hauler")
'            calShippedDate.Text = RS.Field("ScaleOutTime")
'            txtTank.Text = RS.Field("TankNumber")
'            nbrTonsShipped.Value = Round(RS.Field("NetTons"), 2)
'            txtLot.Text = "" 'rs.Field("Lot")
'            txtTruck.Text = RS.Field("TruckNo")
'        Else
'            'SGS DEJ 7/26/11 added for SO manually entered
'            RS.Close
'            'Statement below should find the Hand-Written BOL's missed by SQL above
'            sSQL = "SELECT * FROM vluSalesOrderBOLContract_SGS WHERE Lading = " & gsQuoted(Trim(gsGetValidStr(txtOrigBOL.Text)))
'            Set RS = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
'
'            If Not RS.IsEmpty Then
'                txtContract.Text = gsGetValidStr(RS.Field("ContractNo"))
'                nbrTonsShipped.Value = Round(gdGetValidDbl(RS.Field("QtyOrd")), 2)
'
'                If glGetValidLong(RS.Field("IsAdditive")) = 0 Then
'                    txtProduct.Text = gsGetValidStr(RS.Field("Product"))
'                Else
'                    txtAdditive.Text = gsGetValidStr(RS.Field("Additive"))
'                End If
'
'                RS.MoveNext
'
'                If Not RS.IsBOF And Not RS.IsEOF Then
'                    If glGetValidLong(RS.Field("IsAdditive")) = 0 Then
'                        txtProduct.Text = gsGetValidStr(RS.Field("Product"))
'                    Else
'                        txtAdditive.Text = gsGetValidStr(RS.Field("Additive"))
'                    End If
'                End If
'
'            Else
'                MsgBox "Invalid Lading Number", vbOKOnly + vbExclamation, "Sage MAS 500"
'                txtOrigBOL.Text = ""
'                txtOrigBOL.SetFocus
'            End If
'        End If
'    End If
'
'    RS.Close
'
'    Exit Sub
'Error:
'    MsgBox Me.Name & ".txtOrigBOL_LostFocus()" & vbCrLf & _
'    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
'
'    Err.Clear
'
'End Sub


Sub ClearCheckboxes()
    On Error Resume Next
    
    mbClearingCh = True
    
    chIASPayFreightNo.Value = vbUnchecked
    chIASPayFreightYes.Value = vbUnchecked
    chIASPaysFreight.Value = vbUnchecked
    
    mbClearingCh = False
    
    Err.Clear
End Sub


Sub testing()
    On Error GoTo Error
    
    Dim vPrompt As Variant
    Dim sID As String
    Dim sUser As String

    sID = "SGSRtnAuditTQD"
    sUser = moClass.moSysSession.UserId
    vPrompt = False
    If moClass.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt) = 0 Then
        Debug.Print "0"
    Else
        Debug.Print "not 0"
    End If
    
    
    With moClass.moFramework
        'SGS DEJ 1/6/2012 (START)
'        vPrompt = False 'SGS DEJ 1/6/2012
        miSecurityAuditTQD = .GetSecurityEventPerm(sID, sUser, vPrompt)
        
        If miSecurityAuditTQD <> 1 Then
            'Disable the chInvstgtd checkbox (Only the Lab should have permissions to this.
            chInvstgtd.Enabled = False
        End If
        'SGS DEJ 1/6/2012 (STOP)
    End With
    
    Exit Sub
Error:
    MsgBox Me.Name & ".testing()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Testing Error"
    
    Err.Clear
    
End Sub


'RKL DEJ 2016-09-07 (START)
Sub GetListOfBOL(lLading As Long)
    On Error GoTo Error
    
    Dim sSQL As String
    Dim RS  As DASRecordSet
    
    'Clear Existing List
    cmboOrigBOL.Clear
    
    'Get List of BOL From Contract Number and tsoSalesOrderExt_SGS table

    'Statement below should find all BOL's but those that have been Hand-Written
    sSQL = "select distinct l.CompanyID, l.SoKey, l.TranID, l.TranNo, l.ContractNo, l.Lading 'OrigLading', l.BOLNo 'OrigBOL', l.ItemKey "
    sSQL = sSQL & "From vluSalesOrderBOLContract_SGS l "
    sSQL = sSQL & "Inner join tsoReturnLoadInfo_SGS r "
    sSQL = sSQL & "    on l.CompanyID = r.CompanyID "
    sSQL = sSQL & "    and l.TranNo = r.ReturnContractNo"
    sSQL = sSQL & "    and l.ItemID = r.ReturnItem"
    sSQL = sSQL & "    and r.Lading = " & lLading & " "
    sSQL = sSQL & "Inner Join tsoShipmentExt_SGS shx with(NoLock) "
    sSQL = sSQL & "    on R.Lading = shx.Lading "
    sSQL = sSQL & "Inner Join tsoShipLine shl with(NoLock) "
    sSQL = sSQL & "    on shx.ShipKey = shl.ShipKey "
    sSQL = sSQL & "    and l.ItemKey = shl.ItemKey "
    sSQL = sSQL & "where l.CompanyID = " & gsQuoted(msCompanyID) + " "
    sSQL = sSQL & "And r.Lading = " & lLading & " "

    Set RS = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)

    If Not RS.IsEmpty Then
        RS.MoveFirst
        While Not RS.IsBOF And Not RS.IsEOF
            cmboOrigBOL.AddItem RS.Field("OrigLading")
            RS.MoveNext
        Wend
    Else
        'No Data
    End If
    
    RS.Close
    
    Set RS = Nothing
    

    Exit Sub
Error:
    MsgBox Me.Name & ".testing()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Testing Error"
    
    Err.Clear
End Sub
'RKL DEJ 2016-09-07 (STOP)

