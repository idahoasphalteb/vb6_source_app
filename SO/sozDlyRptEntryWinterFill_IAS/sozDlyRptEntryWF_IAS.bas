Attribute VB_Name = "basDailyReportEntry"
Option Explicit

Const VBRIG_MODULE_ID_STRING = "sozDlyRptEntryWF_IAS.BAS"


Public Sub Main()
    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("sozDlyRptEntryWF_IAS.clsDailyReportEntry")
        StartAppInStandaloneMode oApp, Command$()    ' ** The third parameter MUST be the
                                                     '    TaskID for this project in order for the
                                                     '    Standalone method to be activated for ActiveX Exe's
    End If
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basDailyReportEntry"
End Function
