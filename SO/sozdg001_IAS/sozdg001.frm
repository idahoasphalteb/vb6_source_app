VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#157.0#0"; "EntryLookupControls.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#84.0#0"; "LookupView.ocx"
Begin VB.Form frmConfirmPicks 
   Caption         =   "Confirm Picks"
   ClientHeight    =   7635
   ClientLeft      =   405
   ClientTop       =   1200
   ClientWidth     =   11910
   HelpContextID   =   70186
   Icon            =   "sozdg001.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   7635
   ScaleWidth      =   11910
   Begin FPSpreadADO.fpSpread grdLineInvt 
      Height          =   1455
      Left            =   180
      TabIndex        =   13
      Top             =   5805
      Visible         =   0   'False
      WhatsThisHelpID =   70211
      Width           =   11475
      _Version        =   524288
      _ExtentX        =   20241
      _ExtentY        =   2566
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SpreadDesigner  =   "sozdg001.frx":23D2
      AppearanceStyle =   0
   End
   Begin VB.Frame fraLine 
      Height          =   3345
      Left            =   75
      TabIndex        =   36
      Top             =   3855
      Width           =   11685
      Begin VB.TextBox txtMultipleBin 
         Height          =   315
         Left            =   8310
         TabIndex        =   47
         Text            =   "Multiple Bin"
         Top             =   300
         WhatsThisHelpID =   17777185
         Width           =   1500
      End
      Begin VB.TextBox txtNavUOM 
         Height          =   285
         Left            =   6120
         TabIndex        =   42
         Text            =   "Text1"
         Top             =   150
         Visible         =   0   'False
         WhatsThisHelpID =   70197
         Width           =   1425
      End
      Begin VB.CommandButton cmdPickAll 
         Caption         =   "&Pick All"
         Height          =   375
         Left            =   120
         TabIndex        =   40
         Top             =   240
         WhatsThisHelpID =   70199
         Width           =   1065
      End
      Begin VB.CommandButton cmdClearPicks 
         Caption         =   "&Clear Picks"
         Height          =   375
         Left            =   1260
         TabIndex        =   39
         Top             =   240
         WhatsThisHelpID =   70200
         Width           =   1065
      End
      Begin VB.CheckBox chkShowOnlyPicked 
         Caption         =   "Show &Only Picked"
         Height          =   195
         Left            =   2490
         TabIndex        =   38
         Top             =   330
         WhatsThisHelpID =   70201
         Width           =   2385
      End
      Begin VB.TextBox txtNavSubItem 
         Height          =   285
         Left            =   4380
         TabIndex        =   37
         Text            =   "Text1"
         Top             =   150
         Visible         =   0   'False
         WhatsThisHelpID =   70202
         Width           =   1425
      End
      Begin VB.CommandButton cmdLotSerialBin 
         Caption         =   "&Dist..."
         Height          =   375
         Left            =   10260
         TabIndex        =   43
         Top             =   240
         WhatsThisHelpID =   70203
         Width           =   945
      End
      Begin FPSpreadADO.fpSpread grdLine 
         Height          =   2565
         Left            =   120
         TabIndex        =   41
         Top             =   660
         WhatsThisHelpID =   70198
         Width           =   11445
         _Version        =   524288
         _ExtentX        =   20188
         _ExtentY        =   4524
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         SpreadDesigner  =   "sozdg001.frx":2810
         AppearanceStyle =   0
      End
      Begin LookupViewControl.LookupView navGridUOM 
         Height          =   285
         Left            =   7560
         TabIndex        =   44
         TabStop         =   0   'False
         Top             =   150
         Visible         =   0   'False
         WhatsThisHelpID =   70196
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   503
         LookupMode      =   1
      End
      Begin LookupViewControl.LookupView navGridSubItem 
         Height          =   285
         Left            =   5820
         TabIndex        =   45
         TabStop         =   0   'False
         Top             =   150
         Visible         =   0   'False
         WhatsThisHelpID =   70195
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   503
         LookupMode      =   1
      End
      Begin EntryLookupControls.TextLookup lkuAutoDistBin 
         Height          =   315
         Left            =   8310
         TabIndex        =   46
         Top             =   300
         WhatsThisHelpID =   17777186
         Width           =   1830
         _ExtentX        =   3228
         _ExtentY        =   556
         ForeColor       =   -2147483640
         LookupID        =   "DistBin"
         ParentIDColumn  =   "WhseBinID"
         ParentKeyColumn =   "WhseBinKey"
         ParentTable     =   "timWhseBin"
         BoundColumn     =   "WhseBinKey"
         BoundTable      =   "timWhseBin"
         sSQLReturnCols  =   "Bin,lkuAutoDistBin,;WhseBinKey,,;OnHand,,;Available,,;UOMKey,,;"
      End
   End
   Begin VB.Frame Frame1 
      Height          =   2895
      Left            =   9645
      TabIndex        =   30
      Top             =   960
      Width           =   2115
      Begin VB.Frame fraInclude 
         Caption         =   "Include"
         Height          =   1335
         Left            =   240
         TabIndex        =   33
         Top             =   240
         Width           =   1650
         Begin VB.CheckBox chkInclude 
            Caption         =   "Sales Orders"
            Height          =   345
            Index           =   0
            Left            =   120
            TabIndex        =   35
            Top             =   360
            Value           =   1  'Checked
            WhatsThisHelpID =   70228
            Width           =   1425
         End
         Begin VB.CheckBox chkInclude 
            Caption         =   "Transfer Orders"
            Height          =   345
            Index           =   1
            Left            =   120
            TabIndex        =   34
            Top             =   720
            Value           =   1  'Checked
            WhatsThisHelpID =   70229
            Width           =   1425
         End
      End
      Begin VB.CommandButton cmdSelect 
         Caption         =   "&Select"
         Height          =   375
         Left            =   360
         TabIndex        =   32
         Top             =   1800
         WhatsThisHelpID =   70227
         Width           =   1425
      End
      Begin VB.CommandButton cmdDeSelect 
         Caption         =   "&De-Select"
         Height          =   375
         Left            =   360
         TabIndex        =   31
         Top             =   2280
         WhatsThisHelpID =   70226
         Width           =   1425
      End
   End
   Begin VB.ComboBox cboSetting 
      Height          =   315
      Left            =   870
      Style           =   2  'Dropdown List
      TabIndex        =   28
      Top             =   600
      WhatsThisHelpID =   17777187
      Width           =   3585
   End
   Begin TabDlg.SSTab tabSelect 
      Height          =   2820
      Left            =   60
      TabIndex        =   15
      Top             =   1020
      WhatsThisHelpID =   70225
      Width           =   9525
      _ExtentX        =   16801
      _ExtentY        =   4974
      _Version        =   393216
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   520
      ShowFocusRect   =   0   'False
      TabCaption(0)   =   "Sales Orders"
      TabPicture(0)   =   "sozdg001.frx":2C4E
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraSelect(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Transfer Orders"
      TabPicture(1)   =   "sozdg001.frx":2C6A
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraSelect(1)"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Selection(Hidden)"
      TabPicture(2)   =   "sozdg001.frx":2C86
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "grdSelSettings"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Custom Tab"
      TabPicture(3)   =   "sozdg001.frx":2CA2
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "fraSelect(2)"
      Tab(3).ControlCount=   1
      Begin VB.Frame fraSelect 
         BorderStyle     =   0  'None
         Height          =   2370
         Index           =   2
         Left            =   -74880
         TabIndex        =   48
         Top             =   375
         Width           =   9285
      End
      Begin FPSpreadADO.fpSpread grdSelSettings 
         Height          =   2205
         Left            =   -74895
         TabIndex        =   25
         Top             =   435
         WhatsThisHelpID =   70224
         Width           =   9225
         _Version        =   524288
         _ExtentX        =   16272
         _ExtentY        =   3889
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         SpreadDesigner  =   "sozdg001.frx":2CBE
         AppearanceStyle =   0
      End
      Begin VB.Frame fraSelect 
         Caption         =   "S&elect"
         Height          =   2370
         Index           =   1
         Left            =   -74880
         TabIndex        =   22
         Top             =   375
         Width           =   9285
         Begin FPSpreadADO.fpSpread grdSelection 
            Height          =   2055
            Index           =   1
            Left            =   120
            TabIndex        =   23
            Top             =   225
            WhatsThisHelpID =   24
            Width           =   9060
            _Version        =   524288
            _ExtentX        =   15981
            _ExtentY        =   3625
            _StockProps     =   64
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            SpreadDesigner  =   "sozdg001.frx":30FC
            AppearanceStyle =   0
         End
         Begin NEWSOTALib.SOTACurrency curControl 
            Height          =   285
            Index           =   1
            Left            =   1530
            TabIndex        =   24
            TabStop         =   0   'False
            Top             =   1035
            WhatsThisHelpID =   48
            Width           =   1950
            _Version        =   65536
            _ExtentX        =   3440
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            sBorder         =   1
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTAMaskedEdit mskControl 
            Height          =   240
            Index           =   1
            Left            =   1605
            TabIndex        =   26
            TabStop         =   0   'False
            Top             =   960
            WhatsThisHelpID =   45
            Width           =   1950
            _Version        =   65536
            _ExtentX        =   3440
            _ExtentY        =   423
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            sBorder         =   1
         End
         Begin NEWSOTALib.SOTANumber nbrControl 
            Height          =   285
            Index           =   1
            Left            =   1590
            TabIndex        =   27
            TabStop         =   0   'False
            Top             =   1665
            WhatsThisHelpID =   47
            Width           =   1950
            _Version        =   65536
            _ExtentX        =   3440
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            sBorder         =   1
            mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin LookupViewControl.LookupView navControlTransfer 
            Height          =   285
            Index           =   0
            Left            =   3555
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   450
            WhatsThisHelpID =   70218
            Width           =   285
            _ExtentX        =   503
            _ExtentY        =   503
            LookupMode      =   1
         End
      End
      Begin VB.Frame fraSelect 
         Caption         =   "S&elect"
         Height          =   2370
         Index           =   0
         Left            =   120
         TabIndex        =   16
         Top             =   375
         Width           =   9285
         Begin FPSpreadADO.fpSpread grdSelection 
            Height          =   2055
            Index           =   0
            Left            =   120
            TabIndex        =   17
            Top             =   225
            WhatsThisHelpID =   24
            Width           =   9060
            _Version        =   524288
            _ExtentX        =   15981
            _ExtentY        =   3625
            _StockProps     =   64
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            SpreadDesigner  =   "sozdg001.frx":353A
            AppearanceStyle =   0
         End
         Begin NEWSOTALib.SOTACurrency curControl 
            Height          =   285
            Index           =   0
            Left            =   1530
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   1035
            WhatsThisHelpID =   48
            Width           =   1950
            _Version        =   65536
            _ExtentX        =   3440
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            sBorder         =   1
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTANumber nbrControl 
            Height          =   285
            Index           =   0
            Left            =   1530
            TabIndex        =   19
            TabStop         =   0   'False
            Top             =   1440
            WhatsThisHelpID =   47
            Width           =   1950
            _Version        =   65536
            _ExtentX        =   3440
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            sBorder         =   1
            mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTAMaskedEdit mskControl 
            Height          =   240
            Index           =   0
            Left            =   1530
            TabIndex        =   20
            TabStop         =   0   'False
            Top             =   720
            WhatsThisHelpID =   45
            Width           =   1950
            _Version        =   65536
            _ExtentX        =   3440
            _ExtentY        =   423
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            sBorder         =   1
         End
         Begin LookupViewControl.LookupView navControl 
            Height          =   285
            Index           =   0
            Left            =   3555
            TabIndex        =   21
            TabStop         =   0   'False
            Top             =   450
            WhatsThisHelpID =   46
            Width           =   285
            _ExtentX        =   503
            _ExtentY        =   503
            LookupMode      =   1
         End
      End
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -10000
      TabIndex        =   1
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -10000
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.ComboBox CustomCombo 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      ForeColor       =   &H80000012&
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   7
      TabStop         =   0   'False
      Text            =   "Combo1"
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      Style           =   7
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   7245
      WhatsThisHelpID =   73
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   688
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   2
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   5
      Top             =   2880
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   12
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin VB.Label lblSetting 
      Caption         =   "Se&tting"
      Height          =   225
      Left            =   120
      TabIndex        =   29
      Top             =   645
      Width           =   615
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   11
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmConfirmPicks"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

'Public Form Variables
Public moSotaObjects            As New Collection
Public msCompanyID              As String
Public moMapSrch                As Collection   'Search Buttons
Public moSettings               As clsSettings

Private mbMultipleBin           As Boolean  'To Check if the bin is "MultipleBin"
'Private Object Variables
Private moClass                 As Object
Private moAppDB                 As Object
Private moSysSession            As Object
Private moSelect(1)             As clsSelection ' Array for Multiple Grid
Private WithEvents moDMGrid     As clsDmGrid
Attribute moDMGrid.VB_VarHelpID = -1
Private WithEvents moDMGridInvt As clsDmGrid
Attribute moDMGridInvt.VB_VarHelpID = -1
Private WithEvents moGM         As clsGridMgr
Attribute moGM.VB_VarHelpID = -1
Private moContextMenu           As clsContextMenu
Private moModuleOptions         As clsModuleOptions
Private WithEvents moHook       As AFWinHook.clsMsgs
Attribute moHook.VB_VarHelpID = -1
Private moGridNavUOM            As clsGridLookup
Private moGridNavSubItem        As clsGridLookup
Private moItemDist              As Object
Private moMultSelGridMgr        As clsMultiSelGridSettings

'Private Miscellaneous Variables
Private mbEnterAsTab            As Boolean
Private msUserID                As String
Private msHomeCurrID            As String
Private miSecurityLevel         As Integer
Private mlLanguage              As Long
Private mbLoading               As Boolean
Private mlRunMode               As Long
Private mbCancelShutDown        As Boolean
Private mbLoadSuccess           As Boolean
Private mbTmpTableCreated       As Boolean
Private bMaskCtrlLostFocus      As Boolean
Private miNbrDecPlaces          As Integer
Private mbValidating            As Boolean
Private msLineToPick            As String
Private msLinesToPick           As String
Private sLotSerialBin           As String
Private bContextMenu            As Boolean
Private bResult                 As Boolean
Private bAutoPickUsed           As Boolean
Private mbFinishAndExit         As Boolean
Private mbWMIsLicensed          As Boolean
Private mbIMIntegrated          As Boolean
Private mbCellChangeFired       As Boolean

'Multiple Grid Variables
Private msBaseCaption(1)        As String
Private moDDData                As clsDDData
Private sRealTableCollection    As Collection
Private moOptions               As clsOptions
Private miOverShipmentPolicy    As SOOverShipmentPolicy

'Grid Edit Variables
Private mlInvtTranID            As Long
Private mlInvtTranKey           As Long
Private mlKitLineKey            As Long
Private miTrackMethod           As Integer
Private miTrackByBin            As Integer
Private mdQtyToPick             As Double
Private mdQtyPicked             As Double
Private miShipComplete          As Integer
Private mlWhseKey               As Long
Private miShipCompleteInv          As Integer

'Private Form Resize Variables
Private miMinFormHeight As Long
Private miMinFormWidth As Long
Private miOldFormHeight As Long
Private miOldFormWidth As Long


'Line Grid Column Constants

Private Const kColPickCheck = 1         'checkbox indicating that the line has been picked
Private Const kColOrder = 2             'displays Order ID
'******************************************************************************
'SGS DEJ IA (Reorder Constants) (START)
'******************************************************************************
Private Const kColBOL = 3               '****   SGS DEJ IA New Field    ******
Private Const kColTranTypeText = 4 '3      'displays text indication of transaction type (Sale / Transfer)
Private Const kColLine = 5 '4              'displays Order Line Number
Private Const kColPickList = 6 '5          'displays the Pick List Number
Private Const kColPickLineNo = 7 '6        'displays the Pick List Line Number
Private Const kColItem = 8 '7              'displays Displays Item ID
Private Const kColQtyToPick = 9 '8         'displays Line Item Quantity To Pick
Private Const kColQtyPicked = 10 '9         'displays Line Item Qty Picked
Private Const kColUOM = 11 '10              'displays Unit Of Measure
Private Const kColShipToAddr = 12 '11       'displays Customer Ship To Address
Private Const kColShipVia = 13 '12          'displays Ship Via Method
Private Const kColShipDate = 14 '13         'displays Shiping Date
Private Const kColShipPriority = 15 '14     'displays Shipping Priority
Private Const kColShipWarehouse = 16 '15    'displays Shipping Warehouse For Item
Private Const kColRcvgWarehouse = 17 '16    'displays Receiving Warehouse for Item
Private Const kColCustomer = 18 '17         'displays Customer ID
Private Const kColCustName = 19 '18         'displays Customer Name
Private Const kColDescription = 20 '19      'displays Item Description
Private Const kColSubItem = 21 '20          'displays the substitute item ID
Private Const kColSubDesc = 22 '21          'displays Substitute Item Description

Private Const kColInvTranKey = 23 '22       'hidden - Inventory Transaction Key from IMS
Private Const kColAllowSubItem = 24 '23     'hidden - does customer allow substitute items
Private Const kColTrackMeth = 25 '24        'hidden - is this item tracked by serial number, UOM cannot change
Private Const kColTrackQtyAtBin = 26 '25    'hidden - is this item tracked by bin
Private Const kColInvTranID = 27 '26        'hidden - Inventory Tran ID from IMS
Private Const kColKitLineKey = 28 '27       'hidden - Kit Ship Line Key
Private Const kColShipComplete = 29 '28     'hidden - Ship Complete indicator
Private Const kColShipDateRaw = 30 '29      'hidden - Ship Date for non-display purposes
Private Const kColShipCompleteInv = 31 '30     'hidden column indicating if this item allows backorders
Private Const kColAllowDecimalQty = 32 '31  'hidden column indicating if this item allows decimal
Private Const kColSOLineUOMKey = 33 '32     'hidden column indicating sales UOMKey
Private Const kColConfirmed = 34 '33        'hidden column indicating if this soline confirmed
Private Const kColSumOfDist = 35 '34        'hidden column that is the sum of the tsoShipLineDistBin quantities
Private Const kColTranType = 36 '35         'Hidden column for TranType

Private Const kColAutoDist = 37 '36
Private Const kColAutoDistBinID = 38 '37
Private Const kColAutoDistBinKey = 39 '38
Private Const kColAutoDistBinQtyAvail = 40 '39
Private Const kColAutoDistBinUOMKey = 41 '40
Private Const kColAutoDistLotNo = 42 '41
Private Const kColAutoDistLotExpDate = 43 '42
Private Const kColAutoDistInvtLotKey = 44 '43
Private Const kColAutoDistMultipleBin = 45 '44
Private Const kColItemType = 46 '45
Private Const kColQtyOrdered = 47 '46
'******************************************************************************
'SGS DEJ IA (Reorder Constants) (STOP)
'******************************************************************************

'Invt Grid Column Constants
Private Const kColInvtShipLineDist = 1  'Ship Line Dist Key
Private Const kColInvtInvtTranID = 2    'Invt Tran ID
Private Const kColInvtInvtTranKey = 3   'Invt Tran Key
Private Const kColInvtCols = 3          'Nbr of columns

'SGS DEJ IA (START)
'Private Const kMaxCols = 46             'maximum columns to display in grid
Private Const kMaxCols = 47             'maximum columns to display in grid
'SGS DEJ IA (STOP)

Private Const kLineOpen = 1             '1 = open tsoSOLine lines available for picking
Private Const kLineDistOpen = 1         '1 = open tsoSOLineDist lines available for picking
Private Const kMaxQtyLen = "99999999.99999999"
Private Const kSOOpenStatus = 1         'Sales Order open status
Private Const kSONoHold = 0             'Sales Order NOT on hold
Private Const kSONoCrHold = 0           'Sales Order NOT on credit hold
Private Const kiEffectOnInventory = 1   'Effect On Inventory Counters, Plus/Minus

'Multiple Grid Constants
Private Const kSalesOrder As Integer = 0
Private Const kTransferOrder As Integer = 1
Private Const kOpen = 1                 '1 = open tsoSOLine, tsoSOLineDist, or timTrnsfrOrderLine lines available for picking


' ItemTypes
Private Const klTypeFinishedGood As Long = 5     'Item Type Finished Good
Private Const klTypeRawMaterial As Long = 6      'Item Type Raw Material
Private Const klTypeBTOKit As Long = 7           'Item Type BTO Kit
Private Const klTypeAssembledKit As Long = 8     'Item Type Assembled Kit

Private Const kiAmtToPickValid As Integer = 0    'Return values for iAmtToPickInvalid function
Private Const kiAmtToPickWarning As Integer = 1
Private Const kiAmtToPickInvalid As Integer = 2

Private mbValidAutoDistBin  As Boolean
Private mbAutoDistBinClicked As Boolean

Const VBRIG_MODULE_ID_STRING = "SOZDG001.FRM"

Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'********************************************************************
' Description:
'    lRunMode contains the user-defined run mode context in which the
'    class object was Initialzed. The run mode is extracted from the
'    context passed in through the InitializeObject.
'********************************************************************
    lRunMode = mlRunMode
        Exit Property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "lRunMode", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlRunMode = lNewRunMode
        Exit Property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "lRunMode", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'***********************************************************************
' Description:
'    oClass contains the reference to the parent class object. The form
'    needs this reference to use the public variables created within the
'    class object.
'***********************************************************************
    Set oClass = moClass
        Exit Property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "oClass", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = oNewClass
        Exit Property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "oClass", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "frmConfirmPicks"
End Function

Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bLoadSuccess = mbLoadSuccess
        Exit Property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "bLoadSuccess", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'**********************************************************************
' Description:
'    bCancelShutDown tells the framework whether the form has requested
'    the shutdown process to be cancelled.
'**********************************************************************
    bCancelShutDown = mbCancelShutDown
        Exit Property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "bCancelShutDown", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
    '-- the three-letter abbreviation used below is built as follows:
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> + "Z"
    WhatHelpPrefix = "SOZ"
        Exit Property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "WhatHelpPrefix", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
    '-- the four-letter abbreviation used below is built as follows:
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> +
    '-- "Z" + <one-letter mnemonic of program type, e.g., "M"=Maintenance>
    FormHelpPrefix = "SOZ"
        Exit Property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "FormHelpPrefix", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property



Private Sub cboSetting_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick cboSetting, True
    #End If
'+++ End Customizer Code Push +++
    Dim i As Integer
    If Not moSettings.gbSkipClick Then
'        mbLoadSetting = True
        moSettings.bLoadReportSettings
'        mbLoadSetting = False
        RefreshTabCaptions
    End If
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboSetting_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub chkInclude_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkInclude(Index), True
    #End If
'+++ End Customizer Code Push +++
    Dim iTabCnt As Integer
    
    'Enable/disable the associated tab.
    '(VB-8/28) Added logic to switch tabs and enable/disable grid according to Include checkbox selection
    If chkInclude(Index).Enabled = True Then
        If chkInclude(Index).Value = vbChecked Then
            tabSelect.TabEnabled(Index) = True
            grdSelection(Index).Enabled = True
            tabSelect.Tab = Index
        Else
            With tabSelect
                .TabEnabled(Index) = False
                grdSelection(Index).Enabled = False
                For iTabCnt = 0 To .Tabs - 3
                    If .TabEnabled(iTabCnt) = True Then
                        .Tab = iTabCnt
                        Exit For
                    End If
                Next
            End With
        End If
    
        'Refresh the associated tab's caption.
        tabSelect.TabCaption(Index) = moMultSelGridMgr.sBuildCaption(msBaseCaption(Index), _
    grdSelection(Index), tabSelect.TabEnabled(Index))
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "chkInclude_Click", VBRIG_IS_FORM      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub chkShowOnlyPicked_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkShowOnlyPicked, True
    #End If
'+++ End Customizer Code Push +++
    If chkShowOnlyPicked.Value = 0 Then
        ShowUnpicked
    Else
        HideUnpicked
    End If
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "chkShowOnlyPicked_Click", VBRIG_IS_FORM      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub HideUnpicked()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iChecked    As Integer
Dim lRow        As Long

    grdLine.redraw = False
    For lRow = 1 To grdLine.DataRowCnt
        iChecked = giGetValidInt(gsGridReadCell(grdLine, lRow, kColPickCheck))
        If iChecked = 0 Then
            With grdLine
                .Row = lRow
                .RowHidden = True
            End With
        End If
    Next lRow
    grdLine.redraw = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "HideUnpicked", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ShowUnpicked()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lRow        As Long

    grdLine.redraw = False
    For lRow = 1 To grdLine.DataRowCnt
        With grdLine
            .Row = lRow
            .RowHidden = False
        End With
    Next lRow
    grdLine.redraw = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "ShowUnpicked", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub cmdClearPicks_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdClearPicks, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
Dim iChecked    As Integer
Dim lRow        As Long
    
    frmConfirmPicks.MousePointer = vbHourglass
    grdLine.redraw = False
    
    For lRow = 1 To grdLine.DataRowCnt
    iChecked = giGetValidInt(gsGridReadCell(grdLine, lRow, kColPickCheck))
        If iChecked = 1 Then
            With grdLine
                .Col = kColPickCheck
                .Row = lRow
                .Value = 0
            End With
            SetPickDefaults lRow, False 'Save after loop
        End If
    Next lRow
    moDMGrid.Save
    
    chkShowOnlyPicked.Value = 1 - chkShowOnlyPicked.Value
    chkShowOnlyPicked_Click
    chkShowOnlyPicked.Value = 1 - chkShowOnlyPicked.Value
    grdLine.redraw = True
    frmConfirmPicks.MousePointer = vbDefault

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "cmdClearPicks_Click", VBRIG_IS_FORM          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub cmdDeSelect_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdDeSelect, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
Dim bResult         As Boolean
Dim sWhereClause    As String
Dim sTablesUsed     As String
Dim iCounter       As Integer

    frmConfirmPicks.MousePointer = vbHourglass
    sbrMain.Status = SOTA_SB_BUSY
    For iCounter = 0 To UBound(moSelect)
        If tabSelect.TabEnabled(iCounter) Then
            sWhereClause = ""
            sTablesUsed = ""
            
        'Delete all rows in tmp table matching criteria in the Selection Grid and refresh DM/Line Grid
            If Not bBuildWhereClause(iCounter, sWhereClause, sTablesUsed) Then
                frmConfirmPicks.MousePointer = vbDefault
                sbrMain.Status = SOTA_SB_START
                Exit Sub
            End If
            If grdLine.DataRowCnt > 0 Then
                bResult = bCancelIMDist(sWhereClause, sTablesUsed)
            End If
            bResult = bDeSelectRows(iCounter, sWhereClause, sTablesUsed)
        End If
        
    Next iCounter
    
    SetFormState
    frmConfirmPicks.MousePointer = vbDefault
    sbrMain.Status = SOTA_SB_START
    'The following lines will hide the any visible navigators when lines are deleted,otherwise the navigators will stay in the same position in the grid.
    HideNavs
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        frmConfirmPicks.MousePointer = vbDefault
        gSetSotaErr Err, "frmConfirmPicks", "cmdDeSelect_Click", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub HideNavs()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    navGridSubItem.Visible = False
    navGridUOM.Visible = False

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "HideNavs", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub cmdLotSerialBin_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdLotSerialBin, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
Dim lRow As Long
Static bButtonClicked As Boolean

    If bButtonClicked Then Exit Sub
    
    bButtonClicked = True
    
    lRow = glGridGetActiveRow(grdLine)
    
    Me.MousePointer = vbHourglass
    
    If bValidQtyPicked(lRow) Then
        bResult = bLoadLotSerBinForm(lRow)
    Else
        grdLine_Click kColQtyPicked, lRow
        grdLine.SetFocus
    End If
    
    Me.MousePointer = vbDefault
    bButtonClicked = False
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                  'Repository Error Rig  {1.1.1.0.0}
        Me.MousePointer = vbDefault
        bButtonClicked = False
        gSetSotaErr Err, "frmConfirmPicks", "cmdLotSerialBin_Click", VBRIG_IS_FORM        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub cmdPickAll_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdPickAll, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
Dim iChecked    As Integer
Dim lRow        As Long
Dim sSQL    As String
    
    frmConfirmPicks.MousePointer = vbHourglass
    grdLine.redraw = False

    For lRow = 1 To grdLine.DataRowCnt
        iChecked = giGetValidInt(gsGridReadCell(grdLine, lRow, kColPickCheck))
        If iChecked = 0 Then
            With grdLine
                .Col = kColPickCheck
                .Row = lRow
                .Value = 1
            End With
            SetPickDefaults lRow, False, True 'Save after loop
        End If
    Next lRow
    moDMGrid.Save
    
    chkShowOnlyPicked.Value = 1 - chkShowOnlyPicked.Value
    chkShowOnlyPicked_Click
    chkShowOnlyPicked.Value = 1 - chkShowOnlyPicked.Value
    grdLine.redraw = True
    grdLine_Click kColQtyPicked, 1
    frmConfirmPicks.MousePointer = vbDefault

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "cmdPickAll_Click", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Sub

Private Sub cmdSelect_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdSelect, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
Dim bResult         As Boolean
Dim sWhereClause    As String
Dim sTablesUsed     As String
Dim sSQL            As String

Dim iCounter As Integer

        
    frmConfirmPicks.MousePointer = vbHourglass
    sbrMain.Status = SOTA_SB_BUSY
    On Error Resume Next
     'truncate work table
    sSQL = "TRUNCATE TABLE #tsoConfPickWrk1"
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        gClearSotaErr
    End If
    On Error GoTo VBRigErrorRoutine
    For iCounter = 0 To UBound(moSelect)
            sWhereClause = ""
            sTablesUsed = ""
        
        If tabSelect.TabEnabled(iCounter) Then
            
            If Not bBuildWhereClause(iCounter, sWhereClause, sTablesUsed) Then
                frmConfirmPicks.MousePointer = vbDefault
                sbrMain.Status = SOTA_SB_START
                Exit Sub
            End If
            moDMGrid.Save 'Write all dirty rows to database
            bResult = bSelectRows(iCounter, sWhereClause, sTablesUsed)
        
        End If
        
    Next iCounter
    
    'Call SP to populate work table
    If Not bPopulateWrkTables Then
        frmConfirmPicks.MousePointer = vbDefault
        sbrMain.Status = SOTA_SB_START
        Exit Sub
    End If
    moDMGrid.Refresh
    If Not bSetCellStates(True) Then
        frmConfirmPicks.MousePointer = vbDefault
        sbrMain.Status = SOTA_SB_START
        Exit Sub
    End If
    SetFormState
    cmdLotSerialBin.Enabled = False
    lkuAutoDistBin.SetTextAndKeyValue "", 0
    lkuAutoDistBin.Enabled = False
    
    frmConfirmPicks.MousePointer = vbDefault
    sbrMain.Status = SOTA_SB_START
    'Hide all unpicked rows if required that have been added to the grid
    If chkShowOnlyPicked.Value = 1 Then
        HideUnpicked
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        frmConfirmPicks.MousePointer = vbDefault
        gSetSotaErr Err, "frmConfirmPicks", "cmdSelect_Click", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetFormState()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iNbrSelected    As Integer

    iNbrSelected = glGridGetDataRowCnt(grdLine)
    If iNbrSelected = 0 Then
        cmdPickAll.Enabled = False
        cmdClearPicks.Enabled = False
        chkShowOnlyPicked.Enabled = False
        cmdDeSelect.Enabled = False
        cmdLotSerialBin.Enabled = False
        lkuAutoDistBin.SetTextAndKeyValue "", 0
        lkuAutoDistBin.Enabled = False
        tbrMain.ButtonEnabled(kTbFinishExit) = False
        tbrMain.ButtonEnabled(kTbFinish) = False
        tbrMain.ButtonEnabled(kTbCancel) = False
    Else
        cmdPickAll.Enabled = True
        cmdClearPicks.Enabled = True
        chkShowOnlyPicked.Enabled = True
        cmdDeSelect.Enabled = True
        tbrMain.ButtonEnabled(kTbFinishExit) = True
        tbrMain.ButtonEnabled(kTbFinish) = True
        tbrMain.ButtonEnabled(kTbCancel) = True
    End If
    
    'Bring the bin auto-distribution lookup to the front.
    lkuAutoDistBin.ZOrder 0
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "SetFormState", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bSetCellStates(Optional ByVal bSkipEnableDist As Boolean = False) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lRow                As Long
    'Loop through Grid validating row by row
    bSetCellStates = False
    For lRow = 1 To (glGridGetDataRowCnt(grdLine))
        SetRowValues lRow, bSkipEnableDist
    Next lRow
    bSetCellStates = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "bSetCellStates", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub SetRowValues(ByVal lRow As Long, Optional ByVal bSkipEnableDist As Boolean = False)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iAllowSubItem       As Integer
Dim iTrackMeth          As Integer
Dim iTrackQtyAtBin      As Integer
Dim dQty                As Double
Dim dKitShipLineKey     As Double
Dim sSubItemID          As String
Dim lWhseKey            As Long
Dim lUOMKey             As Long
Dim lItemKey            As Long
Dim iAllowDecimalQty    As Integer
Dim sSQL                As String
Dim iItemType           As Integer
Dim lSubItemKey         As Long


    iAllowSubItem = giGetValidInt(gsGridReadCell(grdLine, lRow, kColAllowSubItem))
    iTrackMeth = giGetValidInt(gsGridReadCell(grdLine, lRow, kColTrackMeth))
    iTrackQtyAtBin = giGetValidInt(gsGridReadCell(grdLine, lRow, kColTrackQtyAtBin))
    dQty = gdGetValidDbl(gsGridReadCell(grdLine, lRow, kColQtyPicked))
    dKitShipLineKey = gdGetValidDbl(gsGridReadCell(grdLine, lRow, kColKitLineKey))
    sSubItemID = gsGetValidStr(gsGridReadCell(grdLine, lRow, kColSubItem))
    lWhseKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "WhseKey"))
    lSubItemKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "SubItemKey"))
    iAllowDecimalQty = giGetValidInt(gsGridReadCell(grdLine, lRow, kColAllowDecimalQty))
    iItemType = giGetValidInt(gsGridReadCell(grdLine, lRow, kColItemType))
    lItemKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ItemKey"))
    
    If lItemKey > 0 Then
        sSQL = "ItemKey = " & CStr(lItemKey)
        iTrackMeth = gsGetValidStr(moAppDB.Lookup("TrackMeth", "timItem", sSQL))
    End If
    
    lUOMKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ShipUnitMeasKey"))
    
    If iAllowSubItem = 0 Or dQty <= 0 Or dKitShipLineKey > 0 Then
        gGridLockCell grdLine, kColSubItem, lRow
    Else
        gGridUnlockCell grdLine, kColSubItem, lRow
    End If
    
    'UOM changes are not allowed for:
    'Serialized Items            Kit Component Items
    'Substituted Items           Items not allowing Deciaml Qtys
    If iAllowDecimalQty = 0 Or (iTrackMeth > 1) Or (dKitShipLineKey > 0) Or (lSubItemKey <> lItemKey And lSubItemKey > 0) _
            Or iItemType < 5 Or iItemType = 7 Then
            gGridLockCell grdLine, kColUOM, lRow
    Else
        gGridUnlockCell grdLine, kColUOM, lRow
    End If
    
    If Not bSkipEnableDist Then
        
        If (iTrackMeth > 0 Or iTrackQtyAtBin = 1) And mbIMIntegrated Then
            cmdLotSerialBin.Enabled = True
            CreateDistForm lRow
            lkuAutoDistBin.Enabled = moItemDist.bEnableBinLkuCtrl(lWhseKey, lItemKey)
        Else
            cmdLotSerialBin.Enabled = False
            lkuAutoDistBin.SetTextAndKeyValue "", 0
            lkuAutoDistBin.Enabled = False
        End If
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "SetRowValues", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bConvertQtyForNewUOM(lRow As Long, lItemKey As Long, lOldUOMKey As Long, lNewUOMKey As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
Dim dOldQtyToPick   As Double
Dim dOldQtyPicked   As Double
Dim dNewQtyToPick   As Double
Dim dNewQtyPicked   As Double
Dim dConvFactor     As Double
Dim dQty            As Double
Dim lRetVal         As Long
    
    bConvertQtyForNewUOM = False
    '-- Convert the shipping quantities when the UOM is changed
    dOldQtyToPick = gdGetValidDbl(gsGridReadCell(grdLine, lRow, kColQtyToPick))
    'Per design, to fixing scopus #20060, when user changes ship/picked unit of measure,
    'do not recalculated the qty picked in the grid
    'dOldQtyPicked = gdGetValidDbl(gsGridReadCell(grdLine, lRow, kColQtyPicked))
    
    '-- Get the conversion factor
    With moAppDB
        .SetInParam msCompanyID
        .SetInParam lItemKey
        .SetInParam lOldUOMKey
        .SetInParam dOldQtyToPick
        'fixing scopus #20060, do not recalculate the QtyPicked field
        '.SetInParam dOldQtyPicked
        .SetInParam 0
        .SetInParam lNewUOMKey
        .SetOutParam lRetVal
        .SetOutParam dNewQtyToPick
        .SetOutParam dNewQtyPicked
        .SetOutParam dConvFactor
        
        .ExecuteSP "spsoValidUOMConv"
        
        lRetVal = glGetValidLong(.GetOutParam(7))
        dNewQtyToPick = gdGetValidDbl(.GetOutParam(8))
        dNewQtyPicked = gdGetValidDbl(.GetOutParam(9))
        dConvFactor = gdGetValidDbl(.GetOutParam(10))

        .ReleaseParams
    End With
    
    ' Check SP return value
    Select Case lRetVal
        Case 1 'Success
        Case 2 'Repeating Decimal Problem
            giSotaMsgBox Me, moSysSession, kSOInvalidUOMConv
            Exit Function
        Case Else
            giSotaMsgBox Me, moSysSession, kSOmsgSPError, lRetVal, "spsoValidUOMConv"
            Exit Function
    End Select
    
    '-- Now update the quantities
    moDMGrid.SetCellValue lRow, kColQtyToPick, SQL_DECIMAL, dNewQtyToPick
    'Fixing scopus #20060, per design, do not recalculate the QtyPicked field
    'moDMGrid.SetCellValue lRow, kColQtyPicked, SQL_DECIMAL, dNewQtyPicked
    bConvertQtyForNewUOM = True
    Exit Function

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "bConvertQtyForNewUOM", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bPopulateWrkTables() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lRet        As Long

    bPopulateWrkTables = False
    
    lRet = 0
    With moAppDB
        .SetInParam msCompanyID
        .SetOutParam lRet
        .ExecuteSP ("spsoConfirmPicks")
        If Err.Number <> 0 Then
            lRet = Err.Number
        Else
            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
            'errors after each attempt.  After storing all return values in local variables OR
            'encountering an error, release parameters.
            lRet = .GetOutParam(2)
        End If
        .ReleaseParams
     End With
     
     Select Case lRet
        Case 0 'sp did not complete successfully
            giSotaMsgBox Me, moSysSession, kSOmsgIMServicesError, lRet   'inform user that there are problems
            Exit Function
        Case 1 'no errors
        Case Else
            giSotaMsgBox Me, moClass.moSysSession, kSOmsgIMServicesError, lRet, "spcmInsertBankTran"  'inform user that there are problems
            Exit Function
    End Select
    
    bPopulateWrkTables = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "bPopulateWrkTables", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bBuildWhereClause(ByRef iTranType As Integer, ByRef sWhereClause As String, ByRef sTablesUsed As String) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iresult         As Integer
    
    bBuildWhereClause = False
    
    'Validate Selection Grid
    iresult = moSelect(iTranType).lValidateGrid(True) 'True indicates than any missing criteria will be set to 'All'
    If iresult <> 0 Then
        giSotaMsgBox Me, moSysSession, kSOmsgSelCriteriaInvalid
        Exit Function
    End If

        tabSelect.TabCaption(iTranType) = moMultSelGridMgr.sBuildCaption(msBaseCaption(iTranType), _
grdSelection(iTranType), tabSelect.TabEnabled(iTranType))

    If Not moSelect(iTranType).bGetWhereClause(sWhereClause, sTablesUsed, 0, False, , , , "") Then
        giSotaMsgBox Me, moSysSession, kSOmsgSelCriteriaInvalid
        Exit Function
    End If
    
    Select Case iTranType
        
        Case kSalesOrder
        
            sTablesUsed = "tsoShipLine WITH (NOLOCK),tsoShipLineDist WITH (NOLOCK),tsoSOLineDist WITH (NOLOCK),tsoSOLine WITH (NOLOCK),tsoSalesOrder WITH (NOLOCK),timItem WITH (NOLOCK)"
            AddTableIfMissing sWhereClause, sTablesUsed, "tsoPickList"
            AddTableIfMissing sWhereClause, sTablesUsed, "timWarehouse"
            AddTableIfMissing sWhereClause, sTablesUsed, "tciShipMethod"
            AddTableIfMissing sWhereClause, sTablesUsed, "tarCustomer"
            AddTableIfMissing sWhereClause, sTablesUsed, "tarCustAddr"
        
        
            With moSelect(kSalesOrder)
                .AddJoinIfNecessary sWhereClause, sTablesUsed, "tsoPickList", "tsoShipLine.PickListKey = tsoPickList.PickListKey"
                .AddJoinIfNecessary sWhereClause, sTablesUsed, "timWarehouse", "tsoSOLineDist.WhseKey = timWarehouse.WhseKey"
                .AddJoinIfNecessary sWhereClause, sTablesUsed, "timItem", "tsoSOLine.ItemKey = timItem.ItemKey"
                .AddJoinIfNecessary sWhereClause, sTablesUsed, "tciShipMethod", "tsoSOLineDist.ShipMethKey = tciShipMethod.ShipMethKey"
                .AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustomer", "tsoSalesOrder.CustKey = tarCustomer.CustKey"
                .AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustAddr", "tsoSOLineDist.ShipToCustAddrKey = tarCustAddr.AddrKey"
                If sWhereClause = vbNullString Then
                    sWhereClause = sWhereClause & "tsoShipLineDist.ShipLineKey = tsoShipLine.ShipLineKey AND " & _
                        "tsoShipLine.SOLineKey = tsoSOLine.SOLineKey AND tsoSOLineDist.SOLineKey = tsoSOLine.SOLineKey " & _
                        "AND tsoSOLine.SOKey = tsoSalesOrder.SOKey AND tsoShipLineDist.SOLineDistKey = tsoSOLineDist.SOLineDistKey "

                Else
                    sWhereClause = sWhereClause & " AND tsoShipLineDist.ShipLineKey = tsoShipLine.ShipLineKey AND " & _
                        "tsoShipLine.SOLineKey = tsoSOLine.SOLineKey AND tsoSOLineDist.SOLineKey = tsoSOLine.SOLineKey AND " & _
                        "tsoSOLine.SOKey = tsoSalesOrder.SOKey AND tsoShipLineDist.SOLineDistKey = tsoSOLineDist.SOLineDistKey "

                End If
            End With
            bBuildWhereClause = True
        Case kTransferOrder
        
        
        sTablesUsed = "tsoShipLine WITH (NOLOCK),tsoShipLineDist WITH (NOLOCK),timTrnsfrOrderLine WITH (NOLOCK),vimTrnsfrOrder WITH (NOLOCK),timItem WITH (NOLOCK),timWarehouseRcvg WITH (NOLOCK)"
            AddTableIfMissing sWhereClause, sTablesUsed, "tsoPickList"
            AddTableIfMissing sWhereClause, sTablesUsed, "timWarehouseShip"
            AddTableIfMissing sWhereClause, sTablesUsed, "tciShipMethod"
            'AddTableIfMissing sWhereClause, sTablesUsed, "vimTrnsfrOrder"
            'AddTableIfMissing sWhereClause, sTablesUsed, "tarCustAddr"
                    
            With moSelect(kTransferOrder)
                .AddJoinIfNecessary sWhereClause, sTablesUsed, "tsoPickList", "tsoShipLine.PickListKey = tsoPickList.PickListKey"
                '.AddJoinIfNecessary sWhereClause, sTablesUsed, "timWarehouse", "timTrnsfrOrder.ShipWhseKey = timWarehouse.WhseKey"
                .AddJoinIfNecessary sWhereClause, sTablesUsed, "timItem", "timTrnsfrOrderLine.ItemKey = timItem.ItemKey"
                .AddJoinIfNecessary sWhereClause, sTablesUsed, "tciShipMethod", "vimTrnsfrOrder.ShipMethKey = tciShipMethod.ShipMethKey"
                '.AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustomer", "tsoSalesOrder.CustKey = tarCustomer.CustKey"
                '.AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustAddr", "tsoSOLineDist.ShipToCustAddrKey = tarCustAddr.AddrKey"
                If sWhereClause = vbNullString Then
                    sWhereClause = sWhereClause & "tsoShipLineDist.ShipLineKey = tsoShipLine.ShipLineKey AND " & _
                        "tsoShipLine.TrnsfrOrderLineKey = timTrnsfrOrderLine.TrnsfrOrderLineKey " & _
                        "AND timTrnsfrOrderLine.TrnsfrOrderKey = vimTrnsfrOrder.TrnsfrOrderKey "
                Else
                    sWhereClause = sWhereClause & " AND tsoShipLineDist.ShipLineKey = tsoShipLine.ShipLineKey AND " & _
                        "tsoShipLine.TrnsfrOrderLineKey = timTrnsfrOrderLine.TrnsfrOrderLineKey " & _
                        "AND timTrnsfrOrderLine.TrnsfrOrderKey = vimTrnsfrOrder.TrnsfrOrderKey "
                End If
            End With
            bBuildWhereClause = True
    
    End Select
    
            
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "bBuildWhereClause", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub AddTableIfMissing(ByRef sWhereClause As String, ByRef sTablesUsed As String, sTableName As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'This Sub handles the Key Relationships if there is more than on level of indirection, so that the sTablesUsed works with the sWhereClause
    If InStr(1, sWhereClause, sTableName, vbTextCompare) > 0 And _
InStr(1, sTablesUsed, sTableName, vbTextCompare) = 0 Then
        sTablesUsed = sTablesUsed & ", " & sTableName & " WITH (NOLOCK)"
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "AddTableIfMissing", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bSelectRows(iTranType As Integer, sWhereClause As String, sTablesUsed As String) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String
    
    bSelectRows = False
    
    sSQL = "INSERT INTO #tsoConfPickWrk1 "
    sSQL = sSQL & "(ShipLineDistKey,ShipLineKey,KitShipLineKey,SOLineDistKey," & _
            "SOLineKey,SOKey,PickListKey,PickListNo,PickListLineNo,ItemKey,SubItemKey," & _
            "QtyToPick,QtyPicked,ShipUnitMeasKey,ShipToCustAddrKey,ShipMethKey,WhseKey,CustKey," & _
            "PickStatus,InvtTranID,InvtTranKey,AllowSubItem,TrackMeth,TrackQtyAtBin,SOLineUOMKey," & _
            "AllowDecimalQty,Confirmed,LineUpdateCounter,DistUpdateCounter,ItemType,RcvgWhseID,TranType,TranTypeText, QtyOrdered) "
    sSQL = sSQL & "SELECT tsoShipLineDist.ShipLineDistKey, "
    sSQL = sSQL & "tsoShipLine.ShipLineKey, "
    sSQL = sSQL & "tsoShipLine.KitShipLineKey, "

    
    Select Case iTranType
    
        Case kSalesOrder
        
            sSQL = sSQL & "tsoSOLineDist.SOLineDistKey, "
            sSQL = sSQL & "tsoSOLine.SOLineKey, "
            sSQL = sSQL & "tsoSalesOrder.SOKey, "
            sSQL = sSQL & "tsoShipLine.PickListKey, "
            sSQL = sSQL & "'', "
            sSQL = sSQL & "tsoShipLine.PickListLineNo, "
            sSQL = sSQL & "tsoShipLine.ItemKey, "
            sSQL = sSQL & "CASE WHEN tsoShipLine.ItemKey <> tsoShipLine.ItemShippedKey THEN tsoShipLine.ItemShippedKey ELSE NULL END, "
            sSQL = sSQL & "tsoShipLineDist.QtyToPick, "
            sSQL = sSQL & "tsoShipLineDist.QtyShipped, "
            sSQL = sSQL & "tsoShipLine.ShipUnitMeasKey, "
            sSQL = sSQL & "tsoSOLineDist.ShipToCustAddrKey, "
            sSQL = sSQL & "tsoSOLineDist.ShipMethKey, "
            sSQL = sSQL & "tsoSOLineDist.WhseKey, "
            sSQL = sSQL & "tsoSalesOrder.CustKey, "
            sSQL = sSQL & "tsoShipLine.ConfirmedPick, "
            sSQL = sSQL & "0, "
            sSQL = sSQL & "tsoShipLine.InvtTranKey, "
            sSQL = sSQL & "0, "
            sSQL = sSQL & "0, "
            sSQL = sSQL & "0, "
            sSQL = sSQL & "0, "
            sSQL = sSQL & "0, "
            sSQL = sSQL & "tsoShipLine.ConfirmedPick, "
            sSQL = sSQL & "tsoShipLine.UpdateCounter, "
            sSQL = sSQL & "tsoShipLineDist.UpdateCounter, "
        '   Defect #13994: We need to know the ItemType.  It was updating it in
        '                  spsoGetInvtfo during save time. We need it for validation
        '                  when the user leaves a line.
        '    sSQL = sSQL & "0 "
            sSQL = sSQL & "timItem.ItemType, "
            sSQL = sSQL & "NULL, "
            sSQL = sSQL & CStr(810) & ", "
            sSQL = sSQL & gsQuoted(gsBuildString(kSOSale, moAppDB, moSysSession)) & ", "
            sSQL = sSQL & "tsoSOLineDist.QtyOrd "
            sSQL = sSQL & "FROM " & sTablesUsed & " "
            sSQL = sSQL & "WHERE " & sWhereClause & " "
            'restrict to Sales Orders only
            sSQL = sSQL & "AND tsoSalesOrder.TranType = " & kTranTypeSOSO & " "
            'restrict to Sales Orders that are open
            sSQL = sSQL & "AND tsoSalesOrder.Status = " & kSOOpenStatus & " "
            'excluse SOs on hold or credit hold
            sSQL = sSQL & "AND tsoSalesOrder.Hold = " & kSONoHold & " "
            sSQL = sSQL & "AND tsoSalesOrder.CrHold = " & kSONoCrHold & " "
            'only select open lines
            sSQL = sSQL & "AND tsoSOLine.Status = " & kLineOpen & " "
            'Only select open distribution lines
            sSQL = sSQL & "AND tsoSOLineDist.Status = " & kLineDistOpen & " "
            'only select distribution lines not on hold
            sSQL = sSQL & "AND tsoSOLineDist.Hold = " & kSONoHold & " "
            'only select lines with QtyOpenToShip > 0 for current company
            sSQL = sSQL & "AND tsoSOLineDist.QtyOpenToShip > 0 "
            'exclude drop ship items
            sSQL = sSQL & "AND tsoSOLineDist.DeliveryMeth <> 2 "
            'exclude non-inventory items
            sSQL = sSQL & "AND timItem.ItemType IN (" & _
                        klTypeFinishedGood & ", " & klTypeRawMaterial & ", " & klTypeBTOKit & _
                        ", " & klTypeAssembledKit & ")"
            sSQL = sSQL & "AND tsoSalesOrder.CompanyID = '" & msCompanyID & "' "
            sSQL = sSQL & "AND tsoShipLine.ShipKey IS NULL "
            'exclude pick lines in process
            sSQL = sSQL & "AND tsoShipLine.PickingComplete = 1"
    
        Case kTransferOrder
        
                sSQL = sSQL & " NULL, "
                sSQL = sSQL & " timTrnsfrOrderLine.TrnsfrOrderLineKey, "
                sSQL = sSQL & " timTrnsfrOrderLine.TrnsfrOrderKey, "
                sSQL = sSQL & "tsoShipLine.PickListKey, "
                sSQL = sSQL & "'', "
                sSQL = sSQL & "tsoShipLine.PickListLineNo, "
                sSQL = sSQL & "tsoShipLine.ItemKey, "
                sSQL = sSQL & "tsoShipLine.ItemShippedKey, "
                sSQL = sSQL & "tsoShipLineDist.QtyToPick, "
                sSQL = sSQL & "tsoShipLineDist.QtyShipped, "
                sSQL = sSQL & "tsoShipLine.ShipUnitMeasKey, "
                sSQL = sSQL & "NULL, "
                sSQL = sSQL & "vimTrnsfrOrder.ShipMethKey, "
                sSQL = sSQL & "vimTrnsfrOrder.ShipWhseKey, "
                sSQL = sSQL & "NULL, "
                sSQL = sSQL & "tsoShipLine.ConfirmedPick, "
                sSQL = sSQL & "0, "
                sSQL = sSQL & "tsoShipLine.InvtTranKey, "
                sSQL = sSQL & "0, "
                sSQL = sSQL & "0, "
                sSQL = sSQL & "0, "
                sSQL = sSQL & "0, "
                sSQL = sSQL & "0, "
                sSQL = sSQL & "tsoShipLine.ConfirmedPick, "
                sSQL = sSQL & "tsoShipLine.UpdateCounter, "
                sSQL = sSQL & "tsoShipLineDist.UpdateCounter, "
                sSQL = sSQL & "timItem.ItemType, "
                sSQL = sSQL & "timWarehouseRcvg.WhseID, "
                sSQL = sSQL & kTranTypeSOTS & ", "
                sSQL = sSQL & gsQuoted(gsBuildString(kSOTransfer, moAppDB, moSysSession)) & ", "
                sSQL = sSQL & "timTrnsfrOrderLine.QtyOrd "
                sSQL = sSQL & " FROM " & sTablesUsed _
                        & " WHERE " & sWhereClause _
                        & " AND vimTrnsfrOrder.Status = " & kOpen _
                        & " AND timTrnsfrOrderLine.Status = " & kOpen _
                        & " AND (timTrnsfrOrderLine.QtyOrd - timTrnsfrOrderLine.QtyShip) > 0" _
                        & " AND vimTrnsfrOrder.CompanyID = " & gsQuoted(msCompanyID) _
                        & " AND timWarehouseRcvg.WhseKey = vimTrnsfrOrder.RcvgWhseKey"
                sSQL = sSQL & " AND tsoShipLine.ShipKey IS NULL "
    End Select
    'Debug.Print sSQL
    
    moAppDB.ExecuteSQL sSQL
     bSelectRows = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "bSelectRows", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bCancelIMDist(ByVal sWhereClause As String, ByVal sTablesUsed As String) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL            As String
Dim lRow            As Long
Dim lInvtTranID     As Long
Dim lInvtTranKey    As Long
Dim lItemKey        As Long
Dim lOrigItemKey    As Long
Dim lShipLineDistKey As Long

    bCancelIMDist = False
    
    On Error Resume Next
    'truncate work table
    sSQL = "TRUNCATE TABLE #tsoConfPickWrk3"
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        gClearSotaErr
    End If
    On Error GoTo VBRigErrorRoutine

    moDMGrid.Save 'Write all dirty rows to database
    
    sSQL = "INSERT INTO #tsoConfPickWrk3 (ShipLineDistKey, InvtTranID, InvtTranKey, ItemKey, SubItemKey ) "
    sSQL = sSQL & "(SELECT tsoShipLineDist.ShipLineDistKey, #tsoConfPickWrk2.InvtTranID, #tsoConfPickWrk2.InvtTranKey, " & _
            "#tsoConfPickWrk2.ItemKey, #tsoConfPickWrk2.SubItemKey "
    If sWhereClause <> vbNullString Then
        If InStr(1, sWhereClause, "tsoSOLine.", vbTextCompare) <> 0 And _
                InStr(1, sTablesUsed, "tsoSOLine", vbTextCompare) = 0 Then
            sTablesUsed = sTablesUsed & ",tsoSOLine WITH (NOLOCK) "
        End If
        If InStr(1, sWhereClause, "tsoSalesOrder.", vbTextCompare) <> 0 And _
                InStr(1, sTablesUsed, "tsoSalesOrder", vbTextCompare) = 0 Then
            sTablesUsed = sTablesUsed & ",tsoSalesOrder WITH (NOLOCK) "
        End If
        If InStr(1, sWhereClause, "tsoShipLine.", vbTextCompare) <> 0 And _
                InStr(1, sTablesUsed, "tsoShipLine", vbTextCompare) = 0 Then
            sTablesUsed = sTablesUsed & ",tsoShipLine WITH (NOLOCK) "
        End If
        sWhereClause = sWhereClause & " AND tsoShipLineDist.ShipLineDistKey = #tsoConfPickWrk2.ShipLineDistKey"
        sWhereClause = sWhereClause & " AND #tsoConfPickWrk2.Confirmed = 0 AND #tsoConfPickWrk2.QtyPicked = 0 "
        sWhereClause = sWhereClause & " AND ((#tsoConfPickWrk2.InvtTranID > 0) OR (#tsoConfPickWrk2.InvtTranKey > 0)) "
    Else
        sWhereClause = sWhereClause & " WHERE tsoShipLineDist.ShipLineDistKey = #tsoConfPickWrk2.ShipLineDistKey "
        sWhereClause = sWhereClause & " AND #tsoConfPickWrk2.Confirmed = 0 AND #tsoConfPickWrk2.QtyPicked = 0 "
        sWhereClause = sWhereClause & " AND ((#tsoConfPickWrk2.InvtTranID > 0) OR (#tsoConfPickWrk2.InvtTranKey > 0)) "
    End If
    sTablesUsed = sTablesUsed & ",#tsoConfPickWrk2 "
    sSQL = sSQL & "FROM " & sTablesUsed & " "
    sSQL = sSQL & "WHERE " & sWhereClause & ")"
    
    'Debug.Print sSQL
    
    moAppDB.ExecuteSQL sSQL

    moDMGridInvt.Refresh
    
    CreateDistForm 1 'need some value for init of distribution form
    
    For lRow = 1 To grdLineInvt.DataRowCnt
        lInvtTranID = glGetValidLong(gsGridReadCell(grdLineInvt, lRow, kColInvtInvtTranID))
        lInvtTranKey = glGetValidLong(gsGridReadCell(grdLineInvt, lRow, kColInvtInvtTranKey))
        lItemKey = glGetValidLong(moDMGridInvt.GetColumnValue(lRow, "SubItemKey"))
        lShipLineDistKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ShipLineDistKey"))

        If lItemKey = 0 Then
            lOrigItemKey = 0
            lItemKey = glGetValidLong(moDMGridInvt.GetColumnValue(lRow, "ItemKey"))
        Else
            lOrigItemKey = glGetValidLong(moDMGridInvt.GetColumnValue(lRow, "ItemKey"))
        End If
        
        '-- Clear out Item Distributions from work tables
        If lInvtTranKey > 0 Then
            If Not (moItemDist.Delete(lInvtTranKey, lItemKey, kiEffectOnInventory, 0, 0, lOrigItemKey, lShipLineDistKey)) Then
                With moItemDist
                    If .oerror.Number <> 0 Then
                        giSotaMsgBox Me, moSysSession, .oerror.messageconstant
                        .oerror.Clear
                        Exit Function
                    End If
                End With
                Exit Function
            End If
            moAppDB.ExecuteSQL "DELETE #timInvtDistWrk WHERE TranKey = " & glGetValidLong(lInvtTranKey)
        Else
            If lInvtTranID > 0 Then
                If Not moItemDist Is Nothing Then
                    If Not (bCancelInvtTranID(lInvtTranID)) Then
                        Exit Function
                    End If
                End If
            End If
        End If
    Next lRow
        
    bCancelIMDist = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "bCancelIMDist", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Function

Private Function bDeSelectRows(iTranType As Integer, sWhereClause As String, sTablesUsed As String, Optional bRefreshGrid As Boolean = True) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String
Dim lRow As Long

    bDeSelectRows = False
    
    sSQL = "DELETE FROM #tsoConfPickWrk2 WHERE #tsoConfPickWrk2.ShipLineDistKey IN "
    sSQL = sSQL & "(SELECT tsoShipLineDist.ShipLineDistKey "
    
    Select Case iTranType
        Case kSalesOrder
        
'            If sWhereClause <> vbNullString Then
'                If InStr(1, sWhereClause, "tsoSOLine.", vbTextCompare) = 0 Then
'                    sTablesUsed = sTablesUsed & ",tsoSOLine "
'                End If
'                If InStr(1, sWhereClause, "tsoSalesOrder.", vbTextCompare) = 0 Then
'                    sTablesUsed = sTablesUsed & ",tsoSalesOrder "
'                End If
'                If InStr(1, sWhereClause, "tsoPickList.", vbTextCompare) = 0 Then
'                    sTablesUsed = sTablesUsed & ",tsoPickList "
'                End If
'                If InStr(1, sWhereClause, "tsoShipLine.", vbTextCompare) = 0 Then
'                    sTablesUsed = sTablesUsed & ",tsoShipLine "
'                End If
'                If InStr(1, sWhereClause, "timTrnsfrOrderLine.", vbTextCompare) = 0 Then
'                    sTablesUsed = sTablesUsed & ",timTrnsfrOrderLine "
'                End If
'                If InStr(1, sWhereClause, "timTrnsfrOrder.", vbTextCompare) = 0 Then
'                    sTablesUsed = sTablesUsed & ",timTrnsfrOrder "
'                End If
'
'            Else
'            End If

                sWhereClause = sWhereClause & " AND tsoSalesOrder.TranType = " & kTranTypeSOSO & " "
                'restrict to Sales Orders that are open
                sWhereClause = sWhereClause & " AND tsoSalesOrder.Status = " & kSOOpenStatus & " "
                'excluse SOs on hold or credit hold
                sWhereClause = sWhereClause & " AND tsoSalesOrder.Hold = " & kSONoHold & " "
                sWhereClause = sWhereClause & " AND tsoSalesOrder.CrHold = " & kSONoCrHold & " "
                'only select open lines
                sWhereClause = sWhereClause & " AND tsoSOLine.Status = " & kLineOpen & " "
                'Only select open distribution lines
                sWhereClause = sWhereClause & " AND tsoSOLineDist.Status = " & kLineDistOpen & " "
                'only select distribution lines not on hold
                sWhereClause = sWhereClause & " AND tsoSOLineDist.Hold = " & kSONoHold & " "
                'only select lines with QtyOpenToShip > 0 for current company
                sWhereClause = sWhereClause & " AND tsoSOLineDist.QtyOpenToShip > 0 "
                'exclude drop ship items
                sWhereClause = sWhereClause & " AND tsoSOLineDist.DeliveryMeth <> 2 "
                'exclude non-inventory items
                sWhereClause = sWhereClause & " AND timItem.ItemType IN (" & _
                                klTypeFinishedGood & ", " & klTypeRawMaterial & ", " & klTypeBTOKit & _
                                ", " & klTypeAssembledKit & ") "
                sWhereClause = sWhereClause & " AND tsoSalesOrder.CompanyID = " & gsQuoted(msCompanyID) & " "
                sWhereClause = sWhereClause & " AND tsoShipLine.ShipKey IS NULL "

                AddTableIfMissing sWhereClause, sTablesUsed, "tsoSOLine"
                AddTableIfMissing sWhereClause, sTablesUsed, "tsoSalesOrder"
                AddTableIfMissing sWhereClause, sTablesUsed, "tsoPickList"
                AddTableIfMissing sWhereClause, sTablesUsed, "tsoShipLine"
            
   
            
            Case kTransferOrder
            
                sWhereClause = sWhereClause & " AND vimTrnsfrOrder.Status = " & kOpen _
                                & " AND timTrnsfrOrderLine.Status = " & kOpen _
                                & " AND (timTrnsfrOrderLine.QtyOrd - timTrnsfrOrderLine.QtyShip) > 0" _
                                & " AND vimTrnsfrOrder.CompanyID = " & gsQuoted(msCompanyID)

            
                AddTableIfMissing sWhereClause, sTablesUsed, "timTrnsfrOrderLine"
                AddTableIfMissing sWhereClause, sTablesUsed, "timTrnsfrOrder"
                AddTableIfMissing sWhereClause, sTablesUsed, "tsoPickList"
                AddTableIfMissing sWhereClause, sTablesUsed, "tsoShipLine"
        End Select
        
    
    sSQL = sSQL & "FROM " & sTablesUsed & " "
    sSQL = sSQL & "WHERE " & sWhereClause & ")"
    
    
    'Debug.Print sSQL
    
    
    moAppDB.ExecuteSQL sSQL
    'If bRefreshGrid = True Then
        moDMGrid.Refresh
    'End If
    
    bDeSelectRows = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "bDeSelectRows", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bInitWorkTables() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String

    bInitWorkTables = False
    
    On Error Resume Next
    
    'Drop Temp Table if it exists
    sSQL = "DROP TABLE #tsoConfPickWrk1"
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        gClearSotaErr
    End If
    
    'Create an empty Temp Table
    sSQL = "SELECT * INTO #tsoConfPickWrk1 FROM tsoConfPickWrk WHERE 1 = 2"
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kSOmsgPickListWrkTable   'inform user that there are problems
        Exit Function
    End If

    'Drop Temp Table if it exists
    sSQL = "DROP TABLE #tsoConfPickWrk2"
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        gClearSotaErr
    End If
    
    'Create an empty Temp Table
    sSQL = "SELECT * INTO #tsoConfPickWrk2 FROM tsoConfPickWrk WHERE 1 = 2"
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kSOmsgPickListWrkTable   'inform user that there are problems
        Exit Function
    End If

    'Drop Temp Table if it exists
    sSQL = "DROP TABLE #tsoConfPickWrk3"
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        gClearSotaErr
    End If
    
    'Create an empty Temp Table
    sSQL = "SELECT * INTO #tsoConfPickWrk3 FROM tsoConfPickWrk WHERE 1 = 2"
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kSOmsgPickListWrkTable   'inform user that there are problems
        Exit Function
    End If
    
    'Create an empty Temp Tables for Kit validations
    sSQL = "CREATE TABLE #tsoConfKitQtyWrk (KitShipLineKey INTEGER NOT NULL,KitQty INTEGER NOT NULL)"
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        MsgBox "Errors occured trying to create work table #tsoConfKitQtyWrk"      'inform user that there are problems"
        Exit Function
    End If

    sSQL = "SELECT * INTO #tsoKitValidateWrk FROM tsoKitValidateWrk WHERE 1 = 2"
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        MsgBox "Errors occured trying to create work table #tsoKitValidateWrk"      'inform user that there are problems"
        Exit Function
    End If

    'Drop Temp Table if it exists
    sSQL = "DROP TABLE #tsoBinSumWrk"
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        gClearSotaErr
    End If
    
    'Create an empty Temp Table
    sSQL = "SELECT * INTO #tsoBinSumWrk FROM tsoConfPickWrk WHERE 1 = 2"
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kSOmsgPickListWrkTable   'inform user that there are problems
        Exit Function
    End If
    
    bInitWorkTables = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "bInitWorkTables", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
            tabSelect.TabVisible(3) = False
            tabSelect.TabsPerRow = tabSelect.TabsPerRow - 1
            moFormCust.Initialize Me, goClass, tabSelect.Name & ";3"
            Set moFormCust.CustToolbarMgr = tbrMain
            moFormCust.ApplyFormCust
        End If
    End If
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "Form_Activate", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub MapControls()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'*********************************************************************
'   Description:
'       MapControls will map the controls on the form to the buttons
'       that have Function key equivalents.  The general procedure
'       giCollectionAdd is used to add the button control as
'       an object Item to the collection and use the mapped
'       entry control's window handle as the key into the collection.
'*********************************************************************
    Set moMapSrch = New Collection
    giCollectionAdd moMapSrch, navControl, mskControl(kSalesOrder).hwnd
    giCollectionAdd moMapSrch, navControlTransfer, mskControl(kTransferOrder).hwnd
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "MapControls", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'*********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True process predefined function keys.
'*********************************************************************
    Dim iNavControl As Integer

    Select Case keycode 'normal function key processing
        Case vbKeyF1 To vbKeyF16              'Function Keys pressed
            gProcessFKeys Me, keycode, Shift  'Run Sage MAS 500 Function Key Processing
    End Select
    
    If Shift = 0 And keycode = vbKeyF5 Then
        If Me.ActiveControl.Name = "mskControl" Then
            iNavControl = moSelect(Me.ActiveControl.Index).SelectionGridF5KeyDown(Me.ActiveControl)
            If iNavControl > 0 Then
                Select Case Me.ActiveControl.Index
                    Case 0
                        navControl_GotFocus iNavControl
                    Case 1
                        navControlTransfer_GotFocus iNavControl
                End Select
            End If
        End If
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "Form_KeyDown", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'********************************************************************
'   Desc: Form_KeyPress traps the keys pressed when the KeyPreview
'         property of the form is set to True process the enter key
'         as if the user pressed the tab key.
'********************************************************************

    Select Case KeyAscii
        Case vbKeyReturn            'User pressed enter key
            If mbEnterAsTab Then    'enter as tab set in session
                'ltd - 19102 don't sendkeys tab, it will skip a line in the grid
                'gProcessSendKeys "{Tab}"    'send tab key
                KeyAscii = 0        'cancel enter key
            End If
        Case Else
            'Other KeyAscii routines.
    End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "Form_KeyPress", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub Form_Load()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'default to Form_Load error
    mbLoadSuccess = False
    mbLoading = True
    
    'set local variables
    With moClass
        Set moSysSession = .moSysSession
        Set moAppDB = moClass.moAppDB
        Set moAppDB = moClass.moAppDB
    End With
    
    With moSysSession
        msCompanyID = .CompanyId
        msUserID = .UserId
        mbEnterAsTab = .EnterAsTab
        mlLanguage = .Language
        msHomeCurrID = .CurrencyID
    
        If CBool(.IsModuleActivated(kModuleWM)) = True Then
            mbWMIsLicensed = .IsModuleLicensed(kModuleWM)
        Else
            mbWMIsLicensed = False
        End If
    End With
    
    mbIMIntegrated = moClass.moAppDB.Lookup("IntegrateWithIM", "tsoOptions", "CompanyID = " & gsQuoted(msCompanyID))
    
    'The Transfer grid should not be available if SO & IM are not integrated.
    '             - this block of code moved after moMultSelGridMgr.Initialize
    '             - chkInclude(kTransfer).Value triggers click, which references moMultSelGridMgr
    If Not mbIMIntegrated Or Not mbWMIsLicensed Then
        chkInclude(kTransferOrder).Enabled = False
        chkInclude(kTransferOrder).Value = vbUnchecked
        tabSelect.TabEnabled(kTransferOrder) = False
    Else
        chkInclude(kTransferOrder).Enabled = True
        chkInclude(kTransferOrder).Value = vbChecked
        tabSelect.TabEnabled(kTransferOrder) = True
    End If
    
    '-- Set up the form resizing variables
    With Me
        miMinFormHeight = .Height
        miMinFormWidth = .Width
        miOldFormHeight = .Height
        miOldFormWidth = .Width
    End With
    
    'Create Work Tables In TempDB
    If Not bInitWorkTables Then
        'Can't initialize Work Tables, so exit
        Exit Sub
    End If

    'Build local strings
    Me.Caption = gsBuildString(kSOConfirmPicks, moAppDB, moSysSession)
    
    sLotSerialBin = gsBuildString(kSOLotSerialBin, moAppDB, moSysSession)
    
    GetModuleOptions
    
    SetupSelectGrid
    
    SetupLineGrid
    
    SetupLineGridInvt
    
    BindLineGrid
    
    BindLineGridInvt
    
    BindGM
    
    BindContextMenu
    
    SetupBars
    
    SetFormState
    
    SetupMultipleGrid

    SetupSettings
    
    'map controls for function keys
    MapControls
    
    'need to capture left mouse clicks to prevent user from double clicking on command buttons after
    'selection grid navigator has been used.
    If moHook Is Nothing Then
        Set moHook = New AFWinHook.clsMsgs
        moHook.SetHook Me.hwnd, HOOK_TYPE_CUSTOM
        moHook.AddMessage WM_LBUTTONDOWN
        moHook.Delayed = True
    End If
      
    cmdLotSerialBin.Enabled = False
    lkuAutoDistBin.SetTextAndKeyValue "", 0
    lkuAutoDistBin.Enabled = False

    sbrMain.Status = SOTA_SB_START

    mbLoading = False
    
    'Form_Load completed successfully
    mbLoadSuccess = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "Form_Load", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub GetModuleOptions()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moModuleOptions = New clsModuleOptions
    With moModuleOptions
        Set .oSysSession = moSysSession
        Set .oAppDB = moAppDB
        .sCompanyID = msCompanyID
    End With
    miNbrDecPlaces = moModuleOptions.CI("QtyDecPlaces")
    miOverShipmentPolicy = giGetValidInt(moModuleOptions.SO("OverShipmentPolicy"))


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "GetModuleOptions", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetupSelectGrid()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Local Object Variables
    'Dim moDDData                As clsDDData
    'Dim sRealTableCollection    As Collection
    Dim moSelAssist         As clsSelGridAssist
    
    'Row Search Variables
    Dim bGoodRow                As Boolean
    Dim bGrdRestrictToCompany   As Boolean
    Dim sGrdTableName           As String
    Dim sGrdColumnName          As String
    Dim sGrdParentTblName       As String
    Dim lIndex                  As Long
    Dim lCurrRow                As Long
    Dim lRet                    As Long
    Dim bResult                 As Boolean
    Dim i                       As Integer
    Dim arrStaticValues()       As String
    Dim arrStaticText()         As String
    Dim sString                 As String
    
    'Instantiate Objects
    Set moSelect(kSalesOrder) = New clsSelection
    Set moSelect(kTransferOrder) = New clsSelection
    Set moDDData = New clsDDData
    Set sRealTableCollection = New Collection
    Set moSelAssist = New clsSelGridAssist
    
    
    'CUSTOMIZE:  Invoke Add method of sRealTableCollection for each table which
    'will provide raw data.  Primary table should appear first.
    sRealTableCollection.Add "tsoShipLineDist"
    
    moDDData.lInitDDData sRealTableCollection, moClass.moAppDB, moClass.moAppDB, msCompanyID

    If Not moDDData.lDDBuildData(App.EXEName) = kSuccess Then
        'Unable to load data from Data Dictionary.
        giSotaMsgBox Me, moSysSession, kmsgCantLoadDDData
        Exit Sub
    End If
    
           
    'Add Manual Row Entries for sales order grid
    With moSelAssist
        Set .Sys_DB = moAppDB
        Set .Selection = moSelect(kSalesOrder)
        .Add "tsoPickList", "PickListNo", gsBuildString(kPickList, moAppDB, moSysSession), , , "SOReprntPick"
        .Add "tsoShipLine", "PickListLineNo", gsBuildString(kPickLine, moAppDB, moSysSession), False
        .Add "tsoSOLineDist", "ShipDate", gsBuildString(kSchedDate, moAppDB, moSysSession), False, gsGetLocalDateMask()
        .Add "timWarehouse", "WhseID", gsBuildString(kShippingWhse, moAppDB, moSysSession)
        .Add "tsoSalesOrder", "TranNoRelChngOrd", gsBuildString(kSOTranNo, moAppDB, moSysSession), , , , " tsoSalesOrder.Status = " & kSOOpenStatus _
                                & " AND tsoSalesOrder.TranType = " & kTranTypeSOSO _
                                & " AND tsoSalesOrder.Hold = " & kSONoHold _
                                & " AND tsoSalesOrder.CrHold = " & kSONoCrHold
        .Add "tsoSOLine", "SOLineNo", gsBuildString(kSOSOLine, moAppDB, moSysSession), False
        .Add "timItem", "ItemID", , , , , " timItem.ItemType IN (" & _
                                klTypeFinishedGood & ", " & klTypeRawMaterial & ", " & klTypeBTOKit & _
                                ", " & klTypeAssembledKit & ")"
        .Add "tciShipMethod", "ShipMethID", gsBuildString(kSOShipvia, moAppDB, moSysSession)
        .Add "tarCustomer", "CustName"
        .Add "tarCustomer", "CustID", gsBuildString(kSOCustomer, moAppDB, moSysSession)
        .Add "tarCustAddr", "CustAddrID", gsBuildString(kSOShipToAddress, moAppDB, moSysSession), False
        .Add "tsoSalesOrder", "CustPONo"
     End With
    
    'Debug.Print gsBuildString(kShipWhse, moAppDB, moSysSession) & " = Ship Whse"
    'add static list for ship priority
    ReDim arrStaticValues(5)
    ReDim arrStaticText(5)
    For i = 1 To 5
        arrStaticValues(i) = i
        arrStaticText(i) = CStr(i)
    Next i
    
    With moSelect(kSalesOrder)
        lRet = .lAddSelRowStaticList("tsoSOLineDist" _
                    , "ShipPriority" _
                    , gsBuildString(kSOShipPriority, moAppDB, moSysSession) _
                    , gsBuildString(kSOShipPriority, moAppDB, moSysSession) _
                    , False _
                    , "" _
                    , arrStaticValues() _
                    , arrStaticText())
    End With
    
    'add static list for Delivery Method
    
    ReDim arrStaticValues(4)
    ReDim arrStaticText(4)
    
    sString = gsBuildString(kslvShip, moAppDB, moSysSession)
    arrStaticValues(1) = 1
    arrStaticText(1) = sString
    sString = gsBuildString(kslvDropShip, moAppDB, moSysSession)
    arrStaticValues(2) = 2
    arrStaticText(2) = sString
    sString = gsBuildString(kslvCounterSale, moAppDB, moSysSession)
    arrStaticValues(3) = 3
    arrStaticText(3) = sString
    sString = gsBuildString(kslvWillCall, moAppDB, moSysSession)
    arrStaticValues(4) = 4
    arrStaticText(4) = sString
    
    sString = gsBuildString(kSODeliveryMethod, moAppDB, moSysSession)
    
    With moSelect(kSalesOrder)
        lRet = .lAddSelRowStaticList("tsoSOLineDist", "DeliveryMeth", sString _
        , sString, False, "", arrStaticValues(), arrStaticText())
    End With
    
    
    If Not moSelect(kSalesOrder).bInitSelect(sRealTableCollection(1), mskControl(kSalesOrder), _
navControl, nbrControl(kSalesOrder), curControl(kSalesOrder), grdSelection(kSalesOrder), _
fraSelect(kSalesOrder), Me, moDDData, 19 * 120 + 60, 17 * 120, 18 * 120, 18 * 120, False) Then
        'Setup of the Selection Grid Failed.
        giSotaMsgBox Me, moSysSession, kmsgSetupSelectionGridFail

         'Exit this function
        Exit Sub
    End If

    moSelect(kSalesOrder).PopulateSelectionGrid
    
    'Hide Selection Rows Not Required
    For lIndex = 1 To moSelect(kSalesOrder).lMaxSelGridRows
        bGoodRow = False
        moSelect(kSalesOrder).lGetTblColFromGrdRow lIndex, sGrdTableName, sGrdColumnName, _
sGrdParentTblName, bGrdRestrictToCompany
        If sGrdTableName = "tsoPickList" And sGrdColumnName = "PickListNo" Then
            bGoodRow = True
        End If
        If sGrdTableName = "tsoShipLine" And sGrdColumnName = "PickListLineNo" Then
            bGoodRow = True
        End If
        If sGrdTableName = "tsoSOLineDist" And sGrdColumnName = "DeliveryMeth" Then
            bGoodRow = True
        End If
        If sGrdTableName = "tsoSOLineDist" And sGrdColumnName = "ShipDate" Then
            bGoodRow = True
        End If
        If sGrdTableName = "timWarehouse" And sGrdColumnName = "WhseID" Then
            bGoodRow = True
        End If
        If sGrdTableName = "tsoSOLineDist" And sGrdColumnName = "ShipPriority" Then
            bGoodRow = True
        End If
        If sGrdTableName = "tsoSalesOrder" And sGrdColumnName = "TranNoRelChngOrd" Then
            bGoodRow = True
        End If
        If sGrdTableName = "tsoSalesOrder" And sGrdColumnName = "CustPONo" Then
            bGoodRow = True
        End If
        If sGrdTableName = "tsoSOLine" And sGrdColumnName = "SOLineNo" Then
            bGoodRow = True
        End If
        If sGrdTableName = "timItem" And sGrdColumnName = "ItemID" Then
            bGoodRow = True
        End If
        If sGrdTableName = "tciShipMethod" And sGrdColumnName = "ShipMethID" Then
            bGoodRow = True
        End If
        If sGrdTableName = "tarCustomer" And sGrdColumnName = "CustID" Then
            bGoodRow = True
        End If
        If sGrdTableName = "tarCustomer" And sGrdColumnName = "CustName" Then
            bGoodRow = True
        End If
        If sGrdTableName = "tarCustAddr" And sGrdColumnName = "CustAddrID" Then
            bGoodRow = True
        End If
        If Not bGoodRow Then
            moSelect(kSalesOrder).SetSelectionRowVisibility sGrdTableName, sGrdColumnName, False
        End If
    Next lIndex
    
    'Order Rows
    With moSelect(kSalesOrder)
        lCurrRow = .lGetGrdRowFromTblCol("tsoPickList", "PickListNo")
        lRet = .lMoveRow(lCurrRow, 1)
        lCurrRow = .lGetGrdRowFromTblCol("tsoShipLine", "PickListLineNo")
        lRet = .lMoveRow(lCurrRow, 2)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSOLineDist", "DeliveryMeth")
        lRet = .lMoveRow(lCurrRow, 3)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSOLineDist", "ShipDate")
        lRet = .lMoveRow(lCurrRow, 4)
        lCurrRow = .lGetGrdRowFromTblCol("timWarehouse", "WhseID")
        lRet = .lMoveRow(lCurrRow, 5)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSOLineDist", "ShipPriority")
        lRet = .lMoveRow(lCurrRow, 6)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSalesOrder", "TranNoRelChngOrd")
        lRet = .lMoveRow(lCurrRow, 7)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSOLine", "SOLineNo")
        lRet = .lMoveRow(lCurrRow, 8)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSalesOrder", "CustPONo")
        lRet = .lMoveRow(lCurrRow, 9)
        lCurrRow = .lGetGrdRowFromTblCol("timItem", "ItemID")
        lRet = .lMoveRow(lCurrRow, 10)
        lCurrRow = .lGetGrdRowFromTblCol("tciShipMethod", "ShipMethID")
        lRet = .lMoveRow(lCurrRow, 11)
        lCurrRow = .lGetGrdRowFromTblCol("tarCustomer", "CustID")
        lRet = .lMoveRow(lCurrRow, 12)
        lCurrRow = .lGetGrdRowFromTblCol("tarCustomer", "CustName")
        lRet = .lMoveRow(lCurrRow, 13)
        lCurrRow = .lGetGrdRowFromTblCol("tarCustAddr", "CustAddrID")
        lRet = .lMoveRow(lCurrRow, 14)
    End With
        
    If Not moDDData Is Nothing Then
        Set moDDData = Nothing
    End If
    
    
    If Not moSelAssist Is Nothing Then
        Set moSelAssist = Nothing
    End If
    
    ' start set up for transfer order grid
    
    ' instantiate objects
    Set moDDData = New clsDDData
    
    Set moSelAssist = New clsSelGridAssist
    


    'CUSTOMIZE:  Invoke Add method of sRealTableCollection for each table which
    'will provide raw data.  Primary table should appear first.
    
    
    sRealTableCollection.Remove 1
    sRealTableCollection.Add "vimTrnsfrOrder"
    
    
    moDDData.lInitDDData sRealTableCollection, moClass.moAppDB, moClass.moAppDB, msCompanyID

    If Not moDDData.lDDBuildData(App.EXEName) = kSuccess Then
        'Unable to load data from Data Dictionary.
        giSotaMsgBox Me, moSysSession, kmsgCantLoadDDData
        Exit Sub
    End If

    'Add Manual Row Entries for transfer order grid
    With moSelAssist
        Set .Sys_DB = moAppDB
        Set .Selection = moSelect(kTransferOrder)
        '.Add "tsoPickList", "PickListNo", gsBuildString(kPickList, moAppDB, moSysSession)
        .Add "tsoPickList", "PickListNo", gsBuildString(kPickList, moAppDB, moSysSession), , , "SOReprntPick"
        .Add "tsoShipLine", "PickListLineNo", gsBuildString(kPickLine, moAppDB, moSysSession), False
        .Add "timTrnsfrOrderLine", "TrnsfrLineNo", , False
        .Add "timItem", "ItemID"
        
        moSelect(kTransferOrder).SetSelectionRowVisibility "vimTrnsfrOrder", "SchdShipDate", False
        .Add "vimTrnsfrOrder", "SchdShipDate", gsBuildString(kSchedDate, moAppDB, moSysSession), False, gsGetLocalDateMask()
        '.Add "timWarehouse", "WhseID", gsBuildString(kIMShipWhse, moAppDB, moSysSession)
        '.Add "timWarehouse", "WhseID", "Rcvg Whse" ' add to local string
        '.Add "timTrnsfrOrder", "TranNo", gsBuildString(kIMOrderNo, moAppDB, moSysSession)
        '.Add "timTrnsfrOrderLine", "TrnsfrLineNo", gsBuildString(kLineNoDomain, moAppDB, moSysSession)
        '.Add "timItem", "ItemID", , , , , " timItem.ItemType IN (" & _
            klTypeFinishedGood & ", " & klTypeRawMaterial & ", " & klTypeBTOKit & _
", " & klTypeAssembledKit & ")"
        '.Add "tciShipMethod", "ShipMethID", gsBuildString(kSOShipvia, moAppDB, moSysSession)

    End With


    If Not moSelect(kTransferOrder).bInitSelect(sRealTableCollection(1), mskControl(kTransferOrder), navControlTransfer, nbrControl(kTransferOrder), _
curControl(kTransferOrder), grdSelection(kTransferOrder), fraSelect(kTransferOrder), Me, moDDData, _
19 * 120 + 60, 17 * 120, 18 * 120, 18 * 120, False) Then
        'Setup of the Selection Grid Failed.
        giSotaMsgBox Me, moSysSession, kmsgSetupSelectionGridFail

         'Exit this function
        Exit Sub
    End If

    moSelect(kTransferOrder).PopulateSelectionGrid

    'Hide Selection Rows Not Required
    For lIndex = 1 To moSelect(kTransferOrder).lMaxSelGridRows
        bGoodRow = True
        moSelect(kTransferOrder).lGetTblColFromGrdRow lIndex, sGrdTableName, sGrdColumnName, _
sGrdParentTblName, bGrdRestrictToCompany
                
        Select Case sGrdTableName
            Case "vimTrnsfrOrder"
            Select Case sGrdColumnName
                   Case "Status", "TransitWhseKey", "TranDate", "CloseDate"
                        bGoodRow = False
            End Select
        End Select
        
        If Not bGoodRow Then
            moSelect(kTransferOrder).SetSelectionRowVisibility sGrdTableName, sGrdColumnName, False
        End If

'Debug.Print lIndex, sGrdTableName, sGrdColumnName, sGrdParentTblName, bGrdRestrictToCompany
    Next lIndex


    With moSelect(kTransferOrder)
        lCurrRow = .lGetGrdRowFromTblCol("tsoPickList", "PickListNo")
        lRet = .lMoveRow(lCurrRow, 1)
        lCurrRow = .lGetGrdRowFromTblCol("tsoShipLine", "PickListLineNo")
        lRet = .lMoveRow(lCurrRow, 2)
        lCurrRow = .lGetGrdRowFromTblCol("vimTrnsfrOrder", "SchdShipDate")
        lRet = .lMoveRow(lCurrRow, 3)
        lCurrRow = .lGetGrdRowFromTblCol("vimTrnsfrOrder", "ReqDelvDate")
        lRet = .lMoveRow(lCurrRow, 4)
        lCurrRow = .lGetGrdRowFromTblCol("vimTrnsfrOrder", "ShipWhseKey")
        lRet = .lMoveRow(lCurrRow, 5)
        lCurrRow = .lGetGrdRowFromTblCol("vimTrnsfrOrder", "RcvgWhseKey")
        lRet = .lMoveRow(lCurrRow, 6)
        lCurrRow = .lGetGrdRowFromTblCol("vimTrnsfrOrder", "TranNo")
        lRet = .lMoveRow(lCurrRow, 7)
        lCurrRow = .lGetGrdRowFromTblCol("timTrnsfrOrderLine", "TrnsfrLineNo")
        lRet = .lMoveRow(lCurrRow, 8)
        lCurrRow = .lGetGrdRowFromTblCol("timItem", "ItemID")
        lRet = .lMoveRow(lCurrRow, 9)
        lCurrRow = .lGetGrdRowFromTblCol("vimTrnsfrOrder", "ShipMethKey")
        lRet = .lMoveRow(lCurrRow, 10)
    End With



    If Not moDDData Is Nothing Then
        Set moDDData = Nothing
    End If
    
    If Not sRealTableCollection Is Nothing Then
        Set sRealTableCollection = Nothing
    End If
    
    If Not moSelAssist Is Nothing Then
        Set moSelAssist = Nothing
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "SetupSelectGrid", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub BindLineGrid()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moDMGrid = New clsDmGrid
    With moDMGrid
        Set .Session = moSysSession
        Set .Database = moAppDB
        Set .Form = frmConfirmPicks
        Set .Grid = grdLine
        .Table = "#tsoConfPickWrk2"
        .UniqueKey = "ShipLineDistKey"
        .NoAppend = True
        .OrderBy = "PickListNo, PickListLineNo"
        
        .BindColumn "ShipLineDistKey", Nothing, SQL_INTEGER
        .BindColumn "ItemType", kColItemType, SQL_SMALLINT
        
        .BindColumn "ShipLineKey", Nothing, SQL_INTEGER
        .BindColumn "ShipDate", kColShipDate, SQL_DATE
        .BindColumn "ShipDateRaw", kColShipDateRaw, SQL_TIME
                
        .BindColumn "PickStatus", kColPickCheck, SQL_SMALLINT
        .BindColumn "QtyToPick", kColQtyToPick, SQL_DECIMAL
        .BindColumn "QtyPicked", kColQtyPicked, SQL_DECIMAL
        .BindColumn "QtyOrdered", kColQtyOrdered, SQL_DECIMAL
        .BindColumn "InvtTranID", kColInvTranID, SQL_INTEGER
        .BindColumn "InvtTranKey", kColInvTranKey, SQL_INTEGER
        .BindColumn "AllowSubItem", kColAllowSubItem, SQL_SMALLINT
        .BindColumn "TrackMeth", kColTrackMeth, SQL_SMALLINT
        .BindColumn "TrackQtyAtBin", kColTrackQtyAtBin, SQL_SMALLINT
        .BindColumn "PickListNo", kColPickList, SQL_CHAR
        .BindColumn "PickListLineNo", kColPickLineNo, SQL_SMALLINT
        .BindColumn "KitShipLineKey", kColKitLineKey, SQL_INTEGER
        .BindColumn "ShipComplete", kColShipComplete, SQL_SMALLINT
        .BindColumn "ShipCompleteInv", kColShipCompleteInv, SQL_SMALLINT
        .BindColumn "SOLineUOMKey", kColSOLineUOMKey, SQL_INTEGER
        .BindColumn "DistQty", kColSumOfDist, SQL_DECIMAL
        .BindColumn "AllowDecimalQty", kColAllowDecimalQty, SQL_SMALLINT
        .BindColumn "Confirmed", kColConfirmed, SQL_SMALLINT

        .BindColumn "SOKey", Nothing, SQL_INTEGER
        .BindColumn "TranNoRelChngOrd", kColOrder, SQL_CHAR

        .BindColumn "SOLineKey", Nothing, SQL_INTEGER
        .BindColumn "SOLineNo", kColLine, SQL_SMALLINT

        .BindColumn "SOLineDistKey", Nothing, SQL_INTEGER
        .BindColumn "ShipPriority", kColShipPriority, SQL_SMALLINT

        .BindColumn "ItemKey", Nothing, SQL_INTEGER
        .BindColumn "ItemID", kColItem, SQL_CHAR
        .BindColumn "ItemDesc", kColDescription, SQL_CHAR

        .BindColumn "SubItemKey", Nothing, SQL_INTEGER
        .BindColumn "SubItemID", kColSubItem, SQL_CHAR
        .BindColumn "SubItemDesc", kColSubDesc, SQL_CHAR

        .BindColumn "ShipUnitMeasKey", Nothing, SQL_INTEGER
        .BindColumn "UnitMeasID", kColUOM, SQL_CHAR

        .BindColumn "ShipToCustAddrKey", Nothing, SQL_INTEGER
        .BindColumn "CustAddrID", kColShipToAddr, SQL_CHAR

        .BindColumn "WhseKey", Nothing, SQL_INTEGER
        .BindColumn "WhseID", kColShipWarehouse, SQL_CHAR
        
        .BindColumn "RcvgWhseID", kColRcvgWarehouse, SQL_CHAR

        .BindColumn "CustKey", Nothing, SQL_INTEGER
        .BindColumn "CustID", kColCustomer, SQL_CHAR
        .BindColumn "CustName", kColCustName, SQL_CHAR

        .BindColumn "ShipMethKey", Nothing, SQL_INTEGER
        .BindColumn "ShipMethID", kColShipVia, SQL_CHAR
        .BindColumn "TranType", kColTranType, SQL_INTEGER
        .BindColumn "TranTypeText", kColTranTypeText, SQL_CHAR

        'SGS DEJ for IA (START)
        .LinkSource "vluBOLTran_SGS", .Table & ".SOKey=vluBOLTran_SGS.TranKey And " & .Table & ".TranTypeText=vluBOLTran_SGS.TranTypeDesc ", kDmJoin, LeftOuter
        .Link giGetValidInt(kColBOL), "BOLNo"
        'SGS DEJ for IA (STOP)

        .Init
    End With
    
    'Setup the grid navigators.
    gbLookupInit navGridUOM, moClass, moAppDB, "UnitOfMeasure"

    gbLookupInit navGridSubItem, moClass, moAppDB, "ItemSubstitute"

    With lkuAutoDistBin
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB
    End With
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "BindLineGrid", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetupLineGrid()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Set general grid properties
    gGridSetProperties grdLine, kMaxCols, kGridDataSheetNoAppend
    gGridSetColors grdLine
        
    With grdLine
        .OperationMode = SS_OP_MODE_NORMAL
        .AllowMultiBlocks = False
        .SelectBlockOptions = SS_SELBLOCKOPT_ALL
        .RowHeaderDisplay = DispNumbers
        .DisplayRowHeaders = True
        .NoBeep = True
    End With
     
    'set column captions
'    gGridSetHeader grdLine, kColPickCheck, gsBuildString(kSOPick, moAppDB, moSysSession)
'    gGridSetHeader grdLine, kColSalesOrder, gsGridReadCell(grdSelection, moSelect(kSalesOrder).lGetGrdRowFromTblCol("tsoSalesOrder", "TranNoRelChngOrd"), 1)
'    gGridSetHeader grdLine, kColSOLine, gsBuildString(kSOsoLilne, moAppDB, moSysSession)
'    gGridSetHeader grdLine, kColPickList, gsGridReadCell(grdSelection, moSelect(kSalesOrder).lGetGrdRowFromTblCol("tsoPickList", "PickListNo"), 1)
'    gGridSetHeader grdLine, kcolPickLineNo, gsBuildString(kSOsoLilne, moAppDB, moSysSession)
'    gGridSetHeader grdLine, kColItem, gsGridReadCell(grdSelection, moSelect(kSalesOrder).lGetGrdRowFromTblCol("timItem", "ItemID"), 1)
'    gGridSetHeader grdLine, kColQtyToPick, gsBuildString(kSOQtyToPick, moAppDB, moSysSession)
'    gGridSetHeader grdLine, kColQtyPicked, gsBuildString(kSOQtyPicked, moAppDB, moSysSession)
'    gGridSetHeader grdLine, kColUOM, gsBuildString(kSOUOM, moAppDB, moSysSession)
'    gGridSetHeader grdLine, kColShipToAddr, gsGridReadCell(grdSelection, moSelect(kSalesOrder).lGetGrdRowFromTblCol("tarCustAddr", "CustAddrID"), 1)
'    gGridSetHeader grdLine, kColShipVia, gsGridReadCell(grdSelection, moSelect(kSalesOrder).lGetGrdRowFromTblCol("tciShipMethod", "ShipMethID"), 1)
'    gGridSetHeader grdLine, kColShipDate, gsGridReadCell(grdSelection, moSelect(kSalesOrder).lGetGrdRowFromTblCol("tsoSOLineDist", "ShipDate"), 1)
'    gGridSetHeader grdLine, kColShipPriority, gsGridReadCell(grdSelection, moSelect(kSalesOrder).lGetGrdRowFromTblCol("tsoSOLineDist", "ShipPriority"), 1)
'    gGridSetHeader grdLine, kColWarehouse, gsGridReadCell(grdSelection, moSelect(kSalesOrder).lGetGrdRowFromTblCol("timWarehouse", "WhseID"), 1)
'    gGridSetHeader grdLine, kColCustomer, gsGridReadCell(grdSelection, moSelect(kSalesOrder).lGetGrdRowFromTblCol("tarCustomer", "CustID"), 1)
'    gGridSetHeader grdLine, kColCustName, gsGridReadCell(grdSelection, moSelect(kSalesOrder).lGetGrdRowFromTblCol("tarCustomer", "CustName"), 1)
'    gGridSetHeader grdLine, kColDescription, gsBuildString(kItemDescriptionTbl, moAppDB, moSysSession)
'    gGridSetHeader grdLine, kColSubItem, gsBuildString(kSOSRegSubItem, moAppDB, moSysSession)
'    gGridSetHeader grdLine, kColSubDesc, gsBuildString(kSOSubItemDesc, moAppDB, moSysSession)
'    gGridSetHeader grdLine, kColInvTranKey, "-"
'    gGridSetHeader grdLine, kColAllowSubItem, "-"
'    gGridSetHeader grdLine, kColTrackMeth, "-"
'    gGridSetHeader grdLine, kColTrackQtyAtBin, "-"
'    gGridSetHeader grdLine, kColInvTranID, "-"
'    gGridSetHeader grdLine, kColKitLineKey, "-"
'    gGridSetHeader grdLine, kColShipComplete, "-"
'    gGridSetHeader grdLine, kColShipDateRaw, "-"
'    gGridSetHeader grdLine, kColShipCompleteInv, "-"
'    gGridSetHeader grdLine, kColSOLineUOMKey, "-"
'    gGridSetHeader grdLine, kColSumOfDist, "-"
'    gGridSetHeader grdLine, kColAllowDecimalQty, "-"
'    gGridSetHeader grdLine, kColConfirmed, "-"

    '***************************************************************************************
    'SGS DEJ IA (START)
    '***************************************************************************************
    gGridSetHeader grdLine, kColBOL, "BOL"
    gGridSetColumnWidth grdLine, kColBOL, 8
    gGridSetColumnType grdLine, kColBOL, SS_CELL_TYPE_EDIT
    gGridLockColumn grdLine, kColBOL
    '***************************************************************************************
    'SGS DEJ IA (STOP)
    '***************************************************************************************
    
    gGridSetHeader grdLine, kColPickCheck, gsBuildString(kSOPick, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColOrder, gsBuildString(kIMOrderNo, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColTranTypeText, gsBuildString(kTypeCol, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColLine, gsBuildString(KSOLineDetl, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColPickList, gsBuildString(kPickList, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColPickLineNo, gsBuildString(kPickLine, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColItem, gsBuildString(kIMItem, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColQtyToPick, gsBuildString(kSOQtyToPick, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColQtyPicked, gsBuildString(kSOQtyPicked, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColUOM, gsBuildString(kSOUOM, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColShipToAddr, gsBuildString(kSOShipToAddress, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColShipVia, gsBuildString(kShipMethodTbl, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColShipDate, gsBuildString(kSOShipDate, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColShipPriority, gsBuildString(kSOShipPriority, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColShipWarehouse, gsBuildString(kShipWhse, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColRcvgWarehouse, gsBuildString(kShortRcvgWhse, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColCustomer, gsBuildString(kSOCustomer, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColCustName, gsBuildString(kSOCustomerName, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColDescription, gsBuildString(kItemDescriptionTbl, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColSubItem, gsBuildString(kSOSRegSubItem, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColSubDesc, gsBuildString(kSOSubItemDesc, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColAutoDistBinID, "BinID"

    gGridSetHeader grdLine, kColInvTranKey, "-"
    gGridSetHeader grdLine, kColAllowSubItem, "-"
    gGridSetHeader grdLine, kColTrackMeth, "-"
    gGridSetHeader grdLine, kColTrackQtyAtBin, "-"
    gGridSetHeader grdLine, kColInvTranID, "-"
    gGridSetHeader grdLine, kColKitLineKey, "-"
    gGridSetHeader grdLine, kColShipComplete, "-"
    gGridSetHeader grdLine, kColShipDateRaw, "-"
    gGridSetHeader grdLine, kColShipCompleteInv, "-"
    gGridSetHeader grdLine, kColSOLineUOMKey, "-"
    gGridSetHeader grdLine, kColSumOfDist, "-"
    gGridSetHeader grdLine, kColAllowDecimalQty, "-"
    gGridSetHeader grdLine, kColConfirmed, "-"
    gGridSetHeader grdLine, kColTranType, "-"

    
  'Set column widths
    gGridSetColumnWidth grdLine, 0, 4
    gGridSetColumnWidth grdLine, kColPickCheck, 4
    gGridSetColumnWidth grdLine, kColOrder, 11
    gGridSetColumnWidth grdLine, kColTranTypeText, 6
    gGridSetColumnWidth grdLine, kColLine, 4
    gGridSetColumnWidth grdLine, kColPickList, 9
    gGridSetColumnWidth grdLine, kColPickLineNo, 7
    gGridSetColumnWidth grdLine, kColItem, 17
    gGridSetColumnWidth grdLine, kColQtyToPick, 10
    gGridSetColumnWidth grdLine, kColQtyPicked, 10
    gGridSetColumnWidth grdLine, kColUOM, 8
    gGridSetColumnWidth grdLine, kColShipToAddr, 15
    gGridSetColumnWidth grdLine, kColShipVia, 15
    gGridSetColumnWidth grdLine, kColShipDate, 10
    gGridSetColumnWidth grdLine, kColShipPriority, 8
    gGridSetColumnWidth grdLine, kColShipWarehouse, 16
    gGridSetColumnWidth grdLine, kColRcvgWarehouse, 16
    gGridSetColumnWidth grdLine, kColCustomer, 12
    gGridSetColumnWidth grdLine, kColCustName, 40
    gGridSetColumnWidth grdLine, kColDescription, 20
    gGridSetColumnWidth grdLine, kColSubItem, 20
    gGridSetColumnWidth grdLine, kColSubDesc, 20
    gGridSetColumnWidth grdLine, kColInvTranKey, 10
    gGridSetColumnWidth grdLine, kColAllowSubItem, 10
    gGridSetColumnWidth grdLine, kColTrackMeth, 10
    gGridSetColumnWidth grdLine, kColTrackQtyAtBin, 10
    gGridSetColumnWidth grdLine, kColInvTranID, 10
    gGridSetColumnWidth grdLine, kColKitLineKey, 10
    gGridSetColumnWidth grdLine, kColShipComplete, 10
    gGridSetColumnWidth grdLine, kColShipDateRaw, 10
    gGridSetColumnWidth grdLine, kColShipCompleteInv, 10
    gGridSetColumnWidth grdLine, kColSOLineUOMKey, 10
    gGridSetColumnWidth grdLine, kColSumOfDist, 10
    gGridSetColumnWidth grdLine, kColAllowDecimalQty, 10
    gGridSetColumnWidth grdLine, kColConfirmed, 10
    gGridSetColumnWidth grdLine, kColTranType, 10
    gGridSetColumnWidth grdLine, kColAutoDistBinID, 12

  'Set column types
    gGridSetColumnType grdLine, kColPickCheck, SS_CELL_TYPE_CHECKBOX
    gGridSetColumnType grdLine, kColOrder, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColLine, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColPickList, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColPickLineNo, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColItem, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColQtyToPick, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColQtyPicked, SS_CELL_TYPE_FLOAT
    gGridSetColumnType grdLine, kColUOM, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColShipToAddr, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColShipVia, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColShipDate, SS_CELL_TYPE_DATE
    gGridSetColumnType grdLine, kColShipPriority, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdLine, kColShipWarehouse, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColRcvgWarehouse, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColCustomer, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColCustName, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColDescription, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColSubItem, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColSubDesc, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColInvTranKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColAllowSubItem, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColTrackMeth, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColTrackQtyAtBin, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColInvTranID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColKitLineKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColShipComplete, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColShipDateRaw, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColShipCompleteInv, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColSOLineUOMKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColSumOfDist, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColAllowDecimalQty, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColConfirmed, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColTranType, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColTranTypeText, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColAutoDistBinID, SS_CELL_TYPE_EDIT
    
    'lock protected columns
    gGridLockColumn grdLine, kColPickCheck
    gGridLockColumn grdLine, kColOrder
    gGridLockColumn grdLine, kColLine
    gGridLockColumn grdLine, kColPickList
    gGridLockColumn grdLine, kColPickLineNo
    gGridLockColumn grdLine, kColItem
    gGridLockColumn grdLine, kColQtyToPick
    gGridLockColumn grdLine, kColShipToAddr
    gGridLockColumn grdLine, kColShipVia
    gGridLockColumn grdLine, kColShipDate
    gGridLockColumn grdLine, kColShipPriority
    gGridLockColumn grdLine, kColShipWarehouse
    gGridLockColumn grdLine, kColRcvgWarehouse
    gGridLockColumn grdLine, kColCustomer
    gGridLockColumn grdLine, kColCustName
    gGridLockColumn grdLine, kColDescription
    gGridLockColumn grdLine, kColShipVia
    gGridLockColumn grdLine, kColSubDesc
    gGridLockColumn grdLine, kColTranType
    gGridLockColumn grdLine, kColTranTypeText
    gGridLockColumn grdLine, kColAutoDistBinID
   
    'freeze columns
    gGridFreezeCols grdLine, 6
    
    'setup numeric formatting
    SetGridNumericAttr grdLine, kColQtyToPick, miNbrDecPlaces, kMaxQtyLen
    SetGridNumericAttr grdLine, kColQtyPicked, miNbrDecPlaces, kMaxQtyLen
    
    'hide columns
    gGridHideColumn grdLine, kColInvTranKey
    gGridHideColumn grdLine, kColAllowSubItem
    gGridHideColumn grdLine, kColTrackMeth
    gGridHideColumn grdLine, kColTrackQtyAtBin
    gGridHideColumn grdLine, kColInvTranID
    gGridHideColumn grdLine, kColKitLineKey
    gGridHideColumn grdLine, kColShipComplete
    gGridHideColumn grdLine, kColShipDateRaw
    gGridHideColumn grdLine, kColShipCompleteInv
    gGridHideColumn grdLine, kColSOLineUOMKey
    gGridHideColumn grdLine, kColSumOfDist
    gGridHideColumn grdLine, kColAllowDecimalQty
    gGridHideColumn grdLine, kColConfirmed
    gGridHideColumn grdLine, kColTranType
    gGridHideColumn grdLine, kColAutoDist
    gGridHideColumn grdLine, kColAutoDistBinKey
    gGridHideColumn grdLine, kColAutoDistBinQtyAvail
    gGridHideColumn grdLine, kColAutoDistBinUOMKey
    gGridHideColumn grdLine, kColAutoDistLotNo
    gGridHideColumn grdLine, kColAutoDistLotExpDate
    gGridHideColumn grdLine, kColAutoDistInvtLotKey
    gGridHideColumn grdLine, kColAutoDistMultipleBin
    gGridHideColumn grdLine, kColItemType
    gGridHideColumn grdLine, kColQtyOrdered
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "SetupLineGrid", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub SetGridNumericAttr(grd As Control, lCol As Long, iDecPlaces As Integer, sMaxFloat As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Set the column type based on the currency attributes passed in
    gGridSetColumnType grd, lCol, SS_CELL_TYPE_FLOAT
    With grd
        .Col = lCol
        .Col2 = lCol
        .Row = -1
        .Row2 = -1
        .BlockMode = True
        .TypeFloatMax = sMaxFloat
        .TypeFloatDecimalPlaces = iDecPlaces
        .TypeFloatSeparator = True
        .BlockMode = False
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "SetGridNumericAttr", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub BindGM()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moGM = New clsGridMgr
    With moGM
        Set .Grid = grdLine
        Set .DM = moDMGrid
        Set .Form = frmConfirmPicks
        .GridType = kGridDataSheetNoAppend
        'Cannot have grid sortable when it is grid type DataSheetNoAppend - If the grid is type DataSheet the data manager
        'will sort the grid.  If the grid is type DataSheetNoAppend the grid manager does the sort which gets dm out of sync.
        .GridSortEnabled = False
        Set moGridNavSubItem = .BindColumn(kColSubItem, navGridSubItem)
        Set moGridNavSubItem.ReturnControl = txtNavSubItem
        Set moGridNavUOM = .BindColumn(kColUOM, navGridUOM)
        Set moGridNavUOM.ReturnControl = txtNavUOM
        .Init
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "BindGM", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moContextMenu = New clsContextMenu
    With moContextMenu
        Set .Form = frmConfirmPicks
        .BindGrid moGM, grdLine.hwnd
        .Bind "*APPEND", grdLine.hwnd, kEntTypeSOPicking
        .Bind "*SELECTION", grdSelection(kSalesOrder).hwnd
        .Bind "*SELECTION", grdSelection(kTransferOrder).hwnd
        .Init
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "BindContextMenu", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function ETWhereClause(ByVal ctl As Object, _
                              ByVal taskID As Long, _
                              ByVal EntityType As Integer, _
                              ByVal naturalKeyCols As String, _
                              ByVal TaskGroup As EntityTask_Groups) As String
'+++ VB/Rig Skip +++
'*******************************************************************************
'   Description: OPTIONAL
'                Entity Task Where Clause. This routine allows the application
'                to specify the where clause to be used against the
'                Host Data View for the specified entity.
'
'                If a where clause is not specified, the where clause will be
'                built using the Natural Key Columns specified for the entity.
'
'                Surrogate Keys as natural keys and other criteria make it
'                impossible without supporting metadata to get these values from
'                the application.
'
'   Parameters:
'                ctl <in> - Control that selected the task
'                taskID <in> - Task ID for the Entity Task
'                entityType <in> - Entity Type bound to the control that selected the task
'                naturalKeyCols <in> - Key columns used to specify where clause (FYI)
'                TaskGroup <in> - Task Group 100-500 (FYI)
'
'   Returns:
'                A specified where clause WITHOUT the 'WHERE' verb. (i.e. "1 = 2")
'*******************************************************************************
On Error Resume Next

    Dim lActiveRow As Long
    
    Select Case True
        Case ctl Is grdLine
            lActiveRow = glGetValidLong(grdLine.ActiveRow)
            ETWhereClause = "ShipLineKey = " & CStr(glGetValidLong(moDMGrid.GetColumnValue(lActiveRow, "ShipLineKey")))
        
        Case Else
            ETWhereClause = ""
    End Select

    Err.Clear

End Function

Private Sub BindLineGridInvt()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moDMGridInvt = New clsDmGrid
    With moDMGridInvt
        Set .Session = moSysSession
        Set .Database = moAppDB
        Set .Form = frmConfirmPicks
        Set .Grid = grdLineInvt
        .Table = "#tsoConfPickWrk3"
        .UniqueKey = "ShipLineDistKey"
        .NoAppend = True
        
        .BindColumn "ShipLineDistKey", kColInvtShipLineDist, SQL_INTEGER
        .BindColumn "InvtTranID", kColInvtInvtTranID, SQL_INTEGER
        .BindColumn "InvtTranKey", kColInvtInvtTranKey, SQL_INTEGER
        .BindColumn "ItemKey", Nothing, SQL_INTEGER
       
        .Init
    End With
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "BindLineGridInvt", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetupLineGridInvt()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Set general grid properties
    gGridSetProperties grdLineInvt, kColInvtCols, kGridDataSheetNoAppend
    gGridSetColors grdLineInvt
        
    With grdLineInvt
        .OperationMode = SS_OP_MODE_NORMAL
        .AllowMultiBlocks = False
        .SelectBlockOptions = SS_SELBLOCKOPT_ALL
        .RowHeaderDisplay = DispNumbers
        .DisplayRowHeaders = True
    End With
     
  'Set column types
    gGridSetColumnType grdLineInvt, kColInvtShipLineDist, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineInvt, kColInvtInvtTranID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLineInvt, kColInvtInvtTranKey, SS_CELL_TYPE_EDIT
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "SetupLineGridInvt", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function CMAppendContextMenu(ctl As Control, hmenu As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************************
'      Desc: Called to append menu items to right mouse popup menu.
'            Called because form_load did a .Bind *APPEND.
'     Parms: Ctl:   The control that received the right mouse button down message.
'            hmenu: The handle to the popup menu
'   Returns: True if menu item added; False if menu item not added.
'************************************************************************************
    CMAppendContextMenu = False

    Dim sDeleteLine As String
    
    If ctl Is grdLine Then
        If grdLine.MaxRows > 0 Then
            sDeleteLine = gsBuildString(kDeleteLine, moClass.moAppDB, moClass.moSysSession)
            AppendMenu hmenu, MF_ENABLED, 5003, sDeleteLine
            CMAppendContextMenu = True
            Exit Function
        End If
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "CMAppendContextMenu", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Function CMMenuSelected(ctl As Control, lTaskID As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************************
'      Desc: Called when a popup context menu item is selected.
'            Called because menu item was added by CMAppendContextMenu event.
'     Parms: Ctl:   The control that received the right mouse button down message.
'            lTaskID: The Task ID of the selected menu item.
'   Returns: True if successful; False if unsuccessful.
'************************************************************************************
Dim lKitShipLineKey As Long
Dim iRet As Integer

    CMMenuSelected = False

    Dim lActiveRow As Long
    Dim iIndex As Integer
    
        
    If (ctl Is grdLine And lTaskID = 5003) Then
        lActiveRow = grdLine.ActiveRow
        If lActiveRow <= 0 Then
            Exit Function
        Else
            lKitShipLineKey = glGetValidLong(gsGridReadCell(grdLine, lActiveRow, kColKitLineKey))
            If (lKitShipLineKey > 0) Then
                'Ask user if they wish to delete all remaining components of the kit from the grid
                iRet = giSotaMsgBox(Nothing, moSysSession, kSODeleteAllComponents)
                
                If (iRet <> kretYes) Then
                    Exit Function
                End If
            End If
            If glGetValidLong(gsGridReadCell(grdLine, lActiveRow, kColInvTranID)) > 0 Then 'Only back out new distributions, not those that have been permanently saved
                'Cancel distributions
                ClearItemDist (lActiveRow)
            End If
            
            moGM.GridDelete
            moDMGrid.Save
            
            'User has already acknowledged that they want to delete all components
            If (lKitShipLineKey > 0) Then
                DeleteAllKitComponents lKitShipLineKey
                moDMGrid.Refresh
            End If
            
            SetFormState
        End If
    End If
        
    CMMenuSelected = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "CMMenuSelected", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub DeleteAllKitComponents(KitShipLineKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL                As String
Dim lRow                As Long
Dim lRows               As Long
Dim lKitShipLineKey     As Long

    'Delete any temporary distributions
    lRows = glGridGetDataRowCnt(grdLine)
    For lRow = 1 To lRows
        lKitShipLineKey = glGetValidLong(gsGridReadCell(grdLine, lRow, kColKitLineKey))
        If lKitShipLineKey = KitShipLineKey Then
            If glGetValidLong(gsGridReadCell(grdLine, lRow, kColInvTranID)) > 0 Then
                ClearItemDist lRow
            End If
        End If
    Next lRow

    sSQL = "DELETE FROM #tsoConfPickWrk2 WHERE KitShipLineKey = " & KitShipLineKey
    moAppDB.ExecuteSQL sSQL
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "DeleteAllKitComponents", VBRIG_IS_FORM       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    '-- Unload all forms loaded from this main form
    gUnloadChildForms Me
         
    '-- Remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    If Not moItemDist Is Nothing Then
        moItemDist.Terminate
        Set moItemDist = Nothing
    End If
    
    If Not moGM Is Nothing Then
        moGM.UnloadSelf
        Set moGM = Nothing
    End If
    
    If Not moGM Is Nothing Then
        moGM.UnloadSelf
        Set moGM = Nothing
    End If
    
    If Not moHook Is Nothing Then
        moHook.EndHook
        Set moHook = Nothing
    End If
        
    If Not moContextMenu Is Nothing Then
        Set moContextMenu = Nothing
    End If
    
    If Not moGM Is Nothing Then
        moGM.UnloadSelf
        Set moGM = Nothing
    End If
    
    If Not moDMGrid Is Nothing Then
        moDMGrid.UnloadSelf
        Set moDMGrid = Nothing
    End If

    If Not moDMGridInvt Is Nothing Then
        moDMGridInvt.UnloadSelf
        Set moDMGridInvt = Nothing
    End If
    
    If Not moSelect(0) Is Nothing Then
        Set moSelect(0) = Nothing
    End If
    
    If Not moSelect(1) Is Nothing Then
        Set moSelect(1) = Nothing
    End If
    
    If Not moSettings Is Nothing Then
        Set moSettings = Nothing
    End If
    
    If Not moModuleOptions Is Nothing Then
        Set moModuleOptions = Nothing
    End If

    If Not moAppDB Is Nothing Then
        Set moAppDB = Nothing
    End If
    
    If Not moAppDB Is Nothing Then
        Set moAppDB = Nothing
    End If
    
    If Not moSysSession Is Nothing Then
        Set moSysSession = Nothing
    End If
    
    If Not moMultSelGridMgr Is Nothing Then
        Set moMultSelGridMgr = Nothing
    End If
    
    
    If Not moOptions Is Nothing Then
        Set moOptions = Nothing
    End If
    
    
           
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "PerformCleanShutDown", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetupBars()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'****************************************************************
' Description:
'    This routine will setup the toolbar with buttons appropriate
'    for the current project.
'****************************************************************
    '-- Setup the status bar
    With sbrMain
        Set .Framework = moClass.moFramework
        .BrowseVisible = False
        '.StatusVisible = False
    End With
    
    '-- Setup the Toolbar
    With tbrMain
        .Style = sotaTB_DATASHEET
        .RemoveButton kTbPrint
        .AddButton kTbFinish, .GetIndex(kTbHelp)
        .AddButton kTbCancel, .GetIndex(kTbHelp)
        .AddSeparator .GetIndex(kTbHelp)
        .AddButton kTbSave, .GetIndex(kTbHelp)
        .AddButton kTbDelete, .GetIndex(kTbHelp)
        .AddSeparator .GetIndex(kTbHelp)
        .LocaleID = mlLanguage
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "SetupBars", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iConfirmUnload As Integer
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If

   
    '-- Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False
      
        '-- Check all other forms that may have been loaded from this main form.
        '-- If there are any Visible forms, then this means the form is Active.
        '-- Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then GoTo CancelShutDown
    
        Select Case UnloadMode
            Case vbFormCode
                'Do Nothing
            
            Case Else
                '-- Most likely the user has requested to shut down the form.
                '-- If the context is normal or Drill-Around, have the object unload itself.
                 Select Case mlRunMode
                    Case kContextAOF
                        Me.Hide
                        GoTo CancelShutDown
                    Case Else
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                End Select
        End Select
    
    '-- If execution gets to this point, the form and class object of the form
    '-- will be shut down. Perform all operations necessary for a clean shutdown.
    PerformCleanShutDown
        
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
        Case Else
            'Do Nothing
    End Select

    Exit Sub
    
CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
    Exit Sub

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "Form_QueryUnload", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    '-- Resize the grid and associated controls
    If (Me.WindowState = vbNormal Or Me.WindowState = vbMaximized) Then
        '-- Move the controls down
        gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, fraLine, grdLine
        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, fraSelect, grdSelection, fraLine, grdLine
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "Form_Resize", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo ExpectedErrorRoutine
    'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Set the Class object to Nothing
    Set moClass = Nothing
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}

ExpectedErrorRoutine:
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "Form_Unload", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub TogglePickCheck(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    If Col <> kColPickCheck Then Exit Sub
    If Row < 1 Then Exit Sub
    If bAutoPickUsed Then   'on lost focus of the QtyPicked field the Picked Flag is automatically selected.  If the lost focus was due
                            'to this scenario, do not perform the rest of this proc which would reverse the AutoPick
        bAutoPickUsed = False
        Exit Sub
    End If
        
    With grdLine
        .Col = Col
        .Row = Row
        .Value = 1 - .Value
    End With
    '(VB-03/01)Scopus#17584-set grid property here than in SetPickDefaults
    If grdLine.Value = 1 Then
        'Put focus in the Qty Picked cell
        With grdLine
            .Row = Row
            .Row2 = Row
            .Col = kColQtyPicked
            .Col2 = kColQtyPicked
            .Action = SS_ACTION_SELECT_BLOCK
        End With
    End If
    
    SetPickDefaults Row
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "TogglePickCheck", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetPickDefaults(ByVal Row As Long, Optional bSaveGrid As Boolean = True, _
    Optional bSkipEnableDist As Boolean = False)
'****
' Arguments:
'   bSaveGrid : Default True
'           - True  - recommended on single Row changes and SetPickDefaults will execute moGrid.Save
'           - False - recomended when updating multiple rows, then callee does a single moGrid.Save
'
'
'****
Dim sWhere As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim dQtyPicked  As Double
Dim lItemKey    As Long
Dim lSaveUOMKey As Long
Dim lNewUOMKey  As Long
Dim bResult     As Boolean
Dim sNewUOMID   As String
Dim sSQL        As String


    With grdLine
        .Row = Row
        .Col = kColPickCheck
    End With
    If grdLine.Value = 1 Then
        '(VB-03/01)Scopus#17584-Removed setting the grid property from here and moved it to where it was called
        'in order for the PickAll to load the entire grid at once instead of one row at a time.
        'Put focus in the Qty Picked cell
        ' With grdLine
        '     .Row = Row
        '     .Row2 = Row
        '     .Col = kColQtyPicked
        '     .Col2 = kColQtyPicked
        '     .Action = SS_ACTION_SELECT_BLOCK
        ' End With
        dQtyPicked = gdGetValidDbl(gsGridReadCell(grdLine, Row, kColQtyPicked))
        If dQtyPicked = 0 Then
            gGridUpdateCell grdLine, Row, kColQtyPicked, gdGetValidDbl(gsGridReadCell(grdLine, Row, kColQtyToPick))
        End If
    Else
    
        '3/3/05: per spec, when unchecking Pick status, do not reset or clear.  User must zero out QtyPicked manually
        
'        'reset distribution backout/setvalue
'        ClearItemDist (Row)
'        'Reset to initial state
'        gGridUpdateCell grdLine, Row, kColQtyPicked, "0"
'        lItemKey = glGetValidLong(moDMGrid.GetColumnValue(Row, "ItemKey"))
'        lSaveUOMKey = glGetValidLong(moDMGrid.GetColumnValue(Row, "ShipUnitMeasKey"))
'        lNewUOMKey = glGetValidLong(moDMGrid.GetColumnValue(Row, "SOLineUOMKey"))
'        moDMGrid.SetColumnValue Row, "ShipUnitMeasKey", lNewUOMKey
'        sSQL = "UnitMeasKey = " & CStr(lNewUOMKey) & " AND CompanyID = " & gsQuoted(msCompanyID)
'        sNewUOMID = gsGetValidStr(moAppDB.Lookup("UnitMeasID", "tciUnitMeasure", sSQL))
'        bResult = bConvertQtyForNewUOM(Row, lItemKey, lSaveUOMKey, lNewUOMKey)
'        moDMGrid.SetColumnValue Row, "UnitMeasID", sNewUOMID
'        'reset substitute item
'        moDMGrid.SetColumnValue Row, "SubItemKey", "NULL"
'        moDMGrid.SetColumnValue Row, "SubItemID", Null
'        moDMGrid.SetColumnValue Row, "SubItemDesc", Null
'        gGridUpdateCellText grdLine, Row, kColSubItem, ""
'        gGridUpdateCellText grdLine, Row, kColSubDesc, ""
'        'refresh grid
'        'moDMGrid.DoLinks Row
    End If
    moDMGrid.SetRowDirty Row
    If bSaveGrid Then
        moDMGrid.Save
    End If
            
    SetRowValues Row, bSkipEnableDist

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "SetPickDefaults", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CreateDistForm(ByVal lRow As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

Dim lWhseKey    As Long

    'This sub was required since the Distribution form requires a warehousekey, we don't have warehouse key until an item is chosen
    If moItemDist Is Nothing Then
        '-- Setup Item Distribution object
        Set moItemDist = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                            "imzde001.clsimzde001", 117833828, kAOFRunFlags, kContextAOF)
  
        lWhseKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "WhseKey"))
        
        If Not moItemDist Is Nothing Then
            If Not moItemDist.InitDistribution(moAppDB, moClass.moAppDB, lWhseKey) Then
                giSotaMsgBox Me, moSysSession, kmsgErrInitItemDist
                Exit Sub
            End If
        End If
    End If
    
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "CreateDistForm", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bLoadLotSerBinForm(Row As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

Dim lItemKey    As Long
Dim lSubItemKey As Long
Dim lWhseKey    As Long
Dim lUOMKey     As Long
Dim lSOUOMKey   As Long
Dim dQty        As Double
Dim lInvTranID  As Long
Dim lInvTranKey As Long
Dim dReturnQty  As Double
Dim dSumOfDist  As Double
Dim lShipLineDistKey As Long
Dim lShipLineKey As Long
Dim lTranType As Long
        
    bLoadLotSerBinForm = False
    
    
    
    CreateDistForm Row
    '-- Load the Item Distributions form
    If Not moItemDist Is Nothing Then
        
        'use substitute item if entered
        lItemKey = glGetValidLong(moDMGrid.GetColumnValue(Row, "SubItemKey"))
        If lItemKey = 0 Then
            lSubItemKey = 0
            lItemKey = glGetValidLong(moDMGrid.GetColumnValue(Row, "ItemKey"))
        End If
        lUOMKey = glGetValidLong(moDMGrid.GetColumnValue(Row, "ShipUnitMeasKey"))
        lSOUOMKey = glGetValidLong(moDMGrid.GetColumnValue(Row, "SOLineUOMKey"))
        dQty = gdGetValidDbl(gsGridReadCell(grdLine, Row, kColQtyPicked))
        dSumOfDist = gdGetValidDbl(gsGridReadCell(grdLine, Row, kColSumOfDist))
        
        lShipLineKey = glGetValidLong(moDMGrid.GetColumnValue(Row, "ShipLineKey"))
        'lInvTranID = glGetValidLong(gsGridReadCell(grdLine, Row, kColInvTranID))
        'Using the the ShipLineKey as the InvtTranID
        lInvTranID = lShipLineKey
        lInvTranKey = glGetValidLong(gsGridReadCell(grdLine, Row, kColInvTranKey))
        lTranType = glGetValidLong(gsGridReadCell(grdLine, Row, kColTranType))
        
        If dQty <= 0 Then Exit Function
           
        moItemDist.WarehouseKey = glGetValidLong(moDMGrid.GetColumnValue(Row, "WhseKey"))
        frmConfirmPicks.Enabled = False
        
        bAutoDistFromBinLku Row

        If (lUOMKey = lSOUOMKey) _
            And (dQty >= dSumOfDist) _
            And (lInvTranID = 0) _
            And (lInvTranKey = 0) _
            And (lSubItemKey = 0) Then
            lShipLineDistKey = glGetValidLong(moDMGrid.GetColumnValue(Row, "ShipLineDistKey"))
            lInvTranID = moItemDist.EditDistribution(lTranType, lItemKey, lUOMKey, dQty, dReturnQty, _
                                                     lInvTranID, lInvTranKey, , , , False, , lShipLineDistKey)
        Else
            lInvTranID = moItemDist.EditDistribution(lTranType, lItemKey, lUOMKey, dQty, dReturnQty, _
                                                     lInvTranID, lInvTranKey)
        End If
        frmConfirmPicks.Enabled = True

        With moItemDist
             If .oerror.Number <> 0 Then
                 giSotaMsgBox Me, moSysSession, .oerror.messageconstant
                 .oerror.Clear
                 grdLine_Click kColQtyPicked, Row
                 grdLine.SetFocus
                 Exit Function
             End If
        End With
        
        'If the quantity is zero and TranId or TranKey is present, delete distribution.  Do not save distributions
        'in SO if Qty = 0 since the presence of a distribution key indicates that the line can be shipped and invoiced.
        If dReturnQty = 0 Then
            ClearItemDist Row
        End If
        
        'Set the value of the auto-dist bin lookup based on what is saved during EditDistribution.
        GetAutoDistSavedInfo Row
        
        If moItemDist.DistributionChanged(lInvTranID, lInvTranKey) = False Then
            Exit Function
        End If
        
        If dReturnQty = 0 Or (lInvTranID = 0 And lInvTranKey = 0) Then 'Dist form was cancelled by user
            bResult = bCancelInvtTranID(lInvTranID)
            MsgBox "Distribution was not completed successfully."
            gGridUpdateCell grdLine, Row, kColInvTranID, CStr(lInvTranID)
            grdLine_Click kColQtyPicked, Row
            grdLine.SetFocus
            Exit Function
        End If
        
        If gdGetValidDbl(dReturnQty) <> dQty Then 'Dist qty not equal to qty to ship
            'Defect #13994: Do not clear invalid distribution
            'bResult = bCancelInvtTranID(lInvTranID)
            MsgBox "Distribution quantity does not equal quantity picked.", vbOKOnly
            grdLine_Click kColQtyPicked, Row
            grdLine.SetFocus
            'Defect #13994: Save lInvTranID to retrieve distribution from IM
            '               Save dReturnQty to do validation at Save time
            gGridUpdateCell grdLine, Row, kColSumOfDist, CStr(dReturnQty)
            gGridUpdateCell grdLine, Row, kColInvTranID, CStr(lInvTranID)
        
            'Set the value of the auto-dist bin lookup based on what is saved during EditDistribution.
            GetAutoDistSavedInfo Row
            
            moDMGrid.SetRowDirty Row
            moDMGrid.Save
            Exit Function
        End If
        'Defect #13994: Save lInvTranID to retrieve distribution from IM
        '               Save dReturnQty to do validation at Save time
        gGridUpdateCell grdLine, Row, kColSumOfDist, CStr(dReturnQty)
        gGridUpdateCell grdLine, Row, kColInvTranID, CStr(lInvTranID)
        
        'Set the value of the auto-dist bin lookup based on what is saved during EditDistribution.
        GetAutoDistSavedInfo Row
        
        ' testing ***********
        gGridUpdateCell grdLine, Row, kColInvTranKey, CStr(lInvTranKey)
        moDMGrid.SetRowDirty Row
        moDMGrid.Save
    End If
    
    bLoadLotSerBinForm = True
    Exit Function

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "bLoadLotSerBinForm", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub ClearItemDist(Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

Dim lInvtTranID     As Long
Dim lInvtTranKey    As Long
Dim lShipLineKey    As Long
Dim lItemKey        As Long
Dim lOrigItemKey    As Long
Dim lShipLineDistKey As Long
Dim bIsTransaction  As Boolean

    lInvtTranID = glGetValidLong(gsGridReadCell(grdLine, Row, kColInvTranID))
    lInvtTranKey = glGetValidLong(gsGridReadCell(grdLine, Row, kColInvTranKey))
    lItemKey = glGetValidLong(moDMGrid.GetColumnValue(Row, "SubItemKey"))
    lShipLineDistKey = glGetValidLong(moDMGrid.GetColumnValue(Row, "ShipLineDistKey"))
    If lItemKey = 0 Then
        lOrigItemKey = 0
        lItemKey = glGetValidLong(moDMGrid.GetColumnValue(Row, "ItemKey"))
    Else
        lOrigItemKey = glGetValidLong(moDMGrid.GetColumnValue(Row, "ItemKey"))
    End If
    
    CreateDistForm Row
    '-- Clear out Item Distributions from work tables
    If lInvtTranKey > 0 Then
            moItemDist.WarehouseKey = glGetValidLong(moDMGrid.GetColumnValue(Row, "WhseKey"))
            ' Added by Gil: We need a SQL transaction
            moAppDB.BeginTrans
            bIsTransaction = True
            If Not (moItemDist.Delete(lInvtTranKey, lItemKey, kiEffectOnInventory, 0, 0, lOrigItemKey, lShipLineDistKey)) Then
                moAppDB.Rollback
                bIsTransaction = False
                With moItemDist
                    If .oerror.Number <> 0 Then
                        giSotaMsgBox Me, moSysSession, .oerror.messageconstant
                        .oerror.Clear
                        Exit Sub
                    End If
                End With
                Exit Sub
            End If
            gGridUpdateCell grdLine, Row, kColInvTranKey, "0"
            moAppDB.ExecuteSQL "DELETE #timInvtDistWrk WHERE TranKey = " & glGetValidLong(lInvtTranKey)
            moDMGrid.SetRowDirty Row
            moDMGrid.Save
            moAppDB.ExecuteSQL "UPDATE tsoShipLine SET InvtTranKey = NULL WHERE tsoShipLine.ShipLineKey = " & glGetValidLong(moDMGrid.GetColumnValue(Row, "ShipLineKey"))
            ' Added by Gil: Do not forget to update QtyPicked/QtyShipped
            moAppDB.CommitTrans
    Else
        If lInvtTranID > 0 Then
            If Not moItemDist Is Nothing Then
                moItemDist.WarehouseKey = glGetValidLong(moDMGrid.GetColumnValue(Row, "WhseKey"))
                If Not (bCancelInvtTranID(lInvtTranID)) Then
                    Exit Sub
                End If
                gGridUpdateCell grdLine, Row, kColInvTranID, "0"
                moDMGrid.SetRowDirty Row
                moDMGrid.Save
            End If
        End If
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine: 'Repository Error Rig  {1.1.1.0.0}
        If bIsTransaction Then moAppDB.Rollback
        gSetSotaErr Err, "frmConfirmPicks", "ClearItemDist", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
 
 Private Function bCancelInvtTranID(TranId As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    bCancelInvtTranID = False
    
    If Not (moItemDist.Cancel(TranId)) Then
        With moItemDist
            If .oerror.Number <> 0 Then
                giSotaMsgBox Me, moSysSession, .oerror.messageconstant
                .oerror.Clear
                Exit Function
            End If
        End With
        Exit Function
    End If
        
    bCancelInvtTranID = True
 
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
     Exit Function                                                                        'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "bCancelInvtTranID", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
 End Function



Private Sub grdLine_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim dDMQtyPicked As Double
Dim dGridQtyPicked As Double
    
    
    dDMQtyPicked = gdGetValidDbl(moDMGrid.GetColumnValue(Row, "QtyPicked"))
    dGridQtyPicked = gdGetValidDbl(gsGridReadCell(grdLine, Row, kColQtyPicked))

    If Not bValidQtyPicked(Row) Then
        Exit Sub
    End If
    
    moGM.Grid_Change Col, Row
    If dDMQtyPicked <> dGridQtyPicked Then
        AutoPick (Row)
    End If
    
    moGM_CellChange Row, Col

    HideNavs

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "grdLine_Change", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub AutoPick(ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim dQty As Double
    
    dQty = gdGetValidDbl(gsGridReadCell(grdLine, Row, kColQtyPicked))
    With grdLine
        .Col = kColPickCheck
        .Row = Row
    End With
    If dQty < 0 Then
        MsgBox "Quantity picked cannot be less than zero.", vbOKOnly, "Confirm Picks"
    Else
        If dQty > 0 Then
            grdLine.Value = 1
            'Put focus in the Qty Picked cell
            With grdLine
                .Row = Row
                .Row2 = Row
                .Col = kColQtyPicked
                .Col2 = kColQtyPicked
                .Action = SS_ACTION_SELECT_BLOCK
            End With
        Else
            grdLine.Value = 0
        End If
    End If
    bAutoPickUsed = True
    SetPickDefaults (Row)

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "AutoPick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdLine_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim sWhseBinID As String
    Dim lWhseBinKey As Long
    Dim lItemKey As Long
    Dim lWhseKey As Long
'    Dim bMultipleBin As Boolean

    moGM.Grid_Click Col, Row
    
    SetNavRestrict Row, Col
    
    If Row > 0 Then 'if event was not caused by a click in the heading area
        SetRowValues Row
    End If
    
    'Move the auto-dist bin info from the grid to the control.
    GetAutoDistSavedInfo Row
    
    If Row = 0 Then 'hide grid navigators if showing
        HideNavs
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "grdLine_Click", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetNavRestrict(ByVal Row As Long, ByVal Col As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lItemKey As Long
    Dim lWhseKey As Long
    Dim lUOMKey As Long
    Dim iAllowDecimalQty As Integer
    Dim dQtyPicked As Double
    Dim sRestrictClause As String

    If Row = 0 Then
        Exit Sub
    End If

    lItemKey = glGetValidLong(moDMGrid.GetColumnValue(Row, "ItemKey"))
    lWhseKey = glGetValidLong(moDMGrid.GetColumnValue(Row, "WhseKey"))
    lUOMKey = glGetValidLong(moDMGrid.GetColumnValue(Row, "ShipUnitMeasKey"))
    
    iAllowDecimalQty = giGetValidInt(moDMGrid.GetColumnValue(Row, "AllowDecimalQty"))
    sRestrictClause = "UnitMeasKey IN (SELECT a.TargetUnitMeasKey FROM timItemUnitOfMeas a WITH (NOLOCK)"
    sRestrictClause = sRestrictClause & " WHERE a.ItemKey = " & Format$(lItemKey) & ")"
    navGridUOM.RestrictClause = sRestrictClause

    'NOTE: navGridSubItem.RestrictClause used in GetSubItemKey.

    'Only restrict to SubItems that allow Decimals if QtyPicked is decimal.
    dQtyPicked = gdGetValidDbl(gsGridReadCell(grdLine, Row, kColQtyPicked))
    
    'Add restriction to SubItems that allow Decimals if QtyPicked is decimal.
    navGridSubItem.RestrictClause = "ItemKey = " & lItemKey & " AND Status IN (1,3)" & _
        " AND SubstItemKey IN (SELECT SubItemKey FROM fntIMGetInvtItemSubstitute (" & _
        lWhseKey & "," & lItemKey & "," & dQtyPicked & "," & lUOMKey & "," & kAutoSubstitutionUsed & "))"
    
    
'    navGridSubItem.RestrictClause = "ItemKey = " & lItemKey & " AND Status IN (1,3)" & _
'" AND SubstItemKey IN (SELECT DISTINCT ItemKey FROM timInventory WHERE WhseKey = " & lWhseKey & ")" & _
'" AND SubstItemKey IN (SELECT DISTINCT ItemKey FROM timItemUnitOfMeas WHERE TargetUnitMeasKey = " & lUOMKey & ")"
'
'    If dQtyPicked <> CLng(dQtyPicked) Then
'        navGridSubItem.RestrictClause = navGridSubItem.RestrictClause & _
'" AND SubstItemKey IN (SELECT DISTINCT ItemKey FROM timItem WHERE AllowDecimalQty = 1)"
'    End If




'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "SetNavRestrict", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bValidQtyPicked(Row As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim dQtyPicked      As Double
Dim dQtyToPick      As Double
Dim iRslt           As Integer
Dim lTranType       As Long
Dim dOrigQtyPicked  As Double

    bValidQtyPicked = False
    dOrigQtyPicked = gdGetValidDbl(moDMGrid.GetColumnValue(Row, "QtyPicked"))
    dQtyPicked = gdGetValidDbl(gsGridReadCell(grdLine, Row, kColQtyPicked))
    dQtyToPick = gdGetValidDbl(gsGridReadCell(grdLine, Row, kColQtyToPick))
    If dQtyPicked < 0 Then
        iRslt = MsgBox("Quantity picked cannot be less than zero.", vbOKOnly)
        Exit Function
    End If
        
    'Check for overshipment
    lTranType = glGetValidLong(gsGridReadCell(grdLine, Row, kColTranType))
    
    If dQtyPicked > dQtyToPick Then
        If lTranType = kTranTypeSOTS Then
            giSotaMsgBox moClass, moClass.moSysSession, kSOmsgQtyShipCantExceedQtyOpen
            'set the value back to original value
            gGridUpdateCellText grdLine, Row, kColQtyPicked, gsGetValidStr((dOrigQtyPicked))
            gGridSetActiveCell grdLine, Row, kColQtyPicked
            Exit Function
        Else
            Select Case miOverShipmentPolicy
                Case kAllow
                    
                Case kDisAllow
                    giSotaMsgBox moClass, moClass.moSysSession, kSOmsgQtyShipCantExceedQtyOpen
                    'set the value back to original value
                    gGridUpdateCellText grdLine, Row, kColQtyPicked, gsGetValidStr((dOrigQtyPicked))
                    gGridSetActiveCell grdLine, Row, kColQtyPicked
                    Exit Function
                Case kAllowWithWarning
                    giSotaMsgBox moClass, moClass.moSysSession, kSOmsgQtyShipExceedQtyOpen
            End Select
        End If
    End If
        
        
    bValidQtyPicked = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "bValidQtyPicked", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Private Function dGetConvertQty(ByVal lItemKey As Long, ByVal dTranQty As Double, _
                            ByVal lSourceUOMKey As Long, _
                            ByVal lTargetUOMKey As Long) As Double
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++

Dim sSQL As String
Dim rs As Object

    dGetConvertQty = 0

    'Convert the QtyOrdered into Ship UOM
    If lSourceUOMKey <> lTargetUOMKey Then
        'Convert the QtyToPickByOrderUOM using the new UOM
        sSQL = "SELECT QtyByTargetUOM = ROUND(dbo.fnIMItemUOMConv(" & lItemKey & ", " & dTranQty & "," & lSourceUOMKey & "," & lTargetUOMKey & "), " & miNbrDecPlaces & ")"
        Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
        If Not rs.IsEOF Then
            dGetConvertQty = gdGetValidDbl(rs.Field("QtyByTargetUOM"))
        End If
        
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    Else
        dGetConvertQty = dTranQty
    End If
                            
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "dGetConvertQty", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Function
Private Sub grdLine_ColWidthChange(ByVal Col1 As Long, ByVal Col2 As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If glGridGetActiveRow(grdLine) < -1 Then 'Work Around: Bug in clsGridLookup will use -32767 as Active row (this value is returned from the grid.ActiveRow property called in the class)
        Exit Sub
    End If
    moGM.Grid_ColWidthChange Col1
    moGM.Grid_ColWidthChange Col1
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "grdLine_ColWidthChange", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdLine_EditChange(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'User changed the value on the cell.  Set the mbCellChangeFired to false.  It will be set to true
    'once the event fires.  However, if it doesn't fire, we can use this flag to tell if we have to
    'call the GM CellChange manually.
    mbCellChangeFired = False

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLine_EditChange", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdLine_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGM.Grid_KeyDown keycode, Shift
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "grdLine_KeyDown", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdLine_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If KeyAscii = 32 Then
        TogglePickCheck grdLine.ActiveCol, grdLine.ActiveRow
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "grdLine_KeyPress", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdLine_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSubItemID As String
Dim bReturn As Boolean


    If NewRow < 0 Then Exit Sub
    If Row <> NewRow Then
        'Defect #13994: If distribution is invalid, bring up serial/lot/bin form
        bReturn = bValidateDist(Row)
        SetRowValues NewRow
    End If
    
    If Col = kColSubItem Then
        sSubItemID = gsGridReadCell(grdLine, Row, Col)
        If sSubItemID = vbNullString Then    'delete substitute item
            moDMGrid.SetColumnValue Row, "SubItemKey", "NULL"
            moDMGrid.SetColumnValue Row, "SubItemID", Null
            moDMGrid.SetColumnValue Row, "SubItemDesc", Null
            gGridUpdateCellText grdLine, Row, kColSubItem, ""
            gGridUpdateCellText grdLine, Row, kColSubDesc, ""
            'moDMGrid.DoLinks Row
            SetRowValues Row
            grdLine.LeftCol = kColQtyToPick 'Else scrolls back only to QtyPicked col
        End If
    End If
    moGM.Grid_LeaveCell Col, Row, NewCol, NewRow
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "grdLine_LeaveCell", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdLine_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lCol As Long
Dim lRow As Long
'    If bContextMenu Then
'        bContextMenu = False
'        Exit Sub
'    End If
    grdLine.GetCellFromScreenCoord lCol, lRow, x, y
    TogglePickCheck lCol, lRow
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "grdLine_MouseUp", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdLine_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    DoEvents
    
    moGM.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "grdLine_TopLeftChange", VBRIG_IS_FORM          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

'********************** Selection Stuff ****************************
Private Sub grdSelection_EditMode(Index As Integer, ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect(Index).EditMode Col, Row, Mode, ChangeMade
        Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "grdSelection_EditMode", VBRIG_IS_FORM          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdSelection_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect(Index).SelectionGridGotFocus
        Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "grdSelection_GotFocus", VBRIG_IS_FORM          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdSelection_KeyDown(Index As Integer, keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect(Index).SelectionGridKeyDown keycode, Shift
        Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "grdSelection_KeyDown", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdSelection_LeaveCell(Index As Integer, ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect(Index).SelectionGridLeaveCell Col, Row, NewCol, NewRow, Cancel
        Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "grdSelection_LeaveCell", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdSelection_TopLeftChange(Index As Integer, ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect(Index).SelectionGridTopLeftChange OldLeft, OldTop, NewLeft, NewTop
        Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "grdSelection_TopLeftChange", VBRIG_IS_FORM     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function CMRightMouseDown(lWndHandle As Long, lParam As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim y As Long
    Dim x As Long
    Dim lRow As Long
    Dim lCol As Long

    
    CMRightMouseDown = True
    
    '-- Select the row that was right-clicked on
    If lWndHandle = grdLine.hwnd Then
        y = ((lParam And &HFFFF0000) / &H10000) * Screen.TwipsPerPixelY
        x = (lParam And &HFFFF&) * Screen.TwipsPerPixelX
        
        grdLine.GetCellFromScreenCoord lCol, lRow, x, y
                     
        Select Case lRow
            Case 0, -1, -2  '-- RightClick on Column Header or Gray area
                'Do Nothing
             
            Case Else       '-- RightClick on grid proper
                     
                glRC = SendMessage(lWndHandle, WM_LBUTTONDOWN, 0, lParam)
                glRC = SendMessage(lWndHandle, WM_LBUTTONUP, 0, lParam)   ' Simulate click on grid
                gGridSetActiveCell grdLine, lRow, 0
        End Select
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "CMRightMouseDown", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Sub mskControl_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus mskControl(Index), True
    #End If
'+++ End Customizer Code Push +++
    If Not mbLoading Then moSelect(Index).mskControlLostFocus
    bMaskCtrlLostFocus = True
        Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "mskControl_LostFocus", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub navGridSubItem_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sSaveSubItem As String
    Dim sNewSubItem As String
    Dim lRow As Long
    Dim lSOLineUOMKey As Long
    Dim lSaveUOMKey As Long
    Dim lNewUOMKey As Long
    Dim lItemKey As Long
    Dim bResult As Boolean

    lRow = grdLine.ActiveRow
    sSaveSubItem = gsGetValidStr(gsGridReadCell(grdLine, lRow, kColSubItem))
    
    gcLookupClick Me, navGridSubItem, txtNavSubItem, "ItemID"
    moGM.LookupClicked

    sNewSubItem = gsGetValidStr(gsGridReadCell(grdLine, lRow, kColSubItem))
    If sSaveSubItem <> sNewSubItem Then
        'Clear distributions if they exist.
        If glGetValidLong(gsGridReadCell(grdLine, lRow, kColInvTranKey)) > 0 _
Or glGetValidLong(gsGridReadCell(grdLine, lRow, kColInvTranID)) > 0 Then
            'Reset distribution backout / set value.
            ClearItemDist (lRow)
        End If
        
        lItemKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ItemKey"))
        lSaveUOMKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ShipUnitMeasKey"))
        lNewUOMKey = moDMGrid.GetColumnValue(lRow, "SOLineUOMKey")
        moDMGrid.SetColumnValue lRow, "ShipUnitMeasKey", lNewUOMKey
        bResult = bConvertQtyForNewUOM(lRow, lItemKey, lSaveUOMKey, lNewUOMKey)

        'Lock or unlock UOM.
        SetRowValues lRow

        'Refresh substitute item description.
        moDMGrid.DoLink lRow, kColSubDesc
        moDMGrid.DoLink lRow, kColUOM
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "navGridSubItem_Click", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub navGridUOM_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lSaveUOMKey     As Long
    Dim lNewUOMKey      As Long
    Dim lRow            As Long
    Dim lCol            As Long
    Dim lItemKey        As Long

    lRow = grdLine.ActiveRow
    lCol = grdLine.ActiveCol
    lSaveUOMKey = moDMGrid.GetColumnValue(lRow, "ShipUnitMeasKey")
    lItemKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ItemKey"))
    txtNavUOM.Tag = txtNavUOM.Text 'Work-Around to make sure Navigator shows all posible UOMs.

    gcLookupClick Me, navGridUOM, txtNavUOM, "UnitMeasID"
    moGM.LookupClicked

    lNewUOMKey = moDMGrid.GetColumnValue(lRow, "ShipUnitMeasKey")
    
'    moGM.Grid_LeaveCell lCol, lRow, lCol, lRow 'Work-Around to make sure Navigator shows all posible UOMs.

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "navGridUOM_Click", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moGM_CellChange(ByVal lRow As Long, ByVal lCol As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If (lCol <> kColUOM) And (lCol <> kColSubItem) And (lCol <> kColQtyPicked) Then
        Exit Sub
    End If

    Select Case lCol
        Case kColQtyPicked
            SetNavRestrict lRow, lCol
            lkuAutoDistBin_Validate False
            If mbValidAutoDistBin = True Then
                gGridUpdateCell grdLine, lRow, kColAutoDist, "1"
                bAutoDistFromBinLku lRow
            End If
        
        Case kColUOM
            GetUOMKey lRow, lCol

        Case kColSubItem
            GetSubItemKey lRow, lCol
    End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "moGM_CellChange", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub FireGMCellChangeIfNeeded()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'If the user clicks on the toolbar control immediately after typing a value on a
    'grid cell, sometimes the GM CellChange event does not fire.  Make sure the event
    'fires if it hasn't.  (Seems like the issue only applys for the toolbar control.
    'If any other control is clicked, the event still fires.)
    
    'The mbCellChangeFired variable is set to True when the event is called.
    'It is set to False on the Grid_EditChange event or when the user starts changes
    'the value on a cell.
    
    If mbCellChangeFired = False Then
        moGM_CellChange grdLine.ActiveRow, grdLine.ActiveCol
        bValidateDist grdLine.ActiveRow
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FireGMCellChangeIfNeeded", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub GetSubItemKey(lRow As Long, lCol As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sSQL                As String
    Dim lItemKey            As Long
    Dim lOrigItemKey        As Long
    Dim lWhseKey            As Long
    Dim lUOMKey             As Long
    Dim sItemID             As String
    Dim lOrigSubItemKey     As Long
    Dim iAllowDecimalQty    As Integer
    Dim sOrigItemID         As String
    Dim sNewItemDesc        As String
    Dim rs                  As Object
    Dim dQtyPicked          As Double
    Dim sSubItemID          As String

    ' At this time, the column "SubItemKey" of the DM has not been changed
    ' Get old values and save them
    lOrigSubItemKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "SubItemKey"))
    sSQL = "ItemKey = " & CStr(lOrigSubItemKey)
    sOrigItemID = gsGetValidStr(moAppDB.Lookup("ItemID", "timItem", sSQL))
        
    With grdLine
        .Col = lCol
        .Row = lRow
        sItemID = .Value
    End With
        
    'Skip lookup if item was deleted.
    If sItemID <> vbNullString Then
'        sSQL = "ItemID = " & gsQuoted(sItemID) & " AND CompanyID = " & gsQuoted(msCompanyID)
'        lItemKey = glGetValidLong(moAppDB.Lookup("ItemKey", "timItem", sSQL))
        
        lOrigItemKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ItemKey"))
        lWhseKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "WhseKey"))
        lUOMKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ShipUnitMeasKey"))
        dQtyPicked = gdGetValidDbl(gsGridReadCell(grdLine, lRow, kColQtyPicked))
        sSubItemID = gsGetValidStr(gsGridReadCell(grdLine, lRow, kColSubItem))
        
        sSQL = "SELECT t.ItemKey"
        sSQL = sSQL & " FROM timItem t WITH (NOLOCK) "
        'sSQL = sSQL & " JOIN timItemDescription d WITH (NOLOCK) "
        'sSQL = sSQL & " ON t.ItemKey = d.ItemKey "
        sSQL = sSQL & " JOIN (SELECT * FROM fntIMGetInvtItemSubstitute ("
        sSQL = sSQL & lWhseKey & "," & lOrigItemKey & "," & dQtyPicked & ","
        sSQL = sSQL & lUOMKey & "," & kAutoSubstitutionUsed & ")) s"
        sSQL = sSQL & " ON t.ItemKey = s.SubItemKey AND s.ItemKey = " & lOrigItemKey
        sSQL = sSQL & " WHERE t.CompanyID = " & gsQuoted(msCompanyID)
        sSQL = sSQL & " AND t.ItemID = " & gsQuoted(Trim(sSubItemID))
            
        Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
        If Not rs Is Nothing Then
            If Not rs.IsEOF Then
            'Found the substitute item
                lItemKey = glGetValidLong(rs.Field("ItemKey"))
            Else
                lItemKey = 0
            End If
        End If
       
        If lItemKey = 0 Then
            MsgBox "Substitute item is invalid.", vbOKOnly, "Confirm Picks"
            gGridUpdateCellText grdLine, lRow, lCol, sOrigItemID
            SetRowValues lRow
            Exit Sub
        End If

'       LLJ: Not needed per Joanne
'        sSQL = "SubstItemKey = " & lItemKey & " AND " & navGridSubItem.RestrictClause
'        lItemKey = glGetValidLong(moAppDB.Lookup("SubstItemKey", "vSOSubItem WITH (NOLOCK)", sSQL))
'
'        If lItemKey = 0 Then
'            MsgBox "Substitute item is invalid.", vbOKOnly, "Confirm Picks"
'            gGridUpdateCellText grdLine, lRow, lCol, sOrigItemID
'            SetRowValues lRow
'            Exit Sub
'        End If
    Else
        lItemKey = 0
    End If
        
    With moDMGrid
        lOrigSubItemKey = glGetValidLong(.GetColumnValue(lRow, "SubItemKey"))
        If lOrigSubItemKey <> lItemKey Then
            If glGetValidLong(gsGridReadCell(grdLine, lRow, kColInvTranKey)) > 0 _
Or glGetValidLong(gsGridReadCell(grdLine, lRow, kColInvTranID)) > 0 Then
                'Reset distribution backout / set value.
                ClearItemDist (lRow)
            End If
        End If

        If lItemKey = 0 Then
            .SetColumnValue lRow, "SubItemKey", "NULL"
        Else
            .SetColumnValue lRow, "SubItemKey", lItemKey
        End If
        
        'Get the substitute's description.
        If lItemKey = 0 Then
            .SetColumnValue lRow, "SubItemID", Null
            .SetColumnValue lRow, "SubItemDesc", Null
            gGridUpdateCellText grdLine, lRow, kColSubDesc, ""
        Else
            .SetColumnValue lRow, "SubItemID", sItemID
            
            sSQL = "ItemKey = " & CStr(lItemKey)
            sNewItemDesc = gsGetValidStr(moAppDB.Lookup("ShortDesc", "timItemDescription", sSQL))
            
            .SetColumnValue lRow, "SubItemDesc", sNewItemDesc
            gGridUpdateCellText grdLine, lRow, kColSubDesc, sNewItemDesc
        End If
    
        .Save
    End With

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "GetSubItemKey", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub GetUOMKey(lRow As Long, lCol As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL            As String
Dim lSaveUOMKey     As Long
Dim lUOMKey         As Long
Dim lItemKey        As Long
Dim lSubItemKey     As Long
Dim lSOLineUOMKey   As Long
Dim lLookupItemKey  As Long
Dim sSaveUOMID      As String
Dim sNewUOMID       As String
    
    ' At this time, the column "ShipUnitMeasKey" of the DM has not been changed
    ' Get old values and save them
    lSaveUOMKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ShipUnitMeasKey"))
    sSQL = "UnitMeasKey = " & CStr(lSaveUOMKey) & " AND CompanyID = " & gsQuoted(msCompanyID)
    sSaveUOMID = gsGetValidStr(moAppDB.Lookup("UnitMeasID", "tciUnitMeasure", sSQL))
    
    'Get the new UOM key
    With grdLine
        .Col = lCol
        .Row = lRow
        sNewUOMID = .Value
        sSQL = "UnitMeasID = " & gsQuoted(.Value) & " AND CompanyID = " & gsQuoted(msCompanyID)
        lUOMKey = glGetValidLong(moAppDB.Lookup("UnitMeasKey", "tciUnitMeasure", sSQL))
    End With
    
    'If there is a substitute item, make sure the UOM is equal to the SO Line UOM
    lSubItemKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "SubItemKey"))
    lItemKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ItemKey"))
    If (lItemKey <> lSubItemKey And lSubItemKey > 0) Then
        lSOLineUOMKey = glGetValidLong(gsGridReadCell(grdLine, lRow, kColSOLineUOMKey))
        If lUOMKey <> lSOLineUOMKey Then
            MsgBox "UOM is invalid.  The UOM for a substitute item must be equal to the UOM on the Sales Order line.", vbOKOnly, "Confirm Picks"
            'moDMGrid.DoLinks grdLine.Row
            ' Restore old UOM ID
            gGridUpdateCellText grdLine, lRow, lCol, sSaveUOMID
            gProcessSendKeys "+{TAB}"
            Exit Sub
        End If
    End If
    
    'Is this UOM valid for the item?
    sSQL = "ItemKey = " & lItemKey & " AND TargetUnitMeasKey = " & lUOMKey
    lLookupItemKey = glGetValidLong(moAppDB.Lookup("ItemKey", "timItemUnitOfMeas", sSQL))
    
    If lLookupItemKey = 0 Then
        MsgBox "This UOM is invalid for this item.", vbOKOnly, "Confirm Picks"
        'moDMGrid.DoLinks grdLine.Row
        ' Restore old UOM ID
        gGridUpdateCellText grdLine, lRow, lCol, sSaveUOMID
        gProcessSendKeys "+{TAB}"
        Exit Sub
    End If
    
    'Apply changes

    With moDMGrid
        'lSaveUOMKey = glGetValidLong(.GetColumnValue(lRow, "ShipUnitMeasKey"))
        If bConvertQtyForNewUOM(lRow, lItemKey, lSaveUOMKey, lUOMKey) = True Then
            .SetColumnValue lRow, "ShipUnitMeasKey", lUOMKey
            .SetColumnValue lRow, "UnitMeasID", sNewUOMID
            
            .SetRowDirty lRow
            .Save
            
             'Update the restrictions in the lookup control
            SetNavRestrict lRow, lCol

            '.DoLinks lRow
        Else
            'moDMGrid.DoLinks grdLine.Row
            ' Restore old UOM ID
            gGridUpdateCellText grdLine, lRow, lCol, sSaveUOMID
            gProcessSendKeys "+{TAB}"
            Exit Sub
        End If
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "GetUOMKey", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub nbrControl_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrControl(Index), True
    #End If
'+++ End Customizer Code Push +++
    If Not mbLoading Then moSelect(Index).nbrControlLostFocus
        Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "nbrControl_LostFocus", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub curControl_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curControl(Index), True
    #End If
'+++ End Customizer Code Push +++
    If Not mbLoading Then moSelect(Index).curControlLostFocus
        Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "curControl_LostFocus", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub navControl_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect(kSalesOrder).LookupControlClick Index, ""
        Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "navControl_GotFocus", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub navControlTransfer_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect(kTransferOrder).LookupControlClick Index, ""
        Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "navControlTransfer_GotFocus", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub HandletoolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If
Dim bResult As Boolean
'********************************************************************
' Description:
'    This routine is the central routine for processing events when a
'    button on the Toolbar is clicked.
'********************************************************************
''''    'Cause LostFocus events to fire before handling toolbar click
''''    If (glGridGetDataRowCnt(grdLine) > 0) And (Screen.ActiveControl.Name = grdLine.Name) Then
''''        grdLine_Change grdLine.ActiveCol, grdLine.ActiveRow
''''    End If
    Select Case sKey
        Case kTbFinishExit
            frmConfirmPicks.MousePointer = vbHourglass
            mbFinishAndExit = False 'Do not shutdown task if errors exist
            If glGridGetDataRowCnt(grdLine) > 0 Then
                ' The following line insures that the grid_Change event gets called when the user moves from the grid directly to the toolbar
                ' If this happens, the grid events do not fire automatically
                If Me.ActiveControl.Name = "grdLine" Then
                    gProcessSendKeys "{TAB}", True
                End If
                
                'Make sure the GM CellChange event fires.
                FireGMCellChangeIfNeeded
                
                If bValidGridData Then
                    moDMGrid.Save
                    HideNavs
                    If bGridSaveChanges() Then
                        MsgBox "Updates have been completed.", vbOKOnly
                        moDMGrid.Refresh 'only refresh if Save ok
                        mbFinishAndExit = True
                    Else
                        Exit Sub
                    End If
                End If
            Else
                mbFinishAndExit = True 'Shutdown if the grid is empty
            End If
            frmConfirmPicks.MousePointer = vbDefault
            If mbFinishAndExit = True Then
                moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            End If
        Case kTbCancelExit
            'moSelect(0).ResetGrid 'reset the sales order selection grid criteria
            'moSelect(1).ResetGrid 'reset the transfer order selection grid criteria
            moMultSelGridMgr.Reset
            cmdDeSelect_Click 'delete all rows and refresh Grid Manager
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
        Case kTbFinish
            frmConfirmPicks.MousePointer = vbHourglass
            If glGridGetDataRowCnt(grdLine) > 0 Then
                ' The following line insures that the grid_Change event gets called when the user moves from the grid directly to the toolbar
                ' If this happens, the grid events do not fire automatically.
                If Me.ActiveControl.Name = "grdLine" Then
                    gProcessSendKeys "{TAB}", True
                End If
                
                'Make sure the GM CellChange event fires.
                FireGMCellChangeIfNeeded
                
                If bValidGridData Then
                    moDMGrid.Save
                    HideNavs
                    If bGridSaveChanges() Then
                        MsgBox "Updates have been completed.", vbOKOnly
                        
                        'Reset Grid when NO Custom Settings
                        If UCase(Trim(cboSetting.Text)) = "(NONE)" Then
                            moMultSelGridMgr.Reset
                        End If
                        
                        moDMGrid.Refresh 'only refresh if Save ok
                    End If
                    SetFormState
                End If
            End If
            frmConfirmPicks.MousePointer = vbDefault
        Case kTbCancel
            'moSelect(0).ResetGrid 'reset the sales order selection grid criteria
            'moSelect(1).ResetGrid 'reset the transfer order selection grid criteria
            moMultSelGridMgr.Reset
            cmdDeSelect_Click 'delete all rows and refresh Grid Manager
        Case kTbSave
            moSettings.ReportSettingsSaveAs
        Case kTbDelete
            moSettings.ReportSettingsDelete
        Case kTbHelp
            gDisplayFormLevelHelp Me
        Case Else
            tbrMain.GenericHandler sKey, Me, moDMGrid, moClass
    End Select
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "HandletoolbarClick", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bValidateDist(lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lItemKey        As Long
Dim lOrigItemKey    As Long
Dim iConfirmed      As Integer
Dim dQtyPicked      As Double
Dim dSumOfDist      As Double
Dim iItemType       As Integer
Dim lSOUOMKey       As Long
Dim lUOMKey         As Long

    bValidateDist = False
    
    If mbCellChangeFired = False Then
        mbCellChangeFired = True
    End If

    'Skip previously confirmed lines that are to be unconfirmed
    iConfirmed = giGetValidInt(gsGridReadCell(grdLine, lRow, kColConfirmed))
    dQtyPicked = gdGetValidDbl(gsGridReadCell(grdLine, lRow, kColQtyPicked))
    iItemType = giGetValidInt(gsGridReadCell(grdLine, lRow, kColItemType))

    'If previosly confirmed and user is cancelling pick updates
    If iConfirmed = 1 And dQtyPicked = 0 Then
        bValidateDist = True
        Exit Function
    End If

    'Only distribute Finished Goods, Raw Materials and Assembled Kits (needed for kit components that are not these three types)
    Select Case iItemType
        Case 5, 6, 8
        Case Else
            bValidateDist = True
            Exit Function
    End Select

    CreateDistForm lRow
    mlInvtTranID = glGetValidLong(gsGridReadCell(grdLine, lRow, kColInvTranID))
    mlInvtTranKey = glGetValidLong(gsGridReadCell(grdLine, lRow, kColInvTranKey))
    miTrackMethod = giGetValidInt(gsGridReadCell(grdLine, lRow, kColTrackMeth))
    miTrackByBin = giGetValidInt(gsGridReadCell(grdLine, lRow, kColTrackQtyAtBin))
    mdQtyPicked = gdGetValidDbl(gsGridReadCell(grdLine, lRow, kColQtyPicked))
    mlWhseKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "WhseKey"))
    lSOUOMKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "SOLineUOMKey"))
    lUOMKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ShipUnitMeasKey"))
    dSumOfDist = gdGetValidDbl(gsGridReadCell(grdLine, lRow, kColSumOfDist))
    lItemKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "SubItemKey"))
    If lItemKey = 0 Then
        lOrigItemKey = 0
        lItemKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ItemKey"))
    Else
        lOrigItemKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ItemKey"))
    End If

    If miTrackByBin = 0 And miTrackMethod = 0 And mlWhseKey = 0 Then 'Item does not require distributions
        bValidateDist = True
        Exit Function
    End If

    'If the auto-distribution cell is true, perform the auto-distribution now.
    If bAutoDistFromBinLku(lRow) Then
        dSumOfDist = gdGetValidDbl(gsGridReadCell(grdLine, lRow, kColSumOfDist))
    End If

    ' Item does require distributions but defaults can be used
    ' But this will be done in spsoConfPickUpdate
    If (lUOMKey = lSOUOMKey) _
        And (dQtyPicked = dSumOfDist) _
        And (miTrackMethod = 0) _
        And (lOrigItemKey = 0) Then
        bValidateDist = True
        Exit Function
    End If

    '   - We still want to save although the distribution is invalid
    '   - Check to see if the distribution is valid by comparing the quantities
    '     rather than check mlInvtTranID
    'If mlInvtTranID = 0 Then 'Need to show distributions dialog
    ' bin tracted item set changed for posting issue/ not launching bin form
    If (dSumOfDist <> dQtyPicked) Or (Not moItemDist.IsDistComplete(dQtyPicked, lUOMKey, mlInvtTranKey, mlInvtTranID)) Then 'Need to show distributions dialog
        If Not bLoadLotSerBinForm(lRow) Then
            'must be set to false so update is not allowed to complete
            bValidateDist = False
            Exit Function
        End If
    End If
   
    bValidateDist = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "bValidateDist", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Function bGridSaveChanges() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
On Error GoTo ExpectedErrorRoutine

Dim lRet        As Long

Dim lErrorKey   As Long
Dim lMsgNo      As Long
Dim lRow        As Long

    bGridSaveChanges = False
    lRet = 0

    On Error Resume Next
    With moAppDB
        .SetInParam msCompanyID
        .SetInParam gsFormatDateToDB(moSysSession.BusinessDate)
        .SetOutParam lMsgNo
        .SetOutParam lErrorKey
        .SetOutParam lRet
        .ExecuteSP ("spsoConfPickUpdate")
        If Err.Number <> 0 Then
            lRet = Err.Number
        Else
            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
            'errors after each attempt.  After storing all return values in local variables OR
            'encountering an error, release parameters.
            lMsgNo = .GetOutParam(3)
            lErrorKey = .GetOutParam(4)
            lRet = .GetOutParam(5)
        End If
        .ReleaseParams
    End With
    
    Select Case lRet
        Case 1 'no errors
        Case 2 'Concurr Error
            If lErrorKey > 0 Then
                lRow = lSetErrorLine(lErrorKey)
                MsgBox "Line " & lRow & " was updated by another task."
            End If
            Exit Function
        Case 3 'Incomplete Kit
            giSotaMsgBox Me, moSysSession, kmsgCompQtyNotSelected
            Exit Function
        Case Else '0 -sp did not complete successfully
            If lErrorKey > 0 Then
                lSetErrorLine lErrorKey
            End If
            If lMsgNo > 0 Then
                giSotaMsgBox Me, moSysSession, lMsgNo
            Else
                MsgBox "Error: " & lRet & " occured in stored procedure spsoConfPickUpdate." 'inform user that there are problems
            End If

            Exit Function
    End Select
    
    bGridSaveChanges = True
    
    Exit Function
    
ExpectedErrorRoutine:

    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "bGridSaveChanges", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function lSetErrorLine(ErrorKey As Long) As Long
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lRow                As Long
Dim lShipLineDistKey    As Long

    For lRow = 1 To glGridGetDataRowCnt(grdLine)
        lShipLineDistKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ShipLineDistKey"))
        If lShipLineDistKey = ErrorKey Then
            bSetCellStates   'Lock cells unlocked by refresh
            'Put focus in the Qty Picked cell first (to force leave cell focus)  then select the row
            gGridSetTopRow grdLine, lRow
            gGridSetActiveCell grdLine, lRow, kColQtyPicked
            gGridSetSelectRow grdLine, lRow
            grdLine.SetFocus
            lSetErrorLine = lRow
            Exit For
        End If
    Next lRow



'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "lSetErrorLine", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Private Function bValidGridData() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lRow As Long
Dim iChoice As Integer
Dim iNbrZeroQtyLines As Integer
Dim lKitLineKey As Long
Dim dQtyPicked As Double

    bValidGridData = True ' assume all data is valid

    'Delete all unpicked lines before validating
    If Not bDeleteUnpicked Then
        bValidGridData = False
        Exit Function
    End If
    
    'Call SP to retreive the Inventory Ship Complete Flags
    If Not bGetShipCompleteInvFlags Then
        bValidGridData = False
        Exit Function
    End If

    'Refresh grid with latest changes before starting validations
    moDMGrid.Refresh
    
    'Loop through Grid validating row by row
    For lRow = 1 To (glGridGetDataRowCnt(grdLine))

        lKitLineKey = glGetValidLong(gsGridReadCell(grdLine, lRow, kColKitLineKey))
        dQtyPicked = gdGetValidDbl(gsGridReadCell(grdLine, lRow, kColQtyPicked))
        
        If Not (dQtyPicked = 0 And lKitLineKey = 0) Then
        'No need to do validation on Qty or Distribution for non-kit components which
        'QtyPicked is 0
            If iAmtToPickInvalid(lRow) = kiAmtToPickInvalid Then
                gGridSetTopRow grdLine, lRow
                'grdLine.LeftCol = kColQtyToPick 'Else scrolls back only to QtyPicked col
                grdLine_Click kColQtyPicked, lRow
                gGridSetActiveCell grdLine, lRow, kColQtyPicked
                bValidGridData = False
                Exit Function
            End If
'            If Not bValidateDist(lRow) Then
'                gGridSetTopRow grdLine, lRow
'                'grdLine.LeftCol = kColQtyToPick 'Else scrolls back only to QtyPicked col
'                grdLine_Click kColQtyPicked, lRow
'                gGridSetActiveCell grdLine, lRow, kColQtyPicked
'                bValidGridData = False
'                Exit Function
'            End If
        End If
    Next lRow
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "bValidGridData", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bDeleteUnpicked() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL        As String
Dim lRow        As Long
Dim iPickedCnt  As Integer
Dim iPicked     As Integer
Dim iConfirmed  As Integer

    bDeleteUnpicked = False
        
'    For lRow = 1 To (glGridGetDataRowCnt(grdLine))
'        iPicked = giGetValidInt(gsGridReadCell(grdLine, lRow, kColPickCheck))
'        iConfirmed = giGetValidInt(gsGridReadCell(grdLine, lRow, kColConfirmed))
'        If iPicked = 1 Or iConfirmed = 1 Then
'            iPickedCnt = iPickedCnt + 1
'        End If
'    Next lRow
    '****LLJ don't check for this anymore
'    iPickedCnt = moAppDB.Lookup("Count(*)", "#tsoConfPickWrk2", "PickStatus=1 OR Confirmed=1")
'
'    If iPickedCnt = 0 Then
'        MsgBox "No rows have been picked for processing.", vbOKOnly
'        Exit Function
'    End If
    
    moDMGrid.Save
    
    'delete all rows not picked that were not previously picked
'    sSQL = "DELETE FROM #tsoConfPickWrk2 WHERE PickStatus = 0 AND Confirmed = 0"
'    moAppDB.ExecuteSQL sSQL
'    If Err.Number <> 0 Then
'        MsgBox "Error deleting unpicked rows." & " Error = " & Err.Number & " " & Err.Description, vbOKOnly
'        Exit Function
'    End If
'
'    moDMGrid.Refresh
    bDeleteUnpicked = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "bDeleteUnpicked", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bGetShipCompleteInvFlags() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lRet        As Long

    bGetShipCompleteInvFlags = True
    lRet = 0
    With moAppDB
        .SetInParam msCompanyID
        .SetOutParam lRet
        .ExecuteSP ("spsoConfGetInvtInfo")
        If Err.Number <> 0 Then
            lRet = Err.Number
        Else
            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
            'errors after each attempt.  After storing all return values in local variables OR
            'encountering an error, release parameters.
            lRet = .GetOutParam(2)
        End If
        .ReleaseParams
     End With
     
     Select Case lRet
        Case 1 'all is well
            Exit Function
        Case Else
            bGetShipCompleteInvFlags = False
            MsgBox "Error " & lRet & " occurred in the stored procedure spsoConfGetInvtInfo, please contact Customer Support.", vbOKOnly
            Exit Function
    End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "bGetShipCompleteInvFlags", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function iAmtToPickInvalid(lRow As Long) As Integer
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Static lKitLineKeySave  As Long
Dim iConfirmed          As Integer
Dim iAllowDecimalQty    As Integer
Dim lShipUOMKey         As Long
Dim lOrderLineUOMKey    As Long
Dim lItemKey            As Long
Dim sSQL                As String
Dim rs                  As Object
Dim dQtyOrdered         As Double
Dim dQtyOrderedByShipUOM As Double
Dim lTranType           As Long

    iAmtToPickInvalid = kiAmtToPickValid
    
    'Init the static value if this is the first time through
    If lRow = 1 Then
        lKitLineKeySave = 0
    End If
    
    mlInvtTranID = glGetValidLong(gsGridReadCell(grdLine, lRow, kColInvTranID))
    mlInvtTranKey = glGetValidLong(gsGridReadCell(grdLine, lRow, kColInvTranKey))
    mlKitLineKey = glGetValidLong(gsGridReadCell(grdLine, lRow, kColKitLineKey))
    miTrackMethod = giGetValidInt(gsGridReadCell(grdLine, lRow, kColTrackMeth))
    miTrackByBin = giGetValidInt(gsGridReadCell(grdLine, lRow, kColTrackQtyAtBin))
    mdQtyToPick = gdGetValidDbl(gsGridReadCell(grdLine, lRow, kColQtyToPick))
    mdQtyPicked = gdGetValidDbl(gsGridReadCell(grdLine, lRow, kColQtyPicked))
    miShipComplete = giGetValidInt(gsGridReadCell(grdLine, lRow, kColShipComplete))
    miShipCompleteInv = giGetValidInt(gsGridReadCell(grdLine, lRow, kColShipCompleteInv))
    iConfirmed = giGetValidInt(gsGridReadCell(grdLine, lRow, kColConfirmed))
    iAllowDecimalQty = giGetValidInt(gsGridReadCell(grdLine, lRow, kColAllowDecimalQty))
    
    dQtyOrdered = gdGetValidDbl(gsGridReadCell(grdLine, lRow, kColQtyOrdered))
    
    lShipUOMKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ShipUnitMeasKey"))
    lOrderLineUOMKey = glGetValidLong(gsGridReadCell(grdLine, lRow, kColSOLineUOMKey))
    lItemKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ItemKey"))
    
    'Convert the QtyOrdered into Ship UOM
    dQtyOrderedByShipUOM = dGetConvertQty(lItemKey, dQtyOrdered, lOrderLineUOMKey, lShipUOMKey)
    
    If mlKitLineKey > 0 Then 'And mdQtyToPick <> mdQtyPicked Then
        lKitLineKeySave = glGetValidLong(gsGridReadCell(grdLine, lRow, kColKitLineKey))
        If bKitInvalid(mlKitLineKey) Then
            iAmtToPickInvalid = kiAmtToPickInvalid
            Exit Function
        End If
    End If
    
    'This will allow for the un-confirmation of previously picked lines
    If iConfirmed = 1 And mdQtyPicked = 0 Then
        Exit Function
    End If
    
    'Check for shipment complete
    If miShipComplete = 1 Then
        If dQtyOrderedByShipUOM > mdQtyPicked Then
            MsgBox "Customer does not allow partial shipments.", vbOKOnly
            iAmtToPickInvalid = kiAmtToPickWarning
'            Exit Function
        End If
    End If
    
    If (miShipCompleteInv = 1) And mdQtyPicked < dQtyOrderedByShipUOM Then
        MsgBox "Inventory settings require this item to be shipped completely from this warehouse." & vbCr & "Quantity picked must be equal to quantity to pick.", vbOKOnly
        iAmtToPickInvalid = kiAmtToPickWarning
'        Exit Function
    End If
            
    'Check for negative picking quantity
    If mdQtyPicked < 0 Then
        giSotaMsgBox moClass, moClass.moSysSession, kmsgInvalidEntity, mdQtyPicked
        iAmtToPickInvalid = kiAmtToPickInvalid
        Exit Function
    End If
    
'    'Check for overshipment
'    lTranType = glGetValidLong(gsGridReadCell(grdLine, lRow, kColTranType))
'
'    If mdQtyPicked > mdQtyToPick Then
'        If lTranType = kTranTypeSOTS Then
'            giSotaMsgBox moClass, moClass.moSysSession, kSOmsgQtyShipCantExceedQtyOpen
'            iAmtToPickInvalid = kiAmtToPickInvalid
'            Exit Function
'        Else
'            Select Case miOverShipmentPolicy
'                Case kAllow
'
'                Case kDisAllow
'                    giSotaMsgBox moClass, moClass.moSysSession, kSOmsgQtyShipCantExceedQtyOpen
'                    iAmtToPickInvalid = kiAmtToPickInvalid
'                    Exit Function
'                Case kAllowWithWarning
'                    giSotaMsgBox moClass, moClass.moSysSession, kSOmsgQtyShipExceedQtyOpen
'                    iAmtToPickInvalid = kiAmtToPickWarning
'            End Select
'        End If
'    End If
    
    'validate Items that do not allow decimal quantitites
    If (iAllowDecimalQty = 0) And (CLng(mdQtyPicked) <> mdQtyPicked) Then
        MsgBox "This item does not allow decimal quantities.", vbOKOnly
        iAmtToPickInvalid = kiAmtToPickInvalid
        Exit Function
    End If
    
    
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPick", "iAmtToPickInvalid", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bKitInvalid(lKitLineKey As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lRet             As Long
Dim lKitQty          As Long
Dim lShipLineDistKey As Long
Dim sSQL             As String
Dim sItemID          As String
Dim lReqCompQty      As Long

    bKitInvalid = False
    lRet = 0
    With moAppDB
        .SetInParam lKitLineKey
        .SetOutParam lKitQty
        .SetOutParam lShipLineDistKey
        .SetOutParam lRet
        .ExecuteSP ("spsoConfPickKit")
        If Err.Number <> 0 Then
            lRet = Err.Number
        Else
            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
            'errors after each attempt.  After storing all return values in local variables OR
            'encountering an error, release parameters.
            lShipLineDistKey = .GetOutParam(3) 'Scopus#10666-Contains the distribution line resulting in incomplete kit(lRet = 2)
            lRet = .GetOutParam(4)
        End If
        .ReleaseParams
     End With
     
     Select Case lRet
        Case 0, -1, -2 'sp did not complete successfully
            bKitInvalid = True
            MsgBox "Error " & lRet & " occurred in the stored procedure spsoConfPickKit, please contact Customer Support.", vbOKOnly    'inform user that there are problems
            Exit Function
        Case -4
            bKitInvalid = True
            giSotaMsgBox Me, moSysSession, kSOKitOvershipment
            Exit Function
        Case -3 'kit is incomplete
            bKitInvalid = True
            giSotaMsgBox Me, moSysSession, kSOmsgPartialPickedKit
            Exit Function
        Case 2 'quantity is incomplete
            sSQL = " tsoShipLineDist.ShipLineKey = tsoShipLine.ShipLineKey " & "AND tsoShipLine.ItemKey = timItem.ItemKey " & "AND tsoShipLineDist.ShipLineDistKey = " & lShipLineDistKey
            sItemID = gsGetValidStr(moAppDB.Lookup("timItem.ItemID", "tsoShipLineDist WITH (NOLOCK), tsoShipLine WITH (NOLOCK), timItem WITH (NOLOCK) ", sSQL))
            lReqCompQty = glGetValidLong(moAppDB.Lookup("tsoShipLineDist.QtyToPick", "tsoShipLineDist WITH (NOLOCK) ", "tsoShipLineDist.ShipLineDistKey = " & lShipLineDistKey))
            bKitInvalid = True
            giSotaMsgBox Me, moSysSession, kSOmsgQtyNeedToBuildKit, CStr(lReqCompQty), sItemID
            Exit Function
        Case 1 'kit is ok
            Exit Function
        Case -99 'partial confirmation
            bKitInvalid = True
            giSotaMsgBox Me, moSysSession, kSOmsgPartialConfirmedKit
            Exit Function
        Case Else
            bKitInvalid = True
            MsgBox "Error " & lRet & " occurred in the stored procedure spsoConfPickKit, please contact Customer Support.", vbOKOnly
            Exit Function
    End Select
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "bKitInvalid", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bQtyToPickZero() As Integer
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lRow As Long
Dim mdQtyPicked As Double
Dim iConfirmed As Integer

    bQtyToPickZero = 0 'default to 0, no QtyToPick columns = zero
    For lRow = 1 To (glGridGetDataRowCnt(grdLine))
        mdQtyPicked = CDbl(gsGridReadCell(grdLine, lRow, kColQtyPicked))
        iConfirmed = giGetValidInt(gsGridReadCell(grdLine, lRow, kColConfirmed))
        If mdQtyPicked = 0 And iConfirmed = 0 Then
            bQtyToPickZero = bQtyToPickZero + 1
        End If
    Next lRow
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPick", "bQtytoPickZero", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    HandletoolbarClick Button
        Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "tbrMain_ButtonClick", VBRIG_IS_FORM       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If


Private Sub cmdPickAll_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdPickAll, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPickAll_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdPickAll_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdPickAll, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPickAll_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdClearPicks_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdClearPicks, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdClearPicks_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdClearPicks_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdClearPicks, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdClearPicks_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdLotSerialBin_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdLotSerialBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdLotSerialBin_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdLotSerialBin_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdLotSerialBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdLotSerialBin_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSelect_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdSelect, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSelect_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSelect_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdSelect, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSelect_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDeSelect_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdDeSelect, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDeSelect_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDeSelect_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdDeSelect, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDeSelect_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub txtMultipleBin_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtMultipleBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMultipleBin_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMultipleBin_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtMultipleBin, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMultipleBin_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMultipleBin_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtMultipleBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMultipleBin_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMultipleBin_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtMultipleBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMultipleBin_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavUOM_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtNavUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavUOM_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavUOM_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtNavUOM, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavUOM_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavUOM_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtNavUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavUOM_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavUOM_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtNavUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavUOM_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavSubItem_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtNavSubItem, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavSubItem_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavSubItem_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtNavSubItem, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavSubItem_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavSubItem_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtNavSubItem, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavSubItem_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavSubItem_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtNavSubItem, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavSubItem_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub mskControl_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange mskControl(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "mskControl_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub mskControl_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress mskControl(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "mskControl_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub mskControl_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus mskControl(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "mskControl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuAutoDistBin_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuAutoDistBin_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuAutoDistBin_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuAutoDistBin, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuAutoDistBin_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuAutoDistBin_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuAutoDistBin_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuAutoDistBin_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuAutoDistBin_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub curControl_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curControl(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curControl_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curControl_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curControl(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curControl_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curControl_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curControl(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curControl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrControl_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrControl(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrControl_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrControl_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrControl(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrControl_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrControl_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrControl(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrControl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkShowOnlyPicked_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkShowOnlyPicked, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkShowOnlyPicked_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkShowOnlyPicked_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkShowOnlyPicked, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkShowOnlyPicked_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInclude_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkInclude(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInclude_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInclude_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkInclude(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInclude_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSetting_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboSetting, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSetting_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSetting_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboSetting, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSetting_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSetting_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboSetting, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSetting_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then
Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If


Public Property Get MyApp() As Object
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property

Private Sub SetupMultipleGrid()
'+++ VB/Rig Begin Push +++
    #If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
    #End If
'+++ VB/Rig End +++
    Dim iCounter As Integer

    'Initialize Tab Base Label Strings
    msBaseCaption(kSalesOrder) = gsBuildString(kSOSalOrders, moClass.moAppDB, moClass.moSysSession)
    'msBaseCaption(kTransferOrder) = gsBuildString(166124, moClass.moAppDB, moClass.moSysSession)
    msBaseCaption(kTransferOrder) = gsBuildString(250662, moClass.moAppDB, moClass.moSysSession)

    'load captions for include check boxes
    For iCounter = 0 To 1
        chkInclude(iCounter).Caption = msBaseCaption(iCounter)
    Next iCounter
    
    ' initialize moOptions
    Set moOptions = New clsOptions
    For iCounter = 0 To UBound(moSelect)
        moOptions.Add chkInclude(iCounter)
    Next iCounter
        
    'initialize the multi selection mgr
    Set moMultSelGridMgr = New clsMultiSelGridSettings
    moMultSelGridMgr.Initialize moClass.moAppDB, moSysSession.Language, moSelect(), grdSelSettings
    
    'set the hiden tab to not visible
    tabSelect.TabVisible(2) = False
    
    'setup the tab captions
    For iCounter = 0 To UBound(moSelect)
    
        tabSelect.TabCaption(iCounter) = moMultSelGridMgr.sBuildCaption(msBaseCaption(iCounter), _
grdSelection(iCounter), tabSelect.TabEnabled(iCounter))
    Next iCounter
    
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "SetupMultipleGrid", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

End Sub
Private Sub SetupSettings()
'+++ VB/Rig Begin Push +++
    #If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
    #End If
'+++ VB/Rig End +++

 Set moSettings = New clsSettings


    moSettings.Initialize Me, cboSetting, , moMultSelGridMgr, moOptions, , tbrMain, sbrMain

    '-- CUSTOMIZE:  Add all controls on Opt                   ions page for which you will want to save settings
    '-- to the moOptions collection, using its Add method.
    If (Trim$(cboSetting.Text) <> "(None)") Then 'And cboSetting.Visible = True
'        mbLoadSetting = True
        moSettings.bLoadReportSettings
'        mbLoadSetting = False
        RefreshTabCaptions
    End If



'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "SetupSettings", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

End Sub
Private Sub RefreshTabCaptions()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim i As Integer
    For i = 0 To UBound(moSelect())
        tabSelect.TabCaption(i) = moMultSelGridMgr.sBuildCaption(msBaseCaption(i), grdSelection(i), tabSelect.TabEnabled(i))
    Next
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmConfirmPicks", "RefreshTabCaptions", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

End Sub
Private Sub grdLine_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
    moGM.Grid_EditMode Col, Row, Mode, ChangeMade
End Sub

Private Sub lkuAutoDistBin_BeforeValidate(Text As String, iReturn As EntryLookupControls.ENTRY_LKU_CTRL_ValidateTypes)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Validate the lookup control myself through local routine.
    iReturn = EL_CTRL_VALID
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuAutoDistBin_BeforeValidate", VBRIG_IS_FORM          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuAutoDistBin_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuAutoDistBin, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

    Dim iTrackMeth As Integer
    
    'Set flag use to determine if user has selected a value from the lookup section grid.
    'Flag is reset to False after auto-distribution is saved.
    If colSQLReturnVal.Count <> 0 Then
        mbAutoDistBinClicked = True
        gGridUpdateCell grdLine, grdLine.ActiveRow, kColAutoDistBinID, Trim(gsGetValidStr(colSQLReturnVal(1)))
        
        iTrackMeth = giGetValidInt(gsGridReadCell(grdLine, grdLine.ActiveRow, kColTrackMeth))
        'If the item is lot tracked and the user selected the same bin but a different lot
        'from the lookup, set the tag to blank so the new lot/bin can be validated.
        If iTrackMeth = 1 And LCase(Trim(lkuAutoDistBin.Text)) = LCase(Trim(colSQLReturnVal(1))) Then
            If LCase(Trim(gsGridReadCell(grdLine, grdLine.ActiveRow, kColAutoDistLotNo))) <> LCase(Trim(colSQLReturnVal(3))) Then
                lkuAutoDistBin.Tag = ""
            End If
        End If

    Else
        mbAutoDistBinClicked = False
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuAutoDistBin_BeforeLookupReturn", VBRIG_IS_FORM      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuAutoDistBin_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuAutoDistBin, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    
    Dim lRow As Long
    Dim lTranType As Long
    Dim lWhseKey As Long
    Dim lItemKey As Long
    Dim lUOMKey As Long
    Dim lInvtTranKey As Long
    Dim dQtyToDist As Double
    
    lRow = grdLine.ActiveRow
    
    'Get values for the distribution method.
    lTranType = glGetValidLong(gsGridReadCell(grdLine, lRow, kColTranType))
    lWhseKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "WhseKey"))
    lItemKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "SubItemKey"))
    If lItemKey = 0 Then
        lItemKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ItemKey"))
    End If
    lUOMKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ShipUnitMeasKey"))
    lInvtTranKey = glGetValidLong(gsGridReadCell(grdLine, lRow, kColInvTranKey))
    dQtyToDist = gdGetValidDbl(gsGridReadCell(grdLine, lRow, kColQtyPicked))

    moItemDist.bAutoDistBinLkuSetupClick lkuAutoDistBin, lWhseKey, lItemKey, lTranType, lUOMKey, dQtyToDist, lInvtTranKey

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuAutoDistBin_LookupClick", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuAutoDistBin_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

    Dim bDoAutoDist As Boolean
    
    'Bring the bin lookup to the front.
    lkuAutoDistBin.ZOrder 0
    
    If mbValidAutoDistBin = True And lkuAutoDistBin.IsValid = True Then
        lkuAutoDistBin.Tag = lkuAutoDistBin.Text
        bDoAutoDist = True
    Else
        bDoAutoDist = False
    End If

    gGridUpdateCell grdLine, grdLine.ActiveRow, kColAutoDist, IIf(bDoAutoDist = True, "1", "0")
    
    If bDoAutoDist = True Then
        bAutoDistFromBinLku grdLine.ActiveRow
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuAutoDistBin_LostFocus", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuAutoDistBin_Validate(Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim sAutoDistBinID As String
    Dim lAutoDistWhseBinKey As Long
    Dim dAutoDistBinQtyAvail As Double
    Dim lAutoDistBinUOMKey As Long
    Dim sAutoDistLotNo As String
    Dim sAutoDistLotExpDate As String
    Dim lAutoDistInvtLotKey As Long
    Dim bCanAutoDist As Boolean
    Dim lRow As Long
    
    Dim lTranType As Long
    Dim lWhseKey As Long
    Dim lItemKey As Long
    Dim lUOMKey As Long
    Dim dQtyToDist As Double
    Dim sWhseBinID As String
    Dim lWhseBinKey As Long
    Dim sBusinessDate As String
    Dim lInvtTranKey As Long
    Dim colAutoDistBinRetCol As Collection
    
    'This validation routine is designed to tell if the bin can be used
    'to auto-distribute the transaction.  Based on the tracking method,
    'it will try to see if there is enough information to do the auto-
    'distribution.  For example: Lot-tracked items require a lot number.
    
    'We will try to use the return column collection if we detect the user
    'selected a value by clicking the lookup, otherwise we will use the bin
    'itself to get the distribution info.  If the info is lacking, we will
    'raise an error stating the bin is invalid.
    
    mbValidAutoDistBin = False
    lRow = grdLine.ActiveRow
    
    'Get values for the distribution method.
    lTranType = glGetValidLong(gsGridReadCell(grdLine, lRow, kColTranType))
    lWhseKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "WhseKey"))
    lItemKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "SubItemKey"))
    If lItemKey = 0 Then
        lItemKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ItemKey"))
    End If
    lUOMKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ShipUnitMeasKey"))
    dQtyToDist = gdGetValidDbl(gsGridReadCell(grdLine, lRow, kColQtyPicked))
    sWhseBinID = gsGetValidStr(gsGridReadCell(grdLine, lRow, kColAutoDistBinID))
    sAutoDistLotNo = gsGetValidStr(gsGridReadCell(grdLine, lRow, kColAutoDistLotNo))
    sBusinessDate = moClass.moSysSession.BusinessDate
    lInvtTranKey = glGetValidLong(gsGridReadCell(grdLine, lRow, kColInvTranKey))
    
    'Check if the bin's value changed OR if the user changed the quantity / UOM
    '(signified by the Me.ActiveControl.Name not being equal the bin lookup control).
    If Len(lkuAutoDistBin.Text) <> 0 And ((Me.ActiveControl.Name <> "lkuAutoDistBin") Or _
       (LCase(Trim(lkuAutoDistBin.Text)) <> LCase(Trim(lkuAutoDistBin.Tag)))) Then

        If mbAutoDistBinClicked = True Then
            If lkuAutoDistBin.ReturnColumnValues.Count <> 0 Then
                bCanAutoDist = True
                Set colAutoDistBinRetCol = lkuAutoDistBin.ReturnColumnValues
                moItemDist.GetAutoDistInfoFromRetCol lItemKey, colAutoDistBinRetCol, sAutoDistBinID, _
                                                    dAutoDistBinQtyAvail, lAutoDistBinUOMKey, _
                                                    sAutoDistLotNo, sAutoDistLotExpDate, lAutoDistInvtLotKey, lAutoDistWhseBinKey
                
                If Not mbMultipleBin Then
                'Check if we can auto-distribute against the bin.
                bCanAutoDist = moItemDist.bIsValidBinForAutoDist(lWhseKey, lItemKey, sWhseBinID, _
                                                 lTranType, dQtyToDist, _
                                                 lUOMKey, sBusinessDate, _
                                                 lInvtTranKey, dAutoDistBinQtyAvail, lAutoDistBinUOMKey, _
                                                 sAutoDistLotNo, sAutoDistLotExpDate, lAutoDistInvtLotKey, lAutoDistWhseBinKey)
                End If
            End If
        Else
            'User likely typed in the bin or was set manually in code.
            sAutoDistBinID = lkuAutoDistBin.Text
                        
            If Not mbMultipleBin Then
            'Check if we can auto-distribute against the bin.
            bCanAutoDist = moItemDist.bIsValidBinForAutoDist(lWhseKey, lItemKey, sAutoDistBinID, _
                                             lTranType, dQtyToDist, _
                                             lUOMKey, sBusinessDate, _
                                             lInvtTranKey, dAutoDistBinQtyAvail, lAutoDistBinUOMKey, _
                                             sAutoDistLotNo, sAutoDistLotExpDate, lAutoDistInvtLotKey, lAutoDistWhseBinKey)
            End If
        End If
        
        'Check if we can do an auto-distribution.
        If bCanAutoDist = False And Cancel = False Then
            mbValidAutoDistBin = False
            lkuAutoDistBin.ClearData
            If lkuAutoDistBin.Enabled = True Then
                lkuAutoDistBin.SetFocus
            End If
            lkuAutoDistBin.Tag = ""
        Else
            mbValidAutoDistBin = True
            gGridUpdateCell grdLine, lRow, kColAutoDistBinKey, CStr(lAutoDistWhseBinKey)
            gGridUpdateCell grdLine, lRow, kColAutoDistBinID, CStr(sAutoDistBinID)
            gGridUpdateCell grdLine, lRow, kColAutoDistBinUOMKey, CStr(lAutoDistBinUOMKey)
            gGridUpdateCell grdLine, lRow, kColAutoDistBinQtyAvail, CStr(dAutoDistBinQtyAvail)
            gGridUpdateCell grdLine, lRow, kColAutoDistLotNo, sAutoDistLotNo
            gGridUpdateCellText grdLine, lRow, kColAutoDistLotExpDate, sAutoDistLotExpDate
            gGridUpdateCell grdLine, lRow, kColAutoDistInvtLotKey, CStr(lAutoDistInvtLotKey)
        
            'Must reset to false in case user decides to type a value after using the lookup.  If it is not reset,
            'it will use the lku's return columns collection which will have the wrong values.
            mbAutoDistBinClicked = False
        End If
    End If
   
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuAutoDistBin_Validate", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub GetAutoDistSavedInfo(lRow As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim lItemKey As Long
    Dim lTranIdentifier As Long
    Dim lInvtTranKey As Long
    Dim sWhseBinID As String
    Dim lWhseBinKey As Long
'    Dim bMultipleBin As Boolean
    Dim sLotNo As String
    Dim sLotExpDate As String
    Dim lInvtLotKey As Long

    'Get values for the distribution method.
    lItemKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "SubItemKey"))
    If lItemKey = 0 Then
        lItemKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ItemKey"))
    End If
    lTranIdentifier = glGetValidLong(gsGridReadCell(grdLine, lRow, kColInvTranID))
    lInvtTranKey = glGetValidLong(gsGridReadCell(grdLine, lRow, kColInvTranKey))
    
    CreateDistForm lRow
    Call moItemDist.sGetBinLkuCtrlSavedDistValues(lItemKey, lTranIdentifier, lInvtTranKey, mbMultipleBin, sWhseBinID, lWhseBinKey, sLotNo, lInvtLotKey, sLotExpDate)
    
    'Lookup control does not allow any value except those that are available
    'for selection.  If we are dealing with multiple bins, bring the multiple
    'bin text box to the front and set the lookup value to an empty string.
    If mbMultipleBin = True Then
        'Bring the multiple bin text box to the front.
        txtMultipleBin.ZOrder 0
        lkuAutoDistBin.SetTextAndKeyValue sWhseBinID, lWhseBinKey
        sWhseBinID = "Multiple Bin"
    Else
        'Bring the bin lookup to the front.
        lkuAutoDistBin.ZOrder 0
        'Set the values back to the bin lookup control.
        lkuAutoDistBin.SetTextAndKeyValue sWhseBinID, lWhseBinKey
        lkuAutoDistBin.Tag = sWhseBinID
    End If
    
    gGridUpdateCell grdLine, lRow, kColAutoDistBinID, sWhseBinID
    gGridUpdateCell grdLine, lRow, kColAutoDistBinKey, CStr(lWhseBinKey)
    gGridUpdateCell grdLine, lRow, kColAutoDistLotNo, sLotNo
    gGridUpdateCell grdLine, lRow, kColAutoDistLotExpDate, sLotExpDate
    gGridUpdateCell grdLine, lRow, kColAutoDistInvtLotKey, CStr(lInvtLotKey)
    gGridUpdateCell grdLine, lRow, kColAutoDistMultipleBin, IIf(mbMultipleBin = True, "1", "0")
        
    If lkuAutoDistBin.Enabled = True Then
        'Set to false so it will not auto distribution again.
        gGridUpdateCell grdLine, lRow, kColAutoDist, "0"
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetAutoDistSavedInfo", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bAutoDistFromBinLku(lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim lTranIdentifier As Long
    Dim lInvtTranKey As Long
    Dim lWhseKey As Long
    Dim lItemKey As Long
    Dim lUOMKey As Long
    Dim lTranType As Long
    Dim dQtyToDist As Double
    Dim dReturnTranQty As Double
    
    Dim bDoAutoDist As Boolean
    Dim sAutoDistWhseBinID As String
    Dim lAutoDistBinUOMKey As Long
    Dim dAutoDistBinQtyAvail As Double
    Dim sAutoDistLotNo As String
    Dim sAutoDistLotExpDate As String
    
    bAutoDistFromBinLku = False
    
    bDoAutoDist = IIf(gsGridReadCell(grdLine, lRow, kColAutoDist) = "1", True, False)

    If bDoAutoDist = True Then
        'Get values for the distribution method.
        lTranIdentifier = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ShipLineKey"))
        lWhseKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "WhseKey"))
        lItemKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "SubItemKey"))
        If lItemKey = 0 Then
            lItemKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ItemKey"))
        End If
        lUOMKey = glGetValidLong(moDMGrid.GetColumnValue(lRow, "ShipUnitMeasKey"))
        dQtyToDist = gdGetValidDbl(gsGridReadCell(grdLine, lRow, kColQtyPicked))
        lTranType = glGetValidLong(gsGridReadCell(grdLine, lRow, kColTranType))
        lInvtTranKey = glGetValidLong(gsGridReadCell(grdLine, lRow, kColInvTranKey))
        
        sAutoDistWhseBinID = gsGetValidStr(gsGridReadCell(grdLine, lRow, kColAutoDistBinID))
        lAutoDistBinUOMKey = glGetValidLong(gsGridReadCell(grdLine, lRow, kColAutoDistBinUOMKey))
        dAutoDistBinQtyAvail = gdGetValidDbl(gsGridReadCell(grdLine, lRow, kColAutoDistBinQtyAvail))
        sAutoDistLotNo = gsGetValidStr(gsGridReadCell(grdLine, lRow, kColAutoDistLotNo))
        sAutoDistLotExpDate = gsGetValidStr(gsGridReadCell(grdLine, lRow, kColAutoDistLotExpDate))
        
        'Attempt to auto distribute the bin selection.
        'If successful, this routine will assign a TranIdentifier.
        If moItemDist.bPerformSilentDistribution(lTranIdentifier, lWhseKey, lItemKey, lTranType, _
                                               lUOMKey, lAutoDistBinUOMKey, dQtyToDist, dAutoDistBinQtyAvail, _
                                               sAutoDistWhseBinID, sAutoDistLotNo, sAutoDistLotExpDate, , lInvtTranKey) Then
                                           
            If lTranIdentifier > 0 Then
                gGridUpdateCell grdLine, lRow, kColInvTranID, CStr(lTranIdentifier)
                'Set to false so we don't auto dist again.
                gGridUpdateCell grdLine, lRow, kColAutoDist, "0"
                
                '.ClearData will clear the data plus the return columns collection.  Must clear the
                'collection in case user decides to type a value after using the lookup.  If it is not
                'cleared, it will use the collection and distribute against the wrong set of data.
                lkuAutoDistBin.ClearData
                lkuAutoDistBin.Text = lkuAutoDistBin.Tag
            
                dReturnTranQty = gdGetValidDbl(moAppDB.Lookup("SUM(TranQty)", "#timInvtDistWrk", "TranIdentifier = " & lTranIdentifier))
                gGridUpdateCell grdLine, lRow, kColSumOfDist, CStr(dReturnTranQty)
                mlInvtTranID = lTranIdentifier
                moDMGrid.SetRowDirty lRow
                moDMGrid.Save
            Else
                Exit Function
            End If
        End If
    End If
    
    bAutoDistFromBinLku = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bAutoDistFromBinLku", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Function CMMultiSelect(ByVal ctl As Object) As Object
'+++ VB/Rig Skip +++
'*******************************************************************************
'   Description: OPTIONAL
'                The form's selection object MUST be declared as PUBLIC.
'                However, array variables in a form can not be declared public.
'                Therefore, for those small handful of applications that have
'                their selection object declared as Private and/or declared
'                as an array, this mechanism will allow the form to pass back
'                the selection object to be worked on.
'
'                Therefore, if applications are having an issue, they must either
'                declare their selection object as public or implement this call back
'                method
'
'   Parameters:
'                Ctl <in> - Control that selected the task
'
'   Returns:
'                A selection object
'*******************************************************************************
On Error Resume Next
    
    ' moSelect is a private control array, therefore ...
    Set CMMultiSelect = moSelect(ctl.Index)
    
    Err.Clear
    
End Function

