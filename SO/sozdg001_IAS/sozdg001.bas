Attribute VB_Name = "basConfirmPicks"
'**********************************************************************
'     Name: basConfirmPicks
'     Desc: Main Module of this Object containing "Sub Main ()".
' Original: 07-27-1998
'     Mods: mm/dd/yy XXX
'**********************************************************************
Option Explicit
Public Const kiOn = 1
Public Const kiOff = 0

Public Const kTM_None                   As Integer = 0
Public Const kTM_Lot                    As Integer = 1
Public Const kTM_Serial                 As Integer = 2
Public Const kTM_Both                   As Integer = 3

Public Const kSubItemUsed               As Boolean = True
Public Const kSubItemNotUsed            As Boolean = False
Public Const kAutoSubstitutionUsed      As Integer = 1
Public Const kManualSubstitutionUsed    As Integer = 0

Public Enum SOOverShipmentPolicy
    kDisAllow = 0
    kAllow = 1
    kAllowWithWarning = 2
End Enum


Const VBRIG_MODULE_ID_STRING = "SOZDG001.BAS"

Public Sub Main()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("sozdg001.clsConfirmPicks")
        StartAppInStandaloneMode oApp, Command$()
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "basCreatePick", "Main", VBRIG_IS_MODULE                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basCreatePick"
End Function

