VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{9504980C-B928-4BF5-A5D0-13E1F649AECB}#35.0#0"; "SOTAVM.ocx"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#84.0#0"; "LookupView.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmsozxxEscltn_Rpt 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Escalation Reporting"
   ClientHeight    =   5805
   ClientLeft      =   3060
   ClientTop       =   2010
   ClientWidth     =   9450
   HelpContextID   =   17370001
   Icon            =   "sozxxEscltn_Rpt.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5805
   ScaleWidth      =   9450
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -10000
      TabIndex        =   3
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -10000
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.ComboBox CustomCombo 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      ForeColor       =   &H80000012&
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   13
      TabStop         =   0   'False
      Text            =   "Combo1"
      Top             =   1530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.CheckBox chkSummary 
      Caption         =   "Print &Report Settings"
      Height          =   285
      Left            =   5160
      TabIndex        =   6
      Top             =   495
      WhatsThisHelpID =   17370003
      Width           =   1845
   End
   Begin VB.ComboBox cboReportSettings 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   690
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   495
      WhatsThisHelpID =   17370002
      Width           =   4380
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      Width           =   9450
      _ExtentX        =   16669
      _ExtentY        =   741
      Style           =   3
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   5415
      Width           =   9450
      _ExtentX        =   16669
      _ExtentY        =   688
      BrowseVisible   =   0   'False
   End
   Begin TabDlg.SSTab tabReport 
      Height          =   4380
      Left            =   45
      TabIndex        =   7
      Top             =   900
      WhatsThisHelpID =   17370004
      Width           =   9375
      _ExtentX        =   16536
      _ExtentY        =   7726
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   10
      TabHeight       =   529
      TabCaption(0)   =   "&Main"
      TabPicture(0)   =   "sozxxEscltn_Rpt.frx":23D2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraSort"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "fraSelect"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "&Options"
      TabPicture(1)   =   "sozxxEscltn_Rpt.frx":23EE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraMessage"
      Tab(1).Control(1)=   "Frame4"
      Tab(1).Control(2)=   "Frame3"
      Tab(1).ControlCount=   3
      Begin VB.Frame fraMessage 
         Caption         =   "Messa&ge"
         Height          =   1695
         Left            =   -74880
         TabIndex        =   32
         Top             =   2160
         Visible         =   0   'False
         Width           =   9105
         Begin VB.PictureBox pctMessage 
            Height          =   1320
            Left            =   135
            ScaleHeight     =   1260
            ScaleWidth      =   8775
            TabIndex        =   33
            TabStop         =   0   'False
            Top             =   270
            Width           =   8835
            Begin VB.TextBox txtMessageHeader 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Height          =   240
               Index           =   0
               Left            =   0
               MaxLength       =   50
               TabIndex        =   38
               Top             =   0
               WhatsThisHelpID =   18
               Width           =   8770
            End
            Begin VB.TextBox txtMessageHeader 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Height          =   240
               Index           =   1
               Left            =   0
               MaxLength       =   50
               TabIndex        =   37
               Top             =   255
               WhatsThisHelpID =   18
               Width           =   8770
            End
            Begin VB.TextBox txtMessageHeader 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Height          =   240
               Index           =   2
               Left            =   0
               MaxLength       =   50
               TabIndex        =   36
               Top             =   510
               WhatsThisHelpID =   18
               Width           =   8770
            End
            Begin VB.TextBox txtMessageHeader 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Height          =   240
               Index           =   3
               Left            =   0
               MaxLength       =   50
               TabIndex        =   35
               Top             =   765
               WhatsThisHelpID =   18
               Width           =   8770
            End
            Begin VB.TextBox txtMessageHeader 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Height          =   240
               Index           =   4
               Left            =   0
               MaxLength       =   50
               TabIndex        =   34
               Top             =   1020
               WhatsThisHelpID =   18
               Width           =   8770
            End
            Begin VB.Line Line1 
               Index           =   3
               X1              =   0
               X2              =   8775
               Y1              =   1010
               Y2              =   1010
            End
            Begin VB.Line Line1 
               Index           =   2
               X1              =   0
               X2              =   8775
               Y1              =   750
               Y2              =   750
            End
            Begin VB.Line Line1 
               Index           =   1
               X1              =   0
               X2              =   8775
               Y1              =   500
               Y2              =   500
            End
            Begin VB.Line Line1 
               Index           =   0
               X1              =   0
               X2              =   8775
               Y1              =   240
               Y2              =   240
            End
         End
      End
      Begin VB.Frame fraSelect 
         Caption         =   "S&elect"
         Height          =   3780
         Left            =   90
         TabIndex        =   16
         Top             =   360
         Width           =   9195
         Begin FPSpreadADO.fpSpread grdSelection 
            Height          =   3480
            Left            =   90
            TabIndex        =   18
            Top             =   255
            WhatsThisHelpID =   17370015
            Width           =   9015
            _Version        =   524288
            _ExtentX        =   15901
            _ExtentY        =   6138
            _StockProps     =   64
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            SpreadDesigner  =   "sozxxEscltn_Rpt.frx":240A
            AppearanceStyle =   0
         End
         Begin NEWSOTALib.SOTACurrency curControl 
            Height          =   285
            Left            =   1530
            TabIndex        =   21
            TabStop         =   0   'False
            Top             =   1035
            WhatsThisHelpID =   17370011
            Width           =   1950
            _Version        =   65536
            _ExtentX        =   3440
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            sBorder         =   1
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTANumber nbrControl 
            Height          =   285
            Left            =   1530
            TabIndex        =   22
            TabStop         =   0   'False
            Top             =   1440
            WhatsThisHelpID =   17370012
            Width           =   1950
            _Version        =   65536
            _ExtentX        =   3440
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            sBorder         =   1
            mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTAMaskedEdit mskControl 
            Height          =   240
            Left            =   1530
            TabIndex        =   19
            TabStop         =   0   'False
            Top             =   720
            WhatsThisHelpID =   17370013
            Width           =   1950
            _Version        =   65536
            _ExtentX        =   3440
            _ExtentY        =   423
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.24
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            sBorder         =   1
         End
         Begin LookupViewControl.LookupView luvControl 
            Height          =   285
            Index           =   0
            Left            =   3555
            TabIndex        =   20
            TabStop         =   0   'False
            Top             =   720
            WhatsThisHelpID =   17370014
            Width           =   285
            _ExtentX        =   503
            _ExtentY        =   503
            LookupMode      =   1
         End
      End
      Begin VB.Frame Frame4 
         Height          =   1095
         Left            =   -71400
         TabIndex        =   28
         Top             =   600
         Width           =   2175
         Begin VB.OptionButton optSummary 
            Caption         =   "Summary Report"
            Height          =   255
            Left            =   120
            TabIndex        =   30
            Top             =   600
            Width           =   1815
         End
         Begin VB.OptionButton optDetail 
            Caption         =   "Detailed Report"
            Height          =   255
            Left            =   120
            TabIndex        =   29
            Top             =   240
            Value           =   -1  'True
            Width           =   1815
         End
      End
      Begin VB.Frame Frame3 
         Height          =   1455
         Left            =   -74520
         TabIndex        =   25
         Top             =   600
         Width           =   2535
         Begin VB.OptionButton optRemaining 
            Caption         =   "Remaining Tons"
            Height          =   255
            Left            =   120
            TabIndex        =   31
            ToolTipText     =   "Only Shipments/Returns tied to a contract (BSO)"
            Top             =   960
            Width           =   2055
         End
         Begin VB.OptionButton optShipped 
            Caption         =   "Shipped Tons"
            Height          =   255
            Left            =   120
            TabIndex        =   27
            ToolTipText     =   "Only Shipments/Returns tied to a contract (BSO)"
            Top             =   600
            Width           =   2055
         End
         Begin VB.OptionButton optPlanned 
            Caption         =   "Planned Tons"
            Height          =   255
            Left            =   120
            TabIndex        =   26
            ToolTipText     =   "Only Contracts (BSO) planned tons."
            Top             =   240
            Value           =   -1  'True
            Width           =   2055
         End
      End
      Begin VB.Frame fraSort 
         Caption         =   "&Sort"
         Height          =   1905
         Left            =   90
         TabIndex        =   9
         Top             =   480
         Visible         =   0   'False
         Width           =   9195
         Begin FPSpreadADO.fpSpread grdSort 
            Height          =   1545
            Left            =   90
            TabIndex        =   11
            Top             =   270
            WhatsThisHelpID =   17370010
            Width           =   9015
            _Version        =   524288
            _ExtentX        =   15901
            _ExtentY        =   2725
            _StockProps     =   64
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            SpreadDesigner  =   "sozxxEscltn_Rpt.frx":2818
            AppearanceStyle =   0
         End
      End
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   4
      Top             =   4320
      Visible         =   0   'False
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   2880
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   615
      Index           =   0
      Left            =   -10000
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   720
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1085
      _StockProps     =   0
   End
   Begin MSComDlg.CommonDialog dlgCreateRPTFile 
      Left            =   10620
      Top             =   1770
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComDlg.CommonDialog dlgPrintSetup 
      Left            =   9930
      Top             =   1770
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin SOTAVM.SOTAValidationMgr valMgr 
      Left            =   -10000
      Top             =   180
      _ExtentX        =   741
      _ExtentY        =   661
      CtlsCount       =   0
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   17
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Label lblSetting 
      AutoSize        =   -1  'True
      Caption         =   "Se&tting"
      Height          =   195
      Left            =   135
      TabIndex        =   1
      Top             =   540
      Width           =   495
   End
End
Attribute VB_Name = "frmsozxxEscltn_Rpt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mlRunMode               As Long

Private mbSaved                 As Boolean

Private mbCancelShutDown        As Boolean

Private mbLoading               As Boolean

Private mbPeriodEnd             As Boolean

Private mbLoadSuccess           As Boolean

Private mbEnterAsTab            As Boolean

Private miSecurityLevel         As Integer

Private moClass                 As Object

Private moContextMenu           As clsContextMenu

Public moSotaObjects            As New Collection

Public msCompanyID              As String

Public msUserID                 As String

Public mlLanguage               As Long

Public moReport                 As clsReportEngine

Public moSettings               As clsSettings

Public moSort                   As clsSort

Public moSelect                 As clsSelection

Public moDDData                 As clsDDData

Public moOptions                As clsOptions

Public moPrinter                As Printer

Public sRealTableCollection     As Collection

Public sWorkTableCollection     As Collection

Const VBRIG_MODULE_ID_STRING = "sozxxEscltn_Rpt.FRM"

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If


'SGS DEJ
Function GetContractYears(ByRef sStaticDBVals() As String, ByRef sStaticVals() As String) As Boolean
    On Error GoTo Error
    Dim icnt As Long
    Dim RS As DASRecordSet
    Dim sSQL As String
    
    icnt = glGetValidLong(moClass.moAppDB.Lookup("count(distinct ContractYear)", "tsoSalesOrderExt_SGS", ""))
    
    ReDim sStaticDBVals(icnt) As String
    ReDim sStaticVals(icnt) As String
    
    'RKL DEJ 1/20/14 added Sort by.  Requested by Kevin from user group meeting
    sSQL = "select distinct ContractYear From tsoSalesOrderExt_SGS Where isNull(ContractYear,'') <> '' Order By ContractYear"
    'Open Recordset
    Set RS = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If Not RS.IsEOF Then
        icnt = 1
        While Not RS.IsBOF And Not RS.IsEOF And icnt <= RS.RecordCount
            sStaticDBVals(icnt) = gsGetValidStr(RS.Field("ContractYear"))
            sStaticVals(icnt) = gsGetValidStr(RS.Field("ContractYear"))
'Debug.Print gsGetValidStr(RS.Field("ContractYear"))
            icnt = icnt + 1
            RS.MoveNext
        Wend
    End If
    
    If Not RS Is Nothing Then
        Set RS = Nothing
    End If
    
    GetContractYears = True
    Exit Function
Error:
    MsgBox "The following error occurred while loading possible Contract Years:" & vbCrLf & Err.Description, vbExclamation, "MAS 500"
End Function

'********************** Selection Stuff ****************************
Private Sub grdSelection_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
    If Not mbLoading Then moSelect.EditMode Col, Row, Mode, ChangeMade
End Sub

Private Sub grdSelection_GotFocus()
    If Not mbLoading Then moSelect.SelectionGridGotFocus
End Sub

Private Sub grdSelection_KeyDown(KeyCode As Integer, Shift As Integer)
    If Not mbLoading Then moSelect.SelectionGridKeyDown KeyCode, Shift
End Sub

Private Sub grdSelection_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
    If Not mbLoading Then moSelect.SelectionGridLeaveCell Col, Row, NewCol, NewRow, Cancel
End Sub

Private Sub grdSelection_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
    If Not mbLoading Then moSelect.SelectionGridTopLeftChange OldLeft, OldTop, NewLeft, NewTop
End Sub

Private Sub mskControl_LostFocus()
    If Not mbLoading Then moSelect.mskControlLostFocus
End Sub

Private Sub nbrControl_LostFocus()
    If Not mbLoading Then moSelect.nbrControlLostFocus
End Sub

Private Sub curControl_LostFocus()
    If Not mbLoading Then moSelect.curControlLostFocus
End Sub

Private Sub luvControl_GotFocus(Index As Integer)
    If Not mbLoading Then moSelect.LookupControlClick Index, ""
End Sub
'******************** End Selection Stuff ****************************


'    ************************ Sort Stuff
'    ****************
'***************
Private Sub grdSort_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
    If Not mbLoading Then moSort.GridLeaveCell Col, Row, NewCol, NewRow
End Sub

Private Sub grdSort_KeyUp(KeyCode As Integer, Shift As Integer)
    If Not mbLoading Then moSort.GridKey KeyCode, Shift
End Sub

Private Sub grdSort_Change(ByVal Col As Long, ByVal Row As Long)
    If Not mbLoading Then moSort.GridChange Col, Row
End Sub

Private Sub grdSort_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
    If Not mbLoading Then moSort.GridButtonClicked Col, Row, ButtonDown
End Sub

Private Sub grdSort_GotFocus()
    If Not mbLoading Then moSort.SortGridGotFocus
End Sub

Private Sub grdSort_KeyDown(KeyCode As Integer, Shift As Integer)
    If Not mbLoading Then moSort.SortGridKeyDown KeyCode, Shift
End Sub
'*********************** End Sort Stuff *****************************

Public Sub HandleToolbarClick(sKey As String, Optional iFileType As Variant, Optional sFileName As Variant)

    If Me.Visible Then
        Me.SetFocus '   VB5
    End If

#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If
   
    Select Case sKey
        Case kTbProceed, kTbFinish, kTbSave
            moSettings.ReportSettingsSaveAs
        
        Case kTbCancel, kTbDelete
            moSettings.ReportSettingsDelete
        
        Case kTbDefer, kTbPreview, kTbPrint
            gbSkipPrintDialog = False                         'show print dialog one time
            Select Case True
                Case optRemaining.Value
                    Select Case True
                        Case optDetail.Value
                            lStartReport sKey, Me, False, "", "", "", iFileType, sFileName, RemainingTons, Detailed
                        Case optSummary.Value
                            lStartReport sKey, Me, False, "", "", "", iFileType, sFileName, RemainingTons, Summary
                    End Select
                Case optShipped.Value
                    Select Case True
                        Case optDetail.Value
                            lStartReport sKey, Me, False, "", "", "", iFileType, sFileName, ShippedTons, Detailed
                        Case optSummary.Value
                            lStartReport sKey, Me, False, "", "", "", iFileType, sFileName, ShippedTons, Summary
                    End Select
                Case optPlanned.Value
                    Select Case True
                        Case optDetail.Value
                            lStartReport sKey, Me, False, "", "", "", iFileType, sFileName, PlannedTons, Detailed
                        Case optSummary.Value
                            lStartReport sKey, Me, False, "", "", "", iFileType, sFileName, PlannedTons, Summary
                    End Select
            End Select
            
                
'            lStartReport sKey, Me, False, "", "", "", iFileType, sFileName, optStandard.Value, optPlanned.Value
            
        Case kTbPrintSetup
            dlgPrintSetup.ShowPrinter
        
        Case kTbHelp
            gDisplayFormLevelHelp Me
    End Select
End Sub

Public Function lInitializeReport(ModuleID As String, DefaultReport As String, ProjectName As String) As Long
    Dim lRow As Long                'SGS DEJ Added
    Dim sStaticDBVals() As String  'SGS DEJ Added
    Dim sStaticVals() As String    'SGS DEJ Added
    
    lInitializeReport = -1
    
    Set moPrinter = Printer
    Set moSotaObjects = New Collection
    Set moReport = New clsReportEngine
    Set moSettings = New clsSettings
    Set moSort = New clsSort
    Set moSelect = New clsSelection
    Set moDDData = New clsDDData
    Set moOptions = New clsOptions
    
        
    If mbPeriodEnd Then
        moSelect.UI = False
        moReport.UI = False
    End If
    
    If Not moDDData.lInitDDData(sRealTableCollection, moClass.moAppDB, moClass.moAppDB, msCompanyID) = kSuccess Then
        lInitializeReport = kmsgCantLoadDDData
        Exit Function
    End If
    
    If Not moDDData.lDDBuildData(App.EXEName) = kSuccess Then
        lInitializeReport = kmsgCantLoadDDData
        Exit Function
    End If
    
    moReport.AppOrSysDB = kAppDB
    
    #If RPTDEBUG = 1 Then  'Do not use temp tables
        If (moReport.lInitReport(ModuleID, DefaultReport, Me, moDDData, False) = kFailure) Then
            lInitializeReport = kmsgFatalReportInit
    #Else                             ' Do use temp tables
        If (moReport.lInitReport(ModuleID, DefaultReport, Me, moDDData, True) = kFailure) Then
            lInitializeReport = kmsgFatalReportInit
    #End If
        Exit Function
    End If
   
    'CUSTOMIZE: set Groups and Sorts as required
    'note there are two optional parameters: iNumGroups,iNumSorts for lInitSort
    If (moSort.lInitSort(Me, moDDData, 4, 10) = kFailure) Then
        lInitializeReport = kmsgFataSortInit
        Exit Function
    End If
    
    '==========================================================================================
    'SGS DEJ    ===============     START       ===============================================
    '==========================================================================================
'    Debug.Print moSort.lInsSort("WhseID", "tsoEscalationRptWrk_SGS", moDDData, "Origin")
'    Debug.Print moSort.lInsSort("ContractNumber", "tsoEscalationRptWrk_SGS", moDDData, "Contract")
'    Debug.Print moSort.lInsSort("ItemID", "tsoEscalationRptWrk_SGS", moDDData, "Product")
'    Debug.Print moSort.lInsSort("CustName", "tsoEscalationRptWrk_SGS", moDDData, "Customer")
'
'    Debug.Print moSort.lInsSort("EscalationIndexID", "tsoEscalationRptWrk_SGS", moDDData, "Clause")
'    Debug.Print moSort.lInsSort("IndexDate", "tsoEscalationRptWrk_SGS", moDDData, "Index Date")
'    Debug.Print moSort.lInsSort("CalculationDate", "tsoEscalationRptWrk_SGS", moDDData, "Calculation Date")
    '==========================================================================================
    'SGS DEJ    ===============     END       ===============================================
    '==========================================================================================
    
    With moSort

    End With
    
    '==========================================================================================
    'SGS DEJ    ===============     START       ===============================================
    '==========================================================================================
'    If moSelect.bSetRowInitialValues(lrow, kOperatorEqual, True, "Hard Code Value") = False Then
'        'An error occurred.
'    End If
'
'    If moSelect.bSetTypeCellDropdown(lrow, SelEng_Operators.kOperatorAll) = False Then
'        'An error occurred.
'    End If
'
'    If moSelect.lAddSelectionRow() = -1 Then
'        'An error occurred.
'        'This returns the row number that was added for this filter.
'    End If

    'Warehouse/Origin Lookup: START
    If moSelect.lAddSelRowAllParams("SGSSwapOutForRealTableOrView", "WhseID", Empty, "Origin/Warehouse", "Origin", 6, SQL_CHAR, 0, False, Empty, "Warehouse", "", False, False, Empty, Empty, Empty, , "WhseID") = -1 Then
        'An error occurred.
        'This returns the row number that was added for this filter.
    End If
    'Warehouse/Origin Lookup: END

    
    'Blanket Sales Order/Contract Lookup: START
    If moSelect.lAddSelRowAllParams("SGSSwapOutForRealTableOrView", "TranNo", Empty, "Contract/Blanket SO", "Contract", 10, SQL_CHAR, 0, False, Empty, "SalesOrderWithCustomer", "TranType = 802 and Status In(1,4,5)", False, False, Empty, Empty, Empty, , "TranNo") = -1 Then
        'An error occurred.
        'This returns the row number that was added for this filter.
    End If
    'Blanket Sales Order/Contract Lookup: END
    
    'Sales Order Lookup: START
    If moSelect.lAddSelRowAllParams("SGSSwapOutForRealTableOrView", "TranNo", Empty, "Sales Order", "Contract", 10, SQL_CHAR, 0, False, Empty, "SalesOrderWithCustomer", "TranType = 801 and Status In(1,4,5)", False, False, Empty, Empty, Empty, , "TranNo") = -1 Then
        'An error occurred.
        'This returns the row number that was added for this filter.
    End If
    'Sales Order Lookup: END
    
    'Product Lookup: START
    If moSelect.lAddSelRowAllParams("SGSSwapOutForRealTableOrView", "ItemID", Empty, "Product", "Product", 30, SQL_CHAR, 0, False, Empty, "Item", "", False, False, Empty, Empty, Empty, , "ItemID") = -1 Then
        'An error occurred.
        'This returns the row number that was added for this filter.
    End If
    'Product Lookup: END
    
    'Product Class Lookup: START
    If moSelect.lAddSelRowAllParams("SGSSwapOutForRealTableOrView", "ItemClassID", Empty, "Product Group", "Product Group", 12, SQL_CHAR, 0, False, Empty, "ItemClass", "CompanyID = '" & msCompanyID & "'", False, False, Empty, Empty, Empty, , "ItemClassID") = -1 Then
        'An error occurred.
        'This returns the row number that was added for this filter.
    End If
    
    'Has Price Adjustment Clause (Escalation Index): START
    ReDim sStaticDBVals(2) As String
    ReDim sStaticVals(2) As String
    sStaticDBVals(1) = "1"
    sStaticVals(1) = "YES"
    sStaticDBVals(2) = "0"
    sStaticVals(2) = "NO"

    lRow = moSelect.lAddSelRowStaticList("SGSSwapOutForRealTableOrView", "PriceAdjustmentClause", "Has Price Adjustment Clause", "Price Adjustment Clause", False, Empty, sStaticDBVals, sStaticVals, , UseDBValue)
    If lRow = -1 Then
        'An error occurred.
    End If
    'Has Price Adjustment Clause (Escalation Index): END
    
    'Customer Lookup: START
    If moSelect.lAddSelRowAllParams("SGSSwapOutForRealTableOrView", "CustID", Empty, "Customer", "Customer", 12, SQL_CHAR, 0, False, Empty, "Customer", "", False, False, Empty, Empty, Empty, , "CustID") = -1 Then
        'An error occurred.
        'This returns the row number that was added for this filter.
    End If
    'Customer Lookup: END
    
    'Escalation Index ID Lookup: START
    If moSelect.lAddSelRowAllParams("SGSSwapOutForRealTableOrView", "EscalationIndexID", Empty, "Clause", "Clause", 12, SQL_CHAR, 0, False, Empty, "EscalationIndex", "", False, False, Empty, Empty, Empty, , "EscalationIndexID") = -1 Then
        'An error occurred.
        'This returns the row number that was added for this filter.
    End If
    'Escalation Index ID Lookup: END
    
    'Escalation Index ID Lookup: START
    If moSelect.lAddSelectionRow("SGSSwapOutForRealTableOrView", "CalculationDate", "Calc Date", 0, SQL_DATE, 0, False, Empty, Empty, Empty, Empty) = -1 Then
        'An error occurred.
        'This returns the row number that was added for this filter.
    End If
    'Escalation Index ID Lookup: END
    
    'Escalation Index ID Lookup: START
    If moSelect.lAddSelectionRow("SGSSwapOutForRealTableOrView", "IndexDate", "Index Date", 0, SQL_DATE, 0, False, Empty, Empty, Empty, Empty) = -1 Then
        'An error occurred.
        'This returns the row number that was added for this filter.
    End If
    'Escalation Index ID Lookup: END
        
'    '7/28/08 DEJ added Contract Type filter (free text) (START)
'    lRow = moSelect.lAddSelectionRow("SGSSwapOutForRealTableOrView", "ContractType", "Contract Type", 20, SQL_VARCHAR, 0, False, Empty, Empty, Empty)
'    '7/28/08 DEJ added Contract Type filter (free text) (START)
    
    'Status filter (START)
    ReDim sStaticDBVals(7) As String
    ReDim sStaticVals(7) As String
    sStaticDBVals(1) = "0"
    sStaticVals(1) = "Unacknowledged"
    sStaticDBVals(2) = "1"
    sStaticVals(2) = "Open"
    sStaticDBVals(3) = "2"
    sStaticVals(3) = "Inactive"
    sStaticDBVals(4) = "3"
    sStaticVals(4) = "Canceled"
    sStaticDBVals(5) = "4"
    sStaticVals(5) = "Closed"
    sStaticDBVals(6) = "5"
    sStaticVals(6) = "Incomplete"
    sStaticDBVals(7) = "6"
    sStaticVals(7) = "Pending Approval"

    lRow = moSelect.lAddSelRowStaticList("SGSSwapOutForRealTableOrView", "Status", "Status", "Status", False, Empty, sStaticDBVals, sStaticVals, , UseDBValue)
    If lRow = -1 Then
        'An error occurred.
    End If
    'Status filter (START)
    
    'Contract Year: START
    If Not GetContractYears(sStaticDBVals, sStaticVals) Then
        'Error
    End If
    
    If UBound(sStaticDBVals) <= 0 Then
        ReDim sStaticDBVals(1) As String
        sStaticDBVals(1) = IIf(IsNumeric(CStr(Year(Date))) = True, CStr(Year(Date)), "0")
    End If
    
    If UBound(sStaticVals) <= 0 Then
        ReDim sStaticVals(1) As String
        sStaticVals(1) = CStr(Year(Date))
    End If
        
'    lRow = moSelect.lAddSelRowStaticList("SGSSwapOutForRealTableOrView", "ContractYear", "Contract Year", "Contract Year", False, Empty, sStaticDBVals, sStaticVals, , UseText)
    lRow = moSelect.lAddSelRowStaticList("SGSSwapOutForRealTableOrView", "ContractYearInt", "Contract Year", "Contract Year", False, Empty, sStaticDBVals, sStaticVals, , UseDBValue)
    If lRow = -1 Then
        'An error occurred.
    End If
    'Contract Year: END
    
'    'SGS DEJ Set table name to "" and set moDDData to Nothing
'    If Not moSelect.bInitSelect(sRealTableCollection(1), mskControl, luvControl, nbrControl, _
'            curControl, grdSelection, fraSelect, Me, moDDData, _
'            19 * 120 + 60, 17 * 120, 18 * 120, 18 * 120, True) Then
    If Not moSelect.bInitSelect("", mskControl, luvControl, nbrControl, _
            curControl, grdSelection, fraSelect, Me, Nothing, _
            19 * 120 + 60, 17 * 120, 18 * 120, 18 * 120, True) Then
        lInitializeReport = kmsgSetupSelectionGridFail
        Exit Function
    End If
    
    
    'Has Escalation Setteled: START
    ReDim sStaticDBVals(2) As String
    ReDim sStaticVals(2) As String
    sStaticDBVals(1) = "1"
    sStaticVals(1) = "YES"
    sStaticDBVals(2) = "0"
    sStaticVals(2) = "NO"

    lRow = moSelect.lAddSelRowStaticList("SGSSwapOutForRealTableOrView", "Settled", "Has Escalation Setteled", "Has Escalation Setteled", False, Empty, sStaticDBVals, sStaticVals, , UseDBValue)
    If lRow = -1 Then
        'An error occurred.
    End If
    'Has Escalation Setteled: STOP
    
    '==========================================================================================
    'SGS DEJ    ===============     End        ===============================================
    '==========================================================================================
    
    '   Add GL Account Segments to sort and/or selection grid. Examples:
    '   Dim oAppDB as DASDatabase
    '   Set oAppDB = moClass.moAppDB
    '   moSort.lAddGLAcctSeg oAppDB, "Segment1", "Segment2", "Segment3", "Segment4", "Segment5"
    '   moSelect.lAddGLAcctSeg oAppDB, "GLAcctNo", "Segment1", "Segment2", "Segment3", "Segment4", "Segment5"
    
    With moSelect

    End With
    
    moSelect.PopulateSelectionGrid
    
    lInitializeReport = 0
End Function


Private Sub tabReport_Click(PreviousTab As Integer)
Dim bEnabled As Boolean

    If tabReport.Tab = PreviousTab Then Exit Sub
    'POSSIBLE CUSTOMIZE: make sure form is saved with Main tab in front
    'this code is used to ensure tab order works correctly
    bEnabled = (tabReport.Tab = 1)

    fraSort.Enabled = Not bEnabled
    fraSelect.Enabled = Not bEnabled

End Sub

Private Sub tbrMain_ButtonClick(Button As String)
    HandleToolbarClick Button
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function

Private Sub cboReportSettings_Click()
If Not moSettings.gbSkipClick Then
    moSettings.bLoadReportSettings
End If
End Sub

Private Sub cboReportSettings_KeyPress(KeyAscii As Integer)
    If Len(cboReportSettings.Text) > 40 Then
        Beep
        KeyAscii = 0
    End If
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Skip +++
    mbCancelShutDown = False
    mbLoadSuccess = False
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
Dim iLuvControl As Integer
#If CUSTOMIZER Then
    If (Shift = 4) Then
        gProcessFKeys Me, KeyCode, Shift
    End If
#End If

    If Shift = 0 Then
        Select Case KeyCode
            Case vbKeyF1
                gDisplayWhatsThisHelp Me, Me.ActiveControl
            Case vbKeyF5
                iLuvControl = moSelect.SelectionGridF5KeyDown(Me.ActiveControl)
                If iLuvControl > 0 Then
                    luvControl_GotFocus iLuvControl
                End If
        End Select
    End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Skip +++
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
              gProcessSendKeys "{TAB}"
               KeyAscii = 0
            End If
        
        Case Else
            'Other KeyAscii routines.
    End Select
End Sub

Private Sub Form_Load()
    Dim lRetval As Long             'Captures Long function return value(s).
    Dim iRetVal As Integer          'Captures Integer function return value(s).
    Dim sRptProgramName As String   'Stores base filename for identification purposes.
    Dim sModule As String           'The report's module: AR, AP, SM, etc.
    Dim sDefaultReport As String    'The default .RPT file name
    
    mbLoading = True
    mbLoadSuccess = False
           
    'Assign system object properties to module-level variables for easier access
    With moClass.moSysSession
        msCompanyID = .CompanyId
        msUserID = .UserId
        mbEnterAsTab = .EnterAsTab
        mlLanguage = .Language
    End With
    
    'CUSTOMIZE: assign defaults for this project
    sRptProgramName = "sozxxEscltn_Rpt" 'such as rpt001
    sModule = "SO" '2 letter Module name
    sDefaultReport = "EscalationDetail.rpt"
    
    'Set local flag to value of class module's bPeriodEnd flag.
    mbPeriodEnd = moClass.bPeriodEnd
    
    Set sRealTableCollection = New Collection
    'CUSTOMIZE:  Invoke Add method of sRealTableCollection for each table which
    'will provide raw data for the report.  Primary table should appear first.
    With sRealTableCollection
        'SGS DEJ Note:
        'A valid table from the data dictionary must be added to
        'this collection if you want to do sorting and grouping and sub totals
        
        .Add "tsoEscalationRpt_Wrk_SGS"

    End With
    
    Set sWorkTableCollection = New Collection
    'CUSTOMIZE:  Invoke Add method of sWorkTableCollection for each table which
    'will serve as a work table for the report.
    With sWorkTableCollection
        .Add "tsoEscalationRptWrk_SGS"
    End With
    
    lRetval = lInitializeReport(sModule, sDefaultReport, sRptProgramName)
    If lRetval <> 0 Then
        moReport.ReportError lRetval
        GoTo badexit
    End If
    
    'Perform initialization of form and control properties only if you need to display UI.
    If Not mbPeriodEnd Then
        'Set form dimensions.
        Me.Height = 5835 'max height: 7200
        Me.Width = 9600  'max width: 9600
        
        'Initialize toolbar.
        tbrMain.Init sotaTB_REPORT, mlLanguage
        With moClass.moFramework
            tbrMain.SecurityLevel = .GetTaskPermission(.GetTaskID())
        End With
                    
        '   Initialize status bar.
        Set sbrMain.Framework = moClass.moFramework
        sbrMain.Status = SOTA_SB_START
        
        'Set form caption using localized text.
        'CUSTOMIZE:  Pass in appropriate string constant as first argument.
        Me.Caption = "Escalation Reporting"
        
        'Initialize context menu.
        'context-sensitive help.
        Set moContextMenu = New clsContextMenu
        With moContextMenu
            .Bind "*SELECTION", grdSelection.hwnd
            Set .Form = frmsozxxEscltn_Rpt
            .Init
        End With
    End If
    
    'CUSTOMIZE:  Add all controls on Options page for which you will want to save settings
    'to the moOptions collection, using its Add method.
    With moOptions
        .Add chkSummary
    
    End With
    
    'Initialize the report settings combo box.
'    moSettings.Initialize Me, cboReportSettings, moSort, moSelect, moOptions, , tbrMain, sbrMain
    moSettings.Initialize Me, cboReportSettings, , moSelect, moOptions, , tbrMain, sbrMain
    
    'SGS DEJ Remove Selection options
    moSelect.lMultiSelectDeleteAll
    
    'If the report is being invoked from the Period End Processing task, attempt to load the
    'setting named "Period End".  If such setting does not exist, or if not being invoked from
    'the Period End Processing task, load last used setting.
    If mbPeriodEnd Then
        moSettings.lLoadPeriodEndSettings
    End If
        
    'Update status flags to indicate successful load has completed.
    mbLoadSuccess = True
    mbLoading = False

    Exit Sub
    
badexit:

End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
    Dim iRet As Integer

    mbCancelShutDown = False
    
    moReport.CleanupWorkTables
    
    If Not mbPeriodEnd Then
        moSettings.SaveLastSettingKey
    End If
    
    If moClass.mlError = 0 Then
        'if a report preview window is active, query user to continue closing.
        If Not moReport.bShutdownEngine Then
            GoTo CancelShutDown
        End If
        
        If Not mbPeriodEnd And gbActiveChildForms(Me) Then
            GoTo CancelShutDown
        End If
    End If
    
    If UnloadMode <> vbFormCode Then
        moClass.miShutDownRequester = kUnloadSelfShutDown
    End If
    
    PerformCleanShutDown

    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
            
        Case Else 'kFrameworkShutDown
        'Do Nothing
        
    End Select
    
    Exit Sub
    
CancelShutDown:
    Cancel = True
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
End Sub

Public Sub PerformCleanShutDown()

#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    gUnloadChildForms Me
    giCollectionDel moClass.moFramework, moSotaObjects, -1

    UnloadObjects

    TerminateControls Me

End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Skip +++

    Set moClass = Nothing
End Sub

Private Sub UnloadObjects()
'+++ VB/Rig Skip +++
    Set moSotaObjects = Nothing
    Set moReport = Nothing
    Set moSettings = Nothing
    Set moSort = Nothing
    Set moContextMenu = Nothing
    Set moSelect = Nothing
    Set moDDData = Nothing
    Set moOptions = Nothing
    Set moPrinter = Nothing
    Set sRealTableCollection = Nothing
    Set sWorkTableCollection = Nothing
End Sub

Private Sub Form_Activate()

#If CUSTOMIZER Then
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyFormCust
        End If
    End If
#End If

End Sub

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Skip +++
    WhatHelpPrefix = moReport.ModuleID() & "Z"
End Property

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Skip +++
    FormHelpPrefix = moReport.ModuleID() & "Z"
End Property

Public Property Get oClass() As Object
'+++ VB/Rig Skip +++
    Set oClass = moClass
End Property
Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Skip +++
    Set moClass = oNewClass
End Property

Public Property Get oreport() As clsReportEngine
'+++ VB/Rig Skip +++
    Set oreport = moReport
End Property
Public Property Set oreport(oClass As clsReportEngine)
'+++ VB/Rig Skip +++
    Set moReport = oClass
End Property

Public Property Get lRunMode() As Long
'+++ VB/Rig Skip +++
    lRunMode = mlRunMode
End Property
Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Skip +++
    mlRunMode = lNewRunMode
End Property

Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Skip +++
    bLoadSuccess = mbLoadSuccess
End Property

Public Property Let bLoadSuccess(bNewLoadSuccess As Boolean)
'+++ VB/Rig Skip +++
    mbLoadSuccess = bNewLoadSuccess
End Property

Public Property Get bCancelShutDown() As Boolean
'+++ VB/Rig Skip +++
  bCancelShutDown = mbCancelShutDown
End Property
Public Property Let bCancelShutDown(bCancel As Boolean)
'+++ VB/Rig Skip +++
    mbCancelShutDown = bCancel
End Property
'*** END PROPERTIES ***

Public Property Get MyApp() As Object
    Set MyApp = App
End Property

Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property

Public Function CMMultiSelect(ByVal Ctl As Object) As Object
'+++ VB/Rig Skip +++
'*******************************************************************************
'   Description: OPTIONAL
'                The form's selection object MUST be declared as PUBLIC.
'                However, array variables in a form can not be declared public.
'                Therefore, for those small handful of applications that have
'                their selection object declared as Private and/or declared
'                as an array, this mechanism will allow the form to pass back
'                the selection object to be worked on.
'
'                Therefore, if applications are having an issue, they must either
'                declare their selection object as public or implement this call back
'                method
'
'   Parameters:
'                Ctl <in> - Control that selected the task
'
'   Returns:
'                A selection object
'*******************************************************************************
On Error Resume Next
    
    ' moSelect is a private control array, therefore ...
    Set CMMultiSelect = moSelect(Ctl.Index)
    
    Err.Clear
    
End Function


Private Sub CustomButton_Click(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
#End If
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
#End If
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
#End If
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
#End If
End Sub

Private Sub CustomOption_Click(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
#End If
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
#End If
End Sub

Private Sub picdrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseDown Index, Button, Shift, x, y
    End If
#End If
End Sub

Private Sub picdrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseMove Index, Button, Shift, x, y
    End If
#End If
End Sub

Private Sub picdrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseUp Index, Button, Shift, x, y
    End If
#End If
End Sub

Private Sub picdrag_Paint(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_Paint Index
    End If
#End If
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
#End If
End Sub

Private Sub CustomCheck_Click(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
#End If
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
#End If
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
#End If
End Sub

Private Sub CustomCombo_Change(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
#End If
End Sub

Private Sub CustomCombo_Click(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
#End If
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
#End If
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
#End If
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
#End If
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
#End If
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
#End If
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
#End If
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
#End If
End Sub

Private Sub CustomDate_Change(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
#End If
End Sub

Private Sub CustomDate_Click(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
#End If
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
#End If
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
#End If
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
#End If
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
#End If
End Sub

Private Sub CustomFrame_Click(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
#End If
End Sub

Private Sub CustomLabel_Click(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
#End If
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
#End If
End Sub

Private Sub CustomMask_Change(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
#End If
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
#End If
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
#End If
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
#End If
End Sub

Private Sub CustomNumber_Change(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
#End If
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
#End If
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
#End If
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
#End If
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
#End If
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
#End If
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
#End If
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
#End If
End Sub






