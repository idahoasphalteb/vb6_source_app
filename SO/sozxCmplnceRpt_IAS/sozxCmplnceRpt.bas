Attribute VB_Name = "basComplianceReport"
Option Explicit

Const VBRIG_MODULE_ID_STRING = "sozxCmplnceRpt.BAS"

Public gbSkipPrintDialog As Boolean     '   Show print dialog



Function GetPeriodEndingDesc(lYear As Long, iPeriod As Long, DBObj As Object) As String
    On Error GoTo Error
    
'    Select Case iPeriod
'       Case 1
'          GetPeriodEndingDesc = "For the First Months Ended January 31, " & lYear
'       Case 2
'          GetPeriodEndingDesc = "For the Second Months Ended February 28, " & lYear
'       Case 3
'          GetPeriodEndingDesc = "For the Third Months Ended March 31, " & lYear
'       Case 4
'          GetPeriodEndingDesc = "For the Forth Months Ended April 30, " & lYear
'       Case 5
'          GetPeriodEndingDesc = "For the Fifth Months Ended May 31, " & lYear
'       Case 6
'          GetPeriodEndingDesc = "For the Sixth Months Ended June 30, " & lYear
'       Case 7
'          GetPeriodEndingDesc = "For the Seventh Months Ended July 31, " & lYear
'       Case 8
'          GetPeriodEndingDesc = "For the Eighth Months Ended August 31, " & lYear
'       Case 9
'          GetPeriodEndingDesc = "For the Ninth Months Ended September 30, " & lYear
'       Case 10
'          GetPeriodEndingDesc = "For the Tenth Months Ended October 31, " & lYear
'       Case 11
'          GetPeriodEndingDesc = "For the Eleventh Months Ended November 30, " & lYear
'       Case 12
'          GetPeriodEndingDesc = "For the Twelfth Months Ended December 31, " & lYear
'    End Select
    
    Dim sDesc As String
    Dim lRetval As Long

    'CUSTOMIZE:  Set input and output parameters as needed and execute appropriate
    'custom stored procedure to populate remaining fields in primary work table and
    'insert records into secondary work tables.
    With DBObj
        .SetInParam (lYear)   'Fisc Year
        .SetInParam (iPeriod)   'Fisc Period (Month)
        .SetOutParam sDesc
        .SetOutParam lRetval
        
        On Error Resume Next
        .ExecuteSP ("spGLComplianceRptPeriodEndDesc_SGS")
        If Err.Number <> 0 Then
            lRetval = Err.Number
        Else
            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
            'errors after each attempt.  After storing all return values in local variables OR
            'encountering an error, release parameters.
            sDesc = DBObj.GetOutParam(3)
            lRetval = DBObj.GetOutParam(4)
        End If
    End With

    DBObj.ReleaseParams
    
    If lRetval <> 0 Then
        MsgBox "There was an error determining the Period Ending description of the Report Title.", vbExclamation, "MAS 500"
        
    End If
    
    GetPeriodEndingDesc = sDesc
    
    gClearSotaErr
    
    Exit Function
Error:
    MsgBox "basComplianceReport.GetPeriodEndingDesc()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
End Function

Public Sub Main()
    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("sozxCmplnceRpt.clsComplianceReport")
        StartAppInStandaloneMode oApp, Command$()    ' ** The third parameter MUST be the
                                                     '    TaskID for this project in order for the
                                                     '    Standalone method to be activated for ActiveX Exe's
    End If
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basComplianceReport"
End Function

Public Function lStartReport(sButton As String, frm As Form, bPeriodEnd As Boolean, _
    lcCompanyID As String, lcContractID As String, Optional iFileType As Variant, Optional sFileName As Variant) As Long
    
    Dim sWhereClause As String
    Dim sTablesUsed As String
    Dim sSelect As String
    Dim sInsert As String
    Dim lRetval As Long
    Dim bValid As Boolean
    Dim iNumTablesUsed As Integer
    Dim RptFileName As String
    Dim lBadRow As Long
    Dim SelectObj As clsSelection
    Dim ReportObj As clsReportEngine
    Dim SortObj As clsSort
    Dim DBObj As Object
    Dim SettingsObj As clsSettings
    
    Dim bRealWorkTbl As Boolean

    On Error GoTo badexit

    lStartReport = kFailure
    
    If Not bPeriodEnd Then ShowStatusBusy frm

    Set SelectObj = frm.moSelect
    Set ReportObj = frm.moReport
    Set SortObj = frm.moSort

    Set DBObj = frm.oClass.moAppDB
    Set SettingsObj = frm.moSettings
   
   
    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
    
'    'Correct bad user input in the Selection grid.
'    'lBadRow contains the failed row if the parameter passed is False
'    lBadRow = SelectObj.lValidateGrid(True)
'
'    'Create the WHERE clause based on user input in the Selection grid & Session ID.
'    bValid = SelectObj.bGetWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True)
'
'    'Append criteria to restrict data returned to the current company.
'    SelectObj.AppendToWhereClause sWhereClause, "tsoCertificateOfComplianceWrk_SGS.CompanyID=" & gsQuoted(frm.msCompanyID)
'
'    'CUSTOMIZE:  Add join specifications to WHERE clause based on appearance of a given table
'    'in the sTablesUsed string (built as a result of call to bGetWhereClause above).
'    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tciAddress", "tarCustomer.PrimaryAddrKey=tciAddress.AddrKey"
'    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustAddr", "tarCustomer.CustKey=tarCustAddr.CustKey"
'
'    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
'    If Not SelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
'        GoTo badexit
'    End If
'
'    'CUSTOMIZE:  Build a string for selection of key field and session ID.
'    sSelect = "SELECT DISTINCT vluComplianceReportData_SGS.*, " & ReportObj.SessionID & " FROM " & sTablesUsed
'    sSelect = sSelect & " " & sWhereClause
    #If RPTDEBUG Then
        sInsert = "Insert into tsoCertificateOfComplianceWrk_SGS select *, " & ReportObj.SessionID & " From vluSOCertificateOfComplianceRpt_SGS Where CompanyID = " & gsQuoted(lcCompanyID) & " And TranNO = " & gsQuoted(lcContractID)
        bRealWorkTbl = True
    
    #Else
        sInsert = "Insert into #tsoCertificateOfComplianceWrk_SGS select *, " & ReportObj.SessionID & "  From vluSOCertificateOfComplianceRpt_SGS Where CompanyID = " & gsQuoted(lcCompanyID) & " And TranNO = " & gsQuoted(lcContractID)
        bRealWorkTbl = False
    #End If

    On Error Resume Next
    DBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
    gClearSotaErr
    On Error GoTo badexit
    
    #If RPTDEBUG Then
        If glGetValidLong(frmComplianceReport.oClass.moAppDB.Lookup("Count(*)", "tsoCertificateOfComplianceWrk_SGS", "SessionID = " & ReportObj.SessionID)) <= 0 Then
            MsgBox "No data to view.", vbExclamation, "MAS 500"
            GoTo badexit
        End If
    #Else
        If glGetValidLong(frmComplianceReport.oClass.moAppDB.Lookup("Count(*)", "#tsoCertificateOfComplianceWrk_SGS", "SessionID = " & ReportObj.SessionID)) <= 0 Then
            MsgBox "No data to view.", vbExclamation, "MAS 500"
            GoTo badexit
        End If
    #End If
    
    
'
'    'Check whether any records were inserted; if not, go to error handler.
'    If IsMissing(iFileType) Then
'        #If RPTDEBUG Then
'            If Not SelectObj.bRecsToPrintExist(frm.sWorkTableCollection(1), "SessionID", ReportObj.SessionID, True) Then
'        #Else
'            If Not SelectObj.bRecsToPrintExist("#" & Left(frm.sWorkTableCollection(1), 19), "SessionID", ReportObj.SessionID, True) Then
'        #End If
'            GoTo badexit
'        End If
'    End If
   
   
'    'CUSTOMIZE:  Set input and output parameters as needed and execute appropriate
'    'custom stored procedure to populate remaining fields in primary work table and
'    'insert records into secondary work tables.
'    With DBObj
'        .SetInParam (ReportObj.SessionID)   'SessionID
'        .SetInParam (2011)   'Fisc Year
'        .SetInParam (4)   'Fisc Period (Month)
'        .SetInParam (iRealWorkTbl)   'Fisc Period (Month)
'        .SetOutParam lRetval
'        On Error Resume Next
'        .ExecuteSP ("spGLComplianceRptData_SGS")
'        If Err.Number <> 0 Then
'            lRetval = Err.Number
'        Else
'            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
'            'errors after each attempt.  After storing all return values in local variables OR
'            'encountering an error, release parameters.
'            lRetval = DBObj.GetOutParam(5)
'        End If
'    End With
'
'    If lRetval <> 0 Then
'        ReportObj.ReportError kmsgProc, ""
'        DBObj.ReleaseParams
'        GoTo badexit
'    End If
'
'    DBObj.ReleaseParams
'
'    gClearSotaErr
'
'    'Check if records were created for report
'    #If RPTDEBUG Then
'        If Not SelectObj.bRecsToPrintExist(frm.sWorkTableCollection(1), "SessionID", ReportObj.SessionID, True) Then
'    #Else
'        If Not SelectObj.bRecsToPrintExist("#" & Left(frm.sWorkTableCollection(1), 19), "SessionID", ReportObj.SessionID, True) Then
'    #End If
'        GoTo badexit
'    End If

    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)
    ReportObj.ReportFileName() = "Certificate of Compliance.rpt"
    
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    ReportObj.Orientation() = vbPRORPortrait
    
    'CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    'to gsBuildString with a VB constant defined in LocalizationConst.bas. The subtitles should
    'not include the format selected by the user, i.e., "Detail" or "Summary".
    'ReportObj.ReportTitle2() = ""
    
'    ReportObj.MessageLines = "Hellow World" & vbCrLf & "two" & vbCrLf & "three" & vbCrLf & "four" & vbCrLf & "5"
''    ReportObj.ReportTitle1() = "Compliance Report"
''    ReportObj.ReportTitle2() = " testing "
''    ReportObj.lSetOptionalFormulas
'    Call ReportObj.SetReportCaptions

'    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
'    'using the "lbl" convention on formula field names so that label text will handled automatically
'    ReportObj.UseSubTotalCaptions() = 1
'    ReportObj.UseHeaderCaptions() = 1
        
'    'set standard formulas, business date, run time, company name etc.
'    'as defined in the template
'    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
'        GoTo badexit
'    End If

'    'Set sort order in .RPT file according to user selections in the Sort grid.
'    If (ReportObj.lSetSortCriteria(frm.moSort) = kFailure) Then
'        GoTo badexit
'    End If
    
'    '********* the following is specific to your report *************'
'    Select Case RptFileName
'        Case "XXZYY001.RPT"
'        Case Else
'    End Select
'    '********* End of special processing *************'
    
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.SetSQL
    
'    'CUSTOMIZE:  Include this call if you have named column labels on the report
'    'using the "lbl" convention and wish label text to be handled automatically for you.
'    ReportObj.SetReportCaptions
    
'    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
''    ReportObj.SelectString = SelectObj.sGetUserReadableWhereClause(kLenPortrait)
'    #If RPTDEBUG Then
'        'Working with Real Work Table
'        ReportObj.SelectString = "Select * From tsoCertificateOfComplianceWrk_SGS Where SessionID = " & ReportObj.SessionID
'    #Else
'        'Working with Temp Work Table
'        ReportObj.SelectString = "Select * From #tsoCertificateOfComplianceWrk_SGS Where SessionID = " & ReportObj.SessionID
'    #End If
    
    
'    'If user has chosen to print report settings, set text for summary section.
'    If (ReportObj.lSetSummarySection(frm.chkSummary.Value, frm.moOptions) = kFailure) Then
'        GoTo badexit
'    End If
    
    'CUSTOMIZE:  If using work tables, restrict report data to current Session ID.  If using
    'real tables, might restrict report data to current company or other criteria.
'    If (ReportObj.lRestrictBy("{.SessionID} = " & ReportObj.SessionID) = kFailure) Then
    If (ReportObj.lRestrictBy("{tsoCertificateOfComplianceWrk_SGS.SessionID} = " & ReportObj.SessionID) = kFailure) Then
        GoTo badexit
    End If
    
    If bPeriodEnd Then
        gbSkipPrintDialog = True    '   don't show print dialog
    End If
    
    ReportObj.ProcessReport frm, sButton, iFileType, sFileName
    gbSkipPrintDialog = True    '   if multiple rpts, show print dialog one time only.
            
            
    If Not bPeriodEnd Then ShowStatusNone frm
    
    lStartReport = kSuccess
    
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    
    Exit Function
    
badexit:
    ReportObj.CleanupWorkTables
    If Not bPeriodEnd Then ShowStatusNone frm
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
    Exit Function
End Function


Sub InsertMiles(bRealWorkTbl As Boolean, frm As Form, DBObj As Object, SessionID As Long)
    
    Dim MilesCurrMonthActual As Double
    Dim MilesCurrMonthPlan As Double
    Dim MilesCurrActualtoPlan As Double
    
    Dim MilesYTDActual As Double
    Dim MilesYTDPlan As Double
    Dim MilesYTDPriorYear As Double
    
    Dim SQL As String
    
    MilesCurrMonthActual = CDbl(frm.txtMilesCurrMonthActual.Text)
    MilesCurrMonthPlan = CDbl(frm.txtMilesCurrMonthPlan.Text)
    
    MilesCurrActualtoPlan = MilesCurrMonthActual - MilesCurrMonthPlan
    
    MilesYTDActual = CDbl(frm.txtMilesYTDActual.Text)
    MilesYTDPlan = CDbl(frm.txtMilesYTDPlan.Text)
    MilesYTDPriorYear = CDbl(frm.txtMilesYTDYear.Text)
    
    'PageKey, SubCategoryKey,
    'CurrMonthPlan, CurrMonthActual, CurrMonthActualToPlan,
    'CurrYTDActual, CurrYTDPlan, PriorYTDActual
    
    If bRealWorkTbl = True Then
        SQL = "Update tsoCertificateOfComplianceWrk_SGS Set "
        
    Else
        SQL = "Update #tsoCertificateOfComplianceWrk_SGS Set "
    
    End If

    SQL = SQL & "CurrMonthPlan = " & MilesCurrMonthPlan & ", "
    SQL = SQL & "CurrMonthActual = " & MilesCurrMonthActual & ", "
    SQL = SQL & "CurrMonthActualToPlan = " & MilesCurrActualtoPlan & ", "
    SQL = SQL & "CurrYTDActual = " & MilesYTDActual & ", "
    SQL = SQL & "CurrYTDPlan = " & MilesYTDPlan & ", "
    SQL = SQL & "PriorYTDActual = " & MilesYTDPriorYear & " "
    
    SQL = SQL & "Where SubCategoryKey = " & TruckingMiles & " "
    SQL = SQL & "And PageKey IN( " & JohnnyB & ", " & JohnnyBConsolidated & ", " & IASConsolidated & " ) "
    
    SQL = SQL & "And SessionID = " & SessionID

    On Error Resume Next
    DBObj.ExecuteSQL SQL
    
    If Err.Number <> 0 Then
        MsgBox "There was an error inserting the Miles Data for the report:" & vbCrLf & _
        "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
        "SQL = " & SQL, vbExclamation, "MAS 500"
    End If
    
    gClearSotaErr
    
End Sub

Sub InsertEsca(bRealWorkTbl As Boolean, frm As Form, DBObj As Object, SessionID As Long)
    'Insert Escalation/De-Escalation
    Dim EscaCurrMonthActual As Double
    Dim EscaCurrMonthPlan As Double
    Dim EscaCurrActualtoPlan As Double
    
    Dim EscaYTDActual As Double
    Dim EscaYTDPlan As Double
    Dim EscaYTDPriorYear As Double
    
    Dim SQL As String
    
    EscaCurrMonthActual = CDbl(frm.txtEscaCurrMonthActual.Text)
    EscaCurrMonthPlan = CDbl(frm.txtEscaCurrMonthPlan.Text)
    
    EscaCurrActualtoPlan = EscaCurrMonthActual - EscaCurrMonthPlan
    
    EscaYTDActual = CDbl(frm.txtEscaYTDActual.Text)
    EscaYTDPlan = CDbl(frm.txtEscaYTDPlan.Text)
    EscaYTDPriorYear = CDbl(frm.txtEscaYTDPrior.Text)
    
    'PageKey, SubCategoryKey,
    'CurrMonthPlan, CurrMonthActual, CurrMonthActualToPlan,
    'CurrYTDActual, CurrYTDPlan, PriorYTDActual
    
    If bRealWorkTbl = True Then
        SQL = "Update tsoCertificateOfComplianceWrk_SGS Set "
        
    Else
        SQL = "Update #tsoCertificateOfComplianceWrk_SGS Set "
    
    End If

    SQL = SQL & "CurrMonthPlan = " & EscaCurrMonthPlan & ", "
    SQL = SQL & "CurrMonthActual = " & EscaCurrMonthActual & ", "
    SQL = SQL & "CurrMonthActualToPlan = " & EscaCurrActualtoPlan & ", "
    SQL = SQL & "CurrYTDActual = " & EscaYTDActual & ", "
    SQL = SQL & "CurrYTDPlan = " & EscaYTDPlan & ", "
    SQL = SQL & "PriorYTDActual = " & EscaYTDPriorYear & " "
    
    SQL = SQL & "Where SubCategoryKey = " & Escalation & " "
    SQL = SQL & "And PageKey IN( " & IASConsolidated & " ) "
    
    SQL = SQL & "And SessionID = " & SessionID

    On Error Resume Next
    DBObj.ExecuteSQL SQL
    
    If Err.Number <> 0 Then
        MsgBox "There was an error inserting the Escalation/De-Escalation Data for the report:" & vbCrLf & _
        "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
        "SQL = " & SQL, vbExclamation, "MAS 500"
    End If
    
    gClearSotaErr
    
End Sub


Sub InsertSalesTons(bRealWorkTbl As Boolean, frm As Form, DBObj As Object, SessionID As Long)
    Dim CurrMonthActual As Double
    Dim CurrMonthPlan As Double
    Dim CurrActualtoPlan As Double
    
    Dim YTDActual As Double
    Dim YTDPlan As Double
    Dim YTDPriorYear As Double
        
    Dim sTableName As String
    Dim sTable As String
    Dim sUpdate As String
    Dim sFilter As String
    Dim SQL As String
    
    Dim i As Integer
    Dim valIndex As Integer
    
    If bRealWorkTbl = True Then
        sTable = "Update tsoCertificateOfComplianceWrk_SGS Set "
        sTableName = "tsoCertificateOfComplianceWrk_SGS"
    Else
        sTable = "Update #tsoCertificateOfComplianceWrk_SGS Set "
        sTableName = "#tsoCertificateOfComplianceWrk_SGS"
    End If

    
    For i = 1 To 5
        Select Case i
            Case 1
                valIndex = BlackfootPlant
                
            Case 2
                valIndex = HauserPlant
            
            Case 3
                valIndex = NampaPlant
            
            Case 4
                valIndex = RawlinsPlant
            
            Case 5
                valIndex = WoodsCrossPlant
        
        End Select
        
        CurrMonthActual = CDbl(frm.txtCurrMonthActual(valIndex).Text)
        CurrMonthPlan = CDbl(frm.txtCurrMonthPlan(valIndex).Text)
        
        CurrActualtoPlan = CurrMonthActual - CurrMonthPlan
        
        YTDActual = CDbl(frm.txtYTDActual(valIndex).Text)
        YTDPlan = CDbl(frm.txtYTDPlan(valIndex).Text)
        YTDPriorYear = CDbl(frm.txtYTDPriorYear(valIndex).Text)
        
        'PageKey, SubCategoryKey,
        'CurrMonthPlan, CurrMonthActual, CurrMonthActualToPlan,
        'CurrYTDActual, CurrYTDPlan, PriorYTDActual
        
    
        sUpdate = "CurrMonthPlan = " & CurrMonthPlan & ", "
        sUpdate = sUpdate & "CurrMonthActual = " & CurrMonthActual & ", "
        sUpdate = sUpdate & "CurrMonthActualToPlan = " & CurrActualtoPlan & ", "
        sUpdate = sUpdate & "CurrYTDActual = " & YTDActual & ", "
        sUpdate = sUpdate & "CurrYTDPlan = " & YTDPlan & ", "
        sUpdate = sUpdate & "PriorYTDActual = " & YTDPriorYear & " "
        
        sFilter = "Where SubCategoryKey = " & SalesTons & " "
        sFilter = sFilter & "And PageKey IN( " & valIndex & " ) "
        
        sFilter = sFilter & "And SessionID = " & SessionID
        
        SQL = sTable + sUpdate + sFilter
        
        On Error Resume Next
        DBObj.ExecuteSQL SQL
        
        If Err.Number <> 0 Then
            MsgBox "There was an error inserting the Sales-Tons Data for the report:" & vbCrLf & _
            "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
            "SQL = " & SQL, vbExclamation, "MAS 500"
        End If
    
    Next
    
    'Update the records that are groups or summary...
    
    'IAS Consolidated
    SQL = "Update a Set "
    SQL = SQL & "a.CurrMonthPlan = IsNull(c.CurrMonthPlan, 0.000), "
    SQL = SQL & "a.CurrMonthActual = IsNull(c.CurrMonthActual, 0.000), "
    SQL = SQL & "a.CurrMonthActualToPlan = IsNull(c.CurrMonthActualToPlan, 0.000), "
    SQL = SQL & "a.CurrYTDActual = IsNull(c.CurrYTDActual, 0.000), "
    SQL = SQL & "a.CurrYTDPlan = IsNull(c.CurrYTDPlan, 0.000), "
    SQL = SQL & "a.PriorYTDActual = IsNull(c.PriorYTDActual, 0.000) "
    SQL = SQL & "From " & sTableName & " a "
    SQL = SQL & "Inner Join ( "
    SQL = SQL & "select 1 'PageKey', "
    SQL = SQL & "Sum (IsNull(b.CurrMonthPlan, 0.000))  'CurrMonthPlan', Sum(IsNull(b.CurrMonthActual, 0.000)) 'CurrMonthActual', Sum(IsNull(b.CurrMonthActualToPlan, 0.000)) 'CurrMonthActualToPlan', "
    SQL = SQL & "Sum (IsNull(b.CurrYTDActual, 0.000))   'CurrYTDActual', Sum(IsNull(b.CurrYTDPlan, 0.000)) 'CurrYTDPlan', Sum(IsNull(b.PriorYTDActual, 0.000)) 'PriorYTDActual' "
    SQL = SQL & "From " & sTableName & " b "
    SQL = SQL & "Where b.SubCategoryKey = 5 "
    SQL = SQL & "And b.PageKey IN( 8, 9, 10, 18, 19 ) "
    SQL = SQL & "and SessionID = " & SessionID & " "
    SQL = SQL & ") c "
    SQL = SQL & "on a.Pagekey = c.Pagekey "
    SQL = SQL & "Where a.SubCategoryKey = 5 "
    SQL = SQL & "and a.SessionID = " & SessionID & " "
    
    DBObj.ExecuteSQL SQL
    
    If Err.Number <> 0 Then
        MsgBox "There was an error inserting the Sales-Tons Data for the report:" & vbCrLf & _
        "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
        "SQL = " & SQL, vbExclamation, "MAS 500"
    End If
        
    'IAS Operations
    SQL = "Update a Set "
    SQL = SQL & "a.CurrMonthPlan = IsNull(c.CurrMonthPlan, 0.000), "
    SQL = SQL & "a.CurrMonthActual = IsNull(c.CurrMonthActual, 0.000), "
    SQL = SQL & "a.CurrMonthActualToPlan = IsNull(c.CurrMonthActualToPlan, 0.000), "
    SQL = SQL & "a.CurrYTDActual = IsNull(c.CurrYTDActual, 0.000), "
    SQL = SQL & "a.CurrYTDPlan = IsNull(c.CurrYTDPlan, 0.000), "
    SQL = SQL & "a.PriorYTDActual = IsNull(c.PriorYTDActual, 0.000) "
    SQL = SQL & "From " & sTableName & " a "
    SQL = SQL & "Inner Join ( "
    SQL = SQL & "select 4 'PageKey', "
    SQL = SQL & "Sum (IsNull(b.CurrMonthPlan, 0.000))  'CurrMonthPlan', Sum(IsNull(b.CurrMonthActual, 0.000)) 'CurrMonthActual', Sum(IsNull(b.CurrMonthActualToPlan, 0.000)) 'CurrMonthActualToPlan', "
    SQL = SQL & "Sum (IsNull(b.CurrYTDActual, 0.000))   'CurrYTDActual', Sum(IsNull(b.CurrYTDPlan, 0.000)) 'CurrYTDPlan', Sum(IsNull(b.PriorYTDActual, 0.000)) 'PriorYTDActual' "
    SQL = SQL & "From " & sTableName & " b "
    SQL = SQL & "Where b.SubCategoryKey = 5 "
    SQL = SQL & "And b.PageKey IN( 8, 9, 10, 18, 19 ) "
    SQL = SQL & "and SessionID = " & SessionID & " "
    SQL = SQL & ") c "
    SQL = SQL & "on a.Pagekey = c.Pagekey "
    SQL = SQL & "Where a.SubCategoryKey = 5 "
    SQL = SQL & "and a.SessionID = " & SessionID & " "
    
    DBObj.ExecuteSQL SQL
    
    If Err.Number <> 0 Then
        MsgBox "There was an error inserting the Sales-Tons Data for the report:" & vbCrLf & _
        "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
        "SQL = " & SQL, vbExclamation, "MAS 500"
    End If

    
    'IAS
    SQL = "Update a Set "
    SQL = SQL & "a.CurrMonthPlan = IsNull(c.CurrMonthPlan, 0.000), "
    SQL = SQL & "a.CurrMonthActual = IsNull(c.CurrMonthActual, 0.000), "
    SQL = SQL & "a.CurrMonthActualToPlan = IsNull(c.CurrMonthActualToPlan, 0.000), "
    SQL = SQL & "a.CurrYTDActual = IsNull(c.CurrYTDActual, 0.000), "
    SQL = SQL & "a.CurrYTDPlan = IsNull(c.CurrYTDPlan, 0.000), "
    SQL = SQL & "a.PriorYTDActual = IsNull(c.PriorYTDActual, 0.000) "
    SQL = SQL & "From " & sTableName & " a "
    SQL = SQL & "Inner Join ( "
    SQL = SQL & "select 2 'PageKey', "
    SQL = SQL & "Sum (IsNull(b.CurrMonthPlan, 0.000))  'CurrMonthPlan', Sum(IsNull(b.CurrMonthActual, 0.000)) 'CurrMonthActual', Sum(IsNull(b.CurrMonthActualToPlan, 0.000)) 'CurrMonthActualToPlan', "
    SQL = SQL & "Sum (IsNull(b.CurrYTDActual, 0.000))   'CurrYTDActual', Sum(IsNull(b.CurrYTDPlan, 0.000)) 'CurrYTDPlan', Sum(IsNull(b.PriorYTDActual, 0.000)) 'PriorYTDActual' "
    SQL = SQL & "From " & sTableName & " b "
    SQL = SQL & "Where b.SubCategoryKey = 5 "
    SQL = SQL & "And b.PageKey IN( 8, 9, 10 ) "
    SQL = SQL & "and SessionID = " & SessionID & " "
    SQL = SQL & ") c "
    SQL = SQL & "on a.Pagekey = c.Pagekey "
    SQL = SQL & "Where a.SubCategoryKey = 5 "
    SQL = SQL & "and a.SessionID = " & SessionID & " "
    
    DBObj.ExecuteSQL SQL
    
    If Err.Number <> 0 Then
        MsgBox "There was an error inserting the Sales-Tons Data for the report:" & vbCrLf & _
        "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
        "SQL = " & SQL, vbExclamation, "MAS 500"
    End If
    
    'Peak Consolidated
    SQL = "Update a Set "
    SQL = SQL & "a.CurrMonthPlan = IsNull(c.CurrMonthPlan, 0.000), "
    SQL = SQL & "a.CurrMonthActual = IsNull(c.CurrMonthActual, 0.000), "
    SQL = SQL & "a.CurrMonthActualToPlan = IsNull(c.CurrMonthActualToPlan, 0.000), "
    SQL = SQL & "a.CurrYTDActual = IsNull(c.CurrYTDActual, 0.000), "
    SQL = SQL & "a.CurrYTDPlan = IsNull(c.CurrYTDPlan, 0.000), "
    SQL = SQL & "a.PriorYTDActual = IsNull(c.PriorYTDActual, 0.000) "
    SQL = SQL & "From " & sTableName & " a "
    SQL = SQL & "Inner Join ( "
    SQL = SQL & "select 15 'PageKey', "
    SQL = SQL & "Sum (IsNull(b.CurrMonthPlan, 0.000))  'CurrMonthPlan', Sum(IsNull(b.CurrMonthActual, 0.000)) 'CurrMonthActual', Sum(IsNull(b.CurrMonthActualToPlan, 0.000)) 'CurrMonthActualToPlan', "
    SQL = SQL & "Sum (IsNull(b.CurrYTDActual, 0.000))   'CurrYTDActual', Sum(IsNull(b.CurrYTDPlan, 0.000)) 'CurrYTDPlan', Sum(IsNull(b.PriorYTDActual, 0.000)) 'PriorYTDActual' "
    SQL = SQL & "From " & sTableName & " b "
    SQL = SQL & "Where b.SubCategoryKey = 5 "
    SQL = SQL & "And b.PageKey IN( 18, 19 ) "
    SQL = SQL & "and SessionID = " & SessionID & " "
    SQL = SQL & ") c "
    SQL = SQL & "on a.Pagekey = c.Pagekey "
    SQL = SQL & "Where a.SubCategoryKey = 5 "
    SQL = SQL & "and a.SessionID = " & SessionID & " "
    
    DBObj.ExecuteSQL SQL
    
    If Err.Number <> 0 Then
        MsgBox "There was an error inserting the Sales-Tons Data for the report:" & vbCrLf & _
        "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
        "SQL = " & SQL, vbExclamation, "MAS 500"
    End If

    gClearSotaErr

End Sub

Sub DeletePages(bRealWorkTbl As Boolean, frm As Form, DBObj As Object, SessionID As Long)
    
    Dim i As Integer
    
    Dim SQL As String
        
    If bRealWorkTbl = True Then
        SQL = "Delete tsoCertificateOfComplianceWrk_SGS Where PageKey IN( -50 "
        
    Else
        SQL = "Delete #tsoCertificateOfComplianceWrk_SGS Where PageKey IN( -50 "
    
    End If
    
    For i = 1 To 19
        If frm.chRptPages(i).Value = vbUnchecked Then
            SQL = SQL & " , " & i
        End If
    Next
    
    SQL = SQL & " ) "
    
    SQL = SQL & "And SessionID = " & SessionID

    On Error Resume Next
    DBObj.ExecuteSQL SQL
    
    If Err.Number <> 0 Then
        MsgBox "There was an error inserting the Miles Data for the report:" & vbCrLf & _
        "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
        "SQL = " & SQL, vbExclamation, "MAS 500"
    End If
    
    gClearSotaErr

End Sub




