Attribute VB_Name = "basSOCompetitor_RtpReport"
Option Explicit


Const VBRIG_MODULE_ID_STRING = "sozxxCompetitor_Rtp.BAS"

Public gbSkipPrintDialog As Boolean     '   Show print dialog




Public Sub Main()
    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("sozxxCompetitor_Rtp.clsSOCompetitorReport")
        StartAppInStandaloneMode oApp, Command$()    ' ** The third parameter MUST be the
                                                     '    TaskID for this project in order for the
                                                     '    Standalone method to be activated for ActiveX Exe's
    End If
End Sub

'SGS DEJ
Function PrepSelect(SessionID As String) As String
    Dim sSelect As String
    Dim DtVal As Variant
    
    'SGS DEJ Added to Display the Date Range on the Report START
    frmSOCompetitorReport.msShpDtRnge = Empty
     
    frmSOCompetitorReport.grdSelection.GetText 3, frmSOCompetitorReport.mlShpDtRow, DtVal
    frmSOCompetitorReport.msShpDtRnge = frmSOCompetitorReport.msShpDtRnge & Trim(CStr(DtVal))
    frmSOCompetitorReport.grdSelection.GetText 4, frmSOCompetitorReport.mlShpDtRow, DtVal
    
    If Trim(CStr(DtVal)) <> Empty Then
        frmSOCompetitorReport.msShpDtRnge = frmSOCompetitorReport.msShpDtRnge & " - " & Trim(CStr(DtVal))
    End If
    'SGS DEJ Added to Display the Date Range on the Report END
    
  
    sSelect = _
    "Select " & SessionID & "'SessionID', " & _
    "'" & frmSOCompetitorReport.msUserID & "' 'User', " & _
    "IsIASBid, CompanyID, Origin, CompetitorID, CompetitorDesc, BTranNo, " & _
    "ContractNo, Item, ItemLongDesc, ItemShortDesc, BidDate, CustName, " & _
    "CustID, ProjectDesc, OrigBidQty, IASPrice, Price, Freight, " & _
    "Tax, Delivered, MilesToJob, Awarded, Notes, " & _
    "ItemClassKey, ItemClassID" & _
    ", '" & frmSOCompetitorReport.msShpDtRnge & "' " & _
    "From vsoCompetitorBids_SGS Where 1=1 "


    PrepSelect = sSelect
End Function

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basSOCompetitorReport"
End Function

Public Function lStartReport(sButton As String, frm As Form, bPeriodEnd As Boolean, _
    sPerEndDate, iPerNo, sPerYear, Optional iFileType As Variant, Optional sFileName As Variant) As Long
    
Dim sWhereClause As String
Dim sTablesUsed As String
Dim sSelect As String
Dim sInsert As String
Dim lRetval As Long
Dim bValid As Boolean
Dim iNumTablesUsed As Integer
Dim RptFileName As String
Dim lBadRow As Long
Dim SelectObj As clsSelection
Dim ReportObj As clsReportEngine
Dim SortObj As clsSort
Dim DBObj As Object
Dim SettingsObj As clsSettings

    On Error GoTo badexit

    lStartReport = kFailure
    
    If Not bPeriodEnd Then ShowStatusBusy frm

    Set SelectObj = frm.moSelect
    Set ReportObj = frm.moReport
    Set SortObj = frm.moSort

    Set DBObj = frm.oClass.moAppDB
    Set SettingsObj = frm.moSettings
   
    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
    
    'Correct bad user input in the Selection grid.
    'lBadRow contains the failed row if the parameter passed is False
    lBadRow = SelectObj.lValidateGrid(True)
    
    'Create the WHERE clause based on user input in the Selection grid & Session ID.
    bValid = SelectObj.bGetWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True)

    'Append criteria to restrict data returned to the current company.
    SelectObj.AppendToWhereClause sWhereClause, "vsoCompetitorBids_SGS.CompanyID=" & gsQuoted(frm.msCompanyID)
    
    'CUSTOMIZE:  Add join specifications to WHERE clause based on appearance of a given table
    'in the sTablesUsed string (built as a result of call to bGetWhereClause above).
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tciAddress", "tarCustomer.PrimaryAddrKey=tciAddress.AddrKey"
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustAddr", "tarCustomer.CustKey=tarCustAddr.CustKey"
    
'    'SGS DEJ Commented out because we do not need the sintax of 'Where'
'    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
'    If Not SelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
'        GoTo badexit
'    End If

    'CUSTOMIZE:  Build a string for selection of key field and session ID.
'    sSelect = "SELECT DISTINCT timItem., " & ReportObj.SessionID & " FROM " & sTablesUsed
'    sSelect = sSelect & " " & sWhereClause
Debug.Print sWhereClause
    #If RPTDEBUG Then
        sInsert = "INSERT INTO tsoCompetitorBidsWrk_SGS " & PrepSelect(ReportObj.SessionID) & " And " & sWhereClause
    #Else
        sInsert = "INSERT INTO #tsoCompetitorBidsWrk_SGS " & PrepSelect(ReportObj.SessionID) & " And " & sWhereClause
    #End If
Debug.Print sInsert
    
Debug.Print sInsert
    On Error Resume Next
    DBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
    gClearSotaErr
    On Error GoTo badexit
    
    'Check whether any records were inserted; if not, go to error handler.
    If IsMissing(iFileType) Then
        #If RPTDEBUG Then
            If Not SelectObj.bRecsToPrintExist(frm.sWorkTableCollection(1), "SessionID", ReportObj.SessionID, True) Then
'            If Not SelectObj.bRecsToPrintExist(tsoCompetitorBidsWrk_SGS, "SessionID", ReportObj.SessionID, True) Then
        #Else
'            If Not SelectObj.bRecsToPrintExist("#tsoCompetitorBidsWrk_SGS", "SessionID", ReportObj.SessionID, True) Then
            If Not SelectObj.bRecsToPrintExist("#" & frm.sWorkTableCollection(1), "SessionID", ReportObj.SessionID, True) Then
'            If Not SelectObj.bRecsToPrintExist("#" & Left(frm.sWorkTableCollection(1), 19), "SessionID", ReportObj.SessionID, True) Then
        #End If
            GoTo badexit
        End If
    End If
   
'    'CUSTOMIZE:  Set input and output parameters as needed and execute appropriate
'    'custom stored procedure to populate remaining fields in primary work table and
'    'insert records into secondary work tables.
'    With DBObj
'        .SetInParam (ReportObj.SessionID)
'        .SetOutParam lRetval
'        On Error Resume Next
'        .ExecuteSP ("spSGSReportTest")
'        If Err.Number <> 0 Then
'            lRetval = Err.Number
'        Else
'            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
'            'errors after each attempt.  After storing all return values in local variables OR
'            'encountering an error, release parameters.
'            lRetval = DBObj.GetOutParam(2)
'        End If
'    End With
'
'    If lRetval <> 0 Then
'        ReportObj.ReportError kmsgProc, ""
'        DBObj.ReleaseParams
'        GoTo badexit
'    End If
        
    DBObj.ReleaseParams
    gClearSotaErr
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)
    'SGS DEJ
    ReportObj.ReportFileName() = "IACompetitorReport.rpt"
    
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    'SGS DEJ
    ReportObj.Orientation() = vbPRORLandscape
    
    'CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    'to gsBuildString with a VB constant defined in LocalizationConst.bas. The subtitles should
    'not include the format selected by the user, i.e., "Detail" or "Summary".
    'ReportObj.ReportTitle2() = ""

    ReportObj.ReportTitle1() = "Competitor Report"

    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    'using the "lbl" convention on formula field names so that label text will handled automatically
    ReportObj.UseSubTotalCaptions() = 1
    ReportObj.UseHeaderCaptions() = 1
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If

    'Set sort order in .RPT file according to user selections in the Sort grid.
''''''SGS DEJ added the number of hard coded groupings.
    If (ReportObj.lSetSortCriteria(frm.moSort, 4) = kFailure) Then
        GoTo badexit
    End If
    
    '********* the following is specific to your report *************'
    Select Case RptFileName
        Case "XXZYY001.RPT"
        Case Else
    End Select
    '********* End of special processing *************'
    
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.SetSQL

    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    ReportObj.SetReportCaptions
    
    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
    ReportObj.SelectString = SelectObj.sGetUserReadableWhereClause(kLenPortrait)
    
    'If user has chosen to print report settings, set text for summary section.
    If (ReportObj.lSetSummarySection(frm.chkSummary.Value, frm.moOptions) = kFailure) Then
        GoTo badexit
    End If
    
    'CUSTOMIZE:  If using work tables, restrict report data to current Session ID.  If using
    'real tables, might restrict report data to current company or other criteria.
'    If (ReportObj.lRestrictBy("{.SessionID} = " & ReportObj.SessionID) = kFailure) Then
'SGS DEJ change to map to the actual table.
    If (ReportObj.lRestrictBy("{" & frm.sWorkTableCollection(1) & ".SessionID} = " & ReportObj.SessionID) = kFailure) Then

    'frm.sWorkTableCollection(1)
        GoTo badexit
    End If
    
    If bPeriodEnd Then
        gbSkipPrintDialog = True    '   don't show print dialog
    End If
    
    ReportObj.ProcessReport frm, sButton, iFileType, sFileName
    gbSkipPrintDialog = True    '   if multiple rpts, show print dialog one time only.
            
            
    If Not bPeriodEnd Then ShowStatusNone frm
    
    lStartReport = kSuccess
    
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    
    Exit Function
    
badexit:
    ReportObj.CleanupWorkTables
    If Not bPeriodEnd Then ShowStatusNone frm
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
    Exit Function
End Function

