VERSION 1.0 CLASS
BEGIN
  MultiUse = 0   'False
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsSOCompetitorReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public moFramework          As Object

Public mlContext            As Long

Public moDasSession         As Object

Public moAppDB              As Object

Public moSysSession         As Object

Public miShutDownRequester  As Integer

Public mlError              As Long

Private mlRunFlags          As Long

Private mlUIActive          As Long

Public mfrmMain            As Form

Const VBRIG_MODULE_ID_STRING = "sozxxCompetitor_Rtp.CLS"

Public mbPeriodEnd          As Boolean


Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "clsSOCompetitorReport"
End Function

Private Sub Class_Initialize()
    mlUIActive = kChildObjectInactive
    miShutDownRequester = kFrameworkShutDown
End Sub

Public Function InitializeObject(ByVal oFramework As Object, ByVal lContext As Long) As Long
    InitializeObject = kFailure
    DefaultInitializeObject Me, oFramework, lContext, App.ProductName, App.Title
    InitializeObject = kSuccess     ' return success
End Function

Public Function LoadUI(ByVal lContext As Long) As Long
    LoadUI = kFailure

    Set mfrmMain = frmSOCompetitorReport

    Set mfrmMain.oClass = Me
    mfrmMain.lRunMode = mlContext And kRunModeMask
    
    Load frmSOCompetitorReport
    If mlError Then Err.Raise mlError
    
    'determine if the form load was successful
    If mfrmMain.bLoadSuccess Then
        LoadUI = kSuccess
    Else
        LoadUI = kFailure
    End If

End Function

Public Function GetUIHandle(ByVal lContext As Long) As Long
    If Not mfrmMain Is Nothing Then
        GetUIHandle = mfrmMain.hwnd
    End If
End Function

Public Function DisplayUI(ByVal lContext As Long) As Long
#If InProc = 0 Then
    DisplayUI = kFailure
    If mfrmMain Is Nothing Then Exit Function

    mfrmMain.Show
    DoEvents
    If mlError Then Err.Raise mlError
    
    mfrmMain.SetFocus
    DisplayUI = kSuccess
#Else
    DisplayUI = EFW_CT_MODALEXIT
#End If
End Function

Public Function ShowUI(ByVal lContext As Long) As Long
#If InProc = 0 Then
    ShowUI = kFailure

    If Not mfrmMain Is Nothing Then
        mfrmMain.Show
        DoEvents
    End If
#End If

    ShowUI = kSuccess
End Function

Public Function MinimizeUI(ByVal lContext As Long) As Long
    MinimizeUI = kFailure

    If Not mfrmMain Is Nothing Then
#If InProc = 0 Then
        If gbActiveChildObjects(mfrmMain.moSotaObjects) Then Exit Function
#End If
        mfrmMain.WindowState = vbMinimized
    End If

    MinimizeUI = kSuccess
End Function

Public Function RestoreUI(ByVal lContext As Long) As Long
    RestoreUI = kFailure

    If Not mfrmMain Is Nothing Then
        mfrmMain.WindowState = vbNormal
    End If

    RestoreUI = kSuccess
End Function

Public Function HideUI(ByVal lContext As Long) As Long
#If InProc = 0 Then
    HideUI = kFailure

    If Not mfrmMain Is Nothing Then
        If gbActiveChildObjects(mfrmMain.moSotaObjects) Then Exit Function
        mfrmMain.Hide
    End If
#End If

    HideUI = kSuccess
End Function

Public Function QueryShutDown(lContext As Long) As Long
    QueryShutDown = kFailure

    If Not mfrmMain Is Nothing Then
        If gbActiveChildObjects(mfrmMain.moSotaObjects) Then Exit Function
    End If

    QueryShutDown = kSuccess
End Function

Public Function UnloadUI(ByVal lContext As Long) As Long
    UnloadUI = kFailure

    If Not mfrmMain Is Nothing Then
        If miShutDownRequester = kFrameworkShutDown Then
            Unload mfrmMain
            If mfrmMain.bCancelShutDown Then Exit Function
            Set mfrmMain = Nothing
        Else
          If mlError Then mfrmMain.PerformCleanShutDown
        End If
    End If

    UnloadUI = kSuccess
End Function

Public Function TerminateObject(ByVal lContext As Long) As Long

    TerminateObject = kFailure

    DefaultTerminateObject Me
    
    Set mfrmMain = Nothing
    
    TerminateObject = kSuccess
End Function

Public Property Get lUIActive() As Long
    lUIActive = mlUIActive
End Property
Public Property Let lUIActive(lNewActive As Long)
    mlUIActive = lNewActive
End Property

Public Property Get lRunFlags() As Long
    lRunFlags = mlRunFlags
End Property
Public Property Let lRunFlags(ltRunFlags As Long)
    mlRunFlags = ltRunFlags
End Property

Public Sub NavigateTo(ByVal xml As String)
'+++ VB/Rig Skip +++
'*******************************************************************************
'    Desc: Exposed method for object's navigation
'
'   Parms: xml - XML string containing context.
'
' Returns: N/A
'*******************************************************************************
On Error Resume Next

    DrillAround GetFromXML(xml, "{Element Name Goes Here}")
    ' i.e. DrillAround GetFromXML(xml, "GLAcctNo")
    
    ' Exit and clear error buffer
    Err.Clear
    
End Sub

Public Sub DrillAround(sID As String)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*******************************************************************************
'    Desc: DrillAround will work like the Maintenance form is being
'          invoked from the launch pad, with the exception that an ID
'          may be passed in.
'   Parms: sID = ID to be Displayed/Viewed
' Returns: N/A
'*******************************************************************************
On Error GoTo ExpectedErrorRoutine

  If mfrmMain Is Nothing Then
      If moFramework.LoadUI(Me) = kFailure Then GoTo ShutMeDown
      If moFramework.DisplayUI(Me) = kFailure Then GoTo ShutMeDown
  End If
     
  If Len(Trim(sID)) > 0 Then
    ' {Form specific logic goes here}
    ' i.e. mfrmMain.glaGLAcct = sID
    ' i.e. mfrmMain.VMIsValidKey
  End If
  
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
  Exit Sub
  
ShutMeDown:
  miShutDownRequester = kUnloadSelfShutDown
  moFramework.UnloadSelf EFW_TF_MANSHUTDN
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
  Exit Sub
  
ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DrillAround", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


Public Property Get bPeriodEnd() As Boolean
'+++ VB/Rig Skip +++
    bPeriodEnd = mbPeriodEnd
End Property

Public Property Let bPeriodEnd(bSetPeriodEnd As Boolean)
'+++ VB/Rig Skip +++
    mbPeriodEnd = bSetPeriodEnd
End Property

Public Function lGetSettings(ByRef sSetting, ByRef sSelect, Optional oResponse As Variant) As Long
'************************************************************************************
'      Desc: Retrieve the report settings as a string.
'     Parms: sSetting - Output; String of saved setting id/setting key pairs.
'            sSelect - Output; String of saved setting key/select info pairs.
'            oResponse as Variant - Optional object used for debugging
'   Returns: kSuccess; kFailure
'            sSetting - Each setting is a string and a key. Each string and key is
'            separated by a tab (vbTab). Each setting is delimited by a CRLF (vbCrLf).
'            The first setting in the list is the most recently used by the user.
'            If no settings class exists, then empty string " is returned.
'            If error, then empty string " is returned.
'            sSelect - Each setting is a key and selection row information.
'            Each row is separated by a semicolon. Values within a row
'            are separated by commas.  Each row contains setting key,
'            caption, operator, start value, end value.  E.g.,
'            <setting key>, <caption>, <opertor>,<start value>, <end value>;
'            <setting key>, <caption>, <opertor>,<start value>, <end value>.
'            If no settings, then empty string (") is returned.
'   Assumes: Settings class proc Initialize has been run.
'            Selection control proc bInitSelect has been run.
'************************************************************************************
    lGetSettings = kFailure

    On Error Resume Next
    sSetting = mfrmMain.moSettings.sGetSettings()
    If Err <> 0 Then
        sSetting = ""
    End If
    
    sSelect = mfrmMain.moSelect.sGetSettings()
    If Err <> 0 Then
        sSelect = ""
    End If
    
    lGetSettings = kSuccess
End Function

Public Sub GenerateReport(iFileType As Integer, sFileName As String, _
    Optional lSettingKey As Variant, Optional sSelect As Variant, _
    Optional oResponse As Variant)
'************************************************************************************
'      Desc: Generate web report file by calling HandleToolbarClick.
'     Parms: iFileType as Integer - File type to create (0 = HTML, 1 = RPT)
'            sFileName as String - File name to create (including path)
'            lSettingKey as Variant - Optional; setting selected by user
'            sSelect as Variant - Optional; selection grid values
'            oResponse as Variant - Optional object used for debugging
'************************************************************************************
    '    Get the select, sort, and options for given setting.
    If Not IsMissing(lSettingKey) Then
        If lSettingKey <> 0 Then
            mfrmMain.moSettings.bLoadSettingsByKey (CLng(lSettingKey))
        End If
    End If
    
    '   Put Web Client Selection Grid values on Web Server Selection Grid
    If Not IsMissing(sSelect) Then
        On Error Resume Next    '   if selection grid does not exist
        mfrmMain.moSelect.lPutSelectGrid sSelect
        On Error GoTo 0     '   normal error processing
    End If
    
    mfrmMain.HandleToolbarClick kTbPreview, iFileType, sFileName
End Sub


Public Function sGetSelection(Optional oResponse As Variant) As String
'************************************************************************************
'      Desc: Retrieve the report selection grid information as a string.
'     Parms: oResponse as Variant - Optional object used for debugging
'   Returns: String of selection grid information.
'            Assumes: Selection Grid's proc bInitSelect has been run.
'************************************************************************************
    On Error Resume Next
    sGetSelection = mfrmMain.moSelect.sGetSelection()
    If Err <> 0 Then
        sGetSelection = ""
    End If
End Function

