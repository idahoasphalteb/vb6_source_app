VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#105.0#0"; "sotatbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#45.0#0"; "sotasbar.ocx"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{BC90D6A3-491E-451B-ADED-8FABA0B8EE36}#41.0#0"; "sotadropdown.ocx"
Begin VB.Form frmStockAvail 
   Caption         =   "Out of Stock Alternatives"
   ClientHeight    =   6885
   ClientLeft      =   -60
   ClientTop       =   3930
   ClientWidth     =   10140
   HelpContextID   =   17775893
   Icon            =   "sozdj104.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6885
   ScaleWidth      =   10140
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraOptions 
      Caption         =   "For Selected Item/Warehouse"
      Height          =   1455
      Left            =   120
      TabIndex        =   3
      Top             =   4920
      Width           =   9855
      Begin VB.CommandButton cmdAction 
         Caption         =   "&Use Available"
         Enabled         =   0   'False
         Height          =   375
         Index           =   3
         Left            =   240
         TabIndex        =   21
         Top             =   840
         WhatsThisHelpID =   17775894
         Width           =   1215
      End
      Begin VB.CommandButton cmdAction 
         Caption         =   "&Accept"
         Default         =   -1  'True
         Height          =   375
         Index           =   0
         Left            =   240
         TabIndex        =   20
         Top             =   360
         WhatsThisHelpID =   17775895
         Width           =   1215
      End
      Begin VB.CommandButton cmdAction 
         Caption         =   "Drop &Ship"
         Enabled         =   0   'False
         Height          =   375
         Index           =   1
         Left            =   4440
         TabIndex        =   19
         Top             =   360
         WhatsThisHelpID =   17775896
         Width           =   1215
      End
      Begin VB.CommandButton cmdAction 
         Caption         =   "&Delete"
         Height          =   375
         Index           =   2
         Left            =   4440
         TabIndex        =   18
         Top             =   840
         WhatsThisHelpID =   17775897
         Width           =   1215
      End
      Begin VB.Label lblButtonsCap 
         AutoSize        =   -1  'True
         Caption         =   "Delete SO Line"
         Height          =   195
         Index           =   5
         Left            =   5760
         TabIndex        =   25
         Top             =   920
         Width           =   1080
      End
      Begin VB.Label lblButtonsCap 
         AutoSize        =   -1  'True
         Caption         =   "Ship Direct From Vendor"
         Height          =   195
         Index           =   4
         Left            =   5760
         TabIndex        =   24
         Top             =   440
         Width           =   1725
      End
      Begin VB.Label lblButtonsCap 
         AutoSize        =   -1  'True
         Caption         =   "Save Line - Reduce Qty To Available "
         Height          =   195
         Index           =   2
         Left            =   1560
         TabIndex        =   23
         Top             =   915
         Width           =   2685
      End
      Begin VB.Label lblButtonsCap 
         AutoSize        =   -1  'True
         Caption         =   "Save Line - As Selected "
         Height          =   195
         Index           =   0
         Left            =   1560
         TabIndex        =   22
         Top             =   435
         Width           =   1755
      End
   End
   Begin VB.Frame fraLotTran 
      Caption         =   "Alternatives"
      Height          =   3135
      Left            =   120
      TabIndex        =   1
      Top             =   1680
      Width           =   9855
      Begin SOTADropDownControl.SOTADropDown ddnItemSub 
         Height          =   315
         Left            =   1440
         TabIndex        =   17
         Top             =   660
         WhatsThisHelpID =   17775898
         Width           =   3015
         _ExtentX        =   5318
         _ExtentY        =   556
         BackColor       =   -2147483633
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Text            =   "ddnItemSub"
         Sorted          =   0   'False
      End
      Begin VB.OptionButton optWhseSub 
         Caption         =   "Substitute"
         Enabled         =   0   'False
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   6
         Top             =   720
         WhatsThisHelpID =   17775899
         Width           =   1215
      End
      Begin VB.OptionButton optWhseSub 
         Caption         =   "Item Ordered"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Value           =   -1  'True
         WhatsThisHelpID =   17775900
         Width           =   1215
      End
      Begin FPSpreadADO.fpSpread grdStockStatus 
         Height          =   1935
         Left            =   120
         TabIndex        =   2
         Top             =   1080
         WhatsThisHelpID =   17775901
         Width           =   9600
         _Version        =   524288
         _ExtentX        =   16933
         _ExtentY        =   3413
         _StockProps     =   64
         BackColorStyle  =   1
         ColsFrozen      =   1
         DisplayRowHeaders=   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GrayAreaBackColor=   -2147483633
         MaxCols         =   8
         OperationMode   =   2
         RowHeaderDisplay=   0
         ScrollBarExtMode=   -1  'True
         SelectBlockOptions=   0
         SpreadDesigner  =   "sozdj104.frx":23D2
         AppearanceStyle =   0
      End
      Begin VB.Label lblItemID 
         Height          =   255
         Index           =   2
         Left            =   4560
         TabIndex        =   13
         Top             =   720
         Width           =   3120
      End
      Begin VB.Label lblItemID 
         Caption         =   "xxxxxxxxxxxx"
         Height          =   255
         Index           =   1
         Left            =   4560
         TabIndex        =   12
         Top             =   360
         Width           =   3120
      End
      Begin VB.Label lblItemID 
         Caption         =   "xxxxxxxxxxxx"
         Height          =   255
         Index           =   0
         Left            =   1440
         TabIndex        =   4
         Top             =   360
         Width           =   3000
      End
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   10140
      _ExtentX        =   17886
      _ExtentY        =   741
      Style           =   6
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   6495
      WhatsThisHelpID =   73
      Width           =   10140
      _ExtentX        =   17886
      _ExtentY        =   688
      BrowseVisible   =   0   'False
   End
   Begin NEWSOTALib.SOTANumber txtQtyOrdered 
      Height          =   255
      Left            =   5730
      TabIndex        =   26
      Top             =   1290
      Visible         =   0   'False
      WhatsThisHelpID =   17775904
      Width           =   1905
      _Version        =   65536
      _ExtentX        =   3360
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      sBorder         =   0
      mask            =   "<ILH>###|,###|,###|,###|,##<ILp0>#"
      text            =   "              0"
      sIntegralPlaces =   15
      sDecimalPlaces  =   0
   End
   Begin VB.Label lblQtyOrdered 
      Caption         =   "Label1"
      Height          =   255
      Left            =   1320
      TabIndex        =   27
      Top             =   1320
      Width           =   1695
   End
   Begin VB.Label lblWarehouseID 
      Caption         =   "Warehouse Name - short description"
      Height          =   195
      Index           =   1
      Left            =   3480
      TabIndex        =   16
      Top             =   960
      Width           =   4200
   End
   Begin VB.Label lblCustomerName 
      Caption         =   "Customer Company Name - short description"
      Height          =   195
      Index           =   1
      Left            =   3480
      TabIndex        =   15
      Top             =   600
      Width           =   4200
   End
   Begin VB.Label lblUOM 
      Caption         =   "UOM"
      Height          =   195
      Left            =   3480
      TabIndex        =   14
      Top             =   1320
      Width           =   840
   End
   Begin VB.Label lblWarehouseID 
      Caption         =   "Warehouse Name"
      Height          =   195
      Index           =   0
      Left            =   1320
      TabIndex        =   11
      Top             =   960
      Width           =   2040
   End
   Begin VB.Label lblCustomerName 
      Caption         =   "Customer Company"
      Height          =   195
      Index           =   0
      Left            =   1320
      TabIndex        =   10
      Top             =   600
      Width           =   2160
   End
   Begin VB.Label lblQty 
      AutoSize        =   -1  'True
      Caption         =   "Qty Ordered:"
      Height          =   195
      Left            =   240
      TabIndex        =   9
      Top             =   1320
      Width           =   900
   End
   Begin VB.Label lblWarehouse 
      AutoSize        =   -1  'True
      Caption         =   "Warehouse:"
      Height          =   195
      Left            =   240
      TabIndex        =   8
      Top             =   960
      Width           =   870
   End
   Begin VB.Label lblCustomer 
      AutoSize        =   -1  'True
      Caption         =   "Ship-To:"
      Height          =   195
      Left            =   240
      TabIndex        =   7
      Top             =   600
      Width           =   600
   End
End
Attribute VB_Name = "frmStockAvail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    
    
Private Const kColWhse = 1
Private Const kColQtyAvailable = 2
Private Const kColQtyOnHand = 3
Private Const kColReplPosition = 4
Private Const kColSurplusStock = 5
Private Const kMaxCols = 5
Private Const kOtherWhse = 0
Private Const kSubstitute = 1
Private Const kDtlCtlIndex = 1

Private moClass             As Object

Private moDMGrid            As clsDmGrid            ' data manager
Private moGM                As New clsGridMgr       ' Grid Manager
Private moContextMenu       As New clsContextMenu   ' context menu


Private mscompanyid         As String               ' Company ID
Private miOldFormHeight As Long
Private miOldFormWidth As Long
Private miMinFormHeight As Long
Private miMinFormWidth As Long

Private mlItemKey           As Long                 ' original ordered Item key
Private msItemID            As String
Private msItemDesc          As String
Private dOrigQtyAvail       As Double
Private mbContractBlkt      As Boolean
Private mdQtyAvail          As Double
Private mbQtyAvailChng      As Boolean
Private mdQtyOpen           As Double
Private mbIntegrateWithPO   As Boolean
Private miDecPlaces         As Integer
Private mlUOMKey            As Long
Private msUOMID             As String
Private msShipToID          As String
Private msShipToName        As String
Private msWhseID            As String
Private msWhseDesc          As String
Private mbSelectedSubst     As Boolean
Private mdQtyOrd            As Double
Private miAction            As Integer
Private mbAllowDropShip     As Boolean
Private mbAllowInvtSubst    As Boolean

Public moSotaObjects        As Collection

Public Property Get Action() As Integer
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Action = miAction
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Action", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let AllowDropShip(ByVal bAllowDropShip As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mbAllowDropShip = bAllowDropShip
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "AllowDropShip", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let AllowInvtSubst(ByVal bAllowInvtSubst As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mbAllowInvtSubst = bAllowInvtSubst
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "AllowInvtSubst", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let ContractBlanketLine(ByVal bValue As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mbContractBlkt = bValue
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ContractBlanketLine", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property


Public Property Let DecPlaces(ByVal iDecPlaces As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    miDecPlaces = iDecPlaces
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DecPlaces", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let IntegrateWithPO(ByVal bIntegrateWithPO As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mbIntegrateWithPO = bIntegrateWithPO
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "IntegrateWithPO", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let ItemKey(ByVal lItemKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlItemKey = lItemKey
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ItemKey", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let ItemID(ByVal sItemID As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    msItemID = sItemID
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ItemID", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get ItemID() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    ItemID = msItemID
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ItemID", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let ItemDesc(sItemDesc As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    msItemDesc = sItemDesc
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ItemDesc", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let QtyAvail(ByVal dQtyAvail As Double)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mdQtyAvail = dQtyAvail
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "QtyAvail", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get QtyAvail() As Double
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    QtyAvail = mdQtyAvail
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "QtyAvail", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get QtyOrd() As Double
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    QtyOrd = mdQtyOrd
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "QtyOrd", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let QtyAvailChng(ByVal bQtyAvailChng As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mbQtyAvailChng = bQtyAvailChng
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "QtyAvailChng", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let QtyOpen(ByVal dQtyOpen As Double)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mdQtyOpen = dQtyOpen
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "QtyOpen", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get QtyOpen() As Double
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    QtyOpen = mdQtyOpen
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "QtyOpen", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let ShipToID(ByVal sShipToID As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    msShipToID = sShipToID
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ShipToID", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let ShipToName(ByVal sShipToName As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    msShipToName = sShipToName
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ShipToName", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let UOMKey(ByVal lUOMKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlUOMKey = lUOMKey
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "UOMKey", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let UOMID(ByVal sUOMID As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    msUOMID = sUOMID
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "UOMID", VBRIG_IS_FORM                                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let WhseID(ByVal sWhseID As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    msWhseID = sWhseID
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WhseID", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get WhseID() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    WhseID = msWhseID
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WhseID", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let WhseDesc(ByVal sWhseDesc As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    msWhseDesc = sWhseDesc
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WhseDesc", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get SelectedSubst() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    SelectedSubst = mbSelectedSubst
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SelectedSubst", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'***********************************************************************
' Desc: Bind Right Click Context menu
'***********************************************************************
    Set moContextMenu = New clsContextMenu      'Instantiate Context Menu Class

    With moContextMenu
        .Bind "*APPEND", grdStockStatus.hWnd    'add extra item to the context menu
'        .Bind "*DRILL", grdStockStatus.hwnd
         Set .Form = frmStockAvail
        .Init                                   'Init will set properties of Winhook control
     
    End With

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Function CMAppendContextMenu(ctl As Object, hmenu As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    AppendMenu hmenu, MF_ENABLED, 100, gsBuildString(kIMReplPosition, moClass.moAppDB, moClass.moSysSession)
    AppendMenu hmenu, MF_SEPARATOR, 0, ""


    
Exit Function

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CMAppendContextMenu", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Public Function CMMenuSelected(ctl As Object, lTaskID As Long)
On Error GoTo ExpectedErrorRoutine

    Dim sItemID As String
    Dim oDdn As Object
    
        CMMenuSelected = False
        
        Select Case lTaskID
            Case 100
                                                
                Set oDdn = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                 kclsReplPosInq, ktskReplPosInqDDN, kDDRunFlags, kContextDD)
                'get the ItemID and WhseID for the selected item
                If optWhseSub(kSubstitute).Value Then
                    sItemID = ddnItemSub.Text
                Else
                    sItemID = lblItemID(0).Caption
                End If
                
                'set up the Replenish Position parameters
                With oDdn
                    .sItemID = sItemID
                    .sWhseID = Trim$(gsGridReadCellText(grdStockStatus, grdStockStatus.ActiveRow, kColWhse))
                    .iDecimalPlaces = miDecPlaces
                    .lUOMKey = mlUOMKey
                    .DrillDown
                End With

                                
            Case 10000 'for future use when we add the standard Item Context Menu Options
                'Do Nothing
        End Select
    
        CMMenuSelected = True
    
    Exit Function
    
    
ExpectedErrorRoutine:
    Screen.MousePointer = vbDefault
    Exit Function
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CMMenuSelected", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Sub grdStockStatus_DblClick(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    
    If Row <> 0 Then
        CMMenuSelected grdStockStatus, 100
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdStockStatus_DblClick", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub CMDrillDownSelected(oCtl As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************************
'      Desc: Called the default drill down. Used when default 'Drill Down' menu selected.
'     Parms: oCtl:   The control that received the right mouse button down message.
'   Returns: NONE
'************************************************************************************
    Dim sWhere As String
    
    If oCtl Is grdStockStatus Then
        If grdStockStatus.ActiveRow > 0 Then
            'DoDrillDown grdStockStatus.ActiveRow
        End If
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CMDrillDownSelected", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub




Public Sub LoadGrid(lItemKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'**********************************************************
'Customize: customize the query used by DMGrid
'**********************************************************

    Dim sSelect             As String
    Dim sTablesUsed         As String
    Dim iNumTables          As Integer
    Dim sInsert             As String
    Dim sDeleteTable        As String
    Dim sCreateTable        As String
    Dim fTotalQtyOnHand     As Double
    Dim fTotalQtyAvailable  As Double
    Dim sStockUOM           As String
    Dim lStockUOMKey        As Long
    Dim sDropTable          As String
    Dim sMask               As String
    Dim lRow                As Long
    Dim lCol                As Long
    Dim lRetVal             As Long
    Dim lRowIndex           As Long
    Dim dQuantity           As Double
    Dim dConvrtFactor       As Double
    Dim vQty                As Variant
    
    Const kSPRetVal = 50
    
    
    Screen.MousePointer = vbHourglass
    
    ' Create table
    #If Not RPTDEBUG = 1 Then
        sCreateTable = "SELECT * INTO #timStkStatusInqWrk FROM timStkStatusInqWrk WHERE 1 = 2"
        moClass.moAppDB.ExecuteSQL sCreateTable
    #End If
    
    sSelect = "SELECT timInventory.ItemKey, timInventory.WhseKey, timWarehouse.CompanyID"
    sSelect = sSelect & " FROM timInventory"
    sSelect = sSelect & " JOIN timWarehouse"
    sSelect = sSelect & " ON timInventory.WhseKey = timWarehouse.WhseKey"
    sSelect = sSelect & " AND timWarehouse.Transit <> 1"
    sSelect = sSelect & " AND timWarehouse.CompanyID = " & gsQuoted(mscompanyid)
    sSelect = sSelect & " AND timInventory.ItemKey = " & lItemKey
        
    #If RPTDEBUG = 1 Then
        sInsert = "INSERT INTO timStkStatusInqWrk (ItemKey, WhseKey, CompanyID) " & sSelect
    #Else
        sInsert = "INSERT INTO #timStkStatusInqWrk (ItemKey, WhseKey, CompanyID) " & sSelect
    #End If
    
    On Error Resume Next
    
    ' Insert into Work Table
    moClass.moAppDB.ExecuteSQL sInsert
    
    If Err.Number <> 0 Then
        Screen.MousePointer = vbDefault
        giSotaMsgBox Me, moClass.moSysSession, kmsgProc, sInsert
        Exit Sub
    End If
    On Error GoTo VBRigErrorRoutine
    
    ' Execute SP
    lRetVal = 0
    With moClass.moAppDB
        .SetInParamStr (mscompanyid)
        .SetOutParam sStockUOM
        .SetOutParam lRetVal
        On Error Resume Next
        .ExecuteSP ("spsoStockAlternatives")
        
        If Err.Number <> 0 Then
            lRetVal = Err.Number
        Else
            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
            'errors after each attempt.  After storing all return values in local variables OR
            'encountering an error, release parameters.
            sStockUOM = .GetOutParam(2)
            lRetVal = .GetOutParam(3)
        End If
                
    End With
    
    moClass.moAppDB.ReleaseParams
    If lRetVal <> 0 Then
        Screen.MousePointer = vbDefault
        If lRetVal = kSPRetVal Then
            giSotaMsgBox Me, moClass.moSysSession, kIMmsgOverflow
        End If
    End If
    
    moDMGrid.Refresh
    moDMGrid.SortByColumn kColWhse
    
    'if the grid display multiple warehouse, move the original SO selected
    'warehouse to the top of the grid, and remove all the warehouse with qty
    'available less than or equal to 0
    
    If optWhseSub(kOtherWhse).Value = True Then
        If grdStockStatus.DataRowCnt > 1 Then
            For lRow = 1 To grdStockStatus.DataRowCnt
                'move the selected warehouse to the 1st row when not using subst item
                If Trim(LCase(gsGetValidStr(gsGridReadCell(grdStockStatus, lRow, kColWhse)))) = Trim(LCase(lblWarehouseID(0).Caption)) Then
                    moDMGrid.MoveGridRow lRow, 1
                    
                    'if selected SO Line have QtyBO > 0 then need to update the qty avail to true qty avail
                    If mbQtyAvailChng Then
                        gGridUpdateCell grdStockStatus, 1, kColQtyAvailable, CStr(dOrigQtyAvail)
                    End If
                    
                    Exit For
                    
                End If
                
            Next lRow
        End If
        
        'lRowIndex have to be 2 since we will not filter qty avail on the selected warehouse
        lRowIndex = 2
        
    Else
        lRowIndex = 1
        
    End If
    
    'the 1st row always get focus, therefore the grid must to be filter from bottom up
    For lRow = grdStockStatus.DataRowCnt To lRowIndex Step -1

        'remove row with qty avail <=0
        If gsGetValidStr(gsGridReadCell(grdStockStatus, lRow, kColQtyAvailable)) <= 0 Then
            gGridDeleteRow grdStockStatus, lRow
        End If
        
    Next lRow
    
    'if the stock UOM is not the same as SO UOM, convert it to SO UOM for display
    If LCase(Trim(sStockUOM)) <> LCase(Trim(msUOMID)) And grdStockStatus.DataRowCnt <> 0 Then
        dQuantity = 1
        dConvrtFactor = 1
        
        lStockUOMKey = glGetValidLong(moClass.moAppDB.Lookup("StockUnitMeasKey", "timItem", "ItemKey = " & lItemKey))
        dConvrtFactor = gdGetQtyUOMConvertFactor(moClass, lItemKey, mlUOMKey, lStockUOMKey)
             
        For lRow = 1 To grdStockStatus.DataRowCnt
            For lCol = kColQtyAvailable To kColReplPosition
                grdStockStatus.GetText lCol, lRow, vQty
                gGridUpdateCell grdStockStatus, lRow, lCol, CStr(vQty * dConvrtFactor)
            Next lCol
        Next lRow

    End If

    
    'disable the Accept button if there is no data in the grid
    If grdStockStatus.ActiveRow = 0 Then
        gDisableControls cmdAction(kContinue), cmdAction(kDropShip), cmdAction(kBackOrd), cmdAction(kUseQtyAvail)
        cmdAction(kDeleLine).Default = True
    Else
        'always select the 1st row
        grdStockStatus_Click 1, 1
        gEnableControls cmdAction(kContinue)
        cmdAction(kContinue).Default = True
    
    End If


    #If Not RPTDEBUG = 1 Then
        sDropTable = sDropTable & "DROP TABLE #timStkStatusInqWrk"
        moClass.moAppDB.ExecuteSQL sDropTable
    #End If

    Screen.MousePointer = vbDefault
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "LoadGrid", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub BindGM()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moGM = New clsGridMgr

    With moGM
        Set .Grid = grdStockStatus
        Set .Form = Me
        'GridType:  kGridInquiryDD:will add default 'Drill Down'
        '           kGridLENoAppend:no default 'Drill Down'
        .GridType = kGridLENoAppend
        
        'Customize: if let grid handle the Sorting, pay attention to the primary key value,
        '           if the key is bind to nothing, it won't be synthconize between the grid and DMGrid.
        .GridSortEnabled = True
        .Init
    End With

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindGM", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
'**************************************************************************************
'Customize: use Data Manager Grid is the most common way to load data in the grid.
'           but it is optional.  this sub is only needed if you need to bind the DMGrid.
'**************************************************************************************
Private Sub BindForm()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moDMGrid = New clsDmGrid

    With moDMGrid
        #If RPTDEBUG = 1 Then
            .Table = "timStkStatusInqWrk"
        #Else
            .Table = "#timStkStatusInqWrk"
        #End If
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        Set .Form = Me
        Set .Grid = grdStockStatus
        .ReadOnly = True
        .UIType = kDmUISingle
        .NoAppend = True
        .UniqueKey = "ItemKey"
        'scopus 37574 - cej - set .PartialLoad to False
        .PartialLoad = False
        .BindColumn "ItemKey", 0, SQL_INTEGER
        .BindColumn "WhseID", kColWhse, SQL_CHAR
        .BindColumn "QtyAvailable", kColQtyAvailable, SQL_DECIMAL
        .BindColumn "QtyOnHand", kColQtyOnHand, SQL_DECIMAL
        .BindColumn "ReplPosition", kColReplPosition, SQL_DECIMAL
        .BindColumn "SurplusQty", kColSurplusStock, SQL_DECIMAL
        
        '.Init
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindForm", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    FormHelpPrefix = "SOZ"         '       Put your prefix here
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Private Property Get sMyName() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    sMyName = "frmStockAvail"
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "sMyName", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    WhatHelpPrefix = "SOZ"         '       Put your prefix here
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Private Sub FormatGrid()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'**************************************************************************************
'Customize: Format the grid.  usaually some general gid property, col width, col header
'           and special showing and hiding.
'**************************************************************************************
    Dim lCol        As Long
    Dim iQtyDP      As Integer
    
    
    'general Grid Property
    gGridSetColors grdStockStatus
    grdStockStatus.MaxCols = kMaxCols
    gGridSetProperties grdStockStatus, grdStockStatus.MaxCols, kGridInquiryDD

    'Colume Width. such as
    gGridSetColumnWidth grdStockStatus, kColWhse, 14
    gGridSetColumnWidth grdStockStatus, kColQtyOnHand, 15
    gGridSetColumnWidth grdStockStatus, kColQtyAvailable, 15
    gGridSetColumnWidth grdStockStatus, kColReplPosition, 15
    gGridSetColumnWidth grdStockStatus, kColSurplusStock, 15
    
    
    'Column Header, Such as
    gGridSetHeader grdStockStatus, kColWhse, gsBuildString(kIMWarehouse, moClass.moAppDB, moClass.moSysSession)
    gGridSetHeader grdStockStatus, kColQtyOnHand, gsBuildString(kQtyOnHandCol, moClass.moAppDB, moClass.moSysSession)
    gGridSetHeader grdStockStatus, kColQtyAvailable, gsBuildString(kIMQtyAvailable, moClass.moAppDB, moClass.moSysSession)
    gGridSetHeader grdStockStatus, kColReplPosition, gsBuildString(kIMReplPosition, moClass.moAppDB, moClass.moSysSession)
    gGridSetHeader grdStockStatus, kColSurplusStock, gsBuildString(kIMSurplusStockQty, moClass.moAppDB, moClass.moSysSession)
     
    'Get quantity decimals
    iQtyDP = giGetValidInt(frmSalesOrd.nbrQuantity.DecimalPlaces)

    gGridSetColumnType grdStockStatus, kColQtyOnHand, SS_CELL_TYPE_FLOAT, iQtyDP, 9
    gGridSetColumnType grdStockStatus, kColQtyAvailable, SS_CELL_TYPE_FLOAT, iQtyDP, 9
    gGridSetColumnType grdStockStatus, kColReplPosition, SS_CELL_TYPE_FLOAT, iQtyDP, 9
    gGridSetColumnType grdStockStatus, kColSurplusStock, SS_CELL_TYPE_FLOAT, iQtyDP, 9
    
    With grdStockStatus
        .Row = -1: .Col = -1
        '.DisplayRowHeaders = True               ' display row headers
        'gGridHideColumn grdStockStatus, 0
        .Col = 0
        .UserResizeCol = SS_USER_RESIZE_OFF      ' turn on column resize
    End With

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormatGrid", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ddnItemSub_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    'load the item short description for the selected substitute item
    lblItemID(2).Caption = gsGetValidStr(moClass.moAppDB.Lookup("ShortDesc", "timItemDescription", "ItemKey =" & ddnItemSub.ItemData(NewIndex)))
    
    'load the inventory information for the selected substitute item
    If optWhseSub(kSubstitute).Value = True Then
        gGridClearAll grdStockStatus                        'clear the grid
        LoadGrid ddnItemSub.ItemData(NewIndex)
        
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ddnItemSub_Click", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True.
'********************************************************************
    Dim iNavControl As Integer

    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
    End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        Case Else
            'Other KeyAscii routines.
    End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub Form_Load()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

      
    'Save the form size variable to use for resize later
    miOldFormHeight = Me.Height
    miMinFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormWidth = Me.Width
    
    'Setup form level variables
    mscompanyid = moClass.moSysSession.CompanyID
        
    'Setup Toolbar
    tbrMain.LocaleID = moClass.moSysSession.Language       'Language ID
    tbrMain.RemoveButton kTbPrint
    tbrMain.RemoveButton kTbFinishExit
    
    'initial status bar
    Set sbrMain.Framework = moClass.moFramework
    sbrMain.Status = SOTA_SB_INQUIRY
    
    Set moSotaObjects = New Collection
    
    FormatGrid                          ' format the grid
    BindForm                            ' bind DM object
    BindGM                              ' bind Grid Mgr
    BindContextMenu                     ' bind right mouse click menu
    
    'Scroll bars
    grdStockStatus.ScrollBars = ScrollBarsBoth
    Screen.MousePointer = 0
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Sub Init(oClass As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'Initialize the controls on the form.
Dim sSQL As String
Dim rs          As Object
Dim iIndex      As Long

    Set moClass = oClass
        
    txtQtyOrdered.DecimalPlaces = miDecPlaces

    'display the quantity open
    txtQtyOrdered.Value = mdQtyOpen   'this control is hidden because it only right alignment - used for masking.
    lblQtyOrdered.Caption = txtQtyOrdered.Text 'put the value into the label that can handle alignment

    'set the default screen information
    lblCustomerName(0).Caption = msShipToID
    lblCustomerName(1).Caption = msShipToName
    lblWarehouseID(0).Caption = msWhseID
    lblWarehouseID(1).Caption = msWhseDesc
    lblUOM.Caption = msUOMID
    lblItemID(0).Caption = msItemID
    lblItemID(1).Caption = msItemDesc
    gDisableControls optWhseSub(kSubstitute)
    gDisableControls ddnItemSub
    mlItemKey = mlItemKey

    'save the original qty available
    dOrigQtyAvail = mdQtyAvail

    'enabled the drop ship button when an item allows drop ship
    If mbAllowDropShip And mbIntegrateWithPO Then
        gEnableControls cmdAction(kDropShip)
    Else
        gDisableControls cmdAction(kDropShip)
    End If

    'enabled the use qty available button when selected item qty avail > 0 and not from a blanket contract
    If mdQtyAvail > 0 Then
        If mbContractBlkt = False Then
            gEnableControls cmdAction(kUseQtyAvail)
        Else
            gDisableControls cmdAction(kUseQtyAvail)
        End If
    Else
        gDisableControls cmdAction(kUseQtyAvail)
    End If
    
    If mbAllowInvtSubst Then
    
        'clear the control before loading data
        ddnItemSub.Clear

        'load all the items that could substitute the ordered item to the combo box
        sSQL = "SELECT a.ItemKey, a.ItemID " & _
               " FROM timItem a WITH (NOLOCK) , timItemUnitOfMeas b WITH (NOLOCK), timItemSubstitute c WITH (NOLOCK) " & _
               " WHERE a.ItemKey = c.SubstItemKey " & _
               " AND c.SubstItemKey = b.ItemKey " & _
               " AND c.ItemKey = " & mlItemKey & _
               " AND b.TargetUnitMeasKey = " & mlUOMKey & _
               " ORDER BY c.PrefOrder "

        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

        'only enabled the substitute option when there are item to substitute
        If Not rs.IsEOF Then
            If mbContractBlkt = False Then
                gEnableControls optWhseSub(kSubstitute)
            End If

            While Not rs.IsEOF
                ddnItemSub.AddItem gsGetValidStr(rs.Field("ItemID"))
                ddnItemSub.ItemData(iIndex) = CLng(gsGetValidStr(rs.Field("ItemKey")))
                iIndex = iIndex + 1
                rs.MoveNext
            Wend

            rs.Close
            Set rs = Nothing

            'select the 1st item in the list as default
            ddnItemSub.ListIndex = 0
        End If
    Else
        gDisableControls optWhseSub(kSubstitute)
    End If

     'Load the data to the grid base on the item key value
    LoadGrid mlItemKey
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Init", VBRIG_IS_FORM                                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'jlw - why unload?
    'If UnloadMode = 0 Then
        miAction = kCancelLine
    'End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
Dim iOrigWidth As Integer

    iOrigWidth = Me.Width

    'resize the form height and height all ctls on the screen (form height doesn't
    'get resize smaller than its original height)
    gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, fraLotTran, grdStockStatus

    'resize the form Width and Width all ctls on the screen (form Width doesn't
    'get resize smaller than its original Width)
    gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, fraLotTran, grdStockStatus

    miOldFormHeight = Me.Height
    miOldFormWidth = Me.Width

    Me.Width = iOrigWidth        'do not allow the screen width to resize



'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGM.UnloadSelf
    moDMGrid.UnloadSelf
    
    'remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    Set moSotaObjects = Nothing         ' Sage MAS 500 Child objects collection
    
    Set moGM = Nothing
    Set moDMGrid = Nothing
    Set moContextMenu = Nothing
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub grdStockStatus_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim sWhseID         As String
    Dim dGridQtyAvail   As Double
    Dim bEnoughQty      As Boolean
    Dim sItemID         As String
  
    
    'only execute the code when user not click on the header row
    If Row > 0 Then
    
        'get the warehouse id of the selected warehouse from the grid
        sWhseID = Trim(gsGridReadCell(grdStockStatus, grdStockStatus.ActiveRow, kColWhse))
        'get the item from their option selection
        sItemID = IIf(optWhseSub(kOtherWhse), lblItemID(0), ddnItemSub)
    
        'set bEnoughQty to true if qty avail is greater than qty ordered
        dGridQtyAvail = CDbl(Trim(gsGridReadCell(grdStockStatus, Row, kColQtyAvailable)))
        If dGridQtyAvail >= CDbl(txtQtyOrdered.Text) Then
            bEnoughQty = True
        End If
        mdQtyAvail = dGridQtyAvail ' Store the available qty for the selected item, so that it can be propogated back to SO screen to create backorder if required
                        
        'enabled Use Available button if qty for the order is more than qty available
        If Not bEnoughQty And dGridQtyAvail > 0 Then
            If mbContractBlkt = False Then
                gEnableControls cmdAction(kUseQtyAvail)
            Else
                gDisableControls cmdAction(kUseQtyAvail)
            End If
        Else
            gDisableControls cmdAction(kUseQtyAvail)
        End If
        
        If mbIntegrateWithPO Then
            If optWhseSub(kOtherWhse).Value = True Then
                'enabled the dropship button when ordered item allows dropship
                gEnableControl cmdAction(kDropShip), mbAllowDropShip
            Else
                'enabled the dropship button when subst item allows dropship
                gEnableControl cmdAction(kDropShip), gbGetValidBoolean(moClass.moAppDB.Lookup("AllowDropShip", "timItem", "ItemKey =" & ddnItemSub.ItemData(ddnItemSub.ListIndex)))
            End If
        End If
        
    Else
        moGM.Grid_Click Col, Row

    End If

 
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdStockStatus_Click", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub cmdAction_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim sWhseID             As String

    miAction = Index
    mbSelectedSubst = False

    If Index <> kDeleLine Then
        'replace ordered item id with selected substitute item
        If optWhseSub(kSubstitute).Value Then
            mbSelectedSubst = True
            msItemID = Trim(ddnItemSub.Text)
          
        End If

        'retrieve the warehouse id for selected item
        sWhseID = Trim(gsGridReadCell(grdStockStatus, grdStockStatus.ActiveRow, kColWhse))

        If sWhseID <> lblWarehouseID(0).Caption Then    'warehouse change
            msWhseID = sWhseID
        End If

    End If
   
    Me.Hide
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cmdAction_Click", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub optWhseSub_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    'clear the grid
    gGridClearAll grdStockStatus
    
    Select Case Index
        Case kOtherWhse
            'get inventory info of the ordered item base on item key
            LoadGrid mlItemKey
            gDisableControls ddnItemSub
        Case kSubstitute
            'get inventory info of the substitute item base on item key
            LoadGrid ddnItemSub.ItemData(ddnItemSub.ListIndex)
            gEnableControls ddnItemSub
    End Select
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "optWhseSub_Click", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    HandleToolbarClick Button
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    
    Select Case sKey
    
        Case kTbCancelExit
            'does not want to continue or do anything further
            miAction = kCancelLine
            moClass.miShutDownRequester = kFrameworkShutDown
            Unload Me
  
        Case kTbHelp
            'Run form help
            gDisplayFormLevelHelp Me
            
    End Select
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Get MyApp() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyApp = App
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyApp", VBRIG_IS_FORM                                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Get MyForms() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyForms = Forms
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyForms", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property


