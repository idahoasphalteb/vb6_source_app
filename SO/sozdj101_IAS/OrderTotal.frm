VERSION 5.00
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#117.0#0"; "SOTATbar.ocx"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Begin VB.Form frmOrderTotal 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Order Totals"
   ClientHeight    =   3255
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4200
   HelpContextID   =   17776212
   Icon            =   "OrderTotal.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3255
   ScaleWidth      =   4200
   StartUpPosition =   3  'Windows Default
   Begin NEWSOTALib.SOTAMaskedEdit txtTradeDiscPct 
      Height          =   255
      Left            =   3360
      TabIndex        =   17
      Top             =   1440
      WhatsThisHelpID =   17776213
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   450
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      sBorder         =   0
      text            =   "100.00 %"
   End
   Begin VB.CommandButton cmdSalesTax 
      Caption         =   "..."
      Height          =   285
      Left            =   3390
      TabIndex        =   9
      ToolTipText     =   "Sales Tax Detail"
      Top             =   1860
      WhatsThisHelpID =   17776215
      Width           =   285
   End
   Begin VB.CommandButton cmdFreight 
      Caption         =   "..."
      Height          =   285
      Left            =   3390
      TabIndex        =   4
      ToolTipText     =   "Allocate Freight"
      Top             =   1020
      WhatsThisHelpID =   17776216
      Width           =   285
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   15
      Left            =   1800
      TabIndex        =   15
      Top             =   2280
      Width           =   1600
   End
   Begin NEWSOTALib.SOTACurrency curSalesAmt 
      Height          =   285
      Left            =   1890
      TabIndex        =   1
      Top             =   600
      WhatsThisHelpID =   17776217
      Width           =   1395
      _Version        =   65536
      bShowCurrency   =   0   'False
      _ExtentX        =   2461
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      mask            =   "<HL><ILH>#|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "         0.00"
      dMinValue       =   -9999999999.99
      dMaxValue       =   9999999999.999
      sIntegralPlaces =   10
      sDecimalPlaces  =   2
      dAmount         =   0
   End
   Begin NEWSOTALib.SOTACurrency curFrtAmt 
      Height          =   285
      Left            =   1890
      TabIndex        =   3
      Top             =   1020
      WhatsThisHelpID =   17776218
      Width           =   1395
      _Version        =   65536
      bShowCurrency   =   0   'False
      _ExtentX        =   2461
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      mask            =   "<HL><ILH>#|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "         0.00"
      dMinValue       =   -9999999999.99
      dMaxValue       =   9999999999.999
      sIntegralPlaces =   10
      sDecimalPlaces  =   2
      dAmount         =   0
   End
   Begin NEWSOTALib.SOTACurrency curTradeDiscAmt 
      Height          =   285
      Left            =   1890
      TabIndex        =   6
      Top             =   1440
      WhatsThisHelpID =   17776219
      Width           =   1395
      _Version        =   65536
      bShowCurrency   =   0   'False
      _ExtentX        =   2461
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      mask            =   "<HL><ILH>#|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "         0.00"
      dMinValue       =   -999999999.99
      dMaxValue       =   9999999999.999
      sIntegralPlaces =   10
      sDecimalPlaces  =   2
      dAmount         =   0
   End
   Begin NEWSOTALib.SOTACurrency curSTaxAmt 
      Height          =   285
      Left            =   1890
      TabIndex        =   8
      Top             =   1860
      WhatsThisHelpID =   17776220
      Width           =   1395
      _Version        =   65536
      bShowCurrency   =   0   'False
      _ExtentX        =   2461
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      mask            =   "<HL><ILH>#|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "         0.00"
      dMinValue       =   -9999999999.99
      dMaxValue       =   9999999999.999
      sIntegralPlaces =   10
      sDecimalPlaces  =   2
      dAmount         =   0
   End
   Begin NEWSOTALib.SOTACurrency curTranAmt 
      Height          =   285
      Left            =   1890
      TabIndex        =   11
      Top             =   2370
      WhatsThisHelpID =   17776221
      Width           =   1395
      _Version        =   65536
      _ExtentX        =   2461
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      sBorder         =   0
      mask            =   "<HL> <ILH>#|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "         0.00"
      dMinValue       =   -9999999999.99
      dMaxValue       =   9999999999.999
      sIntegralPlaces =   10
      sDecimalPlaces  =   2
      dAmount         =   0
   End
   Begin NEWSOTALib.SOTACurrency curTranAmtHC 
      Height          =   285
      Left            =   1890
      TabIndex        =   13
      Top             =   2730
      WhatsThisHelpID =   17776222
      Width           =   1395
      _Version        =   65536
      _ExtentX        =   2461
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      sBorder         =   0
      mask            =   "<HL> <ILH>#|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "         0.00"
      dMinValue       =   -9999999999.99
      dMaxValue       =   9999999999.999
      sIntegralPlaces =   10
      sDecimalPlaces  =   2
      dAmount         =   0
   End
   Begin NEWSOTALib.SOTAMaskedEdit lblCurrIDDoc 
      Height          =   225
      Left            =   3360
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   2370
      WhatsThisHelpID =   17776223
      Width           =   465
      _Version        =   65536
      _ExtentX        =   820
      _ExtentY        =   397
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      bLocked         =   -1  'True
      sBorder         =   0
   End
   Begin NEWSOTALib.SOTAMaskedEdit lblCurrIDHC 
      Height          =   225
      Left            =   3360
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   2730
      WhatsThisHelpID =   17776224
      Width           =   465
      _Version        =   65536
      _ExtentX        =   820
      _ExtentY        =   397
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      bLocked         =   -1  'True
      sBorder         =   0
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   4200
      _ExtentX        =   7408
      _ExtentY        =   741
      Style           =   8
   End
   Begin VB.Label lblSalesTax 
      Caption         =   "Sales Tax"
      Height          =   225
      Left            =   120
      TabIndex        =   7
      Top             =   1890
      Width           =   1605
   End
   Begin VB.Label lblReturnTotal 
      Caption         =   "Order Total"
      Height          =   225
      Left            =   120
      TabIndex        =   10
      Top             =   2430
      Width           =   1605
   End
   Begin VB.Label lblFreight 
      Caption         =   "Freight"
      Height          =   225
      Left            =   120
      TabIndex        =   2
      Top             =   1050
      Width           =   1605
   End
   Begin VB.Label lblTradeDisc 
      Caption         =   "Trade Discount"
      Height          =   225
      Left            =   120
      TabIndex        =   5
      Top             =   1470
      Width           =   1605
   End
   Begin VB.Label lblAmount 
      Caption         =   "Sales Amount"
      Height          =   225
      Left            =   120
      TabIndex        =   0
      Top             =   630
      Width           =   1605
   End
End
Attribute VB_Name = "frmOrderTotal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************************
'     Name: Order Totals
'     Desc: Sales Order Totals
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: JLW 08-09-2004
'     Mods:
'************************************************************************************
Option Explicit

    'Private Variables of Form Properties
    Private moClass                      As Object                   'Class Reference
    Private mlRunMode                    As Long                     'Run Mode of Task
    Private mbCancelShutDown             As Boolean                  'Cancel Shutdown Flag
    Private miSecurityLevel              As Integer                  'Security Level
    Private mbLoadSuccess                As Boolean
    
    Private mbTrackTaxes                 As Boolean
    Private mdExchangeRate               As Double
    Private miDocQtyDecPlaces            As Integer
    Private mbReadOnly                   As Boolean
    Private miCustHold                   As Integer
    Private mbCheckCreditLimit           As Boolean
    Private mlCustKey                    As Long
    Private msCustID                     As String
    Private msCustName                   As String
    Private miTranType                   As Integer
    
    Private moSTax                       As Object
    Private moGrid                       As Object
    Private moDMGrid                     As clsDmGrid
    Private moChildGrid                  As Object
    Private moDMChildGrid                As clsDmGrid
    Private moDMform                     As clsDmForm
    Private moCredForm                   As Object
    
    'Public Form Variables
    Public moSotaObjects                 As New Collection           'Collection of Loaded Objects

    'Private Objects for This Project
    Private moContextMenu                As clsContextMenu       'Context Menu
  
    'Miscellaneous Variables
    Private miMousePointer               As Integer
    Private mbFromQueryUnload            As Boolean
    Private msDocCurrID                  As String
    Private msHomeCurrID                 As String

        
Const VBRIG_MODULE_ID_STRING = "OrderTotal.FRM"

'************************************************************************
'   Description:
'       Process all toolbar clicks (as well as HotKey shortcuts to
'       toolbar buttons).
'   Param:
'       sKey -  Token returned from toolbar.  Indicates what function
'               is to be executed.
'   Returns:
'************************************************************************
Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    'VB 5 does not automatically fire LostFocus event when pressing toolbar.
    Me.SetFocus
    DoEvents

    Select Case sKey
        Case kTbFinish, kTbFinishExit
            'The Finish button was pressed by the user.
            Me.Hide
        
        Case kTbHelp
            'The Help button was pressed by the user.
            gDisplayFormLevelHelp Me

               
        Case Else
            'Error processing
            'Give an error message to the user.
            'Unexpected Operation Value: {0}
            ResetMouse
            'giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation, _
                         CVar(sKey)
    End Select

    ResetMouse
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ResetMouse()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'***********************************************************************
'    Desc: Resets Mouse to Previous Value.
'   Parms: N/A
' Returns: N/A
'***********************************************************************
    Me.MousePointer = miMousePointer

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ResetMouse", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub



'************************************************************************
'   Description:
'       Returns Form Name for Debugging Information.
'   Param:
'       <none>
'   Returns:
'       Form Name
'************************************************************************
Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function
'************************************************************************
'   oClass contains the reference to the parent class object.  The form
'   needs this reference to use the public variables created within the
'   class object.
'************************************************************************
Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set oClass = moClass
    
    'Exit this property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = oNewClass

    'Exit this property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

'************************************************************************
'   Description:
'       Standard Shutdown Procedure.  Unload all child forms
'       and remove any objects created within this app.
'   Param:
'       <none>
'   Returns:
'************************************************************************
Public Sub PerformCleanShutDown()
    On Error GoTo ExpectedErrorRoutine
    
    If Not moGrid Is Nothing Then
        Set moGrid = Nothing
    End If
    
    If Not moChildGrid Is Nothing Then
        Set moChildGrid = Nothing
    End If
    
    If Not moDMGrid Is Nothing Then
        moDMGrid.UnloadSelf
        Set moDMGrid = Nothing
    End If
    
    If Not moDMChildGrid Is Nothing Then
        moDMChildGrid.UnloadSelf
        Set moDMChildGrid = Nothing
    End If
    
    If Not moSTax Is Nothing Then
        Set moSTax = Nothing
    End If
       
    If Not moClass Is Nothing Then
        Set moClass = Nothing
    End If
    
    If Not moDMform Is Nothing Then
        Set moDMform = Nothing
    End If
    
    If Not moCredForm Is Nothing Then
        Set moCredForm = Nothing
    End If
    
    If Not moSotaObjects Is Nothing Then
        Set moSotaObjects = Nothing
    End If
       
 
    TerminateControls Me
    
    'Exit this subroutine
    Exit Sub

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub






Private Sub curSTaxAmt_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim dTranAmt As Double
Dim dTranAmtHC As Double

    'Get the transaction amounts based on the new sales tax amount.  Sales tax is only enabled
    'when not tracking taxes (mbTrackTaxes = False)
    CalcOrderTotals curSTaxAmt.Amount, moGrid, mbTrackTaxes, mdExchangeRate, miHomeDigAfterDec, _
        curSalesAmt.DecimalPlaces, 0, 0, 0, 0, dTranAmtHC, dTranAmt, 0, 0, 0, 0, 0

    curTranAmt.Amount = dTranAmt
    curTranAmtHC.Amount = dTranAmtHC
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "curSTaxAmt_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mbCancelShutDown = False
    mbLoadSuccess = False

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True.
'   Parameters:
'           KeyCode, Shift - see VB reference Manual.
'
'   NOTE: THIS ROUTINE SHOULD CALL A GLOBAL SUBROUTINE TO HANDLE ALL THE
'         AVAILABLE HOTKEYS.
'********************************************************************
    'Determine if the user used the form-level keys and process them
    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            'Process the form-level key pressed
            gProcessFKeys Me, KeyCode, Shift
    End Select
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
'************************************************************************
'   Description:
'       Process Key Presses on the form.  NOTE: Key Preview of the form
'       should be set to True.
'   Param:
'       KeyAscii -  ASCII Key Code of Key Pressed.
'   Returns:
'************************************************************************
Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        
        Case Else
            'Other KeyAscii routines.
    End Select
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'******************************************
'* Set up the form.
'******************************************

Dim uCurrInfo As CurrencyInfo

    moClass.lUIActive = kChildObjectInactive

    'Get session defaults.
    mbEnterAsTab = moClass.moSysSession.EnterAsTab       'Enter Key like Tab Key
    msHomeCurrID = moClass.moSysSession.CurrencyID
                      
    'Set home currency control
    gbSetCurrCtls moClass, msHomeCurrID, uCurrInfo, curTranAmtHC
   
    With tbrMain
        .RemoveSeparator kTbFinishExit
        .RemoveButton kTbCancelExit
        .RemoveButton kTbPrint
    End With

    'Set the application's security level.
    'miSecurityLevel = giSetAppSecurity(moClass, tbrMain, _
                                       moDmFormCustRtrnShipment, moDmFormPendShipment)
    
    If mbTrackTaxes Then
        gEnableControls cmdSalesTax
        cmdSalesTax.Visible = True
        gDisableControls curSTaxAmt
        curSTaxAmt.Protected = True
    Else
        gDisableControls cmdSalesTax
        cmdSalesTax.Visible = False
        gEnableControls curSTaxAmt
        curSTaxAmt.Protected = False
    End If
    
    BindContextMenu     'Bind the Context Menu.
    
    mbLoadSuccess = True
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub ShowForm(ByVal dSalesAmt As Double, dFrtAmt As Double, ByVal dTradeDiscAmt As Double, _
    ByVal dTradeDiscPct As Double, dSTaxAmt As Double, dTranAmt As Double, dTranAmtHC As Double, _
    ByVal dExchangeRate, ByVal sDocCurrID, ByVal sCaption As String, ByVal bReadOnly, _
    ByVal bDisableFreight As Boolean, ByVal iDocQtyDecPlaces As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
Dim uCurrInfo As CurrencyInfo

    mdExchangeRate = dExchangeRate
    msDocCurrID = sDocCurrID
    miDocQtyDecPlaces = iDocQtyDecPlaces
            
    mbReadOnly = bReadOnly
        
    frmOrderTotal.Caption = sCaption
    
    'Set up the controls that are in document currency
    gbSetCurrCtls moClass, msDocCurrID, uCurrInfo, _
                      curSalesAmt, curFrtAmt, curTradeDiscAmt, curSTaxAmt, curTranAmt
    
    curSalesAmt.Amount = dSalesAmt
    curFrtAmt.Amount = dFrtAmt
    curTradeDiscAmt.Amount = -dTradeDiscAmt
    txtTradeDiscPct.Text = dTradeDiscPct & " %"
    curSTaxAmt.Amount = dSTaxAmt
    curTranAmt.Amount = dTranAmt
    curTranAmtHC.Amount = dTranAmtHC
    
    If moClass.moSysSession.CurrencyID <> msDocCurrID Then
        curTranAmtHC.Visible = True
        lblCurrIDHC.Visible = True
        lblCurrIDHC.Text = moClass.moSysSession.CurrencyID
        lblCurrIDDoc.Visible = True
        lblCurrIDDoc.Text = msDocCurrID
    Else
        curTranAmtHC.Visible = False
        lblCurrIDHC.Visible = False
        lblCurrIDDoc.Visible = False
    End If
    
    If bDisableFreight Then
        gDisableControls cmdFreight
    Else
        gEnableControls cmdFreight
    End If
    
    Me.Show vbModal
    
    'Pass back to the caller the amounts which can change
    dFrtAmt = curFrtAmt.Amount
    dSTaxAmt = curSTaxAmt.Amount
    dTranAmt = curTranAmt.Amount
    dTranAmtHC = curTranAmtHC.Amount
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ShowForm", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub Init(oClass As Object, oSTax As Object, oGrid As Object, oDmGrid As clsDmGrid, _
    oDmForm As clsDmForm, oCredForm As Object, _
    oSotaObjects As Object, bTrackTaxes As Boolean, iCustHold As Integer, _
    bCheckCreditLimit As Boolean, iTranType As Integer, lCustKey As Long, sCustID As String, _
    sCustName As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Set moClass = oClass
    Set moSTax = oSTax
    Set moGrid = oGrid
    Set moDMGrid = oDmGrid
    Set moDMform = oDmForm
    Set moCredForm = oCredForm
    Set moSotaObjects = oSotaObjects
    
    miCustHold = iCustHold
    mbCheckCreditLimit = bCheckCreditLimit
    miTranType = iTranType
    mlCustKey = lCustKey
    msCustID = sCustID
    msCustName = sCustName
    mbTrackTaxes = bTrackTaxes

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Init", VBRIG_IS_FORM                                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
'************************************************************************
'   Description:
'       If form is dirty, prompt for save.  Handle cancel of the
'       shutdown or process a normal shutdown.
'   Param:
'       Cancel -        set to True to cancel the form shutdown
'       UnloadMode -    flag indicating type of shutdown requested
'   Returns:
'************************************************************************
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error GoTo ExpectedErrorRoutine
 
    If UnloadMode = vbFormControlMenu Then
        Me.Hide
        Cancel = 1
    Else
        PerformCleanShutDown
    End If
   
    Exit Sub
    
ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub




Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'***************************************************
'  Instantiate the Context Menu Class.
'***************************************************
    Set moContextMenu = New clsContextMenu  'Instantiate Context Menu Class
    
    With moContextMenu
        Set .Form = frmOrderTotal
        .Init
    End With

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z"
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the SOTA identifier.
'************************************************************************
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    WhatHelpPrefix = "SOZ"

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Private Sub cmdFreight_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim dSTaxAmt As Double
Dim dFrtAmt As Double
Dim dTranAmt As Double
Dim dTranAmtHC As Double
    
    dSTaxAmt = curSTaxAmt.Amount
    dFrtAmt = curFrtAmt.Amount
       
    'Returns total StaxAmt and total Freight
    ShowFreightDialog moClass, mbTrackTaxes, mbReadOnly, moSTax, moGrid, moDMGrid, _
        kModuleSO, miDocQtyDecPlaces, curTranAmt.DecimalPlaces, _
        dSTaxAmt, dFrtAmt
        
    curSTaxAmt.Amount = dSTaxAmt
    curFrtAmt.Amount = dFrtAmt
        
    'Get the totals needed for the totals form
    CalcOrderTotals dSTaxAmt, moGrid, mbTrackTaxes, mdExchangeRate, miDocQtyDecPlaces, _
        curTranAmt.DecimalPlaces, 0, 0, 0, 0, dTranAmtHC, dTranAmt, 0, 0, 0, 0, 0
           
    curTranAmt.Amount = dTranAmt
    curTranAmtHC.Amount = dTranAmtHC
                
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cmdFreight_Click", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub cmdSalesTax_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim dTotalTaxAmt    As Double
    Dim dLineSTaxAmt    As Double
    
    If mbTrackTaxes Then
        moSTax.ShowTaxes True, kModeReadOnly, dTotalTaxAmt, dLineSTaxAmt, False
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cmdSalesTax_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    HandleToolbarClick Button

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Public Property Get bReadOnly() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
     bReadOnly = mbReadOnly

    'Exit this property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bReadOnly", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let bReadOnly(bNewReadOnly As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
     mbReadOnly = bNewReadOnly

    'Exit this property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bReadOnly", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    FormHelpPrefix = "SOZ"
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property



