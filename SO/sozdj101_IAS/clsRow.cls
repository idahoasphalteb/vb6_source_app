VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsRow"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'**********************************************************************
'     Name: clsRow
'     Desc: Contains the row number and flag for indication that the row needs to be "processed", i.e.,
'           the row is dirty since the user entered the row.  This is different than the data manager
'           dirty property, since the row can be dirty per the data manager, but nothing has changed
'           since the user entered the row, so when leaving the row, the row doesn't get reprocessed
'           (i.e. no check for out of stock, etc).
' Original: 07-26-04 Jenny Wilson
'     Mods: mm/dd/yy XXX
'**********************************************************************
Private mlRowNo As Long
Private mbProcessRowChange As Boolean


Public Property Let RowNo(lRowNo As Long)
    mlRowNo = lRowNo
End Property
    
Public Property Get RowNo() As Long
    RowNo = mlRowNo
End Property
 
Private Property Let ProcessRowChange(bProcessRowChange As Boolean)
    mbProcessRowChange = bProcessRowChange
End Property
    
Private Property Get ProcessRowChange() As Boolean
    ProcessRowChange = mbProcessRowChange
End Property
 
    
