VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#128.0#0"; "SOTATbar.ocx"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#131.0#0"; "EntryLookupControls.ocx"
Begin VB.Form frmCompItem 
   Caption         =   "Components"
   ClientHeight    =   5850
   ClientLeft      =   60
   ClientTop       =   3930
   ClientWidth     =   9675
   HelpContextID   =   17775888
   Icon            =   "sozdj102.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5850
   ScaleWidth      =   9675
   StartUpPosition =   2  'CenterScreen
   Begin EntryLookupControls.ItemLookup lkuItemID 
      Height          =   285
      Left            =   1560
      TabIndex        =   6
      Top             =   3240
      WhatsThisHelpID =   17777428
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   503
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      LookupMode      =   1
      BoundTable      =   "tsoSOLineCompItem"
      BoundColumn     =   "CompItemKey"
      EnabledLookup   =   -1  'True
      VisibleLookup   =   -1  'True
      IsSurrogateKey  =   -1  'True
      LookupID        =   "Item"
      ParentIDColumn  =   "ItemID"
      ParentKeyColumn =   "ItemKey"
      ParentTable     =   "timItem"
      IsForeignKey    =   -1  'True
      SQLReturnColsString=   "ItemID,lkuItemID,;ShortDesc,,;ItemKey,,;"
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtKitUOM 
      Height          =   285
      Left            =   5940
      TabIndex        =   5
      Top             =   600
      WhatsThisHelpID =   17775889
      Width           =   1215
      _Version        =   65536
      _ExtentX        =   2143
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtCompItem 
      Height          =   285
      Left            =   720
      TabIndex        =   2
      Top             =   600
      WhatsThisHelpID =   17775890
      Width           =   3855
      _Version        =   65536
      _ExtentX        =   6800
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   9675
      _ExtentX        =   17066
      _ExtentY        =   741
      Style           =   6
   End
   Begin FPSpreadADO.fpSpread grdCompItem 
      Height          =   4650
      Left            =   90
      TabIndex        =   3
      Top             =   1080
      WhatsThisHelpID =   17775892
      Width           =   9495
      _Version        =   524288
      _ExtentX        =   16748
      _ExtentY        =   8202
      _StockProps     =   64
      AllowCellOverflow=   -1  'True
      DisplayRowHeaders=   0   'False
      EditModeReplace =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ScrollBarExtMode=   -1  'True
      ScrollBars      =   2
      SpreadDesigner  =   "sozdj102.frx":23D2
      AppearanceStyle =   0
   End
   Begin VB.Label lblKitUOM 
      AutoSize        =   -1  'True
      Caption         =   "Kit UOM"
      Height          =   255
      Left            =   5280
      TabIndex        =   4
      Top             =   640
      Width           =   600
   End
   Begin VB.Label lblKitItem 
      AutoSize        =   -1  'True
      Caption         =   "Kit Item"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   640
      Width           =   525
   End
End
Attribute VB_Name = "frmCompItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************************************
'Date       Project     SE          Description of modification
'****************************************************************************************

    Option Explicit
    
    Private moClass                As Object                   'Class Reference
    
    Private WithEvents moDmKitComp As clsDmGrid
Attribute moDmKitComp.VB_VarHelpID = -1
    Private WithEvents moGMKitComp As clsGridMgr        'Grid Manager
Attribute moGMKitComp.VB_VarHelpID = -1
    
    Private mbLeaveCell As Boolean
    Private mbEditChange As Boolean
    
    Private ItemMgr             As New clsIMSItemMgr
    Private moContextMenu       As New clsContextMenu   'Context Menu
    
    Private msCompanyID         As String               'Company ID
    Private miOldFormHeight     As Long
    Private miOldFormWidth      As Long
    Private miMinFormHeight     As Long
    Private miMinFormWidth      As Long
    Private mbIsValidating      As Boolean
    Private miQtyDP             As Integer
    Private miCustomBTOKit      As Integer
    
    Private msWhseID            As String
    Private msCustID            As String
    Private msCustName          As String
    Private mlSOLineKey         As Long
    
    Private msItemID            As String
    Private mlItemKey           As Long
    Private mdQty               As Double
    
    Private mbReadOnly          As Boolean
    
           
    Private mbIsDirty           As Boolean
    
    
    ' *** RMM 5/2/01, Context menu project
    Private mbIMActivated As Boolean

Private Function bValidateCell(ByVal Col As Long, ByVal Row As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    If mbIsValidating Then Exit Function
    mbIsValidating = True

    bValidateCell = False 'Default to False
    mbLeaveCell = False
    
    'Used to track a change when user clicks on toolbar before tabbing off control
    mbEditChange = False

  
    Select Case Col
        Case kColSOCompItemID
            If Not bIsValidItemID(Row) Then
                lkuItemID.Text = msItemID
                gGridUpdateCell grdCompItem, Row, kColSOCompItemID, gsGetValidStr(msItemID)
                mbIsValidating = False
                Exit Function
            Else
                msItemID = lkuItemID.Text
                mlItemKey = lkuItemID.KeyValue
            End If
        Case kColSOCompItemQty
            If Not bIsValidQty(Row) Then
                gGridUpdateCell grdCompItem, Row, kColSOCompItemQty, gsGetValidStr(mdQty)
                mbIsValidating = False
                Exit Function
            Else
                mdQty = gdGetValidDbl(gsGridReadCell(grdCompItem, Row, Col))
            End If
    End Select
    
   
  
    
    mbIsValidating = False
    bValidateCell = True
    mbLeaveCell = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bValidateCell", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bIsValidQty(lCurrRow As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim dQty As Double

    bIsValidQty = False
    
    dQty = gdGetValidDbl(gsGridReadCell(grdCompItem, lCurrRow, kColSOCompItemQty))
    
    If dQty = mdQty Then
        bIsValidQty = True
        Exit Function
    End If
    
    If gdGetValidDbl(gsGridReadCell(grdCompItem, lCurrRow, kColSOCompItemQty)) <= 0 Then
    
        giSotaMsgBox Me, moClass.moSysSession, kmsgCannotEqual, "Quantity", "0 or less."
        Exit Function
        
    End If
    
    mbIsDirty = True

    bIsValidQty = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsValidQty", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bIsValidItemID(lCurrRow As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lRow As Long
Dim lItemKey As Long

    bIsValidItemID = False
    
    If Trim(lkuItemID.Text) = msItemID Then
        bIsValidItemID = True
        Exit Function
    End If
        
    If Not lkuItemID.IsValid Then
        giSotaMsgBox Me, moClass.moSysSession, kIMmsgInvItem
        Exit Function
    End If
    
    If Len(Trim(lkuItemID.Text)) = 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kIMKitCompValidate
        Exit Function
    End If
    
    mbIsDirty = True
            
    'Item may not exist on more than one row
    For lRow = 1 To grdCompItem.DataRowCnt

        'do not compare to selected row itself
        If lRow <> lCurrRow Then
            If glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColSOCompItemKey)) = lkuItemID.KeyValue Then
            'Display error message when there is duplicate item in the kit
                giSotaMsgBox Me, moClass.moSysSession, kIMmsgDuplicateItem
                Exit Function
            End If
         End If
    Next lRow
    
    gGridUpdateCell grdCompItem, lCurrRow, kColSOCompItemKey, gsGetValidStr(lkuItemID.KeyValue)
    gGridUpdateCell grdCompItem, lCurrRow, kColSOLineKey, gsGetValidStr(mlSOLineKey)
    
    If glGetValidLong(gsGridReadCell(grdCompItem, lCurrRow, kColSOCompKey)) = 0 Then
        gGridUpdateCell grdCompItem, lCurrRow, kColSOCompKey, glGetNextSurrogateKey(moClass.moAppDB, "tsoSOLineCompItem")
    End If
    
    If gdGetValidDbl(gsGridReadCell(grdCompItem, lCurrRow, kColSOCompItemQty)) <= 0 Then
        gGridUpdateCell grdCompItem, lCurrRow, kColSOCompItemQty, "1"
    End If
            
    'Update non-bound grid cells
    DoLinks lCurrRow
    
    SetQtyDecPlaces lCurrRow
    
    If lkuItemID.KeyValue > 0 Then
        gGridUnlockCell grdCompItem, kColSOCompItemQty, lCurrRow
    Else
        gGridLockCell grdCompItem, kColSOCompItemQty, lCurrRow
    End If
    
    bIsValidItemID = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsValidItemID", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    FormHelpPrefix = "SOZ"         '       Put your prefix here
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Private Property Get sMyName() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    sMyName = "frmCompItem"
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "sMyName", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    WhatHelpPrefix = "SOZ"         '       Put your prefix here
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Sub FormatGrid()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    'use datasheet grid formating
    gGridSetProperties grdCompItem, kSOCompMaxCols, kGridDataSheet
    gGridSetColors grdCompItem
    
    'Setup maximum number of rows and columns
    gGridSetMaxRows grdCompItem, 1
    gGridSetMaxCols grdCompItem, kSOCompMaxCols

    'Set column widths
    grdCompItem.UnitType = SS_CELL_UNIT_TWIPS
    
    grdCompItem.ScrollBars = ScrollBarsBoth

    'Setup the Component Item column
    gGridSetHeader grdCompItem, kColSOCompItemID, gsBuildString(kIMCompoItem, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnWidth grdCompItem, kColSOCompItemID, 3300
    gGridSetColumnType grdCompItem, kColSOCompItemID, SS_CELL_TYPE_EDIT, 30  'SS_CELL_TYPE_STATIC_TEXT, 30
    
    'Setup the Description column
    gGridSetHeader grdCompItem, kColSOCompItemDesc, gsBuildString(kDescri, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnWidth grdCompItem, kColSOCompItemDesc, 4000
    gGridLockColumn grdCompItem, kColSOCompItemDesc
    
    'Setup the Quantity column
    gGridSetHeader grdCompItem, kColSOCompItemQty, gsBuildString(kSOQtyKitUOM, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnType grdCompItem, kColSOCompItemQty, SS_CELL_TYPE_FLOAT, miQtyDP, 7
    gGridSetColumnWidth grdCompItem, kColSOCompItemQty, 1100
    
    'Setup the Stock UOM column
    gGridSetHeader grdCompItem, kColSOCompItemUOM, gsBuildString(kIMStkUOM, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnWidth grdCompItem, kColSOCompItemUOM, 1100
    gGridLockColumn grdCompItem, kColSOCompItemUOM
    
    'Hidden columns
    gGridSetHeader grdCompItem, kColSOCompItemKey, "CompItemKey"
    gGridSetHeader grdCompItem, kColSOCompKey, "Key"
    gGridSetHeader grdCompItem, kColSOLineKey, "SOLineKey"
    gGridSetHeader grdCompItem, kColSOAllowDec, "Allow Dec Qty"
        
    gGridLockColumn grdCompItem, kColSOCompItemKey
    gGridLockColumn grdCompItem, kColSOCompKey
    gGridLockColumn grdCompItem, kColSOLineKey
    gGridLockColumn grdCompItem, kColSOAllowDec
          
    gGridHideColumn grdCompItem, kColSOCompItemKey
    gGridHideColumn grdCompItem, kColSOCompKey
    gGridHideColumn grdCompItem, kColSOLineKey
    gGridHideColumn grdCompItem, kColSOAllowDec
       
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormatGrid", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


'************************************************************************
'   Description:
'       create context menu object and bind controls to it
'
'   Param:
'       <none>
'
'   Returns:
'
'************************************************************************

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moContextMenu = New clsContextMenu      'Instantiate Context Menu Class

   With moContextMenu
        .BindGrid moGMKitComp, grdCompItem.hWnd 'define grid context menu (add/delete)
        .Bind "*APPEND", grdCompItem.hWnd       'add extra item to the context menu

        .Bind "IMDA06", lkuItemID.hWnd, kEntTypeIMItem

        Set .Form = frmCompItem
        .Init                                   'Init will set properties of Winhook control

    End With
    

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub



Private Sub grdCompItem_EditChange(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    'Track change in case user clicks on toolbar before tabbing off control
    If Col = kColSOCompItemID Or Col = kColSOCompItemQty Then
        mbEditChange = True
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdCompItem_EditChange", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuItemID_LookupClicked()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGMKitComp.LookupClicked
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuItemID_LookupClicked", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub lkuItemID_ProductCategoryClick()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    With lkuItemID
        .CustID = Trim(msCustID)
        .CustName = Trim(msCustName)
        .WhseID = Trim(msWhseID)
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuItemID_ProductCategoryClick", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub moDmKitComp_DMGridAfterInsert(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

Dim sSQL As String
    
    sSQL = "INSERT #InsertedCompRows SELECT " & gdGetValidDbl(moDmKitComp.GetColumnValue(lRow, "SOLineCompItemKey"))
    moClass.moAppDB.ExecuteSQL sSQL
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmKitComp_DMGridAfterInsert", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmKitComp_DMGridAfterUpdate(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

Dim sSQL As String
    
    sSQL = "INSERT #UpdatedCompRows SELECT " & gdGetValidDbl(moDmKitComp.GetColumnValue(lRow, "SOLineCompItemKey"))
    moClass.moAppDB.ExecuteSQL sSQL
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmKitComp_DMGridAfterUpdate", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig


End Sub

Private Sub moGMKitComp_CellChange(ByVal lRow As Long, ByVal lCol As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bValidateCell lCol, lRow
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moGMKitComp_CellChange", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub moGMKitComp_EnterGridRow(ByVal lRow As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    'Get the starting values for the row for validation purposes
    msItemID = Trim(gsGridReadCell(grdCompItem, lRow, kColSOCompItemID))
    mlItemKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColSOCompItemKey))
    
    mdQty = gdGetValidDbl(gsGridReadCell(grdCompItem, lRow, kColSOCompItemQty))
    
    'Disable quantity if on a row with no item entered
    If Len(msItemID) = 0 Then
        gGridLockCell grdCompItem, kColSOCompItemQty, lRow
    Else
        If Not mbReadOnly Then
            gGridUnlockCell grdCompItem, kColSOCompItemQty, lRow
        End If
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moGMKitComp_EnterGridRow", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub moGMKitComp_GridBeforeDelete(bContinue As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
 Dim sSQL As String
 
    bContinue = True

    'do not allow a kit with no component items
    If grdCompItem.DataRowCnt > 1 Then
        mbIsDirty = True
    
        sSQL = "INSERT #DeletedCompRows SELECT " & gdGetValidDbl(gsGridReadCell(grdCompItem, grdCompItem.ActiveRow, kColSOCompKey))
        moClass.moAppDB.ExecuteSQL sSQL
    Else
        giSotaMsgBox Me, moClass.moSysSession, kmsgKitComponent
        bContinue = False
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moGMKitComp_GridBeforeDelete", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function CMMenuSelected(ctl As Object, lTaskID As Long)

    Dim oDrillAround        As Object
    Dim sItemID             As String
    Dim sParam              As String
    
    On Error GoTo ExpectedErrorRoutine

    CMMenuSelected = False

    If (ctl Is lkuItemID) Then

    Else
        Select Case lTaskID
            Case 100
                sItemID = Trim$(gsGridReadCellText(grdCompItem, grdCompItem.ActiveRow, kColSOCompItemID))
                
                Screen.MousePointer = vbHourglass
                
                On Error Resume Next
            
                ' For Vista OS (to ensure task comes to foreground)
                Dim bRetVal As Boolean
                bRetVal = AllowSetForegroundWindow(ASFW_ANY)
                Err.Clear
                
                Set oDrillAround = moClass.moFramework.LoadSOTAObject(ktskIMItemMaint, kSOTA1RunFlags, kContextNormal)
                
                If (oDrillAround Is Nothing) Or Err.Number Then
                    On Error GoTo ExpectedErrorRoutine
                    GoTo BadLoad
                Else
                    Me.Enabled = False
                    oDrillAround.DrillAround sItemID
                    If Err.Number Then GoTo BadLoad
                    
                    On Error GoTo ExpectedErrorRoutine
                    Set oDrillAround = Nothing
                    Screen.MousePointer = vbDefault
                    Me.Enabled = True
                End If
        
            Case 10000 'for future use when we add the standard Item Context Menu Options
    
        End Select
    
    End If
    
    
    
    If Not (oDrillAround Is Nothing) Then
        Set oDrillAround = Nothing
    End If

    CMMenuSelected = True
    
    Exit Function
    
BadLoad:
        Screen.MousePointer = vbDefault
        Me.Enabled = True
        Me.SetFocus
        If Not (oDrillAround Is Nothing) Then
            Set oDrillAround = Nothing
        End If

        sParam = gsBuildString(kSOMaintainItem, moClass.moAppDB, moClass.moSysSession)
        giSotaMsgBox Me, moClass.moSysSession, kmsgErrorLoadTask, sParam
        
    Exit Function
    
ExpectedErrorRoutine:
    Exit Function
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CMMenuSelected", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Select Case KeyCode

        Case vbKeyF1 To vbKeyF16
            'If Shift = 0 And KeyCode = vbKeyF5 And grdCompItem.ActiveCol = kColSOCompItemID Then
            '    Call navGridKit_Click
            'Else
                gProcessFKeys Me, KeyCode, Shift
            'End If

    End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        Case Else
            'Other KeyAscii routines.
    End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub Form_Load()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    mbLeaveCell = True
    
    'Save the form size variable to use for resize later
    miOldFormHeight = Me.Height
    miMinFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormWidth = Me.Width
    
    'Setup form level variables
    
    msCompanyID = moClass.moSysSession.CompanyID
    ' *** RMM 5/2/01, Context menu project
    mbIMActivated = moClass.moSysSession.IsModuleActivated(kModuleIM)

    'Setup Toolbar
    tbrMain.LocaleID = moClass.moSysSession.Language       'Language ID
    tbrMain.RemoveButton kTbPrint
    
    BindGrid
            
    BindContextMenu
           
    'Format the Kit Component Grid
    FormatGrid
    
    Set lkuItemID.Framework = moClass.moFramework
    Set lkuItemID.SysDB = moClass.moAppDB
    Set lkuItemID.AppDatabase = moClass.moAppDB


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
Resume
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub BindGrid()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Bind the KitComponent Grid Manager
    Set moGMKitComp = New clsGridMgr
    With moGMKitComp
        Set .Grid = grdCompItem
        
        Set .DM = moDmKitComp
        Set .Form = frmCompItem
        .GridType = kGridDataSheet
        
        .BindColumn kColSOCompItemID, lkuItemID
        
        .Init
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindGrid", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iRetVal As Integer
'#If CUSTOMIZER Then
'    If Not moFormCust Is Nothing Then
'        If Not moFormCust.CanShutdown Then
'            Cancel = True
'            Exit Sub
'        End If
'    End If
'#End If
    If UnloadMode <> vbFormCode Then
    
        Cancel = 1
                
        If mbIsDirty Or mbEditChange Then
            If Not bIsValidItemID(grdCompItem.ActiveRow) Then
                Exit Sub
            End If

            iRetVal = giSotaMsgBox(Me, moClass.moSysSession, kmsgDMSaveChangesGen)
            Select Case iRetVal
                Case vbYes
                    HandleToolbarClick kTbFinishExit
    
                Case vbNo
                    HandleToolbarClick kTbCancelExit
    
                Case vbCancel
                    
                    Exit Sub
    
            End Select
        Else
            HideForm
        End If
    Else
        PerformCleanShutDown
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
     
    moGMKitComp.UnloadSelf
    moDmKitComp.UnloadSelf
    Set moGMKitComp = Nothing
    Set moDmKitComp = Nothing
    Set ItemMgr = Nothing
    Set moContextMenu = Nothing
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++

    'resize the form height and height all ctls on the screen (form height doesn't
    'get resize smaller than its original height)
    gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, grdCompItem
     
    'resize the form Width and Width all ctls on the screen (form Width doesn't
    'get resize smaller than its original Width)
    gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth
    
    

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub grdCompItem_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGMKitComp.Grid_Change Col, Row
    
    bValidateCell Col, Row
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdCompItem_Change", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdCompItem_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGMKitComp.Grid_Click Col, Row
    
    'Exit this subroutine


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdCompItem_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub



Private Sub grdCompItem_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGMKitComp.Grid_EditMode Col, Row, Mode, ChangeMade

    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdCompItem_EditMode", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdCompItem_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'    moGMKitComp.Scroll
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdCompItem_GotFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdCompItem_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    If Not mbLeaveCell Then
        Cancel = True
        mbLeaveCell = True
        Exit Sub
    End If
    
    Cancel = Not moGMKitComp.Grid_LeaveCell(Col, Row, NewCol, NewRow)
         
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdCompItem_LeaveCell", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Public Sub SetItemRestrict(lWhseKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sRestrictClause As String

    If lWhseKey = 0 Then
        sRestrictClause = "CompanyID = " & gsQuoted(msCompanyID) & " AND (ItemType IN (1,5,6,8)) AND (Status = 1)"
    Else
        sRestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
        sRestrictClause = sRestrictClause & " AND (ItemType IN (1,5,6,8))"
        sRestrictClause = sRestrictClause & " AND (ItemKey IN (SELECT a.ItemKey "
        sRestrictClause = sRestrictClause & " FROM timInventory a WITH (NOLOCK) "
        sRestrictClause = sRestrictClause & " INNER JOIN timItem b WITH (NOLOCK) ON a.ItemKey = b.ItemKey "
        sRestrictClause = sRestrictClause & " WHERE a.Status = 1 AND a.WhseKey = " & lWhseKey & ")"
        sRestrictClause = sRestrictClause & " OR ItemType < 5)"
    End If
    lkuItemID.RestrictClause = sRestrictClause
    
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetItemRestrict", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub Init(ByVal oClass As Object, oDmKitGrid As Object, ByVal iQtyDP As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = oClass
    Set moDmKitComp = oDmKitGrid
    
    miQtyDP = iQtyDP
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Init", VBRIG_IS_FORM                                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Public Sub SetupKit(lItemKey As Long, lSOLineKey As Long, lWhseKey As Long, iGrossProfitCost As Integer, dKitCost As Double)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String
Dim rs   As Object
Dim lRow As Long

    
'
'NOTE:  Any non-database (i.e. linked) columns must also be added to the DoLinks routine
'
    ClearKit lSOLineKey
    
    'Load the new kit
    sSQL = "SELECT CompItemKey, CompItemQty, KitItemKey, AllowDecimalQty, ItemID, ShortDesc, UnitMeasID, "
    sSQL = sSQL & "(CASE "
    sSQL = sSQL & " WHEN timItem.ItemType < " & IMS_FINISHED_GOOD & " THEN "
    sSQL = sSQL & "     timItem.StdUnitCost "
    If lWhseKey > 0 Then
        sSQL = sSQL & "WHEN " & iGrossProfitCost & " = " & kiGPCostAvg & " THEN "
        sSQL = sSQL & "     timInventory.AvgUnitCost "
        sSQL = sSQL & "WHEN " & iGrossProfitCost & " = " & kiGPCostLanded & " THEN "
        sSQL = sSQL & "     timInventory.LandedUnitCost "
        sSQL = sSQL & "WHEN " & iGrossProfitCost & " = " & kiGPCostLast & " THEN "
        sSQL = sSQL & "     timInventory.LastUnitCost "
        sSQL = sSQL & "WHEN " & iGrossProfitCost & " = " & kiGPCostStd & " THEN "
        sSQL = sSQL & "     timInventory.StdUnitCost "
        sSQL = sSQL & "WHEN " & iGrossProfitCost & " = " & kiGPCostRplc & " THEN "
        sSQL = sSQL & "     timInventory.RplcmntUnitCost "
    End If
    sSQL = sSQL & "ELSE 0 "
    sSQL = sSQL & "END) UnitCost "
    sSQL = sSQL & "FROM  timKitCompList WITH (NOLOCK) "
    sSQL = sSQL & "JOIN  timItem WITH (NOLOCK) "
    sSQL = sSQL & "ON    timKitCompList.CompItemKey = timItem.ItemKey "
    sSQL = sSQL & "JOIN  timItemDescription WITH (NOLOCK) "
    sSQL = sSQL & "ON    timItemDescription.ItemKey = timItem.ItemKey "
    If lWhseKey > 0 Then
        sSQL = sSQL & "LEFT OUTER JOIN  timInventory WITH (NOLOCK) "
        sSQL = sSQL & "ON    timInventory.ItemKey = timItem.ItemKey "
        sSQL = sSQL & "AND   timInventory.WhseKey = " & lWhseKey & " "
    End If
    sSQL = sSQL & "JOIN tciUnitMeasure WITH (NOLOCK) "
    sSQL = sSQL & "ON    tciUnitMeasure.UnitMeasKey = timItem.StockUnitMeasKey "
    sSQL = sSQL & "WHERE KitItemKey = " & lItemKey
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

    sSQL = ""
  
    lRow = 1
    Do Until rs.IsEOF
        
        With grdCompItem
            .Row = lRow
            .Action = ActionActiveCell
        End With

        gGridUpdateCell grdCompItem, lRow, kColSOAllowDec, gsGetValidStr(rs.Field("AllowDecimalQty"))

        SetQtyDecPlaces lRow

        gGridUpdateCell grdCompItem, lRow, kColSOCompItemKey, gsGetValidStr((rs.Field("CompItemKey")))
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemQty, gsGetValidStr(gdGetValidDbl(rs.Field("CompItemQty")))
        gGridUpdateCell grdCompItem, lRow, kColSOCompKey, gsGetValidStr(glGetNextSurrogateKey(moClass.moAppDB, "tsoSOLineCompItem"))
        gGridUpdateCell grdCompItem, lRow, kColSOLineKey, gsGetValidStr(lSOLineKey)


        gGridUpdateCell grdCompItem, lRow, kColSOCompItemID, gsGetValidStr(rs.Field("ItemID"))
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemDesc, gsGetValidStr(rs.Field("ShortDesc"))
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemUOM, gsGetValidStr(rs.Field("UnitMeasID"))

        dKitCost = dKitCost + (gdGetValidDbl(rs.Field("CompItemQty")) * gdGetValidDbl(rs.Field("UnitCost")))

        grdCompItem_Change kColSOCompItemKey, lRow

        If Not rs.IsEOF Then
            grdCompItem_LeaveCell kColSOCompItemKey, lRow, kColSOCompItemKey, lRow + 1, False
        Else
            grdCompItem_LeaveCell kColSOCompItemKey, lRow, kColSOCompItemKey, -1, False
        End If


        'Populate the "real" temp table.  The work table bound to the form will be populated when the form is accessed.
        If Len(sSQL) > 0 Then
            sSQL = sSQL & "; "
        End If
        
        sSQL = sSQL & " INSERT INTO #tsoSOLineCompItem (CompItemKey, CompItemQty, SOLineCompItemKey, SOLineKey)"
        sSQL = sSQL & " VALUES ( " & gsGetValidStr((rs.Field("CompItemKey"))) & ", " & gsGetValidStr(gdGetValidDbl(rs.Field("CompItemQty"))) & ", "
        sSQL = sSQL & gsGridReadCell(grdCompItem, lRow, kColSOCompKey) & ", " & gsGetValidStr(lSOLineKey) & ")"
        
        lRow = lRow + 1
        rs.MoveNext
    Loop
    
    moClass.moAppDB.ExecuteSQL sSQL
          
    If mbKitChanged = True Then
        sSQL = "INSERT #InsertedCompRows (SOLineCompItemKey)"
        sSQL = sSQL & " SELECT DISTINCT SOLineCompItemKey FROM #tsoSOLineCompItem WHERE SOLineKey = " & lSOLineKey

        moClass.moAppDB.ExecuteSQL sSQL
        mbKitChanged = False
    End If
    
    If Not rs Is Nothing Then
        Set rs = Nothing
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupKit", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

'Scopus 37263 - CEJ - 05/17/2007 - Changed from Private to Public to allow for
'cleaning up of component items from bIsValidItemID function in frmSalesOrd
Public Sub ClearKit(lSOLineKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    'All the components in the grid belong to the same SO line
       
    If grdCompItem.DataRowCnt > 0 Then
        'Clear existing kit

        
        moDmKitComp.DeleteBlock 1, grdCompItem.DataRowCnt
        If lSOLineKey > 0 Then
            moClass.moAppDB.ExecuteSQL "DELETE #tsoSOLineCompItem WHERE SOLineKey = " & lSOLineKey
        End If
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ClearKit", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub LoadComp(lSOLineKey As Long, sItemID As String, sUOM As String, sCustID As String, _
    sCustName As String, sWhseID As String, lWhseKey As Long, iGrossProfitCost As Integer, _
    bReadOnly As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim lRow    As Long
    Dim sSQL    As String
    Dim rs      As Object

    msCustID = sCustID
    msCustName = sCustName
    msWhseID = sWhseID
    mlSOLineKey = lSOLineKey
    mbReadOnly = bReadOnly
    
    If bReadOnly Then
        gGridLockColumn grdCompItem, kColSOCompItemID
        gGridLockColumn grdCompItem, kColSOCompItemQty
    Else
        gGridUnlockColumn grdCompItem, kColSOCompItemID
        gGridUnlockColumn grdCompItem, kColSOCompItemQty
    End If
    
      
    txtCompItem.Text = sItemID
      
    txtKitUOM.Text = sUOM
    
    mbIsDirty = False
   
    'Insert the rows for this kit into the work table bound to the kit DM
    sSQL = "TRUNCATE TABLE #tsoSOLineCompWrk;"
    sSQL = sSQL & "INSERT #tsoSOLineCompWrk (CompItemKey, CompItemQty, SOLineCompItemKey, SOLineKey)"
    sSQL = sSQL & " SELECT CompItemKey, CompItemQty, SOLineCompItemKey, SOLineKey FROM #tsoSOLineCompItem WHERE SOLineKey = " & lSOLineKey
    
    moClass.moAppDB.ExecuteSQL sSQL
    
    'Populate the grid
    moDmKitComp.Refresh

             
    For lRow = 1 To grdCompItem.DataRowCnt
    
        'On an existing record, first time the row is accessed, the links
        'are there.  On a new row, first time accessed, the links are there (set in SetupKit).
        'When the row accessed again, the links are not there.
        If Len(Trim(gsGridReadCell(grdCompItem, lRow, kColSOCompItemID))) = 0 Then
            DoLinks lRow
        End If
        
        SetQtyDecPlaces lRow
        
        If Not bReadOnly Then
            gGridUnlockCell grdCompItem, kColSOCompItemQty, lRow
        End If
                   
    Next lRow
    
    If Not bReadOnly Then
        With grdCompItem
            .Row = 1
            .Col = kColSOCompItemID
            .Action = ActionActiveCell
        End With
    End If
    
    'Should be positioned on the first grid cell - set the text value
    lkuItemID.SetTextAndKeyValue Trim(gsGridReadCell(grdCompItem, grdCompItem.ActiveRow, kColSOCompItemID)), glGetValidLong(gsGridReadCell(grdCompItem, grdCompItem.ActiveRow, kColSOCompItemKey))
    msItemID = Trim(lkuItemID.Text)
    mlItemKey = lkuItemID.KeyValue
'
'    gbSetFocus Me, grdCompItem

    'grdCompItem_LostFocus
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "LoadComp", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub DoLinks(lRow As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String
Dim rs As Object
 
     
    'Do the grid links.  Note - DM DoLinks only works when LinkSource uses kDmLink.  Grid is
    'bound using kDmJoin for efficiency, and also because there are links to linked tables...
    sSQL = "SELECT ItemID, AllowDecimalQty, ShortDesc, UnitMeasID "
    sSQL = sSQL & "FROM timItem WITH (NOLOCK) "
    sSQL = sSQL & "JOIN timItemDescription WITH (NOLOCK) "
    sSQL = sSQL & "ON   timItem.ItemKey = timItemDescription.ItemKey "
    sSQL = sSQL & "JOIN tciUnitMeasure WITH (NOLOCK) "
    sSQL = sSQL & "ON   timItem.StockUnitMeasKey = tciUnitMeasure.UnitMeasKey "
    sSQL = sSQL & "WHERE timItem.ItemKey = " & glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColSOCompItemKey))

    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

    If Not rs Is Nothing Then
        gGridUpdateCell grdCompItem, lRow, kColSOAllowDec, gsGetValidStr(rs.Field("AllowDecimalQty"))
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemID, gsGetValidStr(rs.Field("ItemID"))
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemDesc, gsGetValidStr(rs.Field("ShortDesc"))
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemUOM, gsGetValidStr(rs.Field("UnitMeasID"))
        Set rs = Nothing
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DoLinks", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub SetQtyDecPlaces(lRow As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    If giGetValidInt(gsGridReadCell(grdCompItem, lRow, kColSOAllowDec)) = 0 Then
        gGridSetCellType grdCompItem, lRow, kColSOCompItemQty, SS_CELL_TYPE_FLOAT, 0
    Else
        gGridSetCellType grdCompItem, lRow, kColSOCompItemQty, SS_CELL_TYPE_FLOAT, miQtyDP
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetQtyDecPlaces", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    If Not moClass Is Nothing Then
        Set moClass = Nothing
    End If
 
   
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moGMKitComp_LeaveGridRow(ByVal lLeaveRow As Long, ByVal lEnterRow As Long, ByVal eReason As GridMgrExt.LeaveGridRowReasons, bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Debug.Print "moGMKitComp_LeaveGridRow"

    bCancel = False
    
    If Me.Visible Then
        If Not bValidateCell(grdCompItem.ActiveCol, lLeaveRow) Then
            bCancel = True
        End If
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moGMKitComp_LeaveGridRow", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    HandleToolbarClick Button
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim sSQL                As String
    Dim lKey                As Long
    Dim i                   As Long
    Dim dKitPrice           As Double
    
    
    Select Case sKey
        
        Case kTbFinishExit
        
            gbSetFocus Me, tbrMain
            If grdCompItem.ActiveCol = kColSOCompItemID Then
                grdCompItem_LeaveCell kColSOCompItemID, grdCompItem.ActiveRow, kColSOCompItemQty, grdCompItem.ActiveRow, False
            End If

            'If user made a change and clicked on toolbar, validate
            If mbEditChange Then
                If Not bValidateCell(grdCompItem.ActiveCol, grdCompItem.ActiveRow) Then
                    Exit Sub
                End If
            End If
             
            If moDmKitComp.IsDirty Then
                moDmKitComp.Save
                
                'Update the "real" temp table.  Delete rows for this SOline and insert from work table
                sSQL = "DELETE #tsoSOLineCompItem WHERE SOLineKey = " & mlSOLineKey
                sSQL = sSQL & ";INSERT #tsoSOLineCompItem (CompItemKey, CompItemQty, SOLineCompItemKey, SOLineKey)"
                sSQL = sSQL & " SELECT CompItemKey, CompItemQty, SOLineCompItemKey, SOLineKey FROM #tsoSOLineCompWrk"
                
                moClass.moAppDB.ExecuteSQL sSQL
            End If
            
            HideForm
            Exit Sub
        Case kTbCancelExit
            gbSetFocus Me, tbrMain
            
            mbIsDirty = False
            
            HideForm
            Exit Sub
        Case kTbHelp
          
          'Run form help
            gDisplayFormLevelHelp Me
            Exit Sub
    End Select
    

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub HideForm()

    'When the form is shown in read only mode, if previously it was not read only and the lookup control is
    'visible on the grid, it will still be visible, allowing the user to change the item when they should
    'not be able to.  So, leave the cell, which will cause the lookup control to be hidden if the form
    'is shown in read only mode.
    If grdCompItem.ActiveCol = kColSOCompItemID Then
        grdCompItem_LeaveCell kColSOCompItemID, grdCompItem.ActiveRow, kColSOCompItemQty, grdCompItem.ActiveRow, False
    End If
    
    Me.Hide
    
End Sub
Public Function bGetKitPrice(ByVal lKitItemKey As Long, dTotalPrice As Double) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'Assumptions:  This routine is only called if the SO line is not from a quote or a blanket
'              contract.
Dim dQty As Double
Dim dStdPrice As Double
Dim lRow As Long

    bGetKitPrice = False


    'Only reprice if the kit price is based on component total
    If gbGetValidBoolean(moClass.moAppDB.Lookup("PriceAsCompTotal", "timKit", "KitItemKey =" & lKitItemKey)) Then

        dTotalPrice = 0

        For lRow = 1 To grdCompItem.DataRowCnt
            'Get the quantity of the selected row
            dQty = gdGetValidDbl(gsGridReadCell(grdCompItem, lRow, kColSOCompItemQty))

            'get the item price for selected row
            dStdPrice = gdGetValidDbl(moClass.moAppDB.Lookup("StdPrice", "timItem", "ItemKey =" & Val(gsGridReadCell(grdCompItem, lRow, kColSOCompItemKey))))
            dTotalPrice = dTotalPrice + (dQty * dStdPrice)

        Next lRow

        bGetKitPrice = True
    End If
    

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bGetKitPrice", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Function dTotalKitCost(lWhseKey As Long, iGrossProfitCost As Integer, lKitItemKey As Long) As Double
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim dQty As Double
Dim lCompItemKey As Long
Dim sSQL As String
Dim rs As Object
Dim lRow As Long



    'Get the unit cost for each component
    sSQL = "SELECT CompItemKey, "
    sSQL = sSQL & "(CASE "
    sSQL = sSQL & " WHEN timItem.ItemType < " & IMS_FINISHED_GOOD & " THEN "
    sSQL = sSQL & "     timItem.StdUnitCost "
    If lWhseKey > 0 Then
        sSQL = sSQL & "WHEN " & iGrossProfitCost & " = " & kiGPCostAvg & " THEN "
        sSQL = sSQL & "     timInventory.AvgUnitCost "
        sSQL = sSQL & "WHEN " & iGrossProfitCost & " = " & kiGPCostLanded & " THEN "
        sSQL = sSQL & "     timInventory.LandedUnitCost "
        sSQL = sSQL & "WHEN " & iGrossProfitCost & " = " & kiGPCostLast & " THEN "
        sSQL = sSQL & "     timInventory.LastUnitCost "
        sSQL = sSQL & "WHEN " & iGrossProfitCost & " = " & kiGPCostStd & " THEN "
        sSQL = sSQL & "     timInventory.StdUnitCost "
        sSQL = sSQL & "WHEN " & iGrossProfitCost & " = " & kiGPCostRplc & " THEN "
        sSQL = sSQL & "     timInventory.RplcmntUnitCost "
    End If
    sSQL = sSQL & "ELSE 0 "
    sSQL = sSQL & "END) UnitCost "
    sSQL = sSQL & "FROM timItem WITH (NOLOCK) "
    sSQL = sSQL & "JOIN timKitCompList WITH (NOLOCK)"
    sSQL = sSQL & "ON    timKitCompList.CompItemKey = timItem.ItemKey "
    If lWhseKey > 0 Then
        sSQL = sSQL & "LEFT OUTER JOIN  timInventory WITH (NOLOCK) "
        sSQL = sSQL & "ON    timInventory.ItemKey = timItem.ItemKey "
        sSQL = sSQL & "AND   timInventory.WhseKey = " & lWhseKey & " "
    End If
    sSQL = sSQL & "WHERE KitItemKey = " & lKitItemKey & " "
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

    Do Until rs.IsEOF
    
        'Find the component row in the grid
         For lRow = 1 To grdCompItem.DataRowCnt
         
            lCompItemKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColSOCompItemKey))
            If lCompItemKey = glGetValidLong(rs.Field("CompItemKey")) Then
                dQty = gdGetValidDbl(gsGridReadCell(grdCompItem, lRow, kColSOCompItemQty))
                dTotalKitCost = dTotalKitCost + (dQty * gdGetValidDbl(rs.Field("UnitCost")))
                Exit For 'go to next record in the set
            End If
        Next lRow
            
        rs.MoveNext
    Loop
    
    If Not rs Is Nothing Then
        Set rs = Nothing
    End If
   
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "dTotalKitCost", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = oNewClass

    'Exit this property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Get MyApp() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyApp = App
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyApp", VBRIG_IS_FORM                                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Get MyForms() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyForms = Forms
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyForms", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get IsDirty() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    IsDirty = mbIsDirty
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "IsDirty", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property


 Private Sub grdCompItem_ColWidthChange(ByVal Col1 As Long, ByVal Col2 As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGMKitComp.Grid_ColWidthChange Col1
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdCompItem_ColWidthChange", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
 Private Sub grdCompItem_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGMKitComp.Grid_KeyDown KeyCode, Shift
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdCompItem_KeyDown", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
 Private Sub grdCompItem_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGMKitComp.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdCompItem_TopLeftChange", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub



