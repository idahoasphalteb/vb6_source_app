VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Begin VB.Form frmSOGenQuote 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Generate Sales Orders"
   ClientHeight    =   4035
   ClientLeft      =   2040
   ClientTop       =   3015
   ClientWidth     =   9525
   HelpContextID   =   17776078
   Icon            =   "GenQuote.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4035
   ScaleWidth      =   9525
   Begin SOTACalendarControl.SOTACalendar dteNewSODate 
      CausesValidation=   0   'False
      Height          =   315
      Left            =   5070
      TabIndex        =   4
      Top             =   600
      WhatsThisHelpID =   17776079
      Width           =   1710
      _ExtentX        =   3016
      _ExtentY        =   503
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Text            =   "  /  /    "
      Object.CausesValidation=   0   'False
   End
   Begin FPSpreadADO.fpSpread grdLines 
      Height          =   2895
      Left            =   60
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   1020
      WhatsThisHelpID =   17776080
      Width           =   9405
      _Version        =   524288
      _ExtentX        =   16589
      _ExtentY        =   5106
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SpreadDesigner  =   "GenQuote.frx":23D2
      AppearanceStyle =   0
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   9525
      _ExtentX        =   16801
      _ExtentY        =   741
      Style           =   4
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtNewSO 
      Height          =   285
      Left            =   1080
      TabIndex        =   2
      Top             =   600
      WhatsThisHelpID =   17776082
      Width           =   1785
      _Version        =   65536
      _ExtentX        =   3149
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      lMaxLength      =   10
   End
   Begin Threed.SSOption OptnStandardSO 
      Height          =   210
      Left            =   7230
      TabIndex        =   6
      Top             =   480
      Width           =   1515
      _Version        =   65536
      _ExtentX        =   2672
      _ExtentY        =   370
      _StockProps     =   78
      Caption         =   "Standard SO"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin Threed.SSOption OptnBlanketSO 
      Height          =   210
      Left            =   7230
      TabIndex        =   7
      Top             =   735
      Width           =   1515
      _Version        =   65536
      _ExtentX        =   2672
      _ExtentY        =   370
      _StockProps     =   78
      Caption         =   "Blanket SO"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Value           =   -1  'True
   End
   Begin VB.Label lblSODate 
      AutoSize        =   -1  'True
      Caption         =   "Bid &Date"
      Height          =   195
      Left            =   4230
      TabIndex        =   3
      Top             =   630
      Width           =   615
   End
   Begin VB.Label lblNewSO 
      AutoSize        =   -1  'True
      Caption         =   "&New SO"
      Height          =   195
      Left            =   210
      TabIndex        =   1
      Top             =   660
      Width           =   600
   End
End
Attribute VB_Name = "frmSOGenQuote"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public moCM             As New clsContextMenu

Private moClass         As Object
Private moErrorLog      As clsErrorLog
Public moOptions         As New clsOptions

Private msCompanyID             As String
Private mlSOKey                 As Long
Private msUserID                As String
Private mlLanguage              As Long
Private mbGened                 As Boolean
Private miQDP                   As Integer
Private mbUseSameRangeForQuote  As Boolean
Private msNewTranNo             As String


Private mbViewRpt       As Boolean


Const kColGenWhseKey = 1
Const kColGenSOLineNo = 2
Const kColGenSOLineKey = 3
Const kColGenItemKey = 4
Const kColGenItemID = 5
Const kColGenDescription = 6
Const kColGenQtyOrd = 7
Const kColGenUOMKey = 8
Const kColGenUOMID = 9
Const kColGenStockQty = 10
Const kColGenStockUOMKey = 11
Const kColGenItemType = 12

'AvaTax integration - General Declarations
Private mbTrackSTaxOnSales      As Boolean ' Used to check AR TrackSTaxOnSales option
Public moAvaTax                 As Object ' Avalara object of "AVZDADL1.clsAVAvaTaxClass", this is installed with the Avalara 3rd party add-on
Public mbAvaTaxEnabled          As Boolean ' Defines if AvaTax is installed on the DB & Client , and is turned on for this company
'AvaTax end


Public Sub ShowMe(iQDP As Integer, sCompID As String, lSOKey As Long, _
                lLanguage As Long, sBusinessDate As String, oClass As Object, sNewTranNo As String, _
                oErrorLog As clsErrorLog, bGened As Boolean, bUseSameRangeForQuote As Boolean, _
                Optional ByRef bCrtdBlnkt As Boolean = False) 'SGS PTC IA
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
            
    mbGened = False
    
    Set moClass = oClass
    Set moErrorLog = oErrorLog
       
    msCompanyID = sCompID
    mlSOKey = lSOKey
    mlLanguage = lLanguage
    msUserID = moClass.moSysSession.UserId
    mbUseSameRangeForQuote = bUseSameRangeForQuote
                
    '******************************************************
    'RKL DEJ 2016-04-13 (START)
    'Kevin/Hal asked to have the Tran Date copied from the Quote to the BSO
    'So will default the date from the Quote but still allow the user to modify
    '******************************************************
    'dteNewSODate = sBusinessDate    'RkL DEJ this was the base/orig code
    
    'The following is the new custom code
    If IsDate(frmSalesOrd.dteSODate.Value) = True Then
        dteNewSODate.Value = frmSalesOrd.dteSODate.Value
    Else
        dteNewSODate.Value = sBusinessDate
    End If
    
    '******************************************************
    'RKL DEJ 2016-04-13 (STOP)
    '******************************************************
    
    
    txtNewSO.Text = sNewTranNo
    msNewTranNo = sNewTranNo
        
    miQDP = iQDP
    LoadGrid
    mbViewRpt = False
    Me.Show vbModal

    bGened = mbGened
    If bGened Then
        bCrtdBlnkt = IsBSOCreateFrom    'SGS PTC IA
        sNewTranNo = msNewTranNo
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ShowMe", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Property Get sMyName() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    sMyName = "frmSOGenQuote"
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "sMyName", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    FormHelpPrefix = "SOZ"         '       Put your prefix here
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    WhatHelpPrefix = "SOZ"         '       Put your prefix here
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Private Function sGetNextSONo() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
    Dim sNextSONo       As String
    Dim iRetVal         As Integer

    Const iNumChars As Integer = 10

    iRetVal = 0

    On Error GoTo ExpectedErrorRoutine
    With moClass.moAppDB
        .SetInParam msCompanyID
        .SetInParam iNumChars
        .SetInParam kTranTypeSOSO
        .SetInParam 0 'IIf(mbUseSameRangeForBlanket, 1, 0)
        .SetInParam IIf(mbUseSameRangeForQuote, 1, 0)
        .SetOutParam sNextSONo
        .SetOutParam iRetVal
        .ExecuteSP ("spsoGetNextSONo")
        sNextSONo = .GetOutParam(6)
        iRetVal = .GetOutParam(7)
        .ReleaseParams
    End With

    On Error GoTo VBRigErrorRoutine
    
    If iRetVal = 0 Then
        sNextSONo = ""
        giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
            "spsoGetNextSONo: " & "0"
    End If

    If iRetVal = -1 Then
        sNextSONo = ""
        'All Sales Order Numbers Used.
        giSotaMsgBox Me, moClass.moSysSession, kSOmsgAllSONumsUsed
    End If

    sGetNextSONo = sNextSONo
    
    CreateSOLog sGetNextSONo
'    ' added   ** SAC 03/08/00  **
'    mbExistingSO = False

    Exit Function

ExpectedErrorRoutine:
giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
    "spsoGetNextSONo: " & Err.Description
gClearSotaErr

Exit Function

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "sGetNextSONo", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub CreateSOLog(sSONum As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'Create the log now for the SO that will be generated from the quote.
Dim sSQL As String
    
'    sSQL = "INSERT INTO tsoSalesOrdLog " & _
'           "(CompanyID,          SOKey,          TranAmtHC, " & _
'            "TranDate,           TranNo,         TranStatus, " & _
'            "TranType,           TranNoRel)" & _
'        "VALUES   (" & gsQuoted(mscompanyid) & "," & _
'                 0 & ",       0.00, GETDATE(), " & _
'                 gsQuoted(sSONum) & ",       1, " & _
'                 kTranTypeSOSO & ",  " & _
'                 gsQuoted(sSONum) & ")"
                 
    'SGS DEJ 5/3/07 Modified for Idaho Asphalt to create Blanket SO
    If OptnStandardSO.Value = True Then
    sSQL = "INSERT INTO tsoSalesOrdLog " & _
           "(CompanyID,          SOKey,          TranAmtHC, " & _
            "TranDate,           TranNo,         TranStatus, " & _
            "TranType,           TranNoRel)" & _
        "VALUES   (" & gsQuoted(msCompanyID) & "," & _
                 0 & ",       0.00, GETDATE(), " & _
                 gsQuoted(sSONum) & ",       1, " & _
                 kTranTypeSOSO & ",  " & _
                 gsQuoted(sSONum) & ")"
    Else
        'SGS DEJ 5/3/07 Added for Idaho Asphalt to create Blanket SO
        sSQL = "INSERT INTO tsoSalesOrdLog " & _
               "(CompanyID,          SOKey,          TranAmtHC, " & _
                "TranDate,           TranNo,         TranStatus, " & _
                "TranType,           TranNoRel)" & _
            "VALUES   (" & gsQuoted(msCompanyID) & "," & _
                     0 & ",       0.00, GETDATE(), " & _
                     gsQuoted(sSONum) & ",       1, " & _
                     kTranTypeSOBO & ",  " & _
                     gsQuoted(sSONum) & ")"
    End If
    moClass.moAppDB.ExecuteSQL sSQL
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CreateSOLog", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub DeleteSOLog(sSONum As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String

    sSQL = "DELETE tsoSalesOrdLog WHERE TranNo = " & gsQuoted(sSONum) & " AND TranType = " & kTranTypeSOSO & _
            "AND CompanyID = " & gsQuoted(msCompanyID)
    
    moClass.moAppDB.ExecuteSQL sSQL
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DeleteSOLog", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub Form_Load()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    dteNewSODate.DisplayOnly = True
    dteNewSODate.DisplayOnly = False
    
    tbrMain.AddButton kTbNextNumber, tbrMain.GetIndex(kTbHelp)
    tbrMain.AddSeparator tbrMain.GetIndex(kTbHelp)
    SetupGrid

    Set moCM.Form = frmSOGenQuote
    moCM.Init

    'AvaTax Integration - FormLoad
    mbTrackSTaxOnSales = gbGetValidBoolean(moClass.moAppDB.Lookup("TrackSTaxonSales", "tarOptions", "CompanyID = " & gsQuoted(msCompanyID)))
    If mbTrackSTaxOnSales Then
        CheckAvaTax
    Else
        mbAvaTaxEnabled = False
    End If
    ' AvaTax Integration end

    'SGS DEJ 4/19/2010 added to fix reload issue
    IsBSOCreateFrom = OptnBlanketSO.Value
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub LoadGrid()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim rs          As Object
Dim lRow        As Long
    
    
    On Error Resume Next
    moClass.moAppDB.ExecuteSQL ("CREATE TABLE #tsoQuoteLoad " & _
                                "(WhseKey     int        NULL, " & _
                                " SOLineNo    smallint   NULL, " & _
                                " SOLineKey   int        NULL, " & _
                                " ItemKey     int        NULL, " & _
                                " ItemID      char(30)   NULL, " & _
                                " Description char(40)   NULL, " & _
                                " QtyOrd      dec(16,8)  NULL, " & _
                                " StockQty    dec(16,8)  NULL, " & _
                                " UOMKey      int        NULL, " & _
                                " UOMID       char(06)   NULL, " & _
                                " StockUOMKey int        NULL, " & _
                                " ItemType    smallint   NULL) ")
    'On Error GoTo 0
    moClass.moAppDB.ExecuteSQL ("DELETE FROM #tsoQuoteLoad ")
    moClass.moAppDB.ExecuteSQL ("EXEC spsoGetQuoteGrid " & mlSOKey)
    
    
    Set rs = moClass.moAppDB.OpenRecordset( _
                   "SELECT * FROM #tsoQuoteLoad ORDER BY SOLineNo", kSnapshot, kOptionNone)
        
    grdLines.MaxRows = 0

    If Not rs Is Nothing Then
        rs.MoveFirst
        
        While Not rs.IsEOF
            
            lRow = lRow + 1
            gGridSetMaxRows grdLines, lRow
            gGridUpdateCell grdLines, lRow, kColGenWhseKey, gsGetValidStr(rs.Field("WhseKey"))
            gGridUpdateCell grdLines, lRow, kColGenSOLineNo, gsGetValidStr(rs.Field("SOLineNo"))
            gGridUpdateCell grdLines, lRow, kColGenSOLineKey, gsGetValidStr(rs.Field("SOLineKey"))
            gGridUpdateCell grdLines, lRow, kColGenItemKey, gsGetValidStr(rs.Field("ItemKey"))
            gGridUpdateCell grdLines, lRow, kColGenItemID, gsGetValidStr(rs.Field("ItemID"))
            gGridUpdateCell grdLines, lRow, kColGenDescription, gsGetValidStr(rs.Field("Description"))
            gGridSetColumnType grdLines, kColGenQtyOrd, SS_CELL_TYPE_FLOAT, miQDP, 8
            gGridUpdateCell grdLines, lRow, kColGenQtyOrd, gdGetValidDbl(rs.Field("QtyOrd"))
            gGridSetColumnType grdLines, kColGenStockQty, SS_CELL_TYPE_FLOAT, miQDP, 8
            gGridUpdateCell grdLines, lRow, kColGenStockQty, gdGetValidDbl(rs.Field("StockQty"))
            gGridUpdateCell grdLines, lRow, kColGenUOMKey, gsGetValidStr(rs.Field("UOMKey"))
            gGridUpdateCell grdLines, lRow, kColGenUOMID, gsGetValidStr(rs.Field("UOMID"))
            gGridUpdateCell grdLines, lRow, kColGenStockUOMKey, gsGetValidStr(rs.Field("StockUOMKey"))
            gGridUpdateCell grdLines, lRow, kColGenItemType, CStr(giGetValidInt(rs.Field("ItemType")))
            rs.MoveNext
            
        Wend
    
        Set rs = Nothing
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "LoadGrid", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub SetupGrid()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    gGridSetProperties grdLines, kColGenItemType, kGridDataSheet
    gGridSetMaxRows grdLines, 0
    grdLines.TypeFloatSeparator = True
    grdLines.DisplayRowHeaders = False
    
    gGridSetHeader grdLines, kColGenSOLineNo, "Line"
    gGridSetHeader grdLines, kColGenItemID, "Item"
    gGridSetHeader grdLines, kColGenDescription, "Description"
    'SGS DEJ 7/11/07 Changed for IA
'    gGridSetHeader grdLines, kColGenQtyOrd, "Qty Ordered"
    gGridSetHeader grdLines, kColGenQtyOrd, "Planned Qty"
    gGridSetHeader grdLines, kColGenUOMID, "UOM"
    gGridSetHeader grdLines, kColGenStockQty, "Qty Available"

    gGridSetColumnWidth grdLines, kColGenSOLineNo, 5
    gGridSetColumnWidth grdLines, kColGenItemID, 15
    gGridSetColumnWidth grdLines, kColGenDescription, 22
    gGridSetColumnWidth grdLines, kColGenQtyOrd, 12
    gGridSetColumnWidth grdLines, kColGenStockQty, 12
    gGridSetColumnWidth grdLines, kColGenUOMID, 6
    
    gGridHideColumn grdLines, kColGenWhseKey
    gGridLockColumn grdLines, kColGenSOLineNo
    gGridHideColumn grdLines, kColGenSOLineKey
    gGridHideColumn grdLines, kColGenItemKey
    gGridLockColumn grdLines, kColGenItemID
    gGridLockColumn grdLines, kColGenDescription
    gGridLockColumn grdLines, kColGenQtyOrd
    gGridLockColumn grdLines, kColGenStockQty
    gGridHideColumn grdLines, kColGenUOMKey
    gGridLockColumn grdLines, kColGenUOMID
    gGridHideColumn grdLines, kColGenStockUOMKey
    gGridHideColumn grdLines, kColGenItemType

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupGrid", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    On Error Resume Next
    moClass.moAppDB.ExecuteSQL "DELETE FROM #tsoLines "
    If Not mbViewRpt Then
        moClass.moAppDB.ExecuteSQL "DELETE FROM tciErrorLog WHERE SessionID = " & mlSpid
    End If
    Set moCM = Nothing
    Set moClass = Nothing
    Set moErrorLog = Nothing
    Set moOptions = Nothing

    ' AvaTax Integration - FormUnload
    If Not moAvaTax Is Nothing Then
        Set moAvaTax = Nothing
    End If
    ' AvaTax end

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Select Case Button
        Case kTbProceed
            SetHourglass True
            'SGS DEJ 8/29/07  Added for Idaho Asphalt
'            CreateFromQuote
            If OptnBlanketSO.Value = True Then
                CreateBlktFromQuote
            Else
            CreateFromQuote
            End If
            SetHourglass False
        Case kTbNextNumber
            txtNewSO.Text = sGetNextSONo
        Case kTbClose
            Unload Me
        Case kTbHelp
            gDisplayFormLevelHelp Me
       End Select
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

'SGS DEJ 8/29/07 Create method for Idaho Ashpalt
Private Sub CreateBlktFromQuote()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sSQL            As String
    Dim lRow            As Long
    Dim iVal            As Integer
    Dim dQty            As Double
    Dim lSOLineKey      As Double
    Dim iQtyToGen       As Integer
    Dim iRetVal         As Integer
    Dim lErr            As Long
    Dim sErrDesc        As String
    Dim lSOKey          As Long
    Dim iStatus         As Integer

    If bIsFormValid Then
        sSQL = "CREATE TABLE #tsoLines " & _
               "(SOLineKey int NOT NULL, " & _
               " ItemKey Int NULL, " & _
               " Qty dec(16,8) NOT NULL) "

        On Error Resume Next
        moClass.moAppDB.ExecuteSQL sSQL
        
        On Error GoTo VBRigErrorRoutine
        
        moClass.moAppDB.ExecuteSQL "DELETE FROM #tsoLines "
        moClass.moAppDB.ExecuteSQL "DELETE FROM tciErrorLog WHERE SessionID = " & mlSpid
        
        For lRow = 1 To grdLines.MaxRows
            lSOLineKey = Val(gsGridReadCell(grdLines, lRow, kColGenSOLineKey))
            dQty = Val(gsGridReadCell(grdLines, lRow, kColGenQtyOrd))
            sSQL = "INSERT INTO #tsoLines VALUES (" & lSOLineKey & ", NULL, " & dQty & ")"
            moClass.moAppDB.ExecuteSQL sSQL
            iQtyToGen = iQtyToGen + 1
        Next lRow
        
        If iQtyToGen = 0 Then
            'No lines were selected
            giSotaMsgBox Me, moClass.moSysSession, kSOmsgNoLines
            Exit Sub
        End If
        
        iStatus = giGetValidInt(moClass.moAppDB.Lookup("Status", "tsoSalesOrder", "SOKey = " & mlSOKey))
        
        If iStatus <> 1 Then    'kvSOOpen Then
            'This Quote is no longer in an Open Status
            giSotaMsgBox Me, moClass.moSysSession, kSOMsgQuoteNoLongerOpen
            Exit Sub
        End If
        
        'If using the quote number for the SO number, create the log now.
        If Len(msNewTranNo) > 0 And Trim(txtNewSO.Text) = msNewTranNo Then
            CreateSOLog txtNewSO.Text
        End If
        
        On Error GoTo ExpectedErrorRoutine
        With moClass.moAppDB
            .SetInParam msCompanyID
            .SetInParam mlSOKey
            
            'SGS DEJ 5/3/2007 Modified to allow for creation of Blanket SO
            If OptnStandardSO.Value = True Then
                .SetInParam CInt(kTranTypeSOSO)
            Else
                'SGS DEJ 5/3/2007 Added to allow for creation of Blanket SO
                .SetInParam CInt(kTranTypeSOBO)
            End If
            
            .SetInParam dteNewSODate.SQLDate
            .SetInParamNull SQL_CHAR
            .SetInParamNull SQL_CHAR
            .SetInParamNull SQL_CHAR
            If Len(Trim(txtNewSO.Text)) = 0 Then
                .SetInParamNull SQL_CHAR
            Else
                .SetInParam CStr(txtNewSO.Text)
            End If
            .SetInParam msUserID
            .SetInParam CInt(0)
            .SetInParam mlLanguage
            .SetInParam CInt(0)
            .SetOutParam iRetVal
            .SetOutParam mlSpid
            .SetOutParam lSOKey
            .ExecuteSP "spsoCreateASalesOrder"
            iRetVal = giGetValidInt(.GetOutParam(13))
            mlSpid = glGetValidLong(.GetOutParam(14))
            lSOKey = glGetValidLong(.GetOutParam(15))
            
            .ReleaseParams
        End With
    
        On Error GoTo VBRigErrorRoutine
        Select Case iRetVal
            Case 0
                'Unexpected errors occurred.
                giSotaMsgBox Me, moClass.moSysSession, kSOMsgUnexpectedError
                Exit Sub
            Case 1
                'Sales order created successfully.
'                giSotaMsgBox Me, moClass.moSysSession, kSOmsgSOCreated
                MsgBox "Blanket sales order created successfully.", vbInformation, "Sage MAS 500"
                mbGened = True
            Case 2
                'Sales order created - warnings were raised.  Do you wish to view the Error Log?
                If giSotaMsgBox(Me, moClass.moSysSession, kSOMsgSoCreatedWarn) = kretYes Then
                    mbViewRpt = True
                End If
                mbGened = True
            Case 3, 4
                'Errors were raised.  Do you wish to view the Error Log?
                If giSotaMsgBox(Me, moClass.moSysSession, kSOmsgErrorsOccurred) = kretYes Then
                    mbViewRpt = True
                End If
                mbGened = False
        End Select
            
        If mbViewRpt Then
            moErrorLog.ProcessSpidErrorLog Me, 0, mlSpid
            moErrorLog.PrintErrorLog kTbPreview, Me ', frmSalesOrd
            moClass.moAppDB.ExecuteSQL "DELETE FROM tciErrorLog WHERE SessionID = " & mlSpid
        End If
        
        If mbGened Then
            'SGS DEJ 5/4/2010
            Call ProcessSLXData(lSOKey, oClass)
            
            msNewTranNo = Trim(txtNewSO.Text)
            Unload Me
        Else
            'If using the quote for the sales order number and didn't generate, delete the SO log.
            If Len(msNewTranNo) > 0 And Trim(txtNewSO.Text) = msNewTranNo Then
                'SGS DEJ Added 7/9/07 for IA
                If OptnBlanketSO.Value = True Then
                    'SGS DEJ Added 7/9/07 for IA
                    DeleteSOLogBnkt msNewTranNo
                Else
                    DeleteSOLog msNewTranNo
                End If
            End If
        End If
        
    End If

Exit Sub

ExpectedErrorRoutine:
lErr = Err
sErrDesc = Err.Description
SetHourglass False
giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
     "spsoCreateASalesOrder: " & sErrDesc
gClearSotaErr
Exit Sub

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CreateBlktFromQuote", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

'SGS DEJ 8/29/07 Created Method for Idaho Asphalt
Private Sub DeleteSOLogBnkt(sSONum As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String

    sSQL = "DELETE tsoSalesOrdLog WHERE TranNo = " & gsQuoted(sSONum) & " AND TranType = " & kTranTypeSOBO & _
            "AND CompanyID = " & gsQuoted(msCompanyID)
    
    moClass.moAppDB.ExecuteSQL sSQL
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DeleteSOLog", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bIsFormValid() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sDate As String

    If Len(Trim(txtNewSO.Text)) = 0 Then
        'Enter a sales order number.
        giSotaMsgBox Me, moClass.moSysSession, kSOEnterSONum
        On Error Resume Next
        txtNewSO.SetFocus
        Exit Function
    End If
    

    If Len(Trim(dteNewSODate.Text)) = 0 Or Not dteNewSODate.IsValid Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, gsStripChar(lblSODate, "&")
        On Error Resume Next
        dteNewSODate.SetFocus
        Exit Function
    End If

    If Len(Trim(frmSalesOrd.dteExpiration)) > 0 Then
        If dteNewSODate.SQLDate > frmSalesOrd.dteExpiration.SQLDate Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeGreaterThan, _
                gsStripChar(lblSODate, "&"), gsStripChar(frmSalesOrd.lblExpiration, "&")
            On Error Resume Next
            dteNewSODate.SetFocus
            Exit Function
        End If
    End If

    If Not bValidGrid() Then
        Exit Function
    End If

    bIsFormValid = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsFormValid", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Private Function bValidGrid() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lRow            As Long
    Dim dStockQty       As Double
    Dim dQtyOrd         As Double
    
    bValidGrid = True
        
    For lRow = 1 To grdLines.MaxRows
        
        dStockQty = Val(gsGridReadCell(grdLines, lRow, kColGenStockQty))
        dQtyOrd = Val(gsGridReadCell(grdLines, lRow, kColGenQtyOrd))
            
        If dQtyOrd > dStockQty Then
            If Val(gsGridReadCell(grdLines, lRow, kColGenItemType)) <> 7 Then
                'There is not currently sufficient quantity available of selected items
                'in the selected warehouse(s).  Do you want to generate the sales order anyway?
                If giSotaMsgBox(Me, moClass.moSysSession, kSOmsgGenSO) <> kretYes Then
                    bValidGrid = False
                End If
            End If
            Exit Function
        End If
    
    Next lRow
    
    bValidGrid = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bValidGrid", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Private Sub CreateFromQuote()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sSQL            As String
    Dim lRow            As Long
    Dim iVal            As Integer
    Dim dQty            As Double
    Dim lSOLineKey      As Double
    Dim iQtyToGen       As Integer
    Dim iRetVal         As Integer
    Dim lErr            As Long
    Dim sErrDesc        As String
    Dim lSOKey          As Long
    Dim iStatus         As Integer

    If bIsFormValid Then
        sSQL = "CREATE TABLE #tsoLines " & _
               "(SOLineKey int NOT NULL, " & _
               " ItemKey Int NULL, " & _
               " Qty dec(16,8) NOT NULL) "

        On Error Resume Next
        moClass.moAppDB.ExecuteSQL sSQL
        
        On Error GoTo VBRigErrorRoutine
        
        moClass.moAppDB.ExecuteSQL "DELETE FROM #tsoLines "
        moClass.moAppDB.ExecuteSQL "DELETE FROM tciErrorLog WHERE SessionID = " & mlSpid
        
        For lRow = 1 To grdLines.MaxRows
            lSOLineKey = Val(gsGridReadCell(grdLines, lRow, kColGenSOLineKey))
            dQty = Val(gsGridReadCell(grdLines, lRow, kColGenQtyOrd))
            sSQL = "INSERT INTO #tsoLines VALUES (" & lSOLineKey & ", NULL, " & dQty & ")"
            moClass.moAppDB.ExecuteSQL sSQL
            iQtyToGen = iQtyToGen + 1
        Next lRow
        
        If iQtyToGen = 0 Then
            'No lines were selected
            giSotaMsgBox Me, moClass.moSysSession, kSOmsgNoLines
            Exit Sub
        End If
        
        iStatus = giGetValidInt(moClass.moAppDB.Lookup("Status", "tsoSalesOrder", "SOKey = " & mlSOKey))
        
        If iStatus <> 1 Then    'kvSOOpen Then
            'This Quote is no longer in an Open Status
            giSotaMsgBox Me, moClass.moSysSession, kSOMsgQuoteNoLongerOpen
            Exit Sub
        End If
        
        'If using the quote number for the SO number, create the log now.
        If Len(msNewTranNo) > 0 And Trim(txtNewSO.Text) = msNewTranNo Then
            CreateSOLog txtNewSO.Text
        End If
        
        On Error GoTo ExpectedErrorRoutine
        With moClass.moAppDB
            .SetInParam msCompanyID
            .SetInParam mlSOKey
            'SGS DEJ 5/3/2007 Modified to allow for creation of Blanket SO
'            .SetInParam CInt(kTranTypeSOSO)
            If OptnStandardSO.Value = True Then
            .SetInParam CInt(kTranTypeSOSO)
            Else
                'SGS DEJ 5/3/2007 Added to allow for creation of Blanket SO
                .SetInParam CInt(kTranTypeSOBO)
            End If
            .SetInParam dteNewSODate.SQLDate
            .SetInParamNull SQL_CHAR
            .SetInParamNull SQL_CHAR
            .SetInParamNull SQL_CHAR
            If Len(Trim(txtNewSO.Text)) = 0 Then
                .SetInParamNull SQL_CHAR
            Else
                .SetInParam CStr(txtNewSO.Text)
            End If
            .SetInParam msUserID
            .SetInParam CInt(0)
            .SetInParam mlLanguage
            .SetInParam CInt(0)
            .SetOutParam iRetVal
            .SetOutParam mlSpid
            .SetOutParam lSOKey
            .ExecuteSP "spsoCreateASalesOrder"
            iRetVal = giGetValidInt(.GetOutParam(13))
            mlSpid = glGetValidLong(.GetOutParam(14))
            lSOKey = glGetValidLong(.GetOutParam(15))
            .ReleaseParams
        End With
    
        On Error GoTo VBRigErrorRoutine
        Select Case iRetVal
            Case 0
                'Unexpected errors occurred.
                giSotaMsgBox Me, moClass.moSysSession, kSOMsgUnexpectedError
                Exit Sub
            Case 1
                'Sales order created successfully.
                giSotaMsgBox Me, moClass.moSysSession, kSOmsgSOCreated
                mbGened = True
                'Avatax Integration - CreateFromQuote
                If mbAvaTaxEnabled Then
                    moAvaTax.bCalculateTax lSOKey, kModuleSO
                End If
                'Avatax end
            Case 2
                'Sales order created - warnings were raised.  Do you wish to view the Error Log?
                If giSotaMsgBox(Me, moClass.moSysSession, kSOMsgSoCreatedWarn) = kretYes Then
                    mbViewRpt = True
                End If
                mbGened = True
                'Avatax Integration - CreateFromQuote
                If mbAvaTaxEnabled Then
                    moAvaTax.bCalculateTax lSOKey, kModuleSO
                End If
                'Avatax end
            Case 3, 4
                'Errors were raised.  Do you wish to view the Error Log?
                If giSotaMsgBox(Me, moClass.moSysSession, kSOmsgErrorsOccurred) = kretYes Then
                    mbViewRpt = True
                End If
                mbGened = False
        End Select
            
        If mbViewRpt Then
            moErrorLog.ProcessSpidErrorLog Me, 0, mlSpid
            moErrorLog.PrintErrorLog kTbPreview, Me ', frmSalesOrd
            moClass.moAppDB.ExecuteSQL "DELETE FROM tciErrorLog WHERE SessionID = " & mlSpid
        End If
        
        If mbGened Then
            msNewTranNo = Trim(txtNewSO.Text)
            
            'SGS DEJ 5/4/2010
            Call ProcessSLXData(lSOKey, oClass, frmSalesOrd.txtIACustom(0).Text)
            
            Unload Me
        Else
            'If using the quote for the sales order number and didn't generate, delete the SO log.
            If Len(msNewTranNo) > 0 And Trim(txtNewSO.Text) = msNewTranNo Then
                DeleteSOLog msNewTranNo
            End If
        End If
        
    End If

Exit Sub

ExpectedErrorRoutine:
lErr = Err
sErrDesc = Err.Description
SetHourglass False
giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
     "spsoCreateASalesOrder: " & sErrDesc
gClearSotaErr
Exit Sub

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CreateFromQuote", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set oClass = goClass
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Get MyApp() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyApp = App
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyApp", VBRIG_IS_FORM                                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Get MyForms() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyForms = Forms
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyForms", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property


Private Sub CheckAvaTax()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***********************************************************************
' Avatax Integration: Check the availability of Avatax for the CompanyID.
'***********************************************************************

On Error GoTo ExpectedErrorRoutine

    'Assume AvaTax feature not configured
    mbAvaTaxEnabled = False

    mbAvaTaxEnabled = gbGetValidBoolean(moClass.moAppDB.Lookup("AvaTaxEnabled", "tavConfiguration", "CompanyID = " & gsQuoted(msCompanyID)))

    If mbAvaTaxEnabled Then
        On Error GoTo ExpectedErrorRoutine_AvaTaxObj
        Set moAvaTax = CreateObject("AVZDADL1.clsAVAvaTaxClass")
        moAvaTax.Init moClass, moClass.moSysSession, moClass.moAppDB, moClass.moAppDB, moClass.moFramework
    End If

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine:
'Expected Error:
'This is how we find out if AvaTax is not installed.
'The missing table (err 4050) will tell us not to fire the AvaTax code
'throughout the program.

    Select Case Err.Number
    
        Case 4050 ' SQL Object Not Found
            'tavConfiguration not found meaning AvaTax not installed.
            'No Error action required.

        Case Else
            'Unexpected Error reading tavConfiguration
            giSotaMsgBox Me, moClass.moSysSession, kmsgtavConfigurationError

    End Select

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine_AvaTaxObj:
'Expected Error:
'If the DB Avatax table was found, AvaTax is likely used for this Site.
'But when creating the local client object, the object was likely not installed
'This is not a good error, let the administrator know to check that AvaTax was properly implemented.

    giSotaMsgBox Me, moClass.moSysSession, kmsgAvaObjectError

    mbAvaTaxEnabled = False

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CheckAvaTax", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub




