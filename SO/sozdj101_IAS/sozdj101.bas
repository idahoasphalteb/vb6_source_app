Attribute VB_Name = "basSalesOrder"
Option Explicit
'**********************************************************************
'     Name: basSalesOrder
'     Desc: Main Module of this Object containing "Sub Main ()".
' Original: 02-27-04 JLW
'     Mods: mm/dd/yy XXX
'**********************************************************************
'Date       Project     SE          Description
'****************************************************************************************
'
'-- Security Event  Constants
    '-- if any security events are defined for this task, here is
    '-- where you would define the constant for the event
    'Public Const ksecChangeStatus As String = "CICHGSTAT"

'-- Object Task Constants
    '-- if this maintenance program has an associated report/listing,
    '-- here is where you would define the report/listing's Task ID so
    '-- that it may be launched when the Print button is pressed on the
    '-- toolbar
  
' Column "Field" descriptions.  These are the indexes of the elements in the
' moColumnDetails collection, and map to tsmCustomGridField.HostFormFieldName.
Public Declare Function AllowSetForegroundWindow Lib "user32" (ByVal dwProcessID As Long) As Boolean
Public Const ASFW_ANY = -1

    Public Const ksFieldItem As String = "ItemID"
    Public Const ksFieldQtyOrd As String = "QtyOrd"
    Public Const ksFieldQtySpinner As String = "QtySpinner"
    Public Const ksFieldUOM As String = "UOM"
    Public Const ksFieldUnitPrice As String = "UnitPrice"
    Public Const ksFieldAmt As String = "Amt"
    Public Const ksFieldDelvMethod As String = "DelMeth"
    Public Const ksFieldShipCompl As String = "SC"
    Public Const ksFieldItemDesc As String = "ItemDesc"
    Public Const ksFieldWhse As String = "Whse"
    Public Const ksFieldStatus As String = "Status"
    Public Const ksFieldShipTo As String = "ShipTo"
    Public Const ksFieldShipToAddr As String = "ShipToAddr"
    Public Const ksFieldShipVia As String = "ShipVia"
    Public Const ksFieldFOB As String = "FOB"
    Public Const ksFieldPriority As String = "Priority"
    Public Const ksFieldFreight As String = "Freight"
    Public Const ksFieldDiscAmt As String = "DiscAmt"
    Public Const ksFieldDiscPct As String = "DiscPct"
    Public Const ksFieldTaxClass As String = "TaxClass"
    Public Const ksFieldTaxAmt As String = "TaxAmt"
    Public Const ksFieldTaxBtn As String = "TaxForm"
    Public Const ksFieldReqDate As String = "ReqDt"
    Public Const ksFieldShipDate As String = "ShipDt"
    Public Const ksFieldPromDate As String = "PromDt"
    Public Const ksFieldCommPlan As String = "CommPlan"
    Public Const ksFieldCommClass As String = "CommClass"
    Public Const ksFieldGLAcctNo As String = "GLAcctNo"
    Public Const ksFieldGLAcctDesc As String = "GLAcctDesc"
    Public Const ksFieldGLAcctRef As String = "GLAcctRef"
    Public Const ksFieldGenPO As String = "GenPO"
    Public Const ksFieldTagOrder As String = "TagOrder"
    Public Const ksFieldPONo As String = "PONo"
    Public Const ksFieldVendor As String = "Vendor"
    Public Const ksFieldVendAddr As String = "VendAddr"
    Public Const ksFieldVendAddrForm As String = "VendAddrForm"
    Public Const ksFieldOrigItem As String = "OrigItem"
    Public Const ksFieldOrigWhse As String = "OrigWhse"
    Public Const ksFieldComment As String = "Comment"
    Public Const ksFieldCommentForm As String = "CommentForm"
    Public Const ksFieldCustFldForm As String = "CustFldForm"
    Public Const ksFieldHold As String = "Hold"
    Public Const ksFieldHoldReason As String = "HoldRsn"
    Public Const ksFieldInclPick As String = "InclPick"
    Public Const ksFieldInclPack As String = "InclPack"
    Public Const ksFieldCTPForm As String = "CTPForm"
    Public Const ksFieldCrtWOForm As String = "CrtWOForm"
    Public Const ksFieldMntWOForm As String = "MntWOForm"
    Public Const ksFieldSchedInqForm As String = "SchedInqForm"
    Public Const ksFieldQtyOrig As String = "QtyOrig"
    Public Const ksFieldQtyOpen As String = "QtyOpen"
    Public Const ksFieldQtyShip As String = "QtyShip"
    Public Const ksFieldQtyBO As String = "QtyBO"
    Public Const ksFieldQtyInvc As String = "QtyInvc"
    Public Const ksFieldQtyRtrnCr As String = "QtyRtrnCr"
    Public Const ksFieldQtyRtrnRplc As String = "QtyRtrnRplc"
    Public Const ksFieldAltShip As String = "AltShip"
    Public Const ksFieldSubShip As String = "SubShip"
    Public Const ksFieldKitComp As String = "KitComp"
    Public Const ksFieldPC As String = "PC"
    Public Const ksFieldEstimate As String = "Estimate"
    
    '******************************************************************************
    'SGS PTC IA (START)
    '******************************************************************************
    Public Const ksFieldAwarded As String = "Awarded"
    Public Const ksFieldUpdatedDate As String = "UpdatedDate"
    Public Const ksFieldEstStartDate As String = "EstStartDate"
    Public Const ksFieldEstEndDate As String = "EstEndDate"
    Public Const ksFieldWeekendsIndicator As String = "WeekendsIndicator"
    Public Const ksFieldUpdatedBy As String = "UpdatedBy"
    Public Const ksFieldLoadsPerDay As String = "LoadsPerDay"
    Public Const ksFieldEstSpreaders As String = "EstSpreaders"
    Public Const ksFieldFreightRate As String = "FreightRate"
    Public Const ksFieldPayTruckRate As String = "PayTruckRate"
    Public Const ksFieldMilesToJob As String = "MilesToJob"
    Public Const ksFieldOrigBidQty As String = "OrigBidQty"
    
    Public IsBSOCreateFrom As Boolean   'SGS DEJ added 4/19/2010
    Public Const ksFieldCertRequired As String = "CertRequired"   'EB DEJ added 11/7/2012
    Public Const ksFieldQASample As String = "QASample"   'EB DEJ added 4/2/13
    Public Const ksFieldReqLoads As String = "ReqLoads"   'EB DEJ added 4/2/13
    
    Public Const ksFieldCurrCnt As String = "CurrentNbrOfQASamples"   'EB DEJ added 4/11/13
    Public Const ksFieldTotalCnt As String = "TotalNbrOfQASamples"   'EB DEJ added 4/11/13
    '******************************************************************************
    'end SGS PTC IA
    '******************************************************************************
    
    Public mlSpid                   As Long
    Public mfrmMain                 As Form
    Public mbEnterAsTab             As Boolean              ' use enter as tab?
   

' tsoSalesOrder | CreateType
    Public Const kvCreateTypeOther  As Integer = 0
    Public Const kvCreateTypeStd    As Integer = 1
    Public Const kvCreateTypeImport As Integer = 2
    Public Const kvCreateTypeUpgrd  As Integer = 3


' tarCustomer | Status
    Public Const kvActiveCust       As Integer = 1
    Public Const kvInactiveCust     As Integer = 2
    Public Const kvTemporaryCust    As Integer = 3
    Public Const kvDeletedCust      As Integer = 4

'-- Static List Constants
' timItem | Status
    Public Const kvActiveItem       As Integer = 1
    Public Const kvInactiveItem     As Integer = 2
    Public Const kvDiscontinuedItem As Integer = 3
    Public Const kvDeletedItem      As Integer = 4
    
    ' timItem | ItemType
    Public Const kvMiscItem         As Integer = 1
    Public Const kvService          As Integer = 2
    Public Const kvExpense          As Integer = 3
    Public Const kvCommentOnly      As Integer = 4
    Public Const kvFinishedGoods    As Integer = 5
    Public Const kvRawMaterial      As Integer = 6
    Public Const kvBTOKit           As Integer = 7
    Public Const kvAssembledKit     As Integer = 8
    Public Const kvCatalog          As Integer = 9
    
    'Gross profit cost type
    Public Const kiGPCostAvg        As Integer = 1
    Public Const kiGPCostLanded     As Integer = 2
    Public Const kiGPCostLast       As Integer = 3
    Public Const kiGPCostRplc       As Integer = 4
    Public Const kiGPCostStd        As Integer = 5
    
    'Credit check type
    Public Const kiCreditCheckPick    As Integer = 0
    Public Const kiCreditCheckSOEntry As Integer = 1
        
    Public moUF             As Object
    Public moSTax           As Object

    Public mlUniqueKey      As Long
    
    Public Type CustDflts
     'Customer
       iAllowInvtSubst            As Integer
       iBillingType               As Integer
       dCreditLimit               As Double
       iCredAgeCat                As Integer
       sCustID                    As String
       sCustName                  As String
       lBillToCustAddrKey         As Long
       sBillToCustAddrID          As String
       lDfltItemKey               As Long
       sDfltItemID                As String
       lDfltSalesAcctKey          As Long
       lShipToCustAddrKey         As Long
       sShipToCustAddrID          As String
       lShipToCustAddrUpdCtr      As String
       iCreditHold                As Integer
       lPrimaryAddrKey            As Long
       sPrimaryAddrLine1          As String
       sPrimaryCity               As String
       sPrimaryState              As String
       sPrimaryPostalCode         As String
       sPrimaryCountry            As String
       dPrimaryLatitude           As Double
       dPrimaryLongitude          As Double
       iReqPO                     As Integer
       dRetntPct                  As Double
       lDfltReturnAcctKey         As Long
       iStatus                    As Integer
       dTradeDiscPct              As Double
       lCustClassKey              As Long
       sCustClassID               As String
       sClassOvrdSegVal           As String
       iPriority                  As Integer
       lSource                    As Long
       sSource                    As String
     'Bill To
       lPmtTermsKey               As Long
       sPmtTermsID                As String
       lSOAckFormKey              As Long
       sSOAckFormID               As String
       lCurrExchSchdKey           As Long
       sCurrExchSchdID            As String
       sCurrID                    As String
       iPrintOrdAck               As Integer
       iReqAck                    As Integer
     'Ship To
       lDfltCntctKey              As Long
       sContactName               As String
       lPackListKey               As Long
       sPackListID                As String
       lShipMethKey               As Long
       sShipMethID                As String
       lShipZoneKey               As Long
       sShipZoneID                As String
       lSperKey                   As Long
       sSperID                    As String
       sSperName                  As String
       lSTaxSchdKey               As Long
       lShipToSTaxSchdKey         As Long
       sTaxSchedID                As String
       lFOBKey                    As Long
       sFOBID                     As String
       lCommPlanKey               As Long
       sCommPlanID                As String
       lWhseKey                   As Long
       sWhseID                    As String
       iShipDays                  As Integer
       iShipComplete              As Integer
       iTaxPoint                  As Integer
       iBillToNationalAcctParent  As Integer
       iPmtByNationalAcctParent   As Integer
       lNationalAcctParentCustKey As Long
       iNationalAcctHold          As Integer
       iWhseAllowImmedPick        As Integer
       iFreightMethod             As Integer
    End Type
    
    Public Type ItemDflts
        lSOLineKey          As Long
        iSOLineNo           As Integer
        iPriceOvrd          As Integer
        dQtyShipped         As Double
        dQtyInvcd           As Double
        dQtyRtrnCred        As Double
        dQtyRtrnRplc        As Double
        dAmtInvcd           As Double
        dOrigQtyOrd         As Double
        sOrigPromiseDate    As String
        lQuoteLineKey       As Long
        lSTaxTranKey        As Long
        lSTaxSchdKey        As Long 'Actual - based on FOB, Delivery, Ship To
        lShipToSTaxSchdKey  As Long 'Ship To tax schedule
        lUpdateCounter      As Long
        lShipZoneKey        As Long
        lBlanketKey         As Long
        bBlanketContract    As Boolean
        iCommentOnly        As Integer
        lSOLineDistKey      As Long
        sUserFld1           As String
        sUserFld2           As String
        dQtyBO              As Double
        lShipToCustAddrKey  As Long
        lShipToAddrKey      As Long
        iTaxPoint           As Integer
        lPOLineKey          As Long
        iCustomBTOKit       As Integer
        iDeliveryMethod     As Integer
        iWhseAllowImmedPick As Integer
        iShipDays           As Integer
        bNonInventory       As Boolean
        iItemType           As Integer
        bAllowPriceOvrd     As Boolean
        bSubjTradeDisc      As Boolean
        bAllowDecimalQty    As Boolean
    End Type
    
        
    Public Type ItemWasDflts
        lWhseKey            As Long
        iStatus             As Integer
        lItemKey            As Long
        sItemDesc           As String
        dQtyOrd             As Double
        dQtyOpen            As Double
        lUOMKey             As Long
        dUnitPrice          As Double
        dExtAmt             As Double
        lShipMethKey        As Long
        lFOBKey             As Long
        iPriority           As Integer
        lShipToCustAddrKey  As Long
        dFreightAmt         As Double
        iDropShip           As Integer
        lVendKey            As Long
        lVendAddrKey        As Long
        lCommPlanKey        As Long
        lCommClassKey       As Long
        lGLAcctKey          As Long
        lAcctRefKey         As Long
        sReqDate            As String
        sPromiseDate        As String
        sShipDate           As String
        lSTaxClassKey       As Long
        dSTaxAmt            As Double
        dTradeDiscAmt       As Double
        dTradeDiscPct       As Double
        sComment            As String
        iHold               As Integer
        sHoldRsn            As String
        lSOLineKey          As Long
        iSOLineNo           As Integer
        iPriceOvrd          As Integer
        iReqCert            As Integer
        dQtyShipped         As Double
        dQtyInvcd           As Double
        dQtyRtrnCred        As Double
        dQtyRtrnRplc        As Double
        dAmtInvcd           As Double
        dOrigQtyOrd         As Double
        sOrigPromiseDate    As String
        lQuoteLineKey       As Long
        lSTaxTranKey        As Long
        lSTaxSchdKey        As Long
        lShipToSTaxSchdKey  As Long 'Ship To tax schedule
        lUpdateCounter      As Long
        lShipZoneKey        As Long
        lBlanketKey         As Long
        iCommentOnly        As Integer
        iInclOnPackList     As Integer
        iInclOnPickList     As Integer
        lPOKey              As Long
        lSOLineDistKey      As Long
        sUserFld1           As String
        sUserFld2           As String
        dQtyBO              As Double
        lShipToAddrKey      As Long
        iTaxPoint           As Integer
        iDeliveryMethod     As Integer
    End Type
    
    '-- Invoice Info structure
    Public Type uInvoiceInfo
        lInvcKey            As Long
        sCurrID             As String
        sInvcNo             As String
        dSalesAmt           As Double
        dFrtAmt             As Double
        dTradeDiscAmt       As Double
        dTradeDiscPct       As Double
        dSTaxAmt            As Double
        dTranAmt            As Double
        dTranAmtHC          As Double
        dPmtAmt             As Double
    End Type
    
    Public muInvoiceInfo       As uInvoiceInfo
    
    Public miMaxColumns As Integer 'grid customizations will determine the number of columns
   'Detail grid column indexes 'grid customizations will determine the actual indices
    Public mlColSOAllowImmedPick As Long
    Public mlColSOAllowPriceOvrd As Long
    Public mlColSOAllowDecimalQty As Long
    Public mlColSOAltOrdered As Long
    Public mlColSOAmtInvcd As Long
    Public mlColSOBlnktKey As Long
    Public mlColSOBlnktContract As Long
    Public mlColSOCloseDate As Long
    Public mlColSOCmntOnly As Long
    Public mlColSOCommClassID As Long
    Public mlColSOCommClassKey As Long
    Public mlColSOCommPlanID As Long
    Public mlColSOCommPlanKey As Long
    Public mlColSOCommentBtn As Long
    Public mlColSOGeneratePO As Long
    Public mlColSOCreateWOBtn As Long
    Public mlColSOCTPBtn As Long
    Public mlColSOCustomBTOKit As Long
    Public mlColSOCustomFldsBtn As Long
    Public mlColSOQuoteLineKey As Long
    Public mlColSODelvMethod As Long
    Public mlColSODelvMethodText As Long
    Public mlColSODescription As Long
    Public mlColSOEstimateKey As Long
    Public mlColSOEstimateNo As Long
    Public mlColSOExistingDelMeth As Long
    Public mlColSOExistingItemKey As Long
    Public mlColSOExistingLineActivity As Long
    Public mlColSOExistingUnitMeasKey As Long
    Public mlColSOExistingWhseKey As Long
    Public mlColSOExistingWOForLine As Long
    Public mlColSOExtCmnt As Long
    Public mlColSOExtAmt As Long
    Public mlColSOFOBID As Long
    Public mlColSOFOBKey As Long
    Public mlColSOFreightAmt As Long
    Public mlColSOGLAcctDesc As Long
    Public mlColSOGLAcctIDMsk As Long
    Public mlColSOGLAcctKey As Long
    Public mlColSOGLAcctRefID As Long
    Public mlColSOGLAcctRefKey As Long
    Public mlColSOItemID As Long
    Public mlColSOItemKey As Long
    Public mlColSOItemType As Long
    Public mlColSOHold As Long
    Public mlColSOHoldReason As Long
    Public mlColSOInclOnPick As Long
    Public mlColSOInclOnPack As Long
    Public mlColSOKitCompBtn As Long
    Public mlColSOMaintainWOBtn As Long
    Public mlColSONonInventory As Long
    Public mlColSOOrigItemKey As Long
    Public mlColSOOrigItemID As Long
    Public mlColSOOrigQtyOrd As Long
    Public mlColSOOrigPromiseDate As Long
    Public mlColSOOrigWhseKey As Long
    Public mlColSOOrigWhseID As Long
    Public mlColSOProdConfBtn As Long
    Public mlColSOPOLineKey As Long
    Public mlColSOPONum As Long
    Public mlColSOPriceOvrd As Long
    Public mlColSOPromiseDate As Long
    Public mlColSOQtyInvcd As Long
    Public mlColSOQtyBackOrd As Long
    Public mlColSOQtyOpen As Long
    Public mlColSOQtyOrdered As Long
    Public mlColSOQtyShipped As Long
    Public mlColSOQtyRtrnCred As Long
    Public mlColSOQtyRtrnRplc As Long
    Public mlColSOReqCert As Long
    Public mlColSORequestDate As Long
    Public mlColSOReset As Long
    Public mlColSOSalesPromoID As Long
    Public mlColSOSalesPromoKey As Long
    Public mlColSOSchedBtn As Long
    Public mlColSOSOLineNo As Long
    Public mlColSOSOLineKey As Long
    Public mlColSOSOLineDistKey As Long
    Public mlColSOShipComplete As Long
    Public mlColSOShipDate As Long
    Public mlColSOShipMethID As Long
    Public mlColSOShipMethKey As Long
    Public mlColSOShipPriority As Long
    Public mlColSOShipToAddrKey As Long
    Public mlColSOShipToAddrBtn As Long
    Public mlColSOShipToCustAddrKey As Long
    Public mlColSOShipToCustAddrID As Long
    Public mlColSOShipToShipDays As Long
    Public mlColSOShipZoneKey As Long
    Public mlColSOStatus As Long
    Public mlColSOStatusText As Long
    Public mlColSOSTaxSchdKey As Long 'actual bound column, determined by FOB, etc.
    Public mlColSOShipToSTaxSchdKey As Long 'Ship To's tax schedule
    Public mlColSOSTaxTranKey As Long
    Public mlColSOSTaxAmt As Long
    Public mlColSOSTaxBtn As Long
    Public mlColSOSTaxClassID As Long
    Public mlColSOSTaxClassKey As Long
    Public mlColSOSubjTradeDisc As Long
    Public mlColSOSysPriceDet As Long
    Public mlColSOSubstShipped As Long
    Public mlColSOTagOrderBtn As Long
    Public mlColSOTaxPoint As Long
    Public mlColSOTradeDiscAmt As Long
    Public mlColSOTradeDiscPct As Long
    Public mlColSOUnitMeasID As Long
    Public mlColSOUnitMeasKey As Long
    Public mlColSOUnitCost As Long
    Public mlColSOUnitPrice As Long
    Public mlColSOUnitPriceFromPromo As Long
    Public mlColSOUnitPriceFromSched As Long
    Public mlColSOUserFld1 As Long
    Public mlColSOUserFld2 As Long
    Public mlColSOVendID As Long
    Public mlColSOVendAddrBtn As Long
    Public mlColSOVendAddrID As Long
    Public mlColSOVendAddrKey As Long
    Public mlColSOVendKey As Long
    Public mlColSOWhseID As Long
    Public mlColSOWhseKey As Long
    Public mlColSOShipToCustUpdCtr As Long
    
    '************************************************************************
    'SGS PTC IA (START)
    '************************************************************************
    Public mlColSOAwarded As Long
    Public mlColSOUpdatedDate As Long
    Public mlColSOEstStartDate As Long
    Public mlColSOEstEndDate As Long
    Public mlColSOWeekendsIndicator As Long
    Public mlColSOUpdatedBy As Long
    Public mlColSOLoadsPerDay As Long
    Public mlColSOEstSpreaders As Long
    Public mlColSOFreightRate As Long
    Public mlColSOPayTruckRate As Long
    Public mlColSOMilesToJob As Long
    Public mlColSOOrigBidQty As Long
    
    Public mlColCertRequired As Long 'EB DEJ 11/7/12
    Public mlColQASample As Long 'EB DEJ 4/2/13
    Public mlColReqLoads As Long 'EB DEJ 4/2/13
    
    Public mlColCurrCnt As Long 'EB DEJ 4/11/13
    Public mlColTotalCnt As Long 'EB DEJ 4/11/13
    '************************************************************************
    'end SGS PTC IA
    '************************************************************************
    
    'Kit component grid columns
    Public Const kColSOCompKey = 1
    Public Const kColSOCompItemKey = 2
    Public Const kColSOCompItemID = 3
    Public Const kColSOCompItemDesc = 4
    Public Const kColSOCompItemQty = 5
    Public Const kColSOCompItemUOM = 6
    Public Const kColSOLineKey = 7
    Public Const kColSOAllowDec = 8
    Public Const kSOCompMaxCols = 8
    
       
    Public msCurrSymbol         As String
    Public msHomeCurrSymbol     As String
    Public miDocDigAfterDec     As Integer
    Public miHomeDigAfterDec    As Integer
    Public miRoundPrec          As Integer
    Public miHomeRoundPrec      As Integer
    Public miRoundMeth          As Integer
    Public miHomeRoundMeth      As Integer
    Public mlCurrencyLocale     As Long
    Public mlHomeCurrencyLocale As Long
    Public mbKitChanged         As Boolean
      

    'Constant for Alternative Stock Available / Out of Stock screen
    Public Const kContinue = 0
    Public Const kDropShip = 1
    Public Const kDeleLine = 2
    Public Const kUseQtyAvail = 3
    Public Const kBackOrd = 4
    Public Const kAlternate = 5
    Public Const kCancelLine = 6
    Public Const kTagOrder = 7
    Public Const kCreateWO = 8
    
    'Open and Closed lines
    Public Const kLineOpen = 1
    Public Const kLineClosed = 2
    
    'Tax form edit modes
    Public Const kModeReadOnly = 1                                 'No editing allowed on Tax form - read only
    Public Const kModeEditable = 2                                 'Editing allowed on tax form
    Public Const kModeScheduleOnly = 3                             'Editing is allowed only on Tax schedule field
    
    'Modole Option variables
    Public mbIMActivated               As Boolean
    Public mbIMIntegrated              As Boolean
    
    'Constatns for overshipment policy
    Public Const kOverShip_DisAllow = 0
    Public Const kOverShip_Allow = 1
    Public Const kOverShip_AllowWithWarning = 2
    
    'Constants for pick list report format
    Public Const kPickRptTypeSummary   As Integer = 1
    Public Const kPickRptTypeDetail    As Integer = 2
    Public Const kPickRptTypeOrder     As Integer = 3
    
    'Public Constants for calling task
    Public Const kCreatePickList = 1
    Public Const kReprintPickList = 2
    
    'Share String variables
    Public msQtyPicked                 As String

    'Line Grid Column Constants
    Public Const kColLinePickCheck = 1
    Public Const kColLineLineHold = 2
    Public Const kColLineItem = 3
    Public Const kColLineLineHoldReason = 4
    Public Const kColLineShiptoAddrID = 5
    Public Const kColLineTranTypeText = 6
    Public Const kColLineShipUOM = 7
    Public Const kColLineCompItemQty = 8
    Public Const kColLineQtyToPick = 9
    Public Const kColLineQtyPicked = 10
    Public Const kColLineQtyOrdered = 18
    Public Const kColLineOrderLineUOMID = 29
    Public Const kColLineQtyShort = 11
    Public Const kColLineQtyAvail = 12
    Public Const kColLineQtyDist = 13
    Public Const kColLineStatus = 14
    Public Const kColLineSubFlag = 15
    Public Const kColLineShipPriority = 16
    Public Const kColLineShipDate = 17
    Public Const kColLineSubItem = 31
    Public Const kColLineAutoDistBinID = 19
    Public Const kColLineDeliveryMethod = 20
    Public Const kColLineShipVia = 21
    Public Const kColLineOrderNo = 22
    Public Const kColLineLine = 23
    Public Const kColLineShipWhseID = 24
    Public Const kColLineShiptoName = 25
    Public Const kColLineShiptoAddr = 26
    Public Const kColLineItemDesc = 27
    Public Const kColLineSASequence = 28
    Public Const kColLineAllowDecimalQty = 73
    Public Const kColLineRcvgWarehouse = 74
    Public Const kColLineItemType = 75
    Public Const kColLineTranKey = 32
    Public Const kColLineTranType = 33
    Public Const kColLineTranLineKey = 34
    Public Const kColLineTranLineDistKey = 35
    Public Const kColLineAllowSubItem = 36
    Public Const kColLineTrackMeth = 37
    Public Const kColLineTrackQtyAtBin = 38
    Public Const kColLineInvtTranKey = 39
    Public Const kColLineShipLineDistKey = 40
    Public Const kColLineOrderLineUOMKey = 41
    Public Const kColLineShipUOMKey = 42
    Public Const kColLineInvtTranID = 43
    Public Const kColLineSubItemKey = 44
    Public Const kColLineKitShipLineKey = 45
    Public Const kColLineCrHold = 46
    Public Const kColLineOrdHold = 47
    Public Const kColLineItemKey = 48
    Public Const kColLineShipWhseKey = 49
    Public Const kColLineShipLineKey = 50
    Public Const kColLineShortPickFlag = 51
    Public Const kColLineCurrDisplayRow = 52
    Public Const kColLineCustReqShipComplete = 53
    Public Const kColLineItemReqShipComplete = 54
    Public Const kColLineAllowImmedShipFromPick = 55
    Public Const kColLineAutoDist = 56
    Public Const kColLineAutoDistBinQtyAvail = 57
    Public Const kColLineAutoDistBinUOMKey = 58
    Public Const kColLineAutoDistLotNo = 59
    Public Const kColLineAutoDistLotExpDate = 60
    Public Const kColLineDistComplete = 61
    Public Const kColLineShipKey = 62
    Public Const kColLineShipmentCommitStatus = 63
    Public Const kColLineShipLineUpdateCounter = 64
    Public Const kColLineShipLineDistUpdateCounter = 65
    Public Const kColLineExtShipmentExists = 66
    Public Const kColLinePacked = 67
    Public Const kColLineStatusCustShipCompleteViolation = 68
    Public Const kColLineStatusItemShipCompLeteViolation = 69
    Public Const kColLineStatusDistWarning = 70
    Public Const kColLineStatusDistQtyShort = 71
    Public Const kColLineStatusOverShipment = 72
    Public Const kColLineQtyToPickByOrdUOM = 30
    Public Const kColLineWhseUseBin = 76
    Public Const kColLineAutoDistLotKey = 77
    Public Const kColLineAutoDistBinKey = 78

    'Maximum columns to display in grid
    Public Const kMaxLineCols = 78
    Public Const kMaxQtyLen = "99999999.99999999"
    Public Const kColFrozenCols = 7
    
    'Misc constants
    Public Const kNo = 0
    Public Const kYes = 1
       
    'Misc constants
    Public Const kAllowDecimalPlaces = 1
    Public Const kDisAllowDecimalPlaces = 0
    Public Const kAutoSubstituionUsed = 1
    Public Const kManualSubstituionUsed = 0
    
    Public Const kDeleteTempTableYes = 1
    Public Const kDeleteTempTableNo = 0
    Public Const kDeleteDistributionOnlyYes = 1
    Public Const kDeleteDistributionOnlyNo = 0

    Public Const kTM_None      As Integer = 0
    Public Const kTM_Lot       As Integer = 1
    Public Const kTM_Serial    As Integer = 2
    Public Const kTM_Both      As Integer = 3

    Public Const kFinishedGood As Integer = 5
    Public Const kRawMaterial As Integer = 6
    Public Const kBTOKit As Integer = 7
    Public Const kPreAssembledKit As Integer = 8

    Public Const kDistBin_LkuRetCol_BinID As Integer = 1
    Public Const kDistBin_LkuRetCol_BinKey As Integer = 2
    Public Const kDistBin_LkuRetCol_QtyOnHand As Integer = 3
    Public Const kDistBin_LkuRetCol_AvailQty As Integer = 4
    Public Const kDistBin_LkuRetCol_UOMKey As Integer = 5

    Public Const kDistBinLot_LkuRetCol_BinID As Integer = 1
    Public Const kDistBinLot_LkuRetCol_BinKey As Integer = 2
    Public Const kDistBinLot_LkuRetCol_LotNo As Integer = 3
    Public Const kDistBinLot_LkuRetCol_LotKey As Integer = 4
    Public Const kDistBinLot_LkuRetCol_ExpDate As Integer = 5
    Public Const kDistBinLot_LkuRetCol_QtyOnHand As Integer = 6
    Public Const kDistBinLot_LkuRetCol_AvailQty As Integer = 7
    Public Const kDistBinLot_LkuRetCol_UOMKey As Integer = 8
        
    
#If Prototype <> 1 Then
Const VBRIG_MODULE_ID_STRING = "sozdj101.BAS"


Public Sub MyErrMsg(oClass As Object, sDesc As String, lErr As Long, sSub As String, sProc As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sText As String

    If lErr = guSotaErr.Number And Trim(guSotaErr.Description) <> Trim(sDesc) Then
        sDesc = sDesc & " " & guSotaErr.Description
    End If

    If oClass Is Nothing Then
        sText = sText & " AppName:  " & App.Title
        sText = sText & "    Error < " & Format(lErr)
        sText = sText & " >   occurred at < " & sSub
        sText = sText & " >  in Procedure < " & sProc
        sText = sText & " > " & sDesc
    Else
        sText = "Module: " & oClass.moSysSession.MenuModule
        sText = sText & "  Company: " & oClass.moSysSession.CompanyID
        sText = sText & "  AppName:  " & App.Title
        sText = sText & "    Error < " & Format(lErr)
        sText = sText & " >   occurred at < " & sSub
        sText = sText & " >  in Procedure < " & sProc
        sText = sText & " > " & sDesc
    End If
    
    MsgBox sText, vbExclamation, "Sage 500 ERP"

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyErrMsg", VBRIG_IS_MODULE                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

#End If
Public Sub Main()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("sozdj101.clsSalesOrder")
        StartAppInStandaloneMode oApp, Command$()
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Main", VBRIG_IS_MODULE                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basSalesOrder"
End Function

Public Sub ShowFreightDialog(oClass As Object, ByVal bTrackTaxes As Boolean, ByVal bReadOnly As Boolean, _
    oSTax As Object, oGrid As fpSpread, oDmGrid As clsDmGrid, ByVal iModule As Integer, ByVal iDocQtyDecPlaces As Integer, _
    ByVal iDocAmtDecPlaces As Integer, dTotalSTaxAmt As Double, dTotalFreightAmt As Double)
'This routine is called from the freight command button on the sales order totals form, and the
'sales order payments form.

  On Error GoTo ExpectedErrorRoutine

    Dim oFrt            As Object       'Freight Object
    'Dim lChildRow       As Long         'Child Row
    Dim lParentRow      As Long         'Parent Row

    Dim lLineDistKey    As Long         'Line Distribution Surrogate Key
    Dim sShipMethID     As String       'Ship Method ID
    Dim lShipMethKey    As Long         'Ship Method key
    Dim lShipToCAddrKey As Long         'Customer ship to address key for line
    Dim lSTaxClassKey   As Long         'Sales tax class key
    Dim dQuantity       As Double       'Quantity
    Dim dExtAmt         As Double       'Extended Amount
    Dim dFrtAmt         As Double       'Freight Amount
    Dim bChangesMade    As Boolean      'Changes Were Made
    Dim bRecalcTaxes    As Boolean      'Recalculate Taxes
    Dim dOldFrtAmt      As Double       'Old Freight Amount for Line
    Dim bChangedClosed  As Boolean
    Dim iStatus         As Integer
    Dim bClosedLines    As Boolean
    Dim iSOLineNo       As Integer
    Dim lSOLineKey      As Long
    Dim sItemID         As String
    Dim sItemDesc       As String
    Dim lSTaxTranKey    As Long
    Dim dLineTax        As Double
    Dim lCurrRow        As Long
    Dim dDiscAmt        As Double
    Dim iFreightAllocMeth As Integer
    Dim lTranUOMKey As Long
    Dim bFormExitNormal As Boolean
    Dim lDataRows       As Long

'*************************************************************
'*  Initialize the freight allocations object/form
'*************************************************************

  'Create the freight allocation object
    Set oFrt = CreateObject("cizdcdl1.clsFrtAlloc")

  'An error occurred Creating the freight allocation object
    If oFrt Is Nothing Then
        MsgBox "An error occurred bringing up the freight form", , "Sage 500 ERP"
        Exit Sub
    End If

  'Initialize the freight allocation class
    oFrt.Init oClass.moSysSession, oClass.moAppDB, oClass.moFramework

    
  'Set up the freight allocation class form
    oFrt.bSetupForm iModule, iDocQtyDecPlaces, iDocAmtDecPlaces, 0, CInt(bTrackTaxes * -1), bReadOnly

'*************************************************************
'*  Load the freight allocations grid
'*************************************************************
  'Loop through the parent to read the grid
    SetHourglass True

    lDataRows = oGrid.DataRowCnt

    'If SO is empty, do not add rows in Freight grid
    If glGetValidLong(gsGridReadCell(oGrid, 1, mlColSOItemKey)) = 0 _
        And Len(gsGridReadCell(oGrid, 1, mlColSODescription)) = 0 Then
        lDataRows = lDataRows - 1
    End If
    
    'Assume positioned on first row of grid
    For lParentRow = 1 To lDataRows

        If giGetValidInt(gsGridReadCell(oGrid, lParentRow, mlColSOStatus)) = kLineClosed And _
            giGetValidInt(gsGridReadCell(oGrid, lParentRow, mlColSOCmntOnly)) = 0 Then

            bClosedLines = True
        End If

        sShipMethID = gsGridReadCell(oGrid, lParentRow, mlColSOShipMethID)
        dExtAmt = gdGetValidDbl(gsGridReadCell(oGrid, lParentRow, mlColSOExtAmt))
        dQuantity = gdGetValidDbl(gsGridReadCell(oGrid, lParentRow, mlColSOQtyOrdered))
        dFrtAmt = gdGetValidDbl(gsGridReadCell(oGrid, lParentRow, mlColSOFreightAmt))

        iSOLineNo = giGetValidInt(gsGridReadCell(oGrid, lParentRow, mlColSOSOLineNo))
        lSOLineKey = glGetValidLong(gsGridReadCell(oGrid, lParentRow, mlColSOSOLineKey))
        sItemID = gsGridReadCell(oGrid, lParentRow, mlColSOItemID)
        sItemDesc = gsGridReadCell(oGrid, lParentRow, mlColSODescription)
        lTranUOMKey = glGetValidLong(gsGridReadCell(oGrid, lParentRow, mlColSOUnitMeasKey))
        
        lLineDistKey = glGetValidLong(gsGridReadCell(oGrid, lParentRow, mlColSOSOLineDistKey))
        oFrt.bLoadGrid CStr(iSOLineNo), lSOLineKey, lLineDistKey, sItemID, sItemDesc, 0, dQuantity, lTranUOMKey, dExtAmt, sShipMethID, dFrtAmt
  
        'move to next row
        If lParentRow < oGrid.DataRowCnt Then
            oDmGrid.LeaveRow lParentRow, lParentRow + 1
        Else
            oDmGrid.LeaveRow lParentRow, -1
        End If
        
    Next lParentRow
    SetHourglass False

'*************************************************************
'*  Show Freight Allocation form
'*************************************************************
    If bClosedLines Then
        bFormExitNormal = oFrt.ShowFrt(bChangesMade, dTotalFreightAmt, bRecalcTaxes, iFreightAllocMeth, True)
    Else
        bFormExitNormal = oFrt.ShowFrt(bChangesMade, dTotalFreightAmt, bRecalcTaxes, iFreightAllocMeth, False)
    End If

    If bFormExitNormal Then

        '*************************************************************
        '*  Update the grid
        '*************************************************************
    
      'Changes were made go back and update the grid
        If bChangesMade Then
        
            'Set row to last row entered (via oDmGrid.LeaveRow) in previous loop
            lCurrRow = oGrid.DataRowCnt
        
            SetHourglass True
            For lParentRow = 1 To oGrid.DataRowCnt
    
              'Now set the specified row active
               oDmGrid.LeaveRow lCurrRow, lParentRow
               lCurrRow = lParentRow
        
                       
                lLineDistKey = glGetValidLong(gsGridReadCell(oGrid, lParentRow, mlColSOSOLineDistKey))
                  'Get Freight Back for line distribution Surrogate Key
                If oFrt.bReturnFrt(lLineDistKey, dFrtAmt, bChangesMade) Then
    
                      'Changes were made update the grid
                        If bChangesMade Then
                          'Check if freight was allocated to a closed line
                            If Not bChangedClosed Then
                                dOldFrtAmt = gdGetValidDbl(gsGridReadCell(oGrid, lParentRow, mlColSOFreightAmt))
                                If dOldFrtAmt <> dFrtAmt Then
                                    iStatus = giGetValidInt(gsGridReadCell(oGrid, lParentRow, mlColSOStatus))
                                    
                                    If iStatus = 2 Then
                                        bChangedClosed = True
                                    End If
                                End If
                            End If
    
                            gGridUpdateCell oGrid, lParentRow, mlColSOFreightAmt, CStr(dFrtAmt)
    
                            oDmGrid.SetRowDirty lParentRow
    
                            If bTrackTaxes Then
                            
                                lSTaxClassKey = glGetValidLong(gsGridReadCell(oGrid, lParentRow, mlColSOSTaxClassKey))
                                lShipToCAddrKey = glGetValidLong(gsGridReadCell(oGrid, lParentRow, mlColSOShipToCustAddrKey))
                                lShipMethKey = glGetValidLong(gsGridReadCell(oGrid, lParentRow, mlColSOShipMethKey))
                                dExtAmt = gdGetValidDbl(gsGridReadCell(oGrid, lParentRow, mlColSOExtAmt))
                                dDiscAmt = gdGetValidDbl(gsGridReadCell(oGrid, lParentRow, mlColSOTradeDiscAmt))
                                lSTaxTranKey = glGetValidLong(gsGridReadCell(oGrid, lParentRow, mlColSOSTaxTranKey))
                                
                                'Load the line taxes
                                moSTax.bLoadDistTaxes lSTaxTranKey, lSTaxClassKey, lShipMethKey, dQuantity, _
                                    dExtAmt - dDiscAmt, dOldFrtAmt, lShipToCAddrKey
                                
                                If Not moSTax.bUpdateFreightTaxes(bRecalcTaxes, dFrtAmt, lSTaxTranKey, dLineTax) Then
                                MsgBox "An error ocurred updating freight on taxes", , "Sage 500 ERP"
                                End If
                            End If
                        End If
    
                    End If
                    
                If bTrackTaxes Then
                   gGridUpdateCell oGrid, lParentRow, mlColSOSTaxAmt, CStr(moSTax.dLineTax)
                End If
    
            Next lParentRow
            SetHourglass False
        End If
    
    End If
    
    'Reposition to the first row in the grid in case user clicks button again
    oDmGrid.LeaveRow oGrid.DataRowCnt, 1
   
  'Set total tax amount for return to caller
    If bTrackTaxes Then
        dTotalSTaxAmt = moSTax.dTotalDocTax
    End If

  'Unload the freight object
    oFrt.UnloadMe

 'Clean up
    Set oFrt = Nothing

    If bChangedClosed Then
        MsgBox "Freight has been allocated to lines which are closed.", , "Sage 500 ERP"
    End If

    SetHourglass False

Exit Sub

ExpectedErrorRoutine:

SetHourglass False
'Clean up
If Not oFrt Is Nothing Then

  'Unload the freight object
    oFrt.UnloadMe
    Set oFrt = Nothing

End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ShowFreightDialog", VBRIG_IS_MODULE                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function bOverrideSecEvent(sEventId As String, oClass As Object, Optional vPrompt As Variant = True)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
'***************************************************************
' Can the user override the security event passed in
'***************************************************************
Dim sUser       As String

    bOverrideSecEvent = False   'Set initial return code

  'User is Authorized set good return code
  'Check the user id typed in can override (no cancel hit)
    sUser = CStr(oClass.moSysSession.UserId)
    
    If oClass.moFramework.GetSecurityEventPerm(sEventId, sUser, vPrompt) <> 0 Then
        bOverrideSecEvent = True
    End If

Exit Function
                                                                       'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bOverrideSecEvent", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Public Function CalcOrderTotals(ByVal dTotSTaxNoTaxTrack As Double, ByVal oGrid As Object, ByVal bTrackTaxes As Boolean, ByVal dExchRate As Double, _
    ByVal iHomeDigAfterDec As Integer, ByVal iDocDigAfterDec As Integer, dTotExtAmt As Double, dTotExtAmtHC As Double, _
    dTotTDAmt As Double, dTotFrtAmt As Double, dTranAmtHC As Double, dTranAmt As Double, dTotShipAmt As Double, dTotShipAmtHC As Double, _
    dInvcdAmt As Double, dTotOpenAmt As Double, dTotOpenAmtHC As Double)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim iStatus         As Integer
    Dim dUnitPrice      As Double
    Dim dQtyOrd         As Double
    Dim dShipQty        As Double
    Dim dQtyOpen        As Double
    Dim dShipAmt        As Double
    Dim dShipAmtHC      As Double
    Dim dInvcdQty       As Double
    Dim dOpenAmt        As Double
    Dim dOpenAmtHC      As Double
    Dim dExtAmt         As Double
    Dim dExtAmtHC       As Double
    Dim dFreightAmt     As Double
    Dim dSTaxAmt        As Double
    Dim dTDAmt          As Double
    Dim dTDAmtHC        As Double
    
    Dim dTotFrtAmtHC    As Double
    Dim dTotSTaxAmtHC   As Double
    Dim dTotSTaxAmt     As Double
    Dim dTotTDAmtHC     As Double
    Dim lRow            As Long

    For lRow = 1 To oGrid.DataRowCnt

        dQtyOrd = Val(Trim(gsGridReadCell(oGrid, lRow, mlColSOQtyOrdered)))
        
        dShipQty = Val(Trim(gsGridReadCell(oGrid, lRow, mlColSOQtyShipped)))

        dUnitPrice = Val(Trim(gsGridReadCell(oGrid, lRow, mlColSOUnitPrice)))

        dShipAmt = gfRound((dUnitPrice * dShipQty), 1, iDocDigAfterDec, 1)
        dShipAmtHC = gfRound((dShipAmt * dExchRate), 1, iHomeDigAfterDec, 1)

        dTDAmt = Val(Trim(gsGridReadCell(oGrid, lRow, mlColSOTradeDiscAmt)))
        dTDAmtHC = gfRound((dTDAmt * dExchRate), 1, iHomeDigAfterDec, 1)

        dExtAmt = Val(Trim(gsGridReadCell(oGrid, lRow, mlColSOExtAmt)))
        dExtAmtHC = gfRound((dExtAmt * dExchRate), 1, iHomeDigAfterDec, 1)

        dTotTDAmt = dTDAmt + dTotTDAmt
        dTotTDAmtHC = dTDAmtHC + dTotTDAmtHC
        dTotShipAmt = dShipAmt + dTotShipAmt
        dTotShipAmtHC = dShipAmtHC + dTotShipAmtHC
        dTotExtAmt = dTotExtAmt + dExtAmt
        dTotExtAmtHC = dTotExtAmtHC + dExtAmtHC

        iStatus = Val(Trim(gsGridReadCell(oGrid, lRow, mlColSOStatus)))
        If iStatus = kLineClosed Then
            dQtyOpen = 0
            dOpenAmt = 0
            dOpenAmtHC = 0
        Else
            dQtyOpen = Val(Trim(gsGridReadCell(oGrid, lRow, mlColSOQtyOpen)))
            dOpenAmt = gfRound((dUnitPrice * dQtyOpen), 1, iDocDigAfterDec, 1)
            dOpenAmtHC = gfRound((dUnitPrice * dQtyOpen * dExchRate), 1, iHomeDigAfterDec, 1)
        End If

        If dQtyOrd < 0 Then

            If dOpenAmt < 0 Then
                dTotOpenAmt = dTotOpenAmt + dOpenAmt
                dTotOpenAmtHC = dTotOpenAmtHC + dOpenAmtHC
            End If

        Else

            If dOpenAmt > 0 Then
                dTotOpenAmt = dTotOpenAmt + dOpenAmt
                dTotOpenAmtHC = dTotOpenAmtHC + dOpenAmtHC
            End If

        End If

        dInvcdQty = Val(Trim(gsGridReadCell(oGrid, lRow, mlColSOQtyInvcd)))
        dInvcdAmt = dInvcdAmt + Val(Trim(gsGridReadCell(oGrid, lRow, mlColSOAmtInvcd)))

        dFreightAmt = Val(Trim(gsGridReadCell(oGrid, lRow, mlColSOFreightAmt)))
        dTotFrtAmtHC = dTotFrtAmtHC + gfRound((dFreightAmt * dExchRate), 1, iHomeDigAfterDec, 1)
        dTotFrtAmt = dTotFrtAmt + dFreightAmt

    Next lRow
    
       
    'Recalculate Total = Sales + Freight + Sales Tax - Trade Discounts
    If bTrackTaxes Then
        dTranAmtHC = dOrderTotal(dTotExtAmtHC, dTotFrtAmtHC, dTotTDAmtHC, gfRound(moSTax.dTotalDocTaxHC(dExchRate, miHomeDigAfterDec), 1, iHomeDigAfterDec))
        dTranAmt = dOrderTotal(dTotExtAmt, dTotFrtAmt, dTotTDAmt, moSTax.dTotalDocTax)
    Else
        dTranAmtHC = dOrderTotal(dTotExtAmtHC, dTotFrtAmtHC, dTotTDAmtHC, gfRound(dTotSTaxNoTaxTrack * dExchRate, 1, iHomeDigAfterDec))
        dTranAmt = dOrderTotal(dTotExtAmt, dTotFrtAmt, dTotTDAmt, dTotSTaxNoTaxTrack)
    End If
        
    dTotShipAmt = gfRound(dTotShipAmt, 1, iDocDigAfterDec)
    dTotShipAmtHC = gfRound(dTotShipAmtHC, 1, iHomeDigAfterDec)

    dInvcdAmt = gfRound(dInvcdAmt, 1, iDocDigAfterDec)

    dTotOpenAmt = gfRound(dTotOpenAmt, 1, iDocDigAfterDec)
    
    'curMsgBox.IntegralPlaces = 4
    'curMsgBox.DecimalPlaces = curTranAmtHC.DecimalPlaces
    'curMsgBox.IntegralPlaces = curTranAmtHC.IntegralPlaces
    'curMsgBox.Amount = dTotOpenAmtHC
    'CurOpenAmtHC.Amount = dTotOpenAmtHC

    dTotOpenAmtHC = gfRound(dTotOpenAmtHC, 1, iHomeDigAfterDec)

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CalcOrderTotals", VBRIG_IS_MODULE                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Public Function dOrderTotal(dSalesAmt As Double, dFreightAmt As Double, _
                      dTradeDiscAmt As Double, dSalesTaxAmt As Double) As Double
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    dOrderTotal = dSalesAmt + dFreightAmt - dTradeDiscAmt + dSalesTaxAmt
 
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "dOrderTotal", VBRIG_IS_MODULE                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Sub CheckCredit(bShowForm As Boolean, iCustHold As Integer, bCheckCredit As Boolean, iTranType As Integer, _
    lCustKey As Long, sCustID As String, sCustName As String, oDmForm As clsDmForm, ByVal oGrid As Object, _
    oCredForm As Object, oClass As Object, oSotaObjects As Object, bReadOnly As Boolean, _
    iCreditCheckType As Integer)
'bShowForm - if true, the user got here by clicking on the credit status button on the
'            order totals form, so always show the form.
    Dim lSOKey As Long
    Dim dOvrdAmt As Double
    Dim iCredHold As Integer
    Dim sTranDesc As String
    Dim lMsgNo As Long
    
    Dim lRow As Long
    Dim iStatus As Integer
    Dim dQtyOpen As Double
    Dim dQtyOrd As Double
    Dim dSTax As Double
    Dim dTradeDisc As Double
    Dim dExtAmt As Double
    Dim dFreightAmt As Double
    Dim dExchRate As Double
    Dim dOpenAmtHC As Double
    Dim dTotOpenAmtHC As Double
    Dim dQtyRatio As Double
    
    dExchRate = gdGetValidDbl(oDmForm.GetColumnValue("CurrExchRate"))
    For lRow = 1 To oGrid.DataRowCnt
        iStatus = Val(Trim(gsGridReadCell(oGrid, lRow, mlColSOStatus)))
        If iStatus = kLineOpen Then
            dQtyOpen = Val(Trim(gsGridReadCell(oGrid, lRow, mlColSOQtyOpen)))
            dQtyOrd = Val(Trim(gsGridReadCell(oGrid, lRow, mlColSOQtyOrdered)))
            dSTax = Val(Trim(gsGridReadCell(oGrid, lRow, mlColSOSTaxAmt)))
            dTradeDisc = Val(Trim(gsGridReadCell(oGrid, lRow, mlColSOTradeDiscAmt)))
            dExtAmt = Val(Trim(gsGridReadCell(oGrid, lRow, mlColSOExtAmt)))
            dFreightAmt = Val(Trim(gsGridReadCell(oGrid, lRow, mlColSOFreightAmt)))
            If dQtyOrd = 0 Then
               dQtyRatio = 0
            Else
               dQtyRatio = dQtyOpen / dQtyOrd
            End If
            
            dOpenAmtHC = gfRound(((dExtAmt - dTradeDisc + dFreightAmt + dSTax) * dExchRate * dQtyRatio), 1, miHomeDigAfterDec, 1)
            dTotOpenAmtHC = dTotOpenAmtHC + dOpenAmtHC

        End If
    Next lRow
    
    'Need to round both of these to HomeCurr digits to ensure a fair comparison
    dOvrdAmt = gfRound(gdGetValidDbl(oDmForm.GetColumnValue("CreditApprovedAmt")), 1, miHomeDigAfterDec, 1)
    dTotOpenAmtHC = gfRound(dTotOpenAmtHC, 1, miHomeDigAfterDec, 1)
    
    If dTotOpenAmtHC <= dOvrdAmt And Not bShowForm Then
        Exit Sub
    End If

    lSOKey = glGetValidLong(oDmForm.GetColumnValue("SOKey"))
    iCredHold = giGetValidInt(oDmForm.GetColumnValue("CrHold"))
    sTranDesc = gsBuildString(kSalesOrderTbl, oClass.moAppDB, oClass.moSysSession)

    If oCredForm Is Nothing Then
        Set oCredForm = CreateObject("InvcCred.ClsInvcCred")
        oCredForm.Init oClass.moSysSession, oClass.moAppDB, oClass.moSysSession.CompanyID, _
            oClass.moSysSession.EnterAsTab, oClass.moAppDB, oSotaObjects, oClass.moFramework
    End If
    
    oCredForm.bReadOnly = bReadOnly 'disallow overrides
    
    
    If iCreditCheckType = kiCreditCheckSOEntry Then
   
        lMsgNo = oCredForm.CalcAdjSOBal(dTotOpenAmtHC, lCustKey, lSOKey, dOvrdAmt, _
                iCredHold, iCustHold, iTranType, bCheckCredit, sTranDesc)
    Else
        
        lMsgNo = oCredForm.CalcAdjShipBal(dTotOpenAmtHC, lCustKey, lSOKey, dOvrdAmt, _
                iCredHold, iCustHold, iTranType, True, sTranDesc)
    End If
    
    If lMsgNo > 0 And Not bShowForm Then
        giSotaMsgBox Nothing, oClass.moSysSession, lMsgNo
    End If
        
    If iCredHold = 1 Or bShowForm Then
    
        'Show the credit Limit form
        oCredForm.ShowMe sCustID, sCustName
        iCredHold = oCredForm.iCredHold
            
        'If overridden...
        If Not bReadOnly Then
            If Len(gsGetValidStr(oCredForm.AuthOvrdUser)) > 0 Then
                oDmForm.SetColumnValue "CreditAuthUserID", gsGetValidStr(oCredForm.AuthOvrdUser)
                oDmForm.SetColumnValue "CreditApprovedAmt", dTotOpenAmtHC
            Else
                oDmForm.SetColumnValue "CreditAuthUserID", Empty
                oDmForm.SetColumnValue "CreditApprovedAmt", 0
            End If
        End If
    Else
        If Not bReadOnly Then
            oDmForm.SetColumnValue "CreditAuthUserID", Empty
            oDmForm.SetColumnValue "CreditApprovedAmt", 0
        End If
    
    End If
        
    If Not bReadOnly Then
        oDmForm.SetColumnValue "CrHold", iCredHold
    End If
    
   
End Sub

Public Function dCalculatePendAmtInvcd(ByVal oClass As Object, ByVal dExchangeRate As Double, ByVal iHomePriceDecPlaces As Integer, ByVal lSOKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

Dim dPendAmtInvcd As Double
Dim sSQL As String
Dim rs As Object

'Pending invoice amounts, converted to home currency.
    sSQL = "SELECT SUM(ROUND(tarInvoiceLineDist.ExtAmt * CONVERT(dec(38,20), CurrExchRate), " & iHomePriceDecPlaces & ")) PendInvcAmt "
    sSQL = sSQL & "FROM  tarInvoiceLineDist WITH (NOLOCK) "
    sSQL = sSQL & "JOIN  tarInvoiceDetl WITH (NOLOCK) "
    sSQL = sSQL & "ON    tarInvoiceLineDist.InvoiceLineKey = tarInvoiceDetl.InvoiceLineKey "
    sSQL = sSQL & "JOIN  tarPendInvoice WITH (NOLOCK) "
    sSQL = sSQL & "ON    tarPendInvoice.InvcKey = tarInvoiceDetl.InvcKey "
    sSQL = sSQL & "JOIN  tsoSOLine WITH (NOLOCK) "
    sSQL = sSQL & "ON    tsoSOLine.SOLineKey = tarInvoiceDetl.SOLineKey "
    sSQL = sSQL & "WHERE tsoSOLine.SOKey = " & lSOKey

    Set rs = oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If Not rs Is Nothing Then
        dPendAmtInvcd = gdGetValidDbl(rs.Field("PendInvcAmt"))
        
        'Protect against divide by 0 - user will have already received a warning at this
        'point.
        If dExchangeRate = 0 Then
            dExchangeRate = 1
        End If
        'convert to sales order currency
        If dExchangeRate <> 1 Then
            dPendAmtInvcd = dPendAmtInvcd * 1 / dExchangeRate
        End If
        Set rs = Nothing
    Else
        dPendAmtInvcd = 0
    End If
    
    dCalculatePendAmtInvcd = dPendAmtInvcd

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "dCalculatePendAmtInvcd", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Public Function dCalculatePrePaymentsNC(ByVal oClass As Object, ByVal lSOKey As Long) As Double
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String
Dim rs   As Object
    
    sSQL = "SELECT "
    sSQL = sSQL & "COALESCE((SELECT SUM(AssignedAmt) "
    sSQL = sSQL & "FROM tarSalesOrderPmt WITH (NOLOCK) "
    sSQL = sSQL & "JOIN tarCustPmt WITH (NOLOCK) "
    sSQL = sSQL & "ON tarSalesOrderPmt.CustPmtKey = tarCustPmt.CustPmtKey "
    sSQL = sSQL & "LEFT OUTER JOIN tccTranLog WITH (NOLOCK) "
    sSQL = sSQL & "ON tccTranLog.CustPmtKey = tarCustPmt.CustPmtKey"
    sSQL = sSQL & "WHERE SOKey = " & lSOKey & " AND ShipKey IS NULL),0) "
    sSQL = sSQL & "AND COALESCE(TranState, 1) <> 7),0) " 'exclude declined credit card payments (if CC module activated)
    sSQL = sSQL & "+ "
    sSQL = sSQL & "COALESCE((SELECT SUM(AssignedAmt) "
    sSQL = sSQL & "FROM tarSalesOrderPmt WITH (NOLOCK) "
    sSQL = sSQL & "JOIN tarInvoice WITH (NOLOCK) "
    sSQL = sSQL & "ON tarSalesOrderPmt.CreditMemoKey = tarInvoice.InvcKey "
    sSQL = sSQL & "WHERE SOkey = " & lSOKey & " AND ShipKey IS NULL),0) "
    sSQL = sSQL & "+ "
    sSQL = sSQL & "COALESCE((SELECT SUM(AssignedAmt) "
    sSQL = sSQL & "FROM tarSalesOrderPmt WITH (NOLOCK) "
    sSQL = sSQL & "JOIN tarPendCustPmt WITH (NOLOCK) "
    sSQL = sSQL & "ON tarSalesOrderPmt.CustPmtKey = tarPendCustPmt.CustPmtKey "
    sSQL = sSQL & "LEFT OUTER JOIN tccTranLog WITH (NOLOCK) "
    sSQL = sSQL & "ON tccTranLog.CustPmtKey = tarSalesOrderPmt.CustPmtKey "
    sSQL = sSQL & "WHERE SOkey = " & lSOKey & " AND ShipKey IS NULL), 0) "
    sSQL = sSQL & "AND COALESCE(TranState, 1) <> 7),0) PrePmtAmt" 'exclude declined credit card payments (if CC module activated)
    
    Set rs = oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If Not rs Is Nothing Then
        dCalculatePrePaymentsNC = rs.Field("PrePmtAmt")
        Set rs = Nothing
    Else
        dCalculatePrePaymentsNC = 0
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "dCalculatePrePaymentsNC", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function




Public Function lStartErrorReport(sButton As String, frm As Form, lSessionID As Long, _
                        bSkipPrintDialog As Boolean, bCancelPrint As Boolean) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++

Dim iRetVal As Integer
Dim sTitle1 As String, sTitle2 As String
Dim iSeverity As Integer
Dim sRestriction As String

    lStartErrorReport = kFailure

    On Error GoTo badexit
    
    '*************** NOTE ********************
    'the order of the following events is important!
    
    'the .RPT file name can be set acording to user selected options
    'this call is only necessary if you need to run a report that is
    'different from the default
    frm.moReport.ReportFileName() = "sozdv003.rpt"
    
    'start Crystal print engine, open print job
    'get strings for report
    If (frm.moReport.lSetupReport = kFailure) Then
        MsgBox "SetupReport failed"
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    frm.moReport.Orientation() = vbPRORLandscape
    
    'there are two report title fields in the report template.
    'the strings themselves should come out of the string table
    
    frm.moReport.ReportTitle1() = "Process SO"
'    frm.moReport.ReportTitle2() = gsBuildString(kSOSRRptErrorTitle, frm.oClass.moAppDB, frm.oClass.moSysSession)
    
    'automated means of labeling subtotal fields.
    'this is not manditory if you choose not to use this feature
    frm.moReport.UseSubTotalCaptions() = 0
    frm.moReport.UseHeaderCaptions() = 0
    
    If (frm.moReport.lSetStandardFormulas(frm) = kFailure) Then
        MsgBox "SetStandardFormulas failed"
        GoTo badexit
    End If
        
    frm.moReport.BuildSQL
    
    '**** Any additional sorts outside of sort grid may be added here ****'
    'If this is necessary make the call, frm.moReport.AppendSQL(", table.column ASC")
    
    'send the formatted SQL statement from above plus any further manipulations
    'to Crystal
    frm.moReport.SetSQL

    
    frm.moReport.SetReportCaptions
    
    iSeverity = frm.cboSeverity.ItemData(frm.cboSeverity.ListIndex)
    
    If iSeverity > 0 Then
        sRestriction = "{tciErrorLog.SessionID} = " & lSessionID & " AND {tciErrorLog.Severity} = " & iSeverity
    Else
        sRestriction = "{tciErrorLog.SessionID} = " & lSessionID
    End If

    If (frm.moReport.lRestrictBy(sRestriction) = kFailure) Then
        MsgBox "RestrictBy failed"
        GoTo badexit
    End If

    frm.moReport.ProcessReport frm, sButton, , , bSkipPrintDialog, bCancelPrint   '   Added parameters. HBS.

'    If sButton = kTbPrint Then
'        bSkipPrintDialog = True    '   don't show print dialog more than once. HBS
'    End If
    
    lStartErrorReport = kSuccess

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

badexit:
    frm.sbrMain.Status = SOTA_SB_START
    
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lStartErrorReport", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

'***************************************************************************************************************************
'***************************************************************************************************************************
'***************************************************************************************************************************
'SGS DEJ 5/4/2010 - Insert/Update SLX Opportunity   (START)
'***************************************************************************************************************************
'***************************************************************************************************************************
'***************************************************************************************************************************
Sub ProcessSLXData(NewSOKey As Long, oClass As Object, Optional ProjDesc As String = Empty)
    On Error GoTo Error
'This method is called from the following locations:
'   frmSalesOrder.moDMform_DMAfterInsert
'   frmSOGenQuote.CreateFromQuote
    
'*************************************************************************************************************
'*************************************************************************************************************
'*************************************************************************************************************
' SGS DEJ 6/1/2010
'This code was commented out because it was put into a trigger on the tsoSalesOrder Table
'*************************************************************************************************************
'*************************************************************************************************************
'*************************************************************************************************************
    
    Dim lTranType As Long
    Dim SLXAccountID As String
    Dim BSOTranNo As String
    Dim QuoteBidNumber As String
    Dim lCustKey As Long
    Dim OpportunityID As String
    
    Dim SLXContractNumber As String 'SGS DEJ 4/26/12

    Dim dCloseDate As Date
    Dim sDesc As String
    Dim sClosed As String
    Dim sStatus As String
    Dim sAcctMgrID As String
    Dim lCloseProb As Long
    Dim dOpened As Date
    Dim SalesPotential As Double
    Dim var As Variant

    'Set Some Defaults
    dOpened = Date + Time
    SalesPotential = 0#
    sStatus = "Open"
    sClosed = "F"
    lCloseProb = 1


    'Check to ensure that new Record exists
    If glGetValidLong(oClass.moAppDB.Lookup("Count(*)", "tsoSalesOrder", "SOKey = " & NewSOKey)) <= 0 Then
        'RKL DEJ 2016-06-07 (Added if for CompanyID) email from Kevin asking to not show the SLX messages for the WEI company
'        If UCase(frmSalesOrd.oClass.moSysSession.CompanyID) <> "WEI" Then
            MsgBox "Could not insert/update SLX Opportunity... the Quote or Contract does not exist in MAS 500" & vbCrLf & _
            "SoKey = " & NewSOKey, vbExclamation, "MAS 500"
'        End If
        Exit Sub
    End If

    'Check the Transaction Type (only use BSO or Quote... do not do anything with SO)
    lTranType = glGetValidLong(oClass.moAppDB.Lookup("TranType", "tsoSalesOrder", "SOKey = " & NewSOKey))

    Select Case lTranType
        Case 801    'Sales Order - return with no action
            Exit Sub
        Case 802    'BSO - Insert/Update SLX
            'Continue On
        Case 840    'Quote - Insert/Update SLX
            'Continue On
        Case Else   'Unknown - return with no action
            Exit Sub
    End Select

    'Get EstimatedClose from SO TranDate
    var = gsGetValidStr(oClass.moAppDB.Lookup("TranDate", "tsoSalesOrder", "SOKey = " & NewSOKey))
    If IsDate(var) = True Then
        dCloseDate = CDate(var)
    Else
        dCloseDate = Date
    End If

    'Get Opportunity Description - Default to MAS SO Project Number
    sDesc = Left(Trim(gsGetValidStr(oClass.moAppDB.Lookup("ProjectDesc", "tsoSalesOrderExt_SGS", "SOKey = " & NewSOKey))), 64)
    If Trim(sDesc) = Empty Then
        If Trim(ProjDesc) = Empty Then
            sDesc = "(None)"
        Else
            sDesc = Left(Trim(ProjDesc), 64)
        End If
    End If

    'Get BSO TranNo
    BSOTranNo = gsGetValidStr(oClass.moAppDB.Lookup("TranNo", "tsoSalesOrder", "SOKey = " & NewSOKey))

    'Get QuoteBidNumber
    QuoteBidNumber = gsGetValidStr(oClass.moAppDB.Lookup("BidNo", "tsoSalesOrderExt_SGS", "SOKey = " & NewSOKey))

    If Trim(QuoteBidNumber) = Empty Then
        If lTranType = 840 Then
            QuoteBidNumber = BSOTranNo
'            MsgBox "Could not insert/update SLX Opportunity... the Bid Number is missing.  You will need to manually create this Opportunity.", vbExclamation, "MAS 500"
'            Exit Sub
        Else
'            QuoteBidNumber = BSOTranNo
        End If
    End If

    'Check to see if the Customer exists in SLX
    lCustKey = glGetValidLong(oClass.moAppDB.Lookup("CustKey", "tsoSalesOrder", "SOKey = " & NewSOKey))
    If glGetValidLong(oClass.moAppDB.Lookup("Count(*)", "vluSLXCustomer_SGS", "AccountReferenceID is not null and CustKey = " & lCustKey)) <= 0 Then
        'RKL DEJ 2016-06-07 (Added if for CompanyID) email from Kevin asking to not show the SLX messages for the WEI company
'        If UCase(frmSalesOrd.oClass.moSysSession.CompanyID) <> "WEI" Then
            MsgBox "Could not insert/update SLX Opportunity... the Customer/Account does not exist in SalesLogix.  You will need to manually create this Opportunity.", vbExclamation, "MAS 500"
'        End If
        Exit Sub
    End If

    'Get SLX Account ID
    SLXAccountID = gsGetValidStr(oClass.moAppDB.Lookup("AccountID", "vluSLXCustomer_SGS", "AccountReferenceID is not null and CustKey = " & lCustKey))


    'Check to see if SLX has an existing Opportunity (If so then update if not then Insert)
    OpportunityID = gsGetValidStr(oClass.moAppDB.Lookup("OpportunityID", "vluSLXOpportunityAndQuotes_SGS", "BidNumber = " & gsQuoted(QuoteBidNumber) & " And CompanyID = " & gsQuoted(oClass.moSysSession.CompanyID)))
    
    
    If Trim(OpportunityID) = Empty And lTranType = 802 Then
        'Search by Contract ID (BSO TranNo)
        OpportunityID = gsGetValidStr(oClass.moAppDB.Lookup("OpportunityID", "vluSLXOpportunityAndContracts_SGS", "ContractNumber = " & gsQuoted(BSOTranNo) & " And CompanyID = " & gsQuoted(oClass.moSysSession.CompanyID)))
    
    ElseIf Trim(OpportunityID) <> Empty And lTranType = 802 Then
        'Check to see what the contract number in SLX is for this Bid Number
        'SGS DEJ 4/26/12 (START)
        'Some BSO are copied from BSO to BSO and that has been overwriting the SLX Opportuinity Contract Number.
        'On 4/26/12 Hal C. said if the SLX Opportuinity Exists and Contract Number in SLX has already been assigned, then create a
        'New SLX Opportunity with no Bid Number
        SLXContractNumber = gsGetValidStr(oClass.moAppDB.Lookup("ContractNumber", "vluSLXOpportunityAndQuotes_SGS", "BidNumber = " & gsQuoted(QuoteBidNumber) & " And CompanyID = " & gsQuoted(oClass.moSysSession.CompanyID)))
        
        If Trim(SLXContractNumber) > Empty Then
            If UCase(Trim(BSOTranNo)) <> UCase(Trim(SLXContractNumber)) Then
                OpportunityID = Empty
                QuoteBidNumber = Empty
            End If
        End If
        'SGS DEJ 4/26/12 (End)
        
    End If

    If Trim(OpportunityID) = Empty Then
        'Create New Opportunity

        If lTranType = 840 Then
            BSOTranNo = Empty
        End If

        InsSLX oClass, OpportunityID, SLXAccountID, BSOTranNo, QuoteBidNumber, dCloseDate, sDesc, sClosed, sStatus, sAcctMgrID, lCloseProb, dOpened, SalesPotential, NewSOKey

    Else
        'Update Opportunity
        If lTranType = 802 Then
            'Only need to update Contracts/BSO
            UpdateSLX oClass, OpportunityID, SLXAccountID, BSOTranNo, NewSOKey
        End If

    End If
    
    Exit Sub
Error:
    MsgBox "basSalesOrder.ProcessSLXData()" & vbCrLf & Date + Time & vbCrLf & _
    "Could not insert/update the Sales Logix Opportunity with the Blanket Sales Order/Contract data you will need to do this manually." & vbCrLf & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
End Sub


'SGS DEJ 5/04/2010 (Added Method)
Public Function UpdateSLX(oClass As Object, OpportunityID As String, SLXAccountID As String, BSOTranNo As String, SOKey As Long) As Boolean
    On Error GoTo Error
    
    Dim ConnStr As String
    Dim Conn As New ADODB.Connection
    Dim SQL As String
    Dim C_UnilateralID As String
    Dim MASUserID As String
    
    MASUserID = gsGetValidStr(oClass.moSysSession.UserId)
    
    ConnStr = GetSLXConnStr(oClass)
    
    Conn.CursorLocation = adUseClient
    Conn.IsolationLevel = adXactBrowse
    Conn.ConnectionString = ConnStr
    
    Conn.Open
    
    SQL = "Update OPPORTUNITY Set " & _
    "ContractNumber = " & gsQuoted(BSOTranNo) & ", " & _
    "AccountID = " & gsQuoted(SLXAccountID) & ", " & _
    "ModifyUser = " & gsQuoted(Left(MASUserID, 12)) & ", " & _
    "ModifyDate = " & gsQuoted(Date + Time) & ", " & _
    "MASSOKeyLink = " & SOKey & ", " & _
    "MASContractSOKey = " & SOKey & " "

    SQL = SQL & "Where OpportunityID = " & gsQuoted(OpportunityID)

    Conn.Execute SQL
    
    UpdateSLX = True

CleanUP:
    Err.Clear
    On Error Resume Next
    
    If Conn.State = 1 Then Conn.Close
    Set Conn = Nothing
    
    Exit Function
Error:
    MsgBox "basSalesOrder.UpdateSLX()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    GoTo CleanUP

End Function

'SGS DEJ 5/04/2010 (Added Method)
Public Function InsSLX(oClass As Object, ByRef OpportunityID As String, SLXAccountID As String, BSOTranNo As String, QuoteBidNumber As String, _
dCloseDate As Date, sDesc As String, sClosed As String, sStatus As String, sAcctMgrID As String, lCloseProb As Long, dOpened As Date, SalesPotential As Double, SOKey As Long) As Boolean
    On Error GoTo Error
    
    Dim ConnStr As String
    Dim Conn As New ADODB.Connection
    Dim rs As New ADODB.Recordset
    Dim SQL As String
    Dim C_UnilateralID As String
    Dim MASUserID As String
    Dim SECCODEID As String
    
    MASUserID = gsGetValidStr(oClass.moSysSession.UserId)
    
    ConnStr = GetSLXConnStr(oClass)
    
    Conn.CursorLocation = adUseClient
    Conn.IsolationLevel = adXactBrowse
    Conn.ConnectionString = ConnStr
    
    Conn.Open
    
    SQL = "slx_dbids('Opportunity', 1)"

    Set rs = Conn.Execute(SQL)

    If rs.RecordCount > 0 Then
        OpportunityID = gsGetValidStr(rs("ID").Value)
    Else
        InsSLX = False
        'RKL DEJ 2016-06-07 (Added if for CompanyID) email from Kevin asking to not show the SLX messages for the WEI company
'        If UCase(frmSalesOrd.oClass.moSysSession.CompanyID) <> "WEI" Then
            MsgBox "Could not get the next key value to create the record in SalesLogix.", vbExclamation, "MAS 500"
'        End If
        GoTo CleanUP
    End If

    SECCODEID = gsGetValidStr(oClass.moAppDB.Lookup("SECCODEID", "vluSLXCustomer_SGS", "AccountID = " & gsQuoted(SLXAccountID)))
    'Get SLX Account Manager ID from account
    sAcctMgrID = gsGetValidStr(oClass.moAppDB.Lookup("AccountManagerID", "vluSLXCustomer_SGS", "AccountID = " & gsQuoted(SLXAccountID)))
    
    If Trim(QuoteBidNumber) = Empty Then
        SQL = "Insert Into OPPORTUNITY (OpportunityID, ACCOUNTID, BidNumber, ContractNumber, CREATEUSER, CREATEDATE, MODIFYUSER, MODIFYDATE, SECCODEID, " & _
        "Description, Closed, Status, EstimatedClose, AccountManagerID, CloseProbability, DateOpened, SalesPotential, ResellerID, MASSOKeyLink, MASContractSOKey, MASCompanyID " & _
        " ) "
    Else
        SQL = "Insert Into OPPORTUNITY (OpportunityID, ACCOUNTID, BidNumber, ContractNumber, CREATEUSER, CREATEDATE, MODIFYUSER, MODIFYDATE, SECCODEID, " & _
        "Description, Closed, Status, EstimatedClose, AccountManagerID, CloseProbability, DateOpened, SalesPotential, ResellerID, MASSOKeyLink, MASQuoteSOKey, MASCompanyID " & _
        " ) "
    End If

    SQL = SQL & "Values(" & _
    gsQuoted(OpportunityID) & ", " & _
    gsQuoted(SLXAccountID) & ", " & _
    gsQuoted(QuoteBidNumber) & ", " & _
    gsQuoted(BSOTranNo) & ", " & _
    gsQuoted(Left(MASUserID, 12)) & ", " & _
    gsQuoted(Date + Time) & ", " & _
    gsQuoted(Left(MASUserID, 12)) & ", " & _
    gsQuoted(Date + Time) & ", " & _
    gsQuoted(SECCODEID) & ", " & _
    gsQuoted(sDesc) & ", " & _
    gsQuoted(sClosed) & ", " & _
    gsQuoted(sStatus) & ", " & _
    gsQuoted(dCloseDate) & ", " & _
    gsQuoted(sAcctMgrID) & ", " & _
    lCloseProb & ", " & _
    gsQuoted(dOpened) & ", " & _
    SalesPotential & ", " & _
    "'', " & _
    SOKey & ", " & _
    SOKey & ", " & _
    gsQuoted(oClass.moSysSession.CompanyID) & _
    " ) "

    Conn.Execute SQL
    
    InsSLX = True
    
CleanUP:
    Err.Clear
    On Error Resume Next
    
    If rs.State = 1 Then rs.Close
    Set rs = Nothing
    
    If Conn.State = 1 Then Conn.Close
    Set Conn = Nothing
    
    Exit Function
Error:
    MsgBox "basSalesOrder.InsSLX()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    GoTo CleanUP

End Function


'SGS DEJ 5/04/2010 (Added Method)
Public Function GetSLXConnStr(oClass As Object) As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    On Error Resume Next
    Dim ConnStr As String
    
    ConnStr = gsGetValidStr(oClass.moAppDB.Lookup("SLXConnStr", "tciSLXConnStr_SGS", Empty))
    
    ConnStr = Decrypt(ConnStr)
    
    GetSLXConnStr = ConnStr
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetSLXConnStr", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

'SGS DEJ 5/04/2010 (Added Method)
Public Function Decrypt(Val As String) As String
    On Error GoTo Error
    
    Dim i As Long
    Dim str As String
    Dim NewVal As Variant
    Dim DecryptedVal As String
    
    For i = 1 To Len(Val)
        str = Mid(Val, i, 4)
        
        If IsNumeric(str) Then
            str = str - 1000
        End If
        
        i = i + 3
        
        NewVal = Chr(CLng(str))
        DecryptedVal = DecryptedVal & NewVal
        
    Next
    
    Decrypt = DecryptedVal
    
    Exit Function
Error:
    MsgBox "basSalesOrder.Decrypt(Val As String) As String" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

End Function

'SGS New Method
Sub ProcessSLXLineData(SOKey As Long, SOLineKey As Long, SOLineDistKey As Long, ItemKey As Long, WhseKey As Long, QtyOrd As Double, oClass As Object)
    On Error GoTo Error
'This method is called from the following locations:
'   frmSalesOrder.moDMSubGrid_DMGridAfterInsert
    
    Dim MASOppPrdtsID As String
    Dim OpportunityID As String
    Dim iRetVal As Long
    Dim SQL As String
    
    Dim ItemID As String
    Dim ShortDesc As String
    Dim LongDesc As String
    Dim WhseID As String
    Dim WhseDesc As String
    
    'Check to see if SLX has an existing Opportunity (If not then exit)
    OpportunityID = gsGetValidStr(oClass.moAppDB.Lookup("OpportunityID", "vluSLXOpportunityAndQuotes_SGS", "SOKey = " & SOKey))
    
    If Trim(OpportunityID) = Empty Then
        'No Need to process... there is no opportunity to update
        Exit Sub
    End If
    
        
    'Get ID's and Descriptions
    ItemID = gsGetValidStr(oClass.moAppDB.Lookup("ItemID", "timItem", "ItemKey = " & ItemKey))
    ShortDesc = gsGetValidStr(oClass.moAppDB.Lookup("ShortDesc", "timItemDescription", "ItemKey = " & ItemKey))
    LongDesc = gsGetValidStr(oClass.moAppDB.Lookup("LongDesc", "timItemDescription", "ItemKey = " & ItemKey))
    
    WhseID = gsGetValidStr(oClass.moAppDB.Lookup("WhseID", "timWarehouse", "WhseKey = " & WhseKey))
    WhseDesc = gsGetValidStr(oClass.moAppDB.Lookup("Description", "timWarehouse", "WhseKey = " & WhseKey))
    
    MASOppPrdtsID = gsGetValidStr(oClass.moAppDB.Lookup("MASOpportunityProductsID", "vluSLXOpportunityProducts_SGS", "SOLineDistKey = " & SOLineDistKey))
    
    If Trim(MASOppPrdtsID) = Empty Then
        'Insert New record
        
        If InsSLXProd(oClass, OpportunityID, SOLineKey, SOLineDistKey, SOKey, ItemKey, ItemID, ShortDesc, LongDesc, _
        WhseKey, WhseID, WhseDesc, QtyOrd) = False Then
            'Did not insert new record
        End If
        
    Else
        'Update Existing record
        
        If UpdSLXProd(oClass, MASOppPrdtsID, OpportunityID, SOLineKey, SOLineDistKey, SOKey, ItemKey, ItemID, ShortDesc, LongDesc, _
        WhseKey, WhseID, WhseDesc, QtyOrd) = False Then
            'Did not update record
        End If
    End If
    
    
    Exit Sub
Error:
    MsgBox "basSalesOrder.ProcessSLXLineData()" & vbCrLf & Date + Time & vbCrLf & _
    "Could not insert/update the Sales Logix Opportunity Product." & vbCrLf & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
End Sub

'SGS New Method
Function GetNextOppProdKey(oClass As Object) As String
    On Error GoTo Error

    Dim MASUserID As String
    Dim ConnStr As String
    Dim Conn As New ADODB.Connection
    Dim rs As New ADODB.Recordset
    Dim SQL As String

    MASUserID = gsGetValidStr(oClass.moSysSession.UserId)

    ConnStr = GetSLXConnStr(oClass)

    Conn.CursorLocation = adUseClient
    Conn.IsolationLevel = adXactBrowse
    Conn.ConnectionString = ConnStr

    Conn.Open

    SQL = "slx_dbids('MASOpportunityProducts', 1)"

    Set rs = Conn.Execute(SQL)

    If rs.RecordCount > 0 Then
        GetNextOppProdKey = gsGetValidStr(rs("ID").Value)
    Else
        'RKL DEJ 2016-06-07 (Added if for CompanyID) email from Kevin asking to not show the SLX messages for the WEI company
'        If UCase(frmSalesOrd.oClass.moSysSession.CompanyID) <> "WEI" Then
            MsgBox "Could not get the next key value to create the Product record in SalesLogix.", vbExclamation, "MAS 500"
'        End If
    End If

CleanUP:
    Err.Clear
    On Error Resume Next

    If rs.State = 1 Then rs.Close
    Set rs = Nothing

    If Conn.State = 1 Then Conn.Close
    Set Conn = Nothing

    Exit Function
Error:
    MsgBox "basSalesOrder.GetNextOppProdKey()" & vbCrLf & Date + Time & vbCrLf & _
    "Could not obtain new Key for SLX table: MASOpportunityProducts." & vbCrLf & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"

    Err.Clear
    GoTo CleanUP

End Function


'SGS DEJ 6/14/2010 (Added Method)
Public Function InsSLXProd(oClass As Object, OpportunityID As String, _
MASSOLineKey As Long, MASSOLineDistKey As Long, MASSOKey As Long, _
MASItemKey As Long, MASItemID As String, MASItemShrtDesc As String, MASItemLongDesc As String, _
MASWhseKey As Long, MASWhseID As String, MASWhseDesc As String, _
MASPlannedTons As Double) As Boolean
    On Error GoTo Error
    
    Dim ConnStr As String
    Dim Conn As New ADODB.Connection
    Dim rs As New ADODB.Recordset
    Dim SQL As String
    Dim MASUserID As String
    Dim InsDate As Date
    Dim MASOppPrdtsID As String
    
    MASUserID = gsGetValidStr(oClass.moSysSession.UserId)
    InsDate = Date + Time
    
    ConnStr = GetSLXConnStr(oClass)
    
    Conn.CursorLocation = adUseClient
    Conn.IsolationLevel = adXactBrowse
    Conn.ConnectionString = ConnStr
    
    Conn.Open
    
    'Get Next TableKey/ID
    SQL = "slx_dbids('MASOpportunityProducts', 1)"

    Set rs = Conn.Execute(SQL)

    If rs.RecordCount > 0 Then
        MASOppPrdtsID = gsGetValidStr(rs("ID").Value)
    Else
        InsSLXProd = False
        'RKL DEJ 2016-06-07 (Added if for CompanyID) email from Kevin asking to not show the SLX messages for the WEI company
'        If UCase(frmSalesOrd.oClass.moSysSession.CompanyID) <> "WEI" Then
            MsgBox "Could not get the next key value to create the product record in SalesLogix.", vbExclamation, "MAS 500"
'        End If
        GoTo CleanUP
    End If
    
    'Create Insert Statement
    SQL = "Insert into MASOpportunityProducts(MASOpportunityProductsID, OpportunityID, MASSOLineKey, MASSOLineDistKey, " & _
    "MASItemKey, MASItemID, MASItemShortDesc, MASItemLongDesc, " & _
    "MASWhseKey, MASWhseID, MASWhseDesc, MASPlannedTons, CreateUser, CreateDate, ModifyUser, ModifyDate) "

    SQL = SQL & "Values(" & _
    gsQuoted(MASOppPrdtsID) & ", " & _
    gsQuoted(OpportunityID) & ", " & _
    MASSOLineKey & ", " & _
    MASSOLineDistKey & ", " & _
    MASItemKey & ", " & _
    gsQuoted(MASItemID) & ", " & _
    gsQuoted(MASItemShrtDesc) & ", " & _
    gsQuoted(MASItemLongDesc) & ", " & _
    MASWhseKey & ", " & _
    gsQuoted(MASWhseID) & ", " & _
    gsQuoted(MASWhseDesc) & ", " & _
    MASPlannedTons & ", " & _
    gsQuoted(MASUserID) & ", " & _
    gsQuoted(InsDate) & ", " & _
    gsQuoted(MASUserID) & ", " & _
    gsQuoted(InsDate) & _
    " ) "

    'Execute insert Statment
    Conn.Execute SQL
    
    
    InsSLXProd = True
    
CleanUP:
    Err.Clear
    On Error Resume Next
    
    If rs.State = 1 Then rs.Close
    Set rs = Nothing
    
    If Conn.State = 1 Then Conn.Close
    Set Conn = Nothing
    
    Exit Function
Error:
    MsgBox "basSalesOrder.InsSLXProd()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    GoTo CleanUP

End Function


'SGS DEJ 6/14/2010 (Added Method)
Public Function UpdSLXProd(oClass As Object, ByRef MASOppPrdtsID As String, OpportunityID As String, _
MASSOLineKey As Long, MASSOLineDistKey As Long, MASSOKey As Long, _
MASItemKey As Long, MASItemID As String, MASItemShrtDesc As String, MASItemLongDesc As String, _
MASWhseKey As Long, MASWhseID As String, MASWhseDesc As String, _
MASPlannedTons As Double) As Boolean
    On Error GoTo Error
    
    Dim ConnStr As String
    Dim Conn As New ADODB.Connection
    Dim rs As New ADODB.Recordset
    Dim SQL As String
    Dim MASUserID As String
    Dim InsDate As Date
    
    MASUserID = gsGetValidStr(oClass.moSysSession.UserId)
    InsDate = Date + Time
    
    ConnStr = GetSLXConnStr(oClass)
    
    Conn.CursorLocation = adUseClient
    Conn.IsolationLevel = adXactBrowse
    Conn.ConnectionString = ConnStr
    
    Conn.Open
    
     
    'Create Update Statement
    SQL = "Update MASOpportunityProducts Set " & _
    "ModifyUser = " & gsQuoted(MASUserID) & _
    ", ModifyDate = " & gsQuoted(InsDate) & _
    ", MASItemKey = " & MASItemKey & _
    ", MASItemID = " & gsQuoted(MASItemID) & _
    ", MASItemShortDesc = " & gsQuoted(MASItemShrtDesc) & _
    ", MASItemLongDesc = " & gsQuoted(MASItemLongDesc) & _
    ", MASWhseKey = " & gsQuoted(MASWhseKey) & _
    ", MASWhseID = " & gsQuoted(MASWhseID) & _
    ", MASWhseDesc = " & gsQuoted(MASWhseDesc) & _
    ", MASPlannedTons = " & gsQuoted(MASPlannedTons) & _
    "Where MASOpportunityProductsID = " & gsQuoted(MASOppPrdtsID)


    'Execute update Statment
    Conn.Execute SQL
    
    
    UpdSLXProd = True
    
CleanUP:
    Err.Clear
    On Error Resume Next
    
    If rs.State = 1 Then rs.Close
    Set rs = Nothing
    
    If Conn.State = 1 Then Conn.Close
    Set Conn = Nothing
    
    Exit Function
Error:
    MsgBox "basSalesOrder.UpdSLXProd()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    GoTo CleanUP

End Function


'***************************************************************************************************************************
'***************************************************************************************************************************
'***************************************************************************************************************************
'SGS DEJ 5/4/2010 - Insert/Update SLX Opportunity   (STOP)
'***************************************************************************************************************************
'***************************************************************************************************************************
'***************************************************************************************************************************



