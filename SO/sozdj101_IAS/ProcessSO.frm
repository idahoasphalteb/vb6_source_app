VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#153.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#51.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "Tabctl32.ocx"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#83.0#0"; "LookupView.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#53.0#0"; "SOTACalendar.ocx"
Object = "{BC90D6A3-491E-451B-ADED-8FABA0B8EE36}#46.0#0"; "SOTADropDown.ocx"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#156.0#0"; "EntryLookupControls.ocx"
Begin VB.Form frmProcessSO 
   Caption         =   "Process Sales Order"
   ClientHeight    =   6600
   ClientLeft      =   705
   ClientTop       =   1395
   ClientWidth     =   11625
   HelpContextID   =   17776083
   Icon            =   "ProcessSO.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6600
   ScaleWidth      =   11625
   Begin VB.CommandButton cmdCCPmnt 
      Caption         =   "Ente&r Credit Cards/EFT..."
      Height          =   360
      Left            =   4440
      TabIndex        =   59
      Top             =   5760
      WhatsThisHelpID =   17776189
      Width           =   1965
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtHoldReason 
      Height          =   255
      Left            =   1080
      TabIndex        =   56
      Top             =   5800
      Width           =   1215
      _Version        =   65536
      _ExtentX        =   2143
      _ExtentY        =   450
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      lMaxLength      =   20
   End
   Begin VB.CheckBox chkHold 
      Caption         =   "Hold"
      Height          =   255
      Left            =   360
      TabIndex        =   55
      Top             =   5800
      Width           =   735
   End
   Begin VB.CommandButton cmdApply 
      Caption         =   "&Apply Payments..."
      Enabled         =   0   'False
      Height          =   360
      Left            =   8160
      TabIndex        =   28
      Top             =   5760
      WhatsThisHelpID =   17776189
      Width           =   1600
   End
   Begin VB.CommandButton cmdPmnt 
      Caption         =   "E&nter Payments..."
      Height          =   360
      Left            =   6480
      TabIndex        =   27
      Top             =   5760
      WhatsThisHelpID =   17776189
      Width           =   1600
   End
   Begin VB.Frame fraPickTickets 
      Caption         =   "Pick Tickets"
      Height          =   5175
      Left            =   240
      TabIndex        =   1
      Top             =   480
      Width           =   11385
      Begin VB.Frame fraInvoice 
         Caption         =   "Invoice"
         Height          =   2295
         Left            =   120
         TabIndex        =   10
         Top             =   2760
         Width           =   1935
         Begin VB.CommandButton cmdGenInvoice 
            Caption         =   "&Generate Invoice"
            Enabled         =   0   'False
            Height          =   285
            Left            =   240
            TabIndex        =   11
            Top             =   360
            Width           =   1455
         End
         Begin VB.CommandButton cmdDelInvoice 
            Caption         =   "&Delete Invoice"
            Enabled         =   0   'False
            Height          =   285
            Left            =   240
            TabIndex        =   12
            Top             =   840
            Width           =   1455
         End
         Begin VB.CommandButton cmdInvcTotals 
            Caption         =   "..."
            Enabled         =   0   'False
            Height          =   285
            Left            =   1440
            TabIndex        =   13
            ToolTipText     =   "Invoice Totals"
            Top             =   1800
            Visible         =   0   'False
            Width           =   315
         End
         Begin NEWSOTALib.SOTACurrency curInvoiceTotal 
            Height          =   285
            Left            =   120
            TabIndex        =   51
            Top             =   1800
            Visible         =   0   'False
            WhatsThisHelpID =   17375210
            Width           =   1335
            _Version        =   65536
            _ExtentX        =   2355
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
            bProtected      =   -1  'True
            sBorder         =   0
            mask            =   "<HL> <ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "            0.00"
            lAlignment      =   0
            sIntegralPlaces =   13
            sDecimalPlaces  =   2
         End
         Begin VB.Label lblInoviceTotal 
            Caption         =   "Total"
            Height          =   255
            Left            =   130
            TabIndex        =   54
            Top             =   1440
            Visible         =   0   'False
            Width           =   615
         End
         Begin VB.Label lblInvcCurrID 
            Alignment       =   1  'Right Justify
            Height          =   255
            Left            =   1080
            TabIndex        =   53
            Top             =   1440
            Width           =   615
         End
         Begin VB.Label lblMultiInvc 
            Caption         =   "(Multiple Invoices)"
            Height          =   255
            Left            =   360
            TabIndex        =   52
            Top             =   1320
            Visible         =   0   'False
            Width           =   1335
         End
      End
      Begin TabDlg.SSTab TabShipLine 
         Height          =   4215
         Left            =   2280
         TabIndex        =   14
         Top             =   960
         WhatsThisHelpID =   17776084
         Width           =   9015
         _ExtentX        =   15901
         _ExtentY        =   7435
         _Version        =   393216
         Tabs            =   2
         TabHeight       =   520
         TabCaption(0)   =   "Ship &Lines"
         TabPicture(0)   =   "ProcessSO.frx":23D2
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "frmShipLines"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "&Messages"
         TabPicture(1)   =   "ProcessSO.frx":23EE
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "frmViewMessage"
         Tab(1).ControlCount=   1
         Begin VB.Frame frmViewMessage 
            Caption         =   "View Messages:"
            Height          =   3735
            Left            =   -74880
            TabIndex        =   16
            Top             =   360
            Width           =   8775
            Begin VB.CommandButton cmdPrintMsg 
               Caption         =   "Print Messages"
               Height          =   315
               Left            =   5640
               TabIndex        =   21
               Top             =   320
               WhatsThisHelpID =   17776085
               Width           =   1380
            End
            Begin VB.CommandButton cmdClearMsg 
               Caption         =   "Clear Messages"
               Height          =   315
               HelpContextID   =   101998
               Left            =   7080
               TabIndex        =   22
               Top             =   320
               WhatsThisHelpID =   17776086
               Width           =   1380
            End
            Begin VB.ComboBox cboSeverity 
               Height          =   315
               HelpContextID   =   101996
               ItemData        =   "ProcessSO.frx":240A
               Left            =   960
               List            =   "ProcessSO.frx":240C
               Style           =   2  'Dropdown List
               TabIndex        =   18
               Top             =   310
               WhatsThisHelpID =   17776087
               Width           =   1635
            End
            Begin SOTADropDownControl.SOTADropDown ddnOutput 
               Height          =   315
               Left            =   3480
               TabIndex        =   20
               Top             =   315
               WhatsThisHelpID =   100280
               Width           =   1380
               _ExtentX        =   2434
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Text            =   "ddnOutput"
            End
            Begin FPSpreadADO.fpSpread grdMessages 
               Height          =   2895
               HelpContextID   =   101999
               Left            =   120
               TabIndex        =   23
               Top             =   705
               WhatsThisHelpID =   17776089
               Width           =   8400
               _Version        =   524288
               _ExtentX        =   14817
               _ExtentY        =   5106
               _StockProps     =   64
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ScrollBars      =   2
               SpreadDesigner  =   "ProcessSO.frx":240E
               AppearanceStyle =   0
            End
            Begin VB.Label lblOutput 
               Caption         =   "Output"
               Height          =   255
               Left            =   2880
               TabIndex        =   19
               Top             =   345
               Width           =   615
            End
            Begin VB.Label lblSeverity 
               Caption         =   "Severity"
               Height          =   210
               Index           =   0
               Left            =   120
               TabIndex        =   17
               Top             =   350
               Width           =   915
            End
         End
         Begin VB.Frame frmShipLines 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   3900
            Left            =   120
            TabIndex        =   29
            Top             =   360
            Width           =   8775
            Begin FPSpreadADO.fpSpread grdShipment 
               Height          =   3795
               Left            =   0
               TabIndex        =   15
               Top             =   75
               WhatsThisHelpID =   17776090
               Width           =   8715
               _Version        =   524288
               _ExtentX        =   15372
               _ExtentY        =   6694
               _StockProps     =   64
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               SpreadDesigner  =   "ProcessSO.frx":2857
               AppearanceStyle =   0
            End
         End
      End
      Begin VB.TextBox txtNavUOM 
         Height          =   285
         Left            =   120
         TabIndex        =   47
         Text            =   "Text1"
         Top             =   3000
         Visible         =   0   'False
         WhatsThisHelpID =   17776091
         Width           =   1425
      End
      Begin VB.CommandButton cmdComponents 
         Caption         =   "&Kit Components..."
         Enabled         =   0   'False
         Height          =   360
         Left            =   8760
         TabIndex        =   24
         Top             =   210
         WhatsThisHelpID =   17776092
         Width           =   1575
      End
      Begin VB.Frame fraOrderLineType 
         Height          =   2055
         Left            =   120
         TabIndex        =   5
         Top             =   600
         Width           =   1935
         Begin VB.CommandButton cmdCreatePick 
            Caption         =   "&Create Pick Ticket"
            Height          =   360
            Left            =   120
            TabIndex        =   9
            Top             =   1440
            WhatsThisHelpID =   17776093
            Width           =   1650
         End
         Begin VB.OptionButton OptPickType 
            Caption         =   "&Shipped Order"
            Height          =   375
            Index           =   3
            Left            =   200
            TabIndex        =   8
            Top             =   960
            WhatsThisHelpID =   17776094
            Width           =   1455
         End
         Begin VB.OptionButton OptPickType 
            Caption         =   "Co&unter Sale"
            Height          =   375
            Index           =   1
            Left            =   200
            TabIndex        =   6
            Top             =   240
            WhatsThisHelpID =   17776095
            Width           =   1215
         End
         Begin VB.OptionButton OptPickType 
            Caption         =   "&Will Call"
            Height          =   375
            Index           =   2
            Left            =   200
            TabIndex        =   7
            Top             =   600
            WhatsThisHelpID =   17776096
            Width           =   1215
         End
      End
      Begin VB.CommandButton cmdDist 
         Caption         =   "&Dist..."
         Height          =   360
         Left            =   10440
         TabIndex        =   25
         Top             =   210
         WhatsThisHelpID =   17776097
         Width           =   615
      End
      Begin EntryLookupControls.TextLookup lkuItem 
         Height          =   285
         Left            =   240
         TabIndex        =   46
         Top             =   2760
         Visible         =   0   'False
         WhatsThisHelpID =   17776098
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   503
         ForeColor       =   -2147483640
         MaxLength       =   13
         IsSurrogateKey  =   -1  'True
         LookupID        =   "Item"
         ParentIDColumn  =   "ItemID"
         ParentKeyColumn =   "ItemKey"
         ParentTable     =   "timItem"
         IsForeignKey    =   -1  'True
         sSQLReturnCols  =   "ItemID,lkuItem,;ShortDesc,,;ItemKey,,;"
      End
      Begin LookupViewControl.LookupView navMain 
         Height          =   285
         Left            =   2640
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Lookup"
         Top             =   240
         WhatsThisHelpID =   14
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   503
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtPickTicketNo 
         Height          =   285
         Left            =   1200
         TabIndex        =   3
         Top             =   240
         WhatsThisHelpID =   17776100
         Width           =   1410
         _Version        =   65536
         _ExtentX        =   2496
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         lMaxLength      =   10
      End
      Begin LookupViewControl.LookupView navGridUOM 
         Height          =   285
         Left            =   1680
         TabIndex        =   48
         TabStop         =   0   'False
         ToolTipText     =   "Lookup"
         Top             =   3120
         Visible         =   0   'False
         WhatsThisHelpID =   17776101
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   503
         LookupMode      =   1
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtlkuAutoDistBin 
         Height          =   255
         Left            =   360
         TabIndex        =   49
         Top             =   2760
         Visible         =   0   'False
         WhatsThisHelpID =   17776102
         Width           =   1215
         _Version        =   65536
         _ExtentX        =   2143
         _ExtentY        =   450
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         lMaxLength      =   13
      End
      Begin LookupViewControl.LookupView lkuAutoDistBin 
         Height          =   285
         Left            =   1560
         TabIndex        =   50
         TabStop         =   0   'False
         ToolTipText     =   "Lookup"
         Top             =   3000
         Visible         =   0   'False
         WhatsThisHelpID =   17776103
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   503
         LookupMode      =   1
      End
      Begin VB.Label lblShipmentStatus 
         Caption         =   "Status"
         Height          =   255
         Left            =   9120
         TabIndex        =   58
         Top             =   720
         Width           =   615
      End
      Begin VB.Label lblShipmentStatusText 
         Height          =   255
         Left            =   9960
         TabIndex        =   57
         Top             =   720
         Width           =   1095
      End
      Begin VB.Label lblShipmentNo 
         Caption         =   "Shipment No"
         Height          =   195
         Left            =   6120
         TabIndex        =   45
         Top             =   270
         Width           =   1815
      End
      Begin VB.Label lblShipment 
         Caption         =   "Shipment"
         Height          =   195
         Left            =   5160
         TabIndex        =   44
         Top             =   270
         Width           =   855
      End
      Begin VB.Label lblPickType 
         Alignment       =   2  'Center
         Caption         =   "Pick Type"
         Height          =   195
         Left            =   3000
         TabIndex        =   43
         Top             =   270
         Width           =   2055
      End
      Begin VB.Label lblPickTicket 
         AutoSize        =   -1  'True
         Caption         =   "Pick &Ticket"
         Height          =   195
         Left            =   240
         TabIndex        =   2
         Top             =   270
         Width           =   810
      End
   End
   Begin VB.CommandButton cmdPrintInvoice 
      Caption         =   "&Print Invoice..."
      Height          =   375
      Left            =   9840
      TabIndex        =   30
      Top             =   5760
      WhatsThisHelpID =   17776104
      Width           =   1600
   End
   Begin VB.CommandButton cmdEditShipment 
      Caption         =   "&Edit Shipment..."
      Height          =   360
      Left            =   2760
      TabIndex        =   26
      Top             =   5760
      WhatsThisHelpID =   17776107
      Width           =   1600
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   42
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   32
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   33
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   34
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   35
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   36
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   37
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   38
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   39
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   40
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   690
      Index           =   0
      Left            =   1500
      TabIndex        =   31
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   1217
      _StockProps     =   0
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   6210
      WhatsThisHelpID =   73
      Width           =   11625
      _ExtentX        =   20505
      _ExtentY        =   688
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   11625
      _ExtentX        =   20505
      _ExtentY        =   741
      Style           =   0
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   41
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmProcessSO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************************
'     Name: ProcessSO
'     Desc: SO Process Order
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: JY 12-03-2004
'     Mods:
'**************************** ********************************************************
Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

    'Private Variables of Form Properties
    Private mlRunMode                   As Long                     'Run Mode of Task
    Private mbCancelShutDown            As Boolean                  'Cancel Shutdown Flag
    Private miSecurityLevel             As Integer                  'Security Level
    Private mbLoadSuccess               As Boolean
    Private msCompanyID                 As String
    Private msUserID                    As String
    Private mbEnterAsTab                As Boolean
    Private mbFocus                     As Boolean
    
    'Public Form Variables
    Public moSotaObjects                As New Collection           'Collection of Loaded Objects
    Public miFilter                     As Integer

    'Private Objects for This Project
    Private moContextMenu               As clsContextMenu           'Context Menu
    Private WithEvents moGMShipLine     As clsGridMgr               'Grid Manager Class
Attribute moGMShipLine.VB_VarHelpID = -1
    Private WithEvents moDMShipLineGrid As clsDmGrid                   'Grid Object
Attribute moDMShipLineGrid.VB_VarHelpID = -1
    Private WithEvents moGMMessage      As clsGridMgr
Attribute moGMMessage.VB_VarHelpID = -1
    Private WithEvents moDMMessageGrid  As clsDmGrid
Attribute moDMMessageGrid.VB_VarHelpID = -1
    
    Private moItemDist                  As Object
    Private moGridNavUOM                As clsGridLookup
    Private moGridLkuAutoDistBin        As clsGridLookup
    Private moModuleOptions             As Object
    Private moSelectPick                As Object
    Private moCreatePickList            As Object
    Private moClass                     As Object
    Private moAppDB                     As Object
    Private moProcessPickList           As clsProcessPickList
    Private moProcessCommitTran         As clsSOCommitTransMgr
    
    'Minimum Form Size
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long

    'Modole Option variables
    Private miAllowNegQty               As Integer
    Private miNbrDecPlaces              As Integer
    Private miOverShipmentPolicy        As Integer
    Private miCreditHoldAutoRelease     As Integer
    Private miMaxSerialNbrsOnPickList   As Integer
    Private miEmptyBins                 As Integer
    Private miEmptyRandomBins           As Integer
    Private msPostDate                  As String
    Private mlHiddenBatchKey            As Long
    Private miUseTranDateAsPostDate      As Integer
    Private miOrderOnCreditHold         As Integer
    Private miUseNationalAccts          As Integer
    Private miCustOnNAHold              As Integer

    'Miscellaneous Variables
    Private msBusinessDate              As String
    Private mlLanguage                  As Long
    Private miMousePointer              As Integer
    Private mbDontChkClick              As Boolean
    Private msFormCaption               As String
    Private mlPickListKey               As Long
    Private mlShipKey                   As Long
    Private mbNewCreatedPickList        As Boolean
    Private msShipmentTranDate          As String
    Private miPickListDelMth            As Integer
    Private msPickListDelMth            As String
    Private miShipmentCommitted         As Integer
    Private mlWhseKey                   As Long
    Private mlSOKey                     As Long
    Private msSOTranNo                  As String
    Private miInvoiceReqdByOption       As Integer
    Private miInvoiceReqdbyCust         As Integer
    Private mbCellChangeFired           As Boolean
    Private mbDontProcessTabClick       As Boolean
    Private mbDenyAction                As Boolean
    Private mbCanCommitTransIgnoreWarning   As Boolean
    Private miCustOnHold                As Integer
    Private miSingleShipmentViolation   As Integer
    Private mlLogicalLockKey            As Long
    Private mlInvcKey                   As Long
    Private miMultiInvc                 As Integer
    Private miTranStatus                As Integer
    Private miShipmentUpdateCtr         As Integer
    Private mbBusy                      As Boolean
    Private msShipmentNo                As String
    
    'Variables needed for sales order payments
    Private mlCustKey                   As Long
    Private mbUseMultiCurr              As Boolean
    Private mdExchangeRate              As Double
    Private msDocCurrID                 As String
    Private mlCurrExchSchedKey          As Long
    Private mlCustExchSchedKey          As Long
    Private miHomePriceDecPlaces        As Integer
    Private miHomeRoundMeth             As Integer
    Private miHomeRoundPrecision        As Integer
    Private mbSOPmntReadOnly            As Boolean
    Private msTranID                    As String
    Private mlBillToAddrKey             As Long
    Private miBillingType               As Integer
    Private msPurchOrderNo              As String
    Private mdTranAmtHC                 As Double
    Private mdTranAmt                   As Double
    Private mdFrtAmt                    As Double
    Private mdSTaxAmt                   As Double
    Private mdAmtInvcd                  As Double
    Private miBillToParent              As Integer
    Private mlParentCustKey             As Long
    Private mbDfltPmtByParent           As Boolean
    Private msOrderDate                 As String
    Private mbCCActivated               As Boolean
    
    Private moARSOPmnts                 As clsARSOPmnts
    Private moCCPmnts                   As clsARSOPmnts 'Used for direct entry of CC payment (payment form is hidden)
    
    Private moCCProcess                 As clsProcessCCUtils
    
    Private mbForceValiationFired       As Boolean
    
    'Local String Variables
    Private msDelMth_CounterSale        As String
    Private msDelMth_WillCall           As String
    Private msDelMth_ShippedOrder       As String
    Private msDelMth_Mixed              As String
    Private msPickTicket                As String

    'Grid Column Width Constants
    Private Const kColWidthItemID = 16
    Private Const kColWidthQtyShipped = 9
    Private Const kColWidthShipUnitMeasID = 7
    Private Const kColWidthQtyOrdered = 9
    Private Const kColWidthOrderUnitMeasID = 7
    Private Const kColWidthBin = 11
    Private Const kColWidthQtyAvail = 9
    Private Const kColWidthSubItem = 16
    
    'Constants for Message Grid
    Private Const kColMsgSeverity = 1
    Private Const kColMsgErrorCmnt = 2
    Private Const kColMsgSessionID = 3
    Private Const kColMsgEntryNo = 4
    Private Const kColMsgErrorType = 5
    
    Private Const kMaxMessageCols = 5
    
    'Constants for Error Severity in Commit Shipment
    Private Const kMsgAll = 0
    Private Const kMsgWarning = 1
    Private Const kMsgFatalError = 2

    'Commit Shipment Error Log
    Private miDfltSeverity              As Integer
    Private mbSkipPrintDialog           As Boolean
    Private mlSessionID                 As Long
    Public msReportPath                 As String
    Public moReport                     As clsReportEngine
    Public moDDData                     As clsDDData
    Public moPrinter                    As Printer
    Private mlErrorCount                As Long
    Private mlWarningCount              As Long
    
    Private Const kSecEventCommitShipment = "SOCOMMSHIP"
    
    'Constants for calling print invoice dialog
    Private Const kInvoicePrintNotReqd = False
    Private Const kInvoicePrintReqd = True

    'Header Status Constants
    Private Const kShipmentStatusFailed = -1
    Private Const kShipmentStatusPending = 0
    Private Const kShipmentStatusCommitted = 1
    
    '-- ShipmentLog TranStatus constants
    Private Const kTranStatusNoShipment = 0
    Private Const kShipTranStatusIncomplete = 1   'Incomplete
    Private Const kShipTranStatusPending = 2      'Pending
    Private Const kShipTranStatusPosted = 3       'Posted
    Private Const kShipTranStatusPurged = 4       'Purged
    Private Const kShipTranStatusVoid = 5         'Void
    Private Const kShipTranStatusCommitted = 6    'Committed
    Private Const kShipTranStatusInvoiced = 7     'Invoiced
    
    'Constants for Delivery Method
    Private Const kDeliveryMeth_Ship = 1
    Private mlShipLineCount             As Long
    Private Const kDeliveryMeth_DropShip = 2
    Private Const kDeliveryMeth_CounterSale = 3
    Private mlCounterSaleLineCount      As Long
    Private Const kDeliveryMeth_WillCall = 4
    Private mlWillCallLineCount         As Long
    Private Const kDeliveryMeth_Mixed = 9
    
    'Constatns for DeliveryMeth Option control array
    Private Const kCounterSale = 1
    Private Const kWillCall = 2
    Private Const kShippedOrder = 3
    Private Const kOpen = 1
    Private Const kInclZeroAvail = 1
    
    Private Enum ValidatePickListInd
        kPickListNoIsInvalid = 1
        kPickListUseDorpShipiDelMeth = 2
        kWhseDoesNotAllowImmediatePick = 3
        kPickListHasMultiShipToAddr = 4
        kPickListHasMultiOrder = 5
        kPickListHasMultiWhse = 6
        kPickListHasMultiShipVia = 7
        kPickListHasMultiShipment = 8
        kShipmentHasBeenVoided = 9
    End Enum
    
    'Constants for Context Menu
    Private Const kMenuDeleteLine = 6002
    Private Const kMenuDistribution = 6001
    
    'Constants for Tab Order
    Private Const kTabShipLine          As Integer = 0
    Private Const kTabMessages          As Integer = 1
    
    'Misc Constants
    Private Const kPickListNo = 1
    Private Const kDontLockData = 0
    Private Const kLockData = 1
    Private Const kImmediatePick = 1
    Private Const kSOStatusClosed = 4

    'AvaTax integration - General Declarations
    Private mbTrackSTaxOnSales      As Boolean ' Used to check AR TrackSTaxOnSales option
    Public moAvaTax                 As Object ' Avalara object of "AVZDADL1.clsAVAvaTaxClass", this is installed with the Avalara 3rd party add-on
    Public mbAvaTaxEnabled          As Boolean ' Defines if AvaTax is installed on the DB & Client , and is turned on for this company
    'AvaTax end

Const VBRIG_MODULE_ID_STRING = "ProcessSO.FRM"

Private Sub SaveHold()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++


    Dim sSQL As String
    
    sSQL = "UPDATE tsoPendShipment SET HoldFlag=" & gsGetValidStr(chkHold.Value)
    sSQL = sSQL & ",HoldReason=" & gsQuoted(txtHoldReason.Text)

    sSQL = sSQL & " WHERE ShipKey = " & gsGetValidStr(mlShipKey)
    

    moClass.moAppDB.ExecuteSQL sSQL
    
    txtHoldReason.Tag = txtHoldReason.Text
    chkHold.Tag = gsGetValidStr(chkHold.Value)
    
  
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SaveHold", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++

End Sub



Private Function bSaveHold() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim iUpdateCtr As Integer

    Dim sSQL As String


    iUpdateCtr = giGetValidInt(moClass.moAppDB.Lookup("UpdateCounter", "tsoPendShipment", "ShipKey = " & mlShipKey))

    If iUpdateCtr = miShipmentUpdateCtr Then

        sSQL = "UPDATE tsoPendShipment SET HoldFlag=" & gsGetValidStr(chkHold.Value)
        sSQL = sSQL & ",HoldReason=" & gsQuoted(txtHoldReason.Text)
        sSQL = sSQL & ",UpdateCounter = " & iUpdateCtr + 1
        sSQL = sSQL & " WHERE ShipKey = " & gsGetValidStr(mlShipKey)


        moClass.moAppDB.ExecuteSQL sSQL

        txtHoldReason.Tag = txtHoldReason.Text
        chkHold.Tag = gsGetValidStr(chkHold.Value)

        miShipmentUpdateCtr = iUpdateCtr + 1

        bSaveHold = True
    Else
        giSotaMsgBox Me, moClass.moSysSession, kmsgDMNoSaveConcurrency
    End If

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSaveHold", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++

End Function


Private Sub SelectGridBlock(ByVal lFirstRow As Long, _
                            ByVal lLastRow As Long, _
                            ByVal lMaxCols As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    With grdShipment
        .Row = lFirstRow
        .Col = 0
        .Row2 = lLastRow
        .Col2 = lMaxCols
        .Action = ActionSelectBlock
    End With

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SelectGridBlock", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub DeselectGridBlock(ByVal lFirstRow As Long, _
                              ByVal lLastRow As Long, _
                              ByVal lMaxCols As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    With grdShipment
        .Row = lFirstRow
        .Col = 0
        .Row2 = lLastRow
        .Col2 = lMaxCols
        .Action = ActionDeselectBlock
    End With

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DeselectGridBlock", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


Public Property Let sFormCaption(sNewFormCaption As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    msFormCaption = sNewFormCaption

'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sFormCaption", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
Private Sub GetModuleOptions()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moModuleOptions = New clsModuleOptions
    With moModuleOptions
        Set .oSysSession = moClass.moSysSession
        Set .oAppDB = moAppDB
        .sCompanyID = msCompanyID
    End With
    
    'Get Qty Decimal Places
    miNbrDecPlaces = giGetValidInt(moModuleOptions.CI("QtyDecPlaces"))
    
    'Look up integrate with IM Flag and AD module activation flag
    mbIMIntegrated = moAppDB.Lookup("IntegrateWithIM", "tsoOptions", "CompanyID = " & gsQuoted(msCompanyID))
    
    'Only get miAllowNegQty when IM enabled
    If mbIMIntegrated Then
        miAllowNegQty = giGetValidInt(moModuleOptions.IM("AllowNegQtyOnHand"))
    End If
    
    'Look up other flags in SO Options
    miCreditHoldAutoRelease = giGetValidInt(moModuleOptions.SO("CreditHoldAutoRelease"))
    miOverShipmentPolicy = giGetValidInt(moModuleOptions.SO("OverShipmentPolicy"))
    miMaxSerialNbrsOnPickList = giGetValidInt(moModuleOptions.SO("MaxSerialNbrsOnPickList"))
    mlHiddenBatchKey = glGetValidLong(moModuleOptions.SO("ShipmentHiddenBatchKey"))
                                                              
    'Set up the iOverShipmentPolicy property in moPrcoessPickList
    moProcessPickList.iOverShipmentPolicy = miOverShipmentPolicy
    
    'Look up PrintInvcs flag in AR Options
    miInvoiceReqdByOption = giGetValidInt(moModuleOptions.AR("PrintInvcs"))
    miUseNationalAccts = giGetValidInt(moModuleOptions.AR("UseNationalAccts"))
    
    'Look up the postdata the shipment
    miUseTranDateAsPostDate = giGetValidInt(moClass.moAppDB.Lookup("UseTranDateAsPostDate", "tsmUser", "UserID = " & gsQuoted(msUserID)))
    If miUseTranDateAsPostDate = 0 Then
        msPostDate = gsGetValidStr(moClass.moAppDB.Lookup("PostDate", "tsmUser", "UserID = " & gsQuoted(msUserID)))
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetModuleOptions", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function bGetLocalStrings() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bGetLocalStrings = False

    msDelMth_CounterSale = gsBuildString(kSOCounterSales, moAppDB, moClass.moSysSession)
    msDelMth_WillCall = gsBuildString(kSOWillCall, moAppDB, moClass.moSysSession)
    msDelMth_ShippedOrder = gsBuildString(kSOShippedOrder, moAppDB, moClass.moSysSession)
    msDelMth_Mixed = gsBuildString(kSOMixedDeliMeth, moAppDB, moClass.moSysSession)
    msPickTicket = gsBuildString(kSOPickTicket, moAppDB, moClass.moSysSession)
    
    msQtyPicked = gsBuildString(kSOQtyPicked, moAppDB, moClass.moSysSession)
    
    bGetLocalStrings = True

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bGetLocalStrings", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function



'************************************************************************
'   bLoadSuccess is used to assist the class in determining whether or
'   not the form loaded correctly.
'************************************************************************
Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bLoadSuccess = mbLoadSuccess
    
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bLoadSuccess", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

'************************************************************************
'   bCancelShutDown tells the framework whether the form has requested
'   the shutdown process to be cancelled.
'************************************************************************
Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancelShutDown = mbCancelShutDown

'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCancelShutDown", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property


Public Property Let lSOKey(lNewSOKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlSOKey = lNewSOKey

'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lSOKey", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property


Public Property Let sSOTranNo(sNewSOTranNo As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    msSOTranNo = sPadWithZero(10, sNewSOTranNo)

'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sSOTranNo", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Private Function bIsValidPickListNo(ByVal bBeep As Boolean) As Boolean
'************************************************************************************
' Desc: Is the Pick List valid?
' Returns:
'************************************************************************************
    On Error GoTo ExpectedErrorRoutine
    
    Dim lRetVal As Integer
    Dim iPickListInvalidIndicator As ValidatePickListInd
    Dim iRet As Integer
    
    bIsValidPickListNo = False

    'Does user enter a PickListNo?
    If Len(Trim(txtPickTicketNo.Text)) = 0 Then
        'No, exit
        bIsValidPickListNo = True
        Exit Function
    End If
    
    'Yes, user has entered a PickListNo, Zero-fill the Pick List Number if appropriate
    txtPickTicketNo.Text = sPadWithZero(10, txtPickTicketNo.Text)
        
    SetMouse
    
    'Run the stored procedure to validate/create (in the log) the PickListNo input.
    On Error GoTo ExpectedErrorRoutine
    With moAppDB
        .SetInParam txtPickTicketNo.Text                'PickList No
        .SetInParam mlSOKey                             'SOKey
        .SetInParam msCompanyID                         'Company ID
        .SetInParam gsFormatDateToDB(moClass.moSysSession.BusinessDate)
        .SetOutParam mlPickListKey                      'Pick List Key
        .SetOutParam iPickListInvalidIndicator          'Indicator for the reason why the pick list is invalid
        .SetOutParam mlShipKey                          'Output ShipKey
        .SetOutParam msShipmentNo                        'Output ShipmentNo
        .SetOutParam miTranStatus                       'Output Shipment TranStatus
        .SetOutParam gsFormatDateToDB(msShipmentTranDate) 'Output ShipmentTranDate
        .SetOutParam miShipmentCommitted                'Output Commit Status
        .SetOutParam miPickListDelMth                   'Output Delivery Method Type
        .SetOutParam mlWhseKey                          'Output WhseKey
        .SetOutParam miInvoiceReqdbyCust                'Output InvoiceRequird Flag
        .SetOutParam miShipmentUpdateCtr                'Oupput Shipment Update Counter
        .SetOutParam lRetVal                            'Return Code 0-Bad, 1-Good
        .ExecuteSP ("spsoGetPickLineForImmediatePick")
        mlPickListKey = glGetValidLong(.GetOutParam(5))
        iPickListInvalidIndicator = giGetValidInt(.GetOutParam(6))
        mlShipKey = glGetValidLong(.GetOutParam(7))
        msShipmentNo = gsGetValidStr(.GetOutParam(8))
        miTranStatus = giGetValidInt(.GetOutParam(9))
        msShipmentTranDate = gsGetValidStr(.GetOutParam(10))
        miShipmentCommitted = giGetValidInt(.GetOutParam(11))
        miPickListDelMth = giGetValidInt(.GetOutParam(12))
        mlWhseKey = glGetValidLong(.GetOutParam(13))
        miInvoiceReqdbyCust = giGetValidInt(.GetOutParam(14))
        miShipmentUpdateCtr = giGetValidInt(.GetOutParam(15))
        lRetVal = glGetValidLong(.GetOutParam(16))
        .ReleaseParams
    End With
 
 
    'Check Return Values of above stored procedure.
    If lRetVal <> 1 Then
        '0 = Unexpected Error Occurred.

        'Give an error message to the user.
        ResetMouse
        giSotaMsgBox Me, moClass.moSysSession, kmsgSPBadReturn, _
                     CVar("spsoGetPickLineForImmediatePick"), CVar("0")
    Else
        'Is the PickListNo valid?
        If (mlPickListKey = 0) Then
            'No, the PickListNo Is Invalid
            'Check the invalid reason and display a message
            Select Case iPickListInvalidIndicator
                Case kPickListNoIsInvalid
                    giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidEntity, "PickList No", txtPickTicketNo.Text
                    ClearForm

                Case kPickListUseDorpShipiDelMeth
                    giSotaMsgBox Me, moClass.moSysSession, kSOInvalidPickListMulDelMeth, txtPickTicketNo.Text
             
                Case kWhseDoesNotAllowImmediatePick
                    giSotaMsgBox Me, moClass.moSysSession, kSOInvalidPickListWhseNoImmedi, txtPickTicketNo.Text
                
                Case kPickListHasMultiShipToAddr
                    giSotaMsgBox Me, moClass.moSysSession, kSOInvalidPickListMulShipTo, txtPickTicketNo.Text
                
                Case kPickListHasMultiOrder
                    giSotaMsgBox Me, moClass.moSysSession, kSOInvalidPickListMulOrder, txtPickTicketNo.Text
                
                Case kPickListHasMultiWhse
                    giSotaMsgBox Me, moClass.moSysSession, kSOInvalidPickListMulWhse, txtPickTicketNo.Text
                
                Case kPickListHasMultiShipVia
                    giSotaMsgBox Me, moClass.moSysSession, kSOInvalidPickListMulShipVia, txtPickTicketNo.Text
            
                Case kPickListHasMultiShipment
                    giSotaMsgBox Me, moClass.moSysSession, kSOInvalidPickListMulShipment, txtPickTicketNo.Text
            
                Case kShipmentHasBeenVoided
                    ClearForm
            
            End Select
            ResetMouse
            
            'Set the PickLisNo back to the previous value
            txtPickTicketNo.Text = txtPickTicketNo.Tag
            'Set focus
            bSetFocus txtPickTicketNo
            Exit Function
        Else
            'Yes, Pick List is valid
            txtPickTicketNo.Tag = txtPickTicketNo.Text
            
            'Now, update the header control value in the form
            'Shipment No
            lblShipmentNo.Caption = msShipmentNo
            
            'Set up delivery Method Option
            'Disable the delivery method options and cmdCreatePick
            ToggleDeliveryMethOpt
            
            Select Case miPickListDelMth
                Case kDeliveryMeth_CounterSale
                    msPickListDelMth = msDelMth_CounterSale
                    OptPickType(kCounterSale) = True
                
                Case kDeliveryMeth_WillCall
                    msPickListDelMth = msDelMth_WillCall
                    OptPickType(kWillCall) = True
                    
                Case kDeliveryMeth_Ship
                    msPickListDelMth = msDelMth_ShippedOrder
                    OptPickType(kShippedOrder) = True
                    
                Case kDeliveryMeth_Mixed
                    msPickListDelMth = msDelMth_Mixed
                    OptPickType(kShippedOrder) = True
            End Select
            
            'Set up pick list type display
            lblPickType.Caption = msPickListDelMth & " " & msPickTicket
            
            'Set the distribution object for further distribution
            'editing in the from
            CreateDistForm mlWhseKey
            
            'Refresh the grid
            moDMShipLineGrid.Refresh
                        
            'Check whether there is any change need to apply to the toolbar
            ToggleControls
            
            cmdCreatePick.Enabled = False
            
            '-- Retrieve the invoice info if there is any
            RetrieveInvcInfo
        End If
    End If
        
    ResetMouse
    
    txtPickTicketNo.Enabled = False
    
    bIsValidPickListNo = True
    Exit Function

ExpectedErrorRoutine:
    'Give an error message to the user.
    ResetMouse
                    
    giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
                 CVar("spsoGetPickLineForImmediatePick: " & Err.Description)
    gClearSotaErr
    Exit Function


'+++ VB/Rig Begin Pop +++

#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidPickListNo", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Sub SetFormCaption()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sString As String
    
    'Put the Batch ID in the form caption.
    If (Len(Trim$(msSOTranNo)) > 0) Then
        'Get the localized string for 'Batch'.
        'sBatch = gsBuildString(kBatch, moAppDB, moClass.moSysSession)
        
        sString = "Process Sales Order " & msSOTranNo
        Me.Caption = sString
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetFormCaption", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
' Description:
'    lRunMode contains the user-defined run mode context in which the
'    class object was Initialzed. The run mode is extracted from the
'    context passed in through the InitializeObject.
'********************************************************************
    lRunMode = mlRunMode

'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlRunMode = lNewRunMode

'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
Private Function sPadWithZero(iLen As Integer, sText As String) As String
'************************************************************************
' Desc:  Load a masked control with a zero padded Number if
'        a Number is passed in
' Parms: iLen - Maximum Number of characters to use if No mask Set
'        ctlMask - Masked edit control
' Returns: Value sent in padded with zeros if a Number is passed in
'************************************************************************
    On Error GoTo ExpectedErrorRoutine
    
    Dim sSetText As String
    Dim bIsANumber As Boolean
    Dim lTheNumber As Long
    
    bIsANumber = True
    sSetText = Trim$(sText)
    
    If Len(sSetText) > 0 Then
        lTheNumber = CLng(sSetText)
        
        If lTheNumber <= 0 Then
            'Invalid - Must Be a Number > 0.
            bIsANumber = False
        End If
    End If
    
    If bIsANumber Then
        sPadWithZero = String(iLen - Len(sSetText), "0") & sSetText
    Else
        sPadWithZero = sText
    End If
    
    Exit Function
    
ExpectedErrorRoutine:
    bIsANumber = False
    gClearSotaErr
    Resume Next
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sPadWithZero", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Function bSalesOrderClosed() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++'

    bSalesOrderClosed = False
    
    If giGetValidInt(moClass.moAppDB.Lookup("Status", "tsoSalesOrder", "SOKey = " & mlSOKey)) = kSOStatusClosed Then
        bSalesOrderClosed = True
    End If

    Exit Function
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSalesOrderClosed", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Sub InitForPmnts(ByVal bUseMultiCurr As Boolean, ByVal dExchangeRate As Double, ByVal sDocCurrID As String, ByVal lCurrExchSchedKey As Long, _
    ByVal lCustExchSchedKey As Long, ByVal iHomePriceDecPlaces As Integer, ByVal iHomeRoundMeth As Integer, ByVal iHomeRoundPrecision As Integer, ByVal bReadOnly As Boolean, _
    ByVal sTranID As String, ByVal lBillToAddrKey As Long, ByVal iBillingType As Integer, ByVal sPurchOrderNo As String, ByVal dTranAmtHC As Double, _
    ByVal dTranAmt As Double, ByVal dFrtAmt As Double, ByVal dSTaxAmt As Double, ByVal dAmtInvcd As Double, ByVal iBillToParent As Integer, _
    ByVal lParentCustKey As Long, ByVal bDfltPmtByParent As Boolean, ByVal sOrderDate As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    mbUseMultiCurr = bUseMultiCurr
    mdExchangeRate = dExchangeRate
    msDocCurrID = sDocCurrID
    mlCurrExchSchedKey = lCurrExchSchedKey
    mlCustExchSchedKey = lCustExchSchedKey
    miHomePriceDecPlaces = iHomePriceDecPlaces
    miHomeRoundMeth = iHomeRoundMeth
    miHomeRoundPrecision = iHomeRoundPrecision
    mbSOPmntReadOnly = bReadOnly
    msTranID = sTranID
    mlBillToAddrKey = lBillToAddrKey
    miBillingType = iBillingType
    msPurchOrderNo = sPurchOrderNo
    mdTranAmtHC = dTranAmtHC
    mdTranAmt = dTranAmt
    mdFrtAmt = dFrtAmt
    mdSTaxAmt = dSTaxAmt
    mdAmtInvcd = dAmtInvcd
    miBillToParent = iBillToParent
    mlParentCustKey = lParentCustKey
    mbDfltPmtByParent = bDfltPmtByParent
    msOrderDate = sOrderDate
    
                    
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitForPmnts", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


Private Sub HandleBrowseClick(ByVal sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    On Error GoTo ExpectedErrorRoutine
    
    Dim sNewKey As String
    Dim lRetVal As Long
    Dim vParseRet As Variant
    Dim bValidKey As Boolean
    Dim iRetVal As Integer
    Dim iActionCode As Integer

    SetMouse

    If mbFocus Then
        ResetMouse
        Exit Sub
    End If
    
    iRetVal = iConfirmUnload(True)
    Select Case iRetVal
        Case Is = kDmSuccess, kDmNotAllowed
            'The Data Manager operation was successful or not allowed

        Case Is = kDmFailure, kDmError
            ResetMouse
            Exit Sub
        
        Case Else
            'Error processing
            'Give an error message to the user
            'Unexpected Confirm Unload Return Value: {0}
            ResetMouse
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedConfirmUnloadRV, _
                         CVar(iConfirmUnload)
            
            Exit Sub
    End Select
   
    'Set up the Main Navigator button
    If Not bSetupNavMain(msCompanyID) Then
        'Give an error message to the user
        'An error occurred while setting up the properties
        'for one of the Navigator buttons on this form.
        ResetMouse
        giSotaMsgBox Me, moClass.moSysSession, kmsgNavigatorSetupError
        
        Exit Sub
    End If
    
    'Execute the requested move.
    lRetVal = glLookupBrowse(navMain, sKey, miFilter, sNewKey)

    'Evaluate outcome of the requested browse move.
    Select Case lRetVal
        Case MS_SUCCESS
            vParseRet = gvParseLookupReturn(sNewKey)
            If IsNull(vParseRet) Then
                ResetMouse
                Exit Sub
            End If
            
            If Trim$(txtPickTicketNo.Text) <> Trim$(vParseRet(1)) Then
                'Clear out all the controls now
                ClearForm
                
                txtPickTicketNo.Text = Trim$(vParseRet(1))

                If Len(Trim$(txtPickTicketNo.Text)) > 0 Then
                    bValidKey = bIsValidPickListNo(True)
                    If bValidKey Then
                        'Do a .KeyChange event now
                        'VMIsValidKey
                    End If
                End If
            End If

            mbFocus = False

        Case Else
            mbFocus = True
            gLookupBrowseError lRetVal, Me, moClass
            mbFocus = False
    End Select

    ResetMouse
    
    Exit Sub
    
ExpectedErrorRoutine:
    ResetMouse

'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleBrowseClick", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub HandleCancelClick(ByVal sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    On Error GoTo ExpectedErrorRoutine

    Dim iRetVal As Integer
    
    SetMouse

    If mbFocus Then
        ResetMouse
        Exit Sub
    End If
    
    If Not moCCProcess Is Nothing Then
        If Not moCCProcess.bShutdownReportEngine Then
            Exit Sub
        End If
    End If
    
    'Force line DM to be dirty if hold is dirty (there is no header DM), so that user will be prompted to save changes.
    If IsHoldDirty Then
        moDMShipLineGrid.SetDirty True
    End If

    iRetVal = iConfirmUnload(True)
    Select Case iRetVal
        Case Is = kDmSuccess, kDmNotAllowed
            'The Data Manager operation was successful or not allowed
            If sKey = kTbCancel Then
                ClearForm
            Else
                ClearForm
                Me.Hide
            End If

        Case Is = kDmFailure, kDmError
            ResetMouse
            Exit Sub
        
        Case Else
            'Error processing
            'Give an error message to the user
            'Unexpected Confirm Unload Return Value: {0}
            ResetMouse
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedConfirmUnloadRV, _
                         CVar(iConfirmUnload)
            
            Exit Sub
    End Select
    
    ResetMouse
    Exit Sub
    
ExpectedErrorRoutine:
    ResetMouse

'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleCancelClick", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub HandleDeleteClick()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    On Error GoTo ExpectedErrorRoutine
    
    Dim iActionCode As Integer
    Dim iRetVal As Integer
    Dim sMsgString As String
    Dim sWhere As String
    
    If mlPickListKey = 0 Then Exit Sub
    
    SetMouse
    
    'Prompted the user for deletion confirmation
    'Prompt the user if they want to delete the pick lines for this shipmentas well
    iRetVal = giSotaMsgBox(Me, moClass.moSysSession, kSOPickLineDeleteConfirmation)
        
    Select Case iRetVal
        Case kretYes
            'delete the shiplines and inventory distributions
            sWhere = ""
            If Not bDeleteShipLines(sWhere) Then
                Exit Sub
            End If
            
            If Not bDeletePickList(mlPickListKey) Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation  'kmsgimspGeneralReserveInvtTran
                Exit Sub
            End If
            
            'Remove the logical lock
            RemoveLogicalLock

            ClearForm
            ToggleControls

        Case kretNo

            If Not bDeleteShipment Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation  'kmsgimspGeneralReserveInvtTran
                Exit Sub
            End If
            
            'If this a newly create pick list, upon leave the application, we need
            'to set the tsoShipLine.PickingComplete flag to true so the pick list will
            'be available to other pcocess like reprint pick list, generate shipment, etc.
            If mbNewCreatedPickList Then
                mbNewCreatedPickList = False
                ReversePickCompleteFlag
            End If

            ClearForm
            ToggleControls
            
        Case kretCancel
            'don't perform the delete
            Exit Sub
    End Select

    ResetMouse
    Exit Sub

ExpectedErrorRoutine:
    ResetMouse

'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleDeleteClick", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub HandleFinishClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    On Error GoTo ExpectedErrorRoutine
    
    SetMouse

    'Execute the save logic
    If bHandleSaveClick Then
        If sKey = kTbFinishExit Then
        
            If Not moCCProcess Is Nothing Then
                If Not moCCProcess.bShutdownReportEngine Then
                    Exit Sub
                End If
            End If
            
            'Clear the error log
            moProcessCommitTran.ClearMessages mlSessionID, moAppDB
            
            Me.Hide
        Else
            ClearForm
        End If
    End If
            
    ResetMouse
    Exit Sub
    
ExpectedErrorRoutine:
    ResetMouse
    
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "HandleFinishClick", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function bProcessCreditCards(oCCproc As clsProcessCCUtils) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++


    Dim bProcessAuthSuccess As Boolean

    sbrMain.Message = "Processing Credit Cards/EFT..."
    
    bProcessAuthSuccess = oCCproc.bProcessCreditCards(moClass.moAppDB, moClass.moSysSession, moClass.moFramework)
    sbrMain.Message = ""

    bProcessCreditCards = bProcessAuthSuccess
    
'+++ VB/Rig Begin Pop +++
        Exit Function
VBRigErrorRoutine:
        ResetMouse
        sbrMain.Message = ""
        gSetSotaErr Err, sMyName, "bProcessCreditCards", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Sub GetHoldValues()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL As String
Dim rs As Object


    sSQL = "SELECT HoldFlag, HoldReason, UpdateCounter FROM tsoPendShipment WHERE ShipKey = " & gsGetValidStr(mlShipKey)

    Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    With rs
        If Not rs.IsEmpty Then
            chkHold.Value = giGetValidInt(rs.Field("HoldFlag"))
            txtHoldReason.Text = gsGetValidStr(rs.Field("HoldReason"))
            miShipmentUpdateCtr = giGetValidInt(rs.Field("UpdateCounter"))
        End If
        .Close
    End With
    Set rs = Nothing
    
    chkHold.Tag = gsGetValidStr(chkHold.Value)
    txtHoldReason.Tag = txtHoldReason.Text
    
    If chkHold.Value = vbChecked Then
        chkHold.Enabled = True
        txtHoldReason.Enabled = True
    Else
        chkHold.Enabled = False
        txtHoldReason.Enabled = False
    End If
    
        
'+++ VB/Rig Begin Pop +++
    Exit Sub
    
    
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetHoldValues", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub



Private Sub HandleCommitClick()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

On Error GoTo ExpectedErrorRoutine
    
Dim iIgnoreWarnings As Integer
Dim iValidationOnly As Integer
Dim lWarningCount As Long
Dim lErrorCount As Long
Dim sSQL As String
Dim iSkipValidation As Integer
Dim bCCPaymentsExistForShipment
Dim bProceed As Boolean

Dim bProcessAuthSuccess As Boolean


Dim lInvcKey As Long ' AvaTax

    sbrMain.Status = SOTA_SB_BUSY
    
    'Check for security right
    If Not moProcessPickList.bOverrideSecEvent(Me, kSecEventCommitShipment) Then Exit Sub
    
    If moProcessCommitTran Is Nothing Then Exit Sub
    
    SetMouse
    'Execute the save logic to save any un-saved changes
    If Not bHandleSaveClick() Then
        'Clear the status bar
        sbrMain.Message = ""
        ResetMouse
        Exit Sub
    End If
    
    ClearMessages
    
    mlSessionID = 0
    'Execute the commit transaction logic
    If bPopTempTableTransToCommit Then
    
        If moClass.moSysSession.IsModuleActivated(kModuleCC) Then
        
            If moCCProcess Is Nothing Then
                ' setup the process Credit Cards class
                Set moCCProcess = New clsProcessCCUtils
            End If
            
            moCCProcess.ShipKey = mlShipKey
            
            bCCPaymentsExistForShipment = moCCProcess.PaymentsExistForShipment(mlShipKey, moClass.moAppDB, moClass.moSysSession, moClass.moFramework)
            
            'Set logical locks before pre-validation.  Pre-validate logic changes batch key on the shipment, which causes problems with concurrent
            'users.
            If bCCPaymentsExistForShipment Then
                moCCProcess.Init moClass.moAppDB, moClass.moSysSession, moClass.moFramework, False
                
                moCCProcess.BatchKey = mlHiddenBatchKey
                If Not moCCProcess.bGetLogicalLocks Then
                
                    mlSessionID = moCCProcess.SessionKey
                    If moProcessCommitTran.ProcessErrorLog(Me, moAppDB, mlSessionID, mlLanguage, msCompanyID) Then
                        DisplayMessages
                        moCCProcess.RemoveLogicalLocks
                        Exit Sub
                    End If
                    
                     moCCProcess.RemoveLogicalLocks
                    
                End If
            End If
             
            If bCCPaymentsExistForShipment Or chkHold.Value = vbChecked Then
                'Run validation if shipment is on hold -- this happens independently of invoicing, so could happen after invoiced.
                If (miTranStatus <> kShipTranStatusInvoiced) Or chkHold.Value = vbChecked Then
 
                    ' if there are any payments to process, call the commit in validate only mode.  Then call the process credit cards method
                    ' this will convert credit card authorizations into sales
                    ' it returns a succes/fail flag that indicates whether the shipment should stop
                    TabShipLine.Tab = kTabShipLine
                    iValidationOnly = 1
                    If moProcessCommitTran.CommitSingleTransaction(Me, moAppDB, iValidationOnly, iIgnoreWarnings, lWarningCount, lErrorCount, mlSessionID, iSkipValidation) Then
                        'Check whether message needed to be displayed
                        mlSessionID = moProcessCommitTran.SessionID
                        If moProcessCommitTran.ErrorCount > 0 Or moProcessCommitTran.WarningCount > 0 Then
                            If moProcessCommitTran.ShowMessage Then
                                'Populate the ErrorCmnt in tciErrorLog
                                If moProcessCommitTran.ProcessErrorLog(Me, moAppDB, mlSessionID, mlLanguage, msCompanyID) Then
                                    
                                    DisplayMessages
                                    
                                    bProceed = True
                                    
                                    If moProcessCommitTran.ErrorCount > 0 Then
                                        bProceed = False
                                    Else
                                        If moProcessCommitTran.WarningCount > 0 And moProcessCommitTran.ErrorCount = 0 Then
                                            If giSotaMsgBox(Me, moClass.moSysSession, kSOmsgCommitShipProceedComfirm, moProcessCommitTran.WarningCount) = kretNo Then
                                                bProceed = False
                                            End If
                                        End If
                                    End If
                                    
                                    If (Not bProceed) And bCCPaymentsExistForShipment Then
                                        moCCProcess.RemoveLogicalLocks
                                    End If
                                    
                                    If Not bProceed Then
                                        Exit Sub
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If

                If bCCPaymentsExistForShipment Then
                    moCCProcess.BeforeCommit = False 'Prevents locks from being removed -- commit will use same locks then clean up.
                    moCCProcess.SessionKey = mlSessionID
                    moCCProcess.BatchKey = mlHiddenBatchKey
                    
                    bProcessAuthSuccess = bProcessCreditCards(moCCProcess)
                                 
                    mlSessionID = moCCProcess.SessionKey
                    
                    If moCCProcess.MessagesExist Then
    
                        If moProcessCommitTran.ProcessErrorLog(Me, moAppDB, mlSessionID, mlLanguage, msCompanyID) Then
    
                            DisplayMessages
                            
                            'Continue if warnings only
                            If moCCProcess.ErrorCount > 0 Then
                                moCCProcess.RemoveLogicalLocks
                                GetHoldValues
                                Exit Sub
                            End If
    
                        End If
                
                    End If
                    
                End If
                
                RetrieveInvcInfo
                
                If muInvoiceInfo.lInvcKey > 0 Then
                     'If credit card processing successful then shipment will be invoiced at this point
                    miTranStatus = kShipTranStatusInvoiced
                    SetupShipmentStatus
                End If

            End If

        End If
    
              
        iValidationOnly = kNo
        
        'If credit card payments, already displayed warnings and prompted to proceed or not.
        If bCCPaymentsExistForShipment Then
            iIgnoreWarnings = kYes
        Else
            If mbCanCommitTransIgnoreWarning = True Then
                iIgnoreWarnings = kYes
            Else
                iIgnoreWarnings = kNo
            End If
        End If
            
        lWarningCount = moProcessCommitTran.WarningCount
        lErrorCount = moProcessCommitTran.ErrorCount
        
        If miTranStatus = kShipTranStatusInvoiced Then
            iSkipValidation = 1
        Else
        
            iSkipValidation = 0
        End If
        
        If chkHold.Value = vbChecked Then
            iSkipValidation = 0
        End If
        
        If moProcessCommitTran.CommitSingleTransaction(Me, moAppDB, iValidationOnly, iIgnoreWarnings, lWarningCount, lErrorCount, mlSessionID, iSkipValidation) Then
           'Check whether message needed to be displayed
            mlSessionID = moProcessCommitTran.SessionID
            
            If moProcessCommitTran.ShowMessage Then
                'Populate the ErrorCmnt in tciErrorLog
                If moProcessCommitTran.ProcessErrorLog(Me, moAppDB, mlSessionID, mlLanguage, msCompanyID) Then
                
                    DisplayMessages
                    
                End If
                       
            End If
            
            If bCCPaymentsExistForShipment Then
                moCCProcess.RemoveLogicalLocks
            End If
                
        Else
        
            If bCCPaymentsExistForShipment Then
                moCCProcess.RemoveLogicalLocks
            End If
            
            ResetMouse
            Exit Sub
        End If
    Else

        ResetMouse
        Exit Sub
    End If
    
    If moProcessCommitTran.CommitTranCount = 1 Then
        'Update the miShipmentCommitted flag and shipment status display
        miShipmentCommitted = kShipmentStatusCommitted
        miTranStatus = kShipTranStatusCommitted
        
        'Refresh the shipline grid and setup the toolbar and cmd buttons
        'due to the change in the shipment commit status
        sSQL = "UPDATE #tsoCreatePickWrk2 SET ShipmentCommitStatus = 1"
        sSQL = sSQL & " WHERE ShipKey = " & mlShipKey
        
        moAppDB.ExecuteSQL sSQL
        If Err.Number <> 0 Then
            gClearSotaErr
        End If
        
        moDMShipLineGrid.Refresh

        ToggleControls

        'AvaTax Integration - HandleCommitClick
        If mbAvaTaxEnabled Then
            lInvcKey = moClass.moAppDB.Lookup("InvoiceKey", "#tarInvoiceCreatedFromShipment", "")
            moAvaTax.bCalculateTax lInvcKey, kModuleAR
        End If
        'AvaTax end

    End If
    
    
    'Reset the status in status bar
    sbrMain.Status = SOTA_SB_START
    ResetMouse
    Exit Sub
    
ExpectedErrorRoutine:

    sbrMain.Status = SOTA_SB_START
    ResetMouse
    
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleCommitClick", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub DisplayMessages()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'Set the focus to the Message Tab
    mbDontProcessTabClick = True
    TabShipLine.Tab = kTabMessages
          
    'Display the messages
    moDMMessageGrid.Where = sBuildMessageWhere
    moDMMessageGrid.Refresh
    
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DisplayMessages", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


Private Sub HandlePrintClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
            
    'tbrMain.ButtonEnabled(skey) = False
    If Not bHandleSaveClick Then
        Exit Sub
    End If
    
    LoadPrintPickTicketForm sKey
    'tbrMain.ButtonEnabled(skey) = True
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandlePrintClick", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Private Sub LoadPrintPickTicketForm(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
      
'    Debug.Print CLng(moAppDB.GetHDBC())
      
    Set frmPrintPickTicket.oClass = moClass
    Set frmPrintPickTicket.oAppDB = moAppDB
    Set frmPrintPickTicket.oCreatePickList = moCreatePickList
    frmPrintPickTicket.lPickListKey = mlPickListKey
    Load frmPrintPickTicket
    
    '-- Attempt to initialize print form
    If Not frmPrintPickTicket.bInitPrintForm(moAppDB, msSOTranNo, txtPickTicketNo, _
                msPickListDelMth, miPickListDelMth, mlWhseKey) Then
        Unload frmPrintPickTicket
        Exit Sub
    End If
    
    With frmPrintPickTicket
        
        '-- Center print form
        .Top = (Me.Height / 2) + Me.Top - (.Height / 2)
        .Left = (Me.Width / 2) + Me.Left - (.Width / 2)
        
        If .lWhseRptSettingKey >= 0 And sKey = kTbPreview Then
        '--Report setting has been setup in whse, process the print job
        '--directly
            .HandleToolbarClick sKey
        Else
        '--Report setting has not been setup in whse, display the print
        '--pick dialog for use to pick the report setting
            .Show vbModal
        End If
    End With
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LoadPrintForm", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Public Function CMMenuSelected(ctl As Control, lTaskID As Long) As Boolean
'************************************************************************************
'      Desc: Called when a popup context menu item is selected.
'            Called because menu item was added by CMAppendContextMenu event.
'     Parms: Ctl:   The control that received the right mouse button down message.
'            lTaskID: The Task ID of the selected menu item.
'   Returns: True if successful; False if unsuccessful.
'************************************************************************************
    On Error GoTo ExpectedErrorRoutine
    
    Dim lActiveRow As Long
    Dim lShipLineKey As Long
    Dim sWhere As String
    Dim lWhseKey As Long

    CMMenuSelected = False
    
    If ctl Is grdShipment Then
        lActiveRow = grdShipment.ActiveRow
        If lActiveRow <= 0 Then
            Exit Function
        Else
            Select Case lTaskID
            Case kMenuDistribution
                'Initialize the Distribution object if not already initialized
                lWhseKey = glGetValidLong(gsGridReadCell(grdShipment, lActiveRow, kColLineShipWhseKey))
                CreateDistForm lWhseKey
    
                If Not moItemDist Is Nothing Then
                    If moProcessPickList.bIsValidQtyToPick(Me, moDMShipLineGrid, grdShipment, lActiveRow) Then
                    'If the QtyToPick is valid, load the distribution form
                    'so use can enter new distribution or view and update exist distribution
    
                        'Lock the Create Pick Form
                        frmProcessSO.Enabled = False
                        If Not moProcessPickList.bCreateManualDistribution(Me, grdShipment, moDMShipLineGrid, lActiveRow) Then
                            'moCreatePick.UpdateGridLine grdShipment, moDMShipLineGrid, lRow
                            grdShipment_Click kColLineQtyToPick, lActiveRow
                            grdShipment.SetFocus
                        End If
    
                        'Unlock the Create Pick Form
                        frmProcessSO.Enabled = True
                    Else
                    'QtyToPick is invalid, set the focus back the QtyToPick cell
                        grdShipment_Click kColLineQtyToPick, lActiveRow
                        grdShipment.SetFocus
                    End If
                End If
    
            Case kMenuDeleteLine
                'Warn before line deletion if Cust requires ship complete
                If giGetValidInt(gsGridReadCell(grdShipment, lActiveRow, kColLineCustReqShipComplete)) = 1 Then
                    If giSotaMsgBox(Me, moClass.moSysSession, kSOPartialPickWarn, "") = vbNo Then
                        Exit Function
                    End If
                End If
                
                'Delete the line from the LinePicked grid
                moGMShipLine.GridDelete
                ToggleControls
   
            End Select
        End If
    End If


    CMMenuSelected = True
    Exit Function

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CMMenuSelected", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function CMAppendContextMenu(ctl As Control, hmenu As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************************
'      Desc: Called to append menu items to right mouse popup menu.
'            Called because form_load did a .Bind *APPEND.
'     Parms: Ctl:   The control that received the right mouse button down message.
'            hmenu: The handle to the popup menu
'   Returns: True if menu item added; false if menu item not added.
'************************************************************************************
    
    Dim lRow As Long
    Dim sString As String
    Dim iShipmentCommitStatus As Integer
    Dim iExtShipmentExists As Integer
    
    CMAppendContextMenu = False
   
    If ctl Is grdShipment Then
        lRow = glGetValidLong(ctl.ActiveRow)
    
        If lRow > 0 Then
            iShipmentCommitStatus = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineShipmentCommitStatus))
            iExtShipmentExists = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineExtShipmentExists))
        
            If iShipmentCommitStatus = 0 And iExtShipmentExists = 0 Then
                sString = gsBuildString(kIMDistCaption, moAppDB, moClass.moSysSession)
                AppendMenu hmenu, MF_ENABLED, kMenuDistribution, sString

                sString = gsBuildString(kDeleteLine, moAppDB, moClass.moSysSession)
                AppendMenu hmenu, MF_ENABLED, kMenuDeleteLine, sString
            End If
      
        End If
    End If

    CMAppendContextMenu = True

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CMAppendContextMenu", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Sub CMMemoSelected(oCtl As Control)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iReply As Integer

    Const vNoClear = True

    If oCtl Is txtPickTicketNo Then
        If Len(Trim$(txtPickTicketNo.Text)) = 0 Then
            Exit Sub
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CMMemoSelected", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function bHandleSaveClick() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    On Error GoTo ExpectedErrorRoutine
    
    Dim iRetVal As Integer
    Dim lRow As Long
    
    bHandleSaveClick = False

    'The Save button was pressed by the user.
    SetMouse

    'Always perform a save to the active row of the grid since
    'click on toolbar does not always fire the leavecell event
    'of the grid
    FireGMCellChangeIfNeeded
    
    If IsHoldDirty() Then
        If Not bSaveHold Then
            Exit Function
        End If
    End If

    
    'Validate all data in the grid.  Raise warnings when distributions do not equal ship line
    'quantities.  Raise error when distribution qty exceed pick quantity.
    If miShipmentCommitted <> kShipmentStatusCommitted Then
        If moDMShipLineGrid.IsDirty Then
            moDMShipLineGrid.Save
            If bIsValidGridData Then
                'Save any un-saved changes
                If Not bSaveShipLine Then
                    'message
                    Exit Function
                End If
            Else
                Exit Function
            End If
        Else
            'Check whether the shipment has ever been committed or not
            If Not moProcessCommitTran Is Nothing And mlSessionID > 0 Then
                If moProcessCommitTran.SingleTranCommitStatus = 0 Or moProcessCommitTran.SingleTranCommitStatus = 1 Then
                    mbCanCommitTransIgnoreWarning = True
                End If
            End If
        End If
    End If
                
    'If this a newly create pick list, upon leave the application, we need
    'to set the tsoShipLine.PickingComplete flag to true so the pick list will
    'be available to other pcocess like reprint pick list, generate shipment, etc.
    If mbNewCreatedPickList Then
        mbNewCreatedPickList = False
        ReversePickCompleteFlag
    End If
    

    ResetMouse
    
    bHandleSaveClick = True
    Exit Function
    
ExpectedErrorRoutine:
    ResetMouse
    
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleSaveClick", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Private Function bIsValidGridData(Optional ByVal lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
 
Dim lRowCount           As Long
Dim iChoice             As Integer
Dim iNbrZeroQtyLines    As Integer
Dim lPickedCnt As Long

    bIsValidGridData = False ' assume all data is invalid
    
'    If grdShipment.MaxRows = 0 Then
'        bIsValidGridData = True
'        Exit Function
'    End If
    
    'Loop through Grid validating row by row
    If lRow = 0 Then
        For lRowCount = 1 To (glGridGetDataRowCnt(grdShipment))
            'No need to do validation on Qty or Distribution is QtyPicked is 0
            If (gdGetValidDbl(gsGridReadCell(grdShipment, lRowCount, kColLineQtyPicked)) <> 0) Then
                If bAmtToPickInvalid(lRowCount) Then
                    grdShipment.LeftCol = kColLineQtyToPick 'Else scrolls back only to QtyPicked col
                    grdShipment_Click kColLineQtyPicked, lRowCount
                    gGridSetActiveCell grdShipment, lRowCount, kColLineQtyPicked
                    ResetMouse
                    Exit Function
                End If
                If Not bIsValidDistribution(lRowCount) Then
                    grdShipment.LeftCol = kColLineQtyToPick 'Else scrolls back only to QtyPicked col
                    grdShipment_Click kColLineQtyPicked, lRowCount
                    gGridSetActiveCell grdShipment, lRowCount, kColLineQtyPicked
                    ResetMouse
                    Exit Function
                End If
            End If
        Next lRowCount
    Else
        'No need to do validation on Qty or Distribution is QtyPicked is 0
        If (gdGetValidDbl(gsGridReadCell(grdShipment, lRow, kColLineQtyPicked)) <> 0) Then
            If bAmtToPickInvalid(lRow) Then
                grdShipment.LeftCol = kColLineQtyToPick 'Else scrolls back only to QtyPicked col
                grdShipment_Click kColLineQtyPicked, lRow
                gGridSetActiveCell grdShipment, lRow, kColLineQtyPicked
                ResetMouse
                Exit Function
            End If
            If Not bIsValidDistribution(lRow) Then
                grdShipment.LeftCol = kColLineQtyToPick 'Else scrolls back only to QtyPicked col
                grdShipment_Click kColLineQtyPicked, lRow
                gGridSetActiveCell grdShipment, lRow, kColLineQtyPicked
                ResetMouse
                Exit Function
            End If
        End If
        moDMShipLineGrid.SetRowDirty lRow
    End If
    
    bIsValidGridData = True
    
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidGridData", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Private Function bIsValidDistribution(lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim sInvtTranID     As String
Dim lInvtTranKey    As Long
Dim iTrackMethod    As Integer
Dim iTrackByBin     As Integer
Dim lItemKey        As Long
Dim lWhseKey        As Long
Dim lOrigItemKey    As Long
Dim iConfirmed      As Integer
Dim dQtyToPick      As Double
Dim dQtyPicked      As Double
Dim dQtyDist        As Double
Dim iItemType       As Integer
Dim lSOUOMKey       As Long
Dim lUOMKey         As Long
Dim bResult         As Boolean
Dim lShipLineKey    As Long
Dim lLineTranType   As Long
Dim iWhseUseBin     As Integer
    
    bIsValidDistribution = False

    'Skip previously confirmed lines that are to be unconfirmed
    dQtyPicked = gdGetValidDbl(gsGridReadCell(grdShipment, lRow, kColLineQtyPicked))
    dQtyDist = gdGetValidDbl(gsGridReadCell(grdShipment, lRow, kColLineQtyDist))
    iItemType = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineItemType))
    lInvtTranKey = glGetValidLong(gsGridReadCell(grdShipment, lRow, kColLineInvtTranKey))
    iWhseUseBin = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineWhseUseBin))
    
    'If previosly confirmed or user is cancelling pick updates
    If dQtyPicked = 0 Then
        bIsValidDistribution = True
        Exit Function
    End If
    
    'Only distribute Finished Goods, Raw Materials and Assembled Kits (needed for kit components that are not these three types)
    Select Case iItemType
        Case 5, 6, 8
        Case Else
            bIsValidDistribution = True
            Exit Function
    End Select
    
    'If whse does not use bin, no distribution is required
    If iWhseUseBin = 0 Then
        bIsValidDistribution = True
        Exit Function
    End If

    iTrackMethod = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineTrackMeth))
    iTrackByBin = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineTrackQtyAtBin))
    dQtyToPick = gdGetValidDbl(gsGridReadCell(grdShipment, lRow, kColLineQtyToPick))
    lWhseKey = glGetValidLong(gsGridReadCell(grdShipment, lRow, kColLineShipWhseKey))
    lSOUOMKey = glGetValidLong(gsGridReadCell(grdShipment, lRow, kColLineOrderLineUOMKey))
    lUOMKey = glGetValidLong(gsGridReadCell(grdShipment, lRow, kColLineShipUOMKey))
    lItemKey = glGetValidLong(gsGridReadCell(grdShipment, lRow, kColLineSubItemKey))
    
    CreateDistForm lWhseKey
    
    If lItemKey = 0 Then
        lOrigItemKey = 0
        lItemKey = glGetValidLong(gsGridReadCell(grdShipment, lRow, kColLineItemKey))
    Else
        lOrigItemKey = glGetValidLong(gsGridReadCell(grdShipment, lRow, kColLineItemKey))
    End If

    If iTrackByBin = 0 And iTrackMethod = 0 And lWhseKey = 0 Then 'Item does not require distributions
        bIsValidDistribution = True
        Exit Function
    End If
    
    ' Item does require distributions but defaults can be used
    If (lUOMKey = lSOUOMKey) And (dQtyPicked = dQtyToPick) _
        And (iTrackMethod = 0) And (lOrigItemKey = 0) Then
        bIsValidDistribution = True
        Exit Function
    End If

    'Check to see if the distribution is valid by comparing the quantities
    'for QtyToPick and QtyPicked
    If dQtyPicked > dQtyDist Then
        'The the shipline is under distributed, bring up the Distribution form for
        'use to correct
        giSotaMsgBox Me, moClass.moSysSession, kSOmsgDistQtyNotEqualToQtyPick
'        If Not bCreateManualDistribution(Me, grdShipment, moItemDist, moDMLineGrid, lRow) Then
'            Exit Function
'        End If
    End If
    
    If dQtyDist > dQtyPicked Then
        'The the shipline is over distributed, bring up the Distribution form for
        'use to correct
        giSotaMsgBox Me, moClass.moSysSession, kSOmsgQtyDistCantExceedQtyPick
        grdShipment_Click kColLineQtyToPick, lRow
        grdShipment.SetFocus
        Exit Function
    End If

    bIsValidDistribution = True
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidDistribution", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Function bAmtToPickInvalid(lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
Dim dQtyPicked As Double
Dim dQtyAvail As Double
Dim dQtyToPick As Double
Dim dQtyDist As Double
Dim lSubItemKey As Long
Dim iItemType As Integer
Dim iAllowDecimalQty  As Integer

Const kDescOnly As Integer = 0

    bAmtToPickInvalid = False

    dQtyPicked = gdGetValidDbl(gsGridReadCell(grdShipment, lRow, kColLineQtyPicked))
    dQtyAvail = gdGetValidDbl(gsGridReadCell(grdShipment, lRow, kColLineQtyAvail))
    dQtyToPick = gdGetValidDbl(gsGridReadCell(grdShipment, lRow, kColLineQtyToPick))
    dQtyDist = gdGetValidDbl(gsGridReadCell(grdShipment, lRow, kColLineQtyDist))
    lSubItemKey = glGetValidLong(gsGridReadCell(grdShipment, lRow, kColLineSubItemKey))
    iItemType = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineItemType))
    iAllowDecimalQty = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineAllowDecimalQty))
       
'    If dQtyPicked < 0 And (iItemType <> IMS_USAGE_MISC_ITEM) And (iItemType <> IMS_USAGE_SERVICE) And (iItemType <> 0) Then
'        giSotaMsgBox Me, moClass.moSysSession, kSOmsgQtyToPickZero
'        bAmtToPickInvalid = True
'        Exit Function
'    End If
'    If Abs(dQtyPicked) > Abs(dQtyToPick) And dQtyPicked <> 0 And miOverShipmentPolicy = kOverShip_DisAllow Then
'        giSotaMsgBox Me, moClass.moSysSession, kSOmsgQtyToPickQtyOpen
'        bAmtToPickInvalid = True
'        Exit Function
'    End If
    If iItemType > 4 Then 'Only perform this edit for those items that are stocked
        If dQtyPicked > dQtyAvail And dQtyDist = 0 And lSubItemKey = 0 Then
            If miAllowNegQty = 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kSOmsgQtyToPickQtyAvail
                bAmtToPickInvalid = True
                Exit Function
            End If
        End If
        'Validate Decimal Qty Indicator
        If iAllowDecimalQty = 0 Then
            If (CLng(dQtyPicked) <> dQtyPicked) Then
                giSotaMsgBox Me, moClass.moSysSession, kSOmsgItemDontAllowDesQty
                bAmtToPickInvalid = True
                Exit Function
            End If
        End If
    End If
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bAmtToPickInvalid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Sub SetMouse()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'    Desc: Sets Mouse to a Busy Value.
'   Parms: N/A
' Returns: N/A
'***********************************************************************
    If Me.MousePointer <> vbHourglass Then
        Me.MousePointer = vbHourglass
    End If
  
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetMouse", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub ResetMouse()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'    Desc: Resets Mouse to Previous Value.
'   Parms: N/A
' Returns: N/A
'***********************************************************************
    Me.MousePointer = miMousePointer

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ResetMouse", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub



'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & 'Z' & <FormType>
'
'   <Module>   is 'CI', 'AP', 'GL', . . .
'   'Z'        is the Sage MAS 500 identifier.
'   <FormType> is 'M' = Maintenance, 'D' = data entry, 'I' = Inquiry,
'                 'P' = PeriodEnd, 'R' = Reports, 'L' = Listings, . . .
'************************************************************************
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    FormHelpPrefix = "SOZ"
    
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & 'Z' & <FormType>
'
'   <Module>   is 'CI', 'AP', 'GL', . . .
'   'Z'        is the Sage MAS 500 identifier.
'************************************************************************
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    WhatHelpPrefix = "SOZ"

'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property


'************************************************************************
'   Description:
'       Bind Grid Manager (handles right mouse clicks for grids).
'   Param:
'       <none>
'   Returns:
'************************************************************************
Private Sub BindGM()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Bind the Grid Manager for shipline
    Set moGMShipLine = New clsGridMgr

    With moGMShipLine
        Set .Grid = grdShipment
        Set .Form = frmProcessSO
        Set .DM = moDMShipLineGrid
        .GridType = kGridDataSheetNoAppend
        .GridSortEnabled = False
        
        Set moGridNavUOM = .BindColumn(kColLineShipUOM, navGridUOM)
        Set moGridNavUOM.ReturnControl = txtNavUOM
        Set moGridLkuAutoDistBin = .BindColumn(kColLineAutoDistBinID, lkuAutoDistBin)
        Set moGridLkuAutoDistBin.ReturnControl = txtlkuAutoDistBin
        
        .Init
    End With
    

    'Bind the Grid Manager for Commit Shipment Message
    Set moGMMessage = New clsGridMgr

    With moGMMessage
        Set .Grid = grdMessages
        Set .Form = frmProcessSO
        Set .DM = moDMMessageGrid
        .GridType = kGridInquiryDD
        .GridSortEnabled = False
       
        .Init
    End With
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindGM", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

'************************************************************************
'   Description:
'       Create Data Manager objects and bind controls to
'       the appropriate fields on the form.
'   Param:
'       <none>
'   Returns:
'************************************************************************
Private Sub BindForm()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    '=========================
    'Bind Ship Line DM
    '=========================
    Dim sWhere As String

    Set moDMShipLineGrid = New clsDmGrid
    
    With moDMShipLineGrid
        Set .Form = frmProcessSO
        Set .Session = moClass.moSysSession
        Set .Grid = grdShipment
        Set .Database = moAppDB

        .Table = "#tsoCreatePickWrk2"
        .UniqueKey = "ShipLineKey"
        .OrderBy = "OrderKey, LineNbr"
        .Where = "KitShipLineKey IS NULL "
        .NoAppend = True
        
        'Columns bound to #tsoCreatePickWrk2
        .BindColumn "AllowDecimalQty", kColLineAllowDecimalQty, SQL_SMALLINT
        .BindColumn "AllowImmedShipFromPick", kColLineAllowImmedShipFromPick, SQL_SMALLINT
        .BindColumn "AllowSubItem", kColLineAllowSubItem, SQL_SMALLINT
        .BindColumn "CurrDisplayRow", kColLineCurrDisplayRow, SQL_SMALLINT
        .BindColumn "CustReqShipComplete", kColLineCustReqShipComplete, SQL_SMALLINT
        .BindColumn "ExtShipmentExists", kColLineExtShipmentExists, SQL_SMALLINT
        .BindColumn "InvtLotNo", kColLineAutoDistLotNo, SQL_VARCHAR
        .BindColumn "InvtLotKey", kColLineAutoDistLotKey, SQL_INTEGER
        .BindColumn "InvtLotExpirationDate", kColLineAutoDistLotExpDate, SQL_DATE
        .BindColumn "InvtTranKey", kColLineInvtTranKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "InvtTranLinkKey", kColLineInvtTranID, SQL_INTEGER
        .BindColumn "ItemID", kColLineItem, SQL_VARCHAR
        .BindColumn "ItemKey", kColLineItemKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ItemDesc", kColLineItemDesc, SQL_VARCHAR
        .BindColumn "ItemReqShipComplete", kColLineItemReqShipComplete, SQL_SMALLINT
        .BindColumn "ItemType", kColLineItemType, SQL_SMALLINT
        .BindColumn "KitShipLineKey", kColLineKitShipLineKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "LineDistKey", kColLineTranLineDistKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "LineKey", kColLineTranLineKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "LineNbr", kColLineLine, SQL_INTEGER
        .BindColumn "OrderKey", kColLineTranKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "OrderLineUnitMeasID", kColLineOrderLineUOMID, SQL_VARCHAR
        .BindColumn "OrderLineUnitMeasKey", kColLineOrderLineUOMKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "Packed", kColLinePacked, SQL_SMALLINT
        .BindColumn "QtyDist", kColLineQtyDist, SQL_DECIMAL
        .BindColumn "QtyOrdered", kColLineQtyOrdered, SQL_DECIMAL
        .BindColumn "QtyPicked", kColLineQtyPicked, SQL_DECIMAL
        .BindColumn "QtyToPick", kColLineQtyToPick, SQL_DECIMAL
        .BindColumn "QtyAvail", kColLineQtyAvail, SQL_DECIMAL
        .BindColumn "SubstituteItemDesc", kColLineSubItem, SQL_VARCHAR
        .BindColumn "ShipKey", kColLineShipKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipmentCommitStatus", kColLineShipmentCommitStatus, SQL_SMALLINT
        .BindColumn "ShipLineKey", kColLineShipLineKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipLineDistKey", kColLineShipLineDistKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipLineUpdateCounter", kColLineShipLineUpdateCounter, SQL_INTEGER
        .BindColumn "ShipLineDistUpdateCounter", kColLineShipLineDistUpdateCounter, SQL_INTEGER
        .BindColumn "ShipToAddrKey", Nothing, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipUnitMeasID", kColLineShipUOM, SQL_VARCHAR
        .BindColumn "ShipUnitMeasKey", kColLineShipUOMKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipWhseKey", kColLineShipWhseKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "SubstituteItemKey", kColLineSubItemKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "TrackMeth", kColLineTrackMeth, SQL_SMALLINT
        .BindColumn "TrackQtyAtBin", kColLineTrackQtyAtBin, SQL_SMALLINT
        .BindColumn "TranType", kColLineTranType, SQL_INTEGER
        .BindColumn "WhseBinID", kColLineAutoDistBinID, SQL_VARCHAR
        .BindColumn "WhseBinKey", kColLineAutoDistBinKey, SQL_INTEGER
        .BindColumn "WhseUseBin", kColLineWhseUseBin, SQL_SMALLINT
        
        .Init
    End With
    
    'Bind the grid navigator for the UOM column.
    gbLookupInit navGridUOM, moClass, moAppDB, "UnitOfMeasure"

    gbLookupInit lkuAutoDistBin, moClass, moAppDB, "DistBin"
    
    
    '=========================
    'Bind Message DM
    '=========================
    Set moDMMessageGrid = New clsDmGrid
    
    With moDMMessageGrid
        Set .Form = frmProcessSO
        Set .Session = moClass.moSysSession
        Set .Grid = grdMessages
        Set .Database = moAppDB

        .Table = "tciErrorLog"
        .UniqueKey = "SessionID, EntryNo"
        .OrderBy = "SessionID, EntryNo"
        .NoAppend = True
        
        'Columns bound to tciErrorLog
        .BindColumn "Severity", kColMsgSeverity, SQL_SMALLINT
        .BindColumn "ErrorCmnt", kColMsgErrorCmnt, SQL_SMALLINT
        .BindColumn "SessionID", kColMsgSessionID, SQL_SMALLINT
        .BindColumn "EntryNo", kColMsgEntryNo, SQL_SMALLINT
        .BindColumn "ErrorType", kColMsgErrorType, SQL_SMALLINT
        .ManualLoad = True
        .Init
    End With
    
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindForm", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***************************************************
'  Instantiate the Context Menu Class.
'***************************************************
    Set moContextMenu = New clsContextMenu  'Instantiate Context Menu Class
    
    With moContextMenu
        .BindGrid moGMShipLine, grdShipment.hwnd
        .Bind "*APPEND", grdShipment.hwnd
        
        Set .Form = frmProcessSO

        .Init
    End With

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function iConfirmUnload(Optional vNoClear As Variant) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim iRetVal As Integer
    
    If IsMissing(vNoClear) Then
        vNoClear = False
    End If
    
    'If currently there is a pick list loaded, check whether it
    'is a newly create pick list, if it is, we need to reverse the
    'PickingComplete flag for the ship lines before we leave
    If mbNewCreatedPickList And mlPickListKey > 0 Then
        mbNewCreatedPickList = False
        ReversePickCompleteFlag
    End If
    
    'Always perform a save to the active row of the grid since
    'click on toolbar does not always fire the leavecell event
    'of the grid
    FireGMCellChangeIfNeeded
    
    If moDMShipLineGrid.IsDirty Then
        
        iRetVal = giSotaMsgBox(Me, moClass.moSysSession, kmsgSaveChanges)
        
        If iRetVal = kretYes Then
        
            'Proceed with save
            HandleToolbarClick kTbSave

        ElseIf iRetVal = kretCancel Then

            iConfirmUnload = kDmFailure
            Exit Function
            
        ElseIf iRetVal = kretNo Then
            'Only process "cancel" (equivalent of canceling from toolbar) when the form is actually being cleared, as this will delete
            If Not vNoClear Then
                iConfirmUnload = kDmFailure
            Else
                iConfirmUnload = kDmSuccess
            End If
            Exit Function
        End If
    End If

    iConfirmUnload = kDmSuccess
    
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iConfirmUnload", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Function bSetFocus(oControl As Object) As Boolean
    On Error GoTo ExpectedErrorRoutine

    bSetFocus = False

    If Not (oControl Is Nothing) Then
        If oControl.Enabled = True Then
            If oControl.Visible = True Then
                On Error Resume Next
                oControl.SetFocus
            End If
        End If
    End If
    
    bSetFocus = True
    Exit Function

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSetFocus", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Sub ClearForm()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*************************************************************************
' Desc:    Cleans up form.
'*************************************************************************
    Dim lCurrMaxRows As Long
    Dim lCurrMaxCols As Long
    Dim sSQL As String

    mbDontChkClick = True

    With txtPickTicketNo
        .Enabled = True
        .Protected = False
        .TabStop = True
        .BackColor = &H80000005   'For some reason the backcolor wasn't getting reset properly.
        .Text = Empty
        .Tag = Empty
    End With
    
    'Clear work table
    On Error Resume Next
    sSQL = "TRUNCATE TABLE #tsoCreatePickWrk2"
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        gClearSotaErr
    End If
    On Error GoTo VBRigErrorRoutine
    
    'Refreshed the grid
    moDMShipLineGrid.Refresh
    
    'Clear the Form
    moGMShipLine.Clear
    moGMMessage.Clear
    
    TabShipLine.Tab = kTabShipLine

    'Clear all message in ErrorLog
    If mbLoadSuccess Then
        moProcessCommitTran.ClearMessages mlSessionID, moAppDB
        mlErrorCount = 0
        mlWarningCount = 0
        sbrMain.Message = ""
        mlSessionID = 0
    End If

    'Clear other module level variables.
    miInvoiceReqdbyCust = 0
    mlPickListKey = 0
    mlShipKey = 0
    miShipmentCommitted = 0
    mbNewCreatedPickList = False
    mbCanCommitTransIgnoreWarning = False
    lblShipmentStatusText.Caption = ""

    '-- clear invoice related fields
    With muInvoiceInfo
        .dFrtAmt = 0
        .dSalesAmt = 0
        .dSTaxAmt = 0
        .dTradeDiscAmt = 0
        .dTradeDiscPct = 0
        .dTranAmt = 0
        .dTranAmtHC = 0
        .lInvcKey = 0
        .sCurrID = ""
        .sInvcNo = ""
    End With
    
    miTranStatus = 0
    mlInvcKey = 0
    miMultiInvc = 0
    curInvoiceTotal.Amount = 0
    
    curInvoiceTotal.Visible = False
    lblInoviceTotal.Visible = False
    cmdInvcTotals.Visible = False
    cmdGenInvoice.Enabled = False
    cmdDelInvoice.Enabled = False
    cmdInvcTotals.Enabled = False
    lblInvcCurrID.Visible = False
    lblMultiInvc.Visible = False
  
    lblShipmentNo.Caption = ""
    lblPickType.Caption = ""

    'Disable command controls
    cmdDist.Enabled = False
    cmdComponents.Enabled = False
    'cmdCreatePick.Enabled = False
    chkHold.Enabled = True
    chkHold.Value = vbUnchecked
    chkHold.Tag = gsGetValidStr(chkHold.Value)
    
    txtHoldReason.Text = ""
    txtHoldReason.Tag = ""
    txtHoldReason.Enabled = False
    
    ToggleDeliveryMethOpt
    ToggleControls

    bSetFocus txtPickTicketNo

    ResetMouse

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ClearForm", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub FormatGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Desc: Initializes/formats the 'Ship Lines' Grid and 'Messages' Grid.
'***********************************************************************
    Dim sString As String
    
    '=======================
    'Set up Ship Lines Grid
    '=======================
    'Set Default Properties for the grid.
    gGridSetProperties grdShipment, kMaxLineCols, kGridDataSheetNoAppend
    gGridSetColors grdShipment

    'Set some additional grid properties (or overwrite the standard ones).
    With grdShipment
        .OperationMode = SS_OP_MODE_NORMAL
        .AllowMultiBlocks = False
        .SelectBlockOptions = SS_SELBLOCKOPT_ALL
        .RowHeaderDisplay = DispNumbers
        .DisplayRowHeaders = False 'true
        .NoBeep = True 'prevent beeps on locked cells
    End With
    
    'Set the Header for 'SOLine'
'    sString = gsBuildString(kSOLine, moAppDB, moClass.moSysSession)
'    gGridSetHeader grdShipment, kColSOLineNo, sString
    
    'Set the Header for 'Item'.
    sString = gsBuildString(kItem, moAppDB, moClass.moSysSession)
    gGridSetHeader grdShipment, kColLineItem, sString
    
    'Set the Header for 'Pick UOM'.
    sString = gsBuildString(kSOPickUOM, moAppDB, moClass.moSysSession)
    gGridSetHeader grdShipment, kColLineShipUOM, sString
    
    'Set the Header for 'QtyShipped'
    sString = gsBuildString(kSOQtyPicked, moAppDB, moClass.moSysSession)
    gGridSetHeader grdShipment, kColLineQtyPicked, sString
    
    'Set the Header for 'QtyOrdered'.
    sString = gsBuildString(kSOQtyOrd, moAppDB, moClass.moSysSession)
    gGridSetHeader grdShipment, kColLineQtyOrdered, sString
    
    'Set the Header for 'OrderUnitMeasID'.
    sString = gsBuildString(kSOAbbrOrdUOM, moAppDB, moClass.moSysSession)
    gGridSetHeader grdShipment, kColLineOrderLineUOMID, sString
    
    'Set the Header for 'Bin'.
    sString = gsBuildString(kSOBin, moAppDB, moClass.moSysSession)
    gGridSetHeader grdShipment, kColLineAutoDistBinID, sString
    
    'Set the Header for 'QtyAvail'.
    sString = gsBuildString(kSOAvailToShip, moAppDB, moClass.moSysSession)
    gGridSetHeader grdShipment, kColLineQtyAvail, sString
    
    'Set the Header for 'Sub'.
    sString = gsBuildString(kSOSRegSubItem, moAppDB, moClass.moSysSession)
    gGridSetHeader grdShipment, kColLineSubItem, sString

    'Set the Grid Column Widths
    'gGridSetColumnWidth grdShipment, kColSOLineNo, kColWidthSOLineNo
    gGridSetColumnWidth grdShipment, kColLineItem, kColWidthItemID
    gGridSetColumnWidth grdShipment, kColLineShipUOM, kColWidthShipUnitMeasID
    gGridSetColumnWidth grdShipment, kColLineQtyPicked, kColWidthQtyShipped
    gGridSetColumnWidth grdShipment, kColLineQtyOrdered, kColWidthQtyOrdered
    gGridSetColumnWidth grdShipment, kColLineOrderLineUOMID, kColWidthOrderUnitMeasID
    gGridSetColumnWidth grdShipment, kColLineAutoDistBinID, kColWidthBin
    gGridSetColumnWidth grdShipment, kColLineQtyAvail, kColWidthQtyAvail
    gGridSetColumnWidth grdShipment, kColLineSubItem, kColWidthSubItem

    'Align the Visible Columns.
'    gGridHAlignColumn grdShipment, kColCRSLCustRtrnLineNo, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER

    'Set the Column Types for Hidden Columns.
'    gGridSetColumnType grdShipment, kColCRSLShipLineKey, SS_CELL_TYPE_STATIC_TEXT

    'Set the Column Types for Visible Columns.
    'GridSetColumnType grdShipment, kColSOLineNo, SS_CELL_TYPE_STATIC_TEXT
    gGridSetColumnType grdShipment, kColLineItem, SS_CELL_TYPE_EDIT, 30
    gGridSetColumnType grdShipment, kColLineShipUOM, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdShipment, kColLineQtyPicked, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdShipment, kColLineQtyOrdered, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdShipment, kColLineOrderLineUOMID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdShipment, kColLineAutoDistBinID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdShipment, kColLineQtyAvail, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdShipment, kColLineSubItem, SS_CELL_TYPE_EDIT, 30

    'Hide the appropriate columns.
    gGridHideColumn grdShipment, kColLineAutoDist
    gGridHideColumn grdShipment, kColLineAutoDistBinQtyAvail
    gGridHideColumn grdShipment, kColLineAutoDistBinUOMKey
    gGridHideColumn grdShipment, kColLineAutoDistLotExpDate
    gGridHideColumn grdShipment, kColLineAutoDistLotNo
    gGridHideColumn grdShipment, kColLineAllowDecimalQty
    gGridHideColumn grdShipment, kColLineAllowImmedShipFromPick
    gGridHideColumn grdShipment, kColLineAllowSubItem
    gGridHideColumn grdShipment, kColLineCompItemQty
    gGridHideColumn grdShipment, kColLineCrHold
    gGridHideColumn grdShipment, kColLineCurrDisplayRow
    gGridHideColumn grdShipment, kColLineCustReqShipComplete
    gGridHideColumn grdShipment, kColLineDeliveryMethod
    gGridHideColumn grdShipment, kColLineDistComplete
    gGridHideColumn grdShipment, kColLineExtShipmentExists
    gGridHideColumn grdShipment, kColLineInvtTranID
    gGridHideColumn grdShipment, kColLineInvtTranKey
    gGridHideColumn grdShipment, kColLineItemDesc
    gGridHideColumn grdShipment, kColLineItemKey
    gGridHideColumn grdShipment, kColLineItemReqShipComplete
    gGridHideColumn grdShipment, kColLineItemType
    gGridHideColumn grdShipment, kColLineKitShipLineKey
    gGridHideColumn grdShipment, kColLineLine
    gGridHideColumn grdShipment, kColLineLineHold
    gGridHideColumn grdShipment, kColLineLineHoldReason
    gGridHideColumn grdShipment, kColLineOrderLineUOMKey
    gGridHideColumn grdShipment, kColLineOrderNo
    gGridHideColumn grdShipment, kColLineOrdHold
    gGridHideColumn grdShipment, kColLinePacked
    gGridHideColumn grdShipment, kColLinePickCheck
    'gGridHideColumn grdShipment, kColLineQtyAvail
    gGridHideColumn grdShipment, kColLineQtyDist
    gGridHideColumn grdShipment, kColLineQtyShort
    gGridHideColumn grdShipment, kColLineQtyToPick
    gGridHideColumn grdShipment, kColLineRcvgWarehouse
    gGridHideColumn grdShipment, kColLineSASequence
    gGridHideColumn grdShipment, kColLineShipDate
    gGridHideColumn grdShipment, kColLineShipKey
    gGridHideColumn grdShipment, kColLineShipLineDistKey
    gGridHideColumn grdShipment, kColLineShipLineDistUpdateCounter
    gGridHideColumn grdShipment, kColLineShipLineKey
    gGridHideColumn grdShipment, kColLineShipLineUpdateCounter
    gGridHideColumn grdShipment, kColLineShipmentCommitStatus
    gGridHideColumn grdShipment, kColLineShipPriority
    gGridHideColumn grdShipment, kColLineShiptoAddr
    gGridHideColumn grdShipment, kColLineShiptoAddrID
    gGridHideColumn grdShipment, kColLineShiptoName
    gGridHideColumn grdShipment, kColLineShipUOMKey
    gGridHideColumn grdShipment, kColLineShipVia
    gGridHideColumn grdShipment, kColLineShipWhseID
    gGridHideColumn grdShipment, kColLineShipWhseKey
    gGridHideColumn grdShipment, kColLineShortPickFlag
    gGridHideColumn grdShipment, kColLineStatus
    gGridHideColumn grdShipment, kColLineStatusCustShipCompleteViolation
    gGridHideColumn grdShipment, kColLineStatusDistQtyShort
    gGridHideColumn grdShipment, kColLineStatusDistWarning
    gGridHideColumn grdShipment, kColLineStatusItemShipCompLeteViolation
    gGridHideColumn grdShipment, kColLineStatusOverShipment
    gGridHideColumn grdShipment, kColLineSubFlag
    'gGridHideColumn grdShipment, kColLineSubItem
    gGridHideColumn grdShipment, kColLineSubItemKey
    gGridHideColumn grdShipment, kColLineTrackMeth
    gGridHideColumn grdShipment, kColLineTrackQtyAtBin
    gGridHideColumn grdShipment, kColLineTranKey
    gGridHideColumn grdShipment, kColLineTranLineDistKey
    gGridHideColumn grdShipment, kColLineTranLineKey
    gGridHideColumn grdShipment, kColLineTranType
    gGridHideColumn grdShipment, kColLineTranTypeText
    gGridHideColumn grdShipment, kColLineQtyToPickByOrdUOM
    gGridHideColumn grdShipment, kColLineWhseUseBin
    gGridHideColumn grdShipment, kColLineAutoDistLotKey
    gGridHideColumn grdShipment, kColLineAutoDistBinKey

    'Freeze the Appropriate Columns.
    gGridFreezeCols grdShipment, kColFrozenCols

    'Set the Grid Colors.
    'gGridSetColors grdShipment

    'Lock the appropriate columns.
    gGridLockColumn grdShipment, kColLineItem
    gGridLockColumn grdShipment, kColLineQtyAvail
    gGridLockColumn grdShipment, kColLineQtyOrdered
    gGridLockColumn grdShipment, kColLineOrderLineUOMID
    gGridLockColumn grdShipment, kColLineSubItem
    
    '=======================
    'Set up Messages Grid
    '=======================
    'Set Default Properties for the grid.
    gGridSetProperties grdMessages, kMaxMessageCols, kGridInquiryDD
    gGridSetColors grdMessages

    'Set some additional grid properties (or overwrite the standard ones).
    With grdMessages
        .OperationMode = SS_OP_MODE_NORMAL
        .AllowMultiBlocks = False
        .SelectBlockOptions = SS_SELBLOCKOPT_ALL
        .RowHeaderDisplay = DispNumbers
        .DisplayRowHeaders = False 'true
        .NoBeep = True 'prevent beeps on locked cells
        .TypeMaxEditLen = 255
    End With
   
    'Set the Header for 'Severity'.
    sString = gsBuildString(kDDSeverity, moAppDB, moClass.moSysSession)
    gGridSetHeader grdMessages, kColMsgSeverity, sString
    
    'Set the Header for 'Notification'.
    sString = gsBuildString(kstrMFMRPNotification, moAppDB, moClass.moSysSession)
    gGridSetHeader grdMessages, kColMsgErrorCmnt, sString
    
    'Set the Header for 'SessionID'
    gGridSetHeader grdMessages, kColMsgSessionID, "SessionID"
    
    'Set the Header for 'Entry No'
    gGridSetHeader grdMessages, kColMsgEntryNo, "EntryNo"
    
    'Set the Header for 'ErrorType'.
    gGridSetHeader grdMessages, kColMsgErrorType, "ErrorType"
  
    'Set the Grid Column Widths
    gGridSetColumnWidth grdMessages, kColMsgSeverity, 14
    gGridSetColumnWidth grdMessages, kColMsgErrorCmnt, 44

    'Set the Column Types for Visible Columns.
    gGridSetColumnType grdMessages, kColMsgSeverity, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdMessages, kColMsgErrorCmnt, SS_CELL_TYPE_EDIT
 
    'Hide the appropriate columns.
    gGridHideColumn grdMessages, kColMsgSessionID
    gGridHideColumn grdMessages, kColMsgEntryNo
    gGridHideColumn grdMessages, kColMsgErrorType

    'Set the Grid Colors.
    'gGridSetColors grdShipment

    'Lock the appropriate columns.
    'gGridLockColumn grdShipment, kColLineShipUOM

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormatGrid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub cboSeverity_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick cboSeverity, True
    #End If
'+++ End Customizer Code Push +++
    
    If mbLoadSuccess Then
        'Present any warnings and errors to the user in the Messages tab grid.
        moDMMessageGrid.Where = sBuildMessageWhere
        moDMMessageGrid.Refresh
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cboSeverity_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Sub

Private Sub chkHold_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkHold, True
    #End If
'+++ End Customizer Code Push +++

    If chkHold.Value = vbUnchecked Then
        txtHoldReason.Text = ""
        txtHoldReason.Enabled = False
    Else
        txtHoldReason.Enabled = True
    End If
        
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "chkHold_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Sub


Private Sub cmdApply_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdApply, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++

On Error GoTo ExpectedErrorRoutine

    Dim bProcessAuthSuccess As Boolean
    Dim iValidationOnly As Integer
    Dim iIgnoreWarnings As Integer
    Dim iSkipValidation As Integer
    Dim bCCPmntsExist As Boolean
    
    Dim lWarningCount As Long
    Dim lErrorCount As Long
    
    If mbBusy Then
        Exit Sub
    End If
    
    mbBusy = True
    
    If Not bHandleSaveClick() Then
        mbBusy = False
        Exit Sub
    End If
    
    
    iValidationOnly = 1
    iIgnoreWarnings = 1
    iSkipValidation = 0
    
    
    
    If Not bPopTempTableTransToCommit() Then
        mbBusy = False
        Exit Sub
    End If
    
    moProcessCommitTran.ClearMessages mlSessionID, moAppDB
    
    If moCCProcess Is Nothing Then
        Set moCCProcess = New clsProcessCCUtils
    End If
    
    If mlShipKey <> 0 Then
        moCCProcess.ShipKey = mlShipKey
        moCCProcess.BatchKey = mlHiddenBatchKey
        
    
        bCCPmntsExist = moCCProcess.PaymentsExistForShipment(mlShipKey, moClass.moAppDB, moClass.moSysSession, moClass.moFramework)
        
        If bCCPmntsExist Then
            moCCProcess.Init moClass.moAppDB, moClass.moSysSession, moClass.moFramework, False
            
            If Not moCCProcess.bGetLogicalLocks Then
            
                mlSessionID = moCCProcess.SessionKey
                
                moProcessCommitTran.SessionID = moCCProcess.SessionKey
                
                If moProcessCommitTran.ProcessErrorLog(Me, moAppDB, mlSessionID, mlLanguage, msCompanyID) Then
                 'Set the focus to the Message Tab
                    mbDontProcessTabClick = True
                    TabShipLine.Tab = kTabMessages
                
                    'Display the messages
                    moDMMessageGrid.Where = sBuildMessageWhere
                    moDMMessageGrid.Refresh
                    
                    moProcessCommitTran.ClearMessages mlSessionID, moAppDB
                End If
                 
                moCCProcess.RemoveLogicalLocks
                mbBusy = False
                Exit Sub
            End If
        End If
    End If
    
           
    If moProcessCommitTran.CommitSingleTransaction(Me, moAppDB, iValidationOnly, iIgnoreWarnings, lWarningCount, lErrorCount, mlSessionID, iSkipValidation) Then
    'Check whether message needed to be displayed
        mlSessionID = moProcessCommitTran.SessionID
              
        If moProcessCommitTran.WarningCount > 0 Or moProcessCommitTran.ErrorCount > 0 Then
        'If moProcessCommitTran.ShowMessage Then
        'Populate the ErrorCmnt in tciErrorLog
            If moProcessCommitTran.ProcessErrorLog(Me, moAppDB, mlSessionID, mlLanguage, msCompanyID) Then
            
                'Set the focus to the Message Tab
                mbDontProcessTabClick = True
                TabShipLine.Tab = kTabMessages
            
                'Display the messages
                moDMMessageGrid.Where = sBuildMessageWhere
                moDMMessageGrid.Refresh
            
                If moProcessCommitTran.ErrorCount > 0 Then
                    If bCCPmntsExist Then
                        moCCProcess.RemoveLogicalLocks
                    End If
                    mbBusy = False
                    Exit Sub
                End If
            End If
        Else
            moDMMessageGrid.Refresh
        End If
        
    End If

        
    If mlShipKey <> 0 Then
        
        If bCCPmntsExist Then
            
            If bHandleSaveClick Then
                
                'Clear the error log.   We don't want to show pre-commit warnings on the CC report.
                moProcessCommitTran.ClearMessages mlSessionID, moClass.moAppDB
                                
                moCCProcess.BeforeCommit = True 'indicator that logical locks should be removed (since commit is not being done at this point)
                
                                
                bProcessAuthSuccess = bProcessCreditCards(moCCProcess)
                
                'Reload in order to refresh form, enable/disable and retrieve invoice information
                bIsValidPickListNo False
                ProcessCCReport
               
            End If
            
        Else
            
            If mlInvcKey = 0 And miMultiInvc = 0 Then
                GenerateInvoice
            Else
                ApplyPmt
            End If
        End If
    End If
    
    mbBusy = False
   
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}

ExpectedErrorRoutine:

    mbBusy = False
    
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cmdApply_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Sub

Private Sub ApplyPmt()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++

    Dim sSQL As String
    Dim iRetVal As Integer
        

       
    ' create sql to create #PendInvcs required to create payment applications

        ' create table
        ' populate with invckey
        sSQL = "IF OBJECT_ID('tempdb..#PendInvcs') IS NOT NULL "
        sSQL = sSQL & " BEGIN TRUNCATE TABLE #PendInvcs END "
        sSQL = sSQL & " ELSE BEGIN CREATE TABLE #PendInvcs(InvcKey INT NOT NULL)  END "

        moAppDB.ExecuteSQL sSQL


        sSQL = "INSERT #PendInvcs(InvcKey) SELECT InvcKey FROM tarInvoiceDetl WITH (NOLOCK) JOIN tsoShipLine WITH (NOLOCK) " & _
                "ON tarInvoiceDetl.ShipLineKey = tsoShipLine.ShipLineKey WHERE ShipKey = " & mlShipKey
                
        moAppDB.ExecuteSQL sSQL
        
        'execute sp to apply payments
        With moClass.moAppDB
            .SetInParam moClass.moSysSession.UserId
            .SetInParam moClass.moSysSession.CurrencyID
            .SetInParam moClass.moSysSession.CompanyID
            .SetOutParam iRetVal
            
            .ExecuteSP "spsoCreateSOPmntAppls"
            iRetVal = .GetOutParam(4)
            .ReleaseParams
        End With
        'truncate the table when finished
        sSQL = "IF OBJECT_ID('tempdb..#PendInvcs') IS NOT NULL " & _
        "BEGIN TRUNCATE TABLE #PendInvcs END "
        
        moAppDB.ExecuteSQL sSQL




'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cmdApply_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Sub

Private Sub ProcessCCReport() 'ccp As clsProcessCCUtils)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++


    moCCProcess.RptSingleShipment = True
    moCCProcess.RunCCReport moClass.moFramework, moClass.moSysSession, moClass.moAppDB




'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ProcessCCReport", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub






Private Sub cmdClearMsg_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdClearMsg, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++

    ClearMessages
   

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdComponents_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub ClearMessages()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    moProcessCommitTran.ClearMessages mlSessionID, moAppDB
    moDMMessageGrid.Where = sBuildMessageWhere
    moDMMessageGrid.Refresh
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ClearMessages", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub cmdComponents_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdComponents, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'********************************************************************
' Description:
'    This routine calls LoadCompForm to load the Component form
'    for a shipline that has a BTOKit
'********************************************************************
    
    On Error GoTo ExpectedErrorRoutine
    Dim lRow            As Long
    Dim lWhseKey        As Long
    
    SetMouse
   
    lRow = glGridGetActiveRow(grdShipment)
    lWhseKey = glGetValidLong(gsGridReadCell(grdShipment, lRow, kColLineShipWhseKey))
    
    'Save change in the row first
    moDMShipLineGrid.SetRowDirty lRow
    moDMShipLineGrid.Save
    
    'Initialize the Distribution object if not already initialized
    CreateDistForm lWhseKey
    
    If Not moItemDist Is Nothing Then
        If Not bLoadCompForm Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgCMUnexpectedSPError, "Load Component Form"
            ResetMouse
            Exit Sub
        End If
    End If
    
    ResetMouse
    Exit Sub
    
ExpectedErrorRoutine:
    ResetMouse
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdComponents_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub cmdCreatePick_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdCreatePick, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++

    On Error GoTo ExpectedErrorRoutine

Dim lLineNbrSelected As Long
Dim lSessionID As Long
Dim sString As String
Dim sSQL As String
Dim rs As Object

    If miPickListDelMth = 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kSODeliveryMethodNeeded
        Exit Sub
    End If
    
    SetMouse
    
    'Call the moSelectPick to create the pick list
    'and then generate the shipment
    If Not moSelectPick Is Nothing Then
        If CheckIsLinesAvailToPick = 0 Then
            'No pick line to process
            ResetMouse
            Exit Sub
        Else
            'Check whether the there is any candidate lines for create ship line
            lLineNbrSelected = glGetValidLong(moClass.moAppDB.Lookup("Count(*)", "#tsoCreatePickWrk2", "LineHold = 0 AND OrderHold = 0 AND CrHold = 0"))

            If lLineNbrSelected > 0 Then
            'Yes, there is cadidate lines
                'Update Status bar
                sString = gsBuildString(kSOCreatingPickList, moAppDB, moClass.moSysSession)
                sbrMain.Message = sString & "..."
                
                'Call the create pick interface to create the pick list
                lSessionID = moSelectPick.CreateShipLines(msCompanyID, 0, 0, _
                    0, 1, 0, "")
                    
                'Save the logical lock key
                mlLogicalLockKey = moSelectPick.lLogicalLockKey
                   
                'Reset the status bar
                sbrMain.Message = ""
                    
                'Evaluate the lSessionID
                Select Case lSessionID
                    Case -1
                        'Inform user if there is error in the pick process
                        giSotaMsgBox Me, moClass.moSysSession, kmsgErrorReleaseToShip
                        ResetMouse
                        Exit Sub
                        
                    Case 0
                        'Inform user if there is no pick line selected
                        giSotaMsgBox Me, moClass.moSysSession, kmsgNoLinePicked
                        ResetMouse
                        Exit Sub
                        
                    Case Is > 0
                        'Shiplines have been created
                        mlPickListKey = moSelectPick.lPickListKey
                End Select
               
                'Now we need to generate the shipment for the selected ship lines
                'Update the status bar
                sString = gsBuildString(kSOGeneratingShip, moAppDB, moClass.moSysSession)
                sbrMain.Message = sString & "..."

                If Not moProcessPickList.bGenerateShipment(Me, miNbrDecPlaces, mlPickListKey, lSessionID) Then
                'Shipment has not been generated, issue message
                    'Message
                    giSotaMsgBox Me, moClass.moSysSession, kSOMsgGenShipFailed
                    sbrMain.Message = ""
                    msShipmentNo = ""
                    mlShipKey = 0
                    mlWhseKey = 0
                    msShipmentTranDate = ""
                    miShipmentCommitted = kShipmentStatusFailed
                Else
                    'Shipment has been generated successfully, retrieve the shipment info
                    'for display
                    sSQL = "SELECT TranID, ShipKey, WhseKey, TranDate FROM tsoPendShipment WITH (NOLOCK) WHERE ShipKey IN"
                    sSQL = sSQL & " (SELECT DISTINCT ShipKey FROM #tsoCreatePickWrk2) "
                
                    Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
                    With rs
                        If Not rs.IsEmpty Then
                            msShipmentNo = gsGetValidStr(rs.Field("TranID"))
                            mlShipKey = glGetValidLong(rs.Field("ShipKey"))
                            mlWhseKey = glGetValidLong(rs.Field("WhseKey"))
                            msShipmentTranDate = gsGetValidStr(rs.Field("TranDate"))
                        End If
                      .Close
                    End With
                    Set rs = Nothing
                    
                    'Update the miShipmentCommitted state flag and shipment transtatus
                    miShipmentCommitted = kShipmentStatusPending
                    miTranStatus = kShipTranStatusPending
                End If
                
                'Update the control values in the form
                mbNewCreatedPickList = True
                txtPickTicketNo.Text = moSelectPick.sPickListNo
                txtPickTicketNo.Enabled = False
                lblShipmentNo.Caption = msShipmentNo
                
                If bOverrideSecEvent("SOGENINV", moClass, False) Then
                    cmdApply.Enabled = True
                Else
                    cmdApply.Enabled = False
                End If
                
                Select Case miPickListDelMth
                    Case kDeliveryMeth_CounterSale
                        msPickListDelMth = msDelMth_CounterSale
                    
                    Case kDeliveryMeth_WillCall
                        msPickListDelMth = msDelMth_WillCall
                        
                    Case kDeliveryMeth_Ship
                        msPickListDelMth = msDelMth_ShippedOrder
                End Select
                
                lblPickType.Caption = msPickListDelMth & " " & msPickTicket
                
                'Pick List has been created, now load the data into UI
                moDMShipLineGrid.Refresh
                
                'Check whether there is any change need to apply to the toolbar
                ToggleControls
                
                'Disable the delivery method options and cmdCreatePick
                ToggleDeliveryMethOpt
                cmdCreatePick.Enabled = False
                
                miShipmentUpdateCtr = 0
            Else
                ResetMouse
                Exit Sub
            End If
        End If
    End If
    
    ResetMouse
    Exit Sub
    
ExpectedErrorRoutine:
    ResetMouse
       
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdCreatePick_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub



Private Sub cmdDelInvoice_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdDelInvoice, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    
    On Error GoTo ExpectedErrorRoutine
    
    Dim iRetVal As Integer
    Dim sShipmentNo As String
    Dim iRet As Integer
    Dim sPickListNo As String
    Dim lCurrBatchKey As Long

    Dim sTranID As String ' AvaTax integration - local vars for cmdDelInvoice

    'Check for security right
    If Not bOverrideSecEvent("SODELINV", moClass, True) Then Exit Sub
    
    'First, check whether the shipment is still in the tsoPendShipment table and hasn't been moved to the tsoShipment table.
    'Then check that the tsoPendShipment.BatchKey is still the same as the system batch key.
    'If either of these is not true, the shipment has been committed by another task (while we had this shipment up in our own task)
    'or is in the process of being committed by another task. If so, issue the error message
    '"This shipment has been committed by another process. Your requested update will not be performed."
    'No updates to the shipment will occur.

    If mlShipKey > 0 And (miTranStatus = kShipTranStatusPending Or miTranStatus = kShipTranStatusInvoiced) Then
        lCurrBatchKey = glGetValidLong(moClass.moAppDB.Lookup("BatchKey", "tsoPendShipment", "ShipKey = " & mlShipKey))
               
        If lCurrBatchKey <> mlHiddenBatchKey Then
            miShipmentCommitted = kShipmentStatusCommitted
          
            giSotaMsgBox Me, moClass.moSysSession, kmsgShipmentCommitted
            ToggleControls
            moDMShipLineGrid.Refresh
            
            'Since we reload the data, we need to set the dm to not dirty
            moDMShipLineGrid.SetDirty False, True
            Exit Sub
        End If
    End If
   
    If mlInvcKey = 0 And miMultiInvc = 0 Then
        Exit Sub
    End If
    
    If mbBusy Then
        Exit Sub
    End If
    
    ' find out if the user wants to delete the ship lines
    iRet = giSotaMsgBox(Nothing, moClass.moSysSession, kSOmsgConfirmDelInvcForShipmen)

    If iRet = kretNo Then
           Exit Sub
    End If
    
    SetMouse
    mbBusy = True
    
    ' No error, continue with invoice generation...
    sbrMain.Message = "Deleting Invoice..."
    
    'AvaTax Integration - cmdDelInvoiceClick
    sTranID = muInvoiceInfo.sInvcNo
    'AvaTax end

    ' Call delete invoice sp
    With moAppDB
        
        .SetInParam mlShipKey
        .SetInParam msCompanyID
        .SetOutParam iRetVal
        
        .ExecuteSP "spsoDelInvForShipment"

        ' Retrieve the return value
        iRetVal = giGetValidInt(.GetOutParam(3))
          
        .ReleaseParams
        
     End With
           
    ' Evaluate return value
    If iRetVal = 1 Then
        ' Invoice has been successfully deleted, refresh the form.
        
        ' Save the current pick list no
        sPickListNo = txtPickTicketNo.Text
        
        'Clear the form
        HandleCancelClick kTbCancel
        
        ' Reload the picklist
        txtPickTicketNo.Text = sPickListNo
        
        'Refresh the pick list due to possible change in the Edit Shipment task
        bIsValidPickListNo False

        'AvaTax Integration - cmdDelInvoiceClick
        If mbAvaTaxEnabled Then
            moAvaTax.CancelTax kModuleAR, sTranID, 501
        End If
        'AvaTax end

    Else
        ' Add error handling
        If (iRetVal = 2) Or (iRetVal = 3) Then
            ' Invoice has already been deleted by another process, action canceled and reload data
            giSotaMsgBox moClass, moClass.moSysSession, kmsgShipmentCommitted
            
            ' Save the current pick list no
            sPickListNo = txtPickTicketNo.Text
            
            'Clear the form
            HandleCancelClick kTbCancel
            
            ' Reload the picklist
            txtPickTicketNo.Text = sPickListNo
            
            'Refresh the pick list due to possible change in the Edit Shipment task
            bIsValidPickListNo False
        Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgShipmentCommitted
        End If
    End If
    
    sbrMain.Message = ""
    ResetMouse
    mbBusy = False
    
 '+++ VB/Rig Begin Pop +++
        Exit Sub
        
ExpectedErrorRoutine:
    sbrMain.Message = ""
    ResetMouse
    mbBusy = False
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdDelInvoice_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDist_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdDist, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'**********************************************************************
' Description:
'    This routine Loads the Distribution Form (imzde001)for user to
'    view or modify the distribution on a Shipline.
'
'**********************************************************************

On Error GoTo ExpectedErrorRoutine

Dim lRow            As Long
Dim lWhseKey        As Long
    
    'Get the active row
    lRow = glGridGetActiveRow(grdShipment)
    
    SetMouse
    
    'Initialize the Distribution object if not already initialized
    lWhseKey = glGetValidLong(gsGridReadCell(grdShipment, lRow, kColLineShipWhseKey))
    CreateDistForm lWhseKey
    
    If Not moItemDist Is Nothing Then
        If moProcessPickList.bIsValidQtyToPick(Me, moDMShipLineGrid, grdShipment, lRow) Then
        'If the QtyToPick is valid, load the distribution form
        'so use can enter new distribution or view and update exist distribution
                        
            'Lock the Create Pick Form
            frmProcessSO.Enabled = False
            If Not moProcessPickList.bCreateManualDistribution(Me, grdShipment, moDMShipLineGrid, lRow) Then
                'moCreatePick.UpdateGridLine grdShipment, moDMShipLineGrid, lRow
                grdShipment_Click kColLineQtyToPick, lRow
                grdShipment.SetFocus
            End If
                        
            'Unlock the Create Pick Form
            frmProcessSO.Enabled = True
        Else
        'QtyToPick is invalid, set the focus back the QtyToPick cell
            grdShipment_Click kColLineQtyToPick, lRow
            mbDenyAction = True
        End If
    End If
    
    'Highlight the row where the user has selected before click the Dist button
    If Me.ActiveControl Is grdShipment Then
        With grdShipment
            .Row = lRow
                SetRowValues lRow
                gGridSetSelectRow grdShipment, lRow
        End With
    End If
    ResetMouse
    Exit Sub
    
ExpectedErrorRoutine:
    ResetMouse
    
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "cmdDist_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++

End Sub

Private Sub cmdEditShipment_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdEditShipment, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++

    Dim oShipment As Object
    Dim iRetVal As Integer
    
    If mlPickListKey > 0 Then
        
        iRetVal = iConfirmUnload(True)
        
        Select Case iRetVal
            Case Is = kDmSuccess, kDmNotAllowed
                'The Data Manager operation was successful or not allowed
                'Continue
                
            Case Is = kDmFailure, kDmError
                ResetMouse
                Exit Sub
            
            Case Else
                'Error processing
                'Give an error message to the user
                'Unexpected Confirm Unload Return Value: {0}
                ResetMouse
                giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedConfirmUnloadRV, _
                             CVar(iConfirmUnload)
                
                Exit Sub
        End Select

    End If

    Set oShipment = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
        kclsSOEditShipDLL, ktskSOEditShipDLL, kAOFRunFlags, kContextAOF)
        
    If Not oShipment Is Nothing Then

        oShipment.EditShipments mlShipKey
    End If
        
    If Not oShipment Is Nothing Then
        Set oShipment = Nothing
    End If
    
    'Refresh the pick list due to possible change in the Edit Shipment task
    'Set the focus to the Message Tab
    mbDontProcessTabClick = True
    TabShipLine.Tab = kTabShipLine
    
    'Display the messages
    If Not moProcessCommitTran Is Nothing Then
        moProcessCommitTran.ClearMessages mlSessionID, moAppDB
        moDMMessageGrid.Where = sBuildMessageWhere
        moDMMessageGrid.Refresh
    End If
    
    bIsValidPickListNo False
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdEditShipment_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdMRP_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim sInsert As String
    Dim oDrillDown As Object
    Dim bRetCode As Integer
    Dim sCutOffDate As String
    Dim sStartDate As String
    Dim sFenceDate As String
    Dim iNumDays As Integer
    Dim iGenLower As Integer
    Dim lScheduleKey As Long
    Dim bStandAlone As Boolean
    Dim iRetVal As Integer
    
    'If currently there is a pick list loaded, check whether it
    'is a newly create pick list, if it is, we need to reverse the
    'PickingComplete flag for the ship lines before we leave
    If mlPickListKey > 0 Then
        
        iRetVal = iConfirmUnload(True)
        
        Select Case iRetVal
            Case Is = kDmSuccess, kDmNotAllowed
                'The Data Manager operation was successful or not allowed
                'Continue
                
            Case Is = kDmFailure, kDmError
                ResetMouse
                Exit Sub
            
            Case Else
                'Error processing
                'Give an error message to the user
                'Unexpected Confirm Unload Return Value: {0}
                ResetMouse
                giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedConfirmUnloadRV, _
                             CVar(iConfirmUnload)
                
                Exit Sub
        End Select

    End If

    
    If Not mbMRActivated Then       'module actviation
        Exit Sub
    End If
    
    sInsert = "INSERT INTO #tmfNetReqHeadWrk (ItemKey, WhseKey, SessionId)" _
            & " SELECT DISTINCT timInventory.ItemKey,timInventory.WhseKey, " & Str(mlSOKey) _
            & " From timInventory, timItem" _
            & " WHERE timItem.CompanyID = " & gsQuoted(msCompanyID) & " AND timInventory.ItemKey = timItem.ItemKey " _
            & " AND timInventory.ItemKey IN" _
            & " (SELECT sol.ItemKey" _
            & " FROM    tsoSalesOrder soh, tsoSOLine sol, tsoSOLineDist sold" _
            & " WHERE   soh.SOKey = " & (mlSOKey) _
            & " AND     sol.SOKey = soh.SOKey" _
            & " AND     sold.SOLineKey = sol.SOLineKey" _
            & " AND     sol.Status = 1" _
            & " AND     timInventory.WhseKey = sold.WhseKey)"
        
    sCutOffDate = Format(Date + 180, "YYYY-MM-DD")
    sStartDate = Format(Date, "YYYY-MM-DD")
    sFenceDate = Format(Date, "YYYY-MM-DD")
    iNumDays = 30
    iGenLower = 0
    lScheduleKey = 0
    
    Set oDrillDown = goGetSOTAChild(moClass.moFramework, mfrmMain.moSotaObjects, _
                     kclsNetReqInqDD, ktskMFMRPPlanningDD, _
                     kDDRunFlags, kContextDD)

    If oDrillDown Is Nothing Then Exit Sub

    bStandAlone = True
    bRetCode = oDrillDown.bNetReqInq(sInsert, "", sCutOffDate, sStartDate, 30, iNumDays, _
                                     bStandAlone, mlSOKey, 0, 0, 0, 0, 0, 0, iGenLower, 0, _
                                     sFenceDate, lScheduleKey)
    Set oDrillDown = Nothing
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdMRP_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub cmdCCPmnt_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdCCPmnt, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
Dim bReadOnly As Boolean
Dim dPrePmtsNC As Double
Dim dPendAmtInvcd As Double
    
    If moCCPmnts Is Nothing Then
        Set moCCPmnts = New clsARSOPmnts
        moCCPmnts.Hidden = True
        moCCPmnts.Init moClass.moAppDB, moClass.moSysSession, moClass.moFramework, mbUseMultiCurr, miHomePriceDecPlaces, miHomeRoundMeth, miHomeRoundPrecision, miUseNationalAccts
    End If

    If bSalesOrderClosed Then
        bReadOnly = True
    Else
        bReadOnly = mbSOPmntReadOnly
    End If

    dPrePmtsNC = moCCPmnts.dCalculatePrePaymentsNC(mlSOKey)
    dPendAmtInvcd = dCalculatePendAmtInvcd(moClass, mdExchangeRate, miHomePriceDecPlaces, mlSOKey)
     
    moCCPmnts.DoPmnts bReadOnly, mlSOKey, msTranID, mdExchangeRate, mlCurrExchSchedKey, mlCustExchSchedKey, msDocCurrID, _
        mlCustKey, mlBillToAddrKey, mlParentCustKey, miBillToParent, miBillingType, mbDfltPmtByParent, mdTranAmtHC, _
        dPrePmtsNC, mdTranAmt, mdFrtAmt, mdSTaxAmt, msPurchOrderNo, mdAmtInvcd, dPendAmtInvcd, msOrderDate, True
              
    moCCPmnts.UnloadMe
    
    Set moCCPmnts = Nothing
End Sub

Private Sub cmdGenInvoice_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdGenInvoice, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    
       
    GenerateInvoice
   
    
 '+++ VB/Rig Begin Pop +++
        Exit Sub
        
ExpectedErrorRoutine:

    sbrMain.Message = ""
    ResetMouse
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdGenInvoice_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub GenerateInvoice()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    On Error GoTo ExpectedErrorRoutine
    
    Dim iRetVal As Integer
    Dim lInvcKey As Long
    Dim sShipmentNo As String
    Dim dSalesAmt As Double
    Dim dFrtAmt As Double
    Dim dTradeDiscAmt As Double
    Dim dTradeDiscPct As Double
    Dim dSTaxAmt As Double
    Dim dTranAmt As Double
    Dim dTranAmtHC As Double
    Dim lSessionID As Long
    Dim iRet As Integer
    Dim iValidationOnly As Integer
    Dim iIgnoreWarnings As Integer
    Dim lWarningCount As Long
    Dim lErrorCount As Long
    Dim lCurrBatchKey As Long

     'Check for security right
    If Not bOverrideSecEvent("SOGENINV", moClass, True) Then Exit Sub
    
    'First, check whether the shipment is still in the tsoPendShipment table and hasn't been moved to the tsoShipment table.
    'Then check that the tsoPendShipment.BatchKey is still the same as the system batch key.
    'If either of these is not true, the shipment has been committed by another task (while we had this shipment up in our own task)
    'or is in the process of being committed by another task. If so, issue the error message
    '"This shipment has been committed by another process. Your requested update will not be performed."
    'No updates to the shipment will occur.

    If mlShipKey > 0 And (miTranStatus = kShipTranStatusPending Or miTranStatus = kShipTranStatusInvoiced) Then
        lCurrBatchKey = glGetValidLong(moClass.moAppDB.Lookup("BatchKey", "tsoPendShipment", "ShipKey = " & mlShipKey))
               
        If lCurrBatchKey <> mlHiddenBatchKey Then
         
            giSotaMsgBox Me, moClass.moSysSession, kmsgShipmentCommitted
            ToggleControls
            moDMShipLineGrid.Refresh
            
            'Since we reload the data, we need to set the dm to not dirty
            moDMShipLineGrid.SetDirty False, True
            Exit Sub
        End If
    End If
   
    If mlInvcKey > 0 Or miMultiInvc > 0 Then
        Exit Sub
    End If
    
    If mbBusy Then
        Exit Sub
    End If
    
    If moProcessCommitTran Is Nothing Then Exit Sub
    
    ' set busy flag
    mbBusy = True
    SetMouse
    
    'Execute the save logic to save any un-saved changes
    If Not bHandleSaveClick Then
        'Clear the status bar
        sbrMain.Message = ""
        ResetMouse
        mbBusy = False
        Exit Sub
    End If
    
    ClearMessages
    
    'Execute the commit transaction logic
    If bPopTempTableTransToCommit Then
    
        iValidationOnly = kYes
        iIgnoreWarnings = kNo
         
        If moProcessCommitTran.CommitSingleTransaction(Me, moAppDB, iValidationOnly, iIgnoreWarnings, lWarningCount, lErrorCount, mlSessionID) Then
           'Check whether message needed to be displayed
           mlSessionID = moProcessCommitTran.SessionID
           If moProcessCommitTran.ShowMessage And (moProcessCommitTran.ErrorCount > 0 Or moProcessCommitTran.WarningCount > 0) Then
           
                'Populate the ErrorCmnt in tciErrorLog
                If moProcessCommitTran.ProcessErrorLog(Me, moAppDB, mlSessionID, mlLanguage, msCompanyID) Then
                
                    moDMMessageGrid.Where = sBuildMessageWhere
                    moDMMessageGrid.Refresh
                End If
           
                If moProcessCommitTran.ErrorCount > 0 Then
                    iRet = giSotaMsgBox(Me, moClass.moSysSession, kSOmsgGenInvFailValidation)
                     ' if yes show the messages and dont clear the form
                    If iRet = vbYes Then
                        
                            'Set the focus to the Message Tab
                            mbDontProcessTabClick = True
                            TabShipLine.Tab = kTabMessages
                            
                            ResetMouse
                            
                     End If
                     mbBusy = False
                     Exit Sub
                Else
                    'Only warning exists
                    'Display message 'X warnings were generated.  Do you still want to generate the invoice?'
                    iRet = giSotaMsgBox(Me, moClass.moSysSession, kSOmsgGenInvoiceProceedConfirm, moProcessCommitTran.WarningCount)
                        
                    ' If user selected no, exit generate invoice process and display the warnings
                    If iRet = vbNo Then
                        'Set the focus to the Message Tab
                        mbDontProcessTabClick = True
                        TabShipLine.Tab = kTabMessages
                        
                        ResetMouse
                        mbBusy = False
                        Exit Sub
                    End If
                End If
             End If
        Else
            ResetMouse
            mbBusy = False
            Exit Sub
        End If
    Else
        ResetMouse
        mbBusy = False
        Exit Sub
    End If
    
    ' No error, continue with invoice generation...
    sbrMain.Message = "Generating Invoice..."
    
    With moAppDB
        
        'if processing sales order shipment, these fields will contain info, else null
        .SetInParam mlShipKey
        .SetInParam 1 ' Create Invoice
        .SetOutParam mlInvcKey
        .SetOutParam dSalesAmt
        .SetOutParam dFrtAmt
        .SetOutParam dTradeDiscAmt
        .SetOutParam dTradeDiscPct
        .SetOutParam dSTaxAmt
        .SetOutParam dTranAmt
        .SetOutParam dTranAmtHC
        .SetOutParam lSessionID
        .SetOutParam miMultiInvc
        .SetOutParam iRetVal
        
        .ExecuteSP "spsoGenInvcForShipment"

        ' Retrieve the return value
        lInvcKey = glGetValidLong(.GetOutParam(3))
        dSalesAmt = gdGetValidDbl(.GetOutParam(4))
        dFrtAmt = gdGetValidDbl(.GetOutParam(5))
        dTradeDiscAmt = gdGetValidDbl(.GetOutParam(6))
        dTradeDiscPct = gdGetValidDbl(.GetOutParam(7))
        dSTaxAmt = gdGetValidDbl(.GetOutParam(8))
        dTranAmt = gdGetValidDbl(.GetOutParam(9))
        dTranAmtHC = gdGetValidDbl(.GetOutParam(10))
        lSessionID = glGetValidLong(.GetOutParam(11))
        miMultiInvc = giGetValidInt(.GetOutParam(12))
        iRetVal = giGetValidInt(.GetOutParam(13))
          
        .ReleaseParams
        
     End With
           
    ' Evaluate return value
    If iRetVal = 1 And lInvcKey > 0 Then
        ' Invoice has been successfully generated, refresh the form.
        
        
        'AvaTax Integration - GenerateInvoice
        If mbAvaTaxEnabled Then
            moAvaTax.bCalculateTax lInvcKey, kModuleAR
        End If
        'AvaTax end

        'Refresh the pick list due to possible change in the Edit Shipment task
        bIsValidPickListNo False
       
    Else
        ' Invoice has already been generated by another process, action canceled and reload data
        giSotaMsgBox moClass, moClass.moSysSession, kmsgShipmentCommitted
        
        'Refresh the pick list due to possible change in the Edit Shipment task
        bIsValidPickListNo False
    End If
    
        
    'Reset the status in status bar
    sbrMain.Message = ""
    sbrMain.Status = SOTA_SB_START
    ResetMouse
    mbBusy = False
    
 '+++ VB/Rig Begin Pop +++
        Exit Sub
        
ExpectedErrorRoutine:

    sbrMain.Message = ""
    ResetMouse
    mbBusy = False
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdGenInvoice_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub cmdInvcTotals_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdInvcTotals, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    
    On Error GoTo ExpectedErrorRoutine
    
    'Check for security right
    If Not bOverrideSecEvent("SOVIEWINV", moClass, True) Then Exit Sub

    If mlInvcKey = 0 Or miMultiInvc = 1 Then
        Exit Sub
    End If
    
    frmInvoiceTotal.Init moClass, moSotaObjects
    
    'Get the latest invoice and payment information
    RetrieveInvcInfo

    frmInvoiceTotal.ShowForm

'+++ VB/Rig Begin Pop +++
        Exit Sub
        
ExpectedErrorRoutine:

    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdInvcTotal_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub cmdPmnt_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdPmnt, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
Dim bReadOnly As Boolean
Dim dPrePmtsNC As Double
Dim dPendAmtInvcd As Double

    If moARSOPmnts Is Nothing Then
        Set moARSOPmnts = New clsARSOPmnts
        moARSOPmnts.Init moClass.moAppDB, moClass.moSysSession, moClass.moFramework, mbUseMultiCurr, miHomePriceDecPlaces, miHomeRoundMeth, miHomeRoundPrecision, miUseNationalAccts
    End If

    If bSalesOrderClosed Then
        bReadOnly = True
    Else
        bReadOnly = mbSOPmntReadOnly
    End If

    dPrePmtsNC = moARSOPmnts.dCalculatePrePaymentsNC(mlSOKey)
    dPendAmtInvcd = dCalculatePendAmtInvcd(moClass, mdExchangeRate, miHomePriceDecPlaces, mlSOKey)
     
   moARSOPmnts.DoPmnts bReadOnly, mlSOKey, msTranID, mdExchangeRate, mlCurrExchSchedKey, mlCustExchSchedKey, msDocCurrID, _
        mlCustKey, mlBillToAddrKey, mlParentCustKey, miBillToParent, miBillingType, mbDfltPmtByParent, mdTranAmtHC, _
        dPrePmtsNC, mdTranAmt, mdFrtAmt, mdSTaxAmt, msPurchOrderNo, mdAmtInvcd, dPendAmtInvcd, msOrderDate, False
        

        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cmdPmnt_Click", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

End Sub

Private Sub cmdPrintMsg_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdPrintMsg, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
   
    Dim bCancelPrint As Boolean
    Dim sPrintButton As String
   
    If ddnOutput.ListIndex = 1 Then
        sPrintButton = kTbPrint
    Else
        sPrintButton = kTbPreview
    End If

    bCancelPrint = False
    PrintErrorLog sPrintButton, mlSessionID, bCancelPrint
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cmdPrintMsg_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

End Sub

Private Sub cmdPrintInvoice_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdPrintInvoice, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++

    On Error GoTo ExpectedErrorRoutine
    
    If mlShipKey > 0 Then
        SetMouse
        sbrMain.Message = gsBuildString(kSOPrintingInvoice, moAppDB, moClass.moSysSession) & "..."
        If Not moProcessCommitTran.PrintInvoice(Me, mlShipKey, kInvoicePrintNotReqd) Then
            'Message
        End If
    End If
    
    sbrMain.Message = " "
    ResetMouse
    Exit Sub
    
ExpectedErrorRoutine:
    ResetMouse
        
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "cmdPrintInvoice_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbCancelShutDown = False
    mbLoadSuccess = False

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True.
'   Parameters:
'           KeyCode, Shift - see VB reference Manual.
'
'   NOTE: THIS ROUTINE SHOULD CALL A GLOBAL SUBROUTINE TO HANDLE ALL THE
'         AVAILABLE HOTKEYS.
'********************************************************************
    'Determine if the user used the form-level keys and process them
    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            'Process the form-level key pressed
            gProcessFKeys Me, KeyCode, Shift
    End Select
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

'************************************************************************
'   Description:
'       Process Key Presses on the form.  NOTE: Key Preview of the form
'       should be set to True.
'   Param:
'       KeyAscii -  ASCII Key Code of Key Pressed.
'   Returns:
'************************************************************************
Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        
        Case Else
            'Other KeyAscii routines.
    End Select
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'******************************************
'* Set up the form.
'******************************************


    Dim uCurrInfo As CurrencyInfo

    moClass.lUIActive = kChildObjectInactive
    
    'Set the form's initial height and width.
    miOldFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormHeight = miOldFormHeight
    miMinFormWidth = miOldFormWidth

    'Set the Status Bar object.
    Set sbrMain.Framework = moClass.moFramework
    Set moAppDB = moClass.moAppDB
    sbrMain.Status = SOTA_SB_START
    
    'Set the browse control to 'Unfiltered'.
    miFilter = RSID_UNFILTERED
    
    'Get session defaults.
    With moClass.moSysSession
        mlLanguage = .Language           'Language ID
        msCompanyID = .CompanyID         'Company ID
        mbEnterAsTab = .EnterAsTab       'Enter Key like Tab Key
        msBusinessDate = .BusinessDate   'Current Business Date
        msCompanyID = .CompanyID
        msUserID = .UserId
        
        mbIMActivated = .IsModuleActivated(kModuleIM)
        
        mbCCActivated = .IsModuleActivated(kModuleCC)
        
    End With
    
    'Get whatever Localized Strings are needed.
    If Not bGetLocalStrings() Then
        'Error processing
        mbLoadSuccess = False

        'An error occurred while retrieving text for messages used in this function.
        ResetMouse
        giSotaMsgBox Me, moClass.moSysSession, kmsgGetLocalStringsError

        Exit Sub
    End If

    'Create the temp table bound to DM.
    If bCreatePickShipLineTempTbl() = False Then
        'Give an error message to the user.
        'An error occurred attempting to create the temporary table.  {0}: {1}
        ResetMouse
        giSotaMsgBox Me, moClass.moSysSession, kmsgErrTmpTblCreate, _
                                               CVar("Name"), CVar("#tsoPickShipLine")

        mbLoadSuccess = False
        Exit Sub
    End If

    BindToolBar         'Bind the Toolbar.
    BindForm            'Bind the Controls to the Database
    
    'Set the application's security level.
    miSecurityLevel = giSetAppSecurity(moClass, tbrMain)
    
    BindGM              'Bind the Grid Manager.
    BindContextMenu     'Bind the Context Menu.
    
    If Not bInitCreatePickObjs Then
        'Error processing
        mbLoadSuccess = False
        Exit Sub
    End If
    
    'Get Module Options
    GetModuleOptions

    'Set up the Ship Lines Grid.
    FormatGrid
    
    'Load up Severity and output combo for Message Displaying
    SetupMsgCombos
    LoadMsgCboDflts
   
    'Setup the currency control
    gbSetCurrCtls moClass, moClass.moSysSession.CurrencyID, uCurrInfo, curInvoiceTotal
    
    'SetupReport
    SetupReport
    
    'Default to Ship Line Tab
    TabShipLine.Tab = kTabShipLine
    
    'Set the mbCellChangeFired to true so if the user clicks the green door immediately
    'after loading the form, the FireGMCellChangeIfNeeded routine will not fire.
    mbCellChangeFired = True

    'AvaTax Integration - FormLoad
    mbTrackSTaxOnSales = gbGetValidBoolean(moClass.moAppDB.Lookup("TrackSTaxonSales", "tarOptions", "CompanyID = " & gsQuoted(msCompanyID)))
    If mbTrackSTaxOnSales Then
        CheckAvaTax
    Else
        mbAvaTaxEnabled = False
    End If
    'AvaTax end
    
    If mbCCActivated Then
        cmdCCPmnt.Enabled = True
    Else
        cmdCCPmnt.Enabled = False
    End If

    mbLoadSuccess = True
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Function bInitCreatePickObjs() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
On Error GoTo ExpectedErrorRoutine
    
    bInitCreatePickObjs = False

    'Create an instant of Create Pick Object
    If moCreatePickList Is Nothing Then
        'Create the Object
        Set moCreatePickList = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                            kclsCreatePickDLL, ktskCreatePickDLL, kAOFRunFlags, kContextNormal)
        
           
        If Err.Number = 0 And Not moCreatePickList Is Nothing Then
            'Pass the current DB connection to the object
            'moCreatePickList.oAppDB = moAppDB
            'Load the UI
            moCreatePickList.LoadUI mlRunMode
        Else
            GoTo ExpectedErrorRoutine
        End If
    End If
    
    'Create an instant of the Select Pick Object
    If moSelectPick Is Nothing Then
        'Create the Object
        Set moSelectPick = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                            kclsSelectOrdToPick, ktskSelectOrdToPick, kAOFRunFlags, kContextNormal)
        'Pass the current DB connection to the object
        If Err.Number = 0 And Not moSelectPick Is Nothing Then
            moSelectPick.oAppDB = moAppDB
        Else
            GoTo ExpectedErrorRoutine
        End If
    End If
    
    'Create an instant of clsProcessPickList
    If moProcessPickList Is Nothing Then
        Set moProcessPickList = New clsProcessPickList
        moProcessPickList.oAppDB = moAppDB
        moProcessPickList.sCompanyID = msCompanyID
    End If
    
    miEmptyBins = 0
    miEmptyRandomBins = 0
    
    'Create an instant of clsProcessCommitTran
    If moProcessCommitTran Is Nothing Then
        'Create the Object
        Set moProcessCommitTran = New clsSOCommitTransMgr
    End If
    
    bInitCreatePickObjs = True
    
    Exit Function
    
ExpectedErrorRoutine:

    If Not moCreatePickList Is Nothing Then
        Set moCreatePickList = Nothing
    End If
    
    If Not moSelectPick Is Nothing Then
        Set moSelectPick = Nothing
    End If
    
    If Not moProcessPickList Is Nothing Then
        Set moProcessPickList = Nothing
    End If
    
    If Not moProcessCommitTran Is Nothing Then
        Set moProcessCommitTran = Nothing
    End If

    Exit Function
'+++ VB/Rig Begin Pop +++

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bInitCreatePickObjs", VBRIG_IS_FORM
        Call giErrorHandler: Exit Function
'+++ VB/Rig End +++
End Function

'************************************************************************
'   Description:
'       Create the toolbar.
'   Param:
'       <none>
'   Returns:
'************************************************************************
Private Sub BindToolBar()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sRenumber As String

    'Setup Toolbar - Transaction.
    tbrMain.Init sotaTB_AOF, moClass.moSysSession.Language

    With moClass.moFramework
        tbrMain.SecurityLevel = .GetTaskPermission(.GetTaskID())
    End With

    With tbrMain
        .Style = sotaTB_NO_STYLE
        .LocaleID = mlLanguage
        .AddButton kTbFinishExit
        .AddButton kTbFinish
        .AddButton kTbCommit
        .AddButton kTbCancel
        .AddButton kTbSave
        .AddButton kTbDelete
        .AddSeparator
        .AddButton kTbPrint
        .AddButton kTbPreview
        .AddSeparator
        If (moClass.moSysSession.ShowOfficeOnToolBar = 1) Then
            .AddButton kTbOffice
            .AddSeparator
            .AddButton kTbHelp
        Else
            .AddButton kTbHelp
        End If
        .AddSeparator
        .AddButton kTbCustomizer
    End With

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindToolbar", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub grdShipment_EditChange(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    'User changed the value on the cell.  Set the mbCellChangeFired to false.  It will be set to true
    'once the event fires.  However, if it doesn't fire, we can use this flag to tell if we have to
    'call the GM CellChange manually.
    mbCellChangeFired = False

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdShipment_EditChange", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Sub

Private Sub grdShipment_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    moGMShipLine.Grid_EditMode Col, Row, Mode, ChangeMade

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdShipment_EditMode", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdShipment_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdShipment_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdShipment_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    moGMShipLine.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdShipment_TopLeftChange", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub



Private Sub moDMMessageGrid_DMGridRowLoaded(lRow As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
     Dim sData  As String
     Dim sCellText As String
    
    ' Load Severity
     sCellText = gsGetValidStr(gsGridReadCellText(grdMessages, lRow, kColMsgSeverity))
     sData = moClass.moAppDB.DASGetStaticCombo("tciErrorLog", "Severity", _
             mlLanguage, sCellText & ",")
     sData = Right(sData, 90): sData = Mid(sData, 1, InStr(1, sData, Chr(0), vbTextCompare))
     gGridUpdateCellText grdMessages, lRow, kColMsgSeverity, sData
     
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDMMessageGrid_DMGridRowLoaded", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++

End Sub


Private Sub moDMShipLineGrid_DMGridRowLoaded(lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    On Error GoTo ExpectedErrorRoutine
    
Dim iAllowDecimalQty As Integer
Dim lInvtTranKey As Long
Dim lSubItemKey As Long
Dim lItemKey As Long
Dim lShipLineKey As Long

    SetMouse
    
    'sSQLDate = Left(gsGridReadCellText(grdshipment, lRow, kColLineShipDate), 10) 'Remove time
    'gGridUpdateCellText grdshipment, lRow, kColLineShipDate, Format(sSQLDate, "SHORT DATE") 'Is Localized
    
    iAllowDecimalQty = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineAllowDecimalQty))
    
    moProcessPickList.SetGridNumericAttr grdShipment, kColLineQtyOrdered, miNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow
    moProcessPickList.SetGridNumericAttr grdShipment, kColLineQtyPicked, miNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow
    moProcessPickList.SetGridNumericAttr grdShipment, kColLineQtyAvail, miNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow
    
    SetRowValues lRow, True
    
'    'Get the bin info from the saved distributions.
'    lInvtTranKey = glGetValidLong(gsGridReadCell(grdShipment, lRow, kColLineInvtTranKey))
'    If lInvtTranKey <> 0 Then
'        lSubItemKey = glGetValidLong(gsGridReadCell(grdShipment, lRow, kColLineSubItemKey))
'        If lSubItemKey <> 0 Then
'            lItemKey = lSubItemKey
'        Else
'            lItemKey = glGetValidLong(gsGridReadCell(grdShipment, lRow, kColLineItemKey))
'        End If
'        CreateDistForm lRow
'
'        lShipLineKey = glGetValidLong(gsGridReadCell(grdShipment, lRow, kColLineShipLineKey))
'
'        moProcessPickList.UpdateAutoDistBinCols grdShipment, lRow, lItemKey, lShipLineKey, lInvtTranKey
'    End If


    GetHoldValues
    


    
    
    ResetMouse
    Exit Sub

ExpectedErrorRoutine:
    ResetMouse

'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDMShipLineGrid_DMGridRowLoaded", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


'************************************************************************
'   Description:
'       Standard Shutdown Procedure.  Unload all child forms
'       and remove any objects created within this app.
'   Param:
'       <none>
'   Returns:
'************************************************************************
Public Sub PerformCleanShutDown()
    On Error GoTo ExpectedErrorRoutine
    
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If
    
    'Unload all forms loaded from this main form.
    gUnloadChildForms Me
         
    'Remove all child collections.
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    If Not moARSOPmnts Is Nothing Then
        moARSOPmnts.UnloadMe
        Set moARSOPmnts = Nothing
    End If

    If Not moDMShipLineGrid Is Nothing Then
        moDMShipLineGrid.UnloadSelf
        Set moDMShipLineGrid = Nothing
    End If
    
    If Not moDMMessageGrid Is Nothing Then
        moDMMessageGrid.UnloadSelf
        Set moDMMessageGrid = Nothing
    End If

    If Not moItemDist Is Nothing Then
        Set moItemDist = Nothing
    End If
    
    If Not moGridNavUOM Is Nothing Then
        Set moGridNavUOM = Nothing
    End If

    If Not moGridLkuAutoDistBin Is Nothing Then
        Set moGridLkuAutoDistBin = Nothing
    End If
    
    If Not moGMShipLine Is Nothing Then
        moGMShipLine.UnloadSelf
        Set moGMShipLine = Nothing
    End If
    
    If Not moGMMessage Is Nothing Then
        moGMMessage.UnloadSelf
        Set moGMMessage = Nothing
    End If
  
    If Not moContextMenu Is Nothing Then
        Set moContextMenu = Nothing
    End If
    
    If Not moModuleOptions Is Nothing Then
        Set moModuleOptions = Nothing
    End If
    
    If Not moSelectPick Is Nothing Then
        Set moSelectPick = Nothing
    End If
    
    If Not moProcessPickList Is Nothing Then
        Set moProcessPickList = Nothing
    End If
    
    If Not moCreatePickList Is Nothing Then
        Set moCreatePickList = Nothing
    End If
    
    If Not moProcessCommitTran Is Nothing Then
        Set moProcessCommitTran = Nothing
    End If
   
    If Not moAppDB Is Nothing Then
        Set moAppDB = Nothing
    End If
    
    If Not moReport Is Nothing Then
        Set moReport = Nothing
    End If
    
    If Not moPrinter Is Nothing Then
        Set moPrinter = Nothing
    End If
    
    If Not moDDData Is Nothing Then
        Set moDDData = Nothing
    End If
    
    If Not moCCProcess Is Nothing Then
        moCCProcess.CleanupObjects
        Set moCCProcess = Nothing
    End If
    
    
    
    ' AvaTax Integration - PerformCleanShutDown
    If Not moAvaTax Is Nothing Then
        Set moAvaTax = Nothing
    End If
    ' AvaTax end

    Exit Sub

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Me.WindowState = vbNormal Or Me.WindowState = vbMaximized Then
        'Resize Height
        gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, grdShipment, grdMessages
          
        'Resize Width
        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, grdShipment, grdMessages
               
        miOldFormHeight = Me.Height
        miOldFormWidth = Me.Width
        
   End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    If Not moClass Is Nothing Then
        Set moClass = Nothing
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub


'************************************************************************
'   Description:
'       If form is dirty, prompt for save.  Handle cancel of the
'       shutdown or process a normal shutdown.
'   Param:
'       Cancel -        set to True to cancel the form shutdown
'       UnloadMode -    flag indicating type of shutdown requested
'   Returns:
'************************************************************************
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If

    If UnloadMode <> vbFormCode Then
        HandleToolbarClick kTbCancelExit
                    
        Cancel = True
    Else
        PerformCleanShutDown
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++End Sub
End Sub

Private Function IsHoldDirty() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    IsHoldDirty = False
    
    If gsGetValidStr(chkHold.Tag) <> gsGetValidStr(chkHold.Value) Or gsGetValidStr(txtHoldReason.Tag) <> gsGetValidStr(txtHoldReason.Text) Then
        IsHoldDirty = True
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsHoldDirty", VBRIG_IS_FORM
        Call giErrorHandler: Exit Function
'+++ VB/Rig End +++End Sub
End Function



'************************************************************************
'   Description:
'       Returns Form Name for Debugging Information.
'   Param:
'       <none>
'   Returns:
'       Form Name
'************************************************************************
Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function

'************************************************************************
'   oClass contains the reference to the parent class object.  The form
'   needs this reference to use the public variables created within the
'   class object.
'************************************************************************
Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oClass = moClass
    
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moClass = oNewClass

'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property
Public Property Get oAppDB() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oAppDB = moAppDB
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oAppDB_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Set oAppDB(oNewAppDB As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moAppDB = oNewAppDB
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oAppDB_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Private Sub grdShipment_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'If mbForceValiationFired is ture that means  cell change event has been called by FireGMCellChangeIfNeeded routine already,
    'there is no need to call the cell change from here again.
    If mbForceValiationFired And gdGetValidDbl(gsGridReadCell(grdShipment, Row, kColLineQtyPicked)) > gdGetValidDbl(gsGridReadCell(grdShipment, Row, kColLineQtyOrdered)) Then
        mbForceValiationFired = False
        Exit Sub
    End If
    
    If (Col <> kColLineShipUOM) And (Col <> kColLineQtyPicked) And (Col <> kColLineSubItem) And (Col <> kColLineAutoDistBinID) And (Col <> kColLineQtyToPick) Then
        Exit Sub
    End If
    
    'moGMLine.Grid_Change Col, Row
    moGMShipLine_CellChange Row, Col

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdShipment_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdShipment_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    If Row = 0 And Col <> 0 Then
        moDMShipLineGrid.Save
    End If
    moGMShipLine.Grid_Click Col, Row
    
    If Row > 0 Then 'if event was not caused by a click in the heading area
        If Not bSetGridNavRestrict(grdShipment, navGridUOM, Row) Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgCMUnexpectedSPError, "Set up lookup control"
        End If
        SetRowValues Row
    End If
    
'    'If the user click on any locked cell, highlight the grid row the cell is in
'    With grdShipment
'        .Row = Row
'        .Col = Col
'        If .Lock = True Then
'            gGridSetSelectRow grdShipment, Row
'        End If
'    End With

    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdShipment_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdShipment_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    If mbDenyAction Then
        mbDenyAction = False
        Cancel = True
        Exit Sub
    End If

    Cancel = Not moGMShipLine.Grid_LeaveCell(Col, Row, NewCol, NewRow)
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdShipment_LeaveCell", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdShipment_LeaveRow(ByVal Row As Long, ByVal RowWasLast As Boolean, ByVal RowChanged As Boolean, ByVal AllCellsHaveData As Boolean, ByVal NewRow As Long, ByVal NewRowIsLast As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    moGMShipLine.Grid_LeaveRow Row, NewRow

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdShipment_LeaveRow", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Function bSetupNavMain(ByVal sCompanyID As String) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**********************************************************************
' Desc: Sets up the Main Navigator.
'**********************************************************************
    Dim sRestrictClause As String

    bSetupNavMain = False

    'Set the Main Navigator's restrict clause.
    'The list of pick tickets accessible to the user from this form are limited
    'to the simple pick tickets whose ship liness belong only to the sales order
    'currently displayed in the form.  The simple pick ticket containing a single
    'shipment for a single order.  The following pick tickets will be filter out here:
    '- pick ticket contains ship lines that belong to other orders
    '- pick ticket contains multiple shiments
    '- pick ticket where PickingComplete is false
    '- pick ticket contains ship lines that are not yet attached to a
    '  shipment header record
    '- pick ticket contains drop ship lines
    sRestrictClause = "PickListKey IN (SELECT PickListKey FROM"
    sRestrictClause = sRestrictClause & " (SELECT sl.PickListKey PickListKey,"
    sRestrictClause = sRestrictClause & "     COUNT(Distinct sol.SOKey) SOKeyCount,"
    sRestrictClause = sRestrictClause & "     COUNT(Distinct sl.ShipKey) ShipKeyCount,"
    sRestrictClause = sRestrictClause & "     COUNT(Distinct sold.ShipToAddrKey) ShipToAddrKeyCount,"
    sRestrictClause = sRestrictClause & "     COUNT(Distinct sold.WhseKey) WhseKeyCount,"
    sRestrictClause = sRestrictClause & "     COUNT(Distinct sold.ShipMethKey) ShipMethKeyCount"
    sRestrictClause = sRestrictClause & "   FROM    tsoShipLine sl WITH (NOLOCK)"
    sRestrictClause = sRestrictClause & "   JOIN    (SELECT Distinct pl.PickListKey PickListKey"
    sRestrictClause = sRestrictClause & "           FROM    tsoShipLine sl WITH (NOLOCK)"
    sRestrictClause = sRestrictClause & "           JOIN    tsoPickList pl WITH (NOLOCK)"
    sRestrictClause = sRestrictClause & "           ON  sl.PickListKey = pl.PickListKey"
    sRestrictClause = sRestrictClause & "           JOIN    tsoSOLine sol WITH (NOLOCK)"
    sRestrictClause = sRestrictClause & "           ON  sl.SOLineKey = sol.SOLineKey"
    sRestrictClause = sRestrictClause & "           WHERE sl.PickingComplete = 1"
    sRestrictClause = sRestrictClause & "           AND pl.DeliveryMeth <> " & kDeliveryMeth_DropShip
    sRestrictClause = sRestrictClause & "           AND sol.SOKey =" & mlSOKey & ") tmp1"
    sRestrictClause = sRestrictClause & "   ON  sl.PickListKey = tmp1.PickListKey"
    sRestrictClause = sRestrictClause & "   JOIN    tsoSOLine sol WITH (NOLOCK)"
    sRestrictClause = sRestrictClause & "   ON  sl.SOLineKey = sol.SOLineKey"
    sRestrictClause = sRestrictClause & "   JOIN    tsoSOLineDist sold WITH (NOLOCK)"
    sRestrictClause = sRestrictClause & "   ON  sol.SOLineKey = sold.SOLineKey"
    sRestrictClause = sRestrictClause & "   WHERE sl.ShipKey Is Not Null"
    sRestrictClause = sRestrictClause & "   GROUP BY sl.PickListKey) tmp2"
    sRestrictClause = sRestrictClause & " WHERE SOKeyCount = 1"
    sRestrictClause = sRestrictClause & " AND ShipKeyCount < = 1"
    sRestrictClause = sRestrictClause & " AND ShipToAddrKeyCount = 1"
    sRestrictClause = sRestrictClause & " AND ShipMethKeyCount < = 1"
    sRestrictClause = sRestrictClause & " AND WhseKeyCount < = 1)"
    
    'Have we initialized the 'Main' Navigator before?
    If Len(Trim$(navMain.Tag)) = 0 Then
        'No, we have NOT initialized the 'Main' Navigator before.
        If Not gbLookupInit(navMain, moClass, moAppDB, "SOPickList", sRestrictClause) Then
            bSetupNavMain = False
            Exit Function
        End If

        'Set the Navigator tag so it is not initialized again.
        navMain.Tag = "SOPickList"
    Else
        'Set the Restrict Clause appropriately.
        navMain.RestrictClause = sRestrictClause
    End If

    bSetupNavMain = True
    
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSetupNavMain", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function


Public Sub CMAddDrillMenu(oCtl As Object, wFlags As Long, lTaskID As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lCustKey As Long
    Dim lRow As Long
    Dim lRowCount As Long
    Dim lActiveRow As Long
    Dim lItemKey As Long
    Dim lRMAKey As Long
    Dim lShipmentCount As Long
    Dim lInvcCount As Long
    Dim lSODistCount As Long
    Dim lOrigShipLineKey As Long
    Dim lShipLineCount  As Long
    Dim lUOMKey As Long
    Dim sColumnList As String
    Dim sTableList As String
    Dim sWhereClause As String

'    If oCtl Is lkuCustID Then
'        If lTaskID = ktskARMaintNatlAccts Then
'            lCustKey = lkuCustID.KeyValue
'
'            If CheckNationalAcct(lCustKey) = False Then
'                wFlags = MF_GRAYED
'            End If
'        End If
'    End If
'
'   If oCtl Is grdShipment Then
'        grdShipment.Col = kColLineItemKey
'        lItemKey = glGetValidLong(grdShipment.Text)
'
'        Select Case lTaskID
'            Case Is = ktskIMInventoryMain
'                '117506238 Maintain Inventory
'                If (mlWhseKey = 0 Or lItemKey = 0) Then
'                    wFlags = MF_GRAYED
'                End If

'        End Select
'    End If
'
'    If oCtl Is lkuRMANo Then
'        lRMAKey = lkuRMANo.KeyValue
'
'        Select Case lTaskID
'            Case Is = ktskRMAEntry
'                '134611069  Enter RMAs
'                If lRMAKey = 0 Then
'                    wFlags = MF_GRAYED
'                End If
'
'        End Select
'    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CMAddDrillMenu", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub



Private Sub moDMShipLineGrid_DMPostSave(bValid As Boolean)

    SaveHold
End Sub

Private Sub moGMShipLine_CellChange(ByVal lRow As Long, ByVal lCol As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim lWhseKey As Long
    Dim iItemType       As Integer 'Declared the local variables to get data from grid.
    Dim dOrigQtyPicked  As Double
    Dim dNewQtyPicked   As Double
    Dim bShowMsg        As Boolean
    
    If (lCol <> kColLineShipUOM) And (lCol <> kColLineQtyPicked) And (lCol <> kColLineAutoDistBinID) And (lCol <> kColLineQtyToPick) Then
        Exit Sub
    End If
    
    lWhseKey = glGetValidLong(gsGridReadCell(grdShipment, lRow, kColLineShipWhseKey))
    'Fetch values from the grid columns.
    iItemType = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineItemType))
    dOrigQtyPicked = gdGetValidDbl(moDMShipLineGrid.GetColumnValue(lRow, "QtyPicked"))
    dNewQtyPicked = gdGetValidDbl(gsGridReadCell(grdShipment, lRow, kColLineQtyPicked))
    
    CreateDistForm lWhseKey
    
    If Not moItemDist Is Nothing Then
        If Not moProcessPickList.bProcessGridChange(Me, moDMShipLineGrid, grdShipment, lRow, lCol) Then
            ResetMouse
            grdShipment_Click lCol, lRow
            mbDenyAction = True
            Exit Sub
        End If
        'Added logic to check for itemtype as BTOKit if so then perform component distribution
        'by invoking the bLoadCompForm function.
        If iItemType = kvBTOKit And dOrigQtyPicked <> dNewQtyPicked Then
            bShowMsg = IIf(giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineCustReqShipComplete)) = 1 _
                        Or giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineItemReqShipComplete)) = 1, False, True)
           
            If bLoadCompForm(False, bShowMsg) Then
                frmPickLineCompItem.nbrKitQty.Tag = dOrigQtyPicked
                frmPickLineCompItem.nbrKitQty.Value = dNewQtyPicked
                On Error Resume Next
                frmPickLineCompItem.HandleToolbarClick (kTbFinishExit)
                On Error GoTo VBRigErrorRoutine
                If frmPickLineCompItem.nbrKitQty.Tag <> dNewQtyPicked Then
                    gGridUpdateCellText grdShipment, lRow, kColLineQtyPicked, CStr(dOrigQtyPicked)
                End If
            End If
        End If
    End If

    ResetMouse

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "moGMShipLine_CellChange", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub navMain_Click()
'************************************************************************************
'   Desc: The navControl_Click event checks for a valid value in the
'         associated entry.  If there isn't a valid value, then focus is
'         set back to itself.  Otherwise, it  calls the reinitialization
'         routine to handle initializing the form with the record's data.
'************************************************************************************
    On Error GoTo ExpectedErrorRoutine
    
    Dim bValidKey As Boolean
    Dim sPickTickNo As String
    Dim bEnabled As Boolean
    Dim iRetVal As Integer

    If Not gbGotFocus(Me, navMain) Then
        Exit Sub
    End If
    
    'If currently there is a pick list loaded, check whether it
    'is a newly create pick list, if it is, we need to reverse the
    'PickingComplete flag for the ship lines before we leave
    If mlPickListKey > 0 Then
        
        iRetVal = iConfirmUnload(True)
        
        Select Case iRetVal
            Case Is = kDmSuccess, kDmNotAllowed
                'The Data Manager operation was successful or not allowed
                'Continue
                
            Case Is = kDmFailure, kDmError
                ResetMouse
                Exit Sub
            
            Case Else
                'Error processing
                'Give an error message to the user
                'Unexpected Confirm Unload Return Value: {0}
                ResetMouse
                giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedConfirmUnloadRV, _
                             CVar(iConfirmUnload)
                
                Exit Sub
        End Select

    End If
    
    'Set up the Main Navigator button.
    If Not bSetupNavMain(msCompanyID) Then
        'Give an error message to the user.
        'An error occurred while setting up the properties
        'for one of the Navigator buttons on this form.
        ResetMouse
        giSotaMsgBox Me, moClass.moSysSession, kmsgNavigatorSetupError
        
        Exit Sub
    End If
    
    'Launch the navigator.
    'This will return the customer return number in the txtPickTicketNo control and
    'cause the txtPickTicketNo_LostFocus event to fire.
    bEnabled = txtPickTicketNo.Enabled

    gcLookupClick Me, navMain, txtPickTicketNo, "PickListNo"
    DoEvents
    Me.SetFocus
    If (navMain.ReturnColumnValues.Count <> 0) And (Not bEnabled) Then
        'Nav was clicked to select another return without clearing form of previous return
        sPickTickNo = navMain.ReturnColumnValues("PickListNo")
        If Len(sPickTickNo) = 0 Then
            Exit Sub
        Else
            'Clear out all the controls now.
            ClearForm
            'Put the returned value into the text box.
            txtPickTicketNo.Text = sPickTickNo
            txtPickTicketNo.Text = sPadWithZero(10, txtPickTicketNo)      'Pad Customer Return Number with Leading Zeros

            'There is no need to validate this value.
            bValidKey = bIsValidPickListNo(True)
            If bValidKey Then
                'Do a .KeyChange event now
                'VMIsValidKey
            End If
        End If
    End If
 
    Exit Sub
    
ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navMain_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub OptPickType_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick OptPickType(Index), True
    #End If
'+++ End Customizer Code Push +++
    
    Select Case Index
        Case kCounterSale
            miPickListDelMth = kDeliveryMeth_CounterSale
            'CheckIsLinesAvailToPick
        Case kWillCall
            miPickListDelMth = kDeliveryMeth_WillCall
            'CheckIsLinesAvailToPick
        Case kShippedOrder
            miPickListDelMth = kDeliveryMeth_Ship
            'CheckIsLinesAvailToPick
    End Select
    
    If Len(Trim$(txtPickTicketNo.Text)) = 0 Then
            CheckIsLinesAvailToPick
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "OptPickType_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub sbrMain_ButtonClick(sButton As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarClick sButton

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sbrMain_ButtonClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub TabShipLine_Click(PreviousTab As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If mbDontProcessTabClick Then
        mbDontProcessTabClick = False
        Exit Sub
    End If
    
    'If user click the Message tab, disable the Dist and Compoment button
    If PreviousTab = kTabShipLine Then
        cmdDist.Enabled = False
        cmdComponents.Enabled = False
    End If
    
    If TabShipLine.Tab = kTabMessages Then
        moDMMessageGrid.Where = sBuildMessageWhere
        moDMMessageGrid.Refresh
        'SendMessage grdMessages.hWnd, WM_SETREDRAW, True, 0
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "TabShipLine_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarClick Button

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub MoveToGridField(lOldCol As Long, lOldRow As Long, lNewCol As Long, lNewRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim bCancel As Boolean

    bSetFocus grdShipment
    gGridSetActiveCell grdShipment, lNewRow, lNewCol
    grdShipment_LeaveCell lOldCol, lOldRow, lNewCol, lNewRow, bCancel

    'This fixed a bug with Item Manager losing track of the correct item.
    grdShipment_Click lNewCol, lNewRow
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MoveToGridField", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

'************************************************************************
'   Description:
'       Process all toolbar clicks (as well as HotKey shortcuts to
'       toolbar buttons).
'   Param:
'       sKey -  Token returned from toolbar.  Indicates what function
'               is to be executed.
'   Returns:
'************************************************************************
Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'#If CUSTOMIZER Then
'    If Not moFormCust Is Nothing Then
'        If moFormCust.ToolbarClick(sKey) Then
'            Exit Sub
'        End If
'    End If
'#End If

    Dim iShipmentStatus As Integer
    Dim lCurrBatchKey As Long
    

   'Me.SetFocus
    DoEvents
    
    If mbBusy Then
        Exit Sub
    End If
    
    
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If

    mbDontChkClick = True       'Don't run click event on form clear.
    
    'First, check whether the shipment is still in the tsoPendShipment table and hasn't been moved to the tsoShipment table.
    'Then check that the tsoPendShipment.BatchKey is still the same as the system batch key.
    'If either of these is not true, the shipment has been committed by another task (while we had this shipment up in our own task)
    'or is in the process of being committed by another task. If so, issue the error message
    '"This shipment has been committed by another process. Your requested update will not be performed."
    'No updates to the shipment will occur.

    If mlShipKey > 0 And miShipmentCommitted <> kShipmentStatusCommitted Then
        lCurrBatchKey = glGetValidLong(moClass.moAppDB.Lookup("BatchKey", "tsoPendShipment", "ShipKey = " & mlShipKey))
               
        If lCurrBatchKey <> mlHiddenBatchKey Then
            miShipmentCommitted = kShipmentStatusCommitted
          
            If sKey = kTbSave Or sKey = kTbDelete Or sKey = kTbCommit Or sKey = kTbFinish Or sKey = kTbFinishExit _
                    Or sKey = kTbPrint Or sKey = kTbPreview Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgShipmentCommitted
                ToggleControls
                moDMShipLineGrid.Refresh
                
                'Since we reload the data, we need to set the dm to not dirty
                moDMShipLineGrid.SetDirty False, True
                Exit Sub
            End If
        End If
        

    End If
    
 
    
    Select Case sKey
    
        Case kTbPrint, kTbPreview
            'The Print or Preview button was pressed by the user.
        
            HandlePrintClick sKey
            
        Case kTbFinish, kTbFinishExit
            'The Finish button was pressed by the user.
           
            HandleFinishClick sKey
        
        Case kTbCancel, kTbCancelExit
            
            'The Cancel button was pressed by the user.
            HandleCancelClick sKey
            
        Case kTbCommit
            'The Commit button was pressed by the user.
            HandleCommitClick

        Case kTbDelete
            'The Delete button was pressed by the user.
            HandleDeleteClick

        Case kTbSave
            'The Save button was pressed by the user.
            'Save all changes to the current shipment being edited.
            If Not bHandleSaveClick Then
                Exit Sub
            End If
        
        Case kTbHelp
            'The Help button was pressed by the user.
            gDisplayFormLevelHelp Me

        Case kTbFirst, kTbPrevious, kTbLast, kTbNext
            'The Browse buttons were pressed by the user.
            
            HandleBrowseClick sKey
            
        Case kTbFilter
            Select Case miFilter
                Case Is = RSID_UNFILTERED
                    miFilter = RSID_FILTERED

                Case Is = RSID_FILTERED
                    miFilter = RSID_UNFILTERED
            End Select
        
        Case Else
            'Error processing
            'Give an error message to the user.
            'Unexpected Operation Value: {0}
            ResetMouse
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation, _
                         CVar(sKey)
    End Select

    ResetMouse

    mbDontChkClick = False
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub



Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

#If CUSTOMIZER Then
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")

        If Not moFormCust Is Nothing Then
            moFormCust.Initialize Me, goClass
            Set moFormCust.CustToolbarMgr = tbrMain
            'moFormCust.ApplyDataBindings moDmForm
            moFormCust.ApplyFormCust
        End If
    End If
#End If
  
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub


Private Function bCreatePickShipLineTempTbl() As Boolean
'*******************************************************************************
'   Desc:  This routine creates the #tsoCustRtrnShipLine temporary table.
'*******************************************************************************
    On Error GoTo ExpectedErrorRoutine

    Dim sSQL As String
    Dim oSOTmpTbls As New clsSOTempTable
    
    bCreatePickShipLineTempTbl = False
    
    'Create Pick List Detail Table
    sSQL = "IF OBJECT_ID ('tempdb..#tsoCreatePickWrk2') IS NOT NULL TRUNCATE TABLE #tsoCreatePickWrk2" & vbCrLf
    sSQL = sSQL & "ELSE BEGIN" & vbCrLf
    sSQL = sSQL & " SELECT * INTO #tsoCreatePickWrk2 FROM tsoCreatePickWrk WHERE 1 = 2 " & vbCrLf
    sSQL = sSQL & " CREATE NONCLUSTERED INDEX #tsoCreatePickWrk2_index1 ON #tsoCreatePickWrk2 (ShipWhseKey, ItemKey, TranNoRelChngOrd, LineNbr)" & vbCrLf
    sSQL = sSQL & " CREATE NONCLUSTERED INDEX #tsoCreatePickWrk2_index2 ON #tsoCreatePickWrk2 (TranType, ItemKey, LineKey, LineDistKey)" & vbCrLf
    sSQL = sSQL & " CREATE CLUSTERED INDEX #tsoCreatePickWrk2_CLUSTERED_index ON #tsoCreatePickWrk2 (ShipLineKey, ShipLineDistKey)" & vbCrLf
    sSQL = sSQL & " END" & vbCrLf
    
    moAppDB.ExecuteSQL sSQL
    
   
    If Not oSOTmpTbls Is Nothing Then
        oSOTmpTbls.gbCreateTmpTblSOCommit moAppDB
    End If
    
    Set oSOTmpTbls = Nothing

    bCreatePickShipLineTempTbl = True
    Exit Function

ExpectedErrorRoutine:
    If Not oSOTmpTbls Is Nothing Then
        Set oSOTmpTbls = Nothing
    End If

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCreatePickShipLineTempTbl", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Property Get MyApp() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set MyApp = App

'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MyApp", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Public Property Get MyForms() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set MyForms = Forms
    
'+++ VB/Rig Begin Pop +++
    Exit Property
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MyForms", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Property

Private Sub txtPickTicketNo_LostFocus()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtPickTicketNo, True
    #End If
'+++ End Customizer Code Push +++
'*************************************************************************
' Desc: Checks for values in txtPickTicketNo and then
'       calls Data Manager's reinitialization routine to
'       handle initializing the form with the record's data.
' Parameters: None.
'*************************************************************************
    On Error GoTo ExpectedErrorRoutine

    Dim sActiveControl As String

    sActiveControl = UCase$(Me.ActiveControl.Name)
    If (sActiveControl = "NAVMAIN" _
    Or sActiveControl = "TBRMAIN") Then
        Exit Sub
    End If
    
    'Is the PickListNo populated?
    If Len(Trim$(txtPickTicketNo.Text)) > 0 Then
        'Yes, the PickListNo is populated.
        'Process the entered PickListNo.
        If txtPickTicketNo.Enabled Then
            bIsValidPickListNo True
        End If
    End If
    
    Exit Sub
    
ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtPickTicketNo_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub


Private Sub SetRowValues(ByVal lRow As Long, Optional bDataRefresh As Boolean = False)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'**********************************************************************
' Description:
'    This routine formats the line grid rows based on the ItemType
'    UOM options, etc.
'
'**********************************************************************

Dim iAllowSubItem       As Integer
Dim iTrackMeth          As Integer
Dim iTrackQtyAtBin      As Integer
Dim lKitShipLineKey     As Long
Dim lSubItemKey         As Long
Dim lWhseKey            As Long
Dim lItemKey            As Long
Dim iItemType           As Integer
Dim iAllowDecimalQty    As Integer
Dim sSQL                As String
Dim iOrderChecked       As Integer
Dim iLineChecked        As Integer
Dim dDMQtyPicked        As Double
Dim dGridQtyPicked      As Double
Dim lOrderTranKey       As Long
Dim lOrderTranType      As Long
Dim lLineTranKey        As Long
Dim lLineTranType       As Long
Dim lRowCount           As Long
Dim iShipmentCommitStatus As Integer
Dim iExtShipmentExists As Integer
Dim iPacked             As Integer
Dim iWhseUseBin         As Integer

    iAllowSubItem = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineAllowSubItem))
    iTrackMeth = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineTrackMeth))
    iTrackQtyAtBin = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineTrackQtyAtBin))
    iWhseUseBin = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineWhseUseBin))
    lKitShipLineKey = glGetValidLong(gsGridReadCell(grdShipment, lRow, kColLineKitShipLineKey))
    lSubItemKey = glGetValidLong(gsGridReadCell(grdShipment, lRow, kColLineSubItemKey))
    lWhseKey = glGetValidLong(gsGridReadCell(grdShipment, lRow, kColLineShipWhseKey))
    lItemKey = glGetValidLong(gsGridReadCell(grdShipment, lRow, kColLineSubItemKey))
    iItemType = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineItemType))
    iAllowDecimalQty = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineAllowDecimalQty))
    iShipmentCommitStatus = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineShipmentCommitStatus))
    iExtShipmentExists = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineExtShipmentExists))
    iPacked = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLinePacked))
    iAllowDecimalQty = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineAllowDecimalQty))
    
    If iShipmentCommitStatus = 1 Or iExtShipmentExists = 1 Or iPacked = 1 Or miShipmentCommitted = kShipmentStatusCommitted Then
    'If Shipment has already been committed, lock the grid
        gGridLockRow grdShipment, lRow
        lkuAutoDistBin.Visible = False
        navGridUOM.Visible = False
    Else
        If lItemKey = 0 Then
            lItemKey = glGetValidLong(gsGridReadCell(grdShipment, lRow, kColLineItemKey))
        Else
            iTrackMeth = giGetValidInt(gsGridReadCell(grdShipment, lRow, kColLineTrackMeth))
        End If
        
        'UOM changes are not allowed for:
        'Serialized Items            Kit Component Items
        'Substituted Items           Items not allowing Deciaml Qtys
        If iAllowDecimalQty = 0 Or (iTrackMeth > 1) Or (lKitShipLineKey > 0) Or _
            (lSubItemKey > 0) Or (iItemType < 5) Or iItemType = IMS_BTO_KIT Then  'Or (iAllowDecimalQty = 0) Then
            gGridLockCell grdShipment, kColLineShipUOM, lRow
        Else
            gGridUnlockCell grdShipment, kColLineShipUOM, lRow
        End If

        'Enable the auto-dist bin column if it can auto distributed from the bin lookup.
        'Lock it first by default.
        gGridLockCell grdShipment, kColLineAutoDistBinID, lRow
        'Unlock it if the conditions are correct.
        If iItemType = IMS_FINISHED_GOOD Or iItemType = IMS_ASSEMBLED_KIT Or iItemType = IMS_RAW_MATERIAL Then
            If iTrackQtyAtBin = 1 And (iTrackMeth = kTM_None Or iTrackMeth = kTM_Lot) And mbIMIntegrated Then
                gGridUnlockCell grdShipment, kColLineAutoDistBinID, lRow
            End If
        End If

        'Setup the distribution button for item needed to be distributed.
        'Skip the following code when the grid is being refreshed since this
        'does not applied to a group of lines
        If Not bDataRefresh Then
            If iItemType > 4 And mbIMIntegrated Then
                If (iTrackMeth > 0 Or iTrackQtyAtBin = 1) Then
                    If iItemType = IMS_BTO_KIT Then
                        cmdComponents.Enabled = True
                        cmdDist.Enabled = False
                    Else
                        cmdDist.Enabled = True
                        cmdComponents.Enabled = False
                    End If
                Else
                    If iItemType = IMS_BTO_KIT Then
                        cmdComponents.Enabled = True
                    Else
                        cmdComponents.Enabled = False
                    End If
                    cmdDist.Enabled = False
                End If
            Else
                cmdComponents.Enabled = False
                cmdDist.Enabled = False
            End If
        End If
    End If
       
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetRowValues", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub CreateDistForm(ByVal lWhseKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**********************************************************************
' Description:
'    This routine creates the Distribution Objects if the object is not
'    alreay created.
'
'**********************************************************************

    'This sub was required since the Distribution form requires a warehousekey, we don't have warehouse key until an item is chosen
    If moItemDist Is Nothing And mbIMActivated Then
        '-- Setup Item Distribution object
       Set moItemDist = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
"imzde001.clsimzde001", 117833828, kAOFRunFlags, kContextAOF)

        If Not moItemDist Is Nothing Then
            If Not moItemDist.InitDistribution(moAppDB, moAppDB, lWhseKey) Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgErrInitItemDist
                Exit Sub
            End If
        End If
    End If
    
    If Not moItemDist Is Nothing And moProcessPickList.oItemDist Is Nothing Then
        moProcessPickList.oItemDist = moItemDist
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CreateDistForm", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub navGridUOM_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
  
    Dim lRow As Long

    lRow = grdShipment.ActiveRow
    gGridSetActiveCell grdShipment, lRow, kColLineShipUOM
    txtNavUOM.Text = ""
    
    'Perform nav_click
    gcLookupClick Me, navGridUOM, txtNavUOM, "UnitMeasID"
    moGMShipLine.LookupClicked

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navGridUOM_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub lkuAutoDistBin_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lTranType As Long
    Dim lWhseKey As Long
    
    lTranType = kTranTypeSOSH
    lWhseKey = glGetValidLong(gsGridReadCell(grdShipment, grdShipment.ActiveRow, kColLineShipWhseKey))
    
    'Initialize the Distribution object if not already initialized
    CreateDistForm lWhseKey
    gGridSetActiveCell grdShipment, grdShipment.ActiveRow, kColLineAutoDistBinID
    moProcessPickList.ProcessAutoBinLkuClick Me, grdShipment, moDMShipLineGrid, moGMShipLine, lkuAutoDistBin, lTranType
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuAutoDistBin_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Function bSetGridNavRestrict(ByRef grd As Object, ByRef navGridUOM As Object, ByVal lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'    Description:
'    This function setup Restriction for the UOM and SubItem Nav on the
'                               grid
'
'    Parameters:
'           grd <in>            - specify the grid to be of the Row
'           lRow <in>           - specify the grid row
'           navGridUOM <in>     - GridUOM control
'           navGridSubItem <in> - GridSubItemcontrol
'
'    Return Values:
'           False   - if the function executed its code without error.
'           True    - if any of the code executed in this
'                     function produced an error or invalid result.
'***********************************************************************'
    Dim lItemKey As Long
    Dim lWhseKey As Long
    Dim lShipUOMKey As Long
    Dim iAllowDecimalQty As Integer
    Dim dQtyToPick As Double
    Dim sRestrictClause As String
    
    bSetGridNavRestrict = False

    If lRow = 0 Then
        bSetGridNavRestrict = True
        Exit Function
    End If

    lItemKey = glGetValidLong(gsGridReadCell(grd, lRow, kColLineItemKey))
    lWhseKey = glGetValidLong(gsGridReadCell(grd, lRow, kColLineShipWhseKey))
    lShipUOMKey = glGetValidLong(gsGridReadCell(grd, lRow, kColLineShipUOMKey))
    iAllowDecimalQty = giGetValidInt(gsGridReadCell(grd, lRow, kColLineAllowDecimalQty))
    dQtyToPick = gdGetValidDbl(gsGridReadCell(grd, lRow, kColLineQtyToPick))
    
    'Add restriction to SubItem base on UOM conversion rules
    sRestrictClause = "UnitMeasKey IN (SELECT a.TargetUnitMeasKey FROM timItemUnitOfMeas a WITH (NOLOCK)"
    sRestrictClause = sRestrictClause & " WHERE a.ItemKey = " & Format$(lItemKey) & ")"
    navGridUOM.RestrictClause = sRestrictClause

    bSetGridNavRestrict = True

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSetGridNavRestrict", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Function bPopulateCreatePickWrkTbl(ByVal iLockData As Integer) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'    Description:
'    This function will populate the work table for create picks
'    Parameters:
'           grd <in>            - specify the grid to be of the Row
'
'    Return Values:
'           False   - if the function executed its code without error.
'           True    - if any of the code executed in this
'                     function produced an error or invalid result.
'***********************************************************************'

Dim sSQL As String

    bPopulateCreatePickWrkTbl = False
    
    On Error Resume Next
    'Clear the create pick work table
    sSQL = "TRUNCATE TABLE #tsoCreatePickWrk1"
    
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        Exit Function
    End If
    
    'Clear the create pick work table
    sSQL = "TRUNCATE TABLE #tsoCreatePickWrk2"
    
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        Exit Function
    End If
    
    'If the sales order is on hold or the customer is on hold then exit
    If miOrderOnCreditHold = 1 Or miCustOnHold = 1 Or miCustOnNAHold = 1 Then
        miSingleShipmentViolation = 1
        bPopulateCreatePickWrkTbl = True
        Exit Function
    End If
    
    'Create the insert statement
    sSQL = "INSERT INTO #tsoCreatePickWrk1 "
    sSQL = sSQL & " (LineDistKey, LineKey, OrderKey, TranType, CrHold, LineHold, OrderHold) "
    sSQL = sSQL & " SELECT tsoSOLineDist.SOLineDistKey, "
    sSQL = sSQL & " tsoSOLineDist.SOLineKey, tsoSOLine.SOKey, "
    sSQL = sSQL & " tsoSalesOrder.TranType, 0,0,0"
    sSQL = sSQL & " FROM tsoSalesOrder WITH (NOLOCK) "
    sSQL = sSQL & " JOIN tsoSOLine WITH (NOLOCK) ON tsoSalesOrder.SOKey = tsoSOLine.SOKey "
    sSQL = sSQL & " JOIN tsoSOLineDist WITH (NOLOCK) ON tsoSOLine.SOLineKey = tsoSOLineDist.SOLineKey "
    sSQL = sSQL & " JOIN tarCustomer WITH (NOLOCK) ON tsoSalesOrder.CustKey = tarCustomer.CustKey "
    sSQL = sSQL & " WHERE tsoSalesOrder.SOKey = " & mlSOKey
    sSQL = sSQL & " AND tsoSalesOrder.Status = " & kOpen
    sSQL = sSQL & " AND tsoSalesOrder.CrHold = 0 "
    sSQL = sSQL & " AND tsoSOLine.Status = " & kOpen
    sSQL = sSQL & " AND tsoSOLineDist.Status = " & kOpen
    sSQL = sSQL & " AND tsoSOLineDist.QtyOpenToShip <> 0"
    sSQL = sSQL & " AND tsoSOLineDist.DeliveryMeth =" & miPickListDelMth
    sSQL = sSQL & " AND tarCustomer.Hold =0"
    
    If miUseNationalAccts = 1 Then
        sSQL = sSQL & " AND (tarCustomer.NationalAcctLevelKey IS NULL OR (tarCustomer.NationalAcctLevelKey IS NOT NULL AND tarCustomer.NationalAcctLevelKey IN"
        sSQL = sSQL & " (SELECT NationalAcctLevelKey FROM tarNationalAcctLevel WITH (NOLOCK)"
        sSQL = sSQL & " WHERE NationalAcctKey IN"
        sSQL = sSQL & " (SELECT NationalAcctKey FROM tarNationalAcct WITH (NOLOCK)"
        sSQL = sSQL & " WHERE Hold = 0))))"
    End If
    
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        Exit Function
    End If
    
    'Populate the work table
    If Not moSelectPick.bGetQtyFromIM(msCompanyID, kInclZeroAvail, kImmediatePick, iLockData) Then
        'Message
        Exit Function
    Else
        miSingleShipmentViolation = moSelectPick.iSingleShipmentViolation
    End If
        
    bPopulateCreatePickWrkTbl = True

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bPopulateCreatePickWrkTbl", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Private Sub ReversePickCompleteFlag()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim sSQL As String

    On Error Resume Next

    'Delete the deleted lines from the lock table
    sSQL = "UPDATE tsoShipLine"
    sSQL = sSQL & " SET PickingComplete = 1"
    sSQL = sSQL & " WHERE ShipLineKey IN"
    sSQL = sSQL & " (SELECT ShipLineKey FROM #tsoCreatePickWrk2)"
    
    moAppDB.ExecuteSQL sSQL
    
    'Remove the logical lock
    RemoveLogicalLock
                            
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ReversePickCompleteFlag", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function CheckIsLinesAvailToPick() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim lLineNbrSelected As Long

    CheckIsLinesAvailToPick = 0
    
    'Check if there is any delivery method specified
    If miPickListDelMth = 0 Then
        If OptPickType(kCounterSale).Enabled And OptPickType(kCounterSale).Value = True Then
            miPickListDelMth = kDeliveryMeth_CounterSale
        End If
        
        If OptPickType(kWillCall).Enabled And OptPickType(kWillCall).Value = True Then
            miPickListDelMth = kDeliveryMeth_WillCall
        End If
        
        If OptPickType(kShippedOrder).Enabled And OptPickType(kShippedOrder).Value = True Then
            miPickListDelMth = kDeliveryMeth_Ship
        End If
    End If
    
    'If miPickListDelMth is still nothing then exit
    If miPickListDelMth = 0 Then
        cmdCreatePick.Enabled = False
        Exit Function
    End If
    
    If miSingleShipmentViolation = 1 Then Exit Function
       
    If bPopulateCreatePickWrkTbl(kDontLockData) Then
        'Check whether the there is any candidate lines for create ship line
        lLineNbrSelected = glGetValidLong(moClass.moAppDB.Lookup("Count(*)", "#tsoCreatePickWrk2", "LineHold = 0 AND OrderHold = 0 AND CrHold = 0"))
        
        If lLineNbrSelected = 0 Then
        'There is no line can be picked
            'Check if there is any ShipleShipmentViolation caused the pick to fail
            If miSingleShipmentViolation = 1 Then
            'yes, issue message
                giSotaMsgBox Me, moClass.moSysSession, kSOmsgSingleShipmentViolation
            End If
        
            'Disable the Create Pick List button
            cmdCreatePick.Enabled = False
        Else
        'Yes, there are order lines to pick for the selected delivery method, enable the
        'create pick list button
            cmdCreatePick.Enabled = True
        End If
    End If
    
    CheckIsLinesAvailToPick = 1

'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CheckIsLinesAvailToPick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bLoadCompForm(Optional bShowForm As Boolean = True, Optional bShowMsg As Boolean = True) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'    Description:
'    This function loads the Component form (frmPickLineCompItem)
'
'    Parameters:
'
'    Return Values:
'           False   - if the function executed its code without error.
'           True    - if any of the code executed in this
'                     function produced an error or invalid result.
'***********************************************************************'
    
    Dim sSQL As String
    Dim sKitItemID As String
    Dim lKitItemKey As Long
    Dim lShipLineKey As Long
    Dim lKitUOMKey As Long
    Dim sKitUOM As String
    Dim dKitQty As Double
    Dim dQtyOrdered As Double
    Dim lShipWhseKey As Long
    Dim lActiveRow As Long
    Dim sKitItemDesc As String
    
    bLoadCompForm = False
    
    Static bCompFormLoaded As Boolean

    Set frmPickLineCompItem.oClass = moClass
    Set frmPickLineCompItem.oAppDB = moAppDB
    Set frmPickLineCompItem.oProcessPickList = moProcessPickList

    If Not bCompFormLoaded Then
        Load frmPickLineCompItem
        bCompFormLoaded = True
    End If
    
    lActiveRow = grdShipment.ActiveRow

    'Are we on a good grid row to go to the Line Details form?
    If lActiveRow > 0 Then
    
       'Read the grid line data to pass into the comp form.
        sKitItemID = gsGetValidStr(gsGridReadCell(grdShipment, lActiveRow, kColLineItem))
        lKitItemKey = glGetValidLong(gsGridReadCell(grdShipment, lActiveRow, kColLineItemKey))
        lShipLineKey = glGetValidLong(gsGridReadCell(grdShipment, lActiveRow, kColLineShipLineKey))
        lKitUOMKey = glGetValidLong(gsGridReadCell(grdShipment, lActiveRow, kColLineShipUOMKey))
        sKitUOM = gsGetValidStr(gsGridReadCell(grdShipment, lActiveRow, kColLineShipUOM))
        dQtyOrdered = gdGetValidDbl(gsGridReadCell(grdShipment, lActiveRow, kColLineQtyOrdered))
        dKitQty = gdGetValidDbl(gsGridReadCell(grdShipment, lActiveRow, kColLineQtyPicked))
        lShipWhseKey = glGetValidLong(gsGridReadCell(grdShipment, lActiveRow, kColLineShipWhseKey))
        sKitItemDesc = gsGetValidStr(gsGridReadCell(grdShipment, lActiveRow, kColLineItemDesc))
       
        'Setup the CurrDisplayRow flag in #tsoCreatePickWrk2 so only the selected Kit Components
        'lines will be displayed in the CompItem Form
        sSQL = "UPDATE #tsoCreatePickWrk2"
        sSQL = sSQL & " SET CurrDisplayRow = CASE WHEN KitShipLineKey = " & lShipLineKey
        sSQL = sSQL & " THEN 1 ELSE 0 END"
    
        moAppDB.ExecuteSQL sSQL
        If Err.Number <> 0 Then
            Exit Function
        End If
        
        'Recalculate the QtyPick for the selected
        'so the Kit Component Qty reflect the latest change from the Kit quantity
        sSQL = "UPDATE wrk"
        sSQL = sSQL & " SET wrk.QtyPicked = t1.QtyPicked * wrk.CompItemQty"
        sSQL = sSQL & " FROM #tsoCreatePickWrk2 wrk JOIN "
        sSQL = sSQL & " #tsoCreatePickWrk2 t1 ON wrk.KitShipLineKey = t1.ShipLineKey "
        sSQL = sSQL & " WHERE wrk.KitShipLineKey = " & lShipLineKey
        
        moAppDB.ExecuteSQL sSQL
        If Err.Number <> 0 Then
            Exit Function
        End If
       
        'Load and display the component form
        If Not moItemDist Is Nothing Then
            frmPickLineCompItem.bIsDirty = False
            'Modified to pass the optional parameters bShowForm and bShowMsg.
            frmPickLineCompItem.LoadCompForm lKitItemKey, sKitItemID, sKitItemDesc, lShipLineKey, _
                                lKitUOMKey, sKitUOM, dKitQty, dQtyOrdered, lShipWhseKey, moItemDist, _
                                miNbrDecPlaces, miEmptyBins, miEmptyRandomBins, miOverShipmentPolicy, _
                                bShowForm, bShowMsg
        End If
   
        'Was some data changed on the Detail form?
        If frmPickLineCompItem.bIsDirty Then
            'Yes, some data WAS changed on the Detail form.
            'Perform necessary updates
            If dKitQty <> frmPickLineCompItem.dKitQty Then
                gGridUpdateCellText grdShipment, lActiveRow, kColLineQtyPicked, CStr(frmPickLineCompItem.dKitQty)
            End If
            
            moDMShipLineGrid.SetDirty True
            
'            grdShipment.redraw = False
'            moDMShipLineGrid.Refresh
'            grdShipment.redraw = True
        End If
    End If
    
    bLoadCompForm = True
    
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bLoadCompForm", VBRIG_IS_CLASS
        Call giErrorHandler: Exit Function
'+++ VB/Rig End +++
End Function

Public Sub ProcessSO(ByVal lSOKey As Long, ByVal sTranNo As String, ByVal lCustKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
' Description: ProcessSO is a method that allows a calling
'              object to run the Process SO program.
'
' Parameters:
'      lSOKey <in> - The SO Key that shipments will be processed.
'      sTranNo <in>  - The SO TranNo that shipments will be processed.
'      lCustKey <in>
'      oCredForm <in>
'*********************************************************************
    On Error GoTo ExpectedErrorRoutine

    Dim lRetVal As Long
    Dim lNACustKey As Long
    Dim sSQL As String
    
    'Show the Batch ID in the form caption.
    mlCustKey = lCustKey
    mlSOKey = lSOKey
    msSOTranNo = sTranNo
    miOrderOnCreditHold = giGetValidInt(moAppDB.Lookup("CrHold", "tsoSalesOrder", "SOKey = " & mlSOKey))
    miCustOnHold = giGetValidInt(moAppDB.Lookup("Hold", "tarCustomer", "CustKey = " & lCustKey))
        
    miCustOnNAHold = 0
    If miUseNationalAccts = 1 Then

        sSQL = "CustKey = " & lCustKey & " AND NationalAcctLevelKey IS NOT NULL AND NationalAcctLevelKey IN"
        sSQL = sSQL & " (SELECT NationalAcctLevelKey FROM tarNationalAcctLevel WITH (NOLOCK)"
        sSQL = sSQL & " WHERE NationalAcctKey IN"
        sSQL = sSQL & " (SELECT NationalAcctKey FROM tarNationalAcct WITH (NOLOCK)"
        sSQL = sSQL & " WHERE Hold = 1))"

        lNACustKey = glGetValidLong(moAppDB.Lookup("CustKey", "tarCustomer", sSQL))

        If lNACustKey > 0 Then
            miCustOnNAHold = 1
        End If
    End If
    
    SetFormCaption
    
    miSingleShipmentViolation = 0
    CheckAvailSOLinePerDeliveryMeth
    ToggleDeliveryMethOpt

    
    bSetupNavMain msCompanyID
    
    '-- Center print form
    Me.Top = (Me.Height / 2) + Me.Top - (Me.Height / 2)
    Me.Left = (Me.Width / 2) + Me.Left - (Me.Width / 2)
    
    ClearForm
    
       
    'Show the main Enter Customer Returns form now.
    Me.Show vbModal
    
    'Check for any error that might have occurred.
    If Err.Number <> 0 Then
        Err.Raise Err.Number
    End If
    
    Exit Sub


ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gMainClassErr Err, sMyName, "ProcessSO", VBRIG_IS_FORM
        Resume Next
'+++ VB/Rig End +++
End Sub

Private Sub CheckAvailSOLinePerDeliveryMeth()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
' Description: This rounte looks up the so lines counts by different
'              delivery method for the Sales Order currently loaded and
'              store the counts in the correspond module variables.
'              Later on in the process the application will use the
'              the value in the variables to determain whether a delivery
'              method pick should be enabled.
' Parameters:

'*********************************************************************

    Dim sSQL As String
    Dim rs As Object
    Dim iDeliveryMeth As Integer
    
    mlShipLineCount = 0
    mlCounterSaleLineCount = 0
    mlWillCallLineCount = 0
 
    sSQL = "SELECT COUNT(sol.SOLineKey) SOLineCount, sold.DeliveryMeth FROM tsoSalesOrder so WITH (NOLOCK)"
    sSQL = sSQL & " JOIN tsoSOLine sol WITH (NOLOCK) ON so.SOKey =sol.SOKey AND so.SOKey = " & mlSOKey
    sSQL = sSQL & " JOIN tsoSOLineDist sold WITH (NOLOCK) ON sol.SOLineKey = sold.SOLineKey"
    sSQL = sSQL & " GROUP BY sold.DeliveryMeth"

    Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If Not rs.IsEmpty Then
        rs.MoveFirst
        Do While Not rs.IsEOF
            iDeliveryMeth = giGetValidInt(rs.Field("DeliveryMeth"))
            
            Select Case iDeliveryMeth
                Case kDeliveryMeth_Ship
                    mlShipLineCount = glGetValidLong(rs.Field("SOLineCount"))
                
                Case kDeliveryMeth_CounterSale
                    mlCounterSaleLineCount = glGetValidLong(rs.Field("SOLineCount"))
                
                Case kDeliveryMeth_WillCall
                    mlWillCallLineCount = glGetValidLong(rs.Field("SOLineCount"))
            
            End Select
            rs.MoveNext
        Loop
        rs.Close
    End If
     
    Set rs = Nothing

'+++ VB/Rig Begin Pop +++
    Exit Sub
#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gMainClassErr Err, sMyName, "CheckAvailSOLinePerDeliveryMeth", VBRIG_IS_FORM
'+++ VB/Rig End +++
End Sub

Private Sub ToggleDeliveryMethOpt()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
' Description: This rounte will enable or disable DeliveryMeth Opt
'              in the form according to the values in SOLineCount
'              by delivery method variables.
' Parameters:
'*********************************************************************

Dim bPickTypeSelected As Boolean

    bPickTypeSelected = False
    
    If mlCounterSaleLineCount > 0 And Len(Trim(txtPickTicketNo.Text)) = 0 Then
        OptPickType(kCounterSale).Enabled = True
        
        If Not bPickTypeSelected Then
            OptPickType(kCounterSale).Value = True
            bPickTypeSelected = True
        End If
    Else
        OptPickType(kCounterSale).Enabled = False
    End If
    
    If mlWillCallLineCount > 0 And Len(Trim(txtPickTicketNo.Text)) = 0 Then
        OptPickType(kWillCall).Enabled = True
        
        If Not bPickTypeSelected Then
            OptPickType(kWillCall).Value = True
            bPickTypeSelected = True
        End If
    Else
        OptPickType(kWillCall).Enabled = False
    End If
    
    If mlShipLineCount > 0 And Len(Trim(txtPickTicketNo.Text)) = 0 Then
        OptPickType(kShippedOrder).Enabled = True
        
        If Not bPickTypeSelected Then
            OptPickType(kShippedOrder).Value = True
            bPickTypeSelected = True
        End If
    Else
        OptPickType(kShippedOrder).Enabled = False
    End If
              
'+++ VB/Rig Begin Pop +++
    Exit Sub
#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gMainClassErr Err, sMyName, "ToggleDeliveryMethOpt", VBRIG_IS_FORM
'+++ VB/Rig End +++
End Sub
Private Sub ToggleControls()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
' Description: This rounte will enable or disable Toolbar control buttons
'              and cmdEditShipment, cmdPrintInvoice in the form according
'              status of the pick list
' Parameters:
'*********************************************************************

    Dim lRowCount As Long
   
    If Len(Trim(txtPickTicketNo.Text)) = 0 Or grdShipment.MaxRows = 0 Then
    'No pick list is selected
        tbrMain.ButtonEnabled(kTbPrint) = False
        tbrMain.ButtonEnabled(kTbPreview) = False
        tbrMain.ButtonEnabled(kTbDelete) = False
        tbrMain.ButtonEnabled(kTbSave) = False
        tbrMain.ButtonEnabled(kTbCommit) = False
        
        cmdEditShipment.Enabled = False
        cmdPrintInvoice.Enabled = False
        chkHold.Enabled = False
        txtHoldReason.Enabled = False
        cmdComponents.Enabled = False
        cmdDist.Enabled = False
        cmdApply.Enabled = False
        
        If Len(Trim(txtPickTicketNo.Text)) = 0 Then
            CheckIsLinesAvailToPick
        End If
        
               
        grdShipment.Enabled = False
    Else
    'A pick list is selected
        If miShipmentCommitted = kShipmentStatusPending And mlShipKey > 0 Then
            tbrMain.ButtonEnabled(kTbDelete) = True
            tbrMain.ButtonEnabled(kTbSave) = True
            
            tbrMain.ButtonEnabled(kTbCommit) = True

            cmdEditShipment.Enabled = True
            chkHold.Enabled = True
            
            txtHoldReason.Enabled = (chkHold.Value = vbChecked)
            
            'Check for user security right to enable the generate invoice button
            If bOverrideSecEvent("SOGENINV", moClass, False) Then
                cmdApply.Enabled = True
                cmdGenInvoice.Enabled = True
            Else
                cmdApply.Enabled = False
            End If
            
            cmdPrintInvoice.Enabled = False
        Else
            If mlShipKey > 0 And miShipmentCommitted = kShipmentStatusCommitted Then
                tbrMain.ButtonEnabled(kTbDelete) = False
                
                
                If miTranStatus = kShipTranStatusInvoiced Then
                    tbrMain.ButtonEnabled(kTbCommit) = True
                    tbrMain.ButtonEnabled(kTbSave) = True
                    chkHold.Enabled = True
                    txtHoldReason.Enabled = (chkHold.Value = vbChecked)
                Else
                    tbrMain.ButtonEnabled(kTbCommit) = False
                    tbrMain.ButtonEnabled(kTbSave) = False
                    chkHold.Enabled = False
                    txtHoldReason.Enabled = False
                End If
                
                cmdEditShipment.Enabled = True
                If bOverrideSecEvent("SOGENINV", moClass, False) Then
                    cmdApply.Enabled = True
                Else
                    cmdApply.Enabled = False
                End If
                

                cmdComponents.Enabled = False
                cmdDist.Enabled = False
                
                                
                If miInvoiceReqdByOption = 1 Then
                    cmdPrintInvoice.Enabled = True
                End If
                
                
            Else
                tbrMain.ButtonEnabled(kTbDelete) = True
                tbrMain.ButtonEnabled(kTbSave) = True
                tbrMain.ButtonEnabled(kTbCommit) = False
                
'                If mlShipKey > 0 Then
'                    cmdEditShipment.Enabled = True
'                Else
                    cmdEditShipment.Enabled = False
                    chkHold.Enabled = False
                    txtHoldReason.Enabled = False
'                End If
                
                cmdPrintInvoice.Enabled = False
                            
                cmdComponents.Enabled = False
                cmdDist.Enabled = False
            End If
            
            If miTranStatus = kShipTranStatusPosted Or miTranStatus = kShipTranStatusCommitted Then
                cmdGenInvoice.Enabled = False
                cmdDelInvoice.Enabled = False
                cmdApply.Enabled = False
                chkHold.Enabled = False
                txtHoldReason.Enabled = False
            End If
        End If
        
        SetupShipmentStatus
       
        
        grdShipment.Enabled = True
        
        tbrMain.ButtonEnabled(kTbPrint) = True
        tbrMain.ButtonEnabled(kTbPreview) = True

    End If
   
'+++ VB/Rig Begin Pop +++
    Exit Sub
#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gMainClassErr Err, sMyName, "ToggleControls", VBRIG_IS_FORM
'+++ VB/Rig End +++
End Sub

Private Sub SetupShipmentStatus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
 'Set up Shipment Status
        Select Case miTranStatus
            Case kTranStatusNoShipment
                lblShipmentStatusText.Caption = ""
            Case kShipTranStatusIncomplete
                lblShipmentStatusText.Caption = gsBuildString(kTranIncomplete, moAppDB, moClass.moSysSession)
            Case kShipTranStatusPending
                lblShipmentStatusText.Caption = gsBuildString(kTranPending, moAppDB, moClass.moSysSession)
            Case kShipTranStatusPosted
                lblShipmentStatusText.Caption = gsBuildString(kTranPosted, moAppDB, moClass.moSysSession)
            Case kShipTranStatusPurged
                lblShipmentStatusText.Caption = gsBuildString(kTranPurged, moAppDB, moClass.moSysSession)
            Case kShipTranStatusVoid
                lblShipmentStatusText.Caption = gsBuildString(kTranVoid, moAppDB, moClass.moSysSession)
            Case kShipTranStatusCommitted
                lblShipmentStatusText.Caption = gsBuildString(kPACommitted, moAppDB, moClass.moSysSession)
            Case kShipTranStatusInvoiced
                lblShipmentStatusText.Caption = gsBuildString(kPAInvoiced, moAppDB, moClass.moSysSession)
        End Select
            
'+++ VB/Rig Begin Pop +++
    Exit Sub
#If ERRORTRAPON = 0 Then
    Err.Raise Err
#End If
VBRigErrorRoutine:
        gMainClassErr Err, sMyName, "ToggleControls", VBRIG_IS_FORM
'+++ VB/Rig End +++
End Sub

Private Sub SetupMsgCombos()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim arrStaticValues()   As String
    Dim sString             As String
    Dim i                   As Integer
    
    'Build Static List for the Severity Combo Box
    ReDim arrStaticValues(3)
    
    sString = gsBuildString(kARAll, moClass.moAppDB, moClass.moSysSession)
    arrStaticValues(1) = sString
    
    arrStaticValues(2) = 1
    sString = gsBuildString(ksWarning, moClass.moAppDB, moClass.moSysSession)
    arrStaticValues(2) = sString
    
    arrStaticValues(3) = 0
    sString = gsBuildString(ksFatal, moClass.moAppDB, moClass.moSysSession)
    arrStaticValues(3) = sString
    
'    arrStaticValues(4) = 1
'    sString = gsBuildString(ksNoError, moClass.moAppDB, moClass.moSysSession)
'    arrStaticValues(4) = sString
    
    For i = 0 To 2
        cboSeverity.AddItem arrStaticValues(i + 1)
        cboSeverity.ItemData(i) = i
    Next i
    cboSeverity.ListIndex = 0

    ' setup Printer dropdown combo
    ddnOutput.AddItem gsBuildString(kPreviewCol, moClass.moAppDB, moClass.moSysSession), 1
    ddnOutput.ItemData(0) = kPreviewCol
    ddnOutput.AddItem gsBuildString(kIMPrinter, moClass.moAppDB, moClass.moSysSession), 0
    ddnOutput.ItemData(1) = kIMPrinter
    
    ddnOutput.ListIndex = 0
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupMsgCombos", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub LoadMsgCboDflts()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    cboSeverity.ListIndex = miDfltSeverity

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "LoadMsgCboDflts", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bSaveShipLine() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    On Error GoTo ExpectedErrorRoutine

Dim sSQL As String
Dim sWhere As String
Dim lRow As Long
Dim lRowCount As Long

    bSaveShipLine = False

    SetMouse
             
    'Begin transaction
    moAppDB.BeginTrans
    
    'Save the ShipLine Distribution
    If Not moProcessPickList.bSaveDistAndUpdGrid(Me, grdShipment, moDMShipLineGrid) Then
        ResetMouse
        GoTo Rollback
    End If
    
    'Update tsoPickList permernent ship line tables
    If Not moProcessPickList.bUpdShipLine(Me, miNbrDecPlaces, mlPickListKey) Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgCMUnexpectedSPError, "Update ShipLine"
        ResetMouse
        GoTo Rollback
    End If
    
    'Delete the pick list if use has delete all the ship lines from the grid
    If Not bDeletePickList(mlPickListKey) Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation  'kmsgimspGeneralReserveInvtTran
        ResetMouse
        GoTo Rollback
    End If
     
    'Commit Transaction
    moAppDB.CommitTrans
    
    'Refresh the grid to reflect the latest change
    grdShipment.redraw = False
    moDMShipLineGrid.Refresh
    grdShipment.redraw = True
   
    ResetMouse
    
    bSaveShipLine = True
    
    Exit Function
    
Rollback:
    On Error Resume Next
    moAppDB.Rollback
    ResetMouse
    Exit Function
    
ExpectedErrorRoutine:

    moAppDB.Rollback
    ResetMouse
    
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSaveShipLine", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Function bDeleteShipLines(ByVal sWhere As String) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'   Description:
'       Delete distributions in the ship lines and remove Ship lines from
'       ship line tables if the caller specified
'
'    Parameters:
'           sWhere <in>         - specify which set of record need to deleted
'           iDeleteDistributionOnly <in>
'                               - 0 not only delete the distributions for the
'                                   specified ship linesbut also
'                                   delete the specified ship lines
'                               - 1 only delete the distributions for the
'                                   specified ship lines
'           iDeleteTempTable <in>
'                               - 0 only remove the distribution in the temporary
'                                   work tables for specified ship lines
'                               - 1 delete the specified ship lines from the
'                                   temporary work tables
'
'   Return Values:
'           False   - if the function executed its code without error.
'           True    - if any of the code executed in this
'                     function produced an error or invalid result.
'***********************************************************************
Dim sSQL        As String
Dim lRow        As Long
Dim lRetVal     As Integer
Dim iUpdateCounterViolationExists As Integer
Dim iConfirmation As Integer

    bDeleteShipLines = False

    On Error Resume Next
    'Clear the temp table #tsoCancelPickWrk1 used by the deletion sp
    sSQL = "TRUNCATE TABLE #tsoCancelPickWrk1"
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        gClearSotaErr
    End If

    'Build the query used to get the data set to populate #tsoCancelPickWrk1
    sSQL = sBuildDeletePickLineQuery(sWhere)

    'Populate #tsoCancelPickWrk1
    moAppDB.ExecuteSQL sSQL

    If Err.Number <> 0 Then
        Exit Function
    End If

    'Call sp to delete the ShipLines
    With moAppDB
        .SetInParam msCompanyID
        .SetInParam miNbrDecPlaces
        .SetInParam kDeleteDistributionOnlyNo
        .SetInParam gsFormatDateToDB(moClass.moSysSession.BusinessDate)
        .SetOutParam iUpdateCounterViolationExists
        .SetOutParam lRetVal
        .ExecuteSP "spsoDelPickShipLines"
        iUpdateCounterViolationExists = giGetValidInt(.GetOutParam(5))
        lRetVal = glGetValidLong(.GetOutParam(6))
        .ReleaseParams
    End With

    'Raise warning to the user there is update counter conflict occurred
    'during the data updating.  A reload of the data from the database
    'to reflect the latest changes is necessary
    If iUpdateCounterViolationExists = 1 Then
        giSotaMsgBox moClass, moClass.moSysSession, kSOmsgUpdateConflictRefreshDat
        moProcessPickList.ReloadDataForUpdateCounterConflict Me
    End If

    If lRetVal <> 1 Or Err.Number <> 0 Then
        giSotaMsgBox moClass, moClass.moSysSession, kmsgProc, "spsoDelPickShipLines"
        Exit Function
    End If

    bDeleteShipLines = True

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        moAppDB.ExecuteSQL "IF @@TRANCOUNT <> 0 ROLLBACK TRAN"
        gSetSotaErr Err, sMyName, "bDeleteShipLines", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
End Function

Private Function sBuildDeletePickLineQuery(ByVal sWhere As String) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'   Description:
'       This function builds the Query used to create the insert statement
'       for temporary table used by shipline deletion store procedure.
'
'    Parameters:
'           sWhere <in>         - specify which set of record need to deleted
'
'   Return Values:
'           sBuildDeletePickLineQuery   - a complete insert statement
'***********************************************************************
Dim sSQL As String

    sSQL = "INSERT INTO #tsoCancelPickWrk1"
    sSQL = sSQL & " (ShipLineDistKey"
    sSQL = sSQL & " ,ShipLineKey"
    sSQL = sSQL & " ,InvtTranKey"
    sSQL = sSQL & " ,ItemKey"
    sSQL = sSQL & " ,SubItemKey"
    sSQL = sSQL & " ,KitShipLineKey"
    sSQL = sSQL & " ,PickListKey"
    sSQL = sSQL & " ,PickListLineNo"
    sSQL = sSQL & " ,ShipUnitMeasKey"
    sSQL = sSQL & " ,ShipDate"
    sSQL = sSQL & " ,OrderKey"
    sSQL = sSQL & " ,LineKey)"
    sSQL = sSQL & " SELECT wrk.ShipLineDistKey"
    sSQL = sSQL & " ,wrk.ShipLineKey"
    sSQL = sSQL & " ,CASE WHEN wrk.InvtTranKey > 0 THEN wrk.InvtTranKey ELSE NULL END"
    sSQL = sSQL & " ,wrk.ItemKey"
    sSQL = sSQL & " ,wrk.SubstituteItemKey" ' COALESCE(wrk.SubstituteItemKey,wrk.ItemKey)"
    sSQL = sSQL & " ,wrk.KitShipLineKey"
    sSQL = sSQL & " ,SL.PickListKey"
    sSQL = sSQL & " ,SL.PickListLineNo"
    sSQL = sSQL & " ,wrk.ShipUnitMeasKey"
    sSQL = sSQL & " ,SL.ShipDate"
    sSQL = sSQL & " ,wrk.OrderKey " 'field does not allow null
    sSQL = sSQL & " ,wrk.LineKey"
    sSQL = sSQL & " FROM #tsoCreatePickWrk2 wrk"
    sSQL = sSQL & " JOIN tsoShipLine SL WITH (NOLOCK) "
    sSQL = sSQL & " ON wrk.ShipLineKey = SL.ShipLineKey "
    'Exclude the BTO Component lines since the deletion sp
    'will take care of this
    sSQL = sSQL & " WHERE wrk.KitShipLineKey IS NULL "

    If Len(Trim(sWhere)) = 0 Then
    'No sWhere passed in, use the default query
        sBuildDeletePickLineQuery = sSQL
    Else
    'sWhere has been passed in, attach it to the default query
        sBuildDeletePickLineQuery = sSQL & " AND " & sWhere
    End If

    'Exit this function
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sBuildDeletePickLineQuery", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Function bDeletePickList(ByVal lPickListKey As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'   Description:
'       Delete Pick List for tsoPickListTable
'
'    Parameters:
'           lPickListKey <in>   - PickListKey to be deleted
'
'   Return Values:
'           False   - if the function executed its code without error.
'           True    - if any of the code executed in this
'                     function produced an error or invalid result.
'***********************************************************************
    
 Dim sSQL As String
 
    bDeletePickList = False
    
    On Error Resume Next
    
    sSQL = "Delete tsoPickList where PickListKey = " & lPickListKey
    sSQL = sSQL & " AND PickListKey NOT IN (SELECT PickListKey FROM tsoShipLine WHERE PickListKey IS NOT NULL)"
    
    moAppDB.ExecuteSQL sSQL
    
    If Err.Number <> 0 Then
        Exit Function
    End If
    
    bDeletePickList = True

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DeletePickList", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Private Function bDeleteShipment() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'   Description:
'       Delete ShipKey from the Ship Line
'
'    Parameters:
'           lPickListKey <in>   - PickListKey to be deleted
'
'   Return Values:
'           False   - if the function executed its code without error.
'           True    - if any of the code executed in this
'                     function produced an error or invalid result.
'***********************************************************************
On Error GoTo ExpectedErrorRoutine

 Dim sSQL As String
 
    bDeleteShipment = False
    
    On Error Resume Next
     
    moAppDB.BeginTrans
     
    sSQL = "UPDATE tsoShipLine SET ShipKey = NULL ,"
    sSQL = sSQL & " UpdateCounter = UpdateCounter + 1 WHERE ShipKey = " & mlShipKey
    moAppDB.ExecuteSQL sSQL
    
    sSQL = "DELETE tsoShipmentLog WHERE ShipKey = " & mlShipKey
    moAppDB.ExecuteSQL sSQL
    
    moAppDB.CommitTrans
    
    bDeleteShipment = True
    
    Exit Function

ExpectedErrorRoutine:
    On Error Resume Next
    moAppDB.Rollback

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DeletePickList", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Private Sub FireGMCellChangeIfNeeded()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    'If the user clicks on the toolbar control immediately after typing a value on a
    'grid cell, sometimes the GM CellChange event does not fire.  Make sure the event
    'fires if it hasn't.  (Seems like the issue only applys for the toolbar control.
    'If any other control is clicked, the event still fires.)
    
    'The mbCellChangeFired variable is set to True when the event is called.
    'It is set to False on the Grid_EditChange event or when the user starts changes
    'the value on a cell.
    
    If mbCellChangeFired = False Then
        'set mbForceValiationFired to avoid addtion cell change event to be fired since CellChange
        'event if called from here already.
        mbForceValiationFired = True
        moGMShipLine_CellChange grdShipment.ActiveRow, grdShipment.ActiveCol
        mbCellChangeFired = True
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FireGMCellChangeIfNeeded", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bPopTempTableTransToCommit() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim sSQL As String

    bPopTempTableTransToCommit = False
    
    sSQL = "TRUNCATE TABLE #tciTransToCommit"
    moClass.moAppDB.ExecuteSQL sSQL
    
    If miUseTranDateAsPostDate = 1 Then
        msPostDate = msShipmentTranDate
    End If
    
    sSQL = " INSERT INTO #tciTransToCommit "
    sSQL = sSQL & " (CompanyID, TranType, TranKey, PostDate, InvcDate,"
    sSQL = sSQL & " PreCommitBatchKey, DispoBatchKey, CommitStatus)"
    sSQL = sSQL & " VALUES (" & gsQuoted(msCompanyID) & "," & Format$(kTranTypeSOSH) & ","
    sSQL = sSQL & mlShipKey & ", " & gsQuoted(gsFormatDateToDB(msPostDate)) & "," & gsQuoted(gsFormatDateToDB(msShipmentTranDate)) & ","
    sSQL = sSQL & mlHiddenBatchKey & ", NULL ,0)"
               
    moClass.moAppDB.ExecuteSQL sSQL
    
    bPopTempTableTransToCommit = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bPopTempTableTransToCommit", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function sBuildMessageWhere() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim iSeverity As Integer
    
    iSeverity = kMsgAll
    
    If cboSeverity.ListIndex = 1 Then
        iSeverity = kMsgWarning
    End If
    If cboSeverity.ListIndex = 2 Then
        iSeverity = kMsgFatalError
    End If
    
    sBuildMessageWhere = " SessionID = " & mlSessionID
    
    If iSeverity <> kMsgAll Then
        sBuildMessageWhere = sBuildMessageWhere & _
        " AND Severity = " & giGetValidInt(iSeverity)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "sBuildMessageWhere", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Private Sub PrintErrorLog(sPrintButton As String, lSessionID As Long, bCancelPrint As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    On Error GoTo ExpectedErrorRoutine

    Dim bSkipPrintDialog As Boolean
    
    bSkipPrintDialog = False
    
    ' Set report path to the report object
    moReport.ReportPath = msReportPath
    
    SetMouse
    
    ' Print the error  reports
    If Not lStartErrorReport(sPrintButton, Me, lSessionID, bSkipPrintDialog, bCancelPrint) Then
        'Message
    End If

    ResetMouse
    Exit Sub
    
ExpectedErrorRoutine:
    ResetMouse

'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PrintErrorLog", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetupReport()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim sRptProgramName As String
    Dim sModule As String
    Dim sDefaultReport As String
    Dim lRetVal As Long

    'CUSTOMIZE: assign defaults for this project
    sRptProgramName = "SOZDV003" 'such as rpt001
    sModule = "SO"
    sDefaultReport = "SOZDV003.RPT" 'such as rpt001
    
    msReportPath = moClass.moSysSession.ModuleReportPath("SO")
    lRetVal = lInitializeReport(sModule, sDefaultReport, sRptProgramName)
    If lRetVal <> 0 Then
        moReport.ReportError lRetVal
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupReport", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Sub
Private Function lInitializeReport(ModuleID As String, DefaultReport As String, ProjectName As String) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
   lInitializeReport = -1
    
    Set moPrinter = Printer
    Set moReport = New clsReportEngine
    Set moDDData = New clsDDData
    
    moReport.AppOrSysDB = kAppDB
    
    If (moReport.lInitReport(ModuleID, DefaultReport, Me, moDDData) = kFailure) Then
        lInitializeReport = kmsgFatalReportInit
        Exit Function
    End If
      
    lInitializeReport = 0
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lInitializeReport", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub RemoveLogicalLock()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
'*************************************************************************
' Desc:   This routine will remove the logical lock created by the
'         create pick process
'*************************************************************************
    
Dim lRetVal As Long
    
    On Error Resume Next
    With moAppDB
        .SetInParam mlLogicalLockKey
        .SetOutParam lRetVal
        .ExecuteSP "spsmLogicalLockRemove"
        lRetVal = glGetValidLong(.GetOutParam(2))
        .ReleaseParams
    End With
    
    If lRetVal <> 1 Or Err.Number <> 0 Then
        giSotaMsgBox moClass, moClass.moSysSession, kmsgProc, "spsmLogicalLockRemove"
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "RemoveLogicalLock", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Sub

Private Sub RetrieveInvcInfo()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

On Error GoTo ExpectedErrorRoutine

    Dim lInvcKey As Long
    Dim sInvcNo As String
    Dim sDocCurrID As String
    Dim dSalesAmt As Double
    Dim dFrtAmt As Double
    Dim dTradeDiscAmt As Double
    Dim dTradeDiscPct As Double
    Dim dSTaxAmt As Double
    Dim dTranAmt As Double
    Dim dTranAmtHC As Double
    Dim dPmtAmt As Double
    Dim iRetVal As Integer
    Dim sHomeCurrID As String
    Dim i As Integer
    Dim uCurrInfo As CurrencyInfo
   
'    If mlInvcKey > 0 Or miMultiInvc = 1 Then
'    ' Invoice info has already be loaded, exit
'        Exit Sub
'    End If
    
    ' Initial invoice info varilables
    lInvcKey = 0
    sInvcNo = 0
    sDocCurrID = 0
    dFrtAmt = 0
    dSalesAmt = 0
    dSTaxAmt = 0
    dTradeDiscAmt = 0
    dTradeDiscPct = 0
    dTranAmt = 0
    dTranAmtHC = 0
    dPmtAmt = 0

    With moAppDB
        
        'if processing sales order shipment, these fields will contain info, else null
        .SetInParam mlShipKey
        .SetOutParam miMultiInvc
        .SetOutParam lInvcKey
        .SetOutParam sInvcNo
        .SetOutParam sDocCurrID
        .SetOutParam dSalesAmt
        .SetOutParam dFrtAmt
        .SetOutParam dTradeDiscAmt
        .SetOutParam dTradeDiscPct
        .SetOutParam dSTaxAmt
        .SetOutParam dTranAmt
        .SetOutParam dTranAmtHC
        .SetOutParam dPmtAmt
        
        .SetOutParam iRetVal
        
        .ExecuteSP "spsoGetInvcInfoForShipment"
     
        ' Retrieve the return value
        miMultiInvc = giGetValidInt(.GetOutParam(2))
        lInvcKey = glGetValidLong(.GetOutParam(3))
        sInvcNo = gsGetValidStr(.GetOutParam(4))
        sDocCurrID = gsGetValidStr(.GetOutParam(5))
        dSalesAmt = gdGetValidDbl(.GetOutParam(6))
        dFrtAmt = gdGetValidDbl(.GetOutParam(7))
        dTradeDiscAmt = gdGetValidDbl(.GetOutParam(8))
        dTradeDiscPct = gdGetValidDbl(.GetOutParam(9))
        dSTaxAmt = gdGetValidDbl(.GetOutParam(10))
        dTranAmt = gdGetValidDbl(.GetOutParam(11))
        dTranAmtHC = gdGetValidDbl(.GetOutParam(12))
        dPmtAmt = gdGetValidDbl(.GetOutParam(13))
        iRetVal = giGetValidInt(.GetOutParam(14))
        
         .ReleaseParams
        
    End With
    
    ' Evaluate return value
    If iRetVal = 1 Then
    
        ' Setup muInvoiceInfo
        With muInvoiceInfo
            .lInvcKey = lInvcKey
            .sInvcNo = sInvcNo
            .sCurrID = sDocCurrID
            .dFrtAmt = dFrtAmt
            .dSalesAmt = dSalesAmt
            .dSTaxAmt = dSTaxAmt
            .dTradeDiscAmt = dTradeDiscAmt
            .dTradeDiscPct = dTradeDiscPct
            .dTranAmt = dTranAmt
            .dTranAmtHC = dTranAmtHC
            .dPmtAmt = dPmtAmt
        End With
        
        mlInvcKey = lInvcKey
        
        If mlInvcKey > 0 Then
        
            ' Update display control
            cmdGenInvoice.Enabled = False
            
            'Check for user security right to enable the delete invoice button
            If bOverrideSecEvent("SODELINV", moClass, False) Then
                If miTranStatus = kShipTranStatusPosted Or miTranStatus = kShipTranStatusCommitted Then
                    cmdDelInvoice.Enabled = False
                Else
                    cmdDelInvoice.Enabled = True
                End If
            End If
            
            'Check for user security right to view the invoice info
            If bOverrideSecEvent("SOVIEWINV", moClass, False) Then
                lblInoviceTotal.Visible = True
                lblInoviceTotal.Enabled = True
                cmdInvcTotals.Visible = True
                cmdInvcTotals.Enabled = True
                
                curInvoiceTotal.Visible = True
                curInvoiceTotal.Amount = dTranAmt
                
                sHomeCurrID = moClass.moSysSession.CurrencyID
                
                If sHomeCurrID <> sDocCurrID Then
                    'Setup the currency control
                    gbSetCurrCtls moClass, sDocCurrID, uCurrInfo, curInvoiceTotal
                    
                    lblInvcCurrID.Visible = True
                    lblInvcCurrID.Caption = sDocCurrID
                Else
                    'Setup the currency control
                    gbSetCurrCtls moClass, sHomeCurrID, uCurrInfo, curInvoiceTotal
                    
                    lblInvcCurrID.Visible = False
                End If
             End If

        Else
            If miMultiInvc = 0 Then
                ' Update display control
                'Check for user security right to enable the generate invoice button
                If bOverrideSecEvent("SOGENINV", moClass, False) Then
                    If miTranStatus = kShipTranStatusPosted Or miTranStatus = kShipTranStatusCommitted Then
                        cmdGenInvoice.Enabled = False
                    Else
                        cmdGenInvoice.Enabled = True
                    End If
                End If
        
                lblInoviceTotal.Visible = False
                cmdDelInvoice.Enabled = False
                curInvoiceTotal.Visible = False
                lblInvcCurrID.Visible = False
                cmdInvcTotals.Visible = False
                lblMultiInvc.Visible = False
            Else
            ' Multiple invoices have been generated for the shipment
            
                'Check for user security right to enable the delete invoice button
                If bOverrideSecEvent("SODELINV", moClass, False) Then
                    If miTranStatus = kShipTranStatusPosted Or miTranStatus = kShipTranStatusCommitted Then
                        cmdDelInvoice.Enabled = False
                    Else
                        cmdDelInvoice.Enabled = True
                    End If
                End If
                
                cmdGenInvoice.Enabled = False
                lblInoviceTotal.Visible = False
                curInvoiceTotal.Visible = False
                lblInvcCurrID.Visible = False
                cmdInvcTotals.Visible = False
                lblMultiInvc.Visible = True
            End If
            
        End If
    Else
    ' Add error handling
    End If
    '+++ VB/Rig Begin Pop +++
        Exit Sub
        
ExpectedErrorRoutine:
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "RetrieveInvcInfo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_Paint(Index As Integer)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If
#If CUSTOMIZER Then


Private Sub cmdCCPmnt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdCCPmnt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdCCPmnt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdCCPmnt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdCCPmnt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdCCPmnt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdApply_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdApply, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdApply_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdApply_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdApply, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdApply_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdPmnt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdPmnt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPmnt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdPmnt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdPmnt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPmnt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdGenInvoice_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdGenInvoice, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdGenInvoice_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdGenInvoice_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdGenInvoice, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdGenInvoice_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDelInvoice_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdDelInvoice, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDelInvoice_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDelInvoice_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdDelInvoice, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDelInvoice_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdInvcTotals_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdInvcTotals, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdInvcTotals_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdInvcTotals_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdInvcTotals, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdInvcTotals_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdPrintMsg_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdPrintMsg, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPrintMsg_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdPrintMsg_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdPrintMsg, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPrintMsg_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdClearMsg_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdClearMsg, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdClearMsg_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdClearMsg_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdClearMsg, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdClearMsg_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdComponents_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdComponents, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdComponents_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdComponents_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdComponents, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdComponents_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdCreatePick_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdCreatePick, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdCreatePick_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdCreatePick_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdCreatePick, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdCreatePick_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDist_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdDist, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDist_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDist_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdDist, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDist_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdPrintInvoice_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdPrintInvoice, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPrintInvoice_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdPrintInvoice_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdPrintInvoice, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPrintInvoice_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdEditShipment_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdEditShipment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdEditShipment_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdEditShipment_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdEditShipment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdEditShipment_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub txtHoldReason_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtHoldReason, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtHoldReason_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtHoldReason_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtHoldReason, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtHoldReason_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtHoldReason_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtHoldReason, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtHoldReason_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtHoldReason_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtHoldReason, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtHoldReason_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavUOM_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtNavUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavUOM_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavUOM_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtNavUOM, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavUOM_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavUOM_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtNavUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavUOM_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavUOM_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtNavUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavUOM_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPickTicketNo_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtPickTicketNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPickTicketNo_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPickTicketNo_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtPickTicketNo, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPickTicketNo_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPickTicketNo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtPickTicketNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPickTicketNo_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtlkuAutoDistBin_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtlkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtlkuAutoDistBin_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtlkuAutoDistBin_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtlkuAutoDistBin, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtlkuAutoDistBin_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtlkuAutoDistBin_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtlkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtlkuAutoDistBin_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtlkuAutoDistBin_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtlkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtlkuAutoDistBin_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItem_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuItem, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItem_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItem_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuItem, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItem_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItem_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuItem, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItem_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItem_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuItem, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItem_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItem_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuItem, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItem_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItem_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuItem, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItem_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItem_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuItem, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItem_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub curInvoiceTotal_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curInvoiceTotal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curInvoiceTotal_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curInvoiceTotal_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curInvoiceTotal, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curInvoiceTotal_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curInvoiceTotal_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curInvoiceTotal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curInvoiceTotal_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curInvoiceTotal_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curInvoiceTotal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curInvoiceTotal_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkHold_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkHold, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkHold_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkHold_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkHold, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkHold_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub OptPickType_DblClick(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnDblClick OptPickType(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "OptPickType_DblClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub OptPickType_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus OptPickType(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "OptPickType_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub OptPickType_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus OptPickType(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "OptPickType_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSeverity_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboSeverity, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSeverity_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSeverity_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboSeverity, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSeverity_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSeverity_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboSeverity, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSeverity_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnOutput_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnOutput, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnOutput_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnOutput_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnOutput, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnOutput_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnOutput_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnOutput, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnOutput_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnOutput_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnOutput, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnOutput_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
               Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub CheckAvaTax()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***********************************************************************
' Avatax Integration: Check the availability of Avatax for the CompanyID.
'***********************************************************************

On Error GoTo ExpectedErrorRoutine

    'Assume AvaTax feature not configured
    mbAvaTaxEnabled = False

    mbAvaTaxEnabled = gbGetValidBoolean(moClass.moAppDB.Lookup("AvaTaxEnabled", "tavConfiguration", "CompanyID = " & gsQuoted(msCompanyID)))

    If mbAvaTaxEnabled Then
        On Error GoTo ExpectedErrorRoutine_AvaTaxObj
        Set moAvaTax = CreateObject("AVZDADL1.clsAVAvaTaxClass")
        moAvaTax.Init moClass, moClass.moSysSession, moClass.moAppDB, moClass.moAppDB, moClass.moFramework
    End If

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine:
'Expected Error:
'This is how we find out if AvaTax is not installed.
'The missing table (err 4050) will tell us not to fire the AvaTax code
'throughout the program.

    Select Case Err.Number
    
        Case 4050 ' SQL Object Not Found
            'tavConfiguration not found meaning AvaTax not installed.
            'No Error action required.

        Case Else
            'Unexpected Error reading tavConfiguration
            giSotaMsgBox Me, moClass.moSysSession, kmsgtavConfigurationError

    End Select

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine_AvaTaxObj:
'Expected Error:
'If the DB Avatax table was found, AvaTax is likely used for this Site.
'But when creating the local client object, the object was likely not installed
'This is not a good error, let the administrator know to check that AvaTax was properly implemented.

    giSotaMsgBox Me, moClass.moSysSession, kmsgAvaObjectError

    mbAvaTaxEnabled = False

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CheckAvaTax", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If
#End If

Public Function iGetTranLogStatus() As Integer
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                         'Repository Error Rig  {1.1.1.0.0}

    iGetTranLogStatus = miTranStatus

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                             'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "iGetTranLogStatus", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Function                                                    'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Function


