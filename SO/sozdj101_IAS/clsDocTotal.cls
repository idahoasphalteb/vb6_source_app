VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsFreight"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'**********************************************************************
'     Name: clsDocTotal
'     Desc: This class is used to manage the controls that make up the
'           document total, and calculate the document total
' Original: 01-29-2004 Jenny Wilson
'     Mods: mm/dd/yy XXX
'**********************************************************************
Private moClass As Object
Private moGrid As Object
Private moChildGrid As Object
Private moDMGrid As clsDmGrid
Private moDMChildGrid As clsDmGrid
Private moSTax As Object
Private mbTrackTaxes As Boolean
Private mlModule As Long

Private mlColChildSOLineDistKey As Long
Private mlColChildExtAmt As Long
Private mlColChildFrtAmt As Long
Private mlColChildQtyOrd As Long
Private mlColChildStatus As Long
    
Private mlColParentShipMethID As Long
Private mlColParentFrtAmt As Long


Public Sub Init(oClass As Object, bTrackTaxes As Boolean, oSTax As Object, oGrid As Object, oDmGrid As clsDmGrid, _
    oChildGrid As Object, oDMChildGrid As clsDmGrid, lModule As Long, lColChildSOLineDistKey As Long, _
    lColChildExtAmt As Long, lColChildQtyOrd As Long, lColChildFrtAmt As Long, lColChildStatus As Long, _
    lColParentShipMethID As Long, lColParentFrtAmt As Long)
    
    Set moClass = oClass
    Set moGrid = oGrid
    Set moDMGrid = oDmGrid
    Set moChildGrid = oChildGrid
    Set moDMChildGrid = oDMChildGrid
    Set moSTax = oSTax

    mlModule = lModule
    

    'set up the columns needs for freight calculations
    mlColChildSOLineDistKey = lColChildSOLineDistKey
    mlColChildExtAmt = lColChildExtAmt
    mlColChildFrtAmt = lColChildFrtAmt
    mlColChildQtyOrd = lColChildQtyOrd
    mlColChildStatus = lColChildStatus
    
    'set up the parent grid columns
    mlColParentShipMethID = lColParentShipMethID
    mlColParentFrtAmt = lColParentFrtAmt
    


End Sub

