VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#105.0#0"; "sotatbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#47.0#0"; "sotacalendar.ocx"
Begin VB.Form frmAppls 
   Caption         =   "Applications"
   ClientHeight    =   4950
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9720
   HelpContextID   =   17776152
   Icon            =   "Appls.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4950
   ScaleWidth      =   9720
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraTotal 
      Height          =   1335
      Left            =   120
      TabIndex        =   14
      Top             =   960
      Width           =   3255
      Begin VB.Frame linTotalLine 
         Height          =   45
         Left            =   1080
         TabIndex        =   18
         Top             =   840
         Width           =   1905
      End
      Begin NEWSOTALib.SOTACurrency curTranAmt 
         Height          =   285
         Left            =   1080
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   240
         WhatsThisHelpID =   17776153
         Width           =   1455
         _Version        =   65536
         _ExtentX        =   2566
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
         sBorder         =   0
         mask            =   "<HL> <ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "            0.00"
         sIntegralPlaces =   13
         sDecimalPlaces  =   2
      End
      Begin NEWSOTALib.SOTACurrency curPrePmts 
         Height          =   285
         Left            =   1200
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   600
         WhatsThisHelpID =   17776154
         Width           =   1335
         _Version        =   65536
         _ExtentX        =   2355
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
         sBorder         =   0
         mask            =   "<HL> <ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "            0.00"
         sIntegralPlaces =   13
         sDecimalPlaces  =   2
      End
      Begin NEWSOTALib.SOTACurrency curBalance 
         Height          =   285
         Left            =   1080
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   960
         WhatsThisHelpID =   17776155
         Width           =   1455
         _Version        =   65536
         _ExtentX        =   2566
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
         sBorder         =   0
         mask            =   "<HL> <ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "            0.00"
         sIntegralPlaces =   13
         sDecimalPlaces  =   2
      End
      Begin VB.Label lblPrePmts 
         AutoSize        =   -1  'True
         Caption         =   "Prepayments"
         Height          =   285
         Left            =   120
         TabIndex        =   22
         Top             =   600
         Width           =   915
      End
      Begin VB.Label lblBalance 
         AutoSize        =   -1  'True
         Caption         =   "Balance"
         Height          =   195
         Left            =   120
         TabIndex        =   21
         Top             =   975
         Width           =   585
      End
      Begin VB.Label lblCurrIDNC 
         AutoSize        =   -1  'True
         Caption         =   "USD"
         Height          =   240
         Index           =   0
         Left            =   2640
         TabIndex        =   17
         Top             =   265
         Width           =   345
      End
      Begin VB.Label lblOrderTotal 
         AutoSize        =   -1  'True
         Caption         =   "Order Total"
         Height          =   285
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   795
      End
   End
   Begin VB.Frame fraPmts 
      Caption         =   "Applications"
      Height          =   2415
      Left            =   120
      TabIndex        =   6
      Top             =   2400
      Width           =   9375
      Begin FPSpreadADO.fpSpread grdAppls 
         Height          =   1935
         Left            =   240
         TabIndex        =   7
         Top             =   300
         WhatsThisHelpID =   17776156
         Width           =   8895
         _Version        =   524288
         _ExtentX        =   15690
         _ExtentY        =   3413
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         SpreadDesigner  =   "Appls.frx":23D2
         AppearanceStyle =   0
      End
   End
   Begin VB.Frame fraShow 
      Caption         =   "&Show"
      Height          =   615
      Left            =   3480
      TabIndex        =   3
      Top             =   1680
      Width           =   3375
      Begin VB.OptionButton optShow 
         Caption         =   "Applications"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Value           =   -1  'True
         WhatsThisHelpID =   17776157
         Width           =   1215
      End
      Begin VB.OptionButton optShow 
         Caption         =   "Available Applications"
         Height          =   255
         Index           =   1
         Left            =   1440
         TabIndex        =   4
         Top             =   240
         WhatsThisHelpID =   17776158
         Width           =   1850
      End
   End
   Begin VB.Frame fraDisplay 
      Caption         =   "&Display"
      Height          =   615
      Left            =   6960
      TabIndex        =   0
      Top             =   1680
      Width           =   2415
      Begin VB.CheckBox chkCustID 
         Caption         =   "Cust ID"
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   240
         WhatsThisHelpID =   17776159
         Width           =   855
      End
      Begin VB.CheckBox chkCustName 
         Caption         =   "Cust Name"
         Height          =   255
         Left            =   1200
         TabIndex        =   1
         Top             =   240
         WhatsThisHelpID =   17776160
         Width           =   1100
      End
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtCustName 
      Height          =   285
      Left            =   5280
      TabIndex        =   8
      Top             =   480
      WhatsThisHelpID =   17776161
      Width           =   3255
      _Version        =   65536
      _ExtentX        =   5741
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      sBorder         =   0
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtCustID 
      Height          =   285
      Left            =   3840
      TabIndex        =   9
      Top             =   480
      WhatsThisHelpID =   17776162
      Width           =   1095
      _Version        =   65536
      _ExtentX        =   1931
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      sBorder         =   0
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtOrderNo 
      Height          =   285
      Left            =   600
      TabIndex        =   10
      Top             =   480
      WhatsThisHelpID =   17776163
      Width           =   1935
      _Version        =   65536
      _ExtentX        =   3413
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      sBorder         =   0
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   9720
      _ExtentX        =   17145
      _ExtentY        =   741
      Style           =   6
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   0
      TabIndex        =   23
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   24
      Top             =   0
      Visible         =   0   'False
      Width           =   1815
      Begin VB.OptionButton CustomOption 
         Caption         =   "Option"
         Enabled         =   0   'False
         Height          =   285
         Index           =   0
         Left            =   -30000
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         WhatsThisHelpID =   68
         Width           =   1245
      End
      Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
         Height          =   285
         Index           =   0
         Left            =   -30000
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         WhatsThisHelpID =   66
         Width           =   1245
         _Version        =   65536
         _ExtentX        =   2196
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
      End
      Begin NEWSOTALib.SOTANumber CustomNumber 
         Height          =   285
         Index           =   0
         Left            =   -30000
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         WhatsThisHelpID =   67
         Width           =   1245
         _Version        =   65536
         _ExtentX        =   2196
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "           0.00"
         sDecimalPlaces  =   2
      End
      Begin MSComCtl2.UpDown CustomSpin 
         Height          =   285
         Index           =   0
         Left            =   -30000
         TabIndex        =   28
         Top             =   0
         Visible         =   0   'False
         WhatsThisHelpID =   69
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
      End
      Begin VB.Label CustomLabel 
         Caption         =   "Label"
         Enabled         =   0   'False
         Height          =   285
         Index           =   0
         Left            =   -30000
         TabIndex        =   29
         Top             =   0
         Visible         =   0   'False
         Width           =   1245
      End
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   30
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   31
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   32
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   33
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   34
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin VB.Label lblOrderNo 
      Caption         =   "Order"
      Height          =   285
      Left            =   120
      TabIndex        =   12
      Top             =   510
      Width           =   495
   End
   Begin VB.Label lblCustID 
      Caption         =   "Customer"
      Height          =   255
      Left            =   2880
      TabIndex        =   11
      Top             =   510
      Width           =   735
   End
End
Attribute VB_Name = "frmAppls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************************
'     Name: frmAppls (Appls.frm)
'     Desc: Enter Sales Order Payments
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: JLW 12-09-2004
'     Mods:
'************************************************************************************
Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

    Private moContextMenu As clsContextMenu

    Private WithEvents moDMApplGrid      As clsDmGrid
Attribute moDMApplGrid.VB_VarHelpID = -1
    Private WithEvents moGMApplGrid      As clsGridMgr
Attribute moGMApplGrid.VB_VarHelpID = -1
    Private moDMform                     As clsDmForm
    
    Private moClass                      As Object
            
    Private msHomeCurrID                 As String
    Private msDocCurrID                  As String
    Private mdDocCurrExchRate            As Double
    Private mbShowHomeCurrCols           As Boolean
    Private miHomePriceDecPlaces         As Integer
    Private miHomeRoundMeth              As Integer
    Private miHomeRoundPrecision         As Integer
    Private mbUseMultiCurr               As Boolean
    Private miUseNationalAccts           As Integer
    Private miPmtByParent                As Integer
    Private mlSOKey                      As Long
    Private mlCustKey                    As Long
    Private mlParentCustKey              As Long
    Private mdAmtInvcd                   As Double
    Private mdPendAmtInvcd               As Double
    Private mbReadOnly                   As Boolean
    
    Private mdAmtToApplyHC               As Double
    Private mdAmtToApplyNC               As Double
    
    Private mbLeaveCell                  As Boolean
    
    'Minimum Form Size
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long
  
  'Grid column constants
    Private Const kColApplyPmt         As Long = 1
    Private Const kColTranDate         As Long = 2
    Private Const kColCustID           As Long = 3
    Private Const kColCustName         As Long = 4
    Private Const kColDocKey           As Long = 5
    Private Const kColRefNo            As Long = 6
    Private Const kColCurrIDHC         As Long = 7
    Private Const kColCurrIDNC         As Long = 8
    Private Const kColCustKey          As Long = 9
    Private Const kColAvailAmtHC       As Long = 10
    Private Const kColAvailAmtNC       As Long = 11
    Private Const kColToApplyHC        As Long = 12
    Private Const kColToApplyNC        As Long = 13
    Private Const kColCustPmtKey       As Long = 14
    Private Const kColCreditMemoKey    As Long = 15
    Private Const kColExchRate         As Long = 16
    Private Const kColDecPlacesNC      As Long = 17
    Private Const kColRoundMethNC      As Long = 18
    Private Const kColRoundPrecisionNC As Long = 19
    Private Const kColEnteredWithSO    As Long = 20
    Private Const kColSalesOrderPmtKey As Long = 21
    Private Const kColUpdateCounter    As Long = 22
       
    Private Const kMaxColumns As Integer = 22
           
    'Display options
    Private Const kOptShowAppl As Integer = 0
    Private Const kOptShowAll  As Integer = 1
    
    Const VBRIG_MODULE_ID_STRING = "Appls.FRM"


'************************************************************************
'   oClass contains the reference to the parent class object.  The form
'   needs this reference to use the public variables created within the
'   class object.
'************************************************************************
Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set oClass = moClass
    
    'Exit this property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = oNewClass

    'Exit this property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Private Function bIsValidAppliedAmt(lRow As Long, bIsHC)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim dToApplyAmtNC     As Double
Dim dToApplyAmtHC     As Double
Dim dCurrExchRate     As Double
Dim iRoundMeth        As Integer
Dim iRoundPrecision   As Integer
Dim iDecPlaces        As Integer
Dim dAvailAmtHC       As Double
Dim lSalesOrderPmtKey As Long
Dim dPrePmtsAmtNC     As Double

    bIsValidAppliedAmt = False
    
    dCurrExchRate = gdGetValidDbl(gsGridReadCell(grdAppls, lRow, kColExchRate))
    
    If bIsHC Then
        dToApplyAmtHC = gdGetValidDbl(gsGridReadCell(grdAppls, lRow, kColToApplyHC))
        
         'Convert the amount to natural currency
        iRoundMeth = giGetValidInt(gsGridReadCell(grdAppls, lRow, kColRoundMethNC))
        iRoundPrecision = giGetValidInt(gsGridReadCell(grdAppls, lRow, kColRoundPrecisionNC))
        iDecPlaces = giGetValidInt(gsGridReadCell(grdAppls, lRow, kColDecPlacesNC))
        
                        
        dToApplyAmtNC = gfRound((dToApplyAmtHC * (1 / dCurrExchRate)), iRoundMeth, iDecPlaces, iRoundPrecision)
    
    Else
        dToApplyAmtNC = gdGetValidDbl(gsGridReadCell(grdAppls, lRow, kColToApplyNC))
        
        'Convert to home currency
        dToApplyAmtHC = gfRound((dToApplyAmtNC * dCurrExchRate), miHomeRoundMeth, miHomePriceDecPlaces, miHomeRoundPrecision)
    End If
            
    dAvailAmtHC = gdGetValidDbl(gsGridReadCell(grdAppls, lRow, kColAvailAmtHC))
    
    If dToApplyAmtHC <= 0 Then
        'Application amount must be greater than zero
        giSotaMsgBox Me, moClass.moSysSession, kSOMsgApplAmtGreatThanZero
        Exit Function
    End If
    
    If dToApplyAmtHC > dAvailAmtHC Then
        'Amount can not exceed the available amount of the application.
        giSotaMsgBox Me, moClass.moSysSession, kSOmsgAmountExceedsAvail
        Exit Function
    End If
    
    'Get the total prepayments amount
    dPrePmtsAmtNC = curPrePmts.Amount - mdAmtToApplyNC + dToApplyAmtNC
        
    'Check to see if prepayments exceed the sales order amount and warn if so.
    If (curTranAmt.Amount - dPrePmtsAmtNC) < 0 Then
        'Warn - Prepayments exceed sales order amount.
        If giSotaMsgBox(Me, moClass.moSysSession, kSOMsgPmntExceedsSalesAmt) = kretCancel Then
            Exit Function
        End If
    End If
    
    If bIsHC Then
        gGridUpdateCell grdAppls, lRow, kColToApplyNC, CStr(dToApplyAmtNC)
    Else
        gGridUpdateCell grdAppls, lRow, kColToApplyHC, CStr(dToApplyAmtHC)
    End If
    
    'Update the total prepayments amount on the form
    curPrePmts.Amount = dPrePmtsAmtNC
    curBalance.Amount = curTranAmt.Amount - curPrePmts.Amount
     
    bIsValidAppliedAmt = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsValidAppliedAmt", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bValidateAppl(ByVal lRow As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

Dim lCustPmtKey          As Long
Dim lCreditMemoKey       As Long
Dim dAvailAmtHC          As Double
Dim dCurrExchRate        As Double
Dim dNewAvailAmtNC       As Double
Dim dNewAvailAmtHC       As Double
Dim dNewSOUnappliedAmtNC As Double
Dim dNewARPendApplsHC    As Double
Dim dNewARPendApplsNC    As Double
Dim dOrigAppliedAmtHC    As Double
Dim dOrigAppliedAmtNC    As Double
Dim dAppliedAmtHC        As Double
Dim dAppliedAmtNC        As Double
Dim lSalesOrderPmtKey    As Long
Dim sSQL                 As String
Dim iDocDecPlaces        As Integer
Dim rs                   As Object
Dim sWhere               As String

    bValidateAppl = False

    lCustPmtKey = glGetValidLong(gsGridReadCell(grdAppls, lRow, kColCustPmtKey))
    lCreditMemoKey = glGetValidLong(gsGridReadCell(grdAppls, lRow, kColCreditMemoKey))
    dAvailAmtHC = gdGetValidDbl(gsGridReadCell(grdAppls, lRow, kColAvailAmtHC))
    dOrigAppliedAmtHC = gdGetValidDbl(gsGridReadCell(grdAppls, lRow, kColToApplyHC))
            
    iDocDecPlaces = giGetValidInt(gsGridReadCell(grdAppls, lRow, kColDecPlacesNC))
    
    dCurrExchRate = gdGetValidDbl(gsGridReadCell(grdAppls, lRow, kColExchRate))
    
    If dOrigAppliedAmtHC <= 0 Then
        bValidateAppl = True
        Exit Function
    End If
        
    'Re-get the available amount - need to make sure that no one else has used the remaining unapplied
    'amount of the actual customer payment since it was first selected for this sales order.
    If lCustPmtKey > 0 Then
    
        sSQL = "SELECT UnappliedAmt, UnappliedAmtHC FROM tarCustPmt WHERE CustPmtKey = " & lCustPmtKey
        
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

        If Not rs.IsEmpty Then
            dNewAvailAmtNC = gdGetValidDbl(rs.Field("UnappliedAmt"))
            dNewAvailAmtHC = gdGetValidDbl(rs.Field("UnappliedAmtHC"))
        Else
            dNewAvailAmtNC = dOrigAppliedAmtNC
            dNewAvailAmtHC = dOrigAppliedAmtHC
        End If
        
        If Not rs Is Nothing Then
            Set rs = Nothing
        End If
                        
        'Add in any pending applications for sales orders so we don't double dip -- the UnappliedAmt is updated before the
        'batch is posted.
        sWhere = "SalesOrderPmtKey IS NOT NULL AND ApplyFromPmtKey = " & lCustPmtKey
        
        dNewARPendApplsNC = gdGetValidDbl(moClass.moAppDB.Lookup("SUM(PmtAmt)", "tarPendCustPmtAppl", sWhere))
        dNewAvailAmtNC = dNewAvailAmtNC + dNewARPendApplsNC
        
        dNewAvailAmtHC = dNewAvailAmtHC + gfRound(dNewARPendApplsNC * dCurrExchRate, miHomeRoundMeth, miHomePriceDecPlaces, miHomeRoundPrecision)
              
        'Subtract any sales order intended applications against this payment
        sSQL = "SELECT SUM(ROUND((AssignedAmt - AppliedAmt) * CONVERT(dec(38,20), " & dCurrExchRate & ")," & miHomePriceDecPlaces & ")) AmtHC, "
        sSQL = sSQL & "SUM(ROUND((AssignedAmt - AppliedAmt), " & iDocDecPlaces & ")) AmtNC "
        sSQL = sSQL & "FROM tarSalesOrderPmt WITH (NOLOCK) "
        sSQL = sSQL & "WHERE CustPmtKey = " & lCustPmtKey & " AND SOKey <> " & mlSOKey
                   
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

        If Not rs.IsEmpty Then
            dNewAvailAmtNC = dNewAvailAmtNC - gdGetValidDbl(rs.Field("AmtNC"))
            dNewAvailAmtHC = dNewAvailAmtHC - gdGetValidDbl(rs.Field("AmtHC"))
        End If
        
        If Not rs Is Nothing Then
            Set rs = Nothing
        End If
            
    Else 'Credit memo
        sSQL = "SELECT Balance, BalanceHC FROM tarInvoice WHERE InvcKey = " & lCreditMemoKey
        
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

        If Not rs.IsEmpty Then
            dNewAvailAmtNC = -gdGetValidDbl(rs.Field("Balance"))
            dNewAvailAmtHC = -gdGetValidDbl(rs.Field("BalanceHC"))
        Else
            dNewAvailAmtNC = 0
            dNewAvailAmtHC = 0
        End If
        
        If Not rs Is Nothing Then
            Set rs = Nothing
        End If
        
        'Subtract any pending applications not earmarked for sales orders
        sSQL = "SELECT SUM(PmtAmt) PmtAmt, SUM(ROUND(PmtAmt * CONVERT(dec(38,20), " & dCurrExchRate & "), " & miHomePriceDecPlaces & ")) PmtAmtHC FROM tarPendCustPmtAppl "
        sSQL = sSQL & "WHERE ApplyFromInvcKey = " & lCreditMemoKey & " AND SalesOrderPmtKey IS NULL"
        
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

        If Not rs.IsEmpty Then
            dNewAvailAmtNC = dNewAvailAmtNC - gdGetValidDbl(rs.Field("PmtAmt"))
            dNewAvailAmtHC = dNewAvailAmtHC - gdGetValidDbl(rs.Field("PmtAmtHC"))
        End If
        
        If Not rs Is Nothing Then
            Set rs = Nothing
        End If
        
        'Subtract any other sales order applications against this credit memo
        sSQL = "SELECT SUM(AssignedAmt-AppliedAmt) AmtNC, "
        sSQL = sSQL & "SUM(ROUND((AssignedAmt - AppliedAmt) * CONVERT(dec(38,20), " & dCurrExchRate & "), " & miHomePriceDecPlaces & ")) AmtHC "
        sSQL = sSQL & "FROM tarSalesOrderPmt WITH (NOLOCK) WHERE CreditMemoKey = " & lCreditMemoKey
        sSQL = sSQL & " AND SOKey <> " & mlSOKey
               
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

        If Not rs.IsEmpty Then
            dNewAvailAmtNC = dNewAvailAmtNC - gdGetValidDbl(rs.Field("AmtNC"))
            dNewAvailAmtHC = dNewAvailAmtHC - gdGetValidDbl(rs.Field("AmtHC"))
        End If
        
        If Not rs Is Nothing Then
            Set rs = Nothing
        End If
        
    End If

    'jlw - not sure why, but is necessary to gdGetValidDbl on credit memos -- amounts look equal
    'but are evaluating to not equal.
    If gdGetValidDbl(dNewAvailAmtHC) <> gdGetValidDbl(dAvailAmtHC) Then
        
        'update the grid with the newly calculated available amount
        gGridUpdateCell grdAppls, lRow, kColAvailAmtNC, CStr(dNewAvailAmtNC)
        
'        'Get the available amount in home currency and update to grid
'        If lCustPmtKey > 0 Then
'
'            dNewAvailAmtHC = gdGetValidDbl(moClass.moAppDB.Lookup("UnappliedAmtHC", "tarCustPmt", "CustPmtKey = " & _
'                lCustPmtKey))
'
'            dNewAvailAmtHC = dNewAvailAmtHC - (dNewSOUnappliedAmtNC * dCurrExchRate)
'
'        Else 'Credit memo
'
'            dNewAvailAmtHC = -gdGetValidDbl(moClass.moAppDB.Lookup("BalanceHC", "tarInvoice", "InvcKey = " & _
'                lCreditMemoKey))
'
'
'
'
'            sSql = "SELECT SUM(ROUND(PmtAmt * CONVERT(dec(38,20), tarInvoice.CurrExchRate), " & miHomePriceDecPlaces & ")) PmtAmtHC, "
'            sSql = sSql & "SUM(ROUND(PmtAmt, " & iDocDecPlaces & ")) PmtAmtNC "
'            sSql = sSql & "FROM tarPendCustPmtAppl WITH (NOLOCK) "
'            sSql = sSql & "JOIN tarInvoice WITH (NOLOCK) "
'            sSql = sSql & "ON tarPendCustPmtAppl.ApplyFromInvcKey = tarInvoice.InvcKey "
'            sSql = sSql & "WHERE tarPendCustPmtAppl.ApplyFromInvcKey = " & lCreditMemoKey
'
'            Set rs = moClass.moAppDB.OpenRecordset(sSql, kSnapshot, kOptionNone)
'
'            If Not rs.IsEmpty Then
'                dNewARPendApplsHC = -gdGetValidDbl(rs.Field("PmtAmtHC"))
'                dNewARPendApplsNC = -gdGetValidDbl(rs.Field("PmtAmtNC"))
'                Set rs = Nothing
'            Else
'                dNewARPendApplsHC = 0
'                dNewARPendApplsNC = 0
'            End If
'
'            dNewAvailAmtHC = dNewAvailAmtHC - dNewARPendApplsHC
'
'            dNewAvailAmtNC = dNewAvailAmtHC - dNewARPendApplsNC
'        End If

        gGridUpdateCell grdAppls, lRow, kColAvailAmtHC, CStr(dNewAvailAmtHC)
        
        If dNewAvailAmtHC <= 0 Then
            'Line {0}: This payment/memo is no longer available and may not be applied.
            If lCustPmtKey > 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kSOMsgPmntNotAvailable, lRow
            Else 'Credit memo
                giSotaMsgBox Me, moClass.moSysSession, kSOMsgMemoNotAvailable, lRow
            End If
                
            Exit Function
        Else
             If gfRound(dNewAvailAmtHC, miHomeRoundMeth, miHomePriceDecPlaces, miHomeRoundPrecision) < _
                        gfRound(dOrigAppliedAmtHC, miHomeRoundMeth, miHomePriceDecPlaces, miHomeRoundPrecision) Then
                'Line {0}: Available amount has changed due to recent activity and the application amount must be adjusted.
                giSotaMsgBox Me, moClass.moSysSession, kSOMsgApplAmtChanged, lRow
                Exit Function
                                
'                If curBalanceHC.Amount < dNewAvailAmtHC Then
'                    dAppliedAmtHC = curBalanceHC.Amount
'                    dAppliedAmtNC = gfRound(dAppliedAmtHC * dCurrExchRate, miHomeRoundMeth, miHomePriceDecPlaces, miHomeRoundPrecision)
'                Else
'                    dAppliedAmtHC = dNewAvailAmtHC
'                    dAppliedAmtNC = dNewAvailAmtNC
'                End If
'
'                gGridUpdateCell grdAppls, lRow, kColToApplyHC, CStr(dAppliedAmtHC)
'                gGridUpdateCell grdAppls, lRow, kColToApplyNC, CStr(dAppliedAmtNC)
'
            End If
        End If
        
'        'Update the total prepayments amount on the form
'        curPrePmtsHC.Amount = curPrePmtsHC.Amount - dOrigAppliedAmtHC + dAppliedAmtHC
'        curBalanceHC.Amount = curTranAmtHC.Amount - curPrePmtsHC.Amount
'
'        'Enable/disable the ToApply amount column on the grid
'        SetToApplyEnable lRow
    End If
          
    bValidateAppl = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bValidateAppl", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bIsValidApplyTo(lRow As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim dAvailAmtNC    As Double
Dim dAvailAmtHC    As Double
Dim dApplyAmtNC    As Double
Dim dApplyAmtHC    As Double
Dim lCustPmtKey    As Long
Dim lCreditMemoKey As Long
Dim dCurrExchRate  As Double

    bIsValidApplyTo = False
        
    lCustPmtKey = glGetValidLong(gsGridReadCell(grdAppls, lRow, kColCustPmtKey))
    lCreditMemoKey = glGetValidLong(gsGridReadCell(grdAppls, lRow, kColCreditMemoKey))
    dAvailAmtHC = gdGetValidDbl(gsGridReadCell(grdAppls, lRow, kColAvailAmtHC))
    dAvailAmtNC = gdGetValidDbl(gsGridReadCell(grdAppls, lRow, kColAvailAmtNC))
    
    dCurrExchRate = gdGetValidDbl(gsGridReadCell(grdAppls, lRow, kColExchRate))
    
    If giGetValidInt(gsGridReadCell(grdAppls, lRow, kColApplyPmt)) = vbChecked Then

        'Update the the lesser of the available amount and the balance to the ToApply column
        If curBalance.Amount < dAvailAmtNC Then
            dApplyAmtNC = curBalance.Amount
            dApplyAmtHC = gfRound(dApplyAmtNC * (dCurrExchRate / 1), miHomeRoundMeth, miHomePriceDecPlaces, miHomeRoundPrecision)
        Else
            dApplyAmtNC = dAvailAmtNC
            dApplyAmtHC = dAvailAmtHC
        End If
        
        If dApplyAmtNC <= 0 Then
            dApplyAmtNC = 0
            dApplyAmtHC = 0
        End If
        
        gGridUpdateCell grdAppls, lRow, kColToApplyHC, CStr(dApplyAmtHC)
        gGridUpdateCell grdAppls, lRow, kColToApplyNC, CStr(dApplyAmtNC)
                                        
        'Update the total prepayments amount on the form
        curPrePmts.Amount = curPrePmts.Amount + dApplyAmtNC
        curBalance.Amount = curTranAmt.Amount - curPrePmts.Amount

    Else 'Unapplying
        'Update the total prepayments amount on the form
        curPrePmts.Amount = curPrePmts.Amount - gdGetValidDbl(gsGridReadCell(grdAppls, lRow, kColToApplyNC))
        curBalance.Amount = curTranAmt.Amount - curPrePmts.Amount
        
        'clear the applied amount
        gGridUpdateCell grdAppls, lRow, kColToApplyNC, Empty
        gGridUpdateCell grdAppls, lRow, kColToApplyHC, Empty
           
    End If
    
    'Enable/disable the ToApply amount column on the grid
    SetToApplyEnable lRow
        
    bIsValidApplyTo = True
    
           
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsValidApplyTo", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


'************************************************************************
'   Description:
'       Process all toolbar clicks (as well as HotKey shortcuts to
'       toolbar buttons).
'   Param:
'       sKey -  Token returned from toolbar.  Indicates what function
'               is to be executed.
'   Returns:
'************************************************************************
Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If
Dim lRow As Long

    'VB 5 does not automatically fire LostFocus event when pressing toolbar.
    Me.SetFocus
    DoEvents

    Select Case sKey
        Case kTbFinishExit
            'The Finish button was pressed by the user.
            
            If bProcessSave Then
                Me.Hide
            End If
                                
        Case kTbCancelExit
            
            Me.Hide
                               
        Case kTbHelp
            'The Help button was pressed by the user.
            gDisplayFormLevelHelp Me
            
        Case kTbCurrency
        
            ShowCurrencyColumns
                           
        Case Else
            'Error processing
            'Give an error message to the user.
            'Unexpected Operation Value: {0}
            'ResetMouse
            'giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation, _
                         CVar(sKey)
    End Select

    'ResetMouse
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bProcessSave()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lRow As Long

    bProcessSave = False
 
    For lRow = 1 To grdAppls.DataRowCnt
        If Not bValidateAppl(lRow) Then
            Exit Function
        End If
    Next lRow
    
    bProcessSave = bSaveAppls
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bProcessSave", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function iConfirmUnload() As Integer
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iRetVal As Integer

    If mbReadOnly = kSecLevelDisplayOnly Then
        iConfirmUnload = kDmSuccess
        Exit Function
    End If
    
    If moDMApplGrid Is Nothing Then
        iConfirmUnload = kDmSuccess
        Exit Function
    End If

    
    If moDMApplGrid.IsDirty Then
        
        iRetVal = giSotaMsgBox(Me, moClass.moSysSession, kmsgDMSaveChangesGen)
                
        If iRetVal = kretYes Then
                     
            iConfirmUnload = IIf(bProcessSave, kDmSuccess, kDmFailure)

        ElseIf iRetVal = kretCancel Then

            iConfirmUnload = kDmFailure
            Exit Function
        ElseIf iRetVal = kretNo Then
            
            iConfirmUnload = kDmSuccess
            Exit Function
        End If
    End If

    iConfirmUnload = kDmSuccess

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "iConfirmUnload", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

'Private Sub ResetMouse()
''+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
'#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
'    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
'#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
''+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
''***********************************************************************
''    Desc: Resets Mouse to Previous Value.
''   Parms: N/A
'' Returns: N/A
''***********************************************************************
'    Me.MousePointer = miMousePointer
'
'    'Exit this subroutine
''+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
'    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
'VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
'        gSetSotaErr Err, sMyName, "ResetMouse", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
'        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
''+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'End Sub

'************************************************************************
'   Description:
'       Returns Form Name for Debugging Information.
'   Param:
'       <none>
'   Returns:
'       Form Name
'************************************************************************
Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function

Private Sub ShowCurrencyColumns()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

Dim sSQL As String

       
    If mbShowHomeCurrCols Then
        
        gGridShowColumn grdAppls, kColCurrIDHC
         
        If mdAmtInvcd = 0 And mdPendAmtInvcd = 0 Then
            gGridShowColumn grdAppls, kColAvailAmtHC
        Else
            gGridHideColumn grdAppls, kColAvailAmtHC
        End If
        
        gGridShowColumn grdAppls, kColToApplyHC
        
       
        gGridHideColumn grdAppls, kColCurrIDNC
        gGridHideColumn grdAppls, kColAvailAmtNC
        gGridHideColumn grdAppls, kColToApplyNC
         
        mbShowHomeCurrCols = False
     Else
     
        gGridShowColumn grdAppls, kColCurrIDNC
        
        If mdAmtInvcd = 0 And mdPendAmtInvcd = 0 Then
            gGridShowColumn grdAppls, kColAvailAmtNC
        Else
            gGridHideColumn grdAppls, kColAvailAmtNC
        End If
        
        gGridShowColumn grdAppls, kColToApplyNC
        
        
        gGridHideColumn grdAppls, kColCurrIDHC
        gGridHideColumn grdAppls, kColAvailAmtHC
        gGridHideColumn grdAppls, kColToApplyHC
                
        mbShowHomeCurrCols = True
     End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ShowCurrencyColumns", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub BindToolBar()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    With tbrMain
            
        .RemoveButton kTbHelp
        .RemoveButton kTbPrint
        
        If mbUseMultiCurr Then
            .AddButton kTbCurrency, 5
        End If
        
        .AddButton kTbHelp
        
    End With
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindToolBar", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function CMAppendContextMenu(ctl As Object, hmenu As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    If ctl Is grdAppls Then
    
        If glGetValidLong(gsGridReadCell(grdAppls, grdAppls.ActiveRow, kColCustPmtKey)) > 0 Then
        'This is a payment
            moContextMenu.AppendDrillMenu hmenu, "Payment", ctl, Me, True, kEntTypeARCustPayments
        Else
            moContextMenu.AppendDrillMenu hmenu, "CashReceipt", ctl, Me, True, kEntTypeARInvoice
        End If
   
    End If

Exit Function

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CMAppendContextMenu", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Function ETWhereClause(ByVal ctl As Object, _
                              ByVal taskID As Long, _
                              ByVal EntityType As Integer, _
                              ByVal naturalKeyCols As String, _
                              ByVal TaskGroup As EntityTask_Groups) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*******************************************************************************
'   Description: OPTIONAL
'                Entity Task Where Clause. This routine allows the application
'                to specify the where clause to be used against the
'                Host Data View for the specified entity.
'
'                If a where clause is not specified, the where clause will be
'                built using the Natural Key Columns specified for the entity.
'
'                Surrogate Keys as natural keys and other criteria make it
'                impossible without supporting metadata to get these values from
'                the application.
'
'   Parameters:
'                ctl <in> - Control that selected the task
'                taskID <in> - Task ID for the Entity Task
'                entityType <in> - Entity Type bound to the control that selected the task
'                naturalKeyCols <in> - Key columns used to specify where clause (FYI)
'
'   Returns:
'                A specified where clause WITHOUT the 'WHERE' verb. (i.e. "1 = 2")
'*******************************************************************************

    ETWhereClause = ""
    
    Select Case EntityType
    
        Case kEntTypeARInvoice
             ETWhereClause = "InvcKey = " & gsGridReadCell(grdAppls, grdAppls.ActiveRow, kColCreditMemoKey)
        Case kEntTypeARCustPayments
            ETWhereClause = "CustPmtKey = " & gsGridReadCell(grdAppls, grdAppls.ActiveRow, kColCustPmtKey)
    
    End Select
    
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ETWhereClause", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function


Private Sub Form_Load()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim uCurrInfo As CurrencyInfo


    moClass.lUIActive = kChildObjectInactive

    'Get session defaults.
    mbEnterAsTab = moClass.moSysSession.EnterAsTab       'Enter Key like Tab Key
    msHomeCurrID = moClass.moSysSession.CurrencyID

    

    'Set the form's initial height and width.
    miOldFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormHeight = miOldFormHeight
    miMinFormWidth = miOldFormWidth

    'Initial defaults - if user changes, new value will be used as next default this session.
    chkCustID.Value = vbChecked
    
    'Initially default option to display all available applications
    optShow(kOptShowAll).Value = True
    
       
    FormatGrid
    BindToolBar
           
    BindGrid                                'Bind the application grid to the data manager
    BindGM                                  'Bind the application grid manager
    BindContextMenu                         'Bind the Context Menu.
    
    
        
           
    'mbLoadSuccess = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Public Sub Init(ByVal oClass As Object, ByVal iHomePriceDecPlaces As Integer, ByVal iHomeRoundMeth, _
    ByVal iHomeRoundPrecision, ByVal bUseMultiCurr As Boolean, iUseNationalAccts As Integer, _
    sHomeCurrID As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Set moClass = oClass
    
    miHomePriceDecPlaces = iHomePriceDecPlaces
    miHomeRoundMeth = iHomeRoundMeth
    miHomeRoundPrecision = iHomeRoundPrecision
    
    mbUseMultiCurr = bUseMultiCurr
    miUseNationalAccts = iUseNationalAccts
    msHomeCurrID = sHomeCurrID
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Init", VBRIG_IS_FORM                                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub chkCustID_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkCustID, True
    #End If
'+++ End Customizer Code Push +++

    If chkCustID.Value = vbChecked Then
        gGridShowColumn grdAppls, kColCustID
    Else
        gGridHideColumn grdAppls, kColCustID
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "chkCustID_Click", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub chkCustName_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkCustName, True
    #End If
'+++ End Customizer Code Push +++

    If chkCustName.Value = vbChecked Then
        gGridShowColumn grdAppls, kColCustName
    Else
        gGridHideColumn grdAppls, kColCustName
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "chkCustName_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'    mbCancelShutDown = False
'    mbLoadSuccess = False

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True.
'   Parameters:
'           KeyCode, Shift - see VB reference Manual.
'
'   NOTE: THIS ROUTINE SHOULD CALL A GLOBAL SUBROUTINE TO HANDLE ALL THE
'         AVAILABLE HOTKEYS.
'********************************************************************
    'Determine if the user used the form-level keys and process them
    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            'Process the form-level key pressed
            gProcessFKeys Me, KeyCode, Shift
    End Select
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
'************************************************************************
'   Description:
'       Process Key Presses on the form.  NOTE: Key Preview of the form
'       should be set to True.
'   Param:
'       KeyAscii -  ASCII Key Code of Key Pressed.
'   Returns:
'************************************************************************
Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        
        Case Else
            'Other KeyAscii routines.
    End Select
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub PopulateGrid()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lRetVal           As Long
Dim iShowHomeCurrCols As Integer
Dim lRow              As Long
Dim rs                As Object
Dim sSQL              As String
Dim dCurrExchRate     As Double
Dim dToApplyAmtNC     As Double
Dim dToApplyAmtHC     As Double
Dim lCreditMemoKey    As Long
Dim lCustPmtKey       As Long
Dim dApplAmt          As Long
Dim dEarmarkAmtHC     As Long
Dim dAvailAmtHC       As Long

    With moClass.moAppDB
        .SetInParam mlSOKey
        .SetInParam mlCustKey
        .SetInParam miUseNationalAccts
        .SetInParam miPmtByParent
        .SetInParam mlParentCustKey
        .SetInParam msHomeCurrID
        .SetInParam miHomePriceDecPlaces
        .SetInParam msDocCurrID
        .SetInParam mdDocCurrExchRate
        .SetOutParam iShowHomeCurrCols 'If all applications are in the SO currency, show doc currency, else show application's currency
        .SetOutParam lRetVal
        .ExecuteSP "spsoPopulateApplsTempTable"
        iShowHomeCurrCols = giGetValidInt(.GetOutParam(10))
        lRetVal = glGetValidLong(.GetOutParam(11))
        .ReleaseParams
    End With
    
    mbShowHomeCurrCols = IIf(iShowHomeCurrCols = 1, True, False)

    moDMApplGrid.Refresh
    
    ResetGrid
             
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "PopulateGrid", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ResetGrid()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iDecPlacesNC      As Integer
Dim sCurrIDNC         As String
Dim lRow              As Long
    'Set the decimal places on the NC columns based on the natural currency of each payment
    For lRow = 1 To grdAppls.DataRowCnt

        iDecPlacesNC = giGetValidInt(gsGridReadCell(grdAppls, lRow, kColDecPlacesNC))

        gGridSetColumnType grdAppls, kColAvailAmtNC, SS_CELL_TYPE_FLOAT, iDecPlacesNC, 12
        gGridSetColumnType grdAppls, kColToApplyNC, SS_CELL_TYPE_FLOAT, iDecPlacesNC, 12
       
        With grdAppls
            .Row = lRow
            .Col = kColApplyPmt
            .Action = ActionActiveCell
        End With

        SetToApplyEnable lRow

        sCurrIDNC = gsGridReadCell(grdAppls, lRow, kColCurrIDNC)
        
    Next lRow
    
    With grdAppls
        .Row = 1
        .Col = kColApplyPmt
        .Action = ActionActiveCell
    End With

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ResetGrid", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetToApplyEnable(lRow As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    If (giGetValidInt(gsGridReadCell(grdAppls, lRow, kColApplyPmt)) = 1) And mdAmtInvcd = 0 _
        And mdPendAmtInvcd = 0 And (Not bOverrideSecEvent("SOAPPLYPMNTS", moClass, False)) _
        And (Not mbReadOnly) Then
        
        gGridUnlockCell grdAppls, kColToApplyHC, lRow
        gGridUnlockCell grdAppls, kColToApplyNC, lRow
    Else
        gGridLockCell grdAppls, kColToApplyHC, lRow
        gGridLockCell grdAppls, kColToApplyNC, lRow
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetToApplyEnable", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub BindGM()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'Bind the payments grid manager
    Set moGMApplGrid = New clsGridMgr

    With moGMApplGrid
        Set .Grid = grdAppls
        Set .Form = frmAppls
        Set .DM = moDMApplGrid

        .GridType = kGridDataSheet
       

        .Init
    End With

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindGM", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub BindGrid()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'Bind the payments grid

    Set moDMApplGrid = New clsDmGrid

    With moDMApplGrid
        Set .Form = frmAppls
        Set .Session = moClass.moSysSession
        Set .Grid = grdAppls
        Set .Database = moClass.moAppDB
        .Table = "#tarSalesOrderPmt"
        .UniqueKey = "DocKey"

        .OrderBy = "ApplyPmt DESC, TranDate"
        .NoAppend = True

        .BindColumn "AvailAmtNC", kColAvailAmtNC, SQL_DECIMAL
        .BindColumn "AvailAmtHC", kColAvailAmtHC, SQL_DECIMAL
        .BindColumn "ApplyPmt", kColApplyPmt, SQL_DECIMAL
        .BindColumn "CreditMemoKey", kColCreditMemoKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "CurrExchRate", kColExchRate, SQL_FLOAT
        .BindColumn "CurrIDHC", kColCurrIDHC, SQL_VARCHAR
        .BindColumn "CurrIDNC", kColCurrIDNC, SQL_VARCHAR
        .BindColumn "CustKey", kColCustKey, SQL_INTEGER
        .BindColumn "CustPmtKey", kColCustPmtKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "DocKey", kColDocKey, SQL_INTEGER
        .BindColumn "DecPlacesNC", kColDecPlacesNC, SQL_SMALLINT
        .BindColumn "EnteredWithSO", kColEnteredWithSO, SQL_SMALLINT
        .BindColumn "RefNo", kColRefNo, SQL_VARCHAR
        .BindColumn "RoundMethNC", kColRoundMethNC, SQL_SMALLINT
        .BindColumn "RoundPrecisionNC", kColRoundPrecisionNC, SQL_SMALLINT
        .BindColumn "ToApplyNC", kColToApplyNC, SQL_DECIMAL
        .BindColumn "ToApplyHC", kColToApplyHC, SQL_DECIMAL
        .BindColumn "TranDate", kColTranDate, SQL_DATE
        .BindColumn "SalesOrderPmtKey", kColSalesOrderPmtKey, SQL_INTEGER, , kDmSetNull
        'Do not bind UpdateCounter!!!
                      
        .LinkSource "tarCustomer", "#tarSalesOrderPmt.CustKey = tarCustomer.CustKey", kDmJoin, LeftOuter
        .Link kColCustID, "CustID"
        .Link kColCustName, "CustName"

        .Init
    End With

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindGrid", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub FormatGrid()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lCol As Long

    'Set Default Properties for the grid.
    If mbReadOnly Then
        gGridSetProperties grdAppls, kMaxColumns, kGridInquiryDD 'kGridDataSheetNoAppend
    Else
        gGridSetProperties grdAppls, kMaxColumns, kGridDataSheet
    End If


    'Set some additional grid properties (or overwrite the standard ones).
    With grdAppls
        .AllowMultiBlocks = False
        .ScrollBars = ScrollBarsBoth
        .UserResizeCol = UserResizeOn
        .TypeFloatSeparator = True
        .UnitType = UnitTypeNormal
        .EditModePermanent = True
        .RowHeadersShow = True
        .RowHeaderDisplay = DispNumbers
   '    .ButtonDrawMode = ButtonDrawModeCurrentRow
   '     .EditEnterAction = EditEnterActionNone

    End With


    'set the default colors
    gGridSetColors grdAppls

    gGridSetMaxRows grdAppls, 1


    'Setup Grid headers
    gGridSetHeader grdAppls, kColApplyPmt, "Apply"
    gGridSetHeader grdAppls, kColTranDate, "Tran Date"
    gGridSetHeader grdAppls, kColRefNo, "Reference"
    gGridSetHeader grdAppls, kColCustID, "Cust ID"
    gGridSetHeader grdAppls, kColCustName, "Customer Name"
    gGridSetHeader grdAppls, kColEnteredWithSO, "Entered With SO"


    'Determine later which of these columns to show/hide based on currency toggle in ShowForm
    gGridSetHeader grdAppls, kColAvailAmtHC, "Available Amt HC"
    gGridSetHeader grdAppls, kColAvailAmtNC, "Available Amt NC"
    gGridSetHeader grdAppls, kColToApplyHC, "Assigned Amt HC"
    gGridSetHeader grdAppls, kColToApplyNC, "Assigned Amt NC"
    gGridSetHeader grdAppls, kColCurrIDHC, "Curr HC"
    gGridSetHeader grdAppls, kColCurrIDNC, "Curr NC"

    'Hidden columns (for debugging)
    gGridSetHeader grdAppls, kColCustKey, "CustKey"
    gGridSetHeader grdAppls, kColCustPmtKey, "CustPmtKey"
    gGridSetHeader grdAppls, kColCreditMemoKey, "CreditMemoKey"
    gGridSetHeader grdAppls, kColDocKey, "DocKey"
    gGridSetHeader grdAppls, kColExchRate, "Exchange Rate"
    gGridSetHeader grdAppls, kColDecPlacesNC, "NC Dec Places"
    gGridSetHeader grdAppls, kColRoundMethNC, "NC Round Meth"
    gGridSetHeader grdAppls, kColRoundPrecisionNC, "NC Round Prec"
    gGridSetHeader grdAppls, kColSalesOrderPmtKey, "SalesOrderPmtKey"
    gGridSetHeader grdAppls, kColUpdateCounter, "UpdateCounter"
    

    'Set grid column widths
    gGridSetColumnWidth grdAppls, kColApplyPmt, 4
    gGridSetColumnWidth grdAppls, kColTranDate, 8
    gGridSetColumnWidth grdAppls, kColRefNo, 12
    gGridSetColumnWidth grdAppls, kColCurrIDHC, 4
    gGridSetColumnWidth grdAppls, kColCurrIDNC, 4
    gGridSetColumnWidth grdAppls, kColCustID, 8
    gGridSetColumnWidth grdAppls, kColCustName, 12
    gGridSetColumnWidth grdAppls, kColAvailAmtHC, 12
    gGridSetColumnWidth grdAppls, kColAvailAmtNC, 12
    gGridSetColumnWidth grdAppls, kColToApplyHC, 12
    gGridSetColumnWidth grdAppls, kColToApplyNC, 12
    gGridSetColumnWidth grdAppls, kColEnteredWithSO, 12
    
    'Set display column types
    gGridSetColumnType grdAppls, kColApplyPmt, SS_CELL_TYPE_CHECKBOX
    gGridSetColumnType grdAppls, kColEnteredWithSO, SS_CELL_TYPE_CHECKBOX
    gGridSetColumnType grdAppls, kColTranDate, SS_CELL_TYPE_DATE
    gGridSetColumnType grdAppls, kColRefNo, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdAppls, kColCurrIDHC, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdAppls, kColCurrIDNC, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdAppls, kColCustID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdAppls, kColCustName, SS_CELL_TYPE_EDIT

    'Set the home currency decimals places.  Payment currency columns will be set after
    'populating the temp table for available payments.
    gGridSetColumnType grdAppls, kColAvailAmtHC, SS_CELL_TYPE_FLOAT, miHomePriceDecPlaces, 12
    gGridSetColumnType grdAppls, kColToApplyHC, SS_CELL_TYPE_FLOAT, miHomePriceDecPlaces, 12
    
    'Locked columns
    gGridLockColumn grdAppls, kColTranDate
    gGridLockColumn grdAppls, kColRefNo
    gGridLockColumn grdAppls, kColCustID
    gGridLockColumn grdAppls, kColCustName
    gGridLockColumn grdAppls, kColCurrIDHC
    gGridLockColumn grdAppls, kColCurrIDNC
    gGridLockColumn grdAppls, kColAvailAmtHC
    gGridLockColumn grdAppls, kColAvailAmtNC
    gGridLockColumn grdAppls, kColEnteredWithSO
    
    'Hidden columns
    gGridHideColumn grdAppls, kColCustKey
    gGridHideColumn grdAppls, kColCustPmtKey
    gGridHideColumn grdAppls, kColCreditMemoKey
    gGridHideColumn grdAppls, kColDocKey
    gGridHideColumn grdAppls, kColExchRate
    gGridHideColumn grdAppls, kColDecPlacesNC
    gGridHideColumn grdAppls, kColSalesOrderPmtKey
    gGridHideColumn grdAppls, kColRoundMethNC
    gGridHideColumn grdAppls, kColRoundPrecisionNC
    gGridHideColumn grdAppls, kColUpdateCounter
   
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormatGrid", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Public Sub ShowForm(ByVal iPmtByParent As Integer, ByVal lSOKey As Long, ByVal lCustKey As Long, _
    ByVal sTranID As String, ByVal sCustID As String, ByVal sCustName As String, ByVal lParentCustKey As Long, _
    ByVal dPendAmtInvcd As Double, ByVal dAmtInvcd As Double, ByVal sDocCurrID As String, ByVal dDocCurrExchRate As Double, _
    ByVal bReadOnly As Boolean, ByVal dTranAmtHC As Double, ByVal dTranAmtNC As Double, ByVal dTotalPrePmntsNC As Double)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim dBalanceNC As Double
Dim ix As Integer
Dim uCurrInfo As CurrencyInfo
            
    miPmtByParent = iPmtByParent
    mlSOKey = lSOKey
    mlCustKey = lCustKey
    mlParentCustKey = lParentCustKey
    mdAmtInvcd = dAmtInvcd
    msDocCurrID = sDocCurrID
    mdDocCurrExchRate = dDocCurrExchRate
    mdPendAmtInvcd = dPendAmtInvcd
    
    mbReadOnly = (bReadOnly Or mdAmtInvcd <> 0 Or mdPendAmtInvcd <> 0)
       
    txtOrderNo.Text = sTranID
    txtCustID.Text = sCustID
    txtCustName.Text = sCustName
    
    'Set document currency controls
    gbSetCurrCtls moClass, sDocCurrID, uCurrInfo, curTranAmt, curPrePmts, curBalance
    
    curTranAmt.Amount = dTranAmtNC
    curPrePmts.Amount = dTotalPrePmntsNC
    
    lblCurrIDNC(0).Caption = msDocCurrID

    'Displaying the currency ID only when the Multicurrency is enabled
    If mbUseMultiCurr Then
        lblCurrIDNC(0).Visible = True
    Else
        lblCurrIDNC(0).Visible = False
    End If

'    'Assigning the caption using the FOR loop, for the future use, if required
'    For ix = 0 To lblCurrIDHC.Count - 1
'        lblCurrIDHC(ix).Caption = moClass.moSysSession.CurrencyID
'    Next
    
    dBalanceNC = dTranAmtNC - dTotalPrePmntsNC
    curBalance.Amount = IIf((dBalanceNC) < 0, 0, dBalanceNC)
                
    If miUseNationalAccts = 1 And miPmtByParent = 1 Then
        fraDisplay.Visible = True
    Else
        fraDisplay.Visible = False
    End If
    
    If dPendAmtInvcd = 0 And dAmtInvcd = 0 Then
        linTotalLine.Visible = True
        curBalance.Visible = True
        lblBalance.Visible = True
    Else
        linTotalLine.Visible = False
        curBalance.Visible = False
        lblBalance.Visible = False
    End If

    SetGridForNatlAcct

    PopulateGrid
    
    'Show appropriate currency columns
    ShowCurrencyColumns
    
    'Disable the Apply checkbox if the user does not have rights
    If bOverrideSecEvent("SOAPPLYPMT", moClass, False) And Not mbReadOnly Then
        gGridUnlockColumn grdAppls, kColApplyPmt
    Else
        gGridLockColumn grdAppls, kColApplyPmt
    End If
    
    moDMApplGrid.SetDirty False, True

    Me.Show vbModal
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ShowForm", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub SetGridForNatlAcct()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    If miUseNationalAccts = 1 And miPmtByParent = 1 Then

        If chkCustID.Value = vbChecked Then
            gGridShowColumn grdAppls, kColCustID
        Else
            gGridHideColumn grdAppls, kColCustID
        End If

        If chkCustName.Value = vbChecked Then
            gGridShowColumn grdAppls, kColCustName
        Else
            gGridHideColumn grdAppls, kColCustName
        End If
    Else
        gGridHideColumn grdAppls, kColCustID
        gGridHideColumn grdAppls, kColCustName
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetGridForNatlAcct", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

'************************************************************************
'   Description:
'       If form is dirty, prompt for save.  Handle cancel of the
'       shutdown or process a normal shutdown.
'   Param:
'       Cancel -        set to True to cancel the form shutdown
'       UnloadMode -    flag indicating type of shutdown requested
'   Returns:
'************************************************************************

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iRetVal As Integer
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If

    If UnloadMode <> vbFormCode Then
    
        Cancel = 1
        
        If iConfirmUnload = kDmSuccess Then
            Me.Hide
        End If
        
    Else
        PerformCleanShutDown
    End If

    Exit Sub


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Me.WindowState = vbNormal Or Me.WindowState = vbMaximized Then
        'Resize Height
        gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, grdAppls

        'Resize Width
        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, grdAppls
        
        miOldFormHeight = Me.Height
        miOldFormWidth = Me.Width

    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    If Not moClass Is Nothing Then
        Set moClass = Nothing
    End If
     
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'***************************************************
'  Instantiate the Context Menu Class.
'***************************************************
    Set moContextMenu = New clsContextMenu  'Instantiate Context Menu Class

    With moContextMenu
        Set .Form = frmAppls
        
         'Grid
        .Bind "*DYNAMIC", grdAppls.hWnd
        
        .Init
    End With

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z"
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the SOTA identifier.
'************************************************************************
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    WhatHelpPrefix = "SOZ"

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Private Sub grdAppls_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim bValid As Boolean
Dim iOldValue As Integer
Dim iNewValue As Integer

    If Col = grdAppls.ActiveCol Then
        Select Case Col
            Case kColApplyPmt
                iNewValue = giGetValidInt(gsGridReadCell(grdAppls, Row, Col))
                bValid = bIsValidApplyTo(Row)
                If Not bValid Then
                    If iNewValue = vbChecked Then
                        iOldValue = vbUnchecked
                    Else
                        iOldValue = vbChecked
                    End If
                    gGridUpdateCell grdAppls, Row, Col, gsGetValidStr(iOldValue)
                Else
                    'Save the original values in order to restore if user changes amount and validation fails
                    mdAmtToApplyHC = gdGetValidDbl(gsGridReadCell(grdAppls, Row, kColToApplyHC))
                    mdAmtToApplyNC = gdGetValidDbl(gsGridReadCell(grdAppls, Row, kColToApplyNC))
                     'Row dirty not always set after checking the box
                    moDMApplGrid.SetRowDirty Row
                End If
        End Select
    End If
    
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdAppls_ButtonClicked", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub grdAppls_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim bIsHC             As Boolean

    moGMApplGrid.Grid_Change Col, Row
    
    Select Case Col
        Case kColToApplyHC
            bIsHC = True
            mbLeaveCell = bIsValidAppliedAmt(Row, bIsHC)
            If Not mbLeaveCell Then
                gGridUpdateCell grdAppls, Row, Col, gsGetValidStr(mdAmtToApplyHC)
            Else
                mdAmtToApplyHC = gdGetValidDbl(gsGridReadCell(grdAppls, Row, kColToApplyHC))
                mdAmtToApplyNC = gdGetValidDbl(gsGridReadCell(grdAppls, Row, kColToApplyNC))
                moDMApplGrid.SetRowDirty Row
            End If
        Case kColToApplyNC
            bIsHC = False
            mbLeaveCell = bIsValidAppliedAmt(Row, bIsHC)
            If Not mbLeaveCell Then
                gGridUpdateCell grdAppls, Row, Col, gsGetValidStr(mdAmtToApplyNC)
            Else
                mdAmtToApplyNC = gdGetValidDbl(gsGridReadCell(grdAppls, Row, kColToApplyNC))
                mdAmtToApplyHC = gdGetValidDbl(gsGridReadCell(grdAppls, Row, kColToApplyHC))
                moDMApplGrid.SetRowDirty Row
            End If
    End Select
    
   
            
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdAppls_Change", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdAppls_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    ' We do not want to be in here if we do not have focus
    If Not (gbGotFocus(Me, grdAppls)) And Not (grdAppls.EditModePermanent) Then Exit Sub

    moGMApplGrid.Grid_Click Col, Row

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdAppls_Click", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdAppls_ColWidthChange(ByVal Col1 As Long, ByVal Col2 As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    moGMApplGrid.Grid_ColWidthChange Col1

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdAppls_ColWidthChange", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdAppls_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

     moGMApplGrid.Grid_EditMode Col, Row, Mode, ChangeMade

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdAppls_EditMode", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdAppls_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    moGMApplGrid.Grid_KeyDown KeyCode, Shift

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdAppls_KeyDown", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdAppls_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

'    If moDMform.State = kDmStateNone Then
'        Exit Sub
'    End If

    If Not mbLeaveCell Then
        Cancel = True
        mbLeaveCell = True
        Exit Sub
    End If

    Cancel = Not moGMApplGrid.Grid_LeaveCell(Col, Row, NewCol, NewRow)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdAppls_LeaveCell", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdAppls_LeaveRow(ByVal Row As Long, ByVal RowWasLast As Boolean, ByVal RowChanged As Boolean, ByVal AllCellsHaveData As Boolean, ByVal NewRow As Long, ByVal NewRowIsLast As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

     moGMApplGrid.Grid_LeaveRow Row, NewRow

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdAppls_LeaveRow", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdAppls_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

     moGMApplGrid.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdAppls_TopLeftChange", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Function bSaveAppls()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lRetVal As Long

    bSaveAppls = False
    
    moDMApplGrid.Save
        
    'Save the sales order payment applications from the temp table
    With moClass.moAppDB
        .SetInParam mlSOKey
        .SetOutParam lRetVal
        .ExecuteSP "spsoSaveSalesOrderAppls"
        lRetVal = glGetValidLong(.GetOutParam(2))
        .ReleaseParams
    End With

    Select Case lRetVal
        Case 0
            'Error executing spsoSaveSalesOrderAppls.  Sales Order save canceled.
            giSotaMsgBox Me, moClass.moSysSession, kSOMsgErrorOnSave, "spsoSaveSalesOrderAppls"
            Exit Function
        Case 2
            'Changes were made to applications by another user.  Save canceled.
            giSotaMsgBox Me, moClass.moSysSession, kSOMsgApplSaveCanceled
            Exit Function
    End Select
    
    bSaveAppls = True
 
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bSaveAppls", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Sub moGMApplGrid_EnterGridRow(ByVal lRow As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Debug.Print "EnterGridRow Row "; lRow

    'Save the original values in order to restore if validation fails
    mdAmtToApplyHC = gdGetValidDbl(gsGridReadCell(grdAppls, lRow, kColToApplyHC))
    mdAmtToApplyNC = gdGetValidDbl(gsGridReadCell(grdAppls, lRow, kColToApplyNC))
    
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moGMApplGrid_EnterGridRow", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub optShow_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick optShow(Index), True
    #End If
'+++ End Customizer Code Push +++

    If Not moDMApplGrid Is Nothing Then
        'Repopulate the grid based on the display option
        moDMApplGrid.Save
        
        If Index = kOptShowAppl Then
            moDMApplGrid.Where = "ApplyPmt = 1"
        Else
            moDMApplGrid.Where = ""
        End If
        
        moDMApplGrid.Refresh
        
        ResetGrid
                
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "optShow_Click", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    HandleToolbarClick Button
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

'************************************************************************
'   Description:
'       Standard Shutdown Procedure.  Unload all child forms
'       and remove any objects created within this app.
'   Param:
'       <none>
'   Returns:
'************************************************************************
Public Sub PerformCleanShutDown()
    On Error GoTo ExpectedErrorRoutine
         
    If Not moDMApplGrid Is Nothing Then
        moDMApplGrid.UnloadSelf
        Set moDMApplGrid = Nothing
    End If
    
    If Not moGMApplGrid Is Nothing Then
        moGMApplGrid.UnloadSelf
        Set moGMApplGrid = Nothing
    End If
    
    If Not moContextMenu Is Nothing Then
        Set moContextMenu = Nothing
    End If

    If Not moDMform Is Nothing Then
        Set moDMform = Nothing
    End If
   
    TerminateControls Me
    
    'Exit this subroutine
    Exit Sub

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    FormHelpPrefix = "SOZ"         '       Put your prefix here
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Sub Appls_CustInitialize(oDmForm As clsDmForm)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    Set moDMform = oDmForm
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
            moFormCust.Initialize Me, goClass
            Set moFormCust.CustToolbarMgr = tbrMain
            moFormCust.ApplyDataBindings moDMform
            moFormCust.ApplyFormCust
        End If
    End If
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Appls_CustInitialize", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_Paint(Index As Integer)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If
#If CUSTOMIZER Then


Private Sub txtCustName_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCustName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustName_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustName_KeyPress(KeyAscii as Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCustName, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustName_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustName_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCustName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustName_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustName_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCustName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustName_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustID_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCustID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustID_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustID_KeyPress(KeyAscii as Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCustID, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustID_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCustID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustID_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustID_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCustID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustID_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtOrderNo_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtOrderNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtOrderNo_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtOrderNo_KeyPress(KeyAscii as Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtOrderNo, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtOrderNo_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtOrderNo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtOrderNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtOrderNo_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtOrderNo_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtOrderNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtOrderNo_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub curTranAmt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curTranAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTranAmt_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curTranAmt_KeyPress(KeyAscii as Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curTranAmt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTranAmt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curTranAmt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curTranAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTranAmt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curTranAmt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curTranAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTranAmt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curPrePmts_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curPrePmts, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curPrePmts_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curPrePmts_KeyPress(KeyAscii as Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curPrePmts, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curPrePmts_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curPrePmts_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curPrePmts, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curPrePmts_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curPrePmts_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curPrePmts, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curPrePmts_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBalance_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curBalance, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBalance_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBalance_KeyPress(KeyAscii as Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curBalance, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBalance_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBalance_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curBalance, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBalance_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBalance_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curBalance, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBalance_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCustID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkCustID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkCustID_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCustID_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkCustID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkCustID_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCustName_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkCustName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkCustName_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCustName_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkCustName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkCustName_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optShow_DblClick(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnDblClick optShow(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optShow_DblClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optShow_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus optShow(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optShow_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optShow_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus optShow(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optShow_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
               Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If
#End If


