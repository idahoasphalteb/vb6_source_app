VERSION 5.00
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Begin VB.Form frmOutOfStock 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Out of Stock"
   ClientHeight    =   4635
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7845
   HelpContextID   =   17775905
   Icon            =   "sozdj105.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4635
   ScaleWidth      =   7845
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAction 
      Caption         =   "Create &WO"
      Enabled         =   0   'False
      Height          =   375
      Index           =   8
      Left            =   3720
      TabIndex        =   12
      Top             =   1080
      WhatsThisHelpID =   17775906
      Width           =   1215
   End
   Begin VB.CommandButton cmdAction 
      Caption         =   "&Tag Order"
      Enabled         =   0   'False
      Height          =   375
      Index           =   7
      Left            =   3720
      TabIndex        =   20
      Top             =   3000
      WhatsThisHelpID =   17775907
      Width           =   1215
   End
   Begin NEWSOTALib.SOTANumber txtQtyOrdered 
      Height          =   255
      Left            =   1080
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   690
      WhatsThisHelpID =   17775908
      Width           =   1425
      _Version        =   65536
      _ExtentX        =   2514
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      sBorder         =   0
      mask            =   "<ILH>###|,###|,###|,###|,##<ILp0>#"
      text            =   "              0"
      sIntegralPlaces =   15
      sDecimalPlaces  =   0
   End
   Begin VB.CommandButton cmdAction 
      Caption         =   "&Cancel"
      Height          =   375
      Index           =   6
      Left            =   3720
      TabIndex        =   24
      Top             =   3960
      WhatsThisHelpID =   17775909
      Width           =   1215
   End
   Begin VB.CommandButton cmdAction 
      Caption         =   "&Use Available"
      Enabled         =   0   'False
      Height          =   375
      Index           =   3
      Left            =   3720
      TabIndex        =   14
      Top             =   1560
      WhatsThisHelpID =   17775910
      Width           =   1215
   End
   Begin VB.CommandButton cmdAction 
      Caption         =   "&Back Order"
      Enabled         =   0   'False
      Height          =   375
      Index           =   4
      Left            =   3720
      TabIndex        =   10
      Top             =   600
      WhatsThisHelpID =   17775911
      Width           =   1215
   End
   Begin VB.CommandButton cmdAction 
      Caption         =   "&Delete"
      Height          =   375
      Index           =   2
      Left            =   3720
      TabIndex        =   22
      Top             =   3480
      WhatsThisHelpID =   17775912
      Width           =   1215
   End
   Begin VB.CommandButton cmdAction 
      Caption         =   "A&lternatives..."
      Enabled         =   0   'False
      Height          =   375
      Index           =   5
      Left            =   3720
      TabIndex        =   16
      Top             =   2040
      WhatsThisHelpID =   17775913
      Width           =   1215
   End
   Begin VB.CommandButton cmdAction 
      Caption         =   "Drop &Ship"
      Enabled         =   0   'False
      Height          =   375
      Index           =   1
      Left            =   3720
      TabIndex        =   18
      Top             =   2520
      WhatsThisHelpID =   17775914
      Width           =   1215
   End
   Begin NEWSOTALib.SOTANumber txtQtyAvail 
      Height          =   255
      Left            =   1200
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   1050
      WhatsThisHelpID =   17775915
      Width           =   1305
      _Version        =   65536
      _ExtentX        =   2302
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      sBorder         =   0
      mask            =   "<ILH>###|,###|,###|,###|,##<ILp0>#"
      text            =   "              0"
      sIntegralPlaces =   15
      sDecimalPlaces  =   0
   End
   Begin NEWSOTALib.SOTANumber txtQtyShort 
      Height          =   255
      Left            =   1080
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   1410
      WhatsThisHelpID =   17775916
      Width           =   1425
      _Version        =   65536
      _ExtentX        =   2514
      _ExtentY        =   450
      _StockProps     =   93
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      sBorder         =   0
      mask            =   "<ILH>###|,###|,###|,###|,##<ILp0>#"
      text            =   "              0"
      sIntegralPlaces =   15
      sDecimalPlaces  =   0
   End
   Begin VB.Label lblDiscontinued 
      Caption         =   "*** DISCONTINUED ***"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   480
      TabIndex        =   26
      Top             =   2280
      Width           =   2415
   End
   Begin VB.Label lblButtonsCap 
      AutoSize        =   -1  'True
      Caption         =   "Save Line - Create Work Order"
      Height          =   195
      Index           =   7
      Left            =   5040
      TabIndex        =   13
      Top             =   1155
      Width           =   2190
   End
   Begin VB.Label lblTagOrder 
      Caption         =   "Tag Line To A Purchase Order"
      Height          =   195
      Left            =   5040
      TabIndex        =   21
      Top             =   3075
      Width           =   2355
   End
   Begin VB.Label lblButtonsCap 
      AutoSize        =   -1  'True
      Caption         =   "Return To SO Line"
      Height          =   195
      Index           =   6
      Left            =   5040
      TabIndex        =   25
      Top             =   4035
      Width           =   1335
   End
   Begin VB.Label lblButtonsCap 
      AutoSize        =   -1  'True
      Caption         =   "Delete SO Line"
      Height          =   195
      Index           =   5
      Left            =   5040
      TabIndex        =   23
      Top             =   3555
      Width           =   1080
   End
   Begin VB.Label lblButtonsCap 
      AutoSize        =   -1  'True
      Caption         =   "Ship Direct From Vendor"
      Height          =   195
      Index           =   4
      Left            =   5040
      TabIndex        =   19
      Top             =   2595
      Width           =   1725
   End
   Begin VB.Label lblButtonsCap 
      AutoSize        =   -1  'True
      Caption         =   "View Other Warehouse/Substitutes"
      Height          =   195
      Index           =   3
      Left            =   5040
      TabIndex        =   17
      Top             =   2115
      Width           =   2505
   End
   Begin VB.Label lblButtonsCap 
      AutoSize        =   -1  'True
      Caption         =   "Save Line - Reduce Qty To Available "
      Height          =   195
      Index           =   2
      Left            =   5040
      TabIndex        =   15
      Top             =   1635
      Width           =   2685
   End
   Begin VB.Label lblButtonsCap 
      AutoSize        =   -1  'True
      Caption         =   "Save Line - Back Order Short Qty"
      Height          =   195
      Index           =   1
      Left            =   5040
      TabIndex        =   11
      Top             =   675
      Width           =   2370
   End
   Begin VB.Label lblUOM 
      Caption         =   "UOM"
      Height          =   255
      Index           =   2
      Left            =   2520
      TabIndex        =   6
      Top             =   1080
      Width           =   975
   End
   Begin VB.Label lblUOM 
      Caption         =   "UOM"
      Height          =   255
      Index           =   1
      Left            =   2520
      TabIndex        =   9
      Top             =   1440
      Width           =   975
   End
   Begin VB.Label lblUOM 
      Caption         =   "UOM"
      Height          =   255
      Index           =   0
      Left            =   2520
      TabIndex        =   3
      Top             =   720
      Width           =   975
   End
   Begin VB.Line Line1 
      X1              =   1200
      X2              =   2520
      Y1              =   1320
      Y2              =   1320
   End
   Begin VB.Label lblQtyShort 
      AutoSize        =   -1  'True
      Caption         =   "Qty Short:"
      Height          =   195
      Left            =   120
      TabIndex        =   7
      Top             =   1440
      Width           =   705
   End
   Begin VB.Label lblQtyAvailable 
      AutoSize        =   -1  'True
      Caption         =   "Qty Available:"
      Height          =   195
      Left            =   120
      TabIndex        =   4
      Top             =   1080
      Width           =   975
   End
   Begin VB.Label lblQtyOrdered 
      AutoSize        =   -1  'True
      Caption         =   "Qty Ordered:"
      Height          =   195
      Left            =   120
      TabIndex        =   1
      Top             =   720
      Width           =   900
   End
   Begin VB.Label lblOutOfStock 
      Caption         =   "LblOutOfStock"
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7545
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmOutOfStock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private moContextMenu  As New clsContextMenu   'Context Menu

Private moClass        As Object

Private mbEnableTagOrder    As Boolean
Private mbEnableCreateWO    As Boolean
Private mdPendQty           As Double
Private mlPOLineKey         As Long
Private mlSOLineKey         As Long
Private mlSOKey             As Long
Private mdQtyShort          As Double
Private mdQtyShip           As Double
Private mdQtyRplc           As Double
Private mbQuote             As Boolean
Private miItemStatus        As Integer
Private miAction            As Integer
Private mlItemKey           As Long                 ' original ordered Item key
Private msItemID            As String
Private msItemDesc          As String
Private mbContractBlkt      As Boolean
Private mdQtyAvail          As Double
Private mbQtyAvailChng      As Boolean
Private mdQtyOpen           As Double
Private mbIntegrateWithPO   As Boolean
Private miDecPlaces         As Integer
Private mlUOMKey            As Long
Private msUOMID             As String
Private msShipToID          As String
Private msShipToName        As String
Private msWhseID            As String
Private mlWhseKey           As Long
Private msWhseDesc          As String
Private mbSelectedSubst     As Boolean
Private mdQtyOrd            As Double
Private mbAllowDropShip     As Boolean
Private mbAllowInvtSubst    As Boolean
Private miLineStatus        As Integer
Private mbActivity          As Boolean


Public Property Get Action() As Integer
    Action = miAction
End Property

Public Property Let Activity(ByVal bActivity As Boolean)
    mbActivity = bActivity
End Property

Public Property Let AllowDropShip(ByVal bAllowDropShip As Boolean)
    mbAllowDropShip = bAllowDropShip
End Property

Public Property Let AllowInvtSubst(ByVal bAllowInvtSubst As Boolean)
    mbAllowInvtSubst = bAllowInvtSubst
End Property

Public Property Let ContractBlanketLine(ByVal bValue As Boolean)
    mbContractBlkt = bValue
End Property

Public Property Get ContractBlanketLine() As Boolean
    ContractBlanketLine = mbContractBlkt
End Property

Public Property Let DecPlaces(ByVal iDecPlaces As Integer)
    miDecPlaces = iDecPlaces
End Property

Public Property Let EnableTagOrder(ByVal bEnableTagOrder As Boolean)
    mbEnableTagOrder = bEnableTagOrder
End Property

Public Property Let EnableCreateWO(ByVal bEnableCreateWO As Boolean)
    mbEnableCreateWO = bEnableCreateWO
End Property

Public Property Let IntegrateWithPO(ByVal bIntegrateWithPO As Boolean)
    mbIntegrateWithPO = bIntegrateWithPO
End Property

Public Property Let ItemKey(ByVal lNewKey As Long)
    mlItemKey = lNewKey
End Property

Public Property Get ItemKey() As Long
    ItemKey = mlItemKey
End Property

Public Property Let ItemID(sItemID As String)
    msItemID = sItemID
End Property

Public Property Get ItemID() As String
    ItemID = msItemID
End Property

Public Property Let ItemDesc(sItemDesc As String)
    msItemDesc = sItemDesc
End Property

Public Property Let ItemStatus(ByVal iItemStatus As Integer)
    miItemStatus = iItemStatus
End Property

Public Property Let LineStatus(ByVal iLineStatus As Integer)
    miLineStatus = iLineStatus
End Property

Public Property Set oClass(oNewClass As Object)
    Set moClass = oNewClass
End Property

Public Property Get oClass() As Object
    Set oClass = moClass
End Property

Public Property Get OpenQty() As Double
    OpenQty = mdQtyOpen
End Property

Public Property Let OpenQty(ByVal dNewQty As Double)
    mdQtyOpen = dNewQty
End Property

Public Property Get PendQty() As Double
    PendQty = mdPendQty
End Property

Public Property Let PendQty(ByVal dNewQty As Double)
    mdPendQty = dNewQty
End Property

Public Property Get POLineKey() As Long
    POLineKey = mlPOLineKey
End Property

Public Property Let POLineKey(ByVal lNewKey As Long)
    mlPOLineKey = lNewKey
End Property

Public Property Let QtyAvail(ByVal dQtyAvail As Double)
    mdQtyAvail = dQtyAvail
End Property

Public Property Get QtyAvail() As Double
    QtyAvail = mdQtyAvail
End Property

Public Property Get QtyOrd() As Double
    QtyOrd = mdQtyOrd
End Property

Public Property Let QtyAvailChng(ByVal bQtyAvailChng As Boolean)
    mbQtyAvailChng = bQtyAvailChng
End Property

Public Property Let QtyOpen(ByVal dQtyOpen As Double)
    mdQtyOpen = dQtyOpen
End Property

Public Property Get QtyOpen() As Double
    QtyOpen = mdQtyOpen
End Property

Public Property Let Quote(ByVal bQuote As Boolean)
    mbQuote = bQuote
End Property

Public Property Let QtyShort(ByVal dQtyShort As Double)
    mdQtyShort = dQtyShort
End Property

Public Property Get SOLineKey() As Long
    SOLineKey = mlSOLineKey
End Property

Public Property Let SOKey(ByVal lNewKey As Long)
    mlSOKey = lNewKey
End Property

Public Property Get SOKey() As Long
    SOKey = mlSOKey
End Property

Public Property Let SOLineKey(ByVal lNewKey As Long)
    mlSOLineKey = lNewKey
End Property

Public Property Get SelectedSubst() As Boolean
    SelectedSubst = mbSelectedSubst
End Property

Public Property Let ShipToID(ByVal sShipToID As String)
    msShipToID = sShipToID
End Property

Public Property Let ShipToName(ByVal sShipToName As String)
    msShipToName = sShipToName
End Property

Public Property Let UOMKey(ByVal lUOMKey As Long)
    mlUOMKey = lUOMKey
End Property

Public Property Let UOMID(ByVal sUOMID As String)
    msUOMID = sUOMID
End Property

Public Property Let WhseKey(ByVal lNewKey As Long)
    mlWhseKey = lNewKey
End Property

Public Property Get WhseKey() As Long
    WhseKey = mlWhseKey
End Property

Public Property Let WhseID(ByVal sWhseID As String)
    msWhseID = sWhseID
End Property

Public Property Get WhseID() As String
    WhseID = msWhseID
End Property

Public Property Let WhseDesc(ByVal sWhseDesc As String)
    msWhseDesc = sWhseDesc
End Property


Private Property Get sMyName() As String
    sMyName = "frmOutOfStock"
End Property


'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    FormHelpPrefix = "SOZ"         '       Put your prefix here
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    WhatHelpPrefix = "SOZ"         '       Put your prefix here
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Private Sub cmdAction_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
        
    Dim oDdn As Object
    Dim dOrderQty As Double
    Dim dOpenQty As Double
    Dim iLineStatus As Integer
    Dim iRetVal As Integer
    Dim lPOLineKey As Long
    Dim lSOLineKey As Long
    Dim lPOKey As Long
    Dim bRetCode As Boolean

    miAction = Index
    
    Select Case Index

        Case kAlternate
            'Display the Stock Alternative form.
            With frmStockAvail
                .QtyAvail = mdQtyAvail
                .QtyOpen = mdQtyOpen
                .DecPlaces = miDecPlaces
                .AllowDropShip = mbAllowDropShip
                .QtyAvailChng = mbQtyAvailChng
                .AllowInvtSubst = mbAllowInvtSubst
                .ContractBlanketLine = mbContractBlkt
                .ItemID = msItemID
                .ItemKey = mlItemKey
                .ItemDesc = msItemDesc
                .ShipToID = msShipToID
                .ShipToName = msShipToName
                .WhseID = msWhseID
                .WhseDesc = msWhseDesc
                .UOMID = msUOMID
                .UOMKey = mlUOMKey
                .IntegrateWithPO = mbIntegrateWithPO
                .Init moClass
                .Show vbModal
                mbSelectedSubst = .SelectedSubst
                mdQtyAvail = .QtyAvail
                msWhseID = .WhseID
                msItemID = .ItemID
                miAction = .Action
            End With

        Case kTagOrder
            Set oDdn = goGetSOTAChild(moClass.moFramework, mfrmMain.moSotaObjects, _
            kclsSOTaggedOrd, ktskSOTaggedOrd, kDDRunFlags, kContextDD)

            If oDdn Is Nothing Then
                'An error occurred pulling up Tagged Orders.
                giSotaMsgBox Me, moClass.moSysSession, kSOMsgTagOrdError
                Exit Sub
            Else
                 'Set up SO Tagging cls properties.
                 oDdn.SOLineKey = mlSOLineKey
                 oDdn.POLineKey = mlPOLineKey
                 oDdn.ItemKey = mlItemKey
                 oDdn.ItemID = msItemID
                 oDdn.WhseKey = mlWhseKey
                 oDdn.WhseID = msWhseID
                 oDdn.UOMKey = mlUOMKey
                 oDdn.UOMID = msUOMID
                 oDdn.LineStatus = miLineStatus
                 oDdn.ItemDesc = msItemDesc
                 oDdn.OpenQty = mdQtyOpen
                 oDdn.PendQty = mdPendQty
    
                 If miLineStatus = 1 Then
                     oDdn.RunMode = 1   'kmodeSOEntry
                 Else
                     oDdn.RunMode = 2   'kmodeSOReadOnly
                 End If
    
                 'Bring up the SO Tagging window.
                 mlPOLineKey = glGetValidLong(oDdn.GetTagLine)
            End If
        
        End Select
        
        If miAction <> kTagOrder Then
            Me.Hide
        End If

    Exit Sub

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cmdAction_Click", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Set moContextMenu.Form = frmOutOfStock
    moContextMenu.Init


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    miAction = kCancelLine
    Me.Hide

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Sub Init(oClass As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
Dim i As Integer
Dim sMask As String

Const kItemStatusDisc As Integer = 3 'discontinued item

    Set moClass = oClass

 'Display the Out of Stock message.
    lblOutOfStock.Caption = "You currently have insufficient quantity of " & msItemID & _
                        " in the " & msWhseID & " warehouse to fill this order."

    If miItemStatus = kItemStatusDisc Then
        lblDiscontinued.Visible = True
    Else
        lblDiscontinued.Visible = False
    End If

    'Set decimal places
    txtQtyOrdered.DecimalPlaces = miDecPlaces
    txtQtyAvail.DecimalPlaces = miDecPlaces
    txtQtyShort.DecimalPlaces = miDecPlaces

    'Display the value of Qty Ordered, Qty Available and Qty Short.
    txtQtyOrdered.Value = mdQtyOpen 'Qty open is QtyOrd less Qty Shipped less Qty Returned for Rplc
    txtQtyAvail.Value = mdQtyAvail
    txtQtyShort.Value = mdQtyShort

    'Display the UOM of Qty Ordered, Qty Available and Qty Short.
    For i = 0 To lblUOM.UBound
        lblUOM(i).Caption = msUOMID
    Next i

    'Enable the Drop Ship button when an item allows drop ships.
    If (mbAllowDropShip And mbIntegrateWithPO And Not mbActivity) Then
        gEnableControls cmdAction(kDropShip)
    End If

    'Enable the Tag Order button if the item allows tag orders.
    If mbEnableTagOrder Then
        gEnableControls cmdAction(kTagOrder)
    End If

    'Back Order button is always enabled.  Wouldn't be here unless in out of stock situation.
    gEnableControls cmdAction(kBackOrd)

    'Enable the Create Work Order button when Advanced Manufacturing is activated.
    If mbEnableCreateWO Then
        gEnableControls cmdAction(kCreateWO)
    End If

    'Enable the Use Qty Available button when selected item qty avail > and not from a blanket contract
    If mdQtyAvail > 0 Then
        If mbContractBlkt = False Then
            gEnableControls cmdAction(kUseQtyAvail)
        Else
            gDisableControls cmdAction(kUseQtyAvail)
        End If
    Else
        gDisableControls cmdAction(kUseQtyAvail)
    End If

    'Alternatives button gets enabled when selected SOLine has no activity.
    If Not mbActivity Then
        gEnableControls cmdAction(kAlternate), cmdAction(kDeleLine)
    Else
        gDisableControls cmdAction(kAlternate), cmdAction(kDeleLine)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Init", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moContextMenu = Nothing

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


