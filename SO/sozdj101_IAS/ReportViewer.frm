VERSION 5.00
Object = "{F62B9FA4-455F-4FE3-8A2D-205E4F0BCAFB}#11.5#0"; "CRViewer.dll"
Begin VB.Form frmReportViewer 
   Caption         =   "Document Viewer"
   ClientHeight    =   15375
   ClientLeft      =   1020
   ClientTop       =   630
   ClientWidth     =   15180
   LinkTopic       =   "Form1"
   ScaleHeight     =   15375
   ScaleWidth      =   15180
   Begin CrystalActiveXReportViewerLib11_5Ctl.CrystalActiveXReportViewer CRViewer 
      Height          =   15315
      Left            =   30
      TabIndex        =   0
      Top             =   30
      Width           =   15105
      _cx             =   26644
      _cy             =   27014
      DisplayGroupTree=   0   'False
      DisplayToolbar  =   -1  'True
      EnableGroupTree =   0   'False
      EnableNavigationControls=   -1  'True
      EnableStopButton=   -1  'True
      EnablePrintButton=   -1  'True
      EnableZoomControl=   -1  'True
      EnableCloseButton=   -1  'True
      EnableProgressControl=   -1  'True
      EnableSearchControl=   -1  'True
      EnableRefreshButton=   -1  'True
      EnableDrillDown =   -1  'True
      EnableAnimationControl=   -1  'True
      EnableSelectExpertButton=   0   'False
      EnableToolbar   =   -1  'True
      DisplayBorder   =   -1  'True
      DisplayTabs     =   0   'False
      DisplayBackgroundEdge=   -1  'True
      SelectionFormula=   ""
      EnablePopupMenu =   -1  'True
      EnableExportButton=   -1  'True
      EnableSearchExpertButton=   0   'False
      EnableHelpButton=   0   'False
      LaunchHTTPHyperlinksInNewBrowser=   -1  'True
      EnableLogonPrompts=   -1  'True
      LocaleID        =   1033
      EnableInteractiveParameterPrompting=   0   'False
   End
End
Attribute VB_Name = "frmReportViewer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False



Private Sub Form_Load()
    On Error Resume Next
    
End Sub

Private Sub Form_Resize()

    CRViewer.Top = 330                     ' To take into account the toolbar
    CRViewer.Left = 0                      ' set the left
    CRViewer.Height = ScaleHeight - 350    ' To take into account the taskbar
    CRViewer.Width = ScaleWidth            ' set to screen width
    
End Sub



