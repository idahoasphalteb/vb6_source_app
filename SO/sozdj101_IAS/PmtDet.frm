VERSION 5.00
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#29.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#12.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "Tabctl32.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#13.0#0"; "SOTACalendar.ocx"
Object = "{9504980C-B928-4BF5-A5D0-13E1F649AECB}#12.0#0"; "SOTAVM.ocx"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#32.0#0"; "EntryLookupControls.ocx"
Begin VB.Form frmPmnts 
   Caption         =   "Enter Payments"
   ClientHeight    =   6045
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7680
   HelpContextID   =   17776226
   Icon            =   "PmtDet.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   ScaleHeight     =   6045
   ScaleWidth      =   7680
   StartUpPosition =   3  'Windows Default
   Begin NEWSOTALib.SOTAMaskedEdit txtCurrIDNC 
      Height          =   345
      Left            =   3480
      TabIndex        =   7
      Top             =   1305
      WhatsThisHelpID =   17776227
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   609
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.26
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      bLocked         =   -1  'True
      sBorder         =   0
      text            =   "CAD"
   End
   Begin SOTAVM.SOTAValidationMgr ValMgr 
      Left            =   3360
      Top             =   1920
      _ExtentX        =   741
      _ExtentY        =   661
      CtlsCount       =   0
   End
   Begin VB.CheckBox chkPmntByParent 
      Alignment       =   1  'Right Justify
      Caption         =   "Payment By Parent"
      Height          =   285
      Left            =   120
      TabIndex        =   10
      Top             =   1920
      WhatsThisHelpID =   17776228
      Width           =   1760
   End
   Begin VB.Frame fraTotal 
      Height          =   1335
      Left            =   4080
      TabIndex        =   37
      Top             =   480
      Width           =   3255
      Begin VB.Frame linTotalLine 
         Height          =   45
         Left            =   1080
         TabIndex        =   38
         Top             =   840
         Width           =   1905
      End
      Begin NEWSOTALib.SOTACurrency curOrderAmtHC 
         Height          =   285
         Left            =   1080
         TabIndex        =   39
         TabStop         =   0   'False
         Top             =   240
         WhatsThisHelpID =   17776229
         Width           =   1455
         _Version        =   65536
         _ExtentX        =   2566
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
         sBorder         =   0
         mask            =   "<HL> <ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "            0.00"
         sIntegralPlaces =   13
         sDecimalPlaces  =   2
      End
      Begin NEWSOTALib.SOTACurrency curPrePmtsHC 
         Height          =   285
         Left            =   1200
         TabIndex        =   40
         TabStop         =   0   'False
         Top             =   600
         WhatsThisHelpID =   17776230
         Width           =   1335
         _Version        =   65536
         bShowCurrency   =   0   'False
         _ExtentX        =   2355
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
         sBorder         =   0
         mask            =   "<HL><ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "            0.00"
         sIntegralPlaces =   13
         sDecimalPlaces  =   2
      End
      Begin NEWSOTALib.SOTACurrency curBalanceHC 
         Height          =   285
         Left            =   1080
         TabIndex        =   41
         TabStop         =   0   'False
         Top             =   960
         WhatsThisHelpID =   17776231
         Width           =   1455
         _Version        =   65536
         _ExtentX        =   2566
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
         sBorder         =   0
         mask            =   "<HL> <ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "            0.00"
         sIntegralPlaces =   13
         sDecimalPlaces  =   2
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtCurrIDHC 
         Height          =   225
         Index           =   1
         Left            =   2640
         TabIndex        =   48
         Top             =   240
         WhatsThisHelpID =   17776232
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   388
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.07
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
         bLocked         =   -1  'True
         sBorder         =   0
         text            =   "USD"
      End
      Begin VB.Label lblOrderAmt 
         AutoSize        =   -1  'True
         Caption         =   "Order Total"
         Height          =   285
         Left            =   120
         TabIndex        =   44
         Top             =   240
         Width           =   795
      End
      Begin VB.Label lblBalanceHC 
         AutoSize        =   -1  'True
         Caption         =   "Balance"
         Height          =   195
         Left            =   120
         TabIndex        =   43
         Top             =   975
         Width           =   585
      End
      Begin VB.Label lblPrePmts 
         AutoSize        =   -1  'True
         Caption         =   "Prepayments"
         Height          =   285
         Left            =   120
         TabIndex        =   42
         Top             =   600
         Width           =   915
      End
   End
   Begin EntryLookupControls.TextLookup lkuPmtID 
      Height          =   285
      Left            =   1680
      TabIndex        =   2
      Top             =   600
      WhatsThisHelpID =   17776233
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   503
      ForeColor       =   -2147483640
      LookupMode      =   0
      MaxLength       =   10
      LookupID        =   "PendCustPayment"
      BoundColumn     =   "TranNo"
      BoundTable      =   "tarPendCustPmt"
      sSQLReturnCols  =   "TranNo,lkuPmtID,;CustPmtKey,,;"
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtBatch 
      Height          =   285
      Left            =   4850
      TabIndex        =   12
      Top             =   1920
      WhatsThisHelpID =   17776234
      Width           =   1255
      _Version        =   65536
      _ExtentX        =   2214
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      sBorder         =   0
   End
   Begin SOTACalendarControl.SOTACalendar dteTranDate 
      Height          =   285
      Left            =   1680
      TabIndex        =   4
      Top             =   930
      WhatsThisHelpID =   17776235
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   503
      Alignment       =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Text            =   "  /  /    "
   End
   Begin VB.Frame fraCmnts 
      Caption         =   "Comment"
      Height          =   675
      Left            =   120
      TabIndex        =   36
      Top             =   4920
      Width           =   7335
      Begin VB.TextBox txtCmnts 
         Height          =   285
         Left            =   120
         MaxLength       =   50
         MultiLine       =   -1  'True
         TabIndex        =   33
         Top             =   240
         WhatsThisHelpID =   17776236
         Width           =   7070
      End
   End
   Begin NEWSOTALib.SOTACurrency curTranAmtHC 
      Height          =   285
      Left            =   1680
      TabIndex        =   8
      Top             =   1590
      WhatsThisHelpID =   17776237
      Width           =   1695
      _Version        =   65536
      _ExtentX        =   2990
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   5655
      WhatsThisHelpID =   73
      Width           =   7680
      _ExtentX        =   13547
      _ExtentY        =   688
   End
   Begin VB.CommandButton cmdCurrency 
      Caption         =   "Currency..."
      Height          =   315
      Left            =   6240
      TabIndex        =   13
      Top             =   1920
      Visible         =   0   'False
      WhatsThisHelpID =   17776239
      Width           =   1095
   End
   Begin VB.Frame fraTender 
      Height          =   2535
      Left            =   120
      TabIndex        =   34
      Top             =   2280
      Width           =   7335
      Begin TabDlg.SSTab tabTender 
         Height          =   2175
         Left            =   120
         TabIndex        =   14
         Top             =   240
         WhatsThisHelpID =   17776240
         Width           =   7095
         _ExtentX        =   12515
         _ExtentY        =   3836
         _Version        =   393216
         Tab             =   1
         TabHeight       =   520
         TabCaption(0)   =   "Check"
         TabPicture(0)   =   "PmtDet.frx":23D2
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "pnlTender(0)"
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Credit Card"
         TabPicture(1)   =   "PmtDet.frx":23EE
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "pnlTender(1)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Other"
         TabPicture(2)   =   "PmtDet.frx":240A
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "pnlTender(2)"
         Tab(2).ControlCount=   1
         Begin Threed.SSPanel pnlTender 
            Height          =   1700
            Index           =   0
            Left            =   -74900
            TabIndex        =   45
            Top             =   360
            Width           =   6900
            _Version        =   65536
            _ExtentX        =   12171
            _ExtentY        =   2999
            _StockProps     =   15
            BackColor       =   14215660
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BevelOuter      =   0
            Begin VB.TextBox txtBankABANo 
               Height          =   285
               Left            =   1600
               MaxLength       =   10
               TabIndex        =   20
               Top             =   925
               WhatsThisHelpID =   17776241
               Width           =   1815
            End
            Begin VB.TextBox txtCheckNum 
               Height          =   285
               Left            =   1600
               MaxLength       =   20
               TabIndex        =   18
               Top             =   600
               WhatsThisHelpID =   17776242
               Width           =   1815
            End
            Begin VB.ComboBox cboCheckType 
               Height          =   315
               Left            =   1600
               Style           =   2  'Dropdown List
               TabIndex        =   16
               Top             =   240
               WhatsThisHelpID =   17776243
               Width           =   1815
            End
            Begin VB.Label lblBankABA 
               Caption         =   "Bank ABA Number"
               Height          =   255
               Left            =   120
               TabIndex        =   19
               Top             =   955
               Width           =   1575
            End
            Begin VB.Label lblCheckNum 
               Caption         =   "Check Number"
               Height          =   255
               Left            =   120
               TabIndex        =   17
               Top             =   630
               Width           =   1095
            End
            Begin VB.Label lblTenderType 
               Caption         =   "Tender Type"
               Height          =   255
               Left            =   120
               TabIndex        =   15
               Top             =   270
               Width           =   1095
            End
         End
         Begin Threed.SSPanel pnlTender 
            Height          =   1695
            Index           =   1
            Left            =   100
            TabIndex        =   46
            Top             =   360
            Width           =   6900
            _Version        =   65536
            _ExtentX        =   12171
            _ExtentY        =   2999
            _StockProps     =   15
            BackColor       =   14215660
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.28
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BevelOuter      =   0
            Begin VB.CommandButton cmdCreditCardDetail 
               Caption         =   "Detail..."
               Height          =   375
               Left            =   5640
               TabIndex        =   61
               Top             =   120
               Width           =   1095
            End
            Begin VB.ComboBox cboCardType 
               Height          =   315
               Left            =   1800
               Style           =   2  'Dropdown List
               TabIndex        =   22
               Top             =   240
               WhatsThisHelpID =   17776244
               Width           =   1815
            End
            Begin VB.TextBox txtCCNum 
               Height          =   285
               Left            =   1800
               MaxLength       =   20
               TabIndex        =   24
               Text            =   " "
               Top             =   600
               WhatsThisHelpID =   17776245
               Width           =   2775
            End
            Begin VB.TextBox txtAuthNum 
               Height          =   285
               Left            =   1800
               MaxLength       =   15
               TabIndex        =   28
               Top             =   1250
               WhatsThisHelpID =   17776246
               Width           =   2775
            End
            Begin VB.TextBox txtExprDate 
               Height          =   285
               Left            =   1800
               MaxLength       =   10
               TabIndex        =   26
               Top             =   925
               WhatsThisHelpID =   17776247
               Width           =   975
            End
            Begin VB.Label lblCCTendertype 
               Caption         =   "Tender Type"
               Height          =   255
               Left            =   120
               TabIndex        =   21
               Top             =   270
               Width           =   1095
            End
            Begin VB.Label lblCCNum 
               Caption         =   "Card Number"
               Height          =   255
               Left            =   120
               TabIndex        =   23
               Top             =   630
               Width           =   1095
            End
            Begin VB.Label lblExprDate 
               Caption         =   "Expiration Date"
               Height          =   255
               Left            =   120
               TabIndex        =   25
               Top             =   955
               Width           =   1575
            End
            Begin VB.Label lblAuthNum 
               Caption         =   "Authorization Number"
               Height          =   255
               Left            =   120
               TabIndex        =   27
               Top             =   1280
               Width           =   1575
            End
         End
         Begin Threed.SSPanel pnlTender 
            Height          =   1695
            Index           =   2
            Left            =   -74900
            TabIndex        =   47
            Top             =   360
            Width           =   6900
            _Version        =   65536
            _ExtentX        =   12171
            _ExtentY        =   2999
            _StockProps     =   15
            BackColor       =   14215660
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BevelOuter      =   0
            Begin VB.ComboBox cboOtherType 
               Height          =   315
               Left            =   1200
               Style           =   2  'Dropdown List
               TabIndex        =   30
               Top             =   240
               WhatsThisHelpID =   17776248
               Width           =   1815
            End
            Begin VB.TextBox txtOtherRef 
               Height          =   285
               Left            =   1200
               MaxLength       =   20
               TabIndex        =   32
               Text            =   " "
               Top             =   600
               WhatsThisHelpID =   17776249
               Width           =   1815
            End
            Begin VB.Label Label12 
               Caption         =   "Tender Type"
               Height          =   255
               Left            =   120
               TabIndex        =   29
               Top             =   270
               Width           =   1095
            End
            Begin VB.Label lblOtherRef 
               Caption         =   "Reference"
               Height          =   255
               Left            =   120
               TabIndex        =   31
               Top             =   630
               Width           =   1095
            End
         End
      End
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   7680
      _ExtentX        =   13547
      _ExtentY        =   741
      Style           =   2
   End
   Begin NEWSOTALib.SOTACurrency curTranAmtNC 
      Height          =   285
      Left            =   1680
      TabIndex        =   6
      Top             =   1260
      WhatsThisHelpID =   17776251
      Width           =   1695
      _Version        =   65536
      _ExtentX        =   2990
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtCurrIDHC 
      Height          =   225
      Index           =   0
      Left            =   3480
      TabIndex        =   9
      Top             =   1630
      WhatsThisHelpID =   17776252
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   397
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      bLocked         =   -1  'True
      sBorder         =   0
      text            =   "USD"
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   0
      TabIndex        =   49
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   50
      Top             =   0
      Visible         =   0   'False
      Width           =   1815
      Begin VB.OptionButton CustomOption 
         Caption         =   "Option"
         Enabled         =   0   'False
         Height          =   285
         Index           =   0
         Left            =   -30000
         TabIndex        =   51
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         WhatsThisHelpID =   68
         Width           =   1245
      End
      Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
         Height          =   285
         Index           =   0
         Left            =   -30000
         TabIndex        =   52
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         WhatsThisHelpID =   66
         Width           =   1245
         _Version        =   65536
         _ExtentX        =   2196
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
      End
      Begin NEWSOTALib.SOTANumber CustomNumber 
         Height          =   285
         Index           =   0
         Left            =   -30000
         TabIndex        =   53
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         WhatsThisHelpID =   67
         Width           =   1245
         _Version        =   65536
         _ExtentX        =   2196
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "           0.00"
         sDecimalPlaces  =   2
      End
      Begin MSComCtl2.UpDown CustomSpin 
         Height          =   285
         Index           =   0
         Left            =   -30000
         TabIndex        =   54
         Top             =   0
         Visible         =   0   'False
         WhatsThisHelpID =   69
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Enabled         =   0   'False
      End
      Begin VB.Label CustomLabel 
         Caption         =   "Label"
         Enabled         =   0   'False
         Height          =   285
         Index           =   0
         Left            =   -30000
         TabIndex        =   55
         Top             =   0
         Visible         =   0   'False
         Width           =   1245
      End
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   56
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   57
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   58
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   59
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   60
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin VB.Label lblBatch 
      Caption         =   "AR Batch"
      Height          =   255
      Left            =   4030
      TabIndex        =   11
      Top             =   1950
      Width           =   705
   End
   Begin VB.Label lblPmntDate 
      Caption         =   "Payment Date"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   960
      Width           =   1335
   End
   Begin VB.Label Label7 
      Caption         =   "Payment Amount"
      Height          =   255
      Left            =   0
      TabIndex        =   35
      Top             =   0
      Width           =   1455
   End
   Begin VB.Label lblPmntAmt 
      Caption         =   "Payment Amount"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   1290
      Width           =   1455
   End
   Begin VB.Label lblPmntRef 
      Caption         =   "Payment Reference"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   630
      Width           =   1455
   End
End
Attribute VB_Name = "frmPmnts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************************
'     Name: frmPmnts (Pmnts.frm)
'     Desc: Enter Sales Order Payments
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: JLW 12-09-2004
'     Mods:
'************************************************************************************
Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

    Private moContextMenu      As clsContextMenu
    Private moClass            As Object
    
    Private WithEvents moDmSalesOrderPmt  As clsDmForm
Attribute moDmSalesOrderPmt.VB_VarHelpID = -1
    Private WithEvents moDmCustPmt        As clsDmForm
Attribute moDmCustPmt.VB_VarHelpID = -1
    Private WithEvents moDmTenderDetl     As clsDmForm
Attribute moDmTenderDetl.VB_VarHelpID = -1
   
    
    Private msHomeCurrID         As String
    Private msDocCurrID          As String
    Private mlCurrExchSchdKey    As Long
    Private mdCurrExchRate       As Double
    Private mbUseMultiCurr       As Boolean
    Private mlSOKey              As Long
    Private msTranNo             As String
    Private mlBatchKey           As Long
    Private mlCustKey            As Long
    Private mlParentCustKey      As Long
    Private msParentCurrID       As String
    Private mlParentExchSchdKey  As Long
    Private miPmtByParent        As Integer
    Private miBillingType        As Integer
    Private mbSOActivity         As Boolean
    Private miHomePriceDecPlaces As Integer
    Private miHomeRoundMeth      As Integer
    Private miHomeRoundPrecision As Integer
    Private miUseNationalAccts   As Integer
    Private mbDfltPmtByParent    As Boolean
    Private mbReadOnly           As Boolean
    Private mbReadOnlyPerSO      As Boolean
    Private mdOrigTranAmtHC      As Double
    Private mlCustClassKey       As Long
    Private mlParentCustClassKey As Long
    Private msTranTypeID         As String
    Private mbSecurityRights     As Boolean
    Private miSaveStatus         As Integer
    Private mbCCActivated        As Boolean
    Private mlCCCustPmtKey       As Long
    Private mdFrtAmtNC           As Double
    Private mdSTaxAmtNC          As Double
    Private mdOrderAmtNC         As Double
    Private mlBillToAddrKey      As Long
    Private miCCTranStatus       As Integer
    Private msPurchOrderNum      As String
    
       
    Private WithEvents moCCTransactions     As clsCCTransactions
Attribute moCCTransactions.VB_VarHelpID = -1
            
    Private msCompanyID        As String
    Private mbEnterAsTab       As Boolean
    Private msBusinessDate     As String
    Private mlLanguage         As Long
        
    
    Private miFilter           As Integer
    
    'Minimum Form Size
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long
    
    
    ' Tender Type Class Constants
    Private Const kTenderClassCash = 1
    Private Const kTenderClassCheck = 2
    Private Const kTenderClassCCard = 3
    Private Const kTenderClassNegInst = 4
    Private Const kTenderClassEFT = 5
    Private Const kTenderClassMisc = 6
    
    'Tab index
    Private Const kTenderTabCheck = 0
    Private Const kTenderTabCredit = 1
    Private Const kTenderTabOther = 2
    
    'Billing type
    Private Const kvBalanceForward = 2
    
    

Const VBRIG_MODULE_ID_STRING = "PMNTS.FRM"

Private Sub cboCardType_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'********************************************************************************
' set field of unbound column
'********************************************************************************
    If cboCardType.ListIndex <> -1 Then
    
        moDmTenderDetl.SetColumnValue "TenderTypeKey", cboCardType.ItemData(cboCardType.ListIndex)
        
        ' clear out check info
        cboCheckType.ListIndex = -1
        txtBankABANo = ""
        
        ' clear out other info
        cboOtherType.ListIndex = -1
        txtOtherRef = ""

    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cboCardType_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub cboCheckType_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'********************************************************************************
' set field of unbound column
'********************************************************************************
    If cboCheckType.ListIndex <> -1 Then
    
        moDmTenderDetl.SetColumnValue "TenderTypeKey", cboCheckType.ItemData(cboCheckType.ListIndex)
        
        If Len(Trim(txtCheckNum)) = 0 Then
            txtCheckNum = lkuPmtID.Text
        End If
        
        If Len(Trim(txtBankABANo)) = 0 Then
            'If moDmForm.State = kDmStateAdd Then
            If gbGotFocus(Me, cboCheckType) Then
                txtBankABANo = gsGetValidStr(moClass.moAppDB.Lookup("ABANo", "tarCustomer", "CustKey = " & glGetValidLong(moDmCustPmt.GetColumnValue("CustKey"))))
            End If
                     
        End If
        
        ' clear out cr card info
        cboCardType.ListIndex = -1
        txtCCNum = ""
        txtExprDate = ""
        txtAuthNum = ""
        
        ' clear out other info
        cboOtherType.ListIndex = -1
        txtOtherRef = ""

    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cboCheckType_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub cboOtherType_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'********************************************************************************
' set field of unbound column
'********************************************************************************
    If cboOtherType.ListIndex <> -1 Then
        
        moDmTenderDetl.SetColumnValue "TenderTypeKey", cboOtherType.ItemData(cboOtherType.ListIndex)

        If Len(Trim(txtOtherRef)) = 0 Then
            txtOtherRef = lkuPmtID.Text
        End If
        
        ' clear out check info
        cboCheckType.ListIndex = -1
        txtCheckNum = ""
        txtBankABANo = ""
        
        ' clear out cr card info
        cboCardType.ListIndex = -1
        txtCCNum = ""
        txtExprDate = ""
        txtAuthNum = ""

    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cboOtherType_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub chkPmntByParent_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim dExchangeRate As Double
Dim lCurrExchSchdKey As Long
Dim sCurrID As String
Dim sSQL As String
Dim rs As Object

    'Determine whether this payment for this payer exists in the system batch for a different sales order.
    'If so, disallow the change and display an error message.
    If chkPmntByParent.Value = vbChecked Then
        If bPmntExistsOnOtherSO(mlParentCustKey) Then
            giSotaMsgBox Me, moClass.moSysSession, kSOParentPmntExists
            chkPmntByParent.Value = vbUnchecked
            Exit Sub
        End If
    Else
        If bPmntExistsOnOtherSO(mlCustKey) Then
            giSotaMsgBox Me, moClass.moSysSession, kSOPmntExists
            chkPmntByParent.Value = vbChecked
            Exit Sub
        End If
    End If
   

    If chkPmntByParent.Value = vbChecked Then
        moDmCustPmt.SetColumnValue "CustKey", mlParentCustKey
        sCurrID = msParentCurrID
        lCurrExchSchdKey = mlParentExchSchdKey
        
        moDmCustPmt.SetColumnValue "CustClassKey", mlParentCustClassKey
        
    Else
        moDmCustPmt.SetColumnValue "CustKey", mlCustKey
        sCurrID = msDocCurrID
        lCurrExchSchdKey = mlCurrExchSchdKey
        
        moDmCustPmt.SetColumnValue "CustClassKey", mlCustClassKey
        
    End If
    
    If sCurrID <> gsGetValidStr(moDmCustPmt.GetColumnValue("CurrID")) Then
        'Calculate Exchange rate
        If sCurrID = msHomeCurrID Then
        
            dExchangeRate = 1
        Else

            dExchangeRate = gdGetExchangeRate(moClass.moAppDB, _
                         msHomeCurrID, msCompanyID, _
                         sCurrID, msHomeCurrID, _
                         gsFormatDateToDB(dteTranDate.Text), _
                         kExchangeRateSell, lCurrExchSchdKey)
        End If

        'Load Currency Exchange Schedules
        If lCurrExchSchdKey <> 0 Then
            moDmCustPmt.SetColumnValue "CurrExchSchdKey", lCurrExchSchdKey
        Else
            moDmCustPmt.SetColumnValue "CurrExchSchdKey", Empty
        End If
        
        moDmCustPmt.SetColumnValue "CurrID", sCurrID
        
        txtCurrIDNC.Text = sCurrID

        SetCurrencyVisibility

        'Load exchange Rate
        If dExchangeRate > -1 Then
            moDmCustPmt.SetColumnValue "CurrExchRate", dExchangeRate
        Else
            moDmCustPmt.SetColumnValue "CurrExchRate", 1
        End If
        
        curTranAmtHC.Amount = gfRound(curTranAmtNC.Amount * gdGetValidDbl(moDmCustPmt.GetColumnValue("CurrExchRate")), _
                miHomeRoundMeth, miHomePriceDecPlaces, miHomeRoundPrecision)
                      
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "chkPmntByParent_Click", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub cmdCreditCardDetail_Click()
Dim bDoKeyChange As Boolean
Dim lCustPmtKey As Long


    lCustPmtKey = moDmCustPmt.GetColumnValue("CustPmtKey")
    bDoKeyChange = (mlCCCustPmtKey <> lCustPmtKey)
    
    If bDoKeyChange Then
        mlCCCustPmtKey = lCustPmtKey
    End If
    
            
    With moCCTransactions
       
        .CustPmtKey = moDmCustPmt.GetColumnValue("CustPmtKey")
        .CustPmtTranNo = lkuPmtID.Text
        .OrigTranKey = mlSOKey
        .OrigTranType = kTranTypeSOSO
        .OrigTranNo = msTranNo
        .PmntCurrID = txtCurrIDHC(1).Text
        .PmntDate = dteTranDate.Text
        .OrderAmtNC = mdOrderAmtNC
        .STaxAmtNC = mdSTaxAmtNC
        .FrtAmtNC = mdFrtAmtNC
        .PrePmntsHC = curPrePmtsHC.Amount
        .TranAmt = curTranAmtNC.Amount
        .CustID = gsGetValidStr(moClass.moAppDB.Lookup("CustID", "tarCustomer", "CustKey = " & mlCustKey))
        .PONumber = msPurchOrderNum
        
        .DMState = moDmCustPmt.State
        .PmntTenderTypeID = cboCardType.Text
        .CustKey = mlCustKey
        .CreditCardKey = glGetValidLong(moDmTenderDetl.GetColumnValue("CreditCardKey"))
        .BillToAddrKey = mlBillToAddrKey
        .GetOrigTranTypeDesc
        .ShowForm bDoKeyChange
                
    End With
    
    ResetForCreditCard
    
End Sub


Private Sub moCCTransactions_SavePmnt(dTranAmt As Double, sTenderType As String, sMaskedAccountNo As String, sAuth As String, sExpDate As String, _
    lCreditCardKey As Long, iTranStatus As PmntTranStatus, bSaveStatus As Boolean)
'This is event is fired from the credit card transactions object to force save of the payment
'before allowing a credit card transaction
Dim i As Integer

    bSaveStatus = False
   
    curTranAmtNC.Amount = dTranAmt
    'JLW -- Need to handle multicurrency!!!!!
    curTranAmtHC.Amount = dTranAmt
    
    For i = 0 To cboCardType.ListCount - 1
        If cboCardType.List(i) = sTenderType Then
            cboCardType.ListIndex = i
            Exit For
        End If
    Next i
    
    txtOtherRef.Text = ""
    txtCheckNum.Text = ""
    txtBankABANo.Text = ""
    
    txtCCNum.Text = sMaskedAccountNo
    txtAuthNum.Text = sAuth
    txtExprDate.Text = sExpDate
    
    If lCreditCardKey > 0 Then
        moDmTenderDetl.SetColumnValue "CreditCardKey", lCreditCardKey
    Else
        moDmTenderDetl.SetColumnValue "CreditCardKey", Empty
    End If
     
    'Reset form amounts
    If Not bIsValidPmntAmt(True, False) Then
        Exit Sub
    End If
    
    'Since we are bypassing the validation manager, set the valid value.    This is used by the payment validation to
    'determine what the previous value was when calculating the total prepayments amount.
    ValMgr.SetControlValid curTranAmtHC
        
    'moDmTenderDetl.SetDirty True, True
    
    'If CC transaction is authorized, the status is "hidden".
    miCCTranStatus = iTranStatus
   
    moDmCustPmt.SetDirty True, True
    
    HandleToolbarClick kTbSave
        
    If miSaveStatus = kDmSuccess Then
        
        bSaveStatus = True
    End If

End Sub

Private Sub moCCTransactions_UpdatePmnt(dTranAmt As Double, sTenderType As String, sMaskedAccountNo As String, sExpDate As String, lCreditCardKey As Long, iTranStatus As PmntTranStatus)
'This is event is fired from the credit card transactions object in the event the user updated credit card information but did not
'process a transaction.   This will allow the user to save the changes to the actual payment.   Otherwise they won't be able
'to save since there are required fields that are not populated.  Or if the pmnt type changed from non-CC to CC, need to hide the pmnt
'if no transactions.
Dim i As Integer

      
    curTranAmtNC.Amount = dTranAmt
    'Need to handle multicurrency
    curTranAmtHC.Amount = dTranAmt
    
    'Since we are bypassing the validation manager, set the valid value.    This is used by the payment validation to
    'determine what the previous value was when calculating the total prepayments amount.
    ValMgr.SetControlValid curTranAmtHC
    
    For i = 0 To cboCardType.ListCount - 1
        If cboCardType.List(i) = sTenderType Then
            cboCardType.ListIndex = i
            Exit For
        End If
    Next i
    
    txtCCNum.Text = sMaskedAccountNo
    txtExprDate.Text = sExpDate
    
    If lCreditCardKey > 0 Then
        moDmTenderDetl.SetColumnValue "CreditCardKey", lCreditCardKey
    Else
        moDmTenderDetl.SetColumnValue "CreditCardKey", Empty
    End If
    
    miCCTranStatus = iTranStatus
        
End Sub

Private Sub cmdCurrency_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'********************************************************************************
' display the multi-currency form and reset currency values returned
' from it
'********************************************************************************
    Dim sCurrID     As String
    Dim lKey        As Long
    Dim sKey        As String
    Dim iHookType   As Integer
    Dim uCurrInfo As CurrencyInfo

    On Error GoTo ExpectedErrorRoutine

    If Not ValMgr.ValidateForm Then
        Exit Sub
    End If

    ' initialize the currency form
    With frmCurrency
        ' pass in current values
        .DocCurrency = Trim(txtCurrIDNC.Text)
        .ExchRate = gdGetValidDbl(moDmCustPmt.GetColumnValue("CurrExchRate"))
        .CustSchedKey = glGetValidLong(moDmCustPmt.GetColumnValue("CustKey"))
        .ExchSchedKey = glGetValidLong(moDmCustPmt.GetColumnValue("CurrExchSchdKey"))
                           
        .TranDate = gsFormatDateToDB(dteTranDate.Value)
        
        If mbReadOnly Then
            .SetReadOnly
        Else
            If miBillingType = kvBalanceForward Then
                .SelectCurr = False
            Else
                .SelectCurr = True
            End If
        End If
        .bGuarRate = False
    End With
       
    ' show the currency form
    frmCurrency.Display

    moDmCustPmt.SetColumnValue "CurrExchRate", gdGetValidDbl(frmCurrency.ExchRate)
    
    txtCurrIDNC.Text = gsGetValidStr(frmCurrency.DocCurrency)
    
    moDmCustPmt.SetColumnValue "CurrID", gsGetValidStr(frmCurrency.DocCurrency)
        
    ' save exchange key
    If frmCurrency.ExchSchedKey = 0 Then
        moDmCustPmt.SetColumnValue "CurrExchSchdKey", ""
    Else
        moDmCustPmt.SetColumnValue "CurrExchSchdKey", gdGetValidDbl(frmCurrency.ExchSchedKey)
    End If
    
    SetCurrencyVisibility
    
    curPrePmtsHC.Amount = curPrePmtsHC.Amount - ValMgr.ValidValue(curTranAmtHC)
    
    'Calculate the home currency amount
    curTranAmtHC.Amount = gfRound(curTranAmtNC.Amount * gdGetValidDbl(frmCurrency.ExchRate), miHomeRoundMeth, miHomePriceDecPlaces, miHomeRoundPrecision)
    ValMgr.SetControlValid curTranAmtHC
    
    curPrePmtsHC.Amount = curPrePmtsHC.Amount + curTranAmtHC
    curBalanceHC.Amount = curOrderAmtHC.Amount - curPrePmtsHC.Amount
    
    'Doc currency controls
    gbSetCurrCtls moClass, gsGetValidStr(frmCurrency.DocCurrency), uCurrInfo, curTranAmtNC
       
    
    Exit Sub

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "cmdCurrency_Click"
    gClearSotaErr
    Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cmdCurrency_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub






Private Sub dteTranDate_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If moDmCustPmt.State = kDmStateNone Then
        gbSetFocus Me, lkuPmtID
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "dteTranDate_GotFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Static bDone As Boolean

#If CUSTOMIZER Then
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
            moFormCust.Initialize Me, goClass
            Set moFormCust.CustToolbarMgr = tbrMain
            moFormCust.ApplyDataBindings moDmCustPmt, moDmTenderDetl, moDmSalesOrderPmt
            moFormCust.ApplyFormCust
        End If
    End If
#End If

    If Not bDone Then
        bDone = True
        With ValMgr
            Set .Framework = moClass.moFramework
            .Keys.Add lkuPmtID
            .Init
        End With
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

    
Private Sub Form_Load()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim uCurrInfo As CurrencyInfo
Dim iPrevTab As Integer
       
    'moClass.lUIActive = kChildObjectInactive
    
    miFilter = RSID_UNFILTERED
    
     'get initial parameters from class
    With moClass.moSysSession
        msCompanyID = .CompanyID
        mbEnterAsTab = .EnterAsTab
        msBusinessDate = .BusinessDate
        msHomeCurrID = .CurrencyID
        mlLanguage = .Language
    End With
    
    msTranTypeID = gsGetValidStr(moClass.moAppDB.Lookup("TranTypeID", "tciTranTypeCompany", "TranType = " & kTranTypeARCR & " AND CompanyID = " & gsQuoted(msCompanyID)))

    
    'Set home currency controls
    gbSetCurrCtls moClass, msHomeCurrID, uCurrInfo, curOrderAmtHC, curPrePmtsHC, curBalanceHC, curTranAmtHC

    'Set the form's initial height and width.
    miOldFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormHeight = miOldFormHeight
    miMinFormWidth = miOldFormWidth
    
    BindForm
    
    SetupPmntLookup
    
    SetupTenderTypeDropDowns
    
    BindContextMenu
    BindToolBar
    
    
    mlBatchKey = glGetValidLong(moClass.moAppDB.Lookup("SOPaymentBatchKey", "tsoOptions", "CompanyID = " & gsQuoted(msCompanyID)))
    
    'Disabling all multicurrency processing because of problems in AR Invoice -- see scopus 33594
    'cmdCurrency.Visible = False
'    If mbUseMultiCurr Then
'        cmdCurrency.Visible = True
'    Else
'        cmdCurrency.Visible = False
'    End If

    mbCCActivated = moClass.moSysSession.IsModuleActivated(kModuleCC)
    
    If mbCCActivated Then
        cmdCreditCardDetail.Visible = True
        EnableCombo cboCardType, False
        gDisableControls txtCCNum, txtExprDate, txtAuthNum
        
        Set moCCTransactions = New clsCCTransactions
        With moCCTransactions
            Set .moAppDB = moClass.moAppDB
            Set .moFramework = moClass.moFramework
            Set .moSysSession = moClass.moSysSession
            .Context = CCContext.SOPmnt
            .Init
        End With
    Else
        cmdCreditCardDetail.Visible = False
    End If

    mbSecurityRights = bOverrideSecEvent("SOAPPLYPMT", moClass, False)
    
    SetFormEnableDisable
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub



Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    If Not moClass Is Nothing Then
        Set moClass = Nothing
    End If
     
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True.
'   Parameters:
'           KeyCode, Shift - see VB reference Manual.
'
'   NOTE: THIS ROUTINE SHOULD CALL A GLOBAL SUBROUTINE TO HANDLE ALL THE
'         AVAILABLE HOTKEYS.
'********************************************************************
    'Determine if the user used the form-level keys and process them
    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            'Process the form-level key pressed
            gProcessFKeys Me, KeyCode, Shift
    End Select
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
'************************************************************************
'   Description:
'       Process Key Presses on the form.  NOTE: Key Preview of the form
'       should be set to True.
'   Param:
'       KeyAscii -  ASCII Key Code of Key Pressed.
'   Returns:
'************************************************************************
Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        
        Case Else
            'Other KeyAscii routines.
    End Select
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


'************************************************************************
'   Description:
'       If form is dirty, prompt for save.  Handle cancel of the
'       shutdown or process a normal shutdown.
'   Param:
'       Cancel -        set to True to cancel the form shutdown
'       UnloadMode -    flag indicating type of shutdown requested
'   Returns:
'************************************************************************

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iRetVal As Integer
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
    If UnloadMode <> vbFormCode Then
    
        Cancel = 1
        
        If iConfirmUnload = kDmSuccess Then
            moDmCustPmt.Clear True
            ClearForm
            gbSetFocus Me, lkuPmtID
            Me.Hide
        End If
        
    Else
        PerformCleanShutDown
    End If

    Exit Sub


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

'************************************************************************
'   Description:
'       Standard Shutdown Procedure.  Unload all child forms
'       and remove any objects created within this app.
'   Param:
'       <none>
'   Returns:
'************************************************************************
Private Sub PerformCleanShutDown()

    On Error GoTo ExpectedErrorRoutine
         
    If Not moDmSalesOrderPmt Is Nothing Then
        moDmSalesOrderPmt.UnloadSelf
        Set moDmSalesOrderPmt = Nothing
    End If
    
    If Not moDmTenderDetl Is Nothing Then
        moDmTenderDetl.UnloadSelf
        Set moDmTenderDetl = Nothing
    End If
    
    If Not moDmCustPmt Is Nothing Then
        moDmCustPmt.UnloadSelf
        Set moDmCustPmt = Nothing
    End If
    
    If Not moCCTransactions Is Nothing Then
        moCCTransactions.UnloadMe
        Set moCCTransactions = Nothing
    End If
    
    If Not moContextMenu Is Nothing Then
        Set moContextMenu = Nothing
    End If

   
    TerminateControls Me
    
    'Exit this subroutine
    Exit Sub

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Function iConfirmUnload() As Integer
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iRetVal As Integer

    If mbReadOnly Then
        iConfirmUnload = kDmSuccess
        Exit Function
    End If
    
    If moDmCustPmt Is Nothing Then
        iConfirmUnload = kDmSuccess
        Exit Function
    End If

    
    If moDmCustPmt.IsDirty(True) Then
        
        iRetVal = giSotaMsgBox(Me, moClass.moSysSession, kmsgDMSaveChangesGen)
                
        If iRetVal = kretYes Then
                     
            iConfirmUnload = moDmCustPmt.Action(kDmFinish)
           
            Exit Function
            
        ElseIf iRetVal = kretCancel Then

            iConfirmUnload = kDmFailure
            Exit Function
            
        ElseIf iRetVal = kretNo Then
            
            iConfirmUnload = kDmSuccess
            moDmCustPmt.SetDirty False, True
            Exit Function
        End If
    End If

    iConfirmUnload = kDmSuccess

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "iConfirmUnload", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Sub BindForm()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moDmCustPmt = New clsDmForm
    Set moDmTenderDetl = New clsDmForm
    Set moDmSalesOrderPmt = New clsDmForm
   
    
    With moDmCustPmt
        .Table = "tarPendCustPmt"
        .UniqueKey = "CustPmtKey"
        .SaveOrder = 1
        Set .Form = frmPmnts
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        Set .SOTAStatusBar = sbrMain
        Set .Toolbar = tbrMain
        .BindLookup lkuPmtID
        .Bind Nothing, "CustPmtKey", SQL_INTEGER ', kDmNoInsert
        .Bind Nothing, "BatchKey", SQL_INTEGER ', kDmNoInsert
        .Bind Nothing, "CurrExchRate", SQL_FLOAT
        .Bind Nothing, "CurrExchSchdKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "CustClassKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "CustKey", SQL_INTEGER, kDmSetNull
        .Bind txtCurrIDNC, "CurrID", SQL_VARCHAR
        .Bind Nothing, "DiscTakenAmt", SQL_FLOAT
        .Bind Nothing, "GuarntdExchRate", SQL_SMALLINT
        .Bind Nothing, "PrepaidCustInvcNo", SQL_VARCHAR
        .Bind Nothing, "SeqNo", SQL_INTEGER
        .Bind curTranAmtNC, "TranAmt", SQL_FLOAT
        .Bind curTranAmtHC, "TranAmtHC", SQL_FLOAT
        .Bind txtCmnts, "TranCmnt", SQL_VARCHAR
        .Bind dteTranDate, "TranDate", SQL_DATE
        .Bind Nothing, "TranNo", SQL_VARCHAR
        .Bind Nothing, "TranType", SQL_INTEGER
        .Bind Nothing, "UnappliedAmt", SQL_FLOAT
        .Bind Nothing, "UnappliedAmtHC", SQL_FLOAT
        .Bind Nothing, "UpdateCounter", SQL_SMALLINT
        .Bind Nothing, "PmtRcptDate", SQL_DATE
        .Bind Nothing, "TranID", SQL_VARCHAR
        .Bind Nothing, "RevrsCustPmtKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "CreateType", SQL_SMALLINT
    
        .Init
    End With
    
    With moDmSalesOrderPmt
        .Table = "tarSalesOrderPmt"
        .UniqueKey = "SOKey, CustPmtKey"
        .SaveOrder = 2
        Set .Form = frmPmnts
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        Set .Parent = moDmCustPmt
        .ParentLink "CustPmtKey", "CustPmtKey", SQL_INTEGER
        .Bind Nothing, "SOKey", SQL_INTEGER
        .Bind Nothing, "CreditMemoKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "AssignedAmt", SQL_DECIMAL
        .Bind Nothing, "AppliedAmt", SQL_DECIMAL
        .Bind Nothing, "EnteredWithSO", SQL_SMALLINT
        .Bind Nothing, "SalesOrderPmtKey", SQL_INTEGER
        .Bind Nothing, "UpdateCounter", SQL_INTEGER
        .Init
    End With
         
    
    With moDmTenderDetl
        
        .Table = "tcmCashRcptDetl"
        .UniqueKey = "CustPmtKey"
        .SaveOrder = 3
        Set .Database = moClass.moAppDB
        Set .Form = frmPmnts
        Set .Session = moClass.moSysSession
        Set .Parent = moDmCustPmt
        .ParentLink "CustPmtKey", "CustPmtKey", SQL_INTEGER
        .Bind Nothing, "CashRcptDetlKey", SQL_INTEGER
        .Bind txtBankABANo, "ABANo", SQL_VARCHAR
        .Bind Nothing, "Amount", SQL_DECIMAL
        .Bind txtExprDate, "CrCardExpiration", SQL_CHAR
        .Bind txtAuthNum, "CrCardAuthNo", SQL_VARCHAR
        .Bind Nothing, "TenderTypeKey", SQL_INTEGER
        .Bind Nothing, "RefNo", SQL_VARCHAR
        .Bind Nothing, "SourceBatchKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "CurrID", SQL_VARCHAR
        .Bind Nothing, "TranAmtHC", SQL_DECIMAL
        .Bind Nothing, "CashTranKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "Description", SQL_CHAR, kDmSetNull
        .Bind Nothing, "CreditCardKey", SQL_INTEGER, kDmSetNull
                        
        .Init
        
    End With
    
  
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindForm", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub BindToolBar()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Set sbrMain.Framework = moClass.moFramework

    With tbrMain
        .RemoveSeparator kTbRenameId
        .RemoveButton kTbNextNumber
        .RemoveButton kTbRenameId               'Remove the rename id button
        .RemoveButton kTbMemo
        .RemoveButton kTbPrint
       
    End With

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindToolBar", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moContextMenu = New clsContextMenu  'Instantiate Context Menu Class

   With moContextMenu
        Set .Form = frmPmnts
        .Init                      'Init will set properties of Winhook control
    End With

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetupPmntLookup()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    With lkuPmtID
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB

        .LookupID = "PendCustPayment"

    End With
          
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupPmntLookup", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetupTenderTypeDropDowns()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim rs                As Object
    Dim sSQL              As String
    Dim lProcessorAcctKey As Long

    
    ' clear out combo boxes
    cboCheckType.Clear
    cboCardType.Clear
    cboOtherType.Clear

    
    sSQL = "SELECT TenderTypeKey, TenderTypeID, TenderTypeClass, ProcessorAcctKey FROM tcmTenderType WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not (rs Is Nothing) Then
        rs.MoveFirst
        While Not rs.IsEOF
            lProcessorAcctKey = glGetValidLong(rs.Field("ProcessorAcctKey"))
            
            ' load appropriate list box based on tender Type class
            Select Case giGetValidInt(rs.Field("TenderTypeClass"))

                Case kTenderClassCCard
                    
                    If (mbCCActivated And lProcessorAcctKey > 0) Or ((Not mbCCActivated) And lProcessorAcctKey = 0) Then
                        gComboAddItem cboCardType, gsGetValidStr(rs.Field("TenderTypeID")), glGetValidLong(rs.Field("TenderTypeKey"))
                    End If
                
                Case kTenderClassCheck
                    gComboAddItem cboCheckType, gsGetValidStr(rs.Field("TenderTypeID")), glGetValidLong(rs.Field("TenderTypeKey"))

                Case Else
                    gComboAddItem cboOtherType, gsGetValidStr(rs.Field("TenderTypeID")), gvCheckNull(rs.Field("TenderTypeKey"))

            End Select
            rs.MoveNext
        Wend
        rs.Close
        Set rs = Nothing
    End If
    Exit Sub
          
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupTenderTypeDropDowns", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub ResetCreditCardCombo(bDoForCC As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'For credit card payments entered before CC module activation, only allow cc tender type with no CC processor account.
'For credit card payments enter after CC module activation, only allow CC tender types with a CC Processor account.
    Dim rs            As Object
    Dim sSQL          As String
    Dim sTenderTypeID As String
    Dim iListIndex    As Integer

    iListIndex = cboCardType.ListIndex
    sTenderTypeID = cboCardType.Text
    
    ' clear out combo boxes
    cboCardType.Clear
       
    sSQL = "SELECT * FROM tcmTenderType WHERE CompanyID = " & gsQuoted(msCompanyID)
    sSQL = sSQL & " AND TenderTypeClass = " & kTenderClassCCard
    
    If bDoForCC Then
        sSQL = sSQL & " AND ProcessorAcctKey IS NOT NULL"
    Else
        sSQL = sSQL & " AND ProcessorAcctKey IS NULL"
    End If
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not (rs Is Nothing) Then
        rs.MoveFirst
        While Not rs.IsEOF
            
            gComboAddItem cboCardType, gsGetValidStr(rs.Field("TenderTypeID")), glGetValidLong(rs.Field("TenderTypeKey"))

            rs.MoveNext
        Wend
        rs.Close
        Set rs = Nothing
    End If
    
    If iListIndex <> -1 Then
        cboCardType.Text = sTenderTypeID
    Else
        cboCardType.ListIndex = -1
    End If
    
    Exit Sub
          
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ResetCreditCardCombo", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub Init(ByVal oClass As Object, ByVal bUseMultiCurr As Boolean, ByVal iHomePriceDecPlaces As Integer, _
    ByVal iHomeRoundMeth As Integer, ByVal iHomeRoundPrecision As Integer, ByVal iUseNationalAccts As Integer, _
    ByVal dAmtInvcd As Double, ByVal dPendAmtInvcd As Double)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Set moClass = oClass
    
    mbUseMultiCurr = bUseMultiCurr
    miHomePriceDecPlaces = iHomePriceDecPlaces
    miHomeRoundMeth = iHomeRoundMeth
    miHomeRoundPrecision = iHomeRoundPrecision
    miUseNationalAccts = iUseNationalAccts
    
    If dAmtInvcd > 0 Or dPendAmtInvcd > 0 Then
        curBalanceHC.Visible = False
        lblBalanceHC.Visible = False
        linTotalLine.Visible = False
    End If
        
                     
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Init", VBRIG_IS_FORM                                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub ShowForm(ByVal bReadOnly As Boolean, ByVal lSOKey As Long, ByVal sTranNo As String, _
    ByVal dCurrExchRate As Double, ByVal lCurrExchSchdKey, ByVal sDocCurrID, ByVal lCustKey As Long, _
    ByVal lBillToAddrKey, ByVal lParentCustKey As Long, ByVal iPmtByParent As Integer, ByVal iBillingType As Integer, _
    ByVal bDfltPmtByParent As Boolean, ByVal dOrderAmtHC As Double, ByVal dPrePmtsHC As Double, _
    ByVal dOrderAmtNC As Double, dFrtAmtNC As Double, dSTaxAmtNC As Double, sPurchOrderNum As String)
    
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String
Dim rs As Object
Dim lParentAddrKey As Long
Dim uCurrInfo       As CurrencyInfo

    mbReadOnlyPerSO = bReadOnly
    mlSOKey = lSOKey
    msTranNo = sTranNo
       
    mlCustKey = lCustKey
    mlBillToAddrKey = lBillToAddrKey
    miPmtByParent = iPmtByParent
    miBillingType = iBillingType
    mlCurrExchSchdKey = lCurrExchSchdKey
    msDocCurrID = sDocCurrID
    mdCurrExchRate = dCurrExchRate
    mbDfltPmtByParent = bDfltPmtByParent
    mlParentCustKey = lParentCustKey
    
    'Used by credit card processing
    mdFrtAmtNC = dFrtAmtNC
    mdSTaxAmtNC = dSTaxAmtNC
    mdOrderAmtNC = dOrderAmtNC
    msPurchOrderNum = sPurchOrderNum
    
    curOrderAmtHC.Amount = dOrderAmtHC
    curPrePmtsHC.Amount = dPrePmtsHC
    curBalanceHC.Amount = dOrderAmtHC - dPrePmtsHC
       
    If mlParentCustKey > 0 And miPmtByParent = 1 Then
        lParentAddrKey = moClass.moAppDB.Lookup("DfltBillToAddrKey", "tarCustomer", "CustKey = " & mlParentCustKey)
        
        sSQL = "SELECT CurrID, CurrExchSchdKey FROM tarCustAddr WHERE AddrKey = " & lParentAddrKey
        
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        If Not rs.IsEOF Then
            msParentCurrID = gsGetValidStr(rs.Field("CurrID"))
            mlParentExchSchdKey = glGetValidLong(rs.Field("CurrExchSchdKey"))
        End If
        
        If Not rs Is Nothing Then
            Set rs = Nothing
        End If
        
        mlParentCustClassKey = glGetValidLong(moClass.moAppDB.Lookup("CustClassKey", "tarCustomer", "CustKey = " & mlParentCustKey))
    End If
    
    mlCustClassKey = glGetValidLong(moClass.moAppDB.Lookup("CustClassKey", "tarCustomer", "CustKey = " & mlCustKey))
                 
    lkuPmtID.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID) & " AND BatchKey = " & mlBatchKey & " AND CustPmtKey IN (SELECT CustPmtKey FROM tarSalesOrderPmt WITH (NOLOCK) WHERE SOKey = " & mlSOKey & ")"
    
    dteTranDate.Text = msBusinessDate
    
    
    If miPmtByParent = 1 And miUseNationalAccts = 1 Then
        chkPmntByParent.Visible = True
    Else
        chkPmntByParent.Visible = False
              
    End If
    
    moDmCustPmt.SetColumnValue "CurrID", msDocCurrID
    
    txtCurrIDNC.Text = msDocCurrID
    txtCurrIDHC(0).Text = msHomeCurrID
    txtCurrIDHC(1).Text = msHomeCurrID

    SetCurrencyVisibility
    
    'Home currency controls
    gbSetCurrCtls moClass, msHomeCurrID, uCurrInfo, curTranAmtHC, curOrderAmtHC, _
        curBalanceHC, curPrePmtsHC
        
    'Doc currency controls
    gbSetCurrCtls moClass, msDocCurrID, uCurrInfo, curTranAmtNC
        
    tabTender.Tab = kTenderTabCheck
    
    gbSetFocus Me, lkuPmtID
    
    moDmCustPmt.SetDirty False, True
                       
    Me.Show vbModal
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ShowForm", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetCurrencyVisibility()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If gsGetValidStr(moDmCustPmt.GetColumnValue("CurrID")) <> msHomeCurrID Then
        txtCurrIDHC(0).Visible = True
        txtCurrIDHC(1).Visible = True
        curTranAmtHC.Visible = True
    Else
        txtCurrIDHC(0).Visible = False
        txtCurrIDHC(1).Visible = False
        curTranAmtHC.Visible = False
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetCurrencyVisibility", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetFormEnableDisable()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim ix As Integer
Dim rs As Object
Dim eTranState As CCTranState


    If mbReadOnly Or Not mbSecurityRights Then
        gDisableControls dteTranDate, curTranAmtHC, curTranAmtNC, chkPmntByParent, txtCmnts
                
        'Tender type controls
        gDisableControls txtCheckNum, txtBankABANo, txtOtherRef, txtCCNum, txtExprDate, txtAuthNum
        
        'gDisableControls doesn't work for combo boxes!
        EnableCombo cboCheckType, False
        EnableCombo cboOtherType, False
        EnableCombo cboCardType, False
                     
    Else
        gEnableControls curTranAmtNC, txtCmnts, chkPmntByParent
        
        'Tender type controls
        gEnableControls txtCheckNum, txtBankABANo, txtOtherRef
        
        'gDisableControls doesn't work for combo boxes...
        EnableCombo cboCheckType, True
        EnableCombo cboOtherType, True
            
        If Not mbCCActivated Then
            gEnableControls txtCCNum, txtExprDate, txtAuthNum
            
            EnableCombo cboCardType, True
        
        End If
                
        'Work around for sota date control, able to click on the drop down button and
        'select a date before a key is entered.
        If moDmCustPmt.State = kDmStateNone Then
            gDisableControls dteTranDate
        Else
            gEnableControls dteTranDate
        End If
        
    End If
    
    SetupToolbar
    
        
    ResetForCreditCard
    
    
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetFormEnableDisable", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub EnableCombo(ctrl As ComboBox, bEnable As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'gEnableControls and gDisableControls blank out the value of comboboxes
    If bEnable Then
        ctrl.Enabled = True
        ctrl.BackColor = vbWindowBackground
    Else
        ctrl.Enabled = False
        ctrl.BackColor = vbButtonFace
    End If
   
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "EnableCombo", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetupToolbar()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    With tbrMain
        If mbReadOnly Or Not mbSecurityRights Or moDmCustPmt.State = kDmStateNone Then
            .ButtonEnabled(kTbSave) = False
            .ButtonEnabled(kTbFinish) = False
            .ButtonEnabled(kTbDelete) = False
        Else
            .ButtonEnabled(kTbSave) = True
            .ButtonEnabled(kTbFinish) = True
            If moDmCustPmt.State = kDmStateEdit Then
                .ButtonEnabled(kTbDelete) = True
            Else
                .ButtonEnabled(kTbDelete) = False
            End If
        End If
    End With
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupToolbar", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ResetForCreditCard()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String
Dim rs As Object
    
  
    If bProcessForCC Then
    
        'If moDmCustPmt.State <> kDmStateNone Then
            gDisableControls txtCCNum, txtExprDate, txtAuthNum
            EnableCombo cboCardType, False
            cmdCreditCardDetail.Enabled = True
        'End If
        
        ResetCreditCardCombo True
        
        'Check to see if credit card transaction exists.  If so, disable the payment amount controls.
        If moDmCustPmt.State = kDmStateEdit Then
            
            With moCCTransactions
            
                .CheckState glGetValidLong(moDmCustPmt.GetColumnValue("CustPmtKey"))
    
                If .HasTrans Then
    
                    gDisableControls txtCheckNum, txtBankABANo, txtOtherRef
                    'gDisableControls doesn't work for combo boxes...
                    EnableCombo cboCheckType, False
                    EnableCombo cboOtherType, False
        
                    If .TranVoided Then
                        curTranAmtNC.Enabled = True
                        curTranAmtHC.Enabled = True
                        frmCurrency.SetReadWrite
                    Else
                        curTranAmtNC.Enabled = False
                        curTranAmtHC.Enabled = False
                        frmCurrency.SetReadOnly
                    End If
        
                    If Not .CanDelete Then
                        tbrMain.ButtonEnabled(kTbDelete) = False
                    End If
                Else
                    If (Not mbReadOnly) And mbSecurityRights Then
                        gEnableControls txtCheckNum, txtBankABANo, txtOtherRef
                        'gDisableControls doesn't work for combo boxes...
                        EnableCombo cboCheckType, True
                        EnableCombo cboOtherType, True
        
                        curTranAmtNC.Enabled = True
                        curTranAmtHC.Enabled = True
                        frmCurrency.SetReadWrite
                    End If
                
                End If
            End With
        Else 'New
        
        
            gDisableControls txtCCNum, txtExprDate, txtAuthNum
            EnableCombo cboCardType, False
            cmdCreditCardDetail.Enabled = True
            
            curTranAmtNC.Enabled = True
            curTranAmtHC.Enabled = True
            frmCurrency.SetReadWrite
        End If
    Else 'not a credit card (or credit card entered before CC activated).
        
        If mbCCActivated Then
            ResetCreditCardCombo False
            
            gEnableControls txtCCNum, txtExprDate, txtAuthNum
            EnableCombo cboCardType, True
            cmdCreditCardDetail.Enabled = False
            
            curTranAmtNC.Enabled = True
            curTranAmtHC.Enabled = True
            frmCurrency.SetReadWrite
        End If
    
    End If
         
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ResetForCreditCard", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bProcessForCC() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'Receipts that were entered before the credit card module was activated (or imported) should be processed as though the CC module is NOT
'activated, i.e. the credit card controls on the form will be enabled and the Detail button will be disabled.
Dim rs As Object
Dim sSQL As String
    
    bProcessForCC = False
    
    If Not mbCCActivated Then
        Exit Function
    End If
    
    If moDmCustPmt.State = kDmStateAdd Then
        bProcessForCC = True
    Else
        sSQL = "SELECT ProcessorAcctKey, TenderTypeClass FROM tcmTenderType WITH (NOLOCK) WHERE TenderTypeKey = " & glGetValidLong(moDmTenderDetl.GetColumnValue("TenderTypeKey"))
        
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

        If rs Is Nothing Then
            Exit Function
        End If
        
        If gsGetValidStr(rs.Field("TenderTypeClass")) = kTenderClassCCard Then
            If glGetValidLong(rs.Field("ProcessorAcctKey")) > 0 Then
                bProcessForCC = True
            End If
        End If
   
        Set rs = Nothing
    
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bProcessForCC", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function

Private Function bUpdateBatchTotals(dOldAmt As Double, dNewAmt As Double, lFrom As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iStatus         As Integer

    bUpdateBatchTotals = False

    With moClass.moAppDB
        .SetInParam mlBatchKey                  ' batch key
        .SetInParam kTranTypeARCR               ' tran Type
        .SetInParam Str$(dOldAmt)               ' old amt to subtract out
        .SetInParam Str$(dNewAmt)               ' new amt to add in
        .SetInParam lFrom                       ' 0=update, 1=Insert
        .SetOutParam iStatus                    ' return value
        .ExecuteSP ("spUpdateBatchTran")
        iStatus = .GetOutParam(6)
        .ReleaseParams
    End With

    If iStatus = 1 Then
        bUpdateBatchTotals = True
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bUpdateBatchTotals", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    WhatHelpPrefix = "SOZ"         '       Put your prefix here
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

'************************************************************************
'   oClass contains the reference to the parent class object.  The form
'   needs this reference to use the public variables created within the
'   class object.
'************************************************************************
Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set oClass = moClass
    
    'Exit this property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = oNewClass

    'Exit this property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property





Public Function lGetNextSeqNo(lBatchKey As Long) As Long
    On Error GoTo ExpexctedErrorRoutine
    Dim lSeqNo      As Long

    lSeqNo = 0
    With moClass.moAppDB
        .SetInParam lBatchKey           ' batch key
        .SetOutParam lSeqNo             ' return seq# in here
        .ExecuteSP "spGetNextSeq"       ' run SP
        lSeqNo = .GetOutParam(2)        ' get seq#
        .ReleaseParams                  ' release params
    End With

    lGetNextSeqNo = lSeqNo
    Exit Function

ExpexctedErrorRoutine:
    gClearSotaErr
    Exit Function
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lGetNextSeqNo", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub SetupSOPmnt()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    With moDmSalesOrderPmt
    
        If glGetValidLong(.GetColumnValue("SalesOrderPmtKey")) = 0 Then
            .SetColumnValue "SalesOrderPmtKey", glGetNextSurrogateKey(moClass.moAppDB, "tarSalesOrderPmt")
        End If
        
        .SetColumnValue "CustPmtKey", glGetValidLong(moDmCustPmt.GetColumnValue("CustPmtKey"))
        .SetColumnValue "EnteredWithSO", 1
        .SetColumnValue "AssignedAmt", curTranAmtNC.Amount
        .SetColumnValue "SOKey", mlSOKey
        .SetColumnValue "AppliedAmt", 0
        
    End With
               
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupSOPmnt", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bCheckTenderType() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

Dim bCheck As Boolean
Dim bCrCard As Boolean
Dim bOther As Boolean
        
    bCheck = False
    bCrCard = False
    bOther = False
    
    bCheckTenderType = False
        
    ' reference is only filled in for an 'Other' Type
    If cboOtherType.ListIndex <> -1 Then
        bOther = True
        
        If Len(Trim(txtOtherRef.Text)) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgRegReq
            gbSetFocus Me, txtOtherRef
            Exit Function
        End If
        
        moDmTenderDetl.SetColumnValue "RefNo", Trim(txtOtherRef.Text)
                
    End If
    ' check Number is only filled in for a 'Check' Type
    If cboCheckType.ListIndex <> -1 Then
        bCheck = True
        
        If Len(Trim(txtCheckNum.Text)) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgCheckNoReq
            gbSetFocus Me, txtCheckNum
            Exit Function
        End If
        
        moDmTenderDetl.SetColumnValue "RefNo", Trim(txtCheckNum.Text)
               
    End If
    ' credit card Number is only filled in for a 'Credit Card' Type
    If cboCardType.ListIndex <> -1 Then
        bCrCard = True
        
        If Len(Trim(txtCCNum.Text)) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgCreditCardReq
            gbSetFocus Me, txtCCNum
            Exit Function
        End If
        
        moDmTenderDetl.SetColumnValue "RefNo", Trim(txtCCNum.Text)
        
    End If
   
    If Not bCheck And Not bCrCard And Not bOther Then
    
        Select Case tabTender.Tab
            Case kTenderTabCheck          ' check
                gbSetFocus Me, cboCheckType
                giSotaMsgBox Me, moClass.moSysSession, kmsgCheckTenderReq
                Exit Function
               
            Case kTenderTabCredit         ' credit card
               gbSetFocus Me, cboCardType
               giSotaMsgBox Me, moClass.moSysSession, kmsgCCardTenderReq
               Exit Function
               
            Case kTenderTabOther          ' other
               gbSetFocus Me, cboOtherType
               giSotaMsgBox Me, moClass.moSysSession, kmsgOtherTenderReq
               Exit Function
               
        End Select
    End If
    
    bCheckTenderType = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bCheckTenderType", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bDeleteBatchTran(dOldAmt As Double) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim iRetVal         As Integer

    bDeleteBatchTran = False
    With moClass.moAppDB
        .SetInParam mlBatchKey                  ' batch key
        .SetInParam CLng(kTranTypeARCR)     ' tran Type
        .SetInParam Str$(dOldAmt)               ' old amt to subtract out
        .SetOutParam iRetVal
        .ExecuteSP ("spDeleteBatchTran")
        iRetVal = .GetOutParam(4)
        .ReleaseParams
    End With
    If iRetVal = 1 Then
        bDeleteBatchTran = True
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bDeleteBatchTran", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub moDmCustPmt_DMAfterDelete(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
 
    bValid = False
    
    If moDmSalesOrderPmt.DeleteRow(True) <> kDmSuccess Then
        Exit Sub
    End If
    
    If moDmTenderDetl.DeleteRow(True) <> kDmSuccess Then
        Exit Sub
    End If
           
    
    bValid = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmCustPmt_DMAfterDelete", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmCustPmt_DMBeforeDelete(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String

    bValid = False

    If Not bDeleteBatchTran(mdOrigTranAmtHC) Then
        Exit Sub
    End If
    
    curPrePmtsHC.Amount = curPrePmtsHC.Amount - mdOrigTranAmtHC
    curBalanceHC.Amount = curOrderAmtHC.Amount - curPrePmtsHC.Amount
    
    ' cash receipt is being deleted.  change cut pmt log to Void (5) status
    sSQL = "UPDATE tarCustPmtLog SET TranStatus = 5 WHERE CustPmtKey = " & moDmCustPmt.GetColumnValue("CustPmtKey")
    moClass.moAppDB.ExecuteSQL sSQL
    
    bValid = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmCustPmt_DMBeforeDelete", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmCustPmt_DMBeforeInsert(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iTranStatus As Integer

    Dim lKey        As Long
    Dim sSQL        As String
    Dim sTranID     As String

    bValid = False
    
    If Not bUpdateBatchTotals(0, curTranAmtHC.Amount, 1) Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgBatchTotTooLarge
        Exit Sub
    End If

    'update cust pmt log
    sTranID = gsGetValidStr(moDmCustPmt.GetColumnValue("TranID"))
    
    'Update from CC transaction is credit card, else pending.
    If miCCTranStatus > 0 Then
        iTranStatus = miCCTranStatus
    Else
        iTranStatus = 2
    End If
    
    sSQL = "UPDATE tarCustPmtLog SET TranStatus = " & iTranStatus & ", TranType = " & kTranTypeARCR & _
        ", TranID = " & gsQuoted(sTranID) & ", TranAmtHC = " & curTranAmtHC.Amount & _
        " WHERE CustPmtKey = " & moDmCustPmt.GetColumnValue("CustPmtKey")
    moClass.moAppDB.ExecuteSQL sSQL
    
    miCCTranStatus = 0

'********************************************************************************
' before insert of cash rcpt detail, assign a surrogate key
'********************************************************************************
    lKey = glGetNextSurrogateKey(moClass.moAppDB, "tcmCashRcptDetl")
    With moDmTenderDetl
        .SetColumnValue "CashRcptDetlKey", lKey
        .SetColumnValue "Description", moClass.moAppDB.Lookup("CustName", "tarCustomer", "CustKey = " & glGetValidLong(moDmCustPmt.GetColumnValue("CustKey")))
        .SetColumnValue "CurrID", txtCurrIDNC.Text
        .SetColumnValue "TranAmtHC", curTranAmtHC.Amount
        
        'The value for Tendertype key has been in beforeinsert event because the values are cleared
        Select Case tabTender.Tab
            Case kTenderTabCheck          ' check
                .SetColumnValue "TenderTypeKey", cboCheckType.ItemData(cboCheckType.ListIndex)
            Case kTenderTabCredit         ' credit card
                .SetColumnValue "TenderTypeKey", cboCardType.ItemData(cboCardType.ListIndex)
            Case kTenderTabOther          ' other
                .SetColumnValue "TenderTypeKey", cboOtherType.ItemData(cboOtherType.ListIndex)
        End Select
    End With
    
    bValid = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmCustPmt_DMBeforeInsert", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmCustPmt_DMBeforeTransaction(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    bValid = False
    
    If Not bCheckTenderType Then
        Exit Sub
    End If
    
    If curTranAmtHC.Amount <= 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kSOmsgPmntGreaterThan0, gsBuildString(ksPayment, moClass.moAppDB, moClass.moSysSession)
        Exit Sub
    End If
          
    If gdGetValidDbl(moDmCustPmt.GetColumnValue("CurrExchRate")) = 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgDocNoSave
    End If
    
    bValid = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmCustPmt_DMBeforeTransaction", VBRIG_IS_FORM        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmCustPmt_DMBeforeUpdate(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String

    bValid = False
    
    If Not bUpdateBatchTotals(mdOrigTranAmtHC, curTranAmtHC.Amount, 0) Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgBatchTotTooLarge
    End If
    
    'If coming from credit card transactions, need to hide the transaction if the credit card pmnt is not deposited.
     If miCCTranStatus > 0 Then
        
        sSQL = "UPDATE tarCustPmtLog SET TranStatus = " & miCCTranStatus & _
            ", TranAmtHC = " & curTranAmtHC.Amount & _
            " WHERE CustPmtKey = " & moDmCustPmt.GetColumnValue("CustPmtKey")
            
         moClass.moAppDB.ExecuteSQL sSQL
            
         miCCTranStatus = 0
    End If
    
                    
    With moDmTenderDetl
        .SetColumnValue "Description", moClass.moAppDB.Lookup("CustName", "tarCustomer", "CustKey = " & glGetValidLong(moDmCustPmt.GetColumnValue("CustKey")))
        .SetColumnValue "CurrID", txtCurrIDNC.Text
        .SetColumnValue "TranAmtHC", curTranAmtHC.Amount
    End With
    'End If
    
     bValid = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmCustPmt_DMBeforeUpdate", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmCustPmt_DMPostSave(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    ' re-calc "new" original payment amount (now that pmt has saved)
    mdOrigTranAmtHC = curTranAmtHC.Amount
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmCustPmt_DMPostSave", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmCustPmt_DMPreSave(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
 
    bValid = False
    
    'assign SeqNo and CreateType
    If moDmCustPmt.State = kDmStateAdd Then
        moDmCustPmt.SetColumnValue "SeqNo", lGetNextSeqNo(mlBatchKey)
        
    End If
    
    'Setup sales order payment record
    SetupSOPmnt
    
    bValid = True
     
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmCustPmt_DMPreSave", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmCustPmt_DMReposition(oChild As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    
        If moDmCustPmt.State = kDmStateAdd Then
            
            ValMgr.Reset
         
        End If

    Exit Sub

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmCustPmt_DMReposition", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmCustPmt_DMStateChange(iOldState As Integer, iNewState As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
                   
        If iNewState = kDmStateNone Then
                    
            ValMgr.Reset
            
            moClass.lUIActive = kChildObjectInactive
            
            gbSetFocus Me, lkuPmtID
            
        Else
                                              
            moClass.lUIActive = kChildObjectActive
            
            
        End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmCustPmt_DMStateChange", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub






Private Sub sbrMain_ButtonClick(sButton As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    HandleToolbarClick sButton
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "sbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuPmtID_LookupClicked()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    If lkuPmtID.ReturnColumnValues.Count <> 0 Then
        ValMgr_KeyChange
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuPmtID_LookupClicked", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Sub





Private Function bSOActivity()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lActivity As Long

 'Run the activity stored procedure
    With moClass.moAppDB
        .SetInParam mlSOKey                          'Sales Order Key
        .SetOutParam lActivity                      'Activity Count
        .ExecuteSP ("spsoSOActivity")            'Run the stored procedure
        lActivity = glGetValidLong(.GetOutParam(2)) 'Get The Activity
        .ReleaseParams                              'release the parameters
    End With
    If lActivity > 0 Then
        bSOActivity = True
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bSOActivity", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function



Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    HandleToolbarClick Button
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If
    Dim lRet                As Long
    Dim sNewKey             As String
    Dim sTranNo             As String
    Dim dPrePmtAmt          As Double
    
    'force last control lost_focus
    gbSetFocus Me, tbrMain
    DoEvents
        
    Select Case sKey
        Case kTbFinish
        
            If moDmCustPmt.Action(kDmFinish) = kDmSuccess Then
                ClearForm
            End If
                   
        Case kTbCancel
            
            If moDmCustPmt.Action(kDmCancel) = kDmSuccess Then
                ClearForm
                dPrePmtAmt = curPrePmtsHC.Amount
                curPrePmtsHC.Amount = frmTotPmts.dCalculatePrePayments
                curBalanceHC.Amount = curBalanceHC.Amount + dPrePmtAmt - curPrePmtsHC.Amount
            End If
                
        Case kTbDelete

            If moDmCustPmt.DeleteRow = kDmSuccess Then
                ClearForm
                gbSetFocus Me, lkuPmtID
            End If
            
        Case kTbSave
        
            'Save status of save, used by credit card processing
            miSaveStatus = moDmCustPmt.Save(True)
                        
        Case kTbHelp
            gDisplayFormLevelHelp Me
            Exit Sub

        
        Case kTbFirst, kTbNext, kTbLast, kTbPrevious
        
            If iConfirmUnload = kDmSuccess Then

                lRet = glLookupBrowse(lkuPmtID, sKey, miFilter, sNewKey)
                
                Select Case lRet
                    Case MS_SUCCESS
                        If lkuPmtID.ReturnColumnValues.Count = 0 Then
                            Exit Sub
                        End If
                                 
                        sTranNo = Trim(lkuPmtID.ReturnColumnValues("TranNo"))
                        If Trim(lkuPmtID.Text) <> sTranNo Then
                            moDmCustPmt.Clear True
                            ClearForm
                            lkuPmtID.Text = sTranNo
                            ValMgr_KeyChange
                        End If
                        
                    Case MS_EMPTY_RS
                        giSotaMsgBox Me, moClass.moSysSession, kmsgNavEmptyRecordSet

                    Case MS_ERROR
                        giSotaMsgBox Me, moClass.moSysSession, kmsgNavBrowseError

                    Case MS_NO_CURRENT_KEY
                        giSotaMsgBox Me, moClass.moSysSession, kmsgNavNoCurrentKey

                    Case MS_UNDEF_RS
                        giSotaMsgBox Me, moClass.moSysSession, kmsgNavUndefinedError

                    Case Else
                        gLookupBrowseError lRet, Me, moClass
                               
                End Select
                
            Else
                Exit Sub
            End If

        Case kTbFilter
            miFilter = giToggleLookupFilter(miFilter)
            Exit Sub

    End Select


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ClearForm()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'Clear the unbound controls on the form

    cboCheckType.ListIndex = -1
    cboCardType.ListIndex = -1
    cboOtherType.ListIndex = -1
    
    txtCheckNum.Text = ""
    txtBankABANo.Text = ""
    txtCCNum.Text = ""
    txtExprDate.Text = ""
    txtAuthNum.Text = ""
    txtOtherRef.Text = ""
    
    txtBatch.Text = ""
    
    moDmCustPmt.SetDirty False, True
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ClearForm", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub txtAuthNum_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moDmCustPmt.SetDirty True, True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtAuthNum_Change", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub txtBankABANo_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moDmCustPmt.SetDirty True, True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtBankABANo_Change", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub txtCCNum_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moDmCustPmt.SetDirty True, True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtCCNum_Change", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub txtCheckNum_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moDmCustPmt.SetDirty True, True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtCheckNum_Change", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub txtExprDate_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moDmCustPmt.SetDirty True, True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtExprDate_Change", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub txtOtherRef_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moDmCustPmt.SetDirty True, True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtOtherRef_Change", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ValMgr_BeforeValidate(oControl As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    'When the val mgr validates lookup controls, it calls the lookup's IsValid.  If invalid,
    'then the val mgr Validate routine is never called -- so if the DisplayMessages was set
    'to false on a previous validation, it will never be set back to true, and if a lookup value
    'is invalid because of the IsValid, the messsage will not be displayed.
    '
    'BeforeValidate is called before the IsValid is called, so set to true.
    ValMgr.DisplayMessages = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ValMgr_BeforeValidate", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ValMgr_KeyChange()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iKeyChangeCode As Integer
Dim bIsValidKey As Boolean
Dim lSOKey As Long
Dim bActivity As Boolean
Dim iRetVal As Integer
Dim sSQL As String
Dim lBatchKey As Long
Dim iTenderTypeClass As Integer
Dim sReference As String
Dim lTenderTypeKey As Long
Dim rs As Object
Dim lPmtKey As Long
Dim lPmtApplKey As Long
Dim iCCTranCount As Integer
    
    bIsValidKey = False
    
    If mbReadOnlyPerSO Then
        mbReadOnly = True
    Else
        mbReadOnly = False
    End If
        
    If bIsValidPmtID Then
    
        moDmCustPmt.SetColumnValue "TranType", kTranTypeARCR
              
        moDmSalesOrderPmt.SetColumnValue "SOKey", mlSOKey
        
        moDmCustPmt.Where = "CustPmtKey IN (SELECT CustPmtKey FROM tarSalesOrderPmt WHERE SOKey = " & mlSOKey & ")"
      
        iKeyChangeCode = moDmCustPmt.KeyChange()       'Perform Key change code
    
        Select Case iKeyChangeCode
    
            Case kDmKeyNotFound                         'No Key
                
                bIsValidKey = True
                
    
            Case kDmKeyFound
                          
                bIsValidKey = True
                    
            Case kDmKeyNotComplete
                '-- key not completely filled in
    
            Case kDmError
                '-- database error occurred trying to get row
    
            Case Else
                If mbReadOnly And iKeyChangeCode = kDmNotAllowed Then
                    'No messages here
                Else
                    giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedKeyChangeCode, iKeyChangeCode
                End If
    
        End Select
    Else
        
        bIsValidKey = False
    End If

    If bIsValidKey Then
    
        mdOrigTranAmtHC = curTranAmtHC.Amount
        
        lPmtKey = glGetValidLong(moDmCustPmt.GetColumnValue("CustPmtKey"))
        
        curTranAmtNC.Enabled = True
        curTranAmtHC.Enabled = True

        ValMgr.Reset
        
        If (moDmCustPmt.GetColumnValue("CustKey") = mlParentCustKey) And mlParentCustKey <> mlCustKey Then
            chkPmntByParent.Value = vbChecked
        Else
            chkPmntByParent.Value = vbUnchecked
        End If
        
               
        If moDmCustPmt.State = kDmStateAdd Then
                                    
            moDmCustPmt.SetColumnValue "CurrID", msDocCurrID
            moDmCustPmt.SetColumnValue "CurrExchRate", mdCurrExchRate
            
            If mlCurrExchSchdKey = 0 Then
                moDmCustPmt.SetColumnValue "CurrExchSchdKey", Empty
            Else
                moDmCustPmt.SetColumnValue "CurrExchSchdKey", mlCurrExchSchdKey
            End If
    
            moDmSalesOrderPmt.SetColumnValue "SOKey", mlSOKey
            
            moDmCustPmt.SetColumnValue "BatchKey", mlBatchKey
            moDmTenderDetl.SetColumnValue "SourceBatchKey", mlBatchKey
            
            moDmCustPmt.SetColumnValue "CreateType", kCreateTypeStandard
            
            dteTranDate.Text = msBusinessDate
            moDmCustPmt.SetColumnValue "PmtRcptDate", msBusinessDate
            
            If chkPmntByParent.Value = vbChecked Then
                moDmCustPmt.SetColumnValue "CustClassKey", mlParentCustClassKey
            Else
                moDmCustPmt.SetColumnValue "CustClassKey", mlCustClassKey
            End If
            
            moDmCustPmt.SetColumnValue "TranID", Trim(lkuPmtID) & "-" & msTranTypeID
            
       
        End If
            
        'If the sales order payment has been pulled into an AR batch (from cash receipts or invoice creation),
        'display the batch number on the form, and disable the form.
        lBatchKey = glGetValidLong(moDmCustPmt.GetColumnValue("BatchKey"))
                        
        If lBatchKey <> mlBatchKey Then
            txtBatch.Text = gsGetValidStr(moClass.moAppDB.Lookup("BatchID", "tciBatchLog", "BatchKey = " & lBatchKey))
            mbReadOnly = True
        End If
    
        'Check to see if the payment is included in the table tarPendCustPmtAppl
        'If it is, we do not want to allow edits.
        lPmtApplKey = glGetValidLong(moClass.moAppDB.Lookup("BatchKey", "tarPendCustPmtAppl", "ApplyFromPmtKey = " & lPmtKey))
        If lPmtApplKey > 0 Then
            mbReadOnly = True
        End If
        
        txtCurrIDNC.Text = msDocCurrID
        txtCurrIDHC(0).Text = msHomeCurrID
        txtCurrIDHC(1).Text = msHomeCurrID

        SetCurrencyVisibility
        
        'Insure credit card combo contains valid values (if entered before CC activated the valid values are different than if entered after CC activated)
        ResetCreditCardCombo bProcessForCC
        If moDmCustPmt.State <> kDmStateAdd Then
            iTenderTypeClass = giGetValidInt(moClass.moAppDB.Lookup("TenderTypeClass", _
                "tcmTenderType", "TenderTypeKey = " & glGetValidLong(moDmTenderDetl.GetColumnValue("TenderTypeKey"))))
            sReference = gsGetValidStr(moDmTenderDetl.GetColumnValue("RefNo"))
            lTenderTypeKey = glGetValidLong(moDmTenderDetl.GetColumnValue("TenderTypeKey"))
    
            Select Case iTenderTypeClass
                Case kTenderClassCheck    ' check
                    cboCheckType.ListIndex = giListIndexFromItemData(cboCheckType, lTenderTypeKey)
                    txtCheckNum.Text = sReference
                    tabTender.Tab = kTenderTabCheck
                Case kTenderClassCCard    ' credit card
                    cboCardType.ListIndex = giListIndexFromItemData(cboCardType, lTenderTypeKey)
                    txtCCNum.Text = sReference
                    tabTender.Tab = kTenderTabCredit
                Case Else                 ' cash/other
                    cboOtherType.ListIndex = giListIndexFromItemData(cboOtherType, lTenderTypeKey)
                    txtOtherRef.Text = sReference
                    tabTender.Tab = kTenderTabOther
            End Select
            
            moDmCustPmt.SetDirty False, True
        End If
    
        SetFormEnableDisable
        
               
        gbSetFocus Me, dteTranDate
             
    Else
    
               
        gbSetFocus Me, lkuPmtID

    End If
    
   
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ValMgr_KeyChange", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    FormHelpPrefix = "SOZ"         '       Put your prefix here
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Private Function bCreateCustPmtLog()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sPmtNo As String
Dim bInTrans As Boolean
Dim lPmtKey As Long

    bCreateCustPmtLog = False
    
    sPmtNo = Trim(lkuPmtID.Text)
    ' execute stored procedure to
    ' get next key for Customer payments and
    ' create a payment log record
    With moClass.moAppDB
        .BeginTrans
        bInTrans = True
        On Error GoTo ExpectedErrorRoutine
        .SetInParam msCompanyID
        .SetInParam sPmtNo
        .SetInParam "tarCustPmtLog"
        .SetOutParam lPmtKey
        .ExecuteSP ("spSetupCustPmtLog")
        lPmtKey = .GetOutParam(4)
        .ReleaseParams
        If lPmtKey = 0 Then
            .Rollback
        Else
            .CommitTrans
        End If
        On Error GoTo VBRigErrorRoutine
        bInTrans = False
    End With
    
    If lPmtKey = 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidSurrogateKey
        
        Exit Function
    
    End If
    
    moDmCustPmt.SetColumnValue "CustPmtKey", lPmtKey
    
    bCreateCustPmtLog = True
    
    Exit Function
        
        
ExpectedErrorRoutine:
    If bInTrans = True Then
        moClass.moAppDB.Rollback
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bCreateCustPmtLog", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bIsValidPmtID() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lPayerCustKey    As Long
    Dim sRestrict   As String
    Dim lPmtCount   As Long
    Dim colNavRet   As Collection
    Dim sTranNo     As String
    Dim lCustPmtKey As Long
    Dim lRevCustPmtKey As Long

    bIsValidPmtID = False
    
    ValMgr.DisplayMessages = False
    
       
    sTranNo = Trim(lkuPmtID.Text)
    
        
    'See if this payment already exists for this sales order
    If bPmntExistsOnThisSO(mlCustKey) Then
        lPayerCustKey = mlCustKey
    Else
        If bPmntExistsOnThisSO(mlParentCustKey) Then
            lPayerCustKey = mlParentCustKey
        End If
    End If

    If glGetValidLong(lPayerCustKey) = 0 Then 'this is a new payment
    
        If mbReadOnly Or Not mbSecurityRights Then
            giSotaMsgBox Me, moClass.moSysSession, kSOMsgNoNewPmnt
            bIsValidPmtID = False
            Exit Function
        End If
        
        'If the sales order's Bill To is the national account parent, see if this payment ID already exists
        'in the system batch for the parent on another sales order.   If it does, check to see if the payment ID
        'already exists in the system batch for this customer on another sales order.  If it does,
        'give an error message and invalidate.
         If mbDfltPmtByParent Then
            lPayerCustKey = mlParentCustKey
            If bPmntExistsOnOtherSO(lPayerCustKey) Then
                lPayerCustKey = mlCustKey
                If bPmntExistsOnOtherSO(lPayerCustKey) Then
                    giSotaMsgBox Me, moClass.moSysSession, kSOPmntExists
                    bIsValidPmtID = False
                    Exit Function
                End If
            End If
        Else
            lPayerCustKey = mlCustKey
            If bPmntExistsOnOtherSO(lPayerCustKey) Then
                giSotaMsgBox Me, moClass.moSysSession, kSOPmntExists
                bIsValidPmtID = False
                Exit Function
            End If
        End If
        If Not bCreateCustPmtLog Then
            bIsValidPmtID = False
            Exit Function
        End If
    Else
        moDmCustPmt.SetColumnValue "CustPmtKey", lExistingCustPmtKey(lPayerCustKey)
    End If
    
    moDmCustPmt.SetColumnValue "CustKey", lPayerCustKey
           
    ValMgr.DisplayMessages = True
    
    bIsValidPmtID = True
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
Resume Next
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsValidPmtID", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function lExistingCustPmtKey(ByVal lCustKey As Long) As Long
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

Dim sWhere As String

    sWhere = "BatchKey = " & mlBatchKey & " AND CustKey = " & lCustKey & _
        " And TranNo = " & gsQuoted(lkuPmtID.Text) & " AND TranType = " & kTranTypeARCR

    lExistingCustPmtKey = moClass.moAppDB.Lookup("CustPmtKey", "tarPendCustPmt", sWhere)
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lExistingCustPmtKey", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bPmntExistsOnOtherSO(ByVal lCustKey As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

Dim sSQL        As String
Dim rs          As Object
 
    bPmntExistsOnOtherSO = False
    
    sSQL = "SELECT tarPendCustPmt.CustPmtKey FROM tarPendCustPmt " & _
            "JOIN  tarSalesOrderPmt ON tarPendCustPmt.CustPmtKey = tarSalesOrderPmt.CustPmtKey " & _
            "WHERE TranNo = " & gsQuoted(lkuPmtID.Text) & _
            " AND TranType = " & kTranTypeARCR & " AND BatchKey = " & mlBatchKey & _
            " AND SOKey <> " & mlSOKey & " AND tarPendCustPmt.CustKey = " & lCustKey
            
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If Not rs.IsEOF Then
        bPmntExistsOnOtherSO = True
    End If
    
    If Not rs Is Nothing Then
        Set rs = Nothing
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bPmntExistsOnOtherSO", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
            
End Function

Private Function bPmntExistsOnThisSO(ByVal lCustKey As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

Dim sSQL        As String
Dim rs          As Object
 
    bPmntExistsOnThisSO = False
    
    sSQL = "SELECT tarPendCustPmt.CustPmtKey FROM tarPendCustPmt " & _
            "JOIN  tarSalesOrderPmt ON tarPendCustPmt.CustPmtKey = tarSalesOrderPmt.CustPmtKey " & _
            "WHERE TranNo = " & gsQuoted(lkuPmtID.Text) & _
            " AND TranType = " & kTranTypeARCR & " AND BatchKey = " & mlBatchKey & _
            " AND SOKey = " & mlSOKey & " AND tarPendCustPmt.CustKey = " & lCustKey
            
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If Not rs.IsEOF Then
        bPmntExistsOnThisSO = True
    End If
    
    If Not rs Is Nothing Then
        Set rs = Nothing
    End If
            
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bPmntExistsOnOtherSO", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Function bIsValidPmntDate() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    bIsValidPmntDate = False
    
    If Len(Trim(dteTranDate.Text)) = 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgRequiredField, gsStripChar(lblPmntDate.Caption, "&")
        Exit Function
    End If
    
    moDmCustPmt.SetColumnValue "TranDate", dteTranDate.Text
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsValidPmntDate", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bIsValidPmntAmt(ByVal bIsHC As Boolean, Optional ByVal bValidate As Boolean = True) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

'When validation is firing do to event call from CC Transactions, don't do validations, just update amounts

Dim dPrePmtsAmtHC As Double
Dim dTranAmtHC As Double
Dim dTranAmtNC As Double

    bIsValidPmntAmt = False
    
    ValMgr.DisplayMessages = False
    
    If bValidate Then
        If (bIsHC And curTranAmtHC.Amount <= 0) Or ((Not bIsHC) And curTranAmtNC.Amount <= 0) Then
            giSotaMsgBox Me, moClass.moSysSession, kSOmsgPmntGreaterThan0, gsBuildString(ksPayment, moClass.moAppDB, moClass.moSysSession)
            Exit Function
        End If
    End If
    
    If bIsHC Then
        If txtCurrIDNC.Text <> txtCurrIDHC(0).Text Then
            dTranAmtNC = gfRound(curTranAmtHC.Amount * (1 / gdGetValidDbl(moDmCustPmt.GetColumnValue("CurrExchRate"))), _
                miHomeRoundMeth, miHomePriceDecPlaces, miHomeRoundPrecision)
        Else
            dTranAmtNC = curTranAmtHC.Amount
        End If
       
        dTranAmtHC = curTranAmtHC.Amount
    Else
        If txtCurrIDNC.Text <> txtCurrIDHC(0).Text Then
            dTranAmtHC = gfRound(curTranAmtNC.Amount * gdGetValidDbl(moDmCustPmt.GetColumnValue("CurrExchRate")), _
                miHomeRoundMeth, miHomePriceDecPlaces, miHomeRoundPrecision)
        Else
            dTranAmtHC = curTranAmtNC.Amount
        End If
        
        dTranAmtNC = curTranAmtNC.Amount
    End If
    
    'Get the total prepayments amount.  Get the last valid value from the validation manager and subtract it, then add in the new value.
    dPrePmtsAmtHC = curPrePmtsHC.Amount - gdGetValidDbl(ValMgr.ValidValue(curTranAmtHC)) + dTranAmtHC
            
    If bValidate Then
        'Check to see if prepayments exceed the sales order amount and warn if so.
        If (curOrderAmtHC.Amount - dPrePmtsAmtHC) < 0 Then
            'Warn - Prepayments exceed sales order amount.
            If giSotaMsgBox(Me, moClass.moSysSession, kSOMsgPmntExceedsSalesAmt) = kretCancel Then
                Exit Function
            End If
        End If
    End If
    
    ValMgr.DisplayMessages = True
    
    If bIsHC Then
        curTranAmtNC.Amount = dTranAmtNC
        ValMgr.SetControlValid curTranAmtNC
    Else
        curTranAmtHC.Amount = dTranAmtHC
        ValMgr.SetControlValid curTranAmtHC
    End If
    
    'Update the total prepayments amount on the form
    curPrePmtsHC.Amount = dPrePmtsAmtHC
    curBalanceHC.Amount = curOrderAmtHC.Amount - curPrePmtsHC.Amount
    
    moDmTenderDetl.SetColumnValue "Amount", curTranAmtNC.Amount
    moDmTenderDetl.SetColumnValue "TranAmtHC", curTranAmtHC.Amount
    
    moDmCustPmt.SetColumnValue "UnappliedAmt", dTranAmtNC
    moDmCustPmt.SetColumnValue "UnappliedAmtHC", dTranAmtHC
    
    'moDmCustPmt.SetDirty True, True

    bIsValidPmntAmt = True
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsValidPmntAmt", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Sub ValMgr_Validate(ctlLostFocus As Object, iReturn As _
SOTAVM.SOTA_VALID_RETURN_TYPES, sMessage As String, ByVal iLevel As _
SOTAVM.SOTA_VALIDATION_LEVELS)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    iReturn = SOTA_VALID
    
    If moDmCustPmt.State = kDmStateNone Then
        Exit Sub
    End If
       
    Select Case ctlLostFocus.Name
        Case "curTranAmtHC"
            iReturn = bIsValidPmntAmt(True)
        Case "curTranAmtNC"
            iReturn = bIsValidPmntAmt(False)
          
    End Select
    
               
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ValMgr_Validate", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub




#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_Paint(Index As Integer)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If
#If CUSTOMIZER Then

#If CUSTOMIZER And CONTROLS Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
               Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If
#End If


