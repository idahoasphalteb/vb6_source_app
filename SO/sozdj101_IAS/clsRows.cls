VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsRows"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'**********************************************************************
'     Name: clsRows
'     Desc: This class has the following properties:
'           Rows       - a collection of the rows on the grid (clsRow)
'
'           The public methods are:
'           Add        - used to add a row to the collection.
'
' Original: 07-26-2004 Jenny Wilson
'     Mods: mm/dd/yy XXX
'**********************************************************************
Private moRows As Collection

Public Property Get Row() As Collection
    Set Row = moRows
End Property

Public Sub Init()
    If Not moRows Is Nothing Then
        Set moRows = Nothing
    End If
    
    Set moRows = New Collection
End Sub

'Public Sub Add(lRow As Long, bProcessRowChange As Boolean)
''Add a new row to the rows collection
'Dim oNewRow As New clsRow
'
'    oNewRow.RowNo = lRow
'    oNewRow.ProcessRowChange = bProcessRowChange
'
'    moRows.Add oNewRow, CStr(lRow)
'
'End Sub

Public Sub Delete(lRow As Long)
'Remove row from the collection and then resequence the rows to align with the grid rows.
Dim oRow As clsRow
Dim iRowNum As Integer
Dim oNewRows As New Collection

    moRows.Remove CStr(lRow)
    
    iRowNum = 1
    'Now need to renumber and rekey, retaining the other properties.
    For Each oRow In moRows
    
        oRow.RowNo = iRowNum
        oNewRows.Add oRow, CStr(iRowNum)
        iRowNum = iRowNum + 1
    
    Next oRow
    
    Set moRows = oNewRows
    Set oNewRows = Nothing
    
       
End Sub


Public Sub Clear()
Dim ix As Integer
Dim oRow As clsRow

    If Not moRows Is Nothing Then
        For ix = 1 To moRows.Count
            moRows.Remove 1
        Next ix
    End If
    
End Sub

Public Function ProcessRowChange(lRow As Long) As Boolean
    moRows(lRow).ProcessRowChange = ProcessRowChange
End Function



