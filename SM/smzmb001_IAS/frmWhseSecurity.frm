VERSION 5.00
Begin VB.Form frmWhseSecurity 
   Caption         =   "Warehouse Security"
   ClientHeight    =   7440
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7860
   ControlBox      =   0   'False
   Icon            =   "frmWhseSecurity.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7440
   ScaleWidth      =   7860
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton CmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   4560
      TabIndex        =   8
      Top             =   120
      Width           =   1215
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save"
      Height          =   375
      Left            =   6480
      TabIndex        =   9
      Top             =   120
      Width           =   1215
   End
   Begin VB.CommandButton cmdRemove 
      Caption         =   "<"
      Height          =   255
      Left            =   3480
      TabIndex        =   4
      Top             =   3720
      Width           =   615
   End
   Begin VB.CommandButton cmdRemoveAll 
      Caption         =   "<<"
      Height          =   255
      Left            =   3480
      TabIndex        =   7
      Top             =   3360
      Width           =   615
   End
   Begin VB.CommandButton cmdAddAll 
      Caption         =   ">>"
      Height          =   255
      Left            =   3480
      TabIndex        =   3
      Top             =   2760
      Width           =   615
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   ">"
      Height          =   255
      Left            =   3480
      TabIndex        =   2
      Top             =   2400
      Width           =   615
   End
   Begin VB.ListBox lstWhsePerm 
      Height          =   5715
      Left            =   4440
      MultiSelect     =   2  'Extended
      Sorted          =   -1  'True
      TabIndex        =   6
      Top             =   1440
      Width           =   3135
   End
   Begin VB.ListBox lstAllWhse 
      Height          =   5715
      Left            =   120
      MultiSelect     =   2  'Extended
      Sorted          =   -1  'True
      TabIndex        =   1
      Top             =   1440
      Width           =   3135
   End
   Begin VB.Label lblpermission 
      Caption         =   "Permission to view GPP and otherrReport data for these warehouess"
      Height          =   495
      Left            =   4440
      TabIndex        =   5
      Top             =   840
      Width           =   3015
   End
   Begin VB.Label lblPossilbeWhse 
      Caption         =   "Available Warehouses"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   1200
      Width           =   1815
   End
End
Attribute VB_Name = "frmWhseSecurity"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public sUserGroupID As String
Public oSysDB As DASDatabase
Private bHasChanged As Boolean

Private Sub cmdAdd_Click()
    AddRemovePermission lstAllWhse, lstWhsePerm, False
End Sub

Private Sub cmdAddAll_Click()
    AddRemovePermission lstAllWhse, lstWhsePerm, True
End Sub

Private Sub CmdCancel_Click()
    bHasChanged = False
    Unload Me
End Sub

Private Sub cmdRemove_Click()
    AddRemovePermission lstWhsePerm, lstAllWhse, False
End Sub

Private Sub cmdRemoveAll_Click()
    AddRemovePermission lstWhsePerm, lstAllWhse, True
End Sub

Private Sub cmdSave_Click()
    If bHasChanged = True Then
        Call SavePermissions
    End If
    
    bHasChanged = False
    Unload Me
End Sub

Private Sub Form_Load()
    If Trim(sUserGroupID) = Empty Then
        Unload Me
    End If
    
    Call GetWhse
    
    Call GetPermissions
    
    
End Sub

Sub GetWhse()
    On Error GoTo Error
    
    Dim ErrMsg As String
    Dim i As Integer
    Dim WhseID As String
    Dim sSQL As String
    Dim RS As DASRecordSet
    
    lstAllWhse.Clear
    
    'Get warehouses that are not part of the current permissions
    sSQL = "Select distinct w.WhseID from timWarehouse w with(NoLock) Left outer Join tsmUserGroupWhseExt_SGS g with(NOLock) On w.WhseID = g.WhseID and g.UserGroupID = " & gsQuoted(sUserGroupID) & " Where g.WhseID is Null "
    
    Set RS = oSysDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    'Continue only if the recordset is valid
    If RS Is Nothing Then GoTo DisplaySecEventPermsBail:
    
    With RS
        Do While Not .IsEOF
            lstAllWhse.AddItem Trim(.Field("WhseID"))
            .MoveNext
        Loop
    End With
    
DisplaySecEventPermsBail:
    'Local object cleanup
    If Not RS Is Nothing Then
        RS.Close
        Set RS = Nothing
    End If
    Exit Sub
Error:
    ErrMsg = Me.Name & ".GetWhse() Error: " & Err.Number & " " & Err.Description
    
    Err.Clear
    
    MsgBox ErrMsg, vbExclamation, "SAGE 500"

End Sub

Sub GetPermissions()
    On Error GoTo Error
    
    Dim ErrMsg As String
    Dim i As Integer
    Dim WhseID As String
    Dim sSQL As String
    Dim RS As DASRecordSet
    
    lstWhsePerm.Clear
    
    'Get warehouses that are not part of the current permissions
    sSQL = "Select distinct g.WhseID from tsmUserGroupWhseExt_SGS g with(NOLock) where g.UserGroupID = " & gsQuoted(sUserGroupID)
    
    Set RS = oSysDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    'Continue only if the recordset is valid
    If RS Is Nothing Then GoTo DisplaySecEventPermsBail:
    
    With RS
        Do While Not .IsEOF
            lstWhsePerm.AddItem Trim(.Field("WhseID"))
            RS.MoveNext
        Loop
    End With
    
DisplaySecEventPermsBail:
    'Local object cleanup
    If Not RS Is Nothing Then
        RS.Close
        Set RS = Nothing
    End If
    Exit Sub
Error:
    ErrMsg = Me.Name & ".GetPermissions() Error: " & Err.Number & " " & Err.Description
    
    Err.Clear
    
    MsgBox ErrMsg, vbExclamation, "SAGE 500"

End Sub

Sub SavePermissions()
    On Error GoTo Error
    
    Dim ErrMsg As String
    Dim i As Integer
    Dim WhseID As String
    Dim sSQL As String
    
    If bHasChanged = False Then Exit Sub
    
    'First Clear all existing permissions
    sSQL = "Delete tsmUserGroupWhseExt_SGS where UserGroupID = " & gsQuoted(sUserGroupID)
    oSysDB.ExecuteSQL sSQL
    
    'Now loop through and save new permissions
    For i = 0 To lstWhsePerm.ListCount - 1
        WhseID = Trim(lstWhsePerm.List(i))
        
        If WhseID <> Empty Then
            sSQL = "Insert into tsmUserGroupWhseExt_SGS(UserGroupID, WhseID)"
            sSQL = sSQL & "Values(" & gsQuoted(sUserGroupID) & "," & gsQuoted(WhseID) & ")"
            oSysDB.ExecuteSQL sSQL
        End If
    Next
    
    'If successfull then clear change flag
    bHasChanged = False
    
    Exit Sub
Error:
    ErrMsg = Me.Name & ".SavePermissions() Error: " & Err.Number & " " & Err.Description
    
    Err.Clear
    
    MsgBox ErrMsg, vbExclamation, "SAGE 500"
        
End Sub

Private Sub AddRemovePermission(LstRemove As ListBox, LstAdd As ListBox, Optional bRemoveAll As Boolean = False)
    On Error GoTo Error
    
    Dim ErrMsg As String
    Dim i As Integer
    Dim j As Integer
    
    If bRemoveAll = True Then
        If LstRemove.ListCount > 0 Then bHasChanged = True
        For i = 0 To LstRemove.ListCount - 1
            LstAdd.AddItem LstRemove.List(i)
        Next
        
        LstRemove.Clear
        
        Exit Sub
    End If
    
    
    If LstRemove.SelCount > 0 Then
        bHasChanged = True
        j = 0
StartOver:
        For i = j To LstRemove.ListCount - 1
            If LstRemove.Selected(i) = True Then
                LstAdd.AddItem LstRemove.List(i)
                LstRemove.RemoveItem (i)
                
                j = i - 1
                If j < 0 Then j = 0
                GoTo StartOver
            End If
        Next
    End If
    
    Exit Sub
Error:
    ErrMsg = Me.Name & ".AddRemovePermission() Error: " & Err.Number & " " & Err.Description
    
    Err.Clear
    
    MsgBox ErrMsg, vbExclamation, "SAGE 500"
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error GoTo Error
    Dim ErrMsg As String
    
    If bHasChanged = True Then
        If MsgBox("Do you want to save your changes?", vbYesNo, "SAGE 500") = vbYes Then
            Call SavePermissions
        End If
    End If

    Exit Sub
Error:
    ErrMsg = Me.Name & ".Form_QueryUnload() Error: " & Err.Number & " " & Err.Description
    
    Err.Clear
    
    MsgBox ErrMsg, vbExclamation, "SAGE 500"
End Sub
