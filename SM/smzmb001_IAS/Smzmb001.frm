VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#84.0#0"; "LookupView.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Begin VB.Form frmSecurGroupMaint 
   Caption         =   "Maintain Security Groups"
   ClientHeight    =   6225
   ClientLeft      =   2625
   ClientTop       =   1710
   ClientWidth     =   8055
   HelpContextID   =   32747
   Icon            =   "Smzmb001.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6225
   ScaleWidth      =   8055
   Begin VB.CommandButton CmdWhseSecurity 
      Caption         =   "Whse Security"
      Height          =   360
      Left            =   6630
      TabIndex        =   39
      ToolTipText     =   "Select warehouses this security group is allowed to view Sales data on GPP and other reports"
      Top             =   960
      Width           =   1335
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   8055
      _ExtentX        =   14208
      _ExtentY        =   741
      Style           =   0
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   21
      Top             =   0
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   26
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   27
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   28
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   29
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   30
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   31
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   32
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   34
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin VB.CommandButton cmdCopyFrom 
      Caption         =   "Co&py From"
      Height          =   360
      Left            =   6630
      TabIndex        =   5
      Top             =   600
      WhatsThisHelpID =   32779
      Width           =   1335
   End
   Begin VB.ComboBox lstModuleID 
      Height          =   315
      Left            =   1410
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   1560
      WhatsThisHelpID =   32778
      Width           =   1005
   End
   Begin VB.Frame fraSecurityGroup1 
      Height          =   60
      Left            =   -60
      TabIndex        =   17
      Top             =   1380
      Width           =   8175
   End
   Begin VB.Frame fraSecurityGroup2 
      Height          =   60
      Left            =   -60
      TabIndex        =   16
      Top             =   5535
      Width           =   8175
   End
   Begin TabDlg.SSTab tabSecurGroupMaint 
      Height          =   3495
      Left            =   90
      TabIndex        =   10
      Top             =   1980
      WhatsThisHelpID =   32775
      Width           =   7860
      _ExtentX        =   13864
      _ExtentY        =   6165
      _Version        =   393216
      Tabs            =   2
      Tab             =   1
      TabsPerRow      =   4
      TabHeight       =   529
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "&Tasks"
      TabPicture(0)   =   "Smzmb001.frx":23D2
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "fraTaskPermissions"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Security &Events"
      TabPicture(1)   =   "Smzmb001.frx":23EE
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "fraSecEventPermissions"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      Begin VB.Frame fraSecEventPermissions 
         Height          =   3045
         Left            =   120
         TabIndex        =   35
         Top             =   330
         Width           =   7620
         Begin FPSpreadADO.fpSpread grdSecEventPerm 
            Height          =   2175
            Left            =   120
            TabIndex        =   36
            TabStop         =   0   'False
            Top             =   270
            WhatsThisHelpID =   32773
            Width           =   7380
            _Version        =   524288
            _ExtentX        =   13018
            _ExtentY        =   3836
            _StockProps     =   64
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            SpreadDesigner  =   "Smzmb001.frx":240A
            AppearanceStyle =   0
         End
         Begin Threed.SSPanel pnlDfltPermSecEventPerm 
            Height          =   525
            Left            =   120
            TabIndex        =   37
            Top             =   2460
            Width           =   7365
            _Version        =   65536
            _ExtentX        =   12991
            _ExtentY        =   926
            _StockProps     =   15
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BevelOuter      =   0
            Begin VB.ComboBox lstDfltPermSecEventPerm 
               Enabled         =   0   'False
               Height          =   315
               Left            =   2760
               Style           =   2  'Dropdown List
               TabIndex        =   20
               Top             =   105
               WhatsThisHelpID =   32771
               Width           =   2415
            End
            Begin VB.CommandButton cmdApply 
               Caption         =   "&Apply"
               Enabled         =   0   'False
               Height          =   360
               Index           =   1
               Left            =   5250
               TabIndex        =   38
               Top             =   90
               WhatsThisHelpID =   32770
               Width           =   765
            End
            Begin VB.Label lblDfltPermSecEventPerm 
               AutoSize        =   -1  'True
               Caption         =   "Default Permission"
               Height          =   195
               Left            =   1350
               TabIndex        =   22
               Top             =   150
               Width           =   1365
            End
         End
      End
      Begin VB.Frame fraTaskPermissions 
         Height          =   3045
         Left            =   -74880
         TabIndex        =   11
         Top             =   330
         Width           =   7620
         Begin FPSpreadADO.fpSpread grdTaskPerm 
            Height          =   2175
            Left            =   120
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   270
            WhatsThisHelpID =   32767
            Width           =   7380
            _Version        =   524288
            _ExtentX        =   13018
            _ExtentY        =   3836
            _StockProps     =   64
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            SpreadDesigner  =   "Smzmb001.frx":281E
            AppearanceStyle =   0
         End
         Begin Threed.SSPanel pnlDfltPermTaskPerm 
            Height          =   525
            Left            =   120
            TabIndex        =   18
            Top             =   2460
            Width           =   7365
            _Version        =   65536
            _ExtentX        =   12991
            _ExtentY        =   926
            _StockProps     =   15
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BevelOuter      =   0
            Begin VB.CommandButton cmdApply 
               Caption         =   "&Apply"
               Enabled         =   0   'False
               Height          =   360
               Index           =   0
               Left            =   5250
               TabIndex        =   15
               Top             =   90
               WhatsThisHelpID =   32765
               Width           =   765
            End
            Begin VB.ComboBox lstDfltPermTaskPerm 
               Enabled         =   0   'False
               Height          =   315
               Left            =   2760
               Style           =   2  'Dropdown List
               TabIndex        =   14
               Top             =   105
               WhatsThisHelpID =   32764
               Width           =   2415
            End
            Begin VB.Label lblDfltPermTaskPerm 
               AutoSize        =   -1  'True
               Caption         =   "Default Permission"
               Height          =   195
               Left            =   1350
               TabIndex        =   13
               Top             =   150
               Width           =   1365
            End
         End
      End
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   5835
      WhatsThisHelpID =   73
      Width           =   8055
      _ExtentX        =   0
      _ExtentY        =   688
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtCopyFrom 
      Height          =   285
      Left            =   4980
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   660
      Visible         =   0   'False
      WhatsThisHelpID =   32760
      Width           =   1215
      _Version        =   65536
      _ExtentX        =   2143
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin LookupViewControl.LookupView navCopyFrom 
      Height          =   285
      Left            =   6240
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   660
      Visible         =   0   'False
      WhatsThisHelpID =   32759
      Width           =   285
      _ExtentX        =   503
      _ExtentY        =   503
   End
   Begin LookupViewControl.LookupView navMain 
      Height          =   285
      Left            =   4515
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   600
      WhatsThisHelpID =   14
      Width           =   285
      _ExtentX        =   503
      _ExtentY        =   503
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtUserGroupID 
      Height          =   285
      Left            =   1410
      TabIndex        =   1
      Top             =   615
      WhatsThisHelpID =   32756
      Width           =   3060
      _Version        =   65536
      _ExtentX        =   5397
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      lMaxLength      =   30
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtDescription 
      Height          =   285
      Left            =   1410
      TabIndex        =   4
      Top             =   960
      WhatsThisHelpID =   32755
      Width           =   4515
      _Version        =   65536
      _ExtentX        =   7964
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      lMaxLength      =   40
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   33
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Label txtModuleName 
      Height          =   255
      Left            =   2520
      TabIndex        =   23
      Top             =   1605
      Width           =   5415
   End
   Begin VB.Label lblModuleID 
      AutoSize        =   -1  'True
      Caption         =   "Module ID"
      Height          =   195
      Left            =   150
      TabIndex        =   8
      Top             =   1620
      Width           =   735
   End
   Begin VB.Label lblUserGroupID 
      AutoSize        =   -1  'True
      Caption         =   "Security Group"
      Height          =   195
      Left            =   150
      TabIndex        =   0
      Top             =   645
      Width           =   1050
   End
   Begin VB.Label lblUserGroupName 
      AutoSize        =   -1  'True
      Caption         =   "Description"
      Height          =   195
      Left            =   150
      TabIndex        =   3
      Top             =   1020
      Width           =   1155
   End
End
Attribute VB_Name = "frmSecurGroupMaint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'////////////////////////////////////////////////////////////////////////////
'     Name: frmSecurGroupMaint
'     Desc: Security Group Maintenance
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original:
'     Mods:
'////////////////////////////////////////////////////////////////////////////

Option Explicit
#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

'PUBLIC
    Public moSotaObjects                    As New Collection
    
'PRIVATE
    'Browse navigator filter
    Private miFilter                        As Integer
    
    'UI management variables
    Private mbValidating                    As Boolean
    Private mbContextMenuInvoked            As Boolean
    Private mbEnterAsTab                    As Boolean
    Private mbShiftKeyHeldDown              As Boolean
    
    'ObjMgr "utility object" implementation
    Private moUtility                       As Object
    Private Const kclsFWUtilityObj = "ObjMgr.Utility"
    
    'Tab key tracking variables
    Private mbTaskGridTabKey                As Boolean
    Private mbSecEventGridTabKey            As Boolean
    
    'Context Menu Object
    Private moContextMenu                   As Object
    
    'Private form variables
    Private moClass                         As Object
    Private msOrigUserGroup                 As String
    Private msOrigDescription               As String
    Private miState                         As Integer
    Private mbCancelShutDown                As Boolean
    Private mbLoadSuccess                   As Boolean
    Private mbSaved                         As Boolean
    Private mlRunMode                       As Long
    Private miSecurityLevel                 As Integer
    Private miUserGroupUpdCtr                 As Integer
    
    'Used to reference tab's combobox text
    Private msExcluded                      As String
    Private msDisplayOnly                   As String
    Private msNormal                        As String
    Private msSupervisory                   As String
    Private msNo                            As String
    Private msYes                           As String
    
    'Used to define the current grid's sort order
    Private msTaskSortType                  As String
    Private msSecEventSortType              As String
    'Form sizing variables
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long
    
    'Determines whether all rows of the current grid are selected
    Private mbAllTaskPermRowsSelected       As Boolean
    Private mbAllSecEventPermRowsSelected   As Boolean
    
    'Determines current number of rows selected for the active grid
    Private mlTaskPermRowsSelected          As Long
    Private mlSecEventPermRowsSelected      As Long
    
    'Detemines whether extended selection is active for the current grid
    Private mlExtendSelSecEventPerm         As Long
    Private mlExtendSelTaskPerm             As Long
    
    'Determines whether grid has been displayed for the current ModuleID selection
    Private mbTaskNotFirstTime              As Boolean
    Private mbSecEventNotFirstTime          As Boolean
    
    'Determines access to Security Group Listing
    Private miSecGrpListPerm          As Integer

'PRIVATE CONSTANTS
    Private Const kAll = 1900
    Private Const kAllModules = 1902
    Private Const kSecEventDesc = 2111
    Private Const kTaskDesc = 2112
    Private Const kSecurityPermission = 2113
    Private Const kModuleID = 6002
    Private Const ksInsertRow = "INSERT"
    Private Const ksUpdateRow = "UPDATE"
    Private Const ksSortTypeAscending = "ASCENDING"
    Private Const ksSortTypeDescending = "DESCENDING"
    Private Const ksTaskPermGridName = "grdTaskPerm"
    Private Const ksSecEventPermGridName = "grdSecEventPerm"
    Private Const ktskSecurGroupMNT = 16842962
    Private Const ktskSecurityGroupListing = 17432579
    Private Const kiTabKeyCode = 9
    Private Const kiDisplayPermsFailure = -1
    
    'Security event permissions
    Private Const kiNo = 0
    Private Const kiYes = 1
    
    'Represents grids "Apply" command button
    Private Const kcmdTaskPermApply = 0
    Private Const kcmdSecEventPermApply = 1
    
    'ObjMgr "utility object" implementation
    Private Const kAllPermissions = 0
    Private Const kModulePermissions = 1
    
    'Represents each tab
    Private Const kTaskPermTab = 0
    Private Const kSecEventPermTab = 1
    
    'Task Permission column constants
    Private Const kTaskPermMaxCols = 9
    Private Const kColTaskModuleID As Integer = 1
    Private Const kColTaskPermTaskDesc As Integer = 2
    Private Const kColTaskPermPermissionText As Integer = 3
    Private Const kColTaskPermTaskID As Integer = 4
    Private Const kColTaskPermPermissionValue As Integer = 5
    Private Const kColTaskPermOldPermValue As Integer = 6
    Private Const kColTaskPermInsOrUpdRow As Integer = 7
    Private Const kColTaskPermUpdateCounter As Integer = 8
    Private Const kColTaskPermBlankColumn As Integer = 9
    
    'Security Event Permission column constants
    Private Const kSecEventMaxCols = 9
    Private Const kColSecEventPermModuleID As Integer = 1
    Private Const kColSecEventPermDescription As Integer = 2
    Private Const kColSecEventPermAuthorizedText As Integer = 3
    Private Const kColSecEventPermSecEventID As Integer = 4
    Private Const kColSecEventPermAuthorizedValue As Integer = 5
    Private Const kColSecEventPermOldAuthValue As Integer = 6
    Private Const kColSecEventPermInsOrUpdRow As Integer = 7
    Private Const kColSecEventPermUpdateCounter As Integer = 8
    Private Const kColSecEventPermBlankColumn As Integer = 9
    
    'Used to specify type of row selection request
    Private Const kSelectRow = 1
    Private Const kUNSelectRow = 2
    
    Private Const kModuleIDCol = 1
    Private Const kDescriptionCol = 2
    Private Const kPermissionCol = 3
    
    'Represents animated status state
    Private Const kStatusNone = 0
    Private Const kStatusStart = 1
    Private Const kStatusAdd = 2
    Private Const kStatusEdit = 3
    Private Const kStatusSave = 4
    Private Const kStatusLocked = 5
    Private Const kstatusInquiry = 6
    Private Const kStatusBusy = 7
    
Const VBRIG_MODULE_ID_STRING = "SMZMB001.FRM"



Private Function bUpdateSecurEvent(oDB As Object, ByVal sUserGroupID As String, ByVal sSecurEventID As String, ByVal iPermission As Integer, ByVal iUpdateCounter As Integer, iCommitFlag As Integer) As Boolean

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim lReturnCode As Long
    
    'Default return value
    bUpdateSecurEvent = False
    
    'Setup and execute stored procedure to insert new tsmUserGroup row
    With oDB
        .SetInParam sUserGroupID
        .SetInParam sSecurEventID
        .SetInParam iPermission
        .SetInParam iUpdateCounter
        .SetInParam iCommitFlag
        .SetOutParam lReturnCode
        .ExecuteSP "sptsmSecurEventPermUpdate"
        lReturnCode = .GetOutParam(6)
        .ReleaseParams
    End With
    
    'Evaluate stored procedure outcode to determine return value
    If lReturnCode = kdasSuccess Then
        bUpdateSecurEvent = True
    End If
    
    Exit Function
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bUpdateSecurEvent", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++

End Function


Private Function bUpdateTaskPerm(oDB As Object, ByVal sUserGroupID As String, ByVal lTaskID As Long, ByVal iPermission As Integer, ByVal iUpdateCounter As Integer, iCommitFlag As Integer) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim lReturnCode As Long
    
    'Default return value
    bUpdateTaskPerm = False
    
    'Setup and execute stored procedure to insert new tsmUserGroup row
    With oDB
        .SetInParam sUserGroupID
        .SetInParam lTaskID
        .SetInParam iPermission
        .SetInParam iUpdateCounter
        .SetInParam iCommitFlag
        .SetOutParam lReturnCode
        .ExecuteSP "sptsmTaskPermUpdate"
        lReturnCode = .GetOutParam(6)
        .ReleaseParams
    End With
    
    'Evaluate stored procedure outcode to determine return value
    If lReturnCode = kdasSuccess Then
        bUpdateTaskPerm = True
    End If
    
    Exit Function
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bUpdateTaskPerm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++

End Function

Private Sub BindToolBar()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    
    With tbrMain
        .Build sotaTB_MULTI_ROW
        .RemoveButton kTbRenameId
        .RemoveButton kTbMemo
        .SecurityLevel = miSecurityLevel
        .RemoveSeparator kTbPrint
    End With
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindToolBar", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bInsertSecurEvent(oDB As Object, ByVal sUserGroupID As String, ByVal sSecurEventID As String, ByVal iPermission As Integer, iCommitFlag As Integer) As Boolean

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim lReturnCode As Long
    
    'Default return value
    bInsertSecurEvent = False
    
    'Setup and execute stored procedure to insert new tsmUserGroup row
    With oDB
        .SetInParam sUserGroupID
        .SetInParam sSecurEventID
        .SetInParam iPermission
        .SetInParam iCommitFlag
        .SetOutParam lReturnCode
        .ExecuteSP "sptsmSecurEventPermInsert"
        lReturnCode = .GetOutParam(5)
        .ReleaseParams
    End With
    
    'Evaluate stored procedure outcode to determine return value
    If lReturnCode = kdasSuccess Then
        bInsertSecurEvent = True
    End If
    
    Exit Function
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bInsertSecurEvent", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++

End Function

Private Function bInsertTaskPerm(oDB As Object, ByVal sUserGroupID As String, ByVal lTaskID As Long, ByVal iPermission As Integer, iCommitFlag As Integer) As Boolean

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim lReturnCode As Long
    
    'Default return value
    bInsertTaskPerm = False
    
    'Setup and execute stored procedure to insert new tsmUserGroup row
    With oDB
        .SetInParam sUserGroupID
        .SetInParam lTaskID
        .SetInParam iPermission
        .SetInParam iCommitFlag
        .SetOutParam lReturnCode
        .ExecuteSP "sptsmTaskPermInsert"
        lReturnCode = .GetOutParam(5)
        .ReleaseParams
    End With
    
    'Evaluate stored procedure outcode to determine return value
    If lReturnCode = kdasSuccess Then
        bInsertTaskPerm = True
    End If
    
    Exit Function
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bInsertTaskPerm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++

End Function

Private Function bInsertUserGroup(oDB As Object, ByVal sUserGroupID As String, ByVal sUserGroupDesc As String, ByVal iCommitFlag As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim lReturnCode As Long
    
    'Default return value
    bInsertUserGroup = False
    
    'Setup and execute stored procedure to insert new tsmUserGroup row
    With oDB
        .SetInParam sUserGroupID
        .SetInParam sUserGroupDesc
        .SetInParam iCommitFlag
        .SetOutParam lReturnCode
        .ExecuteSP "sptsmUserGroupInsert"
        lReturnCode = .GetOutParam(4)
        .ReleaseParams
    End With
    
    'Evaluate stored procedure outcode to determine return value
    If lReturnCode = kdasSuccess Then
        bInsertUserGroup = True
    End If
    
    Exit Function
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bInsertUserGroup", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bSetupNavMain() As Boolean

    Dim sRestrictClause As String
    
    bSetupNavMain = False
    
    If Len(Trim(navMain.Tag)) = 0 Then
        sRestrictClause = ""
        bSetupNavMain = gbLookupInit(navMain, moClass, moClass.moAppDB, kNavEntSMUserGroup, sRestrictClause)
        If bSetupNavMain Then navMain.Tag = kNavEntSMUserGroup
    Else
        bSetupNavMain = True
    End If
    
    Exit Function
        
End Function

Private Function bUpdateUserGroup(oDB As Object, ByVal sUserGroupID As String, ByVal sUserGroupDesc As String, ByVal iUpdateCounter As Integer, iCommitFlag As Integer) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim lReturnCode As Long
    
    'Default return value
    bUpdateUserGroup = False
    
    'Setup and execute stored procedure to insert new tsmUserGroup row
    With oDB
        .SetInParam sUserGroupID
        .SetInParam sUserGroupDesc
        .SetInParam iUpdateCounter
        .SetInParam iCommitFlag
        .SetOutParam lReturnCode
        .ExecuteSP "sptsmUserGroupUpdate"
        lReturnCode = .GetOutParam(5)
        .ReleaseParams
    End With
    
    'Evaluate stored procedure outcode to determine return value
    If lReturnCode = kdasSuccess Then
        bUpdateUserGroup = True
    End If
    
    Exit Function
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bUpdateUserGroup", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++

    
End Function

Private Function bValidUserGroup(oDB As Object, sUserGroupID As String) As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++

    On Error GoTo ExpectedErrorRoutine:
    
    Dim oRs     As Object
    Dim sSQL    As String
        
    'Default return value
    bValidUserGroup = False
    
    'Validate input argument
    If Len(sUserGroupID) > 0 Then
        'Build SQL stmt and execute
        sSQL = "#smGetSecurityGroup;" & kSQuote & sUserGroupID & kSQuote & ";"
        Set oRs = oDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        'Continue only if recordset is valid
        If Not oRs Is Nothing Then
            With oRs
                If Not .IsEmpty Then
                    'Set return value to indicate success
                    bValidUserGroup = True
                    'Populate/update header controls
                    txtUserGroupID = .Field("UserGroupID")
                    txtDescription = .Field("UserGroupName")
                    'Cache module level variables
                    msOrigUserGroup = txtUserGroupID
                    msOrigDescription = txtDescription
                    miUserGroupUpdCtr = .Field("UpdateCounter")
                End If
            End With
            'Cleanup local object
            oRs.Close
            Set oRs = Nothing
        End If
    End If
        
ValidUserGroupBail:
    'Local object cleanup
    If Not oRs Is Nothing Then
        oRs.Close
        Set oRs = Nothing
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:
    'Local object cleanup
    If Not oRs Is Nothing Then
        oRs.Close
        Set oRs = Nothing
    End If
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bValidUserGroup", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Private Function bLoadAllLB() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Set default return value
    bLoadAllLB = False
    
    If bLoadModuleIDLB() And bLoadTaskPermLB() And bLoadSecEventPermLB() Then
        bLoadAllLB = True
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bLoadAllLB", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Private Function bIsUserGroupActive(oDB As Object, sGroupID As String) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lReturnCode As Long
    
    'Set default return value
    bIsUserGroupActive = False

    With oDB
        'Check for the existence of the group within any of the company appDB referenced by tsmCompany.PathToDBase
        .SetInParam sGroupID
        .SetOutParam lReturnCode
        .ExecuteSP ("spFindSotaGroup")
        lReturnCode = .GetOutParam(2)
        .ReleaseParams
        'Check to see if the group is currently in use
        If lReturnCode <> 0 Then bIsUserGroupActive = True
    End With
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsUserGroupActive", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function



Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If
    
    Dim sNewKey     As String
    Dim iCurrState  As Integer
    Dim lReturnCode As Long
    Dim vParseRet   As Variant
                    
    iCurrState = Me.iState
    
    Select Case sKey
        Case kTbFinish, kTbSave
            mlExtendSelTaskPerm = 0
            mlExtendSelSecEventPerm = 0
            Select Case iCurrState
                Case kStateAdd, kStateAddAOF, kStateEdit
                    'Attempt save only if we are in the correct mode, the data has chanaged, and is valid
                    If miSecurityLevel > sotaTB_DISPLAYONLY And bDataHasChanged(iCurrState, True) Then
                        If bValidEntry() Then
                            'Verify the name hasn't been added prior to us saving the record
                            If iCurrState = kStateAdd Or iCurrState = kStateAddAOF Then
                                If gbIsNameAlreadyUsed(moClass.moAppDB, txtUserGroupID) Then
                                    giSotaMsgBox Me, moClass.moSysSession, kmsgNameAlreadyExists
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                                    Exit Sub
                                End If
                            End If
                            'Attempt save
                            lReturnCode = lFinishRecord(iCurrState, True)
                            'Evaluate outcome of attempted save
                            If lReturnCode = kSaveCancel Then
                                txtDescription.SetFocus
                            Else
                                If sKey = kTbSave Then
                                    ManageAppState kStateEdit
                                Else
                                    ManageAppState iGetNextFinishState(iCurrState, CInt(lReturnCode))
                                End If
                                InitializeToState Me.iState
                            End If
                        End If
                    Else
                        'Re-initialize form state only when "Finish" button pressed
                        If sKey = kTbFinish Then
                            'No save required, set next form state and initialize
                            If iCurrState = kStateAddAOF Then
                                ManageAppState kStateNone
                            Else
                                ManageAppState kStateStart
                            End If
                        End If
                        InitializeToState Me.iState
                    End If
                Case kStateDisplayOnly
                    'Set next form state
                    ManageAppState kStateStartDisplayOnly
                    'Init form to new state
                    InitializeToState Me.iState
                Case Else
                    giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedState, Format(iCurrState)
            End Select
        Case kTbCancel
            mlExtendSelTaskPerm = 0
            mlExtendSelSecEventPerm = 0
            'Get next form state
            ManageAppState iGetNextCancelState(iCurrState)
            'Init form to new state
            InitializeToState Me.iState
        Case kTbDelete
            mlExtendSelTaskPerm = 0
            mlExtendSelSecEventPerm = 0
            'Attempt deletion
            lReturnCode = lDeleteRecord(txtUserGroupID)
            'Determine next form state dependent upon outcome of attempted delete
            ManageAppState iGetNextDeleteState(iCurrState, lReturnCode)
            'Perform next action dependent outcome of attempted delete
            If lReturnCode = kDeleteCancel Then
                txtDescription.SetFocus
            Else
                InitializeToState Me.iState
            End If
        Case kTbPrint
            bPrintRecord
        Case kTbHelp
            gDisplayFormLevelHelp Me
        Case kTbFirst, kTbPrevious, kTbLast, kTbNext
            If Not bSetupNavMain() Then Exit Sub
            If miSecurityLevel > sotaTB_DISPLAYONLY And bDataHasChanged(Me.iState, True) Then
                Select Case giSotaMsgBox(Me, moClass.moSysSession, kmsgSaveChanges)
                    Case kretYes
                        If bValidEntry() Then
                            lReturnCode = lFinishRecord(Me.iState, True)
                            ManageAppState iGetNextFinishState(Me.iState, lReturnCode)
                            InitializeToState Me.iState
                        Else
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                            Exit Sub
                        End If
                    Case kretNo
                        ManageAppState iGetNextCancelState(Me.iState)
                        InitializeToState Me.iState
                    Case kretCancel
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Sub
                End Select
            End If
            'Execute requested move
            Select Case sKey
                Case kTbFirst
                    lReturnCode = navMain.MoveFirst(miFilter, sNewKey)
                Case kTbPrevious
                    lReturnCode = navMain.MovePrevious(miFilter, sNewKey)
                Case kTbLast
                    lReturnCode = navMain.MoveLast(miFilter, sNewKey)
                Case kTbNext
                    lReturnCode = navMain.MoveNext(miFilter, sNewKey)
            End Select
            'Evaluate outcome of requested move
            Select Case lReturnCode
                Case MS_SUCCESS
                    vParseRet = gvParseLookupReturn(sNewKey)
                    If IsNull(vParseRet) Then Exit Sub
                    
                    If Trim(txtUserGroupID) <> Trim(vParseRet(1)) Then
                        txtUserGroupID = Trim(vParseRet(1))
                        bProcessUserGroupEntry
                    End If
                Case MS_BORS
                    HandleToolbarClick kTbFirst
                Case MS_EMPTY_RS
                    giSotaMsgBox Me, moClass.moSysSession, kmsgNavEmptyRecordSet
                Case MS_EORS
                    HandleToolbarClick kTbLast
                Case MS_ERROR
                    giSotaMsgBox Me, moClass.moSysSession, kmsgNavBrowseError
                Case MS_NO_CURRENT_KEY
                    giSotaMsgBox Me, moClass.moSysSession, kmsgNavNoCurrentKey
                Case MS_UNDEF_RS
                    giSotaMsgBox Me, moClass.moSysSession, kmsgNavUndefinedError
            End Select
        Case kTbFilter
            Select Case miFilter
                Case RSID_UNFILTERED
                    miFilter = RSID_FILTERED
                Case RSID_FILTERED
                    miFilter = RSID_UNFILTERED
            End Select
    End Select
    
HandleToolBarBail:
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub InitListBoxControls()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Const kSystemManager = "SM"
    
    'Force the ModuleID to refresh
    With lstModuleID
        .Tag = Trim(Str(.ListIndex))
        .ListIndex = kItemNotSelected
        .Tag = Trim(Str(.ListIndex))
        Select Case Me.iState
            Case kStateAdd, kStateAddAOF, kStateEdit, kStateDisplayOnly
                .ListIndex = giListIndexFromText(lstModuleID, kSystemManager)
            Case Else
                .ListIndex = kItemNotSelected
        End Select
    End With
    
    'Set listbox default selections
    lstDfltPermTaskPerm.ListIndex = 0
    lstDfltPermSecEventPerm.ListIndex = 0
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitListBoxControls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Property Let iState(iNewState As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    miState = iNewState
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Property
    
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iState_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get iState() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    iState = miState
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Property
    
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iState_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Private Sub ManageAppState(iNewState As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Me.iState = iNewState
    
    Select Case iNewState
        Case kStateStart, kStateNone, kStateInit, kStateStartDisplayOnly
            tbrMain.SetState (kDmStateNone)
        Case kStateEdit, kStateDisplayOnly
            tbrMain.SetState (kDmStateEdit)
        Case kStateAdd, kStateAddAOF
            tbrMain.SetState (kDmStateAdd)
        Case Else
            tbrMain.SetState (kDmStateNone)
    End Select
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ManageAppState", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function oGetSALogin(sPassword As String) As Object
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++

    On Error GoTo ExpectedErrorRoutine:
    
    Dim sConnect As String
    
    'Set default return value
    Set oGetSALogin = Nothing
    
    'Build connect string
    With moClass.moSysSession
        sConnect = "ODBC;"
        sConnect = sConnect & "DSN=" & .SysDSN & ";"
        sConnect = sConnect & "UID=sa;"
        sConnect = sConnect & "PWD=" & Trim(sPassword) & ";"
        sConnect = sConnect & "SERVER=" & .SysServerName & ";"
    End With
    
    'Turn off error handler to trap for err=383 (Failed Login)
    On Error Resume Next
    
    'Attempt to connect to the database
    Set oGetSALogin = goOpenDatabase(moClass.moDasSession, sConnect, False, False, moClass, App.ProductName, App.Title)
    
    'Evaluate error code
    Select Case Err
        Case 0
            'do nothing
        Case 383
            'Login failed - usually an invalid password entered
            Set oGetSALogin = Nothing
        Case Else
            GoTo ExpectedErrorRoutine:
    End Select
    
    'Restore local error handler
    On Error GoTo ExpectedErrorRoutine:
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
ExpectedErrorRoutine:
    If Not oGetSALogin Is Nothing Then
        oGetSALogin.Close
        Set oGetSALogin = Nothing
    End If
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oGetSALogin", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub RestorePreviousUserGroup()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    With txtUserGroupID
        txtUserGroupID = Trim(.Tag)
        .SetFocus
    End With
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "RestorePreviousUserGroup", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Public Function bProcessUserGroupEntry() As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    
    On Error GoTo ExpectedErrorRoutine:
    
    Dim iOldMP  As Integer
    
    'Default return value
    bProcessUserGroupEntry = True
    
    'Store current mousepointer and set to indicate processing
    iOldMP = MousePointer
    MousePointer = vbHourglass
    
    'Cleanup the current UserGroupID
    txtUserGroupID = Trim(txtUserGroupID)
    
    'Processing required only if the entry has changed
    If UCase(txtUserGroupID) <> UCase(Trim(txtUserGroupID.Tag)) Then
        'Validate UserGroupID
        If bValidUserGroup(moClass.moAppDB, txtUserGroupID) Then
            'UserGroupID exists
            Select Case mlRunMode
                Case kContextNormal
                    Select Case Me.iState
                        Case kStateStart, kStateEdit
                            ManageAppState kStateEdit
                            InitializeToState kStateEdit
                        Case kStateStartDisplayOnly, kStateDisplayOnly
                            ManageAppState kStateDisplayOnly
                            InitializeToState kStateDisplayOnly
                        Case Else
                            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedState, Format(Me.iState)
                            RestorePreviousUserGroup
                    End Select
                Case kContextAOF
                    Select Case giSotaMsgBox(Me, moClass.moSysSession, kmsgAOFUserGroupIDExists)
                        Case kretYes
                            Select Case Me.iState
                                Case kStateNone, kStateAddAOF, kStateStart, kStateEdit
                                    ManageAppState kStateEdit
                                    InitializeToState kStateEdit
                                Case kStateStartDisplayOnly, kStateDisplayOnly
                                    ManageAppState kStateDisplayOnly
                                    InitializeToState kStateDisplayOnly
                                Case Else
                                    giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedState, Format(Me.iState)
                                    'RestorePreviousUserGroup
                                    bProcessUserGroupEntry = False
                            End Select
                            mbSaved = True
                            Me.Hide
                        Case kretNo
                            'RestorePreviousUserGroup
                            bProcessUserGroupEntry = False
                    End Select
            End Select
        Else
            If miSecurityLevel > sotaTB_DISPLAYONLY Then
                'UserGroupID does not exist
                Select Case Me.iState
                    Case kStateStart, kStateAdd
                        ManageAppState kStateAdd
                        InitializeToState kStateAdd
                    Case kStateStartDisplayOnly, kStateDisplayOnly
                        RestorePreviousUserGroup
                    Case kStateNone, kStateAddAOF
                        ManageAppState kStateAddAOF
                        InitializeToState kStateAddAOF
                    Case Else
                        giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedState, Format(Me.iState)
                        RestorePreviousUserGroup
                End Select
            Else
                giSotaMsgBox Me, moClass.moSysSession, kmsgDMReadOnly
                RestorePreviousUserGroup
            End If
        End If
    End If
    
    'Save current user group ID
    txtUserGroupID.Tag = txtUserGroupID
    
    'Restore mousepointer
    If iOldMP <> vbHourglass Then
        MousePointer = iOldMP
    Else
        MousePointer = vbDefault
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
ExpectedErrorRoutine:
    MousePointer = vbDefault

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bProcessUserGroupEntry", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Private Sub InitFormVars()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Determine security level
    With moClass
        'Determine if user has priveledge level to print "Security Group Listing"
        miSecGrpListPerm = .moFramework.GetTaskPermission(ktskSecurityGroupListing)
        'Determine how form will treat the "Enter" key
        mbEnterAsTab = .moSysSession.EnterAsTab
    End With
    
    'Initialize sundry form level variables
    mbCancelShutDown = False
    mbLoadSuccess = False
    mlExtendSelSecEventPerm = 0
    mlExtendSelTaskPerm = 0
    mbShiftKeyHeldDown = False
        
    With Me
        'Set minimum form size
        miMinFormHeight = .Height
        miMinFormWidth = .Width
        'Save the original form size
        miOldFormHeight = .Height
        miOldFormWidth = .Width
    End With

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitFormVars", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub InitState()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Initialize form state
    ManageAppState kStateInit
    InitializeToState kStateInit
    
    'Set true form state as determined by the the current "RunMode"
    Select Case True
        Case kContextNormal = mlRunMode And miSecurityLevel = sotaTB_DISPLAYONLY
            ManageAppState kStateStartDisplayOnly
        Case kContextNormal = mlRunMode
            ManageAppState kStateStart
            InitializeToState kStateStart
        Case kContextAOF = mlRunMode
            ManageAppState kStateNone
            InitializeToState kStateNone
        Case kContextDA = mlRunMode
            ManageAppState kStateNone
            InitializeToState kStateNone
        Case Else
            'Do Nothing
    End Select
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitState", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub




Private Sub RefreshFromNewRecord(Optional vListIndex As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim iListIndex  As Integer
    Dim bTmp        As Boolean
    
    'Set the current forms "State"
    ManageAppState kStateEdit
    
    'Set the class "lUIActive" property
    moClass.lUIActive = kChildObjectActive
    
    sbrMain.Status = SOTA_SB_EDIT
    
    'Determine the ModuleID combo box "ListIndex" property
    If IsMissing(vListIndex) Then
        iListIndex = 0
    Else
        iListIndex = CInt(vListIndex)
    End If
    
    'Disable key and record creation controls -> edit mode
    txtUserGroupID.Enabled = False
    
    'Fill module level record set
     bTmp = bValidUserGroup(moClass.moAppDB, txtUserGroupID)
    
    'Activate the first tab -> Task Permissions
    tabSecurGroupMaint.Tab = 0
    
    'Set the default "ListIndex" properties for tab combo box controls
    lstDfltPermTaskPerm.ListIndex = 0
    lstDfltPermSecEventPerm.ListIndex = 0
    
    'Set the currently selected ModuleID
    With lstModuleID
        .ListIndex = iListIndex
        .SetFocus
    End With
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "RefreshFromNewRecord", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub SetAnimatedStatus(ctl As Object, iStatus As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    If TypeOf ctl Is SotaStatus Then
        Select Case iStatus
            Case kStatusStart
                ctl.SetStatusStart
            Case kStatusAdd
                ctl.SetStatusAdd
            Case kStatusEdit
                ctl.SetStatusEdit
            Case Else
                ctl.SetStatusNone
        End Select
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetAnimatedStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ToggleRowColor(grd As Object, iMode As Integer, lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Validate the current row number
    If lRow <= 0 Then lRow = -1
    
    With grd
        'Set the current row
        .Row = lRow
        'Set grid colors dependent upon the current "action" requested (i.e., select vs. unselect)
        Select Case iMode
            Case kUNSelectRow
                .Col = kModuleIDCol
                .BackColor = vbButtonFace
                .ForeColor = vbButtonText
                .Col = kDescriptionCol
                .BackColor = vbButtonFace
                .ForeColor = vbButtonText
                .Col = kPermissionCol
                .BackColor = vbWindowBackground
                .ForeColor = vbWindowText
            Case kSelectRow
                .Col = -1
                .BackColor = vbHighlight
                .ForeColor = vbHighlightText
        End Select
    End With
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ToggleRowColor", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ToggleTabControlsTabStop(iPreviousTab As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim bTaskTabStop    As Boolean
    Dim bSecurTabStop   As Boolean
    
    'Determine each controls "TabStop" property for each tab
    If iPreviousTab = kTaskPermTab Then
        bTaskTabStop = False
        bSecurTabStop = True
    Else
        bTaskTabStop = True
        bSecurTabStop = False
    End If
    
    'Apply "TabStop" values to the Task tab controls
    cmdApply(kTaskPermTab).TabStop = bTaskTabStop
    lstDfltPermTaskPerm.TabStop = bTaskTabStop
    
    'Apply "TabStop" value to the Security tab controls
    cmdApply(kSecEventPermTab).TabStop = bSecurTabStop
    lstDfltPermSecEventPerm.TabStop = bSecurTabStop
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ToggleTabControlsTabStop", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    WhatHelpPrefix = "SMZ"
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Property

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    FormHelpPrefix = "SMZ"
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Property

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property


Private Function sGetModuleName(cboctl As Control, sModuleID As String) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim iModuleNo   As Integer

    'Set default return value
    sGetModuleName = Empty

    iModuleNo = giModuleNoFromID(sModuleID)

    'If the item exists in the combo box then assign current module name from "name array"
    If iModuleNo >= 0 Then
        If iModuleNo = 0 Then
            sGetModuleName = gsBuildString(kAllModules, moClass.moAppDB, moClass.moSysSession)
        Else
            sGetModuleName = gsModuleNameFromNo(iModuleNo)
        End If
    Else
        'error handling
    End If

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetModuleName", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub bPrintRecord()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    
    On Error GoTo ExpectedErrorRoutine:
    
    Dim iOldMP      As Integer
    Dim lReturnCode As Long

    'Save current mousepointer
    iOldMP = MousePointer
    
    'Determine action from current form state
    Select Case Me.iState
        Case kStateAdd, kStateEdit, kStateAddAOF
            'Determine if data has changed
            If bDataHasChanged(Me.iState, True) Then
                MousePointer = vbDefault
                'Prompt user to save changes
                Select Case giSotaMsgBox(Me, moClass.moSysSession, kmsgSaveChanges)
                    Case kretYes
                        'Validate the entry
                        If bValidEntry() Then
                            'Set mousepointer to indicate processing
                            MousePointer = vbHourglass
                            'Attempt to save data
                            lReturnCode = lFinishRecord(Me.iState, True)
                            'Determine next form state dependent upon save return code
                            ManageAppState iGetNextFinishState(Me.iState, lReturnCode)
                            'Initialize form to new state
                            InitializeToState Me.iState
                        End If
                    Case kretNo
                        'Detemine next form state
                        ManageAppState iGetNextCancelState(Me.iState)
                        'Initialize form to new state
                        InitializeToState Me.iState
                    Case kretCancel
                        MousePointer = vbDefault
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Sub
                End Select
            End If
        Case Else
            'Do Nothing
    End Select
    
    'Print the report if the user has sufficient permissions
    If miSecGrpListPerm > sotaTB_DISPLAYONLY Then
        gPrintTask moClass.moFramework, ktskSecurityGroupListing
    Else
        giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidSecurityLevel
    End If
    
    'Restore the mousepointer
    If iOldMP <> vbHourglass Then
        MousePointer = iOldMP
    Else
        MousePointer = vbDefault
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
ExpectedErrorRoutine:
    MousePointer = vbDefault
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bPrintRecord", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub DisableSecEventPermControls()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Disable the combo box
    With lstDfltPermSecEventPerm
        .Enabled = False
        .BackColor = vbButtonFace
    End With
    
    'Disable the command button
    With cmdApply(kSecEventPermTab)
        .Enabled = False
    End With
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DisableSecEventPermControls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub DisableTaskPermControls()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Disable the combo box
    With lstDfltPermTaskPerm
        .Enabled = False
        .BackColor = vbButtonFace
    End With
    
    'Disable command button
    With cmdApply(kTaskPermTab)
        .Enabled = False
    End With
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DisableTaskPermControls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub EnableSecEventPermControls()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Enable the combo box
    With lstDfltPermSecEventPerm
        .Enabled = True
        .BackColor = vbWindowBackground
        .TabStop = True
        .Refresh
    End With
    
    'Enable the command button
    With cmdApply(kSecEventPermTab)
        .Enabled = True
        .TabStop = True
        .Refresh
    End With
    
    'Turn OFF Task Permission tab control tabstops
    lstDfltPermTaskPerm.TabStop = False
    cmdApply(kTaskPermTab).TabStop = False
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "EnableSecEventPermControls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub EnableTaskPermControls()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Enable the "Task Permissions" combo box
    With lstDfltPermTaskPerm
        .Enabled = True
        .BackColor = vbWindowBackground
        .TabStop = True
        .Refresh
    End With
    
    'Enable the "Apply" command button
    With cmdApply(kTaskPermTab)
        .Enabled = True
        .TabStop = True
        .Refresh
    End With
    
    'Turn OFF the Sec Event Permission tab contol tabstops
    lstDfltPermSecEventPerm.TabStop = False
    cmdApply(kcmdSecEventPermApply).TabStop = False
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "EnableTaskPermControls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function iGetNextDeleteState(iCurrState As Integer, lReturnCode As Long) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'Set default return value
    iGetNextDeleteState = iCurrState
    
    Select Case iCurrState
        Case kStateEdit
            Select Case lReturnCode
                Case kDeleteSuccess, kDeleteFailure
                    iGetNextDeleteState = kStateStart
                Case kDeleteCancel
                    iGetNextDeleteState = kStateEdit
                Case Else
                    iGetNextDeleteState = kStateStart
            End Select
        Case Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedState, Format(iCurrState)
    End Select
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iGetNextDeleteState", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub FormatGrid(Grid As Control)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    With Grid
        'Turn off redraw for setup
        .redraw = False
        'Set area affected to entire grid
        .Row = -1
        .Col = -1
        .Font.Bold = False
        'No cell overflow
        .AllowCellOverflow = False
        'No drag-and-drop behavior
        .AllowDragDrop = False
        'No multiblock selection
        .AllowMultiBlocks = False
        .SelectBlockOptions = 0
        'No user formulas
        .AllowUserFormulas = False
        'No automatic recalc
        .AutoCalc = False
        'No resizing
        .AutoSize = False
        'Draw buttons/checkbox for current row
        .ButtonDrawMode = SS_BDM_CURRENT_ROW
        'Set cursor type and style
        .CursorType = SS_CURSOR_TYPE_DEFAULT
        .CursorStyle = SS_CURSOR_STYLE_ARROW
        'Turn on row headers and define type
        .DisplayRowHeaders = True
        .RowHeaderDisplay = SS_HEADER_NUMBERS
        'Turn on column headers and define type
        .DisplayColHeaders = True
        .ColHeaderDisplay = SS_HEADER_BLANK
        'Define "enter" key behavior
        .EditEnterAction = SS_CELL_EDITMODE_EXIT_NEXT
        'Define how cell contents are treated when editing values (true=replace, false=insert)
        .EditModeReplace = True
        'Define "tab" key as a navigational key
        .ProcessTab = True
        'Reset height of all rows
        .RowHeight(-1) = .RowHeight(0)
        
        'Define scrollbar behavior
        .ScrollBars = SS_SCROLLBAR_BOTH
        .ScrollBarMaxAlign = True
        .ScrollBarExtMode = True
        .ScrollBarShowMax = True
        
        'Allow columns to be resizeable
        .UserResize = SS_USER_RESIZE_COL
        
        'Define grid colors
        gGridSetColors Grid
        'Set the grid line attributes
        gGridShowHorizLines Grid, True
        gGridShowVertLines Grid, True
        gGridShowSolidLines Grid, True
        
        'Turn off virtual mode
        .VirtualMode = False
        'Make the grid visible
        .Visible = True
        'Sort data by row
        .SortBy = SS_SORT_BY_ROW
        'Turn on redraw
        .redraw = True
    End With

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormatGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function lDeleteRecord(sUserGroupID As String) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    
    On Error GoTo ExpectedErrorRoutine:
    
    Dim oSysDB              As Object
    Dim oSysSession         As Object
    Dim oProgressBar        As New clsPBar
    Dim sSQL                As String
    Dim sSecurEventID       As String
    Dim sTaskID             As String
    Dim sEventDesc          As String
    Dim iOldMousePointer    As Integer
    Dim bStatusLoaded       As Boolean
    Dim bDone               As Boolean
    Dim lRowIndex           As Long
    Dim lNbrSecEventRows    As Long
    Dim lNbrTaskRows        As Long
    Dim vArgs               As Variant
    
    'Set default return value
    lDeleteRecord = kDeleteFailure
    
    'Initialize status message display flag
    bStatusLoaded = False
    
    'Cache values for local use
    With moClass
        Set oSysDB = .moAppDB
        Set oSysSession = .moSysSession
    End With
    sUserGroupID = Trim(sUserGroupID)
    vArgs = CVar(sUserGroupID)
    
    ' User Group 'public' can not be deleted since this group
    ' exists in the sysusers table and was created by SqlServer
    ' Corrects ics #12654
    If sUserGroupID = "public" Then
      giSotaMsgBox Me, oSysSession, kmsgCannotBeDeleted, kSQuote & sUserGroupID & kSQuote
      GoTo DeleteRecordBail:
    End If
    
    'Prompt the user to continue with delete
    Select Case giSotaMsgBox(Me, oSysSession, kmsgDeleteUserGroup, kSQuote & sUserGroupID & kSQuote)
        Case kretYes
            'Allow delete only if the group is not referenced
            If bIsUserGroupActive(oSysDB, sUserGroupID) Then
                giSotaMsgBox Me, oSysSession, kmsgDBSecurityRemoveGroup
                GoTo DeleteRecordBail:
            End If
            
            'Save the current mousepointer and set to indicate processing
            iOldMousePointer = MousePointer
            MousePointer = vbHourglass
        
            'Setup the progress bar object
            With oProgressBar
                .CreatePBar
                If .bInit(Me, gsBuildString(kDeleteDBSecurGrpRec, oSysDB, oSysSession, vArgs), 1000) Then
                    Me.Enabled = False
                    .ShowPBar
                Else
                    GoTo DeleteRecordBail:
                End If
            End With
            
            'Handle the tsmSystemActivity insert
            sEventDesc = "Deleted UserGroup " & sUserGroupID
            gLogSystemActivity moClass, oSysDB, kEventTypeOther, sEventDesc
        
            'Mark the beginning of the transaction
            oSysDB.BeginTrans
            
            '/////////////////////////////////
            '//  SECURITY EVENT DATA DELETION
            'Determine the number of rows in the Security Event Permission grid
            lNbrSecEventRows = glGridGetMaxRows(grdSecEventPerm)
            'If no rows found make sure the Sec Event Perm grid has been populated
            If lNbrSecEventRows = 0 Then
                'Click on the Sec Event Perm tab -> Syntax indicates that the PreviousTab=Task Perm tab
                tabSecurGroupMaint_Click (0)
                'Now click on the Task Perm tab -> Syntax indicates that the PreviousTab=Sec Event Perm tab
                tabSecurGroupMaint_Click (1)
                'Try to get the number of rows in the Security Event Permission grid again
                lNbrSecEventRows = glGridGetMaxRows(grdSecEventPerm)
            End If
            'Attempt deletion only if rows exist
            If lNbrSecEventRows > 0 Then
                'Update progress bar
                oProgressBar.UpdatePBar gsBuildString(kDeleteSecEventInfo, oSysDB, oSysSession, vArgs), kDescUpdateReq
                'Loop through each row in the Security Event Permission grid
                For lRowIndex = 1 To lNbrSecEventRows
                    'Only delete security event rows flagged as "Update" type
                    If UCase(Trim(gsGridReadCell(grdSecEventPerm, lRowIndex, kColSecEventPermInsOrUpdRow))) = ksUpdateRow Then
                        'Determine the Security Event ID
                        sSecurEventID = gsGridReadCell(grdSecEventPerm, lRowIndex, kColSecEventPermSecEventID)
                        'Build SQL DELETE statement for the current security event record
                        sSQL = oSysDB.BuildSQLString("#smDelFromSecurEventPerm;" & kSQuote & sUserGroupID & _
                                                kSQuote & ";" & kSQuote & sSecurEventID & kSQuote & ";")
                        'Execute security event deletion for the current grid row
                        If Trim(sSQL) <> "" Then
                            oSysDB.ExecuteSQL sSQL
                        Else
                            oSysDB.Rollback
                            GoTo DeleteRecordBail:
                        End If
                    End If
                Next lRowIndex
            End If
            
            '//////////////////////////////////
            '//  TASK PERMISSION DATA DELETION
            'Determine the number of rows in the Task Permission grid
            lNbrTaskRows = glGridGetMaxRows(grdTaskPerm)
            'If no rows found make sure the task permissions grid has been populated
            If lNbrTaskRows = 0 Then
                'Click on the Task Perm tab -> Syntax indicates that the PreviousTab=Sec Event Perm tab
                tabSecurGroupMaint_Click (1)
                'Click on the Task Perm tab -> Syntax indicates that the PreviousTab=Task Perm tab
                tabSecurGroupMaint_Click (0)
                'Try to get the number of rows in the Task Permission grid again
                lNbrTaskRows = glGridGetMaxRows(grdTaskPerm)
            End If
            'Attempt deletion only if rows exist
            If lNbrTaskRows > 0 Then
                'Update progress bar
                oProgressBar.UpdatePBar gsBuildString(kDeleteTaskPermsInfo, oSysDB, oSysSession, vArgs), kDescUpdateReq
                'Loop through each row in the Task Permission grid
                For lRowIndex = 1 To lNbrTaskRows
                    'Only delete task permission rows flagged as "Update" type
                    If UCase(Trim(gsGridReadCell(grdTaskPerm, lRowIndex, kColTaskPermInsOrUpdRow))) = ksUpdateRow Then
                        'Determine the Task ID field
                        sTaskID = gsGridReadCell(grdTaskPerm, lRowIndex, kColTaskPermTaskID)
                        'Build SQL DELETE statement for the current task permission record
                        sSQL = oSysDB.BuildSQLString("#smDelFromTaskPerm;" & kSQuote & sUserGroupID & kSQuote & _
                                                ";" & sTaskID & ";")
                        'Execute task permission deletion for the current grid row
                        If Trim(sSQL) <> "" Then
                            oSysDB.ExecuteSQL sSQL
                        Else
                            oSysDB.Rollback
                            GoTo DeleteRecordBail:
                        End If
                    End If
                Next lRowIndex
            End If
            
            '//////////////////////////////////////////
            '//  HEADER DATA DELETION
            'Update progress bar
            oProgressBar.UpdatePBar gsBuildString(kDeleteSotaSecurGrpRec, oSysDB, oSysSession, vArgs), kDescUpdateReq
            'Build SQL DELETE statement for tsmUserGroup
            sSQL = oSysDB.BuildSQLString("#smDelFromUserGroup;" & kSQuote & sUserGroupID & kSQuote & ";")
            If Trim(sSQL) <> "" Then
                oSysDB.ExecuteSQL sSQL
            Else
                oSysDB.Rollback
                GoTo DeleteRecordBail:
            End If
            
            'Successful delete, commit the transaction
            oSysDB.CommitTrans
            'Set SUCCESS return value
            lDeleteRecord = kDeleteSuccess
            
            'Re-initialize grid edit flags
            grdTaskPerm.ChangeMade = False
            grdSecEventPerm.ChangeMade = False
        
        Case kretNo
            'User aborted the process -> set return value and bail out
            lDeleteRecord = kDeleteCancel
            GoTo DeleteRecordBail:
            
    End Select
    
    
DeleteRecordBail:
    'Progress bar object cleanup
    With Me
        If Not .Enabled Then .Enabled = True
    End With
    If Not oProgressBar Is Nothing Then
        oProgressBar.DestroyPBar
        Set oProgressBar = Nothing
    End If
    
    'Cleanup cached objects
    Set oSysDB = Nothing
    Set oSysSession = Nothing
    
    'Restore mousepointer
    If iOldMousePointer <> vbHourglass Then
        MousePointer = iOldMousePointer
    Else
        MousePointer = vbDefault
    End If

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:
    MousePointer = vbDefault
    oSysDB.Rollback
    
    'Progress bar object cleanup
    With Me
        If Not .Enabled Then .Enabled = True
    End With
    If Not oProgressBar Is Nothing Then
        oProgressBar.DestroyPBar
        Set oProgressBar = Nothing
    End If
    
    'Cleanup cached objects
    Set oSysDB = Nothing
    Set oSysSession = Nothing
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lDeleteRecord", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function iGetNextCancelState(iCurrState As Integer) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Default return value
    iGetNextCancelState = iCurrState

    Select Case iCurrState
        Case kStateDisplayOnly, kStateStartDisplayOnly
            iGetNextCancelState = kStateStartDisplayOnly
        Case kStateStart
            iGetNextCancelState = kStateStart
        Case kStateAdd
            iGetNextCancelState = kStateStart
        Case kStateAddAOF, kStateNone
            iGetNextCancelState = kStateNone
        Case kStateEdit
            iGetNextCancelState = kStateStart
        Case Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedState, Format(iCurrState)
    End Select

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iGetNextCancelState", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function iGetNextFinishState(iCurrState As Integer, lReturnCode As Long) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
   
    'Set default return value
    iGetNextFinishState = iCurrState

    Select Case iCurrState
        Case kStateAdd
            Select Case lReturnCode
                Case kSaveSuccess
                    iGetNextFinishState = kStateStart
                Case kSaveFailure
                    iGetNextFinishState = kStateAdd
                Case kSaveCancel
                    iGetNextFinishState = kStateEdit
            End Select
            
        Case kStateAddAOF
            iGetNextFinishState = kStateNone
        
        Case kStateEdit
            Select Case lReturnCode
                Case kSaveSuccess
                    iGetNextFinishState = kStateStart
                Case kSaveFailure, kSaveCancel
                    iGetNextFinishState = kStateEdit
            End Select
        
        Case Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedState, Format(iCurrState)
    End Select

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iGetNextFinishState", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function lDisplaySecEventPerms(iDisplayType As Integer, sUserGroupID As String, sModuleID As String) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    
    On Error GoTo ExpectedErrorRoutine:
    
    Dim rsSecEventPerm      As Object
    Dim sSQL                As String
    Dim sSecEventID         As String
    Dim sSecEventDesc       As String
    Dim sModule             As String
    Dim sAuthorized         As String
    Dim sCurrentAuth        As String
    Dim sUpdateCtr          As String
    Dim iAuthorized         As Integer
    Dim iUpdateCtr          As Integer
    Dim iOldMousePointer    As Integer
    Dim bUpdateFlag         As Boolean
    Dim lNbrSecEventPerms   As Long
    Dim vAuthorized         As Variant
    Dim vCounter            As Variant
    Dim vCurrentAuth        As Variant
    
    'Set default return value
    lDisplaySecEventPerms = kiDisplayPermsFailure
    
    'Set mousepointer to indicate processing
    iOldMousePointer = MousePointer
    MousePointer = vbHourglass
    
    'Initialize grid variables
    lNbrSecEventPerms = 0
    mlSecEventPermRowsSelected = 0
    mbAllSecEventPermRowsSelected = False

    'Clear the grid before beginning
    gGridClearAll grdSecEventPerm
    
    'Make sure the background color is white
    ToggleRowColor grdSecEventPerm, kUNSelectRow, -1
    
    'Disable controls
    With lstDfltPermSecEventPerm
        .Enabled = False
        .BackColor = vbButtonFace
        .Refresh
    End With
    
    With cmdApply(kcmdSecEventPermApply)
        .Enabled = False
        .Refresh
    End With
    
    'Define SQL statement
    sSQL = "SELECT tsmSecurEvent.SecurEventID, d.ModuleID, tsmLocalString.LocalText Description," & _
            " tsmSecurEventPerm.Authorized, p.UpdateCounter, p.Authorized CurrentlyAuthorized " & _
            " FROM tsmSecurEvent WITH (NOLOCK) " & _
            " LEFT OUTER JOIN tsmSecurEventPerm WITH (NOLOCK) " & _
            " ON tsmSecurEvent.SecurEventID = tsmSecurEventPerm.SecurEventID " & _
            " AND (tsmSecurEventPerm.UserGroupID IS NULL OR tsmSecurEventPerm.UserGroupID = " & gsQuoted(sUserGroupID) & ") " & _
            " JOIN tsmLocalString WITH (NOLOCK) " & _
            " ON tsmSecurEvent.DescStrNo = tsmLocalString.StringNo " & _
            " AND tsmLocalString.LanguageID = " & Me.oClass.moSysSession.Language & _
            " LEFT OUTER JOIN tsmSecurEventPerm p WITH (NOLOCK) " & _
            " ON tsmSecurEvent.SecurEventID = p.SecurEventID " & _
            " AND p.UserGroupID = " & gsQuoted(txtUserGroupID) & _
            " JOIN tsmModule m WITH(NOLOCK) " & _
            " ON m.ModuleNo = tsmSecurEvent.ModuleNo " & _
            " JOIN tsmModuleDef d WITH (NOLOCK) " & _
            " ON m.ModuleNo = d.ModuleNo"
                
    If Not (iDisplayType = kAllPermissions) Then
        sSQL = sSQL & " AND tsmSecurEvent.ModuleNo = " & giModuleNoFromID(sModuleID)
    End If
        
    sSQL = sSQL & " ORDER BY 2 ASC"
    
    'Create the Security Event recordset
    Set rsSecEventPerm = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    'Continue only if the recordset is valid
    If rsSecEventPerm Is Nothing Then GoTo DisplaySecEventPermsBail:
    
    'Disable redraw on the grid until all of the data is loaded
    grdSecEventPerm.redraw = False
    
    With rsSecEventPerm
        Do While Not .IsEOF
            'tsmSecurEvent.SecurEventID
            sSecEventID = .Field("SecurEventID")
            'tsmModuleDef.ModuleID
            sModule = .Field("ModuleID")
            'tsmSecurEvent.Description
            sSecEventDesc = .Field("Description")
            'tsmSecurEventPerm.Authorized
            vAuthorized = .Field("Authorized")
            If IsNull(vAuthorized) Then
                sAuthorized = CStr(kiNo)
            Else
                sAuthorized = CStr(vAuthorized)
            End If
            'determine whether to insert or update
            vCurrentAuth = .Field("CurrentlyAuthorized")
            If IsNull(vCurrentAuth) Then
                bUpdateFlag = False
                sCurrentAuth = CStr(kiNo)
            Else
                bUpdateFlag = True
                sCurrentAuth = CStr(vCurrentAuth)
            End If
            'tsmSecurEventPerm.UpdateCounter
            vCounter = .Field("UpdateCounter")
            If IsNull(vCounter) Then
                sUpdateCtr = "0"
            Else
                sUpdateCtr = CStr(vCounter)
            End If
            
            'Increment row counter and grid "MaxRow" property
            lNbrSecEventPerms = lNbrSecEventPerms + 1
            gGridSetMaxRows grdSecEventPerm, lNbrSecEventPerms
            
            '"Sec Event ID"
            gGridUpdateCell grdSecEventPerm, lNbrSecEventPerms, kColSecEventPermSecEventID, sSecEventID
            '"Sec Event Module"
            gGridUpdateCell grdSecEventPerm, lNbrSecEventPerms, kColSecEventPermModuleID, sModule
            '"Sec Event Description"
            gGridUpdateCell grdSecEventPerm, lNbrSecEventPerms, kColSecEventPermDescription, sSecEventDesc
            '"Sec Event Permission Value"
            gGridUpdateCell grdSecEventPerm, lNbrSecEventPerms, kColSecEventPermAuthorizedValue, sAuthorized
            '"Sec Event Permission Insert or Update"
            If bUpdateFlag Then
                gGridUpdateCell grdSecEventPerm, lNbrSecEventPerms, kColSecEventPermInsOrUpdRow, ksUpdateRow
            Else
                gGridUpdateCell grdSecEventPerm, lNbrSecEventPerms, kColSecEventPermInsOrUpdRow, ksInsertRow
            End If
            '"Sec Event Permission Text"
            gGridUpdateCell grdSecEventPerm, lNbrSecEventPerms, kColSecEventPermAuthorizedText, giListIndexFromItemData(lstDfltPermSecEventPerm, CInt(sAuthorized))
            '"Sec Event Permission Old Auth Value"
            gGridUpdateCell grdSecEventPerm, lNbrSecEventPerms, kColSecEventPermOldAuthValue, sCurrentAuth
            '"Sec Event Permission Update Counter"
            gGridUpdateCell grdSecEventPerm, lNbrSecEventPerms, kColSecEventPermUpdateCounter, sUpdateCtr
            
            'Re-init work variables
            sSecEventID = ""
            sSecEventDesc = ""
            sAuthorized = "0"
            vAuthorized = Empty
            sUpdateCtr = "0"
            vCounter = Empty
            
            'Move to the next row in the record set
            .MoveNext
        Loop
    End With
    
    'All data loaded, re-enable redraw on the grid
    grdSecEventPerm.redraw = True
    
    'Set the sort flag to 'Ascending'
    msSecEventSortType = ksSortTypeAscending

    'Evaluate the number of rows loaded to determine if the grids active row needs to be set
    If lNbrSecEventPerms > 0 Then
        gGridSetActiveCell grdSecEventPerm, 1, kColSecEventPermAuthorizedText
    End If
    
    'Initialize the Security Event grid's "ChangeMade" property
    grdSecEventPerm.ChangeMade = False
    
    'Restore the mousepointer
    If iOldMousePointer <> vbHourglass Then
        MousePointer = iOldMousePointer
    Else
        MousePointer = vbDefault
    End If
    
    'Assign the routines return value
    lDisplaySecEventPerms = lNbrSecEventPerms
    
DisplaySecEventPermsBail:
    'Local object cleanup
    If Not rsSecEventPerm Is Nothing Then
        rsSecEventPerm.Close
        Set rsSecEventPerm = Nothing
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
ExpectedErrorRoutine:
    MousePointer = vbDefault
    'Local object cleanup
    If Not rsSecEventPerm Is Nothing Then
        rsSecEventPerm.Close
        Set rsSecEventPerm = Nothing
    End If

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lDisplaySecEventPerms", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function lDisplayTaskPerms(iDisplayType As Integer, sUserGroupID As String, sModuleID As String) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    
    On Error GoTo ExpectedErrorRoutine:
    
    Dim rsTaskPerm          As Object
    Dim sSQL                As String
    Dim sTaskID             As String
    Dim sTaskDesc           As String
    Dim sTaskRights         As String
    Dim sTaskCurrentRights  As String
    Dim sUpdateCtr          As String
    Dim sModule             As String
    Dim iOldMP              As Integer
    Dim bUpdateFlag         As Boolean
    Dim lNbrTaskPerms       As Long
    Dim vRights             As Variant
    Dim vCurrentRights      As Variant
    Dim vID                 As Variant
    Dim vCounter            As Variant
    
    'Set default return value
    lDisplayTaskPerms = kiDisplayPermsFailure
    
    'Set mousepointer to indicate processing
    iOldMP = MousePointer
    MousePointer = vbHourglass
        
    'Initialize grid variables
    lNbrTaskPerms = 0
    mlTaskPermRowsSelected = 0
    mbAllTaskPermRowsSelected = False
    
    'Clear the grid
    gGridClearAll grdTaskPerm
    
    'Init row color
    ToggleRowColor grdTaskPerm, kUNSelectRow, -1
    
    'Disable controls
    With lstDfltPermTaskPerm
        .Enabled = False
        .BackColor = vbButtonFace
        .Refresh
    End With
    With cmdApply(kcmdTaskPermApply)
        .Enabled = False
        .Refresh
    End With
    
    'Define SQL statement
    sSQL = "SELECT tsmTask.TaskID, tsmTask.ModuleNo, tsmModuleDef.ModuleID, tsmTaskStrDef.TaskLongName 'TaskDesc', " & _
            " tsmTaskPerm.Rights, p.UpdateCounter, p.Rights 'CurrentRights' " & _
            " FROM tsmTaskPerm WITH (NOLOCK) " & _
            " RIGHT OUTER JOIN tsmTask WITH (NOLOCK) " & _
            " ON tsmTaskPerm.TaskID = tsmTask.TaskID " & _
            " AND (tsmTaskPerm.UserGroupID Is Null OR tsmTaskPerm.UserGroupID = " & gsQuoted(sUserGroupID) & ") " & _
            " JOIN tsmTaskStrDef WITH (NOLOCK) " & _
            " ON tsmTask.TaskID = tsmTaskStrDef.TaskID " & _
            " AND tsmTaskStrDef.TaskLangID = " & Me.oClass.moSysSession.Language & _
            " AND DATALENGTH(LTRIM(RTRIM(tsmTaskStrDef.TaskLongName))) > 0 " & _
            " LEFT OUTER JOIN tsmTaskPerm p WITH (NOLOCK) " & _
            " ON tsmTask.TaskID = p.TaskID " & _
            " AND p.UserGroupID = " & gsQuoted(txtUserGroupID.Text) & _
            " JOIN tsmModuleDef WITH (NOLOCK) " & _
            " ON tsmModuleDef.ModuleNo = tsmTask.ModuleNo " & _
            " ORDER BY 3 ASC "

            
    'Create Task Permission recordset
    Set rsTaskPerm = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    'Continue only if valid recordset
    If rsTaskPerm Is Nothing Then GoTo DisplayTaskPermsBail:
    
    'Turn off redraw until grid is fully populated
    grdTaskPerm.redraw = False
    
    With rsTaskPerm
        Do While Not .IsEOF
            'tsmTask.TaskID
            vID = .Field("TaskID")
            If IsNull(vID) Then
                sTaskID = "0"
            Else
                sTaskID = CStr(vID)
            End If
                        
            'Value is determined "valid for use" dependent upon requested display type (ALL or Module specific)
            If iDisplayType = kAllPermissions Or (giModuleNoFromID(sModuleID) = .Field("ModuleNo")) Then
            
                'tsmTaskStrDef.TaskLongName
                sTaskDesc = .Field("TaskDesc")
                
                'tsmModuleDef.ModuleID
                sModule = .Field("ModuleID")
                
                'tsmTaskPerm.Rights
                vRights = .Field("Rights")
                
                'display rights in grid
                If IsNull(vRights) Then
                    sTaskRights = CStr(sotaTB_EXCLUDED)
                Else
                    sTaskRights = CStr(vRights)
                End If
                
                'Current Rights determines if this will become an insert or update
                vCurrentRights = .Field("CurrentRights")
                If IsNull(vCurrentRights) Then
                    bUpdateFlag = False
                    sTaskCurrentRights = CStr(sotaTB_EXCLUDED)
                Else
                    bUpdateFlag = True
                    sTaskCurrentRights = CStr(vCurrentRights)
                End If
                
                'tsmTaskPerm.UpdateCounter
                vCounter = .Field("UpdateCounter")
                If IsNull(vCounter) Then
                    sUpdateCtr = "0"
                Else
                    sUpdateCtr = CStr(vCounter)
                End If
                
                'Increment row counter and set grid "MaxRows"
                lNbrTaskPerms = lNbrTaskPerms + 1
                gGridSetMaxRows grdTaskPerm, lNbrTaskPerms
                
                'Task ID
                gGridUpdateCell grdTaskPerm, lNbrTaskPerms, kColTaskPermTaskID, sTaskID
                
                'Module ID
                gGridUpdateCell grdTaskPerm, lNbrTaskPerms, kColTaskModuleID, sModule
                
                'Task Description
                gGridUpdateCell grdTaskPerm, lNbrTaskPerms, kColTaskPermTaskDesc, sTaskDesc
                'Task Permission Value
                gGridUpdateCell grdTaskPerm, lNbrTaskPerms, kColTaskPermPermissionValue, sTaskRights
                'Task Permission Insert or Update
                If bUpdateFlag Then
                    gGridUpdateCell grdTaskPerm, lNbrTaskPerms, kColTaskPermInsOrUpdRow, ksUpdateRow
                Else
                    gGridUpdateCell grdTaskPerm, lNbrTaskPerms, kColTaskPermInsOrUpdRow, ksInsertRow
                End If
                'Task Permission Text
                gGridUpdateCell grdTaskPerm, lNbrTaskPerms, kColTaskPermPermissionText, giListIndexFromItemData(lstDfltPermTaskPerm, CInt(sTaskRights))
                'Task Permission Old Permission
                gGridUpdateCell grdTaskPerm, lNbrTaskPerms, kColTaskPermOldPermValue, sTaskCurrentRights
                'Task Permission Update Counter
                gGridUpdateCell grdTaskPerm, lNbrTaskPerms, kColTaskPermUpdateCounter, sUpdateCtr
                
            End If
           
            'Re-init work variables
            sTaskID = "0"
            vID = Empty
            sTaskDesc = ""
            sTaskRights = "0"
            sTaskCurrentRights = "0"
            vRights = Empty
            vCurrentRights = Empty
            bUpdateFlag = False
            sUpdateCtr = "0"
            vCounter = Empty
            
            'Move to the next row
            .MoveNext
                       
        Loop
        
    End With
    
    'All data populated, re-enable redraw on the grid
    grdTaskPerm.redraw = True
    
    'Set the sort flag to 'Ascending'
    msTaskSortType = ksSortTypeAscending
    
    'Check number of task permissions found to determine is we need to set the active grid row
    If lNbrTaskPerms > 0 Then
        gGridSetActiveCell grdTaskPerm, 1, kColTaskPermPermissionText
    End If
    
    'Initialize the Task Permmission grid's "ChangeMade" property
    grdTaskPerm.ChangeMade = False
    
    'Restore the mouse pointer
    If iOldMP <> vbHourglass Then
        MousePointer = iOldMP
    Else
        MousePointer = vbDefault
    End If
    
    'Assign return value
    lDisplayTaskPerms = lNbrTaskPerms
    
DisplayTaskPermsBail:
    'Local object cleanup
    If Not rsTaskPerm Is Nothing Then
        rsTaskPerm.Close
        Set rsTaskPerm = Nothing
    End If

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
ExpectedErrorRoutine:
    MousePointer = vbDefault
    'Local object cleanup
    If Not rsTaskPerm Is Nothing Then
        rsTaskPerm.Close
        Set rsTaskPerm = Nothing
    End If
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lDisplayTaskPerms", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bLoadSecEventPermLB() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Default return value
    bLoadSecEventPermLB = False

    'Clear out the Sec Event Perm combobox
    lstDfltPermSecEventPerm.Clear

    'Add the FIXED Security Event Permission Types
    With moClass
        msNo = gsBuildString(kNo, .moAppDB, .moSysSession)
        gComboAddItem lstDfltPermSecEventPerm, msNo, kiNo
        
        msYes = gsBuildString(kYes, .moAppDB, .moSysSession)
        gComboAddItem lstDfltPermSecEventPerm, msYes, kiYes
    End With
    
    'Successful load
    bLoadSecEventPermLB = True
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bLoadSecEventPermLB", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function lSaveRecord(iCurrState As Integer, bSaveHeaderData As Boolean) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++

    On Error GoTo ExpectedErrorRoutine:

    Dim oSysDB                  As Object
    Dim oSysSession             As Object
    Dim oProgressBar            As New clsPBar
    Dim sInsOrUpdRow            As String
    Dim sUserGroupID            As String
    Dim sUserGroupDesc          As String
    Dim sSecurEventID           As String
    Dim bTaskGridChange         As Boolean
    Dim bSecEventGridChange     As Boolean
    Dim iOldPermission          As Integer
    Dim iNewPermission          As Integer
    Dim iOldMP                  As Integer
    Dim iCommitFlag             As Integer
    Dim iTaskPermUpdCtr         As Integer
    Dim iSecurEventUpdCtr       As Integer
    Dim lRowIndex               As Long
    Dim lNbrRows                As Long
    Dim lTaskID                 As Long
    Dim vArgs                   As Variant
    
    'Default return value
    lSaveRecord = kSaveFailure
    
    'Save current values so they can be restored
    iOldMP = MousePointer
    With sbrMain
        .Tag = CStr(.Status)
    End With
    
    'Cache values for local use
    With moClass
        Set oSysDB = .moAppDB
        Set oSysSession = .moSysSession
    End With
    sUserGroupID = Trim(txtUserGroupID)
    sUserGroupDesc = Trim(txtDescription)
    vArgs = CVar(sUserGroupID)
    bTaskGridChange = grdTaskPerm.ChangeMade
    bSecEventGridChange = grdSecEventPerm.ChangeMade
    
    'Mark the beginning of DB transaction
    oSysDB.BeginTrans
    
    'Perform actions dependent upon the current form state
    Select Case iCurrState
        Case kStateAdd, kStateAddAOF
            With oProgressBar
                .CreatePBar
                If .bInit(Me, gsBuildString(kCreateSotaSecurGrpRec, oSysDB, oSysSession, vArgs), 1000) Then
                    Me.Enabled = False
                    .ShowPBar
                Else
                    GoTo SaveRecordBail:
                End If
            End With
            
            'Log status into tsmSystemActivity table
            gLogSystemActivity moClass, oSysDB, kEventTypeOther, "Inserted UserGroup " & sUserGroupID
            
            '//////////////////////////
            '//  HEADER DATA ADDITIONS
            If bSaveHeaderData Then
                iCommitFlag = 0
                If Not bInsertUserGroup(oSysDB, sUserGroupID, sUserGroupDesc, iCommitFlag) Then
                    oSysDB.Rollback
                    GoTo SaveRecordBail:
                End If
            End If
            
            '///////////////////////////////////
            '//  TASK PERMISSION DATA ADDITIONS
            If bTaskGridChange Then
                'Update progress bar
                oProgressBar.UpdatePBar gsBuildString(kSaveTaskPermsInfo, oSysDB, oSysSession, vArgs), kDescUpdateReq
                'Determine the number of rows in the Task Permission grid
                lNbrRows = glGridGetMaxRows(grdTaskPerm)
                If lNbrRows > 0 Then
                    'Loop through all the grid rows, inserting rows that have changed
                    For lRowIndex = 1 To lNbrRows
                        'Read the old and new permission value cells
                        iOldPermission = CInt(gsGridReadCell(grdTaskPerm, lRowIndex, kColTaskPermOldPermValue))
                        iNewPermission = CInt(gsGridReadCell(grdTaskPerm, lRowIndex, kColTaskPermPermissionValue))
                        If iNewPermission = 0 Then iNewPermission = 1
                        'Attempt insert only if row in the grid has changed
                        If iNewPermission <> iOldPermission Then
                            iCommitFlag = 0
                            lTaskID = CLng(gsGridReadCell(grdTaskPerm, lRowIndex, kColTaskPermTaskID))
                            If Not bInsertTaskPerm(oSysDB, sUserGroupID, lTaskID, iNewPermission, iCommitFlag) Then
                                oSysDB.Rollback
                                GoTo SaveRecordBail:
                            End If
                        End If
                    Next
                End If
            End If
            
            '//////////////////////////////////
            '//  SECURITY EVENT DATA ADDITIONS
            If bSecEventGridChange Then
                'Update progress bar
                oProgressBar.UpdatePBar gsBuildString(kSaveSecEventInfo, oSysDB, oSysSession, vArgs), kDescUpdateReq
                'Determine the number of rows in the security event grid
                lNbrRows = glGridGetMaxRows(grdSecEventPerm)
                If lNbrRows > 0 Then
                    'Loop through all the grid rows, inserting rows that have changed
                    For lRowIndex = 1 To lNbrRows
                        'Read the old and new permission value cells
                        iOldPermission = CInt(gsGridReadCell(grdSecEventPerm, lRowIndex, kColSecEventPermOldAuthValue))
                        iNewPermission = CInt(gsGridReadCell(grdSecEventPerm, lRowIndex, kColSecEventPermAuthorizedValue))
                        
                        'Attempt insert only if row has changed
                        If iNewPermission <> iOldPermission Then
                            iCommitFlag = 0
                            sSecurEventID = CStr(gsGridReadCell(grdSecEventPerm, lRowIndex, kColSecEventPermSecEventID))
                            If Not bInsertSecurEvent(oSysDB, sUserGroupID, sSecurEventID, iNewPermission, iCommitFlag) Then
                                oSysDB.Rollback
                                GoTo SaveRecordBail:
                            End If
                        End If
                    Next
                End If
            End If
            
            'Successful ADDITION of UserGroup
            lSaveRecord = kSaveSuccess
        
        Case kStateEdit
            With oProgressBar
                .CreatePBar
                If .bInit(Me, gsBuildString(kUpdateSotaSecurGrpRec, oSysDB, oSysSession, vArgs), 1000) Then
                    Me.Enabled = False
                    .ShowPBar
                Else
                    GoTo SaveRecordBail:
                End If
            End With
            
            'Handle the tsmSystemActivity insert
            gLogSystemActivity moClass, oSysDB, kEventTypeOther, "Updated UserGroup " & sUserGroupID
            
            '//////////////////////
            '//  HEADER DATA EDITS
            If bSaveHeaderData Then
                iCommitFlag = 0     'Grid data has changed     -> don't commit txn yet, more to come
                If Not bUpdateUserGroup(oSysDB, sUserGroupID, sUserGroupDesc, miUserGroupUpdCtr, iCommitFlag) Then
                    oSysDB.Rollback
                    GoTo SaveRecordBail:
                Else
                    'Successful update, increment the update counter
                    miUserGroupUpdCtr = miUserGroupUpdCtr + 1
                End If
            End If
                
            '///////////////////////////////
            '//  TASK PERMISSION DATA EDITS
            If bTaskGridChange Then
                'Update progress bar
                oProgressBar.UpdatePBar gsBuildString(kUpdateTaskPermsInfo, oSysDB, oSysSession, vArgs), kDescUpdateReq
                'Determine the number of rows in the grid
                lNbrRows = glGridGetMaxRows(grdTaskPerm)
                If lNbrRows > 0 Then
                    'Loop through all the grid rows, saving the rows that have changed
                    For lRowIndex = 1 To lNbrRows
                        'Read the old and new permission value cells
                        iOldPermission = CInt(gsGridReadCell(grdTaskPerm, lRowIndex, kColTaskPermOldPermValue))
                        iNewPermission = CInt(gsGridReadCell(grdTaskPerm, lRowIndex, kColTaskPermPermissionValue))
                        'Attempt mods only if row's permission has changed
                        If iNewPermission <> iOldPermission Then
                            'Determine SQL statement type:  INSERT vs. UPDATE
                            sInsOrUpdRow = gsGridReadCell(grdTaskPerm, lRowIndex, kColTaskPermInsOrUpdRow)
                            Select Case UCase(Trim(sInsOrUpdRow))
                                Case Is = ksUpdateRow
                                    iCommitFlag = 0
                                    lTaskID = CLng(gsGridReadCell(grdTaskPerm, lRowIndex, kColTaskPermTaskID))
                                    iTaskPermUpdCtr = CInt(gsGridReadCell(grdTaskPerm, lRowIndex, kColTaskPermUpdateCounter))
                                    If Not bUpdateTaskPerm(oSysDB, sUserGroupID, lTaskID, iNewPermission, iTaskPermUpdCtr, iCommitFlag) Then
                                        oSysDB.Rollback
                                        GoTo SaveRecordBail:
                                    End If
                                Case Is = ksInsertRow
                                    iCommitFlag = 0
                                    lTaskID = CLng(gsGridReadCell(grdTaskPerm, lRowIndex, kColTaskPermTaskID))
                                    If Not bInsertTaskPerm(oSysDB, sUserGroupID, lTaskID, iNewPermission, iCommitFlag) Then
                                        oSysDB.Rollback
                                        GoTo SaveRecordBail:
                                    End If
                                Case Else
                                    giSotaMsgBox Me, oSysSession, kmsgUnexpectedOperation, UCase(Trim(sInsOrUpdRow))
                            End Select
                        End If
                    Next
                End If
            End If
            
            '//////////////////////////////
            '//  SECURITY EVENT DATA EDITS
            If bSecEventGridChange Then
                'Update progress bar
                oProgressBar.UpdatePBar gsBuildString(kUpdateSecEventInfo, oSysDB, oSysSession, vArgs), kDescUpdateReq
                'Determine the number of rows currently in the grid
                lNbrRows = glGridGetMaxRows(grdSecEventPerm)
                If lNbrRows > 0 Then
                    'Loop through all the rows, writing out the changes
                    For lRowIndex = 1 To lNbrRows
                        'Read the old and new permission value cells
                        iOldPermission = CInt(gsGridReadCell(grdSecEventPerm, lRowIndex, kColSecEventPermOldAuthValue))
                        iNewPermission = CInt(gsGridReadCell(grdSecEventPerm, lRowIndex, kColSecEventPermAuthorizedValue))
                        'Attempt mods only if the row's permission has changed
                        If iNewPermission <> iOldPermission Then
                            'Determine SQL statement type:  INSERT vs. UPDATE
                            sInsOrUpdRow = gsGridReadCell(grdSecEventPerm, lRowIndex, kColSecEventPermInsOrUpdRow)
                            Select Case UCase(Trim(sInsOrUpdRow))
                                Case Is = ksUpdateRow
                                    iCommitFlag = 0
                                    sSecurEventID = gsGridReadCell(grdSecEventPerm, lRowIndex, kColSecEventPermSecEventID)
                                    iSecurEventUpdCtr = CInt(gsGridReadCell(grdSecEventPerm, lRowIndex, kColSecEventPermUpdateCounter))
                                    If Not bUpdateSecurEvent(oSysDB, sUserGroupID, sSecurEventID, iNewPermission, iSecurEventUpdCtr, iCommitFlag) Then
                                        oSysDB.Rollback
                                        GoTo SaveRecordBail:
                                    End If
                                Case Is = ksInsertRow
                                    iCommitFlag = 0
                                    sSecurEventID = gsGridReadCell(grdSecEventPerm, lRowIndex, kColSecEventPermSecEventID)
                                    If Not bInsertSecurEvent(oSysDB, sUserGroupID, sSecurEventID, iNewPermission, iCommitFlag) Then
                                        oSysDB.Rollback
                                        GoTo SaveRecordBail:
                                    End If
                                Case Else
                                    giSotaMsgBox Me, oSysSession, kmsgUnexpectedOperation, UCase(Trim(sInsOrUpdRow))
                            End Select
                        End If
                    Next
                End If
            End If
            
            'Successful EDIT of UserGroup
            lSaveRecord = kSaveSuccess
        
        Case Else
            giSotaMsgBox Me, oSysSession, kmsgUnexpectedState, Format(Me.iState)
    End Select

    'Commit the transaction to the database
    oSysDB.CommitTrans

    'Re-initialize grid
    grdTaskPerm.ChangeMade = False
    grdSecEventPerm.ChangeMade = False
    
SaveRecordBail:
    'Restore the old mousepointer
    If iOldMP <> vbHourglass Then
        MousePointer = iOldMP
    Else
        MousePointer = vbDefault
    End If
    
    'Progress bar cleanup
    With Me
        If Not .Enabled Then .Enabled = True
    End With
    oProgressBar.DestroyPBar
    Set oProgressBar = Nothing
        
    'Cleanup cached objects
    Set oSysDB = Nothing
    Set oSysSession = Nothing

    'Restore status control
    sbrMain.Status = (sbrMain.Tag)
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:
    'Restore the mousepointer
    MousePointer = vbDefault
    'Rollback the DB transaction
    oSysDB.Rollback
    'Set failure return code
    lSaveRecord = kSaveFailure
    
    'Progress bar cleanup
    With Me
        If Not .Enabled Then .Enabled = True
    End With
    oProgressBar.DestroyPBar
    Set oProgressBar = Nothing
        
    'Cleanup cached objects
    Set oSysDB = Nothing
    Set oSysSession = Nothing
    
    'Restore status control
    sbrMain.Status = (sbrMain.Tag)
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lSaveRecord", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function lFinishRecord(iState As Integer, bSaveHeaderData As Boolean) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'Default return value
    lFinishRecord = kSaveFailure
    
    Select Case iState
        Case kStateAdd, kStateAddAOF, kStateEdit
            lFinishRecord = lSaveRecord(iState, bSaveHeaderData)
            If lFinishRecord = kSaveFailure Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgSaveChangesError
            End If
        Case kStateInit, kStateStart, kStateStartDisplayOnly, kStateNone, kStateDisplayOnly
            lFinishRecord = kSaveSuccess
        Case Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedState, Format(iState)
    End Select

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lFinishRecord", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bIsValidString(sDescription As String) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Default return value
    bIsValidString = False

    'Validate the description
    If Len(Trim(sDescription)) > 0 Then
        bIsValidString = True
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidString", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bValidEntry() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
   
    'Set default return value
    bValidEntry = False
    
    If bIsValidString(txtUserGroupID) Then
        If bIsValidString(txtDescription) Then
            bValidEntry = True
        Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgDescriptionRequired
            With txtDescription
                If Me.Visible And .Visible And .Enabled Then
                    .SetFocus
                End If
            End With
        End If
    Else
        giSotaMsgBox Me, moClass.moSysSession, kmsgSecurityGroupRequired
        With txtUserGroupID
            If Me.Visible And .Visible And .Enabled Then
                .SetFocus
            End If
        End With
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bValidEntry", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Private Function bDataHasChanged(iCurrState As Integer, bCheckHeaderData As Boolean) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
      
    'Default return value
    bDataHasChanged = False

    Select Case iCurrState
        Case kStateStart, kStateNone, kStateStartDisplayOnly, kStateDisplayOnly
            bDataHasChanged = False
        Case kStateAdd, kStateAddAOF
            If bCheckHeaderData Or grdTaskPerm.ChangeMade Or grdSecEventPerm.ChangeMade Then
                bDataHasChanged = True
            End If
        Case kStateEdit
            If bCheckHeaderData Then
                If (UCase(msOrigUserGroup) <> UCase(txtUserGroupID)) Or (UCase(msOrigDescription) <> UCase(txtDescription)) Then
                    bDataHasChanged = True
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Function
                End If
            End If
            If grdTaskPerm.ChangeMade Or grdSecEventPerm.ChangeMade Then
                bDataHasChanged = True
            End If
        Case Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedState, Format(iCurrState)
    End Select

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bDataHasChanged", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub SetFormControls(iState As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Initialize grid "row selection" control vars
    mlTaskPermRowsSelected = 0
    mlSecEventPermRowsSelected = 0
    mbAllTaskPermRowsSelected = False
    mbAllSecEventPermRowsSelected = False
    
    'Set the active tab
    tabSecurGroupMaint.Tab = kTaskPermTab
    
    Select Case iState
        Case kStateStart, kStateNone, kStateStartDisplayOnly
            'Initialize grid
            grdTaskPerm.ChangeMade = False
            gGridSetMaxRows grdTaskPerm, 0
            grdSecEventPerm.ChangeMade = False
            gGridSetMaxRows grdSecEventPerm, 0
            
            'Enable/Disable controls specific to form state
            txtUserGroupID.Enabled = True
'            cmdCopyFrom.Enabled = False

            'Initialize forms listbox controls and associated flags
            InitListBoxControls
            
            'Disable the both permission tabs controls
            DisableTaskPermControls
            DisableSecEventPermControls
            
            'Clear/initialize controls
            With txtUserGroupID
                .Text = Empty
                .Tag = Empty
            End With
            With txtDescription
                .Text = Empty
            End With
            With txtModuleName
                .Caption = Empty
            End With
            
            'Initialize module level variables
            miUserGroupUpdCtr = 0
            msOrigUserGroup = Empty
            msOrigDescription = Empty
            
            'Set current focus
            If Me.Visible = True Then
                With txtUserGroupID
                    If .Visible = True And .Enabled = True Then
                        .SetFocus
                    End If
                End With
            End If
        
        Case kStateAdd, kStateAddAOF
            'Initialize grid
            grdTaskPerm.ChangeMade = False
            grdSecEventPerm.ChangeMade = False
            
            'Enable/Disable controls specific to form state
            txtUserGroupID.Enabled = False
            
            'Initialize forms listbox controls and associated flags
            InitListBoxControls
            
            'Handle moving focus to next field
            If Me.Visible = True Then
                With txtDescription
                    If .Visible = True And .Enabled = True Then
                        .SetFocus
                    End If
                End With
            End If
        
        Case kStateEdit, kStateDisplayOnly
            'Enable/Disable controls specific to form state
            txtUserGroupID.Enabled = False
            
            'Initialize form listbox controls and associated flags
            InitListBoxControls
            
            'Handle moving focus to next field
            If Me.Visible = True Then
                With txtDescription
                    If .Visible = True And .Enabled = True Then
                        .SetFocus
                    End If
                End With
            End If
        
        Case Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedState, Format(iState)
    
    End Select
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetFormControls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub InitializeToState(iState As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Select Case iState
        Case kStateInit
        
        Case kStateNone
            moClass.lUIActive = kChildObjectInactive
            sbrMain.Status = SOTA_SB_NONE
            SetFormControls iState
        
        Case kStateStart, kStateStartDisplayOnly
            moClass.lUIActive = kChildObjectInactive
            sbrMain.Status = SOTA_SB_START
            SetFormControls iState
        
        Case kStateAdd
            moClass.lUIActive = kChildObjectActive
            sbrMain.Status = SOTA_SB_ADD
            SetFormControls iState
        
        Case kStateAddAOF
            moClass.lUIActive = kChildObjectActive
            sbrMain.Status = SOTA_SB_ADD
            SetFormControls iState
        
        Case kStateEdit
            moClass.lUIActive = kChildObjectActive
            sbrMain.Status = SOTA_SB_EDIT
            SetFormControls iState
        
        Case kStateDisplayOnly
            moClass.lUIActive = kChildObjectInactive
            sbrMain.Status = SOTA_SB_LOCKED
            SetFormControls iState
        
        Case Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedState, Format(iState)
    End Select
    
    If iState = kStateDisplayOnly Or iState = kStateStartDisplayOnly Or iState = kStateInit Then
        sbrMain.Status = SOTA_SB_LOCKED
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitializeToState", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetUpSecEventPermGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Set SPREADSHEET-GLOBAL characteristics
    FormatGrid grdSecEventPerm

    With grdSecEventPerm
        'Turn off auto-redraw during grid formatting
        .redraw = False
        'Maximum columns
        gGridSetMaxCols grdSecEventPerm, kSecEventMaxCols
        'Module
        gGridSetColumnWidth grdSecEventPerm, kColTaskModuleID, 5
        gGridSetColumnType grdSecEventPerm, kColSecEventPermModuleID, SS_CELL_TYPE_STATIC_TEXT
        gGridSetCellBorderStyle grdSecEventPerm, 1, kColSecEventPermModuleID, glGridGetMaxRows(grdSecEventPerm), kColSecEventPermModuleID, SS_BORDER_STYLE_BLANK
        gGridHAlignColumn grdSecEventPerm, kColSecEventPermModuleID, SS_CELL_TYPE_STATIC_TEXT, SS_CELL_H_ALIGN_CENTER
        'Event Description
        gGridSetColumnWidth grdSecEventPerm, kColSecEventPermDescription, 38
        gGridSetColumnType grdSecEventPerm, kColSecEventPermDescription, SS_CELL_TYPE_STATIC_TEXT
        gGridSetCellBorderStyle grdSecEventPerm, 1, kColSecEventPermDescription, glGridGetMaxRows(grdSecEventPerm), kColSecEventPermDescription, SS_BORDER_STYLE_BLANK
        'Event Permission Text
        gGridSetColumnWidth grdSecEventPerm, kColSecEventPermAuthorizedText, 11
        gGridSetColumnType grdSecEventPerm, kColSecEventPermAuthorizedText, SS_CELL_TYPE_COMBOBOX, msNo & vbTab & msYes & vbTab & vbTab
        .TypeComboBoxEditable = False
        'Event ID
        gGridSetColumnWidth grdSecEventPerm, kColSecEventPermSecEventID, 12
        gGridSetColumnType grdSecEventPerm, kColSecEventPermSecEventID, SS_CELL_TYPE_STATIC_TEXT
        gGridSetCellBorderStyle grdSecEventPerm, 1, kColSecEventPermSecEventID, glGridGetMaxRows(grdSecEventPerm), kColSecEventPermSecEventID, SS_BORDER_STYLE_BLANK
        .Col = kColSecEventPermSecEventID
        .UserResizeCol = SS_USER_RESIZE_OFF
        'Current Permission Value
        gGridSetColumnWidth grdSecEventPerm, kColSecEventPermAuthorizedValue, 12
        gGridSetColumnType grdSecEventPerm, kColSecEventPermAuthorizedValue, SS_CELL_TYPE_INTEGER
        gGridSetCellBorderStyle grdSecEventPerm, 1, kColSecEventPermAuthorizedValue, glGridGetMaxRows(grdSecEventPerm), kColSecEventPermAuthorizedValue, SS_BORDER_STYLE_BLANK
        .Col = kColSecEventPermSecEventID
        .UserResizeCol = SS_USER_RESIZE_OFF
        'Old Permission Value
        gGridSetColumnWidth grdSecEventPerm, kColSecEventPermOldAuthValue, 12
        gGridSetColumnType grdSecEventPerm, kColSecEventPermOldAuthValue, SS_CELL_TYPE_INTEGER
        gGridSetCellBorderStyle grdSecEventPerm, 1, kColSecEventPermOldAuthValue, glGridGetMaxRows(grdSecEventPerm), kColSecEventPermOldAuthValue, SS_BORDER_STYLE_BLANK
        .Col = kColSecEventPermSecEventID
        .UserResizeCol = SS_USER_RESIZE_OFF
        'Insert or Update Flag
        gGridSetColumnWidth grdSecEventPerm, kColSecEventPermInsOrUpdRow, 6
        gGridSetColumnType grdSecEventPerm, kColSecEventPermInsOrUpdRow, SS_CELL_TYPE_STATIC_TEXT
        gGridSetCellBorderStyle grdSecEventPerm, 1, kColSecEventPermInsOrUpdRow, glGridGetMaxRows(grdSecEventPerm), kColSecEventPermInsOrUpdRow, SS_BORDER_STYLE_BLANK
        .Col = kColSecEventPermSecEventID
        .UserResizeCol = SS_USER_RESIZE_OFF
        'Update Counter
        gGridSetColumnWidth grdSecEventPerm, kColSecEventPermUpdateCounter, 12
        gGridSetColumnType grdSecEventPerm, kColSecEventPermUpdateCounter, SS_CELL_TYPE_INTEGER
        gGridSetCellBorderStyle grdSecEventPerm, 1, kColSecEventPermUpdateCounter, glGridGetMaxRows(grdSecEventPerm), kColSecEventPermUpdateCounter, SS_BORDER_STYLE_BLANK
        .Col = kColSecEventPermSecEventID
        .UserResizeCol = SS_USER_RESIZE_OFF
        'Blank column
        gGridHideColumn grdSecEventPerm, kColSecEventPermBlankColumn
        .Col = kColSecEventPermBlankColumn
        .UserResizeCol = SS_USER_RESIZE_OFF
        'Set column headings
        gGridSetHeader grdSecEventPerm, kColSecEventPermModuleID, gsBuildString(kModuleID, moClass.moAppDB, moClass.moSysSession)
        gGridSetHeader grdSecEventPerm, kColSecEventPermDescription, gsBuildString(kSecEventDesc, moClass.moAppDB, moClass.moSysSession)
        gGridSetHeader grdSecEventPerm, kColSecEventPermAuthorizedText, gsBuildString(kSecurityPermission, moClass.moAppDB, moClass.moSysSession)
        'Hide the Security Event ID
        gGridHideColumn grdSecEventPerm, kColSecEventPermSecEventID
        'Hide the New Sec Event Permission Value
        gGridHideColumn grdSecEventPerm, kColSecEventPermAuthorizedValue
        'Hide the Old Sec Event Permission Value
        gGridHideColumn grdSecEventPerm, kColSecEventPermOldAuthValue
        'Hide the Insert or Update Flag
        gGridHideColumn grdSecEventPerm, kColSecEventPermInsOrUpdRow
        'Hide the Update Counter
        gGridHideColumn grdSecEventPerm, kColSecEventPermUpdateCounter
        'Hide the Blank column
        gGridHideColumn grdSecEventPerm, kColSecEventPermBlankColumn
        'Turn off editing of locked cells
        .Protect = True
        'Re-enable auto redrawing of the grid
        .redraw = True
    End With
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetUpSecEventPermGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub SetUpTaskPermGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Set SPREADSHEET-GLOBAL characteristics
    FormatGrid grdTaskPerm

    With grdTaskPerm
        'Disable auto redraw of the grid during setup
        .redraw = False
        'Define maximum columns within the grid
        gGridSetMaxCols grdTaskPerm, kTaskPermMaxCols
        'Module
        gGridSetColumnWidth grdTaskPerm, kColTaskModuleID, 5
        gGridSetColumnType grdTaskPerm, kColTaskModuleID, SS_CELL_TYPE_STATIC_TEXT
        gGridSetCellBorderStyle grdTaskPerm, 1, kColTaskModuleID, glGridGetMaxRows(grdTaskPerm), kColTaskModuleID, SS_BORDER_STYLE_BLANK
        gGridHAlignColumn grdTaskPerm, kColTaskModuleID, SS_CELL_TYPE_STATIC_TEXT, SS_CELL_H_ALIGN_CENTER
        'Task Description
        gGridSetColumnWidth grdTaskPerm, kColTaskPermTaskDesc, 38
        gGridSetColumnType grdTaskPerm, kColTaskPermTaskDesc, SS_CELL_TYPE_STATIC_TEXT
        gGridSetCellBorderStyle grdTaskPerm, 1, kColTaskPermTaskDesc, glGridGetMaxRows(grdTaskPerm), kColTaskPermTaskDesc, SS_BORDER_STYLE_BLANK
        'Task Permission Text
        gGridSetColumnWidth grdTaskPerm, kColTaskPermPermissionText, 11
        gGridSetColumnType grdTaskPerm, kColTaskPermPermissionText, SS_CELL_TYPE_COMBOBOX, msExcluded & vbTab & msDisplayOnly & vbTab & msNormal & vbTab & msSupervisory & vbTab & vbTab
        .TypeComboBoxEditable = False
        'Task ID
        gGridSetColumnWidth grdTaskPerm, kColTaskPermTaskID, 12
        gGridSetColumnType grdTaskPerm, kColTaskPermTaskID, SS_CELL_TYPE_INTEGER
        gGridSetCellBorderStyle grdTaskPerm, 1, kColTaskPermTaskID, glGridGetMaxRows(grdTaskPerm), kColTaskPermTaskID, SS_BORDER_STYLE_BLANK
        .Col = kColTaskPermTaskID
        .UserResizeCol = SS_USER_RESIZE_OFF
        'Task Permission Current Value
        gGridSetColumnWidth grdTaskPerm, kColTaskPermPermissionValue, 12
        gGridSetColumnType grdTaskPerm, kColTaskPermPermissionValue, SS_CELL_TYPE_INTEGER
        gGridSetCellBorderStyle grdTaskPerm, 1, kColTaskPermPermissionValue, glGridGetMaxRows(grdTaskPerm), kColTaskPermPermissionValue, SS_BORDER_STYLE_BLANK
        .Col = kColTaskPermPermissionValue
        .UserResizeCol = SS_USER_RESIZE_OFF
        'Task Permission Old Value
        gGridSetColumnWidth grdTaskPerm, kColTaskPermOldPermValue, 12
        gGridSetColumnType grdTaskPerm, kColTaskPermOldPermValue, SS_CELL_TYPE_INTEGER
        gGridSetCellBorderStyle grdTaskPerm, 1, kColTaskPermOldPermValue, glGridGetMaxRows(grdTaskPerm), kColTaskPermOldPermValue, SS_BORDER_STYLE_BLANK
        .Col = kColTaskPermOldPermValue
        .UserResizeCol = SS_USER_RESIZE_OFF
        'Insert or Update Flag
        gGridSetColumnWidth grdTaskPerm, kColTaskPermInsOrUpdRow, 6
        gGridSetColumnType grdTaskPerm, kColTaskPermInsOrUpdRow, SS_CELL_TYPE_STATIC_TEXT
        gGridSetCellBorderStyle grdTaskPerm, 1, kColTaskPermInsOrUpdRow, glGridGetMaxRows(grdTaskPerm), kColTaskPermInsOrUpdRow, SS_BORDER_STYLE_BLANK
        .Col = kColTaskPermInsOrUpdRow
        .UserResizeCol = SS_USER_RESIZE_OFF
         'Update Counter
        gGridSetColumnWidth grdTaskPerm, kColTaskPermUpdateCounter, 12
        gGridSetColumnType grdTaskPerm, kColTaskPermUpdateCounter, SS_CELL_TYPE_INTEGER
        gGridSetCellBorderStyle grdTaskPerm, 1, kColTaskPermUpdateCounter, glGridGetMaxRows(grdTaskPerm), kColTaskPermUpdateCounter, SS_BORDER_STYLE_BLANK
        .Col = kColTaskPermUpdateCounter
        .UserResizeCol = SS_USER_RESIZE_OFF
        'Blank column
        gGridHideColumn grdTaskPerm, kColTaskPermBlankColumn
        .Col = kColTaskPermBlankColumn
        .UserResizeCol = SS_USER_RESIZE_OFF
        
        'Set column headings
        gGridSetHeader grdTaskPerm, kColTaskPermTaskDesc, gsBuildString(kTaskDesc, moClass.moAppDB, moClass.moSysSession)
        gGridSetHeader grdTaskPerm, kColTaskModuleID, gsBuildString(kModuleID, moClass.moAppDB, moClass.moSysSession)
        gGridSetHeader grdTaskPerm, kColTaskPermPermissionText, gsBuildString(kSecurityPermission, moClass.moAppDB, moClass.moSysSession)

        'Hide the Task ID
        gGridHideColumn grdTaskPerm, kColTaskPermTaskID
        'Hide the Current Task Permission Value
        gGridHideColumn grdTaskPerm, kColTaskPermPermissionValue
        'Hide the Old Task Permission Value
        gGridHideColumn grdTaskPerm, kColTaskPermOldPermValue
        'Hide the Insert or Update Flag
        gGridHideColumn grdTaskPerm, kColTaskPermInsOrUpdRow
        'Hide the Update Counter
        gGridHideColumn grdTaskPerm, kColTaskPermUpdateCounter
        'Hide the Blank column
        gGridHideColumn grdTaskPerm, kColTaskPermBlankColumn
        'Disable editing of locked columns
        .Protect = True
        'Re-enable auto redraw of grid
        .redraw = True
    End With
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetUpTaskPermGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Function bLoadTaskPermLB() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Default return value
    bLoadTaskPermLB = False
    
    'Clear all current entries from the combo box
    lstDfltPermTaskPerm.Clear

    'Add the FIXED Task Permission Types
    With moClass
        msExcluded = gsBuildString(kExcluded, .moAppDB, .moSysSession)
        gComboAddItem lstDfltPermTaskPerm, msExcluded, sotaTB_EXCLUDED
        
        msDisplayOnly = gsBuildString(kDisplayOnly, .moAppDB, .moSysSession)
        gComboAddItem lstDfltPermTaskPerm, msDisplayOnly, sotaTB_DISPLAYONLY
        
        msNormal = gsBuildString(kNormal, .moAppDB, .moSysSession)
        gComboAddItem lstDfltPermTaskPerm, msNormal, sotaTB_NORMAL
        
        msSupervisory = gsBuildString(kSupervisory, .moAppDB, .moSysSession)
        gComboAddItem lstDfltPermTaskPerm, msSupervisory, sotaTB_SUPERVISORY
    End With

    'Successful load
    bLoadTaskPermLB = True
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bLoadTaskPermLB", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Property Get bSaved() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    bSaved = mbSaved
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Property

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSaved_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property


Public Property Let bSaved(bNewSaved As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    mbSaved = bNewSaved
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Property

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSaved_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property



Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    bCancelShutDown = mbCancelShutDown
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Property

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCancelShutDown_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Private Sub cmdApply_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdApply(Index), True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    
    On Error GoTo ExpectedErrorRoutine:

    Dim lRowIndex   As Long
    Dim iOldMP      As Integer

    iOldMP = MousePointer
    MousePointer = vbHourglass
    
    Select Case Index
        Case Is = kcmdTaskPermApply
            'Determine if row is selected, and if so update the cell and fire the grids change event
            For lRowIndex = 1 To glGridGetMaxRows(grdTaskPerm)
                If glGridGetCellColor(grdTaskPerm, lRowIndex, kColTaskPermPermissionText) = vbHighlight Then
                    gGridUpdateCell grdTaskPerm, lRowIndex, kColTaskPermPermissionText, lstDfltPermTaskPerm.ListIndex
                    grdTaskPerm_Change kColTaskPermPermissionText, lRowIndex
                End If
                'Re-set all rows to natural state
                With grdTaskPerm
                    .Col = kColTaskPermTaskDesc
                    .BackColor = vbButtonFace
                    .ForeColor = vbButtonText
                    .Col = kColTaskPermPermissionText
                    .BackColor = vbWindowBackground
                    .ForeColor = vbWindowText
                End With
            Next lRowIndex
            'Disable the appropriate controls
            With lstDfltPermTaskPerm
                .Enabled = False
                .BackColor = vbButtonFace
                .Refresh
            End With
            With cmdApply(kcmdTaskPermApply)
                .Enabled = False
                .Refresh
            End With
        Case Is = kcmdSecEventPermApply
            'Determine if row is selected, and if so update the cell and fire the grids change event
            For lRowIndex = 1 To glGridGetMaxRows(grdSecEventPerm)
                If glGridGetCellColor(grdSecEventPerm, lRowIndex, kColSecEventPermAuthorizedText) = vbHighlight Then
                    gGridUpdateCell grdSecEventPerm, lRowIndex, kColSecEventPermAuthorizedText, lstDfltPermSecEventPerm.ListIndex
                    grdSecEventPerm_Change kColSecEventPermAuthorizedText, lRowIndex
                End If
                'Re-set all rows to natural state
                With grdSecEventPerm
                    .Col = kColSecEventPermDescription
                    .BackColor = vbButtonFace
                    .ForeColor = vbButtonText
                    .Col = kColSecEventPermAuthorizedText
                    .BackColor = vbWindowBackground
                    .ForeColor = vbWindowText
                End With
            Next lRowIndex
            'Disable the appropriate controls
            With lstDfltPermSecEventPerm
                .Enabled = False
                .BackColor = vbButtonFace
                .Refresh
            End With
            With cmdApply(kcmdSecEventPermApply)
                .Enabled = False
                .Refresh
            End With
        Case Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedCtrlArryIndex
    End Select
    
    If iOldMP <> vbHourglass Then
        MousePointer = iOldMP
    Else
        MousePointer = vbDefault
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
ExpectedErrorRoutine:
    MousePointer = vbDefault

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdApply_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdApply_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Turn the extend selection mode indicators 'off'
    mlExtendSelTaskPerm = 0
    mlExtendSelSecEventPerm = 0
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdApply_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdCopyFrom_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdCopyFrom, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    
    cmdCopyFrom.Enabled = False
    
    'Click the 'Copy From' Navigator button
    navCopyFrom = True
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdCopyFrom_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


'RKL DEJ 2016-05-31 (Start)
Private Sub CmdWhseSecurity_Click()
    Set frmWhseSecurity.oSysDB = moClass.moAppDB
    frmWhseSecurity.sUserGroupID = Trim(txtUserGroupID.Text)
    
    frmWhseSecurity.Show vbModal, Me
    
End Sub
'RKL DEJ 2016-05-31 (StOP)

Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyFormCust
        End If
    End If
#End If
    
    With txtUserGroupID
        If Me.Visible And .Visible And .Enabled Then
            .SetFocus
        End If
    End With
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'Determine if the user used the form-level keys and process them
    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
    End Select
    
    'Determine which control is currently active
    Select Case UCase(Trim(ActiveControl.Name))
        Case Is = ksTaskPermGridName
            'The Task Permissions grid is currently active
            Select Case True
                Case (Shift = vbAltMask And KeyCode = vbKeyReturn)
                    'The user has pressed the 'Alt+Return' keys
                    gGridSetSelectRow grdTaskPerm, glGridGetActiveRow(grdTaskPerm)
            End Select
        
        Case Is = ksSecEventPermGridName
            'The Sec Event Permissions grid is currently active
            Select Case True
                Case (Shift = vbAltMask And KeyCode = vbKeyReturn)
                    'The user has pressed the 'Alt+Return' keys
                    gGridSetSelectRow grdTaskPerm, glGridGetActiveRow(grdTaskPerm)
            End Select
    End Select
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        Case Else
            'Process other keys here
    End Select
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

  Set sbrMain.Framework = moClass.moFramework


    'Initialize/assign form level vars
    InitFormVars
    
    'Initialize status bar
    
    'Set application security level
    miSecurityLevel = giSetAppSecurity(moClass, tbrMain)
    
    'Bind toolbar
    BindToolBar
       
    'Assign default browse navigator filter
    miFilter = RSID_UNFILTERED
    
    'Load array used to translate ModuleID into ModuleNo
    gLoadModuleInfoArray moClass.moAppDB
    
    'Continue only if all the forms comboboxes loaded successfully
    If bLoadAllLB() Then
        'Setup task permission grid
        SetUpTaskPermGrid
        'Setup security event grid
        SetUpSecEventPermGrid
        'Initialize form state
        InitState
        'Bind context menus
        BindContextMenu
        'ObjMgr "utility object" implementation
        Set moUtility = CreateObject(kclsFWUtilityObj)
        If Not moUtility Is Nothing Then
            mbLoadSuccess = True
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    bLoadSuccess = mbLoadSuccess
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Property

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bLoadSuccess_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'Turn the extend selection mode indicators 'off'
    mlExtendSelTaskPerm = 0
    mlExtendSelSecEventPerm = 0
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Paint()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Paint", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
    
    Dim iCtr        As Integer
    Dim lReturnCode As Long

    'Reset the CancelShutDown flag
    mbCancelShutDown = False

    'Continue only if an error condition does not exist
    If moClass.mlError = 0 Then
        'Determine action dependent upon current "form state"
        Select Case Me.iState
            Case kStateAdd, kStateEdit, kStateAddAOF
                If miSecurityLevel > sotaTB_DISPLAYONLY Then
                    'See if any data has changed, including header data
                    If bDataHasChanged(Me.iState, True) Then
                        'Prompt user to save changes
                        Select Case giSotaMsgBox(Me, moClass.moSysSession, kmsgSaveChanges)
                            Case kretYes
                                If bValidEntry() Then
                                    'Save the data
                                    lReturnCode = lFinishRecord(Me.iState, True)
                                    'Get next form state as determined by success/fail of the save operation
                                    ManageAppState iGetNextFinishState(Me.iState, lReturnCode)
                                    'Initialize form to new state
                                    InitializeToState Me.iState
                                    'Evaluate attempted init of form state
                                    Select Case Me.iState
                                        Case kStateStart, kStateNone
                                            'Do Nothing
                                        Case Else
                                            'Form state incorrectly initialized -> cancel
                                            mbCancelShutDown = True
                                            Cancel = True
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                                            Exit Sub
                                    End Select
                                Else
                                    'Set variables to indicate failed/cancelled shutdown and bail out
                                    mbCancelShutDown = True
                                    Cancel = True
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                                    Exit Sub
                                End If
                            Case kretNo
                                'Initialize form to new state
                                ManageAppState iGetNextCancelState(Me.iState)
                                InitializeToState Me.iState
                            Case kretCancel
                                mbCancelShutDown = True
                                Cancel = True
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                                Exit Sub
                        End Select
                    End If
                End If
            Case Else
                'Do Nothing
        End Select
    
        'Verfiy any child forms loaded from this form have been destroyed before unloading the main form
        For iCtr = 0 To Forms.Count - 1
            If (Not Forms(iCtr) Is Me And Not Forms(iCtr) Is Nothing) Then
                If Forms(iCtr).Visible Then GoTo CancelShutDown:
            End If
        Next iCtr
        
        Select Case UnloadMode
            Case vbFormCode
               'Do Nothing.
            Case Else
                Select Case mlRunMode
                    Case kContextAOF
                        Me.Hide
                        GoTo CancelShutDown:
                    Case Else
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                End Select
        End Select
    End If
    
    'Perform clean shutdown of form
    PerformCleanShutDown
    
    'ObjMgr "utility object" implementation
    Set moUtility = Nothing
    
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
        Case Else
            'Do nothing
    End Select
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    
    On Error Resume Next
    
    Dim i As Integer
    
    'Unload all forms loaded from this main form
    For i = 0 To Forms.Count - 1
        If (Not Forms(i) Is Me And Not Forms(i) Is Nothing) Then
            Unload Forms(i)
        End If
    Next
     
    'Remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Set oClass = moClass
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Property

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Set moClass = oNewClass
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Property

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    lRunMode = mlRunMode
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Property

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    mlRunMode = lNewRunMode
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Property

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Resize controls DOWNWARD
    gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, _
        tabSecurGroupMaint, fraTaskPermissions, fraSecEventPermissions, _
        grdTaskPerm, grdSecEventPerm
    
    'Resize controls to the RIGHT
    gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, _
        fraSecurityGroup1, tabSecurGroupMaint, fraTaskPermissions, _
        fraSecEventPermissions, grdTaskPerm, grdSecEventPerm, _
        fraSecurityGroup2
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    Set moContextMenu = Nothing
    Set moSotaObjects = Nothing
    Set moClass = Nothing
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub fraSecEventPermissions_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Turn the extend selection mode indicators 'off'
    mlExtendSelTaskPerm = 0
    mlExtendSelSecEventPerm = 0
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "fraSecEventPermissions_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub fraSecurityGroup1_DragDrop(Source As Control, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Turn the extend selection mode indicators 'off'
    mlExtendSelTaskPerm = 0
    mlExtendSelSecEventPerm = 0
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "fraSecurityGroup1_DragDrop", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub fraSecurityGroup2_DragDrop(Source As Control, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Turn the extend selection mode indicators 'off'
    mlExtendSelTaskPerm = 0
    mlExtendSelSecEventPerm = 0
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "fraSecurityGroup2_DragDrop", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub fraTaskPermissions_DragDrop(Source As Control, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Turn the extend selection mode indicators 'off'
    mlExtendSelTaskPerm = 0
    mlExtendSelSecEventPerm = 0
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "fraTaskPermissions_DragDrop", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSecEventPerm_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim sNewSecEventPermAuthorizedText  As String
    Dim sNewSecEventPermAuthorizedValue As String
    Dim iNewSecEventPermListIndex       As Integer
    
    'If the Sec Event Perm Text has changed on a "real" row
    If Col = kColSecEventPermAuthorizedText And Row > 0 Then
        'Get the cell's new text
        sNewSecEventPermAuthorizedText = gsGridReadCellText(grdSecEventPerm, Row, kColSecEventPermAuthorizedText)
        'Determine the Sec Event Perm combo box "ListIndex" from the cell's new text
        iNewSecEventPermListIndex = giListIndexFromText(lstDfltPermSecEventPerm, sNewSecEventPermAuthorizedText)
        'From the combo's "ListIndex" determine the new Sec Event Perm Value
        sNewSecEventPermAuthorizedValue = Str(lstDfltPermSecEventPerm.ItemData(iNewSecEventPermListIndex))
        'Update the current rows Sec Event Perm Value
        gGridUpdateCell grdSecEventPerm, Row, kColSecEventPermAuthorizedValue, sNewSecEventPermAuthorizedValue
        'Set grid property to indicate change
        grdSecEventPerm.ChangeMade = True
        'If the row is currently selected
        If glGridGetCellColor(grdSecEventPerm, Row, kColSecEventPermDescription) = vbHighlight Then
            'Set rows back and fore color to normal -> "unselected"
            ToggleRowColor grdSecEventPerm, kUNSelectRow, Row
            'Reduce the total number of rows selected by 1
            mlSecEventPermRowsSelected = mlSecEventPermRowsSelected - 1
            'Set flag to indicate all rows have not been selected
            mbAllSecEventPermRowsSelected = False
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSecEventPerm_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSecEventPerm_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lRowIndex           As Long
    Dim lFirstSelectedRow   As Long
    Dim lLastSelectedRow    As Long
    Dim lNbrRows            As Long

    'Clear all block selections
    gGridClearSelectBlock grdSecEventPerm
    
    If (Col > 0 And mlSecEventPermRowsSelected > 0) Then
        If glGridGetCellColor(grdSecEventPerm, Row, kColSecEventPermDescription) = vbWindowBackground Then
            lNbrRows = glGridGetMaxRows(grdSecEventPerm)
            For lRowIndex = 1 To lNbrRows
                If glGridGetCellColor(grdSecEventPerm, lRowIndex, kColSecEventPermDescription) = vbHighlight Then
                    ToggleRowColor grdSecEventPerm, kUNSelectRow, lRowIndex
                    'Subtract one from the number of rows selected
                    mlSecEventPermRowsSelected = mlSecEventPermRowsSelected - 1
                    'We have at least one row that is NOT selected
                    mbAllSecEventPermRowsSelected = False
                    'See if we can stop checking for selected rows now
                    If mlSecEventPermRowsSelected = 0 Then
                        'We have successfully unselected all selected rows
                        'Exit this for/next loop
                        Exit For
                    End If
                End If
            Next lRowIndex
        End If
    End If
    
    'Determine which column has been clicked
    Select Case Col
        Case Is <= 0
            'The user has clicked on the left-side 'row number' column
            'Determine which row has been clicked
            Select Case Row
                Case Is <= 0
                    'The user wants to select or deselect all rows
                    If mbAllSecEventPermRowsSelected = True Then
                        'All rows ARE already selected
                        'Set the row color to indicate 'NOT selected' for all rows
                        ToggleRowColor grdSecEventPerm, kUNSelectRow, Row
                        
                        'Set the number of rows selected to zero
                        mlSecEventPermRowsSelected = 0
                    Else
                        'All rows are NOT selected
                        'Set the row color to indicate 'selected' for all rows
                        ToggleRowColor grdSecEventPerm, kSelectRow, Row
                        
                        'Set the number of rows selected to max rows
                        mlSecEventPermRowsSelected = glGridGetMaxRows(grdSecEventPerm)
                    End If
                
                    'Turn the extend selection mode 'off'
                    mlExtendSelSecEventPerm = 0

                Case Is > 0
                    'The user is trying to select a block of rows OR only one row
                    'Determine what the user is trying to do here
                    Select Case True
                        Case (mlExtendSelSecEventPerm > 0 And mbShiftKeyHeldDown)
                            'The user is trying to select a block of rows
                            'Check to see if the user is selecting from the 'bottom up'
                            Select Case True
                                Case (Row < mlExtendSelSecEventPerm)
                                    'Yes, the user IS selecting from the 'bottom up'
                                    lFirstSelectedRow = Row
                                    lLastSelectedRow = mlExtendSelSecEventPerm
                                    
                                Case (Row = mlExtendSelSecEventPerm)
                                    'The user is selecting only one row
                                    lFirstSelectedRow = Row
                                    lLastSelectedRow = Row
                                    
                                Case (Row > mlExtendSelSecEventPerm)
                                    'No, the user is selecting from the 'top down'
                                    lFirstSelectedRow = mlExtendSelSecEventPerm
                                    lLastSelectedRow = Row
                            End Select
                        
                            'Turn the extend selection mode 'off'
                            mlExtendSelSecEventPerm = 0
                            
                        Case (mlExtendSelSecEventPerm > 0 And (Not mbShiftKeyHeldDown))
                            'The user is selecting only one row at this time
                            lFirstSelectedRow = Row
                            lLastSelectedRow = Row
                            
                            'Turn the extend selection mode 'on', just in case
                            mlExtendSelSecEventPerm = Row
                        
                        Case mlExtendSelSecEventPerm <= 0
                            'The user is selecting only one row at this time
                            lFirstSelectedRow = Row
                            lLastSelectedRow = Row
                            
                            'Turn the extend selection mode 'on', just in case
                            mlExtendSelSecEventPerm = Row
                    End Select
                    
                    'The user wants to select or deselect row(s)
                    'You CANNOT unselect rows using the Shift key, however
                    For lRowIndex = lFirstSelectedRow To lLastSelectedRow
                        'Check to see if the row is already selected
                        'Also check to see if the user is working with ONE row
                        If (glGridGetCellColor(grdSecEventPerm, lRowIndex, kColSecEventPermDescription) = vbHighlight _
                        And lFirstSelectedRow = lLastSelectedRow) Then
                            'The row IS already selected
                            'Set the row color to indicate 'NOT selected'
                            ToggleRowColor grdSecEventPerm, kUNSelectRow, lRowIndex
                            
                            'Subtract one from the number of rows selected
                            mlSecEventPermRowsSelected = mlSecEventPermRowsSelected - 1
                            
                            'We have at least one row that is NOT selected
                            mbAllSecEventPermRowsSelected = False
                        Else
                            'The row is NOT selected
                            'Set the row color to indicate 'selected'
                            ToggleRowColor grdSecEventPerm, kSelectRow, lRowIndex
                            
                            'Add one to the number of rows selected
                            mlSecEventPermRowsSelected = mlSecEventPermRowsSelected + 1
                        End If
                    Next lRowIndex
                    
                    'Set the active row to the clicked-on row
                    gGridSetActiveCell grdSecEventPerm, Row, kColSecEventPermAuthorizedText
            End Select

            'Check to see if any rows are selected in the Sec Event Permissions grid
            If mlSecEventPermRowsSelected = 0 Then
                'Disable the appropriate controls
                With lstDfltPermSecEventPerm
                    .Enabled = False
                    .BackColor = vbButtonFace
                    .Refresh
                End With
                With cmdApply(1)
                    .Enabled = False
                    .Refresh
                End With
            
                'We know not all rows are selected
                mbAllSecEventPermRowsSelected = False
            Else
                'Enable the appropriate controls
                With lstDfltPermSecEventPerm
                    .Enabled = True
                    .BackColor = vbWindowBackground
                    .Refresh
                End With
                With cmdApply(1)
                    .Enabled = True
                    .Refresh
                End With
            End If
        
            'See if all rows are selected
            If mlSecEventPermRowsSelected = glGridGetMaxRows(grdSecEventPerm) Then
                mbAllSecEventPermRowsSelected = True
            End If
        
        Case Is = kColSecEventPermDescription
            'Determine which row has been clicked
            Select Case Row
                Case Is <= 0
                    'The user has clicked the Description button at the top
                    'Sort the grid on this column
                    'Decide if we should sort ascending or descending
                    Select Case UCase$(Trim$(msSecEventSortType))
                        Case Is = ksSortTypeAscending
                            'The current sort is ascending, so change it to descending
                            gGridSortByRow grdSecEventPerm, kColSecEventPermDescription, SS_SORT_ORDER_DESCENDING, kColSecEventPermAuthorizedText, SS_SORT_ORDER_DESCENDING
                            msSecEventSortType = ksSortTypeDescending
                        
                        Case Is = ksSortTypeDescending
                            'The current sort is descending, so change it to ascending
                            gGridSortByRow grdSecEventPerm, kColSecEventPermDescription, SS_SORT_ORDER_ASCENDING, kColSecEventPermAuthorizedText, SS_SORT_ORDER_ASCENDING
                            msSecEventSortType = ksSortTypeAscending
                        
                        Case Else
                            'Error (should not happen), but make it ascending anyway
                            gGridSortByRow grdSecEventPerm, kColSecEventPermDescription, SS_SORT_ORDER_ASCENDING, kColSecEventPermAuthorizedText, SS_SORT_ORDER_ASCENDING
                            msSecEventSortType = ksSortTypeAscending
                    End Select
                    
                    'Make row 1 the top line now (reset)
                    gGridSetTopRow grdSecEventPerm, 1
                
                Case Is > 0
                    'Set the active cell to the Sec Event Authorized column
                    gGridSetActiveCell grdSecEventPerm, Row, kColSecEventPermAuthorizedText
            End Select
        
            'Turn the extend selection mode 'off'
            mlExtendSelSecEventPerm = 0
        
        Case Is = kColSecEventPermAuthorizedText
            'Determine which row has been clicked
            Select Case Row
                Case Is <= 0
                    'The user has clicked the Sec Event Authorized button at the top
                    'Sort the grid on this column
                    'Decide if we should sort ascending or descending
                    Select Case UCase$(Trim$(msSecEventSortType))
                        Case Is = ksSortTypeAscending
                            'The current sort is ascending, so change it to descending
                            gGridSortByRow grdSecEventPerm, kColSecEventPermAuthorizedText, SS_SORT_ORDER_DESCENDING, kColSecEventPermDescription, SS_SORT_ORDER_DESCENDING
                            msSecEventSortType = ksSortTypeDescending
                        Case Is = ksSortTypeDescending
                            'The current sort is descending, so change it to ascending
                            gGridSortByRow grdSecEventPerm, kColSecEventPermAuthorizedText, SS_SORT_ORDER_ASCENDING, kColSecEventPermDescription, SS_SORT_ORDER_ASCENDING
                            msSecEventSortType = ksSortTypeAscending
                        Case Else
                            'Error (should not happen), but make it ascending anyway
                            gGridSortByRow grdSecEventPerm, kColSecEventPermAuthorizedText, SS_SORT_ORDER_ASCENDING, kColSecEventPermDescription, SS_SORT_ORDER_ASCENDING
                            msSecEventSortType = ksSortTypeAscending
                    End Select
                    
                    'Make row 1 the top line now (reset)
                    gGridSetTopRow grdSecEventPerm, 1
                
                Case Is > 0
                    'Set the active cell to the Sec Event Authorized column
                    gGridSetActiveCell grdSecEventPerm, Row, kColSecEventPermAuthorizedText
            End Select
        
            'Turn the extend selection mode 'off'
            mlExtendSelSecEventPerm = 0
        
        Case Is = kColSecEventPermSecEventID, kColSecEventPermAuthorizedValue, kColSecEventPermOldAuthValue, kColSecEventPermInsOrUpdRow, kColSecEventPermUpdateCounter, kColSecEventPermBlankColumn
            'Set the active cell to the Sec Event Authorized column
             gGridSetActiveCell grdSecEventPerm, Row, kColSecEventPermAuthorizedText
        
            'Turn the extend selection mode 'off'
            mlExtendSelSecEventPerm = 0
        
        Case Is > kColSecEventPermBlankColumn
            'Set the active cell to the Sec Event Authorized column
            gGridSetActiveCell grdSecEventPerm, Row, kColSecEventPermAuthorizedText
            
            'Turn the extend selection mode 'off'
            mlExtendSelSecEventPerm = 0
    End Select
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSecEventPerm_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSecEventPerm_ColWidthChange(ByVal Col1 As Long, ByVal Col2 As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim iColIndex As Long

    'See if the user is resizing a column to the right of the last column
    If Col1 > kColSecEventPermAuthorizedText Then
        'We will not allow the user to resize this column or other to the right of this one
        For iColIndex = Col1 To Col2
            'Set the column width to zero
            gGridHideColumn grdSecEventPerm, iColIndex
        Next iColIndex
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSecEventPerm_ColWidthChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSecEventPerm_DblClick(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'Perform the same function as the single click event
    grdSecEventPerm_Click Col, Row
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSecEventPerm_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSecEventPerm_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Determine if the "shift" key was pressed
    If Shift = vbShiftMask Then
        'Yes -> "shift" key WAS pressed
        mbShiftKeyHeldDown = True
    Else
        'No -> "shift" key was NOT pressed
        mbShiftKeyHeldDown = False
    End If
    
    'Check for the "Tab" key
    If KeyCode = kiTabKeyCode Then
        mbSecEventGridTabKey = True
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSecEventPerm_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSecEventPerm_KeyUp(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'See if the "shift" key WAS being held down BUT is not longer
    If Shift <> vbShiftMask Then
        'Shift key is not held down
        mbShiftKeyHeldDown = False
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSecEventPerm_KeyUp", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSecEventPerm_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Only execute if the user is attempting to "Tab" to the description field.  NOTE:  Clicking on the description
    '   field is handled inside the grd_Click() event
    If mbSecEventGridTabKey Then
        'Prevent the description column from receiving focus
        If NewCol = kColSecEventPermDescription Then
            If Row = 1 And NewRow = 1 Then
                Cancel = True
            Else
                'Normal Tab or Shift+Tab -> Force focus to next or previous Sec Event Perm Text cell
'                gProcessSendKeys "{Tab}"
            End If
        End If
        'Reset the Security Event Grid tab key flag
        mbSecEventGridTabKey = False
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSecEventPerm_LeaveCell", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdTaskPerm_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim sNewTaskPermPermissionText  As String
    Dim sNewTaskPermPermissionValue As String
    Dim iNewTaskPermListIndex       As Integer
    
    'If the Task Perm Text has changed on a "real" row
    If Col = kColTaskPermPermissionText And Row > 0 Then
        'Get the cell's new text
        sNewTaskPermPermissionText = gsGridReadCellText(grdTaskPerm, Row, kColTaskPermPermissionText)
        'Determine the Task Perm combo box "ListIndex" from the cell's new text
        iNewTaskPermListIndex = giListIndexFromText(lstDfltPermTaskPerm, sNewTaskPermPermissionText)
        'From the combo's "ListIndex" determine the new Task Perm Value
        sNewTaskPermPermissionValue = Str(lstDfltPermTaskPerm.ItemData(iNewTaskPermListIndex))
        'Update the current row's Task Permission Value
        gGridUpdateCell grdTaskPerm, Row, kColTaskPermPermissionValue, sNewTaskPermPermissionValue
        'Set grid property to indicate change
        grdTaskPerm.ChangeMade = True
        'If the row is currently selected
        If glGridGetCellColor(grdTaskPerm, Row, kColTaskPermTaskDesc) = vbHighlight Then
            'Set row's back and fore color to normal -> "unselected"
            ToggleRowColor grdTaskPerm, kUNSelectRow, Row
            'Reduce total number of row's selected rows by 1
            mlTaskPermRowsSelected = mlTaskPermRowsSelected - 1
            'Set flag to indicate all row's have not been selected
            mbAllTaskPermRowsSelected = False
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdTaskPerm_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdTaskPerm_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lRowIndex           As Long
    Dim lFirstSelectedRow   As Long
    Dim lLastSelectedRow    As Long
    Dim lNbrRows            As Long

    'Clear all block selections
    gGridClearSelectBlock grdTaskPerm
                
    'If the user has clicked on any column other than the numbers column
    'and the row is not selected, then unselect all currently selected rows
    'If rows are currently selected and
    If (Col > 0 And mlTaskPermRowsSelected > 0) Then
        'We need to check now to see if the row is currently UNselected
        If glGridGetCellColor(grdTaskPerm, Row, kColTaskPermTaskDesc) = vbWindowBackground Then
            'Yes, this row IS currently UNselected (NOT selected)
            'Therefore, we must unselect ALL currently selected rows
            'First, get the number of rows in the grid
            lNbrRows = glGridGetMaxRows(grdTaskPerm)
            
            'Now loop through the rows looking for selected rows
            For lRowIndex = 1 To lNbrRows
                'Is this row selected?
                If glGridGetCellColor(grdTaskPerm, lRowIndex, kColTaskPermTaskDesc) = vbHighlight Then
                    'Yes, this row IS selected
                    'So, UNselect this row
                    ToggleRowColor grdTaskPerm, kUNSelectRow, lRowIndex
                
                    'Subtract one from the number of rows selected
                    mlTaskPermRowsSelected = mlTaskPermRowsSelected - 1
                
                    'We have at least one row that is NOT selected
                    mbAllTaskPermRowsSelected = False
                    
                    'See if we can stop checking for selected rows now
                    If mlTaskPermRowsSelected = 0 Then
                        'We have successfully unselected all selected rows
                        'Exit this for/next loop
                        Exit For
                    End If
                End If
            Next lRowIndex
        End If
    End If
    
    'Determine which column has been clicked
    Select Case Col
        Case Is <= 0
            'The user has clicked on the left-side 'row number' column
            'Determine which row has been clicked
            Select Case Row
                Case Is <= 0
                    'The user wants to select or deselect all rows
                    If mbAllTaskPermRowsSelected = True Then
                        'All rows ARE already selected
                        'Set the row color to indicate 'NOT selected' for all rows
                        ToggleRowColor grdTaskPerm, kUNSelectRow, Row
                        
                        'Set the number of rows selected to zero
                        mlTaskPermRowsSelected = 0
                    Else
                        'All rows are NOT selected
                        'Set the row color to indicate 'selected' for all rows
                        ToggleRowColor grdTaskPerm, kSelectRow, Row
                    
                        'Set the number of rows selected to max rows
                        mlTaskPermRowsSelected = glGridGetMaxRows(grdTaskPerm)
                    End If
                    
                    'Turn the extend selection mode 'off'
                    mlExtendSelTaskPerm = 0
                    
                Case Is > 0
                    'The user is trying to select a block or rows OR only one row
                    'Determine what the user is trying to do here
                    Select Case True
                        Case (mlExtendSelTaskPerm > 0 And mbShiftKeyHeldDown)
                            'The user is trying to select a block of rows
                            'Check to see if the user is selecting from the 'bottom up'
                            Select Case True
                                Case (Row < mlExtendSelTaskPerm)
                                    'Yes, the user IS selecting from the 'bottom up'
                                    lFirstSelectedRow = Row
                                    lLastSelectedRow = mlExtendSelTaskPerm
                                
                                Case (Row = mlExtendSelTaskPerm)
                                    'The user is selecting only one row
                                    lFirstSelectedRow = Row
                                    lLastSelectedRow = Row
                                
                                Case (Row > mlExtendSelTaskPerm)
                                    'No, the user is selecting from the 'top down'
                                    lFirstSelectedRow = mlExtendSelTaskPerm
                                    lLastSelectedRow = Row
                            End Select
                        
                            'Turn the extend selection mode 'off'
                            mlExtendSelTaskPerm = 0
                            
                        Case (mlExtendSelTaskPerm > 0 And (Not mbShiftKeyHeldDown))
                            'The user is selecting only one row at this time
                            lFirstSelectedRow = Row
                            lLastSelectedRow = Row
                            
                            'Turn the extend selection mode 'on', just in case
                            mlExtendSelTaskPerm = Row
                            
                        Case mlExtendSelTaskPerm <= 0
                            'The user is selecting only one row at this time
                            lFirstSelectedRow = Row
                            lLastSelectedRow = Row
                            
                            'Turn the extend selection mode 'on', just in case
                            mlExtendSelTaskPerm = Row
                    End Select
                    
                    'The user wants to select or unselect row(s) now
                    'You CANNOT unselect rows using the Shift key, however
                    For lRowIndex = lFirstSelectedRow To lLastSelectedRow
                        'Check to see if the row is already selected
                        'Also check to see if the user is working with ONE row
                        If (glGridGetCellColor(grdTaskPerm, lRowIndex, kColTaskPermTaskDesc) = vbHighlight _
                        And lFirstSelectedRow = lLastSelectedRow) Then
                            'The row IS already selected
                            'Set the row color to indicate 'NOT selected'
                            ToggleRowColor grdTaskPerm, kUNSelectRow, lRowIndex
                        
                            'Subtract one from the number of rows selected
                            mlTaskPermRowsSelected = mlTaskPermRowsSelected - 1
                        
                            'We have at least one row that is NOT selected
                            mbAllTaskPermRowsSelected = False
                        Else
                            'The row is NOT selected
                            'Set the row color to indicate 'selected'
                            ToggleRowColor grdTaskPerm, kSelectRow, lRowIndex
                            
                            'Add one to the number of rows selected
                            mlTaskPermRowsSelected = mlTaskPermRowsSelected + 1
                        End If
                    Next lRowIndex

                    'Set the active row to the clicked-on row
                    gGridSetActiveCell grdTaskPerm, Row, kColTaskPermPermissionText
            End Select
            
            'Check to see if any rows are selected in the Task Permissions grid
            If mlTaskPermRowsSelected = 0 Then
                'Disable the appropriate controls
                With lstDfltPermTaskPerm
                    .Enabled = False
                    .BackColor = vbButtonFace
                    .Refresh
                End With
                With cmdApply(0)
                    .Enabled = False
                    .Refresh
                End With
                            
                'We know not all rows are selected
                mbAllTaskPermRowsSelected = False
            Else
                'Enable the appropriate controls
                With lstDfltPermTaskPerm
                    .Enabled = True
                    .BackColor = vbWindowBackground
                    .Refresh
                End With
                With cmdApply(0)
                    .Enabled = True
                    .Refresh
                End With
            End If
        
            'See if all rows are selected
            If mlTaskPermRowsSelected = glGridGetMaxRows(grdTaskPerm) Then
                mbAllTaskPermRowsSelected = True
            End If
        
        Case Is = kColTaskPermTaskDesc
            'Determine which row has been clicked
            Select Case Row
                Case Is <= 0
                    'Determine current sort order and sort the grid on this column
                    Select Case UCase(Trim(msTaskSortType))
                        Case Is = ksSortTypeAscending
                            'The current sort is ascending, so change it to descending
                            gGridSortByRow grdTaskPerm, kColTaskPermTaskDesc, SS_SORT_ORDER_DESCENDING, kColTaskPermPermissionText, SS_SORT_ORDER_DESCENDING
                            msTaskSortType = ksSortTypeDescending
                        
                        Case Is = ksSortTypeDescending
                            'The current sort is descending, so change it to ascending
                            gGridSortByRow grdTaskPerm, kColTaskPermTaskDesc, SS_SORT_ORDER_ASCENDING, kColTaskPermPermissionText, SS_SORT_ORDER_ASCENDING
                            msTaskSortType = ksSortTypeAscending
                        
                        Case Else
                            'Error (should not happen), but make it ascending anyway
                            gGridSortByRow grdTaskPerm, kColTaskPermTaskDesc, SS_SORT_ORDER_ASCENDING, kColTaskPermPermissionText, SS_SORT_ORDER_ASCENDING
                            msTaskSortType = ksSortTypeAscending
                    End Select
                    
                    'Make row 1 the top line now (reset)
                    gGridSetTopRow grdTaskPerm, 1
                
                Case Is > 0
                    'Set the active cell to the Task Permission column
                    gGridSetActiveCell grdTaskPerm, Row, kColTaskPermPermissionText
            End Select
        
            'Turn the extend selection mode 'off'
            mlExtendSelTaskPerm = 0
            
        Case Is = kColTaskPermPermissionText
            Select Case Row
                Case Is <= 0
                    'Determine the current sort order and sort the grid on this column
                    Select Case UCase(Trim(msTaskSortType))
                        Case Is = ksSortTypeAscending
                            'The current sort is ascending, so change it to descending
                            gGridSortByRow grdTaskPerm, kColTaskPermPermissionText, SS_SORT_ORDER_DESCENDING, kColTaskPermTaskDesc, SS_SORT_ORDER_DESCENDING
                            msTaskSortType = ksSortTypeDescending
                        
                        Case Is = ksSortTypeDescending
                            'The current sort is descending, so change it to ascending
                            gGridSortByRow grdTaskPerm, kColTaskPermPermissionText, SS_SORT_ORDER_ASCENDING, kColTaskPermTaskDesc, SS_SORT_ORDER_ASCENDING
                            msTaskSortType = ksSortTypeAscending
                        
                        Case Else
                            'Error (should not happen), but make it ascending anyway
                            gGridSortByRow grdTaskPerm, kColTaskPermPermissionText, SS_SORT_ORDER_ASCENDING, kColTaskPermTaskDesc, SS_SORT_ORDER_ASCENDING
                            msTaskSortType = ksSortTypeAscending
                    End Select
                    
                    'Make row 1 the top line now (reset)
                    gGridSetTopRow grdTaskPerm, 1
                
                Case Is > 0
                    'Set the active cell to the Task Permission column
                    gGridSetActiveCell grdTaskPerm, Row, kColTaskPermPermissionText
            End Select
            
            'Turn the extend selection mode 'off'
            mlExtendSelTaskPerm = 0
       
        Case Is = kColTaskPermTaskID, kColTaskPermPermissionValue, kColTaskPermOldPermValue, kColTaskPermInsOrUpdRow, kColTaskPermUpdateCounter, kColTaskPermBlankColumn
            'Set the active cell to the Task Permission column
            gGridSetActiveCell grdTaskPerm, Row, kColTaskPermPermissionText

            'Turn the extend selection mode 'off'
            mlExtendSelTaskPerm = 0

        Case Is > kColTaskPermBlankColumn
            'Set the active cell to the Task Permission column
            gGridSetActiveCell grdTaskPerm, Row, kColTaskPermPermissionText
            
            'Turn the extend selection mode 'off'
            mlExtendSelTaskPerm = 0
    End Select
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdTaskPerm_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdTaskPerm_ColWidthChange(ByVal Col1 As Long, ByVal Col2 As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim iColIndex As Long

    'See if the user is resizing a column to the right of the last column
    If Col1 > kColTaskPermPermissionText Then
        'We will not allow the user to resize this column
        For iColIndex = Col1 To Col2
            'Set the column width to zero
            gGridHideColumn grdTaskPerm, iColIndex
        Next iColIndex
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdTaskPerm_ColWidthChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdTaskPerm_DblClick(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'Perform the same function as the single click event
    grdTaskPerm_Click Col, Row
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdTaskPerm_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdTaskPerm_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'Determine if the "shift" key was pressed
    If Shift = vbShiftMask Then
        'Yes -> "shift" key WAS pressed
        mbShiftKeyHeldDown = True
    Else
        'No -> "shift" key was NOT pressed
        mbShiftKeyHeldDown = False
    End If
    
    If KeyCode = kiTabKeyCode Then
        mbTaskGridTabKey = True
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdTaskPerm_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdTaskPerm_KeyUp(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'See if "shift" key WAS being held down BUT is no longer
    If Shift <> vbShiftMask Then
        'Shift key is not held down
        mbShiftKeyHeldDown = False
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdTaskPerm_KeyUp", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdTaskPerm_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'Only execute if the user is attempting to "Tab" to the description field.  NOTE:  Clicking on the description
    '   field is handled inside the grd_Click() event
    If mbTaskGridTabKey Then
        'Prevent the description column from receiving focus
        If NewCol = kColTaskPermTaskDesc Then
            If Row = 1 And NewRow = 1 Then
                Cancel = True
            Else
                'Normal Tab or Shift+Tab -> Force focus to next or previous Task Perm Text cell
'                gProcessSendKeys "{Tab}"
            End If
        End If
        'Reset the Task Grid tab key flag
        mbTaskGridTabKey = False
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdTaskPerm_LeaveCell", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub lblDfltPermSecEventPerm_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Turn the extend selection mode indicators 'off'
    mlExtendSelTaskPerm = 0
    mlExtendSelSecEventPerm = 0
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lblDfltPermSecEventPerm_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lblDfltPermTaskPerm_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Turn the extend selection mode indicators 'off'
    mlExtendSelTaskPerm = 0
    mlExtendSelSecEventPerm = 0
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lblDfltPermTaskPerm_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lblModuleID_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Turn the extend selection mode indicators 'off'
    mlExtendSelTaskPerm = 0
    mlExtendSelSecEventPerm = 0
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lblModuleID_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lblUserGroupID_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Turn the extend selection mode indicators 'off'
    mlExtendSelTaskPerm = 0
    mlExtendSelSecEventPerm = 0
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lblUserGroupID_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lblUserGroupName_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Turn the extend selection mode indicators 'off'
    mlExtendSelTaskPerm = 0
    mlExtendSelSecEventPerm = 0
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lblUserGroupName_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lstModuleID_Click()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick lstModuleID, True
    #End If
'+++ End Customizer Code Push +++
    
    On Error GoTo ExpectedErrorRoutine:

    Dim sListText       As String
    Dim iReturnValue    As Integer
    Dim iOldMP          As Integer
    Dim iPermFlag       As Integer
    Dim iListIndex      As Integer
    Dim lReturnCode     As Long
    
    'Cache lstModuleID control properties
    With lstModuleID
        iListIndex = .ListIndex
        sListText = .Text
    End With
    
    'Continue only if the list selection has changed
    If Trim(Str(iListIndex)) = Trim(lstModuleID.Tag) Then Exit Sub
    
    'Re-initialize each time the ModuleID changes
    mbSecEventNotFirstTime = False
    mbTaskNotFirstTime = False
        
    'Save current and update mousepointer
    iOldMP = MousePointer
    MousePointer = vbHourglass
        
    'Update module name control
    With txtModuleName
        'Determine text from current listindex
        If iListIndex = kItemNotSelected Then
            .Caption = Empty
        Else
            .Caption = sGetModuleName(lstModuleID, sListText)
        End If
    End With

    'Continue only if current form state is not "start"
    If (Me.iState = kStateAdd Or Me.iState = kStateAddAOF Or Me.iState = kStateEdit Or Me.iState = kStateDisplayOnly) Then
        'Check if the grid data has changed
        If bDataHasChanged(Me.iState, False) Then
            'Validate the header data
            If bValidEntry() Then
                'Prompt user to save changes
                MousePointer = vbDefault
                iReturnValue = giSotaMsgBox(Me, moClass.moSysSession, kmsgSaveChanges)
                MousePointer = vbHourglass
                'Evaluate user's response to determine action
                Select Case iReturnValue
                    Case kretYes
                        'Save data -> check current state to determine action
                        Select Case Me.iState
                            Case kStateAdd, kStateAddAOF
                                'Add Mode -> save both header and grid data
                                lReturnCode = lFinishRecord(Me.iState, True)
                                If lReturnCode = kSaveSuccess And Me.iState = kStateAdd Then
                                    ManageAppState kStateEdit
                                    RefreshFromNewRecord iListIndex
                                Else
                                    ManageAppState iGetNextFinishState(Me.iState, CInt(lReturnCode))
                                    InitializeToState Me.iState
                                End If
                            Case kStateEdit, kStateDisplayOnly
                                'Edit Mode -> save only grid data
                                lReturnCode = lFinishRecord(Me.iState, False)
                                If lReturnCode = kSaveSuccess Then
                                    ManageAppState kStateEdit
                                    RefreshFromNewRecord iListIndex
                                Else
                                    ManageAppState iGetNextFinishState(Me.iState, CInt(lReturnCode))
                                    InitializeToState Me.iState
                                End If
                        End Select
                    Case kretNo
                        'Do not save data -> do nothing
                    Case kretCancel
                        'Cancel save -> restore previous module ID, restore mouse pointer and bail out
                        With lstModuleID
                            .ListIndex = CLng(.Tag)
                        End With
                        MousePointer = vbDefault
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Sub
                End Select
            Else
                'Invalid header data -> restore previous module ID, restore mouse pointer and bail out
                With lstModuleID
                    .ListIndex = CInt(.Tag)
                End With
                MousePointer = vbDefault
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
        End If
    
        'Perform either a)Init or b)Load of permission grids
        If iListIndex = kItemNotSelected Then
            gGridClearAll grdTaskPerm
            mbTaskNotFirstTime = False
            DisableTaskPermControls
            gGridClearAll grdSecEventPerm
            mbSecEventNotFirstTime = False
            DisableSecEventPermControls
        Else
            'Determine if type of permissions requested
            If iListIndex = 0 Then
                iPermFlag = kAllPermissions
            Else
                iPermFlag = kModulePermissions
            End If
            'Action dependent upon which tab is currently active
            If tabSecurGroupMaint.Tab = kTaskPermTab Then
                If lDisplayTaskPerms(iPermFlag, txtUserGroupID, sListText) <> kiDisplayPermsFailure Then
                    mbTaskNotFirstTime = True
                End If
            Else
                If lDisplaySecEventPerms(iPermFlag, txtUserGroupID, sListText) <> kiDisplayPermsFailure Then
                    mbSecEventNotFirstTime = True
                End If
            End If
        End If
        
    End If
        
    'Save the current selection
    With lstModuleID
        .Tag = Trim(Str(.ListIndex))
    End With
    
    'Restore the mouse pointer
    If iOldMP <> vbHourglass Then
        MousePointer = iOldMP
    Else
        MousePointer = vbDefault
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
ExpectedErrorRoutine:
    MousePointer = vbDefault

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lstModuleID_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Function bLoadModuleIDLB() As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    
    On Error GoTo ExpectedErrorRoutine:
    
    Dim sSQL        As String
    Dim rsModule    As Object

    'Set default return value
    bLoadModuleIDLB = False

    'Clear current contents of combobox
    lstModuleID.Clear

    'Add the "(All)" item to the combobox
    gComboAddItem lstModuleID, gsBuildString(kAll, moClass.moAppDB, moClass.moSysSession), 0
    
    'Successful load, one item added to combobox
    bLoadModuleIDLB = True
    
    'Define SQL statement to load true ModuleID's
    sSQL = "#smGetModules;"
    Set rsModule = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    'Continue only if record set is valid
    If Not rsModule Is Nothing Then
        With rsModule
            'Continue only if recordset is populated
            If Not .IsEmpty Then
                Do While Not .IsEOF
                    'Add the ModuleID to the combobox
                    gComboAddItem lstModuleID, .Field("ModuleID"), .Field("ModuleNo")
                    'Move to the next row
                    .MoveNext
                Loop
            End If
            'Local object cleanup
            .Close
            Set rsModule = Nothing
        End With
    End If
    
LoadModuleIDLBBail:
    'Local object cleanup
    If Not rsModule Is Nothing Then
        rsModule.Close
        Set rsModule = Nothing
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:
    'Local object cleanup
    If Not rsModule Is Nothing Then
        rsModule.Close
        Set rsModule = Nothing
    End If

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bLoadModuleIDLB", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub lstModuleID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lstModuleID, True
    #End If
'+++ End Customizer Code Push +++
    
    With lstModuleID
        .Tag = UCase(Trim(Str(.ListIndex)))
    End With
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lstModuleID_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub navCopyFrom_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    If Len(Trim(navCopyFrom.Tag)) = 0 Then
        If Not gbLookupInit(navCopyFrom, moClass, moClass.moAppDB, kNavEntSMUserGroup) Then Exit Sub
        navCopyFrom.Tag = kNavEntSMUserGroup
    End If
    
    HandleCopyFromRequest
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navCopyFrom_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub HandleCopyFromRequest()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    
    On Error GoTo ExpectedErrorRoutine:

    Dim rsUserGroup         As Object
    Dim sUserGroup          As String
    Dim sSQL                As String
    Dim sReturnText         As String
    Dim iOldMousePointer    As Integer
    Dim iReturnValue        As Integer
    Dim lNbrTaskPerms       As Long
    Dim lNbrSecEventPerms   As Long
    Dim lRowIndex           As Long
    Dim vTmp                As Variant

    iOldMousePointer = MousePointer
    MousePointer = vbHourglass
    
    'Set the "copy from" hidden control contents to the blank string (i.e., populate navigator with all available groups)
    txtCopyFrom = ""
    'Process what the Navigator returned to us
    vTmp = gvLookupClick(Me, navCopyFrom, txtCopyFrom)
    
    'Validate navigator return value
    If IsNull(vTmp) Then
        txtCopyFrom = ""
        GoTo HandleCopyFromRequestBail:
    End If
    
    sUserGroup = CStr(vTmp(1))
    
    'Check to see if we have a group ID back from the navigator
    If Len(sUserGroup) > 0 Then
        'Check to see if the user group is valid
        If Not bIsValidString(sUserGroup) Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgSecurityGroupInvalid
            txtCopyFrom = ""
            GoTo HandleCopyFromRequestBail:
        End If
        
        sSQL = "#smGetSecurityGroup;" & kSQuote & sUserGroup & kSQuote & ";"
        Set rsUserGroup = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
        With rsUserGroup
            'Did the security group already exist?
            If Not .IsEOF Then
                'Load the hidden control with the Group ID to be copied
                txtCopyFrom = sUserGroup
                
                'Validate security group description
                If Trim(txtDescription) = "" Then
                    txtDescription = Trim(.Field("UserGroupName"))
                End If
            
                'Determine which Module ID list box case we're dealing with
                Select Case lstModuleID.ListIndex
                    Case Is = -1
                        'Do nothing, this is an error
                    Case Is = 0
                        'Display the Task Permissions for all modules
                        lNbrTaskPerms = lDisplayTaskPerms(kAllPermissions, txtCopyFrom, lstModuleID.Text)
                    
                        'Make sure we're on the Task Permissions tab
                        tabSecurGroupMaint_Click (1)

                        'Set the indicator that the Task Permissions grid has changed
                        grdTaskPerm.ChangeMade = True
                
                        'Make sure we're on the Security Event Permissions tab
                        tabSecurGroupMaint_Click (0)
                        
                        'Display the Security Event Permission grid tasks for all modules
                        lNbrSecEventPerms = lDisplaySecEventPerms(kAllPermissions, txtCopyFrom, lstModuleID.Text)
            
                        'Set the indicator that the Security Event Permissions grid has changed
                        grdSecEventPerm.ChangeMade = True
                    
                    Case Else
                        'Display the Task Permissions for the specified module
                        lNbrTaskPerms = lDisplayTaskPerms(kModulePermissions, txtCopyFrom, lstModuleID.Text)
                        
                        'Set the indicator that the Task Permissions grid has changed
                        grdTaskPerm.ChangeMade = True
                
                        'Display the Security Event Permission grid tasks for the specific module
                        lNbrSecEventPerms = lDisplaySecEventPerms(kModulePermissions, txtCopyFrom, lstModuleID.Text)
            
                        'Set the indicator that the Security Event Permissions grid has changed
                        grdSecEventPerm.ChangeMade = True
                End Select
            Else
                'This is an invalid Security Group ID
                txtCopyFrom = Empty
            End If
        End With
        
    Else
        'This is an invalid Security Group ID
        txtCopyFrom = Empty
    End If
    
HandleCopyFromRequestBail:
    'Local object cleanup
    If Not rsUserGroup Is Nothing Then
        rsUserGroup.Close
        Set rsUserGroup = Nothing
    End If
    
    cmdCopyFrom.Enabled = True
    
    'Restore old mousepointer
    If iOldMousePointer <> vbHourglass Then
        MousePointer = iOldMousePointer
    Else
        MousePointer = vbDefault
    End If
    
    DoEvents
    txtDescription.SetFocus
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
ExpectedErrorRoutine:
    MousePointer = vbDefault

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleCopyFromRequest", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub NavMain_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim lReturnCode As Long
    
    If Not bSetupNavMain() Then Exit Sub
    
    Select Case Me.iState
        Case kStateAdd, kStateEdit, kStateAddAOF
            If miSecurityLevel > sotaTB_DISPLAYONLY And bDataHasChanged(Me.iState, True) Then
                Select Case giSotaMsgBox(Me, moClass.moSysSession, kmsgSaveChanges)
                    Case kretYes
                        If bValidEntry() Then
                            lReturnCode = lFinishRecord(Me.iState, True)
                            ManageAppState iGetNextFinishState(Me.iState, lReturnCode)
                            InitializeToState Me.iState
                        Else
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                            Exit Sub
                        End If
                    Case kretNo
                        ManageAppState iGetNextCancelState(Me.iState)
                        InitializeToState Me.iState
                    Case kretCancel
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Sub
                End Select
            Else
                ManageAppState iGetNextCancelState(Me.iState)
                InitializeToState Me.iState
            End If
        Case Else
            'Do Nothing
    End Select
    
    gvLookupClick Me, navMain, txtUserGroupID
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "NavMain_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub sysSecurGroupMaint_SysColorsChanged()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'The system colors have changed, so change the two grids
    FormatGrid grdTaskPerm
    FormatGrid grdSecEventPerm
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sysSecurGroupMaint_SysColorsChanged", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub tabSecurGroupMaint_Click(PreviousTab As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim sCurrListText       As String
    Dim iCurrListIndex      As Integer
    Dim iPermFlag           As Integer
    
    'Toggle the "TabStop" property for each tabs controls
    ToggleTabControlsTabStop PreviousTab
    
    'Cache current lstModuleID properties for local use
    With lstModuleID
        iCurrListIndex = .ListIndex
        sCurrListText = .Text
    End With
    
    'Determine permission flag from current listindex
    If iCurrListIndex = kItemNotSelected Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    Else
        If iCurrListIndex = 0 Then
            iPermFlag = kAllPermissions
        Else
            iPermFlag = kModulePermissions
        End If
    End If
    
    'Continue only if the form is in the correct state
    If Me.iState <> kStateAdd And _
        Me.iState <> kStateAddAOF And _
        Me.iState <> kStateEdit And _
        Me.iState <> kStateDisplayOnly Then Exit Sub
    
    'Display permissions for new active tab
    If PreviousTab = kTaskPermTab Then
        If Not mbSecEventNotFirstTime Then
            If lDisplaySecEventPerms(iPermFlag, txtUserGroupID, sCurrListText) <> kiDisplayPermsFailure Then
                mbSecEventNotFirstTime = True
            End If
        End If
    Else
        If Not mbTaskNotFirstTime Then
            If lDisplayTaskPerms(iPermFlag, txtUserGroupID, sCurrListText) <> kiDisplayPermsFailure Then
                mbTaskNotFirstTime = True
            End If
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tabSecurGroupMaint_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub tabSecurGroupMaint_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Turn the extend selection mode indicators 'off'
    mlExtendSelTaskPerm = 0
    mlExtendSelSecEventPerm = 0
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tabSecurGroupMaint_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub sbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    HandleToolbarClick Button
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    HandleToolbarClick Button
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUserGroupID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtUserGroupID, True
    #End If
'+++ End Customizer Code Push +++

    txtUserGroupID.Tag = Trim(txtUserGroupID)
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtUserGroupID_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUserGroupID_LostFocus()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtUserGroupID, True
    #End If
'+++ End Customizer Code Push +++
    
    On Error GoTo ExpectedErrorRoutine:

    Dim iOldMP  As Integer
    Dim iTmp    As Integer
    
    'Check for validation already in progress
    If mbValidating Then Exit Sub
    
    'Evaluate control that caused lost focus to determine action
    Select Case Trim(UCase(Me.ActiveControl.Name))
        Case "NAVMAIN", "TBARBROWSE", "tbrMain"
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        Case Else
            If mbContextMenuInvoked Or mbValidating Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            Else
                'Set validation in-progress flag
                mbValidating = True
                'Save and update mousepointer
                iOldMP = MousePointer
                MousePointer = vbHourglass
                'Validate the name entered by the user against SQL Sever naming requirements
                If Trim(txtUserGroupID) <> "" Then
                    If bValidUserGroup(moClass.moAppDB, txtUserGroupID) Then
                        'Process the current user's entry
                        bProcessUserGroupEntry
                    Else
                        'Make sure the name is not already in use
                        If Not gbIsNameAlreadyUsed(moClass.moAppDB, txtUserGroupID) Then
                            'Process the current user's entry
                            bProcessUserGroupEntry
                        Else
                            giSotaMsgBox Me, moClass.moSysSession, kmsgNameAlreadyExists
                            MousePointer = vbDefault
                            txtUserGroupID.SetFocus
                        End If
                    End If
                Else
                    MousePointer = vbDefault
                    With txtUserGroupID
                        .Enabled = True
                        .SetFocus
                    End With
                End If
            End If
    End Select
    
    'Reset validation in-progress flag
    mbValidating = False
    
    'Restore the mousepointer
    If iOldMP <> vbHourglass Then
        MousePointer = iOldMP
    Else
        MousePointer = vbDefault
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
ExpectedErrorRoutine:
    MousePointer = vbDefault
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtUserGroupID_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub





Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Instantiate Context Menu Class
    Set moContextMenu = New clsContextMenu
    
    With moContextMenu
        'Add any special .Bind or .BindGrid commands here for Drill Around/ Drill Down
        'or for Grids - Here
        'Example
        ' **PRESTO ** Set .Hook = Me.WinHook1             'Assign Winhook control to Context Menu Class
        Set .Form = frmSecurGroupMaint
        .Init                               'Init will set properties of Winhook to intercept WM_RBUTTONDOWN
    End With

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "frmSecurGroupMaint"
    
    Exit Function
    
End Function
#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_Paint(Index As Integer)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If

Private Sub cmdCopyFrom_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdCopyFrom, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdCopyFrom_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdCopyFrom_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdCopyFrom, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdCopyFrom_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdApply_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdApply(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdApply_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdApply_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdApply(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdApply_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub txtCopyFrom_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCopyFrom, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCopyFrom_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCopyFrom_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCopyFrom, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCopyFrom_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCopyFrom_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCopyFrom, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCopyFrom_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCopyFrom_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCopyFrom, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCopyFrom_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUserGroupID_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtUserGroupID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUserGroupID_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUserGroupID_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtUserGroupID, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUserGroupID_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDescription_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtDescription, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDescription_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDescription_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtDescription, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDescription_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDescription_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtDescription, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDescription_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDescription_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtDescription, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDescription_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub lstModuleID_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lstModuleID, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lstModuleID_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lstModuleID_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lstModuleID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lstModuleID_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lstDfltPermSecEventPerm_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick lstDfltPermSecEventPerm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lstDfltPermSecEventPerm_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lstDfltPermSecEventPerm_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lstDfltPermSecEventPerm, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lstDfltPermSecEventPerm_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lstDfltPermSecEventPerm_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lstDfltPermSecEventPerm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lstDfltPermSecEventPerm_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lstDfltPermSecEventPerm_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lstDfltPermSecEventPerm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lstDfltPermSecEventPerm_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lstDfltPermTaskPerm_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick lstDfltPermTaskPerm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lstDfltPermTaskPerm_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lstDfltPermTaskPerm_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lstDfltPermTaskPerm, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lstDfltPermTaskPerm_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lstDfltPermTaskPerm_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lstDfltPermTaskPerm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lstDfltPermTaskPerm_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lstDfltPermTaskPerm_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lstDfltPermTaskPerm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lstDfltPermTaskPerm_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
               Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If

















Public Property Get MyApp() As Object
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property







