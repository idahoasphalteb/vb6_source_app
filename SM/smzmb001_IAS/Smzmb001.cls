VERSION 1.0 CLASS
BEGIN
  MultiUse = 0   'False
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsSecurGroupMNT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'   Name:
'       clsSecurityGroupMNT
'
'   Description:
'       The clsSecurityGroupMNT class handles the user interface for maintaining security groups that users belong to.
'
'   Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
'   Original: 08/14/95 CEL
'   Mods:
'
'                                   Public Variables for use by other modules
'                                   -----------------------------------------
'
'   moFramework     -   required to give other modules the ablitiy to call the framework methods and properties, such
'                       as UnloadSelf, LoadSotaObject, etc.  Object reference is set in InitializeObject.
'
'   mlContext       -   Contains information of how this class object was invoked.  It is set in InitializeObject
'
'   moAppDB         -   Application Database ojbect reference.
'
'   moAppDB         -   System Database object reference.
'
'   moSysSession    -   System Manager Session Object which contains information specific to the user's session with
'                       this accounting application.  For example, current companyID, Business Date, UserID,
'                       UserName,. . .
'
'   miShutDownRequester
'                   -   The ShutDownRequester is the object responsible for requesting this object to shut down.
'                       There are two possible choices:
'                           1) Framework requests the object to shut itself down (kFrameworkShutDown)
'                           2) User Interface or UnloadSelf call requests the framework to shut this object down
'                              (kUnloadSelfShutDown).
'                       The main purpose of this flag is to ensure that the framework reference isn't removed (or
'                       set to nothing) before the UI.
'////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Option Explicit
    
    Public moFramework          As Object
    Public moAppDB              As Object
    Public moSysSession         As Object
    Public moDasSession         As Object
    Public miShutDownRequester  As Integer
    Public mlContext            As Long
    Public mlError              As Long
    
    '=======================================================================
    'Private Variables for Class Properties
    '=======================================================================
    Private mlRunFlags As Long
    Private mlUIActive As Long

    '=======================================================================
    'Private variables for use within this class only
    '=======================================================================
    Private mfrmMain As Form


Const VBRIG_MODULE_ID_STRING = "SMZMB001.CLS"

Public Function DisplayUI(ByVal lContext As Long) As Long

On Error GoTo ExpectedErrorRoutine

    'Set default return value
    DisplayUI = kFailure

    'Validate the existence of the form
    If mfrmMain Is Nothing Then Exit Function

    'Display the form the the user, checking for errors
    mfrmMain.Show
    DoEvents
    
    If mlError Then
        Err.Raise mlError
    End If
    
    'Set return value to indicate form sucessfully displayed
    DisplayUI = kSuccess
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function
ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DisplayUI", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function GetUIHandle(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
'   Description:
'       GetUIHandle allows the Framework to get the Windows handle
'       for this program if it needs to.
'
'    Parameters:
'           lContext <in>   - Context of the GetUIHandle or how it is
'                             being invoked by the framework.
'
'   Copyright (c) 1995-2005 Sage Software, Inc.
'*********************************************************************
    
    If Not mfrmMain Is Nothing Then
        'Give the Windows handle of this form to the Framework
        GetUIHandle = mfrmMain.hwnd
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "GetUIHandle", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Private Function sGetCollectionKey(lTaskID As Long) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Set default return value
    sGetCollectionKey = Empty
    
    Select Case mfrmMain.lRunMode
        Case kContextNormal
            sGetCollectionKey = lTaskID & "Normal"
        Case kContextAOF
            sGetCollectionKey = lTaskID & "AOF"
        Case kContextDD
            sGetCollectionKey = lTaskID & "DD"
    End Select
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "sGetCollectionKey", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Property Get lRunFlags() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    
    lRunFlags = mlRunFlags
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Property

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "lRunFlags_Get", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Property

Public Property Let lRunFlags(ltRunFlags As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    
    mlRunFlags = ltRunFlags
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Property

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "lRunFlags_Let", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Property

Public Property Get lUIActive() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
   
    lUIActive = mlUIActive
   
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Property

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "lUIActive_Get", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Property

Public Property Let lUIActive(lNewActive As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    
    mlUIActive = lNewActive
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Property

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "lUIActive_Let", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Property

Public Function InitializeObject(ByVal oFramework As Object, ByVal lContext As Long) As Long

On Error GoTo ExpectedErrorRoutine
    
    'Set default return value
    InitializeObject = kFailure

    DefaultInitializeObject Me, oFramework, lContext, App.ProductName, App.Title
        
    'Set return value to indicate successful init of the class
    InitializeObject = kSuccess
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function
ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitializeObject", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function LoadUI(ByVal lContext As Long) As Long

On Error GoTo ExpectedErrorRoutine

    'Set default return value
    LoadUI = kFailure

    'Initialize our Maintain Security Groups form
    Set mfrmMain = frmSecurGroupMaint

    'Inject this form with a reference to its class
    Set mfrmMain.oClass = Me

    'Set the run mode for this form based on the context
    mfrmMain.lRunMode = mlContext And kRunModeMask

    'Load "Maintain Security Groups" form
    Load mfrmMain
    
    If mlError Then
        Err.Raise mlError
    End If
    
    'Set return value if form load successful
    If mfrmMain.bLoadSuccess Then
        LoadUI = kSuccess
    End If

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function
ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LoadUI", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function


Public Function ShowUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++

    ShowUI = kFailure
    
    If Not mfrmMain Is Nothing Then
        mfrmMain.Show
        DoEvents
    End If
    
    ShowUI = kSuccess
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "ShowUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function HideUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++

    'Set default return value
    HideUI = kFailure
    
    If Not mfrmMain Is Nothing Then
        If gbActiveChildObjects(mfrmMain.moSotaObjects) Then Exit Function
        mfrmMain.Hide
    End If
    
    HideUI = kSuccess
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "HideUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function RestoreUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++

    RestoreUI = kFailure

    If Not mfrmMain Is Nothing Then
        mfrmMain.WindowState = vbNormal
    End If
    
    RestoreUI = kSuccess
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "RestoreUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

'////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'                                           Class Object ShutDown Process
'                                           -----------------------------
'
'   Sage MAS 500 Interface 3:
'       If InitializeObject returns kFailure, then the TerminateObject method is called immediately and this object
'       is guaranteed to shutdown whether or not the TerminateObject returned kSuccess or kFailure.
'
'       If LoadUI or DisplayUI returned kFailure, the QueryShutDown method is called, followed by UnloadUI and
'       TerminateObject.  The framework will ignore failures from the QueryShutDown, UnloadUI, and Terminate object,
'       because the object is in a failure.  If the Framework requests shutdown or the UnloadSelf call requests
'       shutdown, then the QueryShutDown method is invoked.  If a successful QueryShutDown is performed, the UnloadUI
'       method is called. And if a successful UnloadUI is performed, TerminateObject method is called.  Once the
'       TerminateObject method is called, the object will be shut down whether or not the TerminateObject returned
'       success or failure.
'
'       SPECIAL NOTE:
'           The QueryShutDown is designed to check for active child objects and return failure to the framework if any
'           have been found.  If there are any Active Child Objects remaining after a successful QueryShutDown, the
'           framework will shut them down prior to the UnloadUI method call.
'
'           The UnloadUI is designed to unload its UI and within that it will unload all its child objects (Sage MAS 500
'           children).  If there are any Sage MAS 500 child objects remaining after a successful UnloadUI, the framework will
'           shut them down prior to the TerminateObject method call.  The TerminateObject is designed to unload/remove
'           all Non-UI object references.  If there are any child Non-UI (No Sage MAS 500 Interface or Sage MAS 500 Interface 1 (?))
'           remaining after the TerminateObject call, then the framework will remove or shut them down.
'////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Public Function QueryShutDown(lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim iCount As Integer

    'Set the default return value
    QueryShutDown = kFailure
    
    If Not mfrmMain Is Nothing Then
        'Step through the collection "moSotaObjects" and verify each element is in the proper state to be shutdown
        For iCount = mfrmMain.moSotaObjects.Count To 1 Step -1
            If mfrmMain.moSotaObjects(iCount).lUIActive = kChildObjectActive Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
        Next iCount
    End If

    'If we get here, everything is ok to continue with shutdown (all Sage MAS 500 objects in the proper state)
    QueryShutDown = kSuccess
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "QueryShutDown", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function MinimizeUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++

    MinimizeUI = kFailure
    
    If Not mfrmMain Is Nothing Then
        mfrmMain.WindowState = vbMinimized
    End If
    
    MinimizeUI = kSuccess
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "MinimizeUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function UnloadUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Set default return value
    UnloadUI = kFailure

    If Not mfrmMain Is Nothing Then
        If miShutDownRequester = kFrameworkShutDown Then
            Unload mfrmMain
            If mfrmMain.bCancelShutDown Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
            Set mfrmMain = Nothing
        Else
            If mlError Then
                mfrmMain.PerformCleanShutDown
            End If
        End If
    End If

    'Set return value
    UnloadUI = kSuccess
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "UnloadUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function TerminateObject(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    
    On Error Resume Next
    
    'Assign default return value
    TerminateObject = kFailure
    
    DefaultTerminateObject Me
    
    'Set return value to indicate succesful termination of the class
    TerminateObject = kSuccess
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "TerminateObject", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Sub Class_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    
    gSetClass Me
    miShutDownRequester = kFrameworkShutDown
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "Class_Initialize", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub

Private Sub Class_Terminate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    
    gSetClass Nothing
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "Class_Terminate", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub
Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "clsSecurGroupMNT"
End Function

