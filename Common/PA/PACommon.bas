Attribute VB_Name = "PACommon"
Option Explicit

'Security events
Public Const kVariation = "VARIATION"
Public Const kOpenProject1 = "OPENPROJ"

'Project Status
'a status of 0 = only the project has been saved
Public Const kSaved = 0
Public Const kOverview = 1
Public Const kQuoted = 2
Public Const kActive = 3
Public Const kClosed = 4

Public Const kPost = 1
Public Const kSave = 2
Public Const kCancel = 3

'id's for different batch posting
Public Const kModTimesheet = 1
Public Const kModExpenseClaim = 2
Public Const kModProjTrxn = 3

'==========================================================================================================
'PA - ' Control specific property mask - used to map values from customizer
'==========================================================================================================

Public Const kCtrlSpecPropAmount = &H1&
Public Const kCtrlSpecPropAmountMax = &H2&
Public Const kCtrlSpecPropAmountMin = &H4&
Public Const kCtrlSpecPropAutoSelect = &H8&
Public Const kCtrlSpecPropBorder = &H10&
Public Const kCtrlSpecPropCurrencyLocale = &H20&
Public Const kCtrlSpecPropDecimalPlaces = &H40&
Public Const kCtrlSpecPropDisplayIntCurrSymbol = &H80&
Public Const kCtrlSpecPropFillChar = &H100&
Public Const kCtrlSpecPropFillOnJump = &H200&
Public Const kCtrlSpecPropFormat = &H400&
Public Const kCtrlSpecPropImpliedDecimal = &H800&
Public Const kCtrlSpecPropIntegralPlaces = &H1000&
Public Const kCtrlSpecPropLocale = &H2000&
Public Const kCtrlSpecPropLocked = &H4000&
Public Const kCtrlSpecPropMask = &H8000&
Public Const kCtrlSpecPropMaxLength = &H10000
Public Const kCtrlSpecPropPasswordChar = &H20000
Public Const kCtrlSpecPropProtected = &H40000
Public Const kCtrlSpecPropShowCurrency = &H80000
Public Const kCtrlSpecPropValue = &H100000
Public Const kCtrlSpecPropValueMax = &H200000
Public Const kCtrlSpecPropValueMin = &H400000
Public Const kCtrlSpecPropWildCardChar = &H800000

Public Const kCtrlSpecPropAll = &HFFFFFF
Public Const kCtrlPropEmptyMask = &H0&


'=====================================================================================================================
'PA_IS Batch Types
'=====================================================================================================================
Public Const kBatchTypePAInv = 801      'Not used at this time  bds
Public Const kBatchTypePATS = 1903
Public Const kBatchTypePAEX = 1905
Public Const kBatchTypePAPT = 1906      'Project Transaction

'=====================================================================================================================
'PA_IS Entity Types
'=====================================================================================================================
Public Const kEntTypePAResource = 1980
Public Const kEntTypePADivision = 1981
Public Const kEntTypePAProject1 = 1982
Public Const kEntTypePAMstrProject = 1983
Public Const kEntTypePAEstimates = 1988
Public Const kEntTypePATimesheets = 1989
Public Const kEntTypePATMEstimates = 1990

Public Const kTranTypePATimesheet = 1900 '25000
Public Const kTranTypePAProject = 1901 '25001
Public Const kTranTypePAExpClaim = 1902 '25002
Public Const kTranTypePATransactionDebit = 1903 '25003
Public Const kTranTypePATransactionCredit = 1904 '25004

Public Function GetJobStatus(moAppDB As Object, sCompany As String, sJobNum As String, sKeyValue As String, sJobType As String) As Boolean
    Dim rs As Object
    On Error GoTo ExpectedErrorRoutine
  
    If sJobNum <> "" Then
        Set rs = moAppDB.OpenRecordset("SELECT intJobKey, ISNULL(siJobType, 0) FROM tPA00175 WHERE chrJobNumber = " & gsQuoted(sJobNum) & " AND CompanyID = '" & sCompany & "'", kSnapshot, kOptionNone)
        If Not rs.IsEOF Then
            sKeyValue = rs.field(0)
            sJobType = rs.field(1)
            GetJobStatus = True
        Else
            GetJobStatus = False
        End If
        rs.Close
        Set rs = Nothing
        Exit Function
    Else
        GetJobStatus = False
        Exit Function
    End If

ExpectedErrorRoutine:
    MsgBox "GetJobStatus - " & Err.Description, vbCritical, "GetJobStatus"
    GetJobStatus = False
End Function

Public Function GetPAOptionsUserDefs(moAppDB As Object, sCompany As String, sUser1 As String, sUser2 As String, sUser3 As String) As Boolean
    Dim rs As Object
    On Error GoTo ExpectedErrorRoutine
  
    Set rs = moAppDB.OpenRecordset("SELECT ISNULL(tPA00402.chrUser1Classif, ''), " & _
        "ISNULL(tPA00402.chrUser2Classif, ''), ISNULL(tPA00402.chrUser3Classif, '') FROM tPA00402 " & _
        "WHERE tPA00402.CompanyID = '" & sCompany & "'", kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        sUser1 = Trim(rs.field(0))
        sUser2 = Trim(rs.field(1))
        sUser3 = Trim(rs.field(2))
    End If
    rs.Close
    Set rs = Nothing
    GetPAOptionsUserDefs = True
    Exit Function

ExpectedErrorRoutine:
    MsgBox "GetPAOptionsUserDefs - " & Err.Description, vbCritical, "GetPAOptionsUserDefs"
    GetPAOptionsUserDefs = False
End Function

Public Function GetPhaseCostClass(moAppDB As Object, sCompany As String, sPhase As String) As Long
    Dim rs As Object
    On Error GoTo ExpectedErrorRoutine
  
    Set rs = moAppDB.OpenRecordset("SELECT tPA00002.siCstClsificatnDDL FROM tPA00002 " & _
        "WHERE tPA00002.chrPhaseNumber = " & gsQuoted(sPhase) & " AND tPA00002.CompanyID = '" & sCompany & "'", kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        GetPhaseCostClass = CLng(rs.field(0))
    Else
        GetPhaseCostClass = -1
    End If
    
    rs.Close
    Set rs = Nothing
    Exit Function

ExpectedErrorRoutine:
    MsgBox "GetPhaseCostClass - " & Err.Description, vbCritical, "GetPhaseCostClass"
    GetPhaseCostClass = -1
End Function

Public Function GetEmpDefaultProject(moAppDB As Object, sCompany As String, lEmpKey As Long, lKeyValue As Long, sProject As String) As Boolean
    Dim rs As Object
    On Error GoTo ExpectedErrorRoutine
  
    Set rs = moAppDB.OpenRecordset("SELECT intJobKey, chrJobNumber FROM tPA00175 WHERE " & _
        "tPA00175.intJobKey IN(SELECT tPA00007.intJobKey FROM tPA00007 WHERE tPA00007.intEmployeeKey = " & lEmpKey & ") AND " & _
        "tPA00175.intJobKey NOT IN(SELECT tPA00175.intJobKey FROM tPA00175 WHERE tPA00175.siJobType = 2 AND tPA00175.siJobStatus IS NULL) AND " & _
        "(tPA00175.siJobStatus <> 4 OR tPA00175.siJobStatus IS NULL) AND tPA00175.CompanyID = '" & sCompany & "'", kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        lKeyValue = rs.field(0)
        sProject = rs.field(1)
    Else
        rs.Close
        Set rs = Nothing
        GetEmpDefaultProject = False
        Exit Function
    End If
    
    rs.Close
    Set rs = Nothing
    GetEmpDefaultProject = True
    Exit Function

ExpectedErrorRoutine:
    MsgBox "GetEmpDefaultProject - " & Err.Description, vbCritical, "GetEmpDefaultProject"
    GetEmpDefaultProject = False
End Function

Public Function CalculateRates(moAppDB As Object, sCompany As String, lJobKey As Long, lEmpKey As Long, lPhaseKey As Long, lTaskKey As Long, sDate As String, sCharge As String, sCost As String) As Boolean
    Dim rv As Long
    On Error GoTo ExpectedErrorRoutine
  
    With moAppDB
        .SetInParamNull SQL_INTEGER
        .SetInParam sCompany
    
        If lJobKey = 0 Then
            .SetInParamNull SQL_INTEGER
        Else
            .SetInParamLong lJobKey
        End If
    
        .SetInParamLong lEmpKey
        .SetInParamNull SQL_INTEGER
    
        If lPhaseKey = 0 Then
            .SetInParamNull SQL_INTEGER
        Else
            .SetInParamLong lPhaseKey
        End If
        
        If lTaskKey = 0 Then
            .SetInParamNull SQL_INTEGER
        Else
            .SetInParamLong lTaskKey
        End If
        
        If sDate = "" Then
            .SetInParamNull SQL_DATE
        Else
            .SetInParam sDate
        End If
        
        .SetOutParam sCharge
        .SetOutParam sCost
        .SetOutParam rv
        .ExecuteSP "spPAGetChargeOutRate"
        sCharge = .GetOutParam(9)
        sCost = .GetOutParam(10)
        rv = .GetOutParam(11)
        .ReleaseParams
    End With
    
    If rv <> 1 Then
        MsgBox "spPAGetChargeOutRate failed:  " & Str(rv), vbExclamation, "spPAGetChargeOutRate"
    End If
    
    CalculateRates = True
    Exit Function

ExpectedErrorRoutine:
    MsgBox "spPAGetChargeOutRate - " & Err.Description, vbCritical, "spPAGetChargeOutRate"
    CalculateRates = False
End Function

Public Function GetJobLineNumber(moAppDB As Object, sCompany As String, iType As Integer, lJobKey As Long, lPhaseKey As Long, lTaskKey As Long, lEmpKey As Long, lVendKey As Long, lItemKey As Long, iMultiFound As Integer, sLine As String) As Boolean
    Dim rv As Long
    On Error GoTo ExpectedErrorRoutine
  
    With moAppDB
        .SetInParamNull SQL_INTEGER
        .SetInParam sCompany
        .SetInParamInt iType
        .SetInParamLong lJobKey
        .SetInParamLong lPhaseKey
        .SetInParamLong lTaskKey
        .SetInParamLong lEmpKey
        .SetInParamLong lVendKey
        .SetInParamLong lItemKey
        .SetOutParam iMultiFound
        .SetOutParam sLine
        .SetOutParam rv
        .ExecuteSP "spPAGetJobLineNumber"
        iMultiFound = .GetOutParam(10)
        sLine = .GetOutParam(11)
        rv = .GetOutParam(12)
        .ReleaseParams
    End With
    
    If rv = 1 Then
        GetJobLineNumber = True
    ElseIf rv = 0 And iMultiFound = 1 Then
        GetJobLineNumber = False
    Else
        GetJobLineNumber = False
    End If
    Exit Function

ExpectedErrorRoutine:
    MsgBox "spPAGetJobLineNumber - " & Err.Description, vbCritical, "spPAGetJobLineNumber"
    GetJobLineNumber = False
End Function

Public Function PAGetNextSurrogateKey(moAppDB As Object, sTable As String, lKey As Long) As Boolean
    On Error GoTo ExpectedErrorRoutine
    With moAppDB
        .SetInParam sTable
        .SetOutParam lKey
        .ExecuteSP ("spGetNextSurrogateKey")
        lKey = .GetOutParam(2)
        .ReleaseParams
    End With
    PAGetNextSurrogateKey = True
    Exit Function

ExpectedErrorRoutine:
    MsgBox "spGetNextSurrogateKey - " & Err.Description, vbCritical, "spGetNextSurrogateKey"
    PAGetNextSurrogateKey = False
End Function

Public Function GetTaskType(moAppDB As Object, sTaskKey As String, iTaskType As Integer) As Boolean
    Dim rs As Object
    On Error GoTo ExpectedErrorRoutine
  
    If sTaskKey = "" Then
        sTaskKey = "0"
    End If
  
    Set rs = moAppDB.OpenRecordset("SELECT ISNULL(siTaskType, 0) FROM tPA00005 WHERE intTaskKey = " & sTaskKey, kSnapshot, kOptionNone)
    If Not rs.IsEOF Then
        iTaskType = CInt(rs.field(0))
    End If
    rs.Close
    Set rs = Nothing
    GetTaskType = True
    Exit Function

ExpectedErrorRoutine:
    MsgBox "GetTaskType - " & Err.Description, vbCritical, "GetTaskType"
    GetTaskType = False
End Function

Public Function CheckIfLineIsVariation(moAppDB As Object, sCompany As String, sJobLineKey As String) As Boolean
    Dim rs As Object, iCount As Integer
    On Error GoTo ExpectedErrorRoutine
  
    Set rs = moAppDB.OpenRecordset("SELECT intJobLineKey FROM tPA00125 WHERE intJobLineKey = " & sJobLineKey & " AND tiIsVariation = 1", kSnapshot, kOptionNone)
    If Not rs.IsEOF Then
        iCount = CInt(rs.field(0))
    End If
    rs.Close
    Set rs = Nothing
    
    If iCount > 0 Then
        CheckIfLineIsVariation = True
    Else
        CheckIfLineIsVariation = False
    End If
    Exit Function

ExpectedErrorRoutine:
    MsgBox "CheckIfLineIsVariation - " & Err.Description, vbCritical, "CheckIfLineIsVariation"
    CheckIfLineIsVariation = False
End Function

Public Function DeleteVariationLine(moAppDB As Object, sJobLineKey As String) As Boolean
  On Error GoTo ExpectedErrorRoutine
  moAppDB.ExecuteSQL ("DELETE FROM tPA00125 WHERE intJobLineKey = " & sJobLineKey)
  DeleteVariationLine = True
  Exit Function
  
ExpectedErrorRoutine:
    MsgBox "DeleteVariationLine - " & Err.Description, vbCritical, "DeleteVariationLine"
    DeleteVariationLine = False
End Function

Public Function FormatComment(sComment As TextBox)
    'Needed to fix a sage bug.  When strings with carriage return and linefeeds are brought in from the database, sage es translates them as a space and linefeed.
    'This function will replace all occurences of the space and linefeed with a carriage return and linefeed.
    Dim x As Long
    Dim sSearch As String
  
    sSearch = Chr(10) ' linefeed character
    x = InStr(1, sComment.Text, sSearch) 'see if a linefeed exists in the comment
    
    While x <> 0  ' while there are linefeeds in the comment
        If x = 1 Then ' if the first character is a linefeed
        ' replace the linefeed with a carriage return and linefeed
        sComment.Text = Left(sComment.Text, x - 1) & vbCrLf & Right(sComment.Text, Len(sComment.Text) - x)
        Else
        ' replace the space and linefeed with a carriage return and linefeed
        sComment.Text = Left(sComment.Text, x - 2) & vbCrLf & Right(sComment.Text, Len(sComment.Text) - x)
        End If
        x = InStr(x + 2, sComment.Text, sSearch) 'find the next linefeed in the comment
    Wend
End Function

Public Function PadProject(sJobNumber As String) As String
    Dim iPos As Integer
    Dim bNumeric As Boolean
    
    If Len(sJobNumber) = 0 Then
        PadProject = ""
        Exit Function
    End If
    
    bNumeric = True
    For iPos = 1 To Len(sJobNumber)
        If Mid(sJobNumber, iPos, 1) < "0" Or _
            Mid(sJobNumber, iPos, 1) > "9" Then
            bNumeric = False
        End If
    Next iPos

    If bNumeric Then
        ' use this function instead of gsZeroFill so we don't need utils.bas
        PadProject = IntellisolZeroFill(sJobNumber, 10)
    Else
        PadProject = sJobNumber
    End If
End Function

Public Function PadLookup(sText As String, iWidth As Integer) As String
    Dim iPos As Integer
    Dim bNumeric As Boolean
    
    If Len(sText) = 0 Then
        PadLookup = ""
        Exit Function
    End If
    
    bNumeric = True
    For iPos = 1 To Len(sText)
        If Mid(sText, iPos, 1) < "0" Or _
            Mid(sText, iPos, 1) > "9" Then
            bNumeric = False
        End If
    Next iPos

    If bNumeric Then
        ' use this function instead of gsZeroFill so we don't need utils.bas
        PadLookup = IntellisolZeroFill(sText, iWidth)
    Else
        PadLookup = sText
    End If
End Function

Public Function IntellisolZeroFill(sValue As String, iNum As Integer) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
' returns a value padded with zeros
' this is a copy of the sage function gsZeroFill - used here because Utils.bas is not
' included in every project

    Dim sPad As String

    If Len(sValue) > iNum Then
        IntellisolZeroFill = sValue
        Exit Function
    End If
    
    ' creating padding
    sPad = String$(iNum, "0")
    ' zero pad it
    IntellisolZeroFill = Right$(sPad & sValue, iNum)
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IntellisolZeroFill", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "PACommon"
End Function

Public Function GetProjectMiscSTaxClass(moAppDB As Object, sCompany As String, sProject As String) As Long
    Dim rs As Object
    On Error GoTo ExpectedErrorRoutine
  
    Set rs = moAppDB.OpenRecordset("SELECT tPA00175.MSTaxClassKey FROM tPA00175 " & _
        "WHERE tPA00175.chrJobNumber = " & gsQuoted(sProject) & " AND tPA00175.CompanyID = '" & sCompany & "'", kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        GetProjectMiscSTaxClass = CLng(rs.field(0))
    Else
        GetProjectMiscSTaxClass = -1
    End If
    
    rs.Close
    Set rs = Nothing
    Exit Function

ExpectedErrorRoutine:
    MsgBox "GetProjectMiscSTaxClass - " & Err.Description, vbCritical, "GetProjectMiscSTaxClass"
    GetProjectMiscSTaxClass = -1
End Function


Public Function GetHrsWrkDecimal(moAppDB As Object, sCompanyID As String) As Integer
'--------------------------------------------------------------
'Returns the number of decimal places to use for Hrs worked on timesheets
'If no value is found then the default value is used 2 ...except in grid which shows 3 places
'Max value allowed for decimal places is 5
'Checking customizer for increased decimal precision
'--------------------------------------------------------------
    On Error GoTo ExpectedErrorRoutine

    Dim sSQL As String
    Dim rs As Object
    Dim sColName As String
    Dim iValue As Integer
    
    GetHrsWrkDecimal = 2 'Ensure we have a default value
    
    Let sColName = "numChildHoursWorked" 'Form is frmTimesheetEntry from customizer
  
    Let sSQL = "SELECT ControlName,CtrlSpecPropMask,CtrlSpecProperty" & _
                                      " FROM tsmCustomFormDetl D " & _
                                      " LEFT OUTER JOIN  tsmCustomForm F ON f.CustomFormKey = d.customformkey" & _
                                     " WHERE f.FormName='frmTimesheetEntry' " & _
                                       " AND f.companyid ='" & sCompanyID & "' " & _
                                       " AND controlname = '" & sColName & "'"
    
    Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        iValue = gParseCtrlSpecProperty(rs.field("CtrlSpecPropMask"), rs.field("CtrlSpecProperty"))
    Else
        iValue = 0
    End If
    
    rs.Close
    Set rs = Nothing
    
    Select Case iValue
    Case 0, 1, 2 'default value
        'do nothing ...always a min of 2 decimal places ... default value already set
    Case 3, 4, 5
        Let GetHrsWrkDecimal = iValue
    Case Else
        Let GetHrsWrkDecimal = 2 'set to default ... value is incorrect
    End Select
    
    Exit Function

ExpectedErrorRoutine:
    MsgBox "GetHrsWrkDecimal - " & Err.Description, vbCritical, "GetHrsWrkDecimal"
    GetHrsWrkDecimal = -1 'something happend send error to calling program ...
    
    
End Function

Public Function GetPropertValue(ByVal CtrlSpecProperty As String, ByRef iEndPos As Integer) As String

'**************************************************************************************************
' GetPropertValue function is used by the gParseCtrlSpecProperty sub to retrieve
' number of decimal places to use for Billed/Work hours
'
'**************************************************************************************************

    Dim iStartPos As Integer
    Dim sValue As String
    
    ' Default return value to empty
    GetPropertValue = ""
    
    'Set iStartPos
    iStartPos = iEndPos
    
    'Check to see if a property can be found
    iEndPos = InStr(iStartPos + 1, CtrlSpecProperty, "|")
    
    'If a property is found then return the property value as a string
    If iEndPos > 0 Then
        sValue = Mid(CtrlSpecProperty, iStartPos + 1, iEndPos - iStartPos - 1)
    End If
    GetPropertValue = sValue

End Function

Public Function gParseCtrlSpecProperty(ByVal CtrlSpecPropMask As Long, ByVal CtrlSpecProperty As String) As Integer
'************************************************************************************************
' gParseCtrlSpecProperty takes the CtrlSpecPropMask and CtrlSpecProperty data and parses it for value
' CtrlSpecPropMaskCol contains the masks for the individual properties found
' and CtrlSpecPropertyValCol contains the parsed property values.
'************************************************************************************************

On Error GoTo ErrHandler

    Dim sProperty   As String
    Dim iEndPos     As Integer

    If CtrlSpecPropMask = kCtrlPropEmptyMask Or CtrlSpecProperty = "" Then
        gParseCtrlSpecProperty = 0 'calling routine will define default value
        Exit Function
    End If

    iEndPos = 1
        
    If (kCtrlSpecPropAmountMax And CtrlSpecPropMask) = kCtrlSpecPropAmountMax Then
        sProperty = GetPropertValue(CtrlSpecProperty, iEndPos)
        'sProperty is the value for this attribute
    End If

    If (kCtrlSpecPropAmountMin And CtrlSpecPropMask) = kCtrlSpecPropAmountMin Then
        sProperty = GetPropertValue(CtrlSpecProperty, iEndPos)
        'sProperty is the value for this attribute
    End If

    If (kCtrlSpecPropAutoSelect And CtrlSpecPropMask) = kCtrlSpecPropAutoSelect Then
        sProperty = GetPropertValue(CtrlSpecProperty, iEndPos)
        'sProperty is the value for this attribute
    End If

    If (kCtrlSpecPropBorder And CtrlSpecPropMask) = kCtrlSpecPropBorder Then
        sProperty = GetPropertValue(CtrlSpecProperty, iEndPos)
        'sProperty is the value for this attribute
    End If

    If (kCtrlSpecPropDecimalPlaces And CtrlSpecPropMask) = kCtrlSpecPropDecimalPlaces Then
        sProperty = GetPropertValue(CtrlSpecProperty, iEndPos)
        If sProperty <> Empty Then
            If IsNumeric(sProperty) Then
                gParseCtrlSpecProperty = CInt(sProperty)
            Else
                gParseCtrlSpecProperty = 0 'let calling routing set the default value
            End If
        End If
    End If
'--------------------------------------------------------------------
'Only concerned with decimal places...
'--------------------------------------------------------------------

    Exit Function

ErrHandler:
    ' 383 - Property is read-only, for SOTAMaskedEdit.Mask and SOTAMaskedEdit.MaxLength
    ' 394 - Property is write-only, for SOTAMaskedEdit.Mask and SOTAMaskedEdit.MaxLength
    Select Case Err.Number
        Case 383
            Resume Next
        Case 394
            Resume Next
        Case Else
            MsgBox "PACommon.gParseCtrlSpecProperty:" & vbCrLf & _
                Err.Description, vbOKOnly + vbExclamation, "PACommon.gParseCtrlSpecProperty"
    End Select

End Function

Public Function GetHrsBilledDecimal(moAppDB As Object, sCompanyID As String) As Integer
'--------------------------------------------------------------
'Returns the number of decimal places to use for Hrs worked on timesheets
'If no value is found then the default value is used 2 ...except in grid which shows 3 places
'Max value allowed for decimal places is 5
'Checking customizer for increased decimal precision
'--------------------------------------------------------------
    On Error GoTo ExpectedErrorRoutine

    Dim sSQL As String
    Dim rs As Object
    Dim sColName As String
    Dim iValue As Integer
    
    GetHrsBilledDecimal = 2 'Ensure we have a default value
    
    Let sColName = "numChildHoursBilled" 'Form is frmTimesheetEntry from customizer
  
    Let sSQL = "SELECT ControlName,CtrlSpecPropMask,CtrlSpecProperty" & _
                                      " FROM tsmCustomFormDetl D " & _
                                      " LEFT OUTER JOIN  tsmCustomForm F ON f.CustomFormKey = d.customformkey" & _
                                     " WHERE f.FormName='frmTimesheetEntry' " & _
                                       " AND f.companyid ='" & sCompanyID & "' " & _
                                       " AND controlname = '" & sColName & "'"
    
    Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        iValue = gParseCtrlSpecProperty(rs.field("CtrlSpecPropMask"), rs.field("CtrlSpecProperty"))
    Else
        iValue = 0
    End If
    
    rs.Close
    Set rs = Nothing
    
    Select Case iValue
    Case 0, 1, 2 'default value
        'do nothing ...always a min of 2 decimal places ... default value already set
    Case 3, 4, 5
        Let GetHrsBilledDecimal = iValue
    Case Else
        Let GetHrsBilledDecimal = 2 'set to default ... value is incorrect
    End Select
    
    Exit Function

ExpectedErrorRoutine:
    MsgBox "GetHrsBilledDecimal - " & Err.Description, vbCritical, "GetHrsBilledDecimal"
    GetHrsBilledDecimal = -1 'something happend send error to calling program ...
    
End Function
