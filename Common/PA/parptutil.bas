Attribute VB_Name = "PARptUtil"
Option Explicit

Private moReport As Object
Private moAppDB As Object
Private moClass As Object

'Custom fields datatype constants
Const kvValidated = 1
Const kvDate = 2
Const kvNumeric = 3
Const kvAlphanumeric = 4

Public Sub PARptInit(oreport As Object, oAppDB As Object, oClass As Object)
    Set moReport = oreport
    Set moAppDB = oAppDB
    Set moClass = oClass
End Sub

Public Sub PARptShutDown()
    Set moReport = Nothing
    Set moAppDB = Nothing
    Set moClass = Nothing
End Sub

Public Sub PARptFreeFormTextC(sFormula As String, lStringNo As Long)
    If moReport Is Nothing Then Exit Sub
    If moClass Is Nothing Then Exit Sub
    
    On Error Resume Next
    moReport.bSetReportFormula sFormula, _
      """" & gsBuildString(lStringNo, moClass.moAppDB, moClass.moSysSession) & """"
      
    gClearSotaErr
    Err.Clear
End Sub

Public Sub PARptFreeFormText(sFormula As String, sConstant As String)
    Dim sLocalText As String
    
    If moAppDB Is Nothing Then Exit Sub
    If moReport Is Nothing Then Exit Sub
    If moClass Is Nothing Then Exit Sub
    
    On Error GoTo ErrHandler
    
    sLocalText = gsGetValidStr(moAppDB.Lookup("L.LocalText", "tsmString S, tsmLocalString L", _
      "UPPER(S.ConstantName) = UPPER(" & gsQuoted(sConstant) & ")" & _
      " AND S.StringNo = L.StringNo AND L.LanguageID = " & _
      Trim(Str(moClass.moSysSession.Language))))
    
    moReport.bSetReportFormula sFormula, """" & sLocalText & """"
      
ErrHandler:
    gClearSotaErr
    Err.Clear
End Sub

Public Sub PASortSelectionGrid(frm As Form, oSelect As Object, grdSelection As Object)
    Dim lRow As Long
    Dim lSubRow As Long
    Dim lMaxRows As Long
    Dim lBigRow As Long
    Dim lOldRow As Long
    Dim lOldCol As Long
    
    lOldRow = grdSelection.Row
    lOldCol = grdSelection.Col
    
    lMaxRows = oSelect.lMaxSelGridRows
    
    For lRow = lMaxRows To 1 Step -1
        grdSelection.Row = lRow
        grdSelection.Col = 1
        If Not grdSelection.RowHidden Then
            lBigRow = lRow
            For lSubRow = lRow - 1 To 1 Step -1
                If gsGetValidStr(gsGridReadCell(grdSelection, lSubRow, 1)) > _
                  gsGetValidStr(gsGridReadCell(grdSelection, lBigRow, 1)) Then
                    lBigRow = lSubRow
                End If
            Next lSubRow
            If lBigRow <> lRow Then     ' new largest value found
                If oSelect.lMoveRow(lBigRow, lRow) <> 1 Then
                    ' big bad error go scream bloody murder
                    MsgBox "Fatal error in PASortSelectionGrid - BUG!"
                End If
            End If
        End If
    Next lRow
    
    grdSelection.Row = lOldRow
    grdSelection.Col = lOldCol
End Sub

Public Sub PAAddCustomFields(frm As Form, sRealTableCollection As Collection, oSelect As Object)
    Dim lItem As Long
    Dim lValue As Long
    
    Dim lEntityType As Long
    Dim sSQL As String
    Dim rs As Object
    Dim rsSub As Object
    
    Dim sTitle As String
    Dim lUserFldKey As Long
    Dim lUserFldNo As Long
    Dim lUserFldDataType as Long
    Dim DBObj As Object
    Dim sColumnName As String
    Dim sColumnBase As String
    Dim sValues As Collection
    Dim sStaticDBValues() As String
    Dim sStaticText() As String
    
    On Error GoTo badexit
    
    Set DBObj = frm.oClass.moAppDB
    
    For lItem = 1 To sRealTableCollection.Count
        sColumnBase = "chrUserDef"
    
        Select Case sRealTableCollection.Item(lItem)
            Case "tPA00007"
                lEntityType = 1980
            
            Case "tPA00006"
                lEntityType = 1981
            
            Case "tPA00175"
                lEntityType = 1982
                
            Case "tPA00176"
                lEntityType = 1983
                
            Case Else
                lEntityType = 0
        End Select
        
        If lEntityType > 0 Then
            sSQL = "SELECT DISTINCT U.Title, U.UserFldKey, U.UserFldNo, U.DataType" & _
                   " FROM tciUserField U " & _
                   " WHERE U.EntityType = " & Trim(Str(lEntityType)) & _
                   " AND U.CompanyID = " & gsQuoted(frm.oClass.moSysSession.CompanyId) & _
                   " AND U.DataType in (1,2,3,4) AND ISNULL(U.Title,'') <>'' " & _
                   " ORDER BY U.UserFldNo"
            Set rs = DBObj.OpenRecordset(sSQL, kSnapshot, kOptionNone)

            While Not rs.IsEOF
                sTitle = gsGetValidStr(rs.Field(0))
                lUserFldKey = glGetValidLong(rs.Field(1))
                lUserFldNo = glGetValidLong(rs.Field(2))
                lUserFldDataType = glGetValidLong(rs.Field(3))
                sColumnName = sColumnBase & Trim(Str(lUserFldNo))
                
                If lUserFldDataType = kvValidated Then 'Static Validated List Check
                    sSQL = "SELECT UserFldValue FROM tciUserFieldValue WHERE UserFldKey = " & _
                        Trim(Str(lUserFldKey)) & " ORDER BY UserFldKey"
                    Set rsSub = DBObj.OpenRecordset(sSQL, kSnapshot, kOptionNone)

                    If Not rsSub.IsEOF Then
                        Set sValues = New Collection

                        While Not rsSub.IsEOF
                            sValues.Add gsGetValidStr(rsSub.Field(0))
                            rsSub.MoveNext
                        Wend

                        ReDim sStaticDBValues(sValues.Count)
                        ReDim sStaticText(sValues.Count)

                        For lValue = 1 To sValues.Count
                            sStaticText(lValue) = sValues.Item(lValue)
                            sStaticDBValues(lValue) = gsQuoted(sValues.Item(lValue))
                        Next lValue

                        oSelect.lAddSelRowStaticList sRealTableCollection.Item(lItem), sColumnName, _
                        sTitle, sTitle, True, "", _
                        sStaticDBValues(), sStaticText()
                        Set sValues = Nothing
                    End If

                    rsSub.Close
                    Set rsSub = Nothing
                ElseIf lUserFldDataType = kvDate Then    'Date Datatype Check
                    oSelect.lAddSelectionRow sRealTableCollection.Item(lItem), sColumnName, sTitle, 10, SQL_DATE, 0, False, "", "", ""
                ElseIf lUserFldDataType = kvAlphanumeric Then     'AlphaNumeric Datatype Check
                    oSelect.lAddSelectionRow sRealTableCollection.Item(lItem), sColumnName, sTitle, 20, SQL_VARCHAR, 0, False, "", "", ""
                ElseIf lUserFldDataType = kvNumeric Then     'numeric Datatype Check
                    oSelect.lAddSelectionRow sRealTableCollection.Item(lItem), sColumnName, sTitle, 20, SQL_NUMERIC, 0, False, "", "", ""
                End If
                rs.MoveNext
            Wend

            rs.Close
            Set rs = Nothing
        End If
    Next lItem
    
    Exit Sub
    
badexit:
    On Error Resume Next
    MsgBox "PAAddCustomFields - " & Err.Description, vbCritical, gsBuildString(kSotaTitle, frm.oClass.moAppDB, frm.oClass.moSysSession)
    gClearSotaErr
    Set rsSub = Nothing
    Set sValues = Nothing
    Set rs = Nothing
    gClearSotaErr
End Sub

