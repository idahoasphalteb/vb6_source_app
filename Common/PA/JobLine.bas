Attribute VB_Name = "basJobLine"
'**********************************************************************
'     Name: basJobLine
'     Desc: Special module to handle some Job-specific attributes for
'            sage apps.
' Original: 06/23/1999
'     Mods: mm/dd/yy XXX
'**********************************************************************
Option Explicit
    
    '-- Job-related grid column constants
    Public Const kColProjectIDJ = 1
    Public Const kColProjectKeyJ = 2
    Public Const kColPhaseIDJ = 3
    Public Const kColPhaseKeyJ = 4
    Public Const kColTaskIDJ = 5
    Public Const kColTaskKeyJ = 6
    Public Const kColCostClassIDJ = 7
    Public Const kColCostClassKeyJ = 8
    Public Const kColVarianceStrJ = 9
    Public Const kColVarianceJ = 10
    Public Const kColJobLineKeyJ = 11
    Public Const kColDocLineKeyJ = 12
    Public Const kColJobTypeJ = 13
    Public Const kColTaskTypeJ = 14
    Public Const kColAddUpdDelJ = 15
    Public Const kColLineClosed = 16
    Public Const kColJobCostRemain = 17
    Public Const kColBillMethodJ = 18
    Public Const kColBillMethodKeyJ = 19

#If RM_Invoice Then
    Public Const kColEstSaleJ = 18
    Public Const kColActSaleJ = 19
    Public Const kColInvDescJ = 20
    Public Const kColInvDescKeyJ = 21
    Public Const kColCommentJ = 22      ' 3/24/00 cec - ts comment
    Public Const kColSiFromTSJ = 23     ' 3/24/00 cec - ts comment
    Public Const kMaxColsJ = 23         ' 3/24/00 cec - ts comment
#Else
    Public Const kMaxColsJ = 19
#End If

    '-- Validation Manager constants
    Public Const kSOTA_INVALID = 0
    Public Const kSOTA_VALID = -1
    Public Const kSOTA_FIELD = 0
    Public Const kSOTA_FORM = 1
    
    '-- Misc. constants
    Public Const kIsLineClosed = "1"
    Public Const kJobSaved = 0
    Public Const kJobEstimated = 1
    Public Const kJobQuoted = 2
    Public Const kJobActive = 3
    Public Const kJobClosed = 4
    Public Const kILDescription = 1
    
    '-- Misc. variables
    Public miLineStatus     As Integer
    Public mbIntPAActive    As Boolean

Const VBRIG_MODULE_ID_STRING = "JOBLINE.BAS"

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basJobLine"
End Function

#If RM_Invoice Then
Public Sub SetGridRedraw(grd As Object, bFlag As Boolean)
'*********************************************************
' Desc: This routine will use the Windows API and the grid
'       methods to turn redraw on/off. The Windows API
'       is used because it WORKS!, whereas the grid method
'       is flaky!
'*********************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    With grd
        SendMessage .hWnd, WM_SETREDRAW, bFlag, 0
        .redraw = bFlag
        If bFlag Then .Refresh
    End With
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetGridRedraw", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
#End If

