Attribute VB_Name = "basPerCashFlowRpt"
'***********************************************************************
'     Name: basPerCashFlowRpt
'     Desc: Main Module of this Object containing "Sub Main ()".
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: KMKD 3/97
'     Mods:
'***********************************************************************
Option Explicit

Public moProjCat                    As Object
Private miAPCalcMethod              As Integer
Private miARCalcMethod              As Integer
Private miPOCalcMethod              As Integer
Private miSOCalcMethod              As Integer
Private msGridWhereClause           As String
Private msTablesUsed                As String
Private miNumTablesUsed             As Integer
Private moDBObj                     As DASDatabase
Private moSelectObj                 As clsSelection
Private mlSessionID                 As Long
Private moReportObj                 As clsReportEngine
Private msCompanyID                 As String
Private msAsOfDate                  As String
Private msDate1                     As String
Private msDate2                     As String
Private msDate3                     As String
Private msDate4                     As String
Private msDate5                     As String
Private msDate6                     As String

Private iRetVal                     As Integer
Private lRow                        As Long
Private sTableColumnName            As String
Private sDescription                As String
Private iSubtotal                   As Integer
Private iPageBreak                  As Integer
Private iSortOrder                  As Integer
Private sColumnName                 As String
Private iSeqNo                      As Integer
Private sTableName                  As String
Private sParentTableName            As String

Private Const kSummaryRpt           As Integer = 0
Private Const kDetailRpt            As Integer = 1
Private Const kConsolidatedRpt      As Integer = 2

Private Const kPayables             As Integer = 1
Private Const kReceivables          As Integer = 2
Private Const kCashMgmt             As Integer = 3
Private Const kOther                As Integer = 4

Private Const kPostedVouchers       As Integer = 1
Private Const kPendingVouchers      As Integer = 2
Private Const kPostedVendPmts       As Integer = 3
Private Const kPendingVendPmts      As Integer = 4
Private Const kOpenPurchOrders      As Integer = 5
Private Const kOpenSalesOrders      As Integer = 6

Private Const kPostedInvoices       As Integer = 1
Private Const kPendingInvoices      As Integer = 2
Private Const kPostedCustPmts       As Integer = 3
Private Const kPendingCustPmts      As Integer = 4

Private Const kMiscCashCM           As Integer = 1
Private Const kMiscCashAR           As Integer = 2

Private Const kFutureCashFlows      As Integer = 1

Private Const kNoMoreEntries = 999

Const VBRIG_MODULE_ID_STRING = "CMZRA001.BAS"

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "CMZRA001.BAS"
End Function

Public Function lStartReport(sButton As String, frm As Form, bPeriodEnd As Boolean, sPerEndDate, iPerNo, sPerYear, _
    Optional iFileType As Variant, _
    Optional sFileName As Variant) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    
    Dim lRetVal As Long
    Dim iFormat As Integer
    Dim iGrpByAcct As Integer
    Dim iGrpByBank As Integer
    Dim sRptFileName As String
    Dim sSortString As String
    
    'set methods selected
    miAPCalcMethod = frm.cboAPCalcBy.ListIndex
    miARCalcMethod = frm.cboARCalcBy.ListIndex
    miPOCalcMethod = frm.cboPOCalcBy.ListIndex
    miSOCalcMethod = frm.ddnSOCalcBy.ListIndex
    
    On Error GoTo badexit
    lStartReport = kFailure
    
    If Not bPeriodEnd Then ShowStatusBusy frm
    
    Set moSelectObj = frm.moSelect
    Set moReportObj = frm.moReport
    Set moDBObj = frm.oClass.moAppDB
    mlSessionID = moReportObj.SessionID
    msCompanyID = frm.msCompanyID
    msAsOfDate = moProjCat.dAsOfDate
    msDate6 = moProjCat.dDate6
    iFormat = frm.iRptType
    msGridWhereClause = ""
    
    'Delete all records from work tables that have the same session ID as current session ID.
    moReportObj.CleanupWorkTables
    
    'Correct bad user input in the Selection grid.
    moSelectObj.lValidateGrid True
    
    'Create the WHERE clause based on user input in the Selection grid & Session ID.
    moSelectObj.bGetWhereClause msGridWhereClause, msTablesUsed, miNumTablesUsed, True
    moSelectObj.AddJoinIfNecessary msGridWhereClause, msTablesUsed, "tcmBank", "tcmBank.BankKey=tcmCashAcct.BankKey"
    
    'Insert keys into work table : Payables
    '1) Posted Vouchers
    If Not bInsertVoucherKeys("tapVoucher", kPostedVouchers) Then
        giSotaMsgBox frm, frm.oClass.moSysSession, kmsgErrInsertingKeys, gsBuildString(kVoucher, frm.oClass.moAppDB, frm.oClass.moSysSession)
        GoTo badexit
    End If
    
    '2) Pending Vouchers
    If frm.chkAPInclPending = vbChecked Then
        If Not bInsertVoucherKeys("tapPendVoucher", kPendingVouchers) Then
            giSotaMsgBox frm, frm.oClass.moSysSession, kmsgErrInsertingKeys, gsBuildString(kVoucher, frm.oClass.moAppDB, frm.oClass.moSysSession)
            GoTo badexit
        End If
    End If
    
    '3) Posted vendor payments
    If frm.chkAPInclUnapplied = vbChecked Then
        If Not bInsertPaymentKeys("tapVendPmt", "VendPmtKey", kPayables, kPostedVendPmts, False) Then
            giSotaMsgBox frm, frm.oClass.moSysSession, kmsgErrInsertingKeys, gsBuildString(kPayment, frm.oClass.moAppDB, frm.oClass.moSysSession)
            GoTo badexit
        End If
        
        '4) Pending vendor payments
        If frm.chkAPInclPending = vbChecked Then
            If Not bInsertPaymentKeys("tapPendVendPmt", "VendPmtKey", kPayables, kPendingVendPmts, True) Then
                giSotaMsgBox frm, frm.oClass.moSysSession, kmsgErrInsertingKeys, gsBuildString(kPayment, frm.oClass.moAppDB, frm.oClass.moSysSession)
                GoTo badexit
            End If
        End If
    End If
    
    '5) Unvouchered portions of open purchase orders
    'Purchase activity is never associated with a bank account, so if any selection
    'criteria entered, skip key selection of purchase orders.
    If InStr(msGridWhereClause, "tcmCashAcct") = 0 Then
        If frm.chkInclOpenPOs = vbChecked Then
            If Not bInsertPOKeys() Then
                giSotaMsgBox frm, frm.oClass.moSysSession, kmsgErrInsertingKeys, gsBuildString(ksPOPurchOrder, frm.oClass.moAppDB, frm.oClass.moSysSession)
                GoTo badexit
            End If
        End If
    End If
        
    
    'Insert keys into work table : Receivables
    'Receivables activity is never associated with a bank account, so if any selection
    'criteria entered, skip key selection of receivables.
    If InStr(msGridWhereClause, "tcmCashAcct") = 0 Then
        '1) Posted Invoices
        If Not bInsertInvoiceKeys("tarInvoice", kPostedInvoices) Then
            giSotaMsgBox frm, frm.oClass.moSysSession, kmsgErrInsertingKeys, gsBuildString(kARInvoice, frm.oClass.moAppDB, frm.oClass.moSysSession)
            GoTo badexit
        End If
          
        '2) Pending invoices
        If frm.chkARInclPending = vbChecked Then
            If Not bInsertInvoiceKeys("tarPendInvoice", kPendingInvoices) Then
                giSotaMsgBox frm, frm.oClass.moSysSession, kmsgErrInsertingKeys, gsBuildString(kARInvoice, frm.oClass.moAppDB, frm.oClass.moSysSession)
                GoTo badexit
            End If
        End If
        
        '3) Posted customer payments
        If frm.chkARInclUnapplied = vbChecked Then
            If Not bInsertPaymentKeys("tarCustPmt", "CustPmtKey", kReceivables, kPostedCustPmts, False) Then
                giSotaMsgBox frm, frm.oClass.moSysSession, kmsgErrInsertingKeys, gsBuildString(kPayment, frm.oClass.moAppDB, frm.oClass.moSysSession)
                GoTo badexit
            End If
            
            '4) Pending customer payments
            If frm.chkARInclPending = vbChecked Then
                If Not bInsertPaymentKeys("tarPendCustPmt", "CustPmtKey", kReceivables, kPendingCustPmts, True) Then
                    giSotaMsgBox frm, frm.oClass.moSysSession, kmsgErrInsertingKeys, gsBuildString(kPayment, frm.oClass.moAppDB, frm.oClass.moSysSession)
                    GoTo badexit
                End If
            End If
        End If
    End If
    
     '5) Uninvoiced portions of open purchase orders
    'Sales activity is never associated with a bank account, so if any selection
    'criteria entered, skip key selection of sales orders.
    If InStr(msGridWhereClause, "tcmCashAcct") = 0 Then
        If frm.chkInclOpenSos = vbChecked Then
            If Not bInsertSOKeys() Then
                giSotaMsgBox frm, frm.oClass.moSysSession, kmsgErrInsertingKeys, gsBuildString(kSOSalesOrder, frm.oClass.moAppDB, frm.oClass.moSysSession)
                GoTo badexit
            End If
        End If
    End If
        

    'Insert keys into work table : Misc Cash
    If frm.chkCashInclPosted = vbChecked Then
        If Not bInsertPostedMiscCashKeys Then
            giSotaMsgBox frm, frm.oClass.moSysSession, kmsgErrInsertingKeys, "posted AR and CM miscellaneous cash"
            GoTo badexit
        End If
    
        If frm.chkCashInclPending = vbChecked Then
            If Not bInsertPendMiscCashKeys Then
                giSotaMsgBox frm, frm.oClass.moSysSession, kmsgErrInsertingKeys, "pending AR and CM miscellaneous cash"
                GoTo badexit
            End If
        End If
    End If

    
    'Insert keys into work table : Future Cash Flows
    If frm.chkFutureCashFlows = vbChecked Then
        If Not bInsertFutureCashFlowKeys() Then
            giSotaMsgBox frm, frm.oClass.moSysSession, kmsgErrInsertingKeys, gsBuildString(kFutureCashFlow, frm.oClass.moAppDB, frm.oClass.moSysSession)
            GoTo badexit
        End If
    End If
    
    
    'Check whether any records were inserted; if not, go to error handler.
    If IsMissing(iFileType) Then
        If Not moSelectObj.bRecsToPrintExist("tcmCashFlowRptWrk", "SessionID", mlSessionID, True) Then
            GoTo badexit
        End If
    End If
    
    If iFormat = kConsolidatedRpt Or iFormat = kSummaryRpt Then
        For lRow = 1 To frm.moSort.MaxRows
            iRetVal = frm.moSort.lGetRow(lRow, sTableColumnName, sDescription, iSubtotal, _
                             iPageBreak, iSortOrder, sColumnName, iSeqNo, sTableName, sParentTableName)
            If iRetVal = kNoMoreEntries Then
                Exit For
            End If
            If iRetVal = kSuccess Then
                If sParentTableName = "tcmBank" And iSubtotal = 1 Then
                    iGrpByBank = 1
                ElseIf sParentTableName = "tcmCashAcct" And iSubtotal = 1 Then
                    iGrpByAcct = 1
                End If
            End If
        Next lRow
    End If

    'Set variables for stored procedure parameters.
    msDate1 = moProjCat.dDate1
    msDate2 = moProjCat.dDate2
    msDate3 = moProjCat.dDate3
    msDate4 = moProjCat.dDate4
    msDate5 = moProjCat.dDate5
        
    With moDBObj
        .SetInParam mlSessionID
        .SetInParam gsFormatDateToDB(msAsOfDate)
        .SetInParam gsFormatDateToDB(msDate1)
        .SetInParam gsFormatDateToDB(msDate2)
        .SetInParam gsFormatDateToDB(msDate3)
        .SetInParam gsFormatDateToDB(msDate4)
        .SetInParam gsFormatDateToDB(msDate5)
        .SetInParam gsFormatDateToDB(msDate6)
        .SetInParamInt miAPCalcMethod
        .SetInParamInt frm.chkAPDiscTaken
        .SetInParamInt frm.chkAPInclPending
        .SetInParamInt frm.chkAPInclUnapplied
        .SetInParamInt miARCalcMethod
        .SetInParamInt frm.chkARDiscTaken
        .SetInParamInt frm.chkARInclPending
        .SetInParamInt frm.chkARInclUnapplied
        .SetInParamInt miPOCalcMethod
        .SetInParamInt frm.chkInclOpenPOs
        .SetInParamInt miSOCalcMethod
        .SetInParamInt frm.chkInclOpenSos
        .SetInParamInt frm.chkFutureCashFlows
        .SetInParamInt frm.chkCashInclPosted
        .SetInParamInt frm.chkCashInclPending
        .SetInParamInt iFormat
        .SetInParamInt iGrpByBank
        .SetInParamInt iGrpByAcct
        .SetInParamStr gsBuildString(kUnassigned, frm.oClass.moAppDB, frm.oClass.moSysSession)
        .SetOutParam lRetVal
        .SetInParamInt frm.ModuleSelections 'EB-JMM 2/13/2018
        On Error Resume Next
        .ExecuteSP ("spCMPerCashFlowRpt")
        If Err.Number <> 0 Then
            lRetVal = Err.Number
        Else
            lRetVal = moDBObj.GetOutParam(28)
        End If
    End With
     
    If lRetVal <> 0 Then
        moReportObj.ReportError kmsgProc, "spCMPerCashFlowRpt"
        moDBObj.ReleaseParams
        GoTo badexit
    End If
        
    moDBObj.ReleaseParams
    gClearSotaErr
    
    'Check work table again in case pending applications reduced all transactions to zero balance.
    If IsMissing(iFileType) Then
        If Not moSelectObj.bRecsToPrintExist("tcmCashFlowRptWrk", "SessionID", mlSessionID, True) Then
            GoTo badexit
        End If
    End If
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    Select Case iFormat
        Case kDetailRpt
            sRptFileName = "cmzra001.rpt"
        Case Else
            sRptFileName = "cmzra002.rpt"
    End Select
    
    moReportObj.ReportFileName() = sRptFileName
    
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (moReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    moReportObj.Orientation() = vbPRORLandscape
    
    Select Case iFormat
        Case kDetailRpt
            moReportObj.ReportTitle1() = gsBuildString(kPerCashFlowProj, frm.oClass.moAppDB, frm.oClass.moSysSession) & " - " & gsBuildString(kDetail, frm.oClass.moAppDB, frm.oClass.moSysSession)
        Case kSummaryRpt
            moReportObj.ReportTitle1() = gsBuildString(kPerCashFlowProj, frm.oClass.moAppDB, frm.oClass.moSysSession) & " - " & gsBuildString(kSummary, frm.oClass.moAppDB, frm.oClass.moSysSession)
        Case kConsolidatedRpt
            moReportObj.ReportTitle1() = gsBuildString(kPerCashFlowProj, frm.oClass.moAppDB, frm.oClass.moSysSession) & " - " & gsBuildString(kConsolidated, frm.oClass.moAppDB, frm.oClass.moSysSession)
    End Select
   
    moReportObj.ReportTitle2() = "As of " & msAsOfDate
            
    
    moReportObj.UseSubTotalCaptions() = 1
    moReportObj.UseHeaderCaptions() = 1
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (moReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If

    'Set sort order in .RPT file according to user selections in the Sort grid.
    If (moReportObj.lSetSortCriteria(frm.moSort) = kFailure) Then
        GoTo badexit
    End If
    
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    moReportObj.BuildSQL
    
    ''Added by NK
    ''To sort the report based on the TranDate
        
    If iFormat = kDetailRpt Then
        moReportObj.AppendSort "tcmCashFlowRptWrk.TranDate"
    End If
    
    moReportObj.SetSQL

    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    moReportObj.SetReportCaptions
    
    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
    moReportObj.SelectString = moSelectObj.sGetUserReadableWhereClause(kLenLandscape)
    
    'If user has chosen to print report settings, set text for summary section.
    If (moReportObj.lSetSummarySection(frm.chkSummary.Value, frm.moOptions, 1) = kFailure) Then
        GoTo badexit
    End If
    
    'CUSTOMIZE:  If using work tables, restrict report data to current Session ID.  If using
    'real tables, might restrict report data to current company or other criteria.
    If iFormat = kDetailRpt Then
        If (moReportObj.lRestrictBy("{tcmCashFlowRptWrk.SessionID} = " & mlSessionID) = kFailure) Then
            GoTo badexit
        End If
    ElseIf (moReportObj.lRestrictBy("{tcmCshFlwRptHdrWrk.SessionID} = " & mlSessionID) = kFailure) Then
        GoTo badexit
    End If
    
      
    If iFormat = kSummaryRpt Then
        moReportObj.bSetReportFormula "Summary", kSQuote & "1" & kSQuote
    ElseIf iFormat = kConsolidatedRpt Then
        moReportObj.bSetReportFormula "Summary", kSQuote & "0" & kSQuote
    End If
    
    'Set labels for date column headings.
    moReportObj.bSetReportFormula "AsOfDateLbl", kSQuote & msAsOfDate & kSQuote
    moReportObj.bSetReportFormula "Date1Lbl", kSQuote & msDate1 & kSQuote
    moReportObj.bSetReportFormula "Date2Lbl", kSQuote & msDate2 & kSQuote
    moReportObj.bSetReportFormula "Date3Lbl", kSQuote & msDate3 & kSQuote
    moReportObj.bSetReportFormula "Date4Lbl", kSQuote & msDate4 & kSQuote
    moReportObj.bSetReportFormula "Date5Lbl", kSQuote & msDate5 & kSQuote
    moReportObj.bSetReportFormula "Date6Lbl", kSQuote & msDate6 & kSQuote
    
    moReportObj.ProcessReport frm, sButton, iFileType, sFileName
            
    If Not bPeriodEnd Then ShowStatusNone frm
    
    lStartReport = kSuccess
    
    Set moSelectObj = Nothing
    Set moReportObj = Nothing
    Set moDBObj = Nothing
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
badexit:
    moReportObj.CleanupWorkTables
    If Not bPeriodEnd Then ShowStatusNone frm
    Set moSelectObj = Nothing
    Set moReportObj = Nothing
    Set moDBObj = Nothing
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lStartReport", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Private Function bInsertVoucherKeys(sTableName As String, iDocType As Integer) As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***************************************************************************************
'      Desc:    bInsertVoucherKeys - Builds and executes SQL statement to insert voucher
'                   keys into the work table.
'     Parms:    sTableName - Name of the voucher table to reference.
'               iDocType - Type of document key to retrieve (posted or pending).
'   Returns:    True - Success
'               False - Failure
'***************************************************************************************
    
    Dim sWhereClause As String
    Dim sTablesUsed As String
    Dim iNumTablesUsed As Integer
    Dim sSelect As String
    Dim sInsert As String
    
    bInsertVoucherKeys = False
    
    sWhereClause = msGridWhereClause
    sTablesUsed = msTablesUsed
    iNumTablesUsed = miNumTablesUsed
    
    If InStr(sWhereClause, "tcmCashAcct") <> 0 Then
        moSelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tcmCashAcct", sTableName & ".CashAcctKey=tcmCashAcct.CashAcctKey"
    End If
    
    moSelectObj.AppendToWhereClause sWhereClause, sTableName & ".CompanyID='" & msCompanyID & kSQuote
    
    Select Case miAPCalcMethod
        Case 0
            moSelectObj.AppendToWhereClause sWhereClause, sTableName & ".DueDate <= " _
                & gsQuoted(gsFormatDateToDB(msDate6))
        Case 1
            moSelectObj.AppendToWhereClause sWhereClause, "ISNULL (" & sTableName & ".DiscDate, " _
                & sTableName & ".DueDate) <= " & gsQuoted(gsFormatDateToDB(msDate6))
        Case 2
            moSelectObj.AppendToWhereClause sWhereClause, "((tapVendStatus.AvgDaysToPay > 0 AND " _
                & "tapVendStatus.AvgDaysToPay IS NOT NULL AND DateAdd(day, ROUND" _
                & "(tapVendStatus.AvgDaysToPay, 0), " & sTableName & ".TranDate) <= " _
                & gsQuoted(gsFormatDateToDB(msDate6)) _
                & ") OR ((tapVendStatus.AvgDaysToPay = 0 OR tapVendStatus.AvgDaysToPay IS NULL) AND " _
                & sTableName & ".DueDate <= " _
                & gsQuoted(gsFormatDateToDB(msDate6)) & "))"
'            moSelectObj.AppendToWhereClause sWhereClause, "ISNULL (DateAdd(day, ROUND" _
'                & "(tapVendStatus.AvgDaysToPay, 0), " & sTableName & ".TranDate), " _
'                & sTableName & ".DueDate) BETWEEN " & gsQuoted(gsFormatDateToDB(msAsOfDate)) & " AND " & gsQuoted(gsFormatDateToDB(msDate6))
            moSelectObj.AppendToWhereClause sWhereClause, sTableName & ".VendKey=tapVendor.VendKey"
            moSelectObj.AppendToWhereClause sWhereClause, "tapVendor.VendKey=tapVendStatus.VendKey"
    End Select
    
    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
    If Not moSelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    sSelect = "SELECT DISTINCT " & mlSessionID & ", " & kPayables & ", " & iDocType & _
        ", " & sTableName & ".VoucherKey FROM " & sTableName
    
    If InStr(sWhereClause, "tapVendor") <> 0 Then
        sSelect = sSelect & ", tapVendor, tapVendStatus"
    End If
    
    If InStr(sWhereClause, "tcmCashAcct") <> 0 Then sSelect = sSelect & ", " & sTablesUsed
    
    sSelect = sSelect & " " & sWhereClause
    
    sInsert = "INSERT INTO tcmCashFlowRptWrk(SessionID, Category, DocType, DocKey) " & sSelect
        
    Debug.Print sInsert
    
    On Error Resume Next
    moDBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        moReportObj.ReportError kmsgProc, sInsert
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    gClearSotaErr
    
    bInsertVoucherKeys = True
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bInsertVoucherKeys", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Private Function bInsertInvoiceKeys(sTableName As String, iDocType As Integer) As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***************************************************************************************
'      Desc:    bInsertInvoiceKeys - Builds and executes SQL statement to insert invoice
'                   keys into the work table.
'     Parms:    sTableName - Name of the invoice table to reference.
'               iDocType - Type of document key to retrieve (posted or pending).
'   Returns:    True - Success
'               False - Failure
'***************************************************************************************
    
    Dim sWhereClause As String
    Dim sTablesUsed As String
    Dim iNumTablesUsed As Integer
    Dim sSelect As String
    Dim sInsert As String
    
    bInsertInvoiceKeys = False
    
    sWhereClause = msGridWhereClause
    sTablesUsed = msTablesUsed
    iNumTablesUsed = miNumTablesUsed
    
    moSelectObj.AppendToWhereClause sWhereClause, sTableName & ".CompanyID='" & msCompanyID & kSQuote
    
    Select Case miARCalcMethod
        Case 0
            moSelectObj.AppendToWhereClause sWhereClause, sTableName & ".DueDate <= " _
                & gsQuoted(gsFormatDateToDB(msDate6))
        Case 1
            moSelectObj.AppendToWhereClause sWhereClause, "ISNULL (" & sTableName & ".DiscDate, " _
                & sTableName & ".DueDate) <= " & gsQuoted(gsFormatDateToDB(msDate6))
        Case 2
            moSelectObj.AppendToWhereClause sWhereClause, "((tarCustStatus.AvgDaysToPay > 0 AND " _
                & "tarCustStatus.AvgDaysToPay IS NOT NULL AND DateAdd(day, ROUND" _
                & "(tarCustStatus.AvgDaysToPay, 0), " & sTableName & ".TranDate) <= " _
                & gsQuoted(gsFormatDateToDB(msDate6)) _
                & ") OR ((tarCustStatus.AvgDaysToPay = 0 OR tarCustStatus.AvgDaysToPay IS NULL) AND " _
                & sTableName & ".DueDate <= " _
                & gsQuoted(gsFormatDateToDB(msDate6)) & "))"
'            moSelectObj.AppendToWhereClause sWhereClause, "ISNULL (DateAdd(day, ROUND" _
'                & "(tarCustStatus.AvgDaysToPay, 0), " & sTableName & ".TranDate), " _
'                & sTableName & ".DueDate) BETWEEN " & gsQuoted(gsFormatDateToDB(msAsOfDate)) & " AND " & gsQuoted(gsFormatDateToDB(msDate6))
            moSelectObj.AppendToWhereClause sWhereClause, sTableName & ".CustKey=tarCustomer.CustKey"
            moSelectObj.AppendToWhereClause sWhereClause, "tarCustomer.CustKey=tarCustStatus.CustKey"
    End Select
    
    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
    If Not moSelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    sSelect = "SELECT DISTINCT " & mlSessionID & ", " & kReceivables & ", " & iDocType & _
        ", " & sTableName & ".InvcKey FROM " & sTableName
    
    If InStr(sWhereClause, "tarCustomer") <> 0 Then
        sSelect = sSelect & ", tarCustomer, tarCustStatus"
    End If
    
    If InStr(sWhereClause, "tcmCashAcct") <> 0 Then sSelect = sSelect & ", " & sTablesUsed
    
    sSelect = sSelect & " " & sWhereClause
    
    sInsert = "INSERT INTO tcmCashFlowRptWrk(SessionID, Category, DocType, DocKey) " & sSelect
    
    Debug.Print sInsert
    
    On Error Resume Next
    moDBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        moReportObj.ReportError kmsgProc, sInsert
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    gClearSotaErr
    
    bInsertInvoiceKeys = True
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bInsertInvoiceKeys", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Private Function bInsertPaymentKeys(sTableName As String, sKeyFieldName As String, _
        iCategory As Integer, iDocType As Integer, bRestrictThroughBatchLog As Boolean) As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***************************************************************************************
'      Desc:    bInsertPaymentKeys - Builds and executes SQL statement to insert payment
'                   keys into the work table.
'     Parms:    sTableName - Name of the payment table to reference.
'               sKeyFieldName - Name of the key field in the payment table.
'               iCategory - Category (AR or AP) or the payments sought.
'               iDocType - Type of document key to retrieve (posted or pending).
'               bRestrictThroughBatchLog - Indicates whether or not a join to tciBatchLog
'                   is necessary to restrict to the current company.
'   Returns:    True - Success
'               False - Failure
'***************************************************************************************
    
    Dim sWhereClause As String
    Dim sTablesUsed As String
    Dim iNumTablesUsed As String
    Dim sSelect As String
    Dim sInsert As String
    
    bInsertPaymentKeys = False
    
    sWhereClause = msGridWhereClause
    sTablesUsed = msTablesUsed
    iNumTablesUsed = miNumTablesUsed
    
    If Not bRestrictThroughBatchLog Then
        moSelectObj.AppendToWhereClause sWhereClause, sTableName & ".CompanyID='" & msCompanyID & kSQuote
    Else
        moSelectObj.AppendToWhereClause sWhereClause, "tciBatchLog.SourceCompanyID = '" & msCompanyID & kSQuote
        moSelectObj.AppendToWhereClause sWhereClause, sTableName & ".BatchKey=tciBatchLog.BatchKey"
    End If
    
    If InStr(sWhereClause, "tcmCashAcct") <> 0 Then
        moSelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tcmCashAcct", sTableName & ".CashAcctKey=tcmCashAcct.CashAcctKey"
    End If
    moSelectObj.AppendToWhereClause sWhereClause, sTableName & ".TranDate <= " & gsQuoted(gsFormatDateToDB(msDate6))
    moSelectObj.AppendToWhereClause sWhereClause, sTableName & ".UnappliedAmtHC <> 0.0"
        
    If Not moSelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
        
    sSelect = "SELECT DISTINCT " & mlSessionID & ", " & iCategory & ", " & iDocType & _
        ", " & sTableName & "." & sKeyFieldName & " FROM " & sTableName
    
    If InStr(sWhereClause, "tcmCashAcct") <> 0 Then
        sSelect = sSelect & ", " & sTablesUsed
    End If
    
    If bRestrictThroughBatchLog Then
        sSelect = sSelect & ", tciBatchLog"
    End If
        
    sSelect = sSelect & " " & sWhereClause
    
    sInsert = "INSERT INTO tcmCashFlowRptWrk(SessionID, Category, DocType, DocKey) " & sSelect
    
    Debug.Print sInsert
        
    On Error Resume Next
    moDBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        moReportObj.ReportError kmsgProc, sInsert
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    gClearSotaErr

    bInsertPaymentKeys = True
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bInsertPaymentKeys", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function bInsertPendMiscCashKeys() As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***************************************************************************************
'      Desc:    bInsertPendMiscCashKeys - Builds and executes SQL statement to insert
'               (1) Pending CashTranKey fields within tcmPendCashTran for Misc Cash in CM
'               (2) Pending CashRcptDetlKey fields within tcmCashRcptDetl for Misc Cash in AR
'   Returns:    True - Success
'               False - Failure
'***************************************************************************************
    
    Dim sWhereClause As String
    Dim sTablesUsed As String
    Dim iNumTablesUsed As String
    Dim sSelect As String
    Dim sInsert As String
    
    bInsertPendMiscCashKeys = False
    
    '(1) Insert Pending CashTranKey fields within tcmPendCashTran for Misc Cash in CM
    sWhereClause = msGridWhereClause
    sTablesUsed = msTablesUsed
    iNumTablesUsed = miNumTablesUsed

    moSelectObj.AppendToWhereClause sWhereClause, "tciBatchLog.SourceCompanyID = " & gsQuoted(msCompanyID)
    moSelectObj.AppendToWhereClause sWhereClause, "tcmPendCashTran.BatchKey = tciBatchLog.BatchKey"
    moSelectObj.AppendToWhereClause sWhereClause, "tcmPendCashTran.TranDate <= " & gsQuoted(gsFormatDateToDB(msDate6))
    
    'Include all CM transactions except beginning balance
    moSelectObj.AppendToWhereClause sWhereClause, "tcmPendCashTran.TranType <> 910"
    moSelectObj.AppendToWhereClause sWhereClause, "tciBatchLog.BatchType IN (901,903)"
    
    If InStr(sWhereClause, "tcmCashAcct") <> 0 Then
        moSelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tcmCashAcct", "tcmPendCashTran.CashAcctKey = tcmCashAcct.CashAcctKey"
    End If
    
    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
    If Not moSelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
        Exit Function
    End If

    sSelect = "SELECT DISTINCT " & mlSessionID & ", " & kCashMgmt & ", " & kMiscCashCM & _
        ", tcmPendCashTran.CashTranKey FROM tcmPendCashTran WITH (NOLOCK), tciBatchLog WITH (NOLOCK)"
    
    If InStr(sWhereClause, "tcmCashAcct") <> 0 Then sSelect = sSelect & ", " & sTablesUsed
    sSelect = sSelect & " " & sWhereClause
    
    sInsert = "INSERT INTO tcmCashFlowRptWrk(SessionID, Category, DocType, DocKey) " & sSelect
    
    Debug.Print sInsert
    
    On Error Resume Next
    moDBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        moReportObj.ReportError kmsgProc, sInsert
        Exit Function
    End If
    
    gClearSotaErr
    
    '(2) Insert Pending CashRcptDetlKey fields within tcmCashRcptDetl for Misc Cash in AR
    sWhereClause = msGridWhereClause
    sTablesUsed = msTablesUsed
    iNumTablesUsed = miNumTablesUsed
    
    moSelectObj.AppendToWhereClause sWhereClause, "tciBatchLog.SourceCompanyID = " & gsQuoted(msCompanyID)
    moSelectObj.AppendToWhereClause sWhereClause, "tcmCashRcptDetl.SourceBatchKey = tciBatchLog.BatchKey"
    moSelectObj.AppendToWhereClause sWhereClause, "tcmCashRcptDetl.CustPmtKey = tarPendCustPmt.CustPmtKey"
    moSelectObj.AppendToWhereClause sWhereClause, "tarPendCustPmt.TranDate <= " & gsQuoted(gsFormatDateToDB(msDate6))
    moSelectObj.AppendToWhereClause sWhereClause, "tcmCashRcptDetl.OffsetAcctKey IS NOT NULL"
    moSelectObj.AppendToWhereClause sWhereClause, "tarPendCustPmt.TranType IN (511,514)"
    
    If InStr(sWhereClause, "tcmCashAcct") <> 0 Then
        moSelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tcmCashAcct", "tcmCashRcptDetl.SourceBatchKey = tarBatch.BatchKey"
        moSelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tcmCashAcct", "tarBatch.CashAcctKey = tcmCashAcct.CashAcctKey"
    End If
    
    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
    If Not moSelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
        Exit Function
    End If
    
    sSelect = "SELECT DISTINCT " & mlSessionID & ", " & kCashMgmt & ", " & kMiscCashAR & _
        ", tcmCashRcptDetl.CashRcptDetlKey FROM tcmCashRcptDetl WITH (NOLOCK), tciBatchLog WITH (NOLOCK), tarPendCustPmt WITH (NOLOCK)"
    
    If InStr(sWhereClause, "tcmCashAcct") <> 0 Then sSelect = sSelect & ", tarBatch WITH (NOLOCK), " & sTablesUsed
    sSelect = sSelect & " " & sWhereClause
    
    sInsert = "INSERT INTO tcmCashFlowRptWrk(SessionID, Category, DocType, DocKey) " & sSelect
    
    Debug.Print sInsert
    
    On Error Resume Next
    moDBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        moReportObj.ReportError kmsgProc, sInsert
        Exit Function
    End If
    
    gClearSotaErr
    
    bInsertPendMiscCashKeys = True
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bInsertPendMiscCashKeys", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function bInsertPostedMiscCashKeys() As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***************************************************************************************
'      Desc:    bInsertPostedMiscCashKeys - Builds and executes SQL statement to insert
'               (1) Posted CashTranKey fields within tcmCashTran for Misc Cash in CM
'               (2) Posted CashRcptDetlKey fields within tcmCashRcptDetl for Misc Cash in AR
'   Returns:    True - Success
'               False - Failure
'***************************************************************************************
    
    Dim sWhereClause As String
    Dim sTablesUsed As String
    Dim iNumTablesUsed As String
    Dim sSelect As String
    Dim sInsert As String
    
    bInsertPostedMiscCashKeys = False
    
    '(1) Insert Posted CashTranKey fields within tcmCashTran for Misc Cash in CM
    sWhereClause = msGridWhereClause
    sTablesUsed = msTablesUsed
    iNumTablesUsed = miNumTablesUsed

    moSelectObj.AppendToWhereClause sWhereClause, "tciBatchLog.SourceCompanyID = " & gsQuoted(msCompanyID)
    moSelectObj.AppendToWhereClause sWhereClause, "tcmCashTran.BatchKey = tciBatchLog.BatchKey"
    moSelectObj.AppendToWhereClause sWhereClause, "tcmCashTran.TranDate <= " & gsQuoted(gsFormatDateToDB(msDate6))
    
    'Include all CM transactions except beginning balance
    moSelectObj.AppendToWhereClause sWhereClause, "tcmCashTran.TranType <> 910"
    moSelectObj.AppendToWhereClause sWhereClause, "tciBatchLog.BatchType IN (901,903)"
    
    If InStr(sWhereClause, "tcmCashAcct") <> 0 Then
        moSelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tcmCashAcct", "tcmCashTran.CashAcctKey = tcmCashAcct.CashAcctKey"
    End If
    
    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
    If Not moSelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
        Exit Function
    End If

    sSelect = "SELECT DISTINCT " & mlSessionID & ", " & kCashMgmt & ", " & kMiscCashCM & _
        ", tcmCashTran.CashTranKey FROM tcmCashTran WITH (NOLOCK), tciBatchLog WITH (NOLOCK)"
    
    If InStr(sWhereClause, "tcmCashAcct") <> 0 Then sSelect = sSelect & ", " & sTablesUsed
    sSelect = sSelect & " " & sWhereClause
    
    sInsert = "INSERT INTO tcmCashFlowRptWrk(SessionID, Category, DocType, DocKey) " & sSelect
        
    Debug.Print sInsert
    
    On Error Resume Next
    moDBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        moReportObj.ReportError kmsgProc, sInsert
        Exit Function
    End If
    
    gClearSotaErr
    
    '(2) Insert Posted CashRcptDetlKey fields within tcmCashRcptDetl for Misc Cash in AR
    sWhereClause = msGridWhereClause
    sTablesUsed = msTablesUsed
    iNumTablesUsed = miNumTablesUsed
    
    moSelectObj.AppendToWhereClause sWhereClause, "tarCustPmtLog.CompanyID = " & gsQuoted(msCompanyID)
    moSelectObj.AppendToWhereClause sWhereClause, "tcmCashRcptDetl.CustPmtKey = tarCustPmtLog.CustPmtKey"
    moSelectObj.AppendToWhereClause sWhereClause, "tarCustPmtLog.TranDate <= " & gsQuoted(gsFormatDateToDB(msDate6))
    moSelectObj.AppendToWhereClause sWhereClause, "tcmCashRcptDetl.OffsetAcctKey IS NOT NULL"
    moSelectObj.AppendToWhereClause sWhereClause, "tarCustPmtLog.TranType IN (511,514)"
    moSelectObj.AppendToWhereClause sWhereClause, "tarCustPmtLog.TranStatus = 3" 'Posted TranStatus
    
    If InStr(sWhereClause, "tcmCashAcct") <> 0 Then
        moSelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tcmCashAcct", "tcmCashRcptDetl.SourceBatchKey = tcmCashRcptSummary.SourceBatchKey"
        moSelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tcmCashAcct", "tcmCashRcptSummary.CashAcctKey = tcmCashAcct.CashAcctKey"
    End If

    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
    If Not moSelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
        Exit Function
    End If
    
    sSelect = "SELECT DISTINCT " & mlSessionID & ", " & kCashMgmt & ", " & kMiscCashAR & _
        ", tcmCashRcptDetl.CashRcptDetlKey FROM tcmCashRcptDetl WITH (NOLOCK), tarCustPmtLog WITH (NOLOCK)"
    
    If InStr(sWhereClause, "tcmCashAcct") <> 0 Then sSelect = sSelect & ", tcmCashRcptSummary WITH (NOLOCK), " & sTablesUsed
    sSelect = sSelect & " " & sWhereClause
    
    sInsert = "INSERT INTO tcmCashFlowRptWrk(SessionID, Category, DocType, DocKey) " & sSelect
    
    Debug.Print sInsert
    
    On Error Resume Next
    moDBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        moReportObj.ReportError kmsgProc, sInsert
        Exit Function
    End If
    
    gClearSotaErr
    
    bInsertPostedMiscCashKeys = True
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bInsertPostedMiscCashKeys", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function


Public Function bInsertFutureCashFlowKeys() As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***************************************************************************************
'      Desc:    bInsertFutureCashFlowKeys - Builds and executes SQL statement to insert
'                   future cash flow keys into the work table.
'     Parms:    none
'   Returns:    True - Success
'               False - Failure
'***************************************************************************************
        
    Dim sWhereClause As String
    Dim sTablesUsed As String
    Dim iNumTablesUsed As String
    Dim sSelect As String
    Dim sInsert As String
    
    bInsertFutureCashFlowKeys = False
    
    sWhereClause = msGridWhereClause
    sTablesUsed = msTablesUsed
    iNumTablesUsed = miNumTablesUsed
    
    moSelectObj.AppendToWhereClause sWhereClause, "tcmFutureCashFlow.CompanyID='" & msCompanyID & kSQuote
    If InStr(sWhereClause, "tcmCashAcct") <> 0 Then
        moSelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tcmCashAcct", "tcmFutureCashFlow.CashAcctKey=tcmCashAcct.CashAcctKey"
    End If
    moSelectObj.AppendToWhereClause sWhereClause, "tcmFutureCashFlow.Date <= " _
     & gsQuoted(gsFormatDateToDB(msDate6))
        
    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
    If Not moSelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    sSelect = "SELECT DISTINCT " & mlSessionID & ", " & kOther & ", " & kFutureCashFlows & _
        ", tcmFutureCashFlow.FutureCashFlowKey FROM tcmFutureCashFlow"
      
    If InStr(sWhereClause, "tcmCashAcct") <> 0 Then sSelect = sSelect & ", " & sTablesUsed
    sSelect = sSelect & " " & sWhereClause
        
    sInsert = "INSERT INTO tcmCashFlowRptWrk(SessionID, Category, DocType, DocKey) " & sSelect
       
    Debug.Print sInsert
        
    On Error Resume Next
    moDBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        moReportObj.ReportError kmsgProc, sInsert
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    gClearSotaErr
    
    bInsertFutureCashFlowKeys = True
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bInsertFutureCashFlowKeys", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function bInsertPOKeys() As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***************************************************************************************
'      Desc:    bInsertPOKeys - Builds and executes SQL statement to insert
'                   purchase order keys into the work table.
'     Parms:    none
'   Returns:    True - Success
'               False - Failure
'***************************************************************************************
        
    Dim sWhereClause As String
    Dim sTablesUsed As String
    Dim iNumTablesUsed As String
    Dim sSelect As String
    Dim sInsert As String
    
    bInsertPOKeys = False
    
    sWhereClause = msGridWhereClause
    sTablesUsed = msTablesUsed
    iNumTablesUsed = miNumTablesUsed
    
    moSelectObj.AppendToWhereClause sWhereClause, "tpoPurchOrder.CompanyID = " & gsQuoted(msCompanyID)
    moSelectObj.AppendToWhereClause sWhereClause, "tpoPurchOrder.Status = " & kStatusOpen
    
    'NOTE:  Checks on activity date are not performed here -- relies on info in PO line detail table.
    'Handled by stored procedure.
    
    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
    If Not moSelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    If InStr(sWhereClause, "tapVendor") <> 0 Then
        sSelect = sSelect & ", tapVendor, tapVendStatus"
    End If
    
    sSelect = "SELECT DISTINCT " & mlSessionID & ", " & kPayables & ", " & kOpenPurchOrders & _
        ", tpoPurchOrder.POKey FROM tpoPurchOrder"
      
    sSelect = sSelect & " " & sWhereClause
        
    sInsert = "INSERT INTO tcmCashFlowRptWrk(SessionID, Category, DocType, DocKey) " & sSelect
        
    Debug.Print sInsert
        
    On Error Resume Next
    moDBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        moReportObj.ReportError kmsgProc, sInsert
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    gClearSotaErr
    
    bInsertPOKeys = True
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bInsertFutureCashFlowKeys", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Public Function bInsertSOKeys() As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***************************************************************************************
'      Desc:    bInsertSOKeys - Builds and executes SQL statement to insert
'                   sales order keys into the work table.
'     Parms:    none
'   Returns:    True - Success
'               False - Failure
'***************************************************************************************
        
    Dim sWhereClause As String
    Dim sTablesUsed As String
    Dim iNumTablesUsed As String
    Dim sSelect As String
    Dim sInsert As String
    
    bInsertSOKeys = False
    
    sWhereClause = msGridWhereClause
    sTablesUsed = msTablesUsed
    iNumTablesUsed = miNumTablesUsed
    
    moSelectObj.AppendToWhereClause sWhereClause, "tsoSalesOrder.CompanyID = " & gsQuoted(msCompanyID)
    moSelectObj.AppendToWhereClause sWhereClause, "tsoSalesOrder.Status = " & kStatusOpen
    
    'NOTE:  Checks on activity date are not performed here -- relies on info in SO line detail table.
    'Handled by stored procedure.
    
    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
    If Not moSelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    If InStr(sWhereClause, "tarCustomer") <> 0 Then
        sSelect = sSelect & ", tarCustomer, tarCustStatus"
    End If
    
    sSelect = "SELECT DISTINCT " & mlSessionID & ", " & kReceivables & ", " & kOpenSalesOrders & _
        ", tsoSalesOrder.SOKey FROM tsoSalesOrder"
      
    sSelect = sSelect & " " & sWhereClause
        
    sInsert = "INSERT INTO tcmCashFlowRptWrk(SessionID, Category, DocType, DocKey) " & sSelect
        
    Debug.Print sInsert
        
    On Error Resume Next
    moDBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        moReportObj.ReportError kmsgProc, sInsert
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    gClearSotaErr
    
    bInsertSOKeys = True
    Exit Function
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bInsertSOKeys", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Sub Main()
'+++ VB/Rig Skip +++

    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("cmzra001.clsPerCashFlowRpt")
        StartAppInStandaloneMode oApp, Command$()
    End If


    Exit Sub
End Sub
