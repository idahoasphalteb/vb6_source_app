VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{6FBA474E-43AC-11CE-9A0E-00AA0062BB4C}#1.0#0"; "SYSINFO.OCX"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Begin VB.Form frmRegister2 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Register/Post"
   ClientHeight    =   7110
   ClientLeft      =   1335
   ClientTop       =   1275
   ClientWidth     =   9315
   HelpContextID   =   6228
   Icon            =   "cmzjadl1.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7110
   ScaleWidth      =   9315
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   6232
      Width           =   9315
      _ExtentX        =   16431
      _ExtentY        =   741
      Style           =   0
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   2
      Top             =   0
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   26
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   27
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   28
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   29
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   30
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Frame fraMessage 
      Caption         =   "&Message"
      Height          =   1695
      Left            =   60
      TabIndex        =   15
      Top             =   4920
      Width           =   9195
      Begin VB.PictureBox pctMessage 
         Height          =   1320
         Left            =   135
         ScaleHeight     =   1260
         ScaleWidth      =   8775
         TabIndex        =   24
         Top             =   270
         Width           =   8835
         Begin VB.TextBox txtMessageHeader 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   240
            Index           =   0
            Left            =   0
            MaxLength       =   50
            TabIndex        =   16
            Top             =   0
            WhatsThisHelpID =   18
            Width           =   8770
         End
         Begin VB.TextBox txtMessageHeader 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   240
            Index           =   1
            Left            =   0
            MaxLength       =   50
            TabIndex        =   17
            Top             =   255
            WhatsThisHelpID =   18
            Width           =   8770
         End
         Begin VB.TextBox txtMessageHeader 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   240
            Index           =   2
            Left            =   0
            MaxLength       =   50
            TabIndex        =   18
            Top             =   510
            WhatsThisHelpID =   18
            Width           =   8770
         End
         Begin VB.TextBox txtMessageHeader 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   240
            Index           =   3
            Left            =   0
            MaxLength       =   50
            TabIndex        =   19
            Top             =   765
            WhatsThisHelpID =   18
            Width           =   8770
         End
         Begin VB.TextBox txtMessageHeader 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   240
            Index           =   4
            Left            =   0
            MaxLength       =   50
            TabIndex        =   20
            Top             =   1020
            WhatsThisHelpID =   18
            Width           =   8770
         End
         Begin VB.Line Line1 
            Index           =   3
            X1              =   0
            X2              =   8775
            Y1              =   1010
            Y2              =   1010
         End
         Begin VB.Line Line1 
            Index           =   2
            X1              =   0
            X2              =   8775
            Y1              =   750
            Y2              =   750
         End
         Begin VB.Line Line1 
            Index           =   1
            X1              =   0
            X2              =   8775
            Y1              =   500
            Y2              =   500
         End
         Begin VB.Line Line1 
            Index           =   0
            X1              =   0
            X2              =   8775
            Y1              =   240
            Y2              =   240
         End
      End
   End
   Begin VB.Frame fraSort 
      Caption         =   "&Sort"
      Enabled         =   0   'False
      Height          =   1905
      Left            =   60
      TabIndex        =   13
      Top             =   2880
      Width           =   9195
      Begin FPSpreadADO.fpSpread grdSort 
         Height          =   1545
         Left            =   90
         TabIndex        =   14
         Top             =   270
         WhatsThisHelpID =   25
         Width           =   9015
         _Version        =   524288
         _ExtentX        =   15901
         _ExtentY        =   2725
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   1
         MaxRows         =   1
         SpreadDesigner  =   "cmzjadl1.frx":23D2
         AppearanceStyle =   0
      End
   End
   Begin VB.Frame frmRegOption 
      Caption         =   "Register &Options"
      Height          =   1815
      Left            =   4590
      TabIndex        =   10
      Top             =   930
      Width           =   4665
      Begin VB.CheckBox chkOutStdTrans 
         Caption         =   "Show Outstanding Bank Transactions"
         Height          =   285
         Left            =   210
         TabIndex        =   12
         Top             =   660
         WhatsThisHelpID =   6251
         Width           =   3345
      End
      Begin VB.ComboBox cboFormat 
         Height          =   315
         Left            =   1020
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   300
         WhatsThisHelpID =   6250
         Width           =   2475
      End
      Begin VB.Label lblFormat 
         Caption         =   "Format"
         Height          =   195
         Left            =   210
         TabIndex        =   9
         Top             =   330
         Width           =   675
      End
   End
   Begin VB.Frame frmPrintPost 
      Caption         =   "&Print/Post"
      Height          =   1815
      Left            =   60
      TabIndex        =   5
      Top             =   930
      Width           =   4425
      Begin VB.ComboBox cboOutput 
         Height          =   315
         Left            =   1170
         Style           =   2  'Dropdown List
         TabIndex        =   21
         Top             =   600
         WhatsThisHelpID =   6245
         Width           =   1995
      End
      Begin VB.CheckBox chkConfirm 
         Caption         =   "Confirm Before Posting"
         Height          =   285
         Left            =   480
         TabIndex        =   8
         Top             =   1290
         Value           =   1  'Checked
         WhatsThisHelpID =   6247
         Width           =   2785
      End
      Begin VB.CheckBox chkPost 
         Caption         =   "Post"
         Height          =   285
         Left            =   180
         TabIndex        =   7
         Top             =   930
         Value           =   1  'Checked
         WhatsThisHelpID =   6246
         Width           =   1125
      End
      Begin VB.CheckBox chkRegister 
         Caption         =   "Register"
         Height          =   285
         Left            =   180
         TabIndex        =   6
         Top             =   270
         Value           =   1  'Checked
         WhatsThisHelpID =   6244
         Width           =   1515
      End
      Begin VB.Label lblOutput 
         Caption         =   "Output"
         Height          =   255
         Left            =   480
         TabIndex        =   22
         Top             =   600
         Width           =   555
      End
   End
   Begin VB.CheckBox chkSummary 
      Caption         =   "Print &Report Settings"
      Height          =   285
      Left            =   4590
      TabIndex        =   4
      Top             =   540
      WhatsThisHelpID =   31
      Width           =   3225
   End
   Begin VB.ComboBox cboReportSettings 
      Height          =   315
      Left            =   1020
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   510
      WhatsThisHelpID =   23
      Width           =   3465
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   31
      Top             =   4320
      Visible         =   0   'False
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   32
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   33
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   34
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   25
      Top             =   645
      Visible         =   0   'False
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   10170
      Top             =   1200
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComDlg.CommonDialog dlgCreateRPTFile 
      Left            =   10200
      Top             =   1785
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin SysInfoLib.SysInfo sysReports 
      Left            =   10170
      Top             =   570
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   6720
      WhatsThisHelpID =   1
      Width           =   9315
      _ExtentX        =   0
      _ExtentY        =   688
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   35
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Label lblStatusMsg 
      Caption         =   "Pending"
      Height          =   375
      Left            =   9420
      TabIndex        =   0
      Top             =   3150
      Width           =   975
   End
   Begin VB.Label lblSetting 
      Caption         =   "S&etting"
      Height          =   195
      Left            =   60
      TabIndex        =   1
      Top             =   570
      Width           =   795
   End
End
Attribute VB_Name = "frmRegister2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'***********************************************************************
'     Name: frmRegister2
'     Desc: Post Transactions
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: 11/22/95 LLJ
'     Mods: {date&Inits};
'***********************************************************************
Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

'Context Menu Object
    Public moContextMenu        As clsContextMenu

'Private variables of Form Properties
    Private moClass             As Object
    Private mlRunMode           As Long
    Private mbCancelShutDown    As Boolean
    Private mbEnterAsTab        As Boolean
    Private mlRetValue          As Long
    Private mlHomeCurrency      As String
    Private mbunload            As Boolean
    Private miPreview           As Integer
    Private mdBusinessDate      As Date
    Private miCommExpOpt        As Integer
    Private miUseSper           As Integer

'Private variables for Report Engine
    Private mbLoading               As Boolean
    Private mbLoadSuccess           As Boolean

'Public Form Variables
    Public msCMReportPath       As String
    Public msGLReportPath       As String
    Public msBatchID            As String
    Public msPrintButton        As String
    Public moSotaObjects        As New Collection
    Public mlBatchKey           As Long
    Public miBatchType          As Integer
    Public mlTaskNumber         As Long
    Public mlPostDate           As Date
    Public miErrorsOccured      As Integer
    Public mbPostOnly           As Boolean


'variables used in posting multiple batches
    Public miMode               As Integer  '1 = default, show form, 0 = posting multiple batches, no UI
    Private miPrintFlag         As Integer
    Private miPostFlag          As Integer
    Public miUseMultCurr        As Integer
    Public miGLPostRgstrFormat  As Integer

'variables for Report Engine
    Public moRpt                As clsReport
    Public moReport             As clsReportEngine
    Public moSettings           As clsSettings
    Public moSort               As clsSort
    Public moDDData             As clsDDData
    Public moOptions            As clsOptions
    Public moPrinter            As Printer
    Public sRealTableCollection As Collection
    Public sWorkTableCollection As Collection
 
'Private form variables
    Private msCompanyID         As String
    Private miFullGL            As Integer
    Private miSessionIDItem     As Integer
    Private moRegProcess        As Object   'clsRegisterPRC
    Private msReportName        As String
    Private msWorkTable         As String
    Private msSPCreateTable     As String
    Private msSPDeleteTable     As String
    Private msSPModulePost      As String
    Private msBatchType         As String
    Private guLocalizedStrings  As uLocalizedStrings
    Private mbRegChecked        As Boolean
    Private mbRegPrinted        As Boolean
    Private muRegStat           As RegStatus
    Private mFiscPer            As Integer
    Private mFiscYear           As String
    Private mPostError          As Integer
    Private mbSkippedToPost     As Boolean
    Private mlLockID            As Long
    Private mbFormLoad          As Boolean
    Private mbLoadSettings      As Boolean
    Private mbPostingAllowed    As Boolean
    Private mbUserIsOwner        As Boolean

'Options constants
    Private Const kPostXXBatches        As Integer = 0
    Private Const kInteractiveBatch     As Integer = 1
    Private Const kNoErrors             As Integer = 0
    Private Const kFatalErrors          As Integer = 1
    Private Const kWarnings             As Integer = 2

'Format indices
    Private miSummary                   As Integer
    Private miDetail                    As Integer
    
'Task Number constants
    Private Const ktskCMRecReg              As Long = 151715940
    Private Const ktskCMDepReg              As Long = 151715990
    Private Const ktskCMTranReg             As Long = 151716040
   
'Private Constant
    Private Const kVClosedPer = 151418
    Private Const kVNonPer = 151434
    Private Const kVCmCantPostGL = 150542
    Private Const kmsgModPIncomp = 153507
    Private Const BACKSLASH = "\"

    Private cypHelper       As Object   'SGS-JMM 2/10/2011

    Const VBRIG_MODULE_ID_STRING = "CMZJADL1.FRM"

Public Property Let bunload(bnewvalue As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbunload = bnewvalue
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bunload_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get bunload() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bunload = mbunload
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bunload_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Public Property Let UserIsOwner(ByVal bnewvalue As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iSecuritylevel As Integer
    
    mbUserIsOwner = bnewvalue
    
    iSecuritylevel = giGetSecurityLevel(moClass.moFramework, moClass.moFramework.GetTaskID)
    
    If iSecuritylevel = sotaTB_SUPERVISORY Or (iSecuritylevel = sotaTB_NORMAL And mbUserIsOwner) Then
        mbPostingAllowed = True
    Else
        mbPostingAllowed = False
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "UserIsOwner_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get UserIsOwner() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    UserIsOwner = mbUserIsOwner
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "UserIsOwner_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let miPrint(ival As Integer)
    miPrintFlag = ival
    If miPrintFlag = 1 Then
        chkRegister = vbChecked
    Else
        chkRegister = vbUnchecked
    End If
End Property

Public Property Let miPost(ival As Integer)
    miPostFlag = ival
    If miPostFlag = 1 Then
        chkPost = vbChecked
    Else
        chkPost = vbUnchecked
    End If
End Property


Private Sub cboFormat_Change()
    Debug.Print "LIST INDEX: ", cboFormat.ListIndex
End Sub

Private Sub cboFormat_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboFormat, True
    #End If
'+++ End Customizer Code Push +++
    Dim bResetSort      As Boolean

    SetupWorkTables
    bResetSort = False

    If miBatchType = BATCH_TYPE_CM Then
        bResetSort = True
    End If

    If miBatchType = BATCH_TYPE_RE Then
        If cboFormat.ListIndex = miSummary Then
            grdSort.Enabled = False
            fraSort.Enabled = False
            bResetSort = True
            chkOutStdTrans.Enabled = False
        Else
            grdSort.Enabled = True
            fraSort.Enabled = True
            chkOutStdTrans.Enabled = True
            Set moSort = Nothing
            Set moSort = New clsSort
            moSort.lInitSort Me, moDDData
            moSettings.Initialize Me, cboReportSettings, moSort, , moOptions, False, tbrMain, sbrMain
            If mbLoadSettings = True Then
                moSettings.bLoadReportSettings
                mbLoadSettings = False
            End If
        End If
    End If

    If bResetSort Then
        Set moSort = Nothing
        Set moSort = New clsSort
        grdSort.MaxRows = 0
        moSort.lInitSort Me, moDDData
        If mlTaskNumber = ktskCMTranReg Then
            '-- Remove Batch from sort list
            moSort.lDeleteSort "BatchID", "tciBatchLog"
        End If
        If miBatchType = BATCH_TYPE_RE Then
            If mbLoadSettings = True And cboReportSettings <> "" Then
                moSettings.Initialize Me, cboReportSettings, moSort, , moOptions, False, tbrMain, sbrMain
                moSettings.bLoadReportSettings
                mbLoadSettings = False
            End If
        End If
    End If
    
    

End Sub



Private Sub cboReportSettings_KeyPress(KeyAscii As Integer)
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboReportSettings, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
'+++ VB/Rig Skip +++
    If Len(cboReportSettings.Text) > 40 Then
        Beep
        KeyAscii = 0
    End If
End Sub

Private Sub cboReportSettings_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboReportSettings, True
    #End If
'+++ End Customizer Code Push +++
    mbLoadSettings = True
    moSettings.bLoadReportSettings
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboReportSettings_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub chkPost_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkPost, True
    #End If
'+++ End Customizer Code Push +++
    If chkPost = 0 Then
        chkConfirm.Enabled = False
    Else
        chkConfirm.Enabled = True
    End If
End Sub

Private Sub chkRegister_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkRegister, True
    #End If
'+++ End Customizer Code Push +++
    
    If chkRegister = 0 Then
        cboOutput.Enabled = False
        If (iCheckRegisterPrinted(Me)) = 0 Then
            EnablePost False
        Else
            EnablePost True
        End If
    Else
        cboOutput.Enabled = True
        EnablePost True
    End If
    
End Sub

'************************ Sort Stuff *******************************
Private Sub grdSort_Click(ByVal Col As Long, ByVal Row As Long)
End Sub

Private Sub grdSort_KeyUp(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSort.GridKey keycode, Shift
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_KeyUp", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSort_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSort.GridChange Col, Row
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub GrdSort_GotFocus()
    If Not mbLoading Then moSort.SortGridGotFocus
End Sub
Private Sub GrdSort_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
    If Not mbLoading Then moSort.GridLeaveCell Col, Row, NewCol, NewRow
End Sub

Private Sub GrdSort_KeyDown(keycode As Integer, Shift As Integer)
    If Not mbLoading Then moSort.SortGridKeyDown keycode, Shift
End Sub

Private Sub grdSort_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSort.GridButtonClicked Col, Row, ButtonDown
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_ButtonClicked", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
'*********************** End Sort Stuff *****************************

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbCancelShutDown = False
    miErrorsOccured = 0
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Select Case keycode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, keycode, Shift
        Case Else
    End Select
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        Case Else
    End Select
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Function lInitializeReport(ModuleID As String, DefaultReport As String, ProjectName As String) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim iFound, i As Integer
Dim lRetVal As Long

    lInitializeReport = -1
    
    Set moPrinter = Printer
    Set moSotaObjects = New Collection
    'Set moRpt = New clsReport
    If moReport Is Nothing Then     '   HBS
        Set moReport = New clsReportEngine
    End If
    
    Set moSettings = New clsSettings
    Set moSort = New clsSort
    Set moDDData = New clsDDData
    Set moOptions = New clsOptions

    moReport.UI = True
    
    If Not moDDData.lInitDDData(sRealTableCollection, moClass.moAppDB, moClass.moAppDB, msCompanyID) = kSuccess Then
        lInitializeReport = kmsgCantLoadDDData
        Exit Function
    End If
    
    If Not moDDData.lDDBuildData(App.EXEName) = kSuccess Then
        lInitializeReport = kmsgCantLoadDDData
        Exit Function
    End If
    
    moReport.AppOrSysDB = kAppDB
    
    If (moReport.lInitReport(ModuleID, DefaultReport, Me, moDDData) = kFailure) Then
        lInitializeReport = kmsgFatalReportInit
        Exit Function
    End If

    If (moSort.lInitSort(Me, moDDData) = kFailure) Then
        lInitializeReport = kmsgFataSortInit
        Exit Function
    End If

    If mlTaskNumber = ktskCMRecReg Then
        iFound = 0
        For i = 1 To moDDData.lMaxDDRows
            moDDData.DDRow() = i
            
            If (moDDData.sTableNameDD = "tcmCashTran") And (moDDData.sColumnNameDD = "Status") Then
                gsStatus = moDDData.sCaptionDD
                iFound = iFound + 1
            ElseIf (moDDData.sTableNameDD = "tcmCashTran") And (moDDData.sColumnNameDD = "TranType") Then
                gsTranType = moDDData.sCaptionDD
                iFound = iFound + 1
            End If
            
            If iFound = 2 Then Exit For
        Next i
    ElseIf mlTaskNumber = ktskCMTranReg Then
        iFound = 0
        For i = 1 To moDDData.lMaxDDRows
            moDDData.DDRow() = i
            
            If (moDDData.sTableNameDD = "tcmPendCashTran") And (moDDData.sColumnNameDD = "TranType") Then
                gsTranType = moDDData.sCaptionDD
                iFound = iFound + 1
            End If
            
            If iFound = 1 Then Exit For
        Next i
        
        '-- Remove Batch from sort list
        moSort.lDeleteSort "BatchID", "tciBatchLog"
    End If
    
    lInitializeReport = 0
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lInitializeReport", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim aniPos As Long
    Dim lRetVal As Long

  Set sbrMain.Framework = moClass.moFramework
  sbrMain.BrowseVisible = False


    msCompanyID = moClass.moSysSession.CompanyID
    miFullGL = moClass.moSysSession.FullGL
    mbEnterAsTab = moClass.moSysSession.EnterAsTab
    mdBusinessDate = moClass.moSysSession.BusinessDate
    mPostError = 0
    mlTaskNumber = moClass.moFramework.GetTaskID()
    
    tbrMain.Build sotaTB_REGISTER
    tbrMain.AddSeparator 4
    tbrMain.AddButton kTbSave, 5
    tbrMain.AddButton kTbDelete, 6
    
    mbFormLoad = True
    cboFormat.AddItem gsBuildString(kARZJA001Summary, moClass.moAppDB, moClass.moSysSession)
    miSummary = cboFormat.NewIndex
    cboFormat.AddItem gsBuildString(kARZJA001Detail, moClass.moAppDB, moClass.moSysSession)
    miDetail = cboFormat.NewIndex
    cboFormat.ListIndex = miSummary
    chkOutStdTrans.Enabled = False
    mbFormLoad = False

    If lSetBatchType(True) = kFailure Then
        ' error
    End If
    
    GetModVariables Me
    GetHomeCurrency
    
    BuildLocalizedStrings moClass.moAppDB, moClass.moSysSession, guLocalizedStrings

    cboOutput.AddItem "Printer"
    cboOutput.AddItem "Screen"
    cboOutput.ListIndex = 0
    
    If mlTaskNumber <> ktskCMRecReg Then
        chkOutStdTrans.Visible = False
    End If
    
    With moOptions
        .Add chkRegister, chkRegister.Caption
        .Add cboOutput, lblOutput
        .Add chkPost
        .Add chkConfirm

        .Add cboFormat, lblFormat
        .Add chkSummary
        
        If mlTaskNumber = ktskCMRecReg Then
            .Add chkOutStdTrans
        End If
    End With
    
    ' initialize the report setting combo
    moSettings.Initialize Me, cboReportSettings, moSort, , moOptions, , tbrMain, sbrMain
    
    If mlTaskNumber = ktskCMRecReg Then
        If cboFormat.ListIndex <> miSummary Then
            grdSort.Enabled = True
            fraSort.Enabled = True
            chkOutStdTrans.Enabled = True
            Set moSort = Nothing
            Set moSort = New clsSort
            moSort.lInitSort Me, moDDData
            moSettings.Initialize Me, cboReportSettings, moSort, , moOptions, False, tbrMain, sbrMain
            moSettings.bLoadReportSettings
        End If
        If cboFormat.ListIndex <> miSummary And chkOutStdTrans = 1 Then
            moSort.lInsSort "Status", "tcmRecRegDtlWrk", moDDData, "Status"
        End If
    End If

'    miUseSper = iCheckUseSper(Me)
    msBatchID = sGetBatchID(Me)
    GetReportPaths
        
    Set moContextMenu = New clsContextMenu
    With moContextMenu
        ' **PRESTO ** Set .Hook = Me.WinHook2
        Set .Form = frmRegister2
        .Init
    End With
    
    sbrMain.Status = SOTA_SB_START
    
    'SGS-JMM 1/31/2011
    On Error Resume Next
    Set cypHelper = CreateObject("CypressIntegrationHelper.MAS500Form")
    If Err.Number = 0 Then
        With cypHelper
            .Init Me, moClass, msCompanyID
            .AddToolbarButton 0
        End With
    Else
        Set cypHelper = Nothing
    End If
    On Error GoTo VBRigErrorRoutine
    'End SGS-JMM
        
    Exit Sub
    
badexit:

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetupWorkTables()
    Dim iCount      As Integer
    
    If mbFormLoad Then Exit Sub

    For iCount = 1 To sWorkTableCollection.Count
        sWorkTableCollection.Remove 1
    Next iCount
    
    If miBatchType = BATCH_TYPE_RE Then
        If cboFormat.ListIndex = miSummary Then
            sWorkTableCollection.Add "tcmRecRegSumWrk"
            sWorkTableCollection.Add "tcmRecRegDtlWrk"
        Else
            sWorkTableCollection.Add "tcmRecRegDtlWrk"
            sWorkTableCollection.Add "tcmRecRegSumWrk"
        End If
    ElseIf miBatchType = BATCH_TYPE_CM Then
        If cboFormat.ListIndex = miSummary Then
            sWorkTableCollection.Add "tcmTrnRegSumWrk"
            sWorkTableCollection.Add "tcmTrnRegDtlWrk"
        Else
            sWorkTableCollection.Add "tcmTrnRegDtlWrk"
            sWorkTableCollection.Add "tcmTrnRegSumWrk"
        End If
    Else
        sWorkTableCollection.Add "tcmDepRegWrk"
    End If

End Sub




Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
    
    
    If gbActiveChildForms(Me) Then GoTo CancelShutDown

    '
   If Not moReport.bShutdownEngine Then
        Cancel = 1
        GoTo CancelShutDown
    End If
    
    cleanErrorLog
    moReport.CleanupWorkTables

    'Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False
    
    'Determine how we shutdown the form when the user as requested it or the form code
    'has requested it.
    If moClass.mlError = 0 Then

        moClass.lUIActive = kChildObjectInactive
        
        
        Select Case UnloadMode
            Case vbFormCode
                ' do nothing
            Case Else
                'User pressed close button
                moClass.miShutDownRequester = kUnloadSelfShutDown
        End Select

        
    End If
        
    PerformCleanShutDown

    'SGS-JMM 1/31/2011
    On Error Resume Next
    If Not (cypHelper Is Nothing) Then cypHelper.Dispose
    Set cypHelper = Nothing
    On Error GoTo VBRigErrorRoutine
    'End SGS-JMM
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
CancelShutDown:
        
        moClass.miShutDownRequester = kFrameworkShutDown
        mbCancelShutDown = True
        Cancel = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Set moClass.mfrmMain = Nothing
    Set moClass = Nothing
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lRunMode = mlRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlRunMode = lNewRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolBarClick Button
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ProcessRegister(ltaskNumber As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim lRetVal As Long
Dim oSessionID As Long
Dim iMsgReturn As Integer
Dim lReturnCode As Long
Dim sSortBy As String
Dim sFormulas() As String
Dim iLoop As Integer
Dim prn As Integer
Dim sRegSP As String
Dim ReportCondition   As New Collection
Dim mcSPInCreateTable   As New Collection
Dim mcSPOutCreateTable  As New Collection
Dim mcSPInDeleteTable As New Collection
Dim mcSPOutDeleteTable As New Collection
Dim sortNum As Integer

ReDim sFormulas(0, 0)

    sbrMain.Status = SOTA_SB_BUSY
            
    'create work table to print register from
    Screen.MousePointer = vbHourglass
                            
    Select Case mlTaskNumber
        Case ktskCMRecReg
            mcSPInCreateTable.Add mlBatchKey
                
            Select Case cboFormat.ListIndex
                Case -1, miSummary
                    mcSPInCreateTable.Add 0 'summary
                Case miDetail
                    mcSPInCreateTable.Add 1 'detail
            End Select
                
            mcSPInCreateTable.Add chkOutStdTrans.Value
            mcSPInCreateTable.Add msCompanyID
            mcSPInCreateTable.Add moReport.SessionID
            mcSPOutCreateTable.Add mlRetValue
                
            sRegSP = "spCmRecReg"
        Case ktskCMTranReg
            mcSPInCreateTable.Add mlBatchKey
                
            Select Case cboFormat.ListIndex
                Case -1, miSummary
                    mcSPInCreateTable.Add 0 'summary
                Case miDetail
                    mcSPInCreateTable.Add 1 'detail
            End Select
                
            mcSPInCreateTable.Add msCompanyID
            mcSPInCreateTable.Add moReport.SessionID
            mcSPOutCreateTable.Add mlRetValue
                
            sRegSP = "spCmTrnReg"
        Case ktskCMDepReg
            mcSPInCreateTable.Add mlBatchKey
                
            Select Case cboFormat.ListIndex
                Case -1, miSummary
                    mcSPInCreateTable.Add 0 'summary
                Case miDetail
                    mcSPInCreateTable.Add 1 'detail
            End Select
                
            mcSPInCreateTable.Add msCompanyID
            mcSPInCreateTable.Add moReport.SessionID
            mcSPOutCreateTable.Add mlRetValue
                
            sRegSP = "spCmDepReg"
    End Select
                            
    If miMode <> kPostXXBatches Then
        sbrMain.Message = guLocalizedStrings.ProcessingRegister
    Else
        frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.ProcessingRegister
    End If
        
    'process register
    lRetVal = lExecSP(sRegSP, mcSPInCreateTable, mcSPOutCreateTable)
            
    Screen.MousePointer = vbArrow
                
    If miMode <> kPostXXBatches Then
        sbrMain.Message = guLocalizedStrings.PrintingRegister
    Else
        frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.PrintingRegister
    End If
            
    If cboFormat.ListIndex = miSummary Then
        StartRecRegSum msPrintButton, Me
        moReport.CascadePreview
    Else
        If mlTaskNumber = ktskCMRecReg Then
            StartRecRegSum msPrintButton, Me
            moReport.CascadePreview
        End If
                
        StartRecRegDtl msPrintButton, Me
        moReport.CascadePreview
    End If
        
    If lRetVal <> 0 Then
        sbrMain.Status = SOTA_SB_START
        Screen.MousePointer = vbArrow
        moClass.lUIActive = kChildObjectInactive
        Set ReportCondition = Nothing
        Set mcSPInCreateTable = Nothing
        Set mcSPOutCreateTable = Nothing
        Set mcSPInDeleteTable = Nothing
        Set mcSPOutDeleteTable = Nothing
            
        Exit Sub
    End If
        
    If miMode <> kPostXXBatches Then
        sbrMain.Message = guLocalizedStrings.RegisterPrinted
    Else
        frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.RegisterPrinted
    End If
        
    Screen.MousePointer = vbArrow
            
    sbrMain.Status = SOTA_SB_START
            
    Set ReportCondition = Nothing
    Set mcSPInCreateTable = Nothing
    Set mcSPOutCreateTable = Nothing
    Set mcSPInDeleteTable = Nothing
    Set mcSPOutDeleteTable = Nothing
            
            
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ProcessRegister", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function iPostDocuments(ltaskNumber As Long) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim lRetVal As Long
Dim iMsgReturn As Integer
Dim lReturnCode As Long
Dim response As Integer
Dim mcSPInGLPost        As New Collection
Dim mcSPOutGLPost       As New Collection
Dim mcSPInGLPost2        As New Collection
Dim mcSPOutGLPost2       As New Collection
Dim bGood As Boolean
Dim iRegisterPrinted As Integer
Dim lStringNo As Long
Dim i As Integer


    'batch mode, no post exit
    If miMode = kPostXXBatches And miPostFlag = 0 Then
        'if Print-Only, release the lock
        bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)
        moClass.lUIActive = kChildObjectInactive
        
        Exit Function
    End If
    
    'turn lock off during prompt for post
    If miMode = kInteractiveBatch And chkPost = 1 And chkConfirm = 1 Then
        bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)
    End If
    
    msBatchID = sGetBatchID(Me)
    
    'single mode print but no post exit
    If miMode = kInteractiveBatch And chkPost = 0 Then
        'if Register-Only, release the lock
        bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)
        
        SetUIStatus
        ResetBatchStatus
        Exit Function
    End If
    
    If muRegStat.iErrorStatus = 2 And miMode = kInteractiveBatch Then       ' fatal errors exist
        If miMode <> kPostXXBatches Then
            iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgCannotPostFatalErrors, guLocalizedStrings.JournalDesc)
        End If
        
        moClass.lUIActive = kChildObjectInactive
        Set mcSPInGLPost = Nothing
        Set mcSPOutGLPost = Nothing
        Set mcSPInGLPost2 = Nothing
        Set mcSPOutGLPost2 = Nothing
            
        Exit Function
    End If
    
    If miMode = kInteractiveBatch And chkPost = 1 And chkConfirm = 1 Then
        iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgPostRegister, guLocalizedStrings.JournalDesc)
        
        If iMsgReturn = kretNo Then
            If miMode = kInteractiveBatch Then
                ResetBatchStatus    'errors occurred in batch post, reset status from posting to balanced.
            End If
             
            SetUIStatus
                
            Exit Function
        Else    'answered yes, set lock
            If miMode <> kPostXXBatches Then
                bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, True)
            Else
                bGood = True
            End If
            
            If bGood = False Then
                'error in logical lock, exit
                mPostError = 1
                miErrorsOccured = 1
                sbrMain.Status = SOTA_SB_START
                Screen.MousePointer = vbArrow
                
                Set mcSPInGLPost = Nothing
                Set mcSPOutGLPost = Nothing
                Set mcSPInGLPost2 = Nothing
                Set mcSPOutGLPost2 = Nothing
                
                ResetBatchStatus
                
                Exit Function
            End If
        End If
    End If
        
    'Start posting......
    sbrMain.Status = SOTA_SB_BUSY
    Screen.MousePointer = vbHourglass
        
    If miMode <> kPostXXBatches Then
        sbrMain.Message = guLocalizedStrings.PostingModule2
    Else
        frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.PostingModule2
    End If
        
    mcSPInGLPost.Add mlBatchKey
    mcSPInGLPost.Add msCompanyID
    mcSPInGLPost.Add gsFormatDateToDB(mdBusinessDate)
    mcSPInGLPost.Add miBatchType
    mcSPInGLPost.Add moReport.SessionID
    mcSPOutGLPost.Add mlRetValue
          
    'if we entered here from a post only situation, need to set lock,
    'otherwise, lock is already set.
    If mbSkippedToPost = True Then
        'don't set locks if in batch mode
        If miMode <> kPostXXBatches Then
            bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, True)
            If bGood = False Then
                bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)
                sbrMain.Status = SOTA_SB_START
                Screen.MousePointer = vbArrow
                
                Set mcSPInGLPost = Nothing
                Set mcSPOutGLPost = Nothing
                Set mcSPInGLPost2 = Nothing
                Set mcSPOutGLPost2 = Nothing
                
                ResetBatchStatus
                Exit Function
            End If
        Else
            bGood = True
        End If
    Else
        mbSkippedToPost = False
        bGood = True
    End If
        
    If chkRegister = 0 And chkConfirm = 0 And miMode <> kPostXXBatches Then
        bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, True)
    End If
        
    'register must be printed...
    If (iCheckRegisterPrinted(Me)) = 0 Then
        'unlock here
        mPostError = 1
        miErrorsOccured = 1
        sbrMain.Status = SOTA_SB_START
        Screen.MousePointer = vbArrow
        
        Set mcSPInGLPost = Nothing
        Set mcSPOutGLPost = Nothing
        Set mcSPInGLPost2 = Nothing
        Set mcSPOutGLPost2 = Nothing
        
        ResetBatchStatus
        Exit Function
    End If
        
    If bGood Then
        lRetVal = lExecSP("spCmPost", mcSPInGLPost, mcSPOutGLPost)
    Else
        'error in logical lock, exit
        mPostError = 1
        miErrorsOccured = 1
        sbrMain.Status = SOTA_SB_START
        Screen.MousePointer = vbArrow
        
        Set mcSPInGLPost = Nothing
        Set mcSPOutGLPost = Nothing
        Set mcSPInGLPost2 = Nothing
        Set mcSPOutGLPost2 = Nothing
        
        ResetBatchStatus
        Exit Function
    End If
        
    'take care of resetting lock & status & errror flags...
    If lRetVal <> 0 Then
        If miMode <> kPostXXBatches Then
            bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)
        End If
        
        If miMode = kInteractiveBatch Then
            ResetBatchStatus    'errors occurred in batch post, reset status from posting to balanced.
        End If
            
        mPostError = 1
        miErrorsOccured = 1
    End If
        
    'display errors in single mode only
    If lRetVal <> 0 Then
        If miMode = kInteractiveBatch Then
            Select Case lRetVal
                Case -1
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgCannotPostFatalErrors, guLocalizedStrings.JournalDesc)
                    lStringNo = kVNoPost
                Case 1
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgMoreUsers, guLocalizedStrings.JournalDesc)
                    lStringNo = kVMoreUsers
                Case 2
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgBatchHold, guLocalizedStrings.JournalDesc)
                    lStringNo = kVBatchHold
                Case 3
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgBadTotal, guLocalizedStrings.JournalDesc)
                    lStringNo = kVBadTotal
                Case 5
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgBeingPosted, guLocalizedStrings.JournalDesc)
                    lStringNo = kVBeingPosted
                Case 6
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgAlreadyPosted, guLocalizedStrings.JournalDesc)
                    lStringNo = kVAlreadyPosted
                Case 7
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgBatchDeleted, guLocalizedStrings.JournalDesc)
                    lStringNo = kVBatchDeleted
                Case 8
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgBatchBStatus, guLocalizedStrings.JournalDesc)
                    lStringNo = kVBatchBStatus
                Case 9
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgModPIncomp, guLocalizedStrings.JournalDesc)
                    lStringNo = kmsgModPostIncomp
                Case Else
            End Select
                    
            iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgPostErrors, guLocalizedStrings.JournalDesc)
            
            If iMsgReturn = kretOK Then
                i = iLogError(mlBatchKey, lStringNo, msBatchID, "", "", "", "", 1, 2)
                PrintErrorLog
            End If
        End If
            
        mPostError = 1
        miErrorsOccured = 1
            
        If miMode <> kPostXXBatches Then
            sbrMain.Message = guLocalizedStrings.PostingFailed
        Else
            frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.PostingFailed
        End If
            
        sbrMain.Status = SOTA_SB_START
        Screen.MousePointer = vbArrow
        moClass.lUIActive = kChildObjectInactive
        
        Set mcSPInGLPost = Nothing
        Set mcSPOutGLPost = Nothing
        Set mcSPInGLPost2 = Nothing
        Set mcSPOutGLPost2 = Nothing
        
        ResetBatchStatus
        Exit Function
    End If
                    
    If miMode <> kPostXXBatches Then
        sbrMain.Message = guLocalizedStrings.GLPosting
    Else
        frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.GLPosting
    End If
                        
    mcSPInGLPost2.Add msCompanyID
    mcSPInGLPost2.Add mlBatchKey
    mcSPInGLPost2.Add moReport.SessionID
    mcSPOutGLPost2.Add mlRetValue
        
    'lRetval = lExecSP("spCmGLPost", mcSPInGLPost2, mcSPOutGLPost2)
    
    '-- GL Posting !!!
    With moClass.moAppDB
      
      .SetInParam msCompanyID
      .SetInParam mlBatchKey
      .SetInParam moReport.SessionID
      .SetOutParam mlRetValue
      
      On Error Resume Next
      .ExecuteSP ("spCmGLPost")
      If Err.Number <> 0 Then
         lRetVal = -1
         MsgBox Err.Description, vbCritical
      Else
         lRetVal = .GetOutParamLong(3)
      End If
      On Error GoTo VBRigErrorRoutine
      
      .ReleaseParams
    
    End With
    
    If lRetVal <> 0 Then
        
        mPostError = 1
        miErrorsOccured = 1
            
        If miMode <> kPostXXBatches Then
            Select Case lRetVal
                Case -1
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgCannotPostFatalErrors, guLocalizedStrings.JournalDesc)
                    lStringNo = kVNoPost
                Case 1
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgMoreUsers, guLocalizedStrings.JournalDesc)
                    lStringNo = kVMoreUsers
                Case 2
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgBatchHold, guLocalizedStrings.JournalDesc)
                    lStringNo = kVBatchHold
                Case 3
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgBadTotal, guLocalizedStrings.JournalDesc)
                    lStringNo = kVBadTotal
                Case 5
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgBeingPosted, guLocalizedStrings.JournalDesc)
                    lStringNo = kVBeingPosted
                Case 6
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgAlreadyPosted, guLocalizedStrings.JournalDesc)
                    lStringNo = kVAlreadyPosted
                Case 7
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgBatchDeleted, guLocalizedStrings.JournalDesc)
                    lStringNo = kVBatchDeleted
                Case 8
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgBatchBStatus, guLocalizedStrings.JournalDesc)
                    lStringNo = kVBatchBStatus
                Case 9
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgBatchPrePostNotDone, guLocalizedStrings.JournalDesc)
                    lStringNo = kVBatchPrePostNotDone
                Case Else
            End Select
                
            iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgPostErrors, guLocalizedStrings.JournalDesc)
                
            If iMsgReturn = kretOK Then
                i = iLogError(mlBatchKey, lStringNo, msBatchID, "", "", "", "", 1, 2)
                PrintErrorLog
            End If
        End If
            
        If miMode <> kPostXXBatches Then
            bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)
            sbrMain.Message = guLocalizedStrings.PostingFailed
        Else
            lblStatusMsg = guLocalizedStrings.PostingFailed
        End If
            
        sbrMain.Status = SOTA_SB_START
        Screen.MousePointer = vbArrow
        moClass.lUIActive = kChildObjectInactive
        
        Set mcSPInGLPost = Nothing
        Set mcSPOutGLPost = Nothing
        Set mcSPInGLPost2 = Nothing
        Set mcSPOutGLPost2 = Nothing
        
        ResetBatchStatus
        Exit Function
    Else
        If miMode <> kPostXXBatches Then
            sbrMain.Message = guLocalizedStrings.GLPosted
        Else
            frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.GLPosted
        End If
            
        SetUIStatus
    End If
                        
    If miMode <> kPostXXBatches Then
        bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)
    End If
                        
    sbrMain.Status = SOTA_SB_START
    Screen.MousePointer = vbArrow
    moClass.lUIActive = kChildObjectInactive
    
    tbrMain.ButtonEnabled(kTbProceed) = False
    
    Set mcSPInGLPost = Nothing
    Set mcSPOutGLPost = Nothing
    Set mcSPInGLPost2 = Nothing
    Set mcSPOutGLPost2 = Nothing

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iPostDocuments", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    On Error GoTo ExpectedErrorRoutine
    On Error Resume Next

#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    gUnloadChildForms Me
    
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    If Not moRegProcess Is Nothing Then
        moRegProcess.destroy
        Set moRegProcess = Nothing
    End If

    


    Set moOptions = Nothing

    Set moDDData = Nothing
    Set moSort = Nothing
    If Not moReport Is Nothing Then
        Set moReport = Nothing
    End If
    
    Set moSettings = Nothing
    




    Set moPrinter = Nothing
    Set sRealTableCollection = Nothing
    Set sWorkTableCollection = Nothing
    


    
    If Not moRpt Is Nothing Then
        Set moRpt = Nothing
    End If
    
     If Not moContextMenu Is Nothing Then
        Set moContextMenu = Nothing
    End If
    Set moSotaObjects = Nothing

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine:

gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub SetUIStatus(Optional ByVal bInitialDisplay As Boolean = False)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim bSet As Boolean
    Dim OrigPostChecked As CheckBoxConstants

    muRegStat.iBatchStatus = 0
    muRegStat.iPrintStatus = 0
    muRegStat.iErrorStatus = 0
    bSet = False
    
    GetRegisterStatus moClass.moAppDB, mlBatchKey, muRegStat

    'The user's saved settings will override any batch status
    If bInitialDisplay And Not moSettings.SettingIsNone Then
        mbRegChecked = True
        Exit Sub
    End If
    
    If muRegStat.iPrintStatus = 1 Then
        sbrMain.Message = guLocalizedStrings.Printed
    Else
        sbrMain.Message = ""
    End If
    
    If muRegStat.iErrorStatus = 2 Then
        sbrMain.Message = guLocalizedStrings.FatalErrors
    End If
    
    If Not bInitialDisplay Then
        OrigPostChecked = chkPost.Value
    End If
    
       
    Select Case muRegStat.iBatchStatus
        'if batch is Inuse(1),OnHold(2) or OutOfBalance(3) print register only
        Case 1, 2, 3
            If muRegStat.iBatchStatus = 3 Then
                sbrMain.Message = guLocalizedStrings.NotBalanced
            End If
            
            chkRegister = 1
            chkPost = 0
            chkConfirm = 0
            
            chkRegister.Enabled = True
            chkPost.Enabled = False
            chkConfirm.Enabled = False
            
            'in this case exit
            Exit Sub
        
        Case 4
            If muRegStat.iPrintStatus = 1 Then
                chkRegister.Enabled = True
                chkPost.Enabled = mbPostingAllowed
                                            
                bSet = True
            End If
        
        Case 5, 7
            'batch interrupted
            chkRegister.Enabled = True
            chkPost.Enabled = mbPostingAllowed
                                    
            bSet = True
        
        Case 6
            'batch is posted
            chkRegister = 0
            chkPost = 0
            chkConfirm = 0

            chkRegister.Enabled = False
            chkPost.Enabled = False
            chkConfirm.Enabled = False
            
            sbrMain.Message = "Batch Posted"
            
            bSet = True
    End Select
        
    If muRegStat.iPrintStatus = 0 Then
        If muRegStat.iErrorStatus = 2 Then
            'disable posting options
            chkRegister = 1
            chkPost = 0

            chkRegister.Enabled = True
            chkPost.Enabled = False
            chkConfirm.Enabled = False
        
            bSet = True
        Else
            chkRegister = 1
        
            If mbRegChecked = False Then
                If mbPostingAllowed Then
                        
                    chkPost = 1
                Else
                    chkPost = 0
                End If
                
            End If

            chkRegister.Enabled = True
            chkPost.Enabled = mbPostingAllowed
        
            bSet = True
        End If
    End If
    
    If bInitialDisplay Then
        If chkPost.Enabled = False Then
            chkPost.Value = vbUnchecked
            chkConfirm.Enabled = False
        Else
            chkPost.Value = vbChecked
            chkConfirm.Enabled = True
        End If
    End If
    
    mbRegChecked = True
             

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetUIStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancelShutDown = mbCancelShutDown
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCancelShutDown_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Private Function bRecordsExist(muRegStat As RegStatus) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim iMsgReturn As Integer
    bRecordsExist = True
    'no batch records exist
    If muRegStat.iRecordsExist <> 1 Then
        If miMode <> kPostXXBatches Then
            iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgNoBatches, guLocalizedStrings.JournalDesc)
        End If
        
        bRecordsExist = False
    End If


'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bRecordsExist", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub WorkTableError(oSessionID As Long, lRetVal As Long, sType As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim iMsgReturn As Integer

    If miMode <> kPostXXBatches Then
    Select Case sType
        Case "Create"
            iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgErrorGeneratingRegister, guLocalizedStrings.JournalDesc)
        Case "Delete"
            iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgErrorGeneratingRegister, guLocalizedStrings.JournalDesc)
    End Select
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WorkTableError", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    FormHelpPrefix = "CIZ"
    
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    WhatHelpPrefix = "CIZ"
    
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
          

Private Function iPrePost(ltaskNumber As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim lRetVal As Long
Dim oSessionID As Long
Dim iMsgReturn As Integer
Dim lReturnCode As Long
Dim i As Integer
Dim mcSPInModulePost    As New Collection
Dim mcSPOutModulePost   As New Collection
Dim bGood As Boolean
Dim lStringNo As Long


    iPrePost = kFailure
    msBatchID = sGetBatchID(Me)
    
    mcSPInModulePost.Add mlBatchKey
    mcSPInModulePost.Add msCompanyID
    mcSPInModulePost.Add miBatchType
    mcSPOutModulePost.Add mlRetValue
        
            
    sbrMain.Status = SOTA_SB_BUSY
            
    'create work table to print register from
    Screen.MousePointer = vbHourglass
    If miMode <> kPostXXBatches Then
        sbrMain.Message = guLocalizedStrings.PostingModule
    Else
        frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.PostingModule
    End If
    
    'set lock if in single batch mode
    If miMode = kInteractiveBatch Then
        bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, True)
    Else
        'multibatch mode, don't set locks so set variable to induce pre post
        bGood = True
    End If
    
    'failure in setting lock
    If miMode = kInteractiveBatch And bGood = False Then
        sbrMain.Status = SOTA_SB_START
        Screen.MousePointer = vbArrow
        moClass.lUIActive = kChildObjectInactive
        
        Set mcSPInModulePost = Nothing
        Set mcSPOutModulePost = Nothing
        'release lock
        bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
         
    If bGood Then
        lRetVal = lExecSP("spCmPrePost", mcSPInModulePost, mcSPOutModulePost)
    End If
     
    'keep logical lock ON until done COMPLETE process or error
    If miMode <> kPostXXBatches Then 'don't display messages in multi-batch mode
        If lRetVal <> 0 Then
            Select Case lRetVal
                Case -1:
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgFatalErrorPrePost, guLocalizedStrings.JournalDesc)
                    lStringNo = kVFatalErrorPrePost
                Case 1:
                    'feature, not an error
                    'iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgCantPost, guLocalizedStrings.JournalDesc)
                    lStringNo = kVCmCantPostGL
                Case 2:
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgBatchPrePost, guLocalizedStrings.JournalDesc)
                    lStringNo = kVBatchPrePost
                Case 3:
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgBatchBStatus, guLocalizedStrings.JournalDesc)
                    lStringNo = kVBatchBStatus
                Case 4
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgBeingPosted, guLocalizedStrings.JournalDesc)
                    lStringNo = kVBeingPosted
                Case 5
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgAlreadyPosted, guLocalizedStrings.JournalDesc)
                    lStringNo = kVAlreadyPosted
                Case 6
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgBatchDeleted, guLocalizedStrings.JournalDesc)
                    lStringNo = kVBatchDeleted
            End Select
            
            If lStringNo = kVCmCantPostGL Then
                'warning on post to gl off in tarOptions
                i = iLogError(mlBatchKey, lStringNo, msBatchID, "", "", "", "", 1, 1)
                
                If mPostError = kNoErrors Then
                    mPostError = kWarnings
                End If
            Else
                i = iLogError(mlBatchKey, lStringNo, msBatchID, "", "", "", "", 1, 2)
                mPostError = kFatalErrors
            End If
            
            sbrMain.Status = SOTA_SB_START
            Screen.MousePointer = vbArrow
            moClass.lUIActive = kChildObjectInactive
            iPrePost = lRetVal
            
            'error situation, release lock
            If miMode = kInteractiveBatch Then
                bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)
                ResetBatchStatus
            End If
            
            Set mcSPInModulePost = Nothing
            Set mcSPOutModulePost = Nothing
            Exit Function
        End If
    End If
    
    iPrePost = kSuccess
            
    If miMode <> kPostXXBatches Then
        sbrMain.Message = guLocalizedStrings.ModulePosted
    Else
        frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.ModulePosted
    End If
        
    Set mcSPInModulePost = Nothing
    Set mcSPOutModulePost = Nothing
                     
    Screen.MousePointer = vbArrow
    sbrMain.Status = SOTA_SB_START
                     
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iPrePost", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function iValidateData(ltaskNumber As Long) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim lRetVal As Long
Dim iMsgReturn As Long
Dim lReturnCode As Long
Dim response As Integer
Dim iLoop As Integer
Dim mcSPInValidate      As New Collection
Dim mcSPOutValidate     As New Collection
Dim sValSP  As String
            
    iValidateData = 0
    
    If miMode <> kPostXXBatches Then
        sbrMain.Message = guLocalizedStrings.Validating
    Else
        lblStatusMsg = guLocalizedStrings.Validating
    End If
    
    sbrMain.Status = SOTA_SB_BUSY
            
    Screen.MousePointer = vbHourglass
              
    mcSPInValidate.Add mlBatchKey
    mcSPInValidate.Add miBatchType
    mcSPInValidate.Add msCompanyID
    mcSPOutValidate.Add mlRetValue
    sValSP = "spCmValidReg"
              
    lRetVal = lExecSP(sValSP, mcSPInValidate, mcSPOutValidate)
                
    If lRetVal = -1 Then
        sbrMain.Status = SOTA_SB_START
        Screen.MousePointer = vbArrow
        
        iValidateData = -1
        mPostError = 1
        miErrorsOccured = 1
        Set mcSPInValidate = Nothing
        Set mcSPOutValidate = Nothing
               
        moClass.lUIActive = kChildObjectInactive
        Exit Function
    ElseIf lRetVal = -2 Then   'warnings only
        mPostError = 2
    End If
                
    iValidateData = 1
              
    Screen.MousePointer = vbArrow
      
    'check the Status of the batch records and determine the ui
    'Status of the controls on the form
    SetUIStatus
                
    sbrMain.Status = SOTA_SB_START
    
    Set mcSPInValidate = Nothing
    Set mcSPOutValidate = Nothing

'    If mlTaskNumber = ktskCMDepReg Then
'        ValidateGainLoss
'    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iValidateData", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Sub PrintErrorLog()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim ret As Integer
Dim sortby As String
Dim prn As Integer
Dim RptCondition As New Collection
Dim lRetVal As Long
Dim mcSPInValidate      As New Collection
Dim mcSPOutValidate     As New Collection
Dim sFormulas() As String

ReDim sFormulas(1, 1)

mcSPInValidate.Add mlBatchKey
mcSPInValidate.Add moClass.moSysSession.Language    '1033
mcSPOutValidate.Add mlRetValue
        
lRetVal = lExecSP("spGLPopulateErrorCmt", mcSPInValidate, mcSPOutValidate)
                
Set mcSPInValidate = Nothing
Set mcSPOutValidate = Nothing

If miMode <> kPostXXBatches Then
    lblStatusMsg = guLocalizedStrings.PrintError
Else
    frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.PrintError
End If

' If user has selected post only and need to print error log, default to screen
' Should never get here because Post Only does not cause a call to PrintErrorLog
If mbPostOnly Then
    msPrintButton = kTbPreview
End If

StartErrorReport msPrintButton, Me

If miMode <> kPostXXBatches Then
    sbrMain.Message = guLocalizedStrings.PrintErrorCompleted
Else
    frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.PrintErrorCompleted
End If

Set RptCondition = Nothing

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PrintErrorLog", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function SetPrintedFlag(iFlag As Integer) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim lRetVal As Long
Dim response As Integer

    With moClass.moAppDB
        .SetInParam mlBatchKey
        .SetInParam iFlag
        .SetInParam msCompanyID
        .SetOutParam lRetVal
        
        ' run the stored procedure
        .ExecuteSP ("spSetRegPrintFlg")
        
        Sleep 0&
        
        lRetVal = .GetOutParam(4)
        .ReleaseParams
    End With
    
    If lRetVal <> 0 Then
        If miMode <> kPostXXBatches Then
        sbrMain.Status = SOTA_SB_START
        Screen.MousePointer = vbArrow
        response = giSotaMsgBox(Me, moClass.moSysSession, kmsgFlagNotFound, guLocalizedStrings.JournalDesc)
        End If
    End If
    
    SetRegPrintedFlag
    
    SetPrintedFlag = lRetVal

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetPrintedFlag", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function PrintGLPostingRecap(ltaskNumber As Long) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim lRetVal As Long
Dim response As Integer
Dim sortby As String
Dim iLoop As Integer
Dim prn As Integer
Dim ReportCondition As New Collection
Dim sReportFile As String
Dim sFormulas() As String

    'if CM options is set to no gl register, exit
    If miGLPostRgstrFormat = 1 Then
        Exit Function
    End If
    
    PrintGLPostingRecap = 0
           
    sbrMain.Status = SOTA_SB_BUSY
            
    'Screen.MousePointer = vbHourglass
    If miMode <> kPostXXBatches Then
        lblStatusMsg = guLocalizedStrings.ProcessingGLReg
    Else
        frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.ProcessingGLReg
    End If
    
    moReport.ReportPath = msGLReportPath
    
    StartGLPostReg msPrintButton, Me
    moReport.CascadePreview
    
    moReport.ReportPath = msCMReportPath
    
    If miMode <> kPostXXBatches Then
        lblStatusMsg = "GL Posting Register completed."
    Else
        frmPrintPostStatus.lblStatusMsg = "GL Posting Register completed."
    End If
    
    Screen.MousePointer = vbArrow
    sbrMain.Status = SOTA_SB_START
        
       
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PrintGLPostingRecap", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function setDates() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL As String
Dim rs As Object
Dim Status As Integer
Dim response As Integer
Dim i As Integer

    setDates = kSuccess
    
    sSQL = "Select Status from tglFiscalPeriod "
    sSQL = sSQL & " where StartDate <=  " & gsQuoted(gsFormatDateToDB(mlPostDate)) _
        & " and CompanyID = " & gsQuoted(msCompanyID) & " and EndDate in ( " _
        & "select distinct EndDate from tglFiscalPeriod where EndDate >= " _
        & gsQuoted(gsFormatDateToDB(mlPostDate)) & " )"
       
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If rs.IsEmpty Then
        msBatchID = sGetBatchID(Me)
        Set rs = Nothing
        setDates = kSuccess
        mPostError = kFatalErrors
        i = iLogError(mlBatchKey, kVNonPer, msBatchID, "", "", "", "", 1, 2)
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    Status = rs.Field("Status")
    
    Set rs = Nothing
           
    If Status = 2 Then
        msBatchID = sGetBatchID(Me)
        i = iLogError(mlBatchKey, kVClosedPer, msBatchID, "", "", "", "", 1, 2)
        setDates = kSuccess
        mPostError = kFatalErrors
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "setDates", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub UnloadCollection(ByRef inCollection As Collection)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim iLoop As Integer

    For iLoop = 1 To inCollection.Count
        inCollection.Remove iLoop
    Next iLoop

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "UnloadCollection", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub GetHomeCurrency()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL As String
Dim rs As Object

    sSQL = ""
    sSQL = "Select CurrID from tsmCompany where CompanyID = "
    sSQL = sSQL & kSQuote & msCompanyID & kSQuote
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If rs.IsEmpty Then
        Set rs = Nothing
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If

    mlHomeCurrency = rs.Field("CurrID")
    Set rs = Nothing
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetHomeCurrency", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Function CheckBalance() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL As String
Dim sumPostAmt As Double
Dim response As Integer
Dim lRetVal As Long
Dim lEntryNo As Long

    CheckBalance = kSuccess
    
    With moClass.moAppDB

        .SetInParam mlBatchKey
        .SetInParam moReport.SessionID
        .SetOutParam lRetVal
        ' run the stored procedure
        .ExecuteSP ("sparCheckBalance")
        
        Sleep 0&
        lRetVal = .GetOutParam(3)
        .ReleaseParams

    End With
    
    If lRetVal = kFailure Then
        mPostError = 1
        miErrorsOccured = 1
    End If
    
    CheckBalance = lRetVal
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CheckBalance", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub HandleToolBarClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iRet As Integer
    Dim iPostStatus As Integer
    Dim bGood As Boolean

#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If

    'Flag indicating we went directly to posting (PostStatus >= 250).
    mbSkippedToPost = False

    Select Case sKey
        Case kTbProceed
            'Validate check boxes.
            If chkRegister = 0 Then
                If chkPost = 0 Then
                    MsgBox "Either Register or Post checkbox or both should be checked!", vbOKOnly, "Register/Post"
                    Exit Sub
                Else
                    If muRegStat.iErrorStatus = 2 Then
                        MsgBox "Posting is not allowed since there are errors in the register!", vbOKOnly, "Register/Post"
                        Exit Sub
                    End If
                End If
            End If
            
            'Initialization
            moReport.CleanupWorkTables
            cleanErrorLog
            miErrorsOccured = 0
            mPostError = kNoErrors
            mlLockID = 0
            
            moClass.lUIActive = kChildObjectActive
            
            'Define the print device for the report engine.
            If chkRegister = 1 Then
                If cboOutput.ListIndex = 0 Then
                    msPrintButton = kTbPrint
                Else
                    msPrintButton = kTbPreview
                End If
            End If
            
            'Show print dialog if not Post Batches or if
            'Post Batches and first time through.
            If miMode <> kPostXXBatches Then
                gbSkipPrintDialog = False
            End If
            
            If Not gbSkipPrintDialog And (msPrintButton = kTbPrint Or msPrintButton = kTbProceed) Then
                'Show windows default printer if in Post Batches.
                If miMode = kPostXXBatches Then
                    moReport.ShowPrinterDialogForBatch gbCancelPrint, Screen.ActiveForm.hwnd, True
                Else
                    moReport.ShowPrinterDialogForBatch gbCancelPrint, Screen.ActiveForm.hwnd
                End If
                
                If gbCancelPrint Then
                    GoTo Cleanup
                End If
                
                gbSkipPrintDialog = True    'Subsequent batches don't show dialog. HBS.
            End If
            
            If miMode = kPostXXBatches Then
                If lSetBatchType() = kFailure Then
                    Exit Sub
                End If
            End If
            
            sbrMain.Message = guLocalizedStrings.CheckingBatch
                    
            iPostStatus = iCheckPostStatus(Me)
            
            'Batch already posted, nothing to do.
            If iPostStatus = 500 Then
                iRet = giSotaMsgBox(Me, moClass.moSysSession, kmsgAlreadyPosted, guLocalizedStrings.JournalDesc)
                Exit Sub
            End If
            
            'Check the status of the fiscal period.
            If setDates = kFailure Then
                moClass.miShutDownRequester = kUnloadSelfShutDown
                Me.Hide

                If miMode = kPostXXBatches Then
                    miErrorsOccured = 1
                    ResetBatchStatus
                End If

                Exit Sub
            End If
    
            'Validate data.
            iRet = iValidateData(mlTaskNumber)
                                                 
            If (muRegStat.iPrintStatus = 1 And miMode <> kPostXXBatches And chkRegister = 0 And chkPost = 1) _
            Or (mbPostOnly = True And miMode = kPostXXBatches) Then
                SetUIStatus
                
                If (mPostError = kNoErrors _
                Or mPostError = kWarnings) Then
                    If CheckBalance = kSuccess Then
                        If iPostDocuments(mlTaskNumber) Then
                            lblStatusMsg = guLocalizedStrings.posted
                            
                            If miMode <> kPostXXBatches Then
                                sbrMain.Message = lblStatusMsg
                            End If
                            
                            sbrMain.Status = SOTA_SB_START
                            Screen.MousePointer = vbArrow
                        End If
                    End If
                Else
                    PrintErrorLog

                    If miMode = kPostXXBatches Then
                        ResetBatchStatus
                    End If
                End If
                
                Exit Sub
            End If
            
            'If pre-posting fails, continue no farther.
            If iPrePost(mlTaskNumber) = kFailure Then
                Exit Sub
            End If
            
            'Validate GL currencies in tglPosting.
            ValidateGLCurrencies
            
            'Process and print register.
            ProcessRegister mlTaskNumber
            
            'Check register balance for errors.
            CheckBalance
            
            If mPostError = kFatalErrors Then
                PrintErrorLog
                moReport.CascadePreview
                SetPrintedFlag (2)
                
                bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)

                If miMode = kPostXXBatches Then
                    ResetBatchStatus
                End If
            ElseIf (mPostError = kWarnings) Then
                PrintErrorLog
                moReport.CascadePreview
                SetPrintedFlag (1)
            Else
                SetPrintedFlag (0)
            End If
                                
            If mPostError = kNoErrors Or mPostError = kWarnings Then
                iRet = PrintGLPostingRecap(mlTaskNumber)
               
                'SGS-JMM 1/31/2011
                On Error Resume Next
                If Not (cypHelper Is Nothing) Then
                    If cypHelper.IsCypressPrinter(moReport.PrinterName) Then
                        cypHelper.ShowViewer
                    End If
                End If
                On Error GoTo VBRigErrorRoutine
                'End SGS-JMM 1/31/2011

PostOnly:
                If (mPostError = kNoErrors _
                Or mPostError = kWarnings) Then
                    'If no errors found during validation.
                    If iPostDocuments(mlTaskNumber) Then
                        lblStatusMsg = guLocalizedStrings.posted

                        If miMode <> kPostXXBatches Then
                            sbrMain.Message = lblStatusMsg
                        End If
                        
                        sbrMain.Status = SOTA_SB_START
                        Screen.MousePointer = vbArrow
                    End If
                    
                    If miPreview = 1 Then
                        miPreview = 0   'Reset preview flag.
                    End If
                Else
                    miPreview = 0
                    mPostError = kNoErrors   'Reset flag.
                End If
            Else
                miPreview = 0
            
                'SGS-JMM 1/31/2011
                On Error Resume Next
                If Not (cypHelper Is Nothing) Then
                    If cypHelper.IsCypressPrinter(moReport.PrinterName) Then
                        cypHelper.ShowViewer
                    End If
                End If
                On Error GoTo VBRigErrorRoutine
                'End SGS-JMM 1/31/2011
            End If
                    
            SetUIStatus
            Screen.MousePointer = vbArrow
        
        Case kTbSave
            moSettings.ReportSettingsSaveAs
        
        Case kTbDelete
            moSettings.ReportSettingsDelete

        Case kTbClose
        
            If Not moReport.bShutdownEngine Then
                Exit Sub
            End If
            
            moClass.miShutDownRequester = kUnloadSelfShutDown

            Unload Me
            
        Case kTbHelp
            gDisplayFormLevelHelp Me
        
        Case Else
            'Do nothing
    End Select
            
    'SGS-JMM 1/31/2011
    On Error Resume Next
    If Not (cypHelper Is Nothing) Then cypHelper.HandleToolBarClick sKey, 1
    On Error GoTo VBRigErrorRoutine
    'End SGS-JMM 1/31/2011

Cleanup:
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolBarClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Function cleanErrorLog()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL As String

    sSQL = "delete tciErrorLog where BatchKey = " & Str(mlBatchKey)
    moClass.moAppDB.ExecuteSQL sSQL

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cleanErrorLog", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub PrintCommissionRegister(sRptName As String)

End Sub
Private Sub ResetInterruptedStatus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL As String
    sSQL = "UPDATE tciBatchLog SET Status = 5 WHERE BatchKey = " & Str(mlBatchKey) & "  AND Status = 7"
    moClass.moAppDB.ExecuteSQL sSQL
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
 
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ResetInterruptedStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ResetBatchStatus()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++

Dim sSQL As String
On Error GoTo ExpectedErrorRoutine
    sSQL = "UPDATE tciBatchLog SET Status = 4 WHERE BatchKey = " & Str(mlBatchKey) & "  AND Status = 5"
    moClass.moAppDB.ExecuteSQL sSQL
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
ExpectedErrorRoutine:
gClearSotaErr

Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ResetBatchStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function lSetBatchType(Optional vFormLoad As Variant) As Long
    Dim sSQL        As String
    Dim iBatchType  As Integer
    Dim rs          As Object
    Dim bFormLoad   As Boolean  ' func is called from formload
    Dim lRetVal     As Long     ' return value form report initialization

    lSetBatchType = kFailure
    On Error GoTo ExpectedErrorRoutine

    bFormLoad = False
    If Not IsMissing(vFormLoad) Then
        bFormLoad = CBool(vFormLoad)
    End If

    If Not bFormLoad Then
         sSQL = "SELECT BatchType FROM tciBatchLog WHERE BatchKey = " & Str(mlBatchKey)
        
         Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
         If rs Is Nothing Then Exit Function
         If rs.IsEmpty Then
             rs.Close
             Set rs = Nothing
             Exit Function
         End If
         
         iBatchType = rs.Field("BatchType")
             
         Select Case iBatchType
             Case kBatchTypeCMRE
                 mlTaskNumber = ktskCMRecReg
             Case kBatchTypeCMDE
                 mlTaskNumber = ktskCMDepReg
             Case kBatchTypeCMCM
                 mlTaskNumber = ktskCMTranReg
         End Select
         rs.Close
         Set rs = Nothing
    End If

    Set sRealTableCollection = New Collection
    If mlTaskNumber = ktskCMRecReg Then
        sRealTableCollection.Add "tcmCashTran"
    Else
        sRealTableCollection.Add "tcmPendCashTran"
    End If
    
    Set sWorkTableCollection = New Collection

    If mlTaskNumber = ktskCMRecReg Then
        miBatchType = BATCH_TYPE_RE
    ElseIf mlTaskNumber = ktskCMTranReg Then
        miBatchType = BATCH_TYPE_CM
    Else
        miBatchType = BATCH_TYPE_DE
    End If
    SetupWorkTables

    If mlTaskNumber = ktskCMRecReg Then
        lRetVal = lInitializeReport("CM", "CMZJD002.RPT", "CMZJADL1")
    ElseIf mlTaskNumber = ktskCMTranReg Then
        lRetVal = lInitializeReport("CM", "CMZJA002.RPT", "CMZJADL1")
    Else
        lRetVal = lInitializeReport("CM", "CMZJC001.RPT", "CMZJADL1")
    End If
    
    If lRetVal <> 0 Then
        moReport.ReportError lRetVal
        Exit Function
    End If
   
    lSetBatchType = kSuccess
   
    Exit Function
   
ExpectedErrorRoutine:
gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lSetBatchType", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function iCheckRegisterPrinted(frm As Form) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL As String
Dim rs As Object

    iCheckRegisterPrinted = 0
    
    sSQL = "SELECT RgstrPrinted FROM tciBatchLog WHERE BatchKey = " & Str(mlBatchKey)
    Set rs = frm.oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        iCheckRegisterPrinted = rs.Field("RgstrPrinted")
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iCheckRegisterPrinted", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function iCheckUseSper(frm As Form) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL As String
Dim rs As Object

    iCheckUseSper = 0
    
    sSQL = "SELECT UseSper FROM tarOptions WHERE CompanyID = '" & frm.oClass.moSysSession.CompanyID & kSQuote
    Set rs = frm.oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        iCheckUseSper = rs.Field("UseSper")
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iCheckUseSper", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function sGetBatchID(frm As Form) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL As String
Dim rs As Object

    sGetBatchID = ""
    
    sSQL = "SELECT BatchID FROM tciBatchLog WHERE BatchKey = " & Str(mlBatchKey)
    Set rs = frm.oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        sGetBatchID = rs.Field("BatchID")
    End If
    
    Set rs = Nothing
    

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetBatchID", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function iCheckPostStatus(frm As Form) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL As String
Dim rs As Object

    iCheckPostStatus = 0
    
    sSQL = "SELECT PostStatus FROM tciBatchLog WHERE BatchKey = " & Str(mlBatchKey)
    Set rs = frm.oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        iCheckPostStatus = rs.Field("PostStatus")
    End If
    
    Set rs = Nothing
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iCheckPostStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

'this functionality will be put into the stored proc
Private Function iCleanCommReg()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
Dim sSQL As String
Dim rs As Object

On Error GoTo ExpectedErrorRoutine
        
    sSQL = "DELETE tarCommRegWrk WHERE SessionID = " & Str(moReport.SessionID)
    moClass.moAppDB.ExecuteSQL sSQL
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
ExpectedErrorRoutine:
gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iCleanCommReg", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function iLogError(lBatchKey As Long, iStringNo As Long, s1 As String, _
    s2 As String, s3 As String, s4 As String, s5 As String, iErrorType As Integer, _
    iSeverity As Integer) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL As String
Dim rs As Object
Dim lRet As Long
Dim iEntryNo As Integer


    iLogError = 0
    
    'first get max entry no for this batch
    sSQL = "SELECT MAX(EntryNo) MaxEntryNo FROM tciErrorLog WHERE SessionID = " & Str(mlBatchKey)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        If Not IsNull(rs.Field("MaxEntryNo")) Then
        iEntryNo = rs.Field("MaxEntryNo") + 1
        Else
            iEntryNo = 1
        End If
    End If
    
    Set rs = Nothing

         
    'Run stored proc to populate commission register work table
      With moClass.moAppDB
          .SetInParam lBatchKey
          .SetInParam iEntryNo
          .SetInParam iStringNo
          .SetInParam s1
          .SetInParam s2
          .SetInParam s3
          .SetInParam s4
          .SetInParam s5
          .SetInParam iErrorType
          .SetInParam iSeverity
          
          .SetOutParam lRet
          .ExecuteSP ("spglCreateErrSuspenseLog")
          lRet = .GetOutParam(11)
          .ReleaseParams
      End With
     
    iLogError = lRet
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iLogError", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function




Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "frmRegister2"
End Function

Private Sub GetModVariables(frm As Form)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL As String
Dim rs As Object

    
    
    sSQL = "SELECT UseMultCurr,GLPostRgstrFormat FROM tcmOptions WHERE CompanyID = '" & frm.oClass.moSysSession.CompanyID & kSQuote
    Set rs = frm.oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        miUseMultCurr = rs.Field("UseMultCurr")
        miGLPostRgstrFormat = rs.Field("GLPostRgstrFormat")
    End If
    
    Set rs = Nothing
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetModVariables", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub GetReportPaths()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim iAttr As Integer
Dim lRet As Long

    msCMReportPath = moClass.moSysSession.ModuleReportPath("CM")
    lRet = lValidateReportPath(msCMReportPath)
    msGLReportPath = moClass.moSysSession.ModuleReportPath("GL")
    lRet = lValidateReportPath(msGLReportPath)
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetReportPaths", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function lValidateReportPath(sPath As String) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
Dim iAttr As Integer
On Error GoTo ExpectedErrorRoutine

    lValidateReportPath = kFailure
    
    If Not Len(sPath) > 0 Then
        GoTo ExpectedErrorRoutine
    Else
        If Right(msCMReportPath, 1) = BACKSLASH Then
            iAttr = GetAttr(Mid(sPath, 1, Len(sPath) - 1)) And vbDirectory
        Else
            iAttr = GetAttr(sPath) And vbDirectory
            sPath = sPath & BACKSLASH
        End If
        If iAttr <> vbDirectory Then
            MsgBox "Report Path: " & sPath & " is not a valid directory on this workstation;" & vbCr & " may not exist on this client workstation", vbCritical, "frmRegister2.lValidateReportPath"
            GoTo ExpectedErrorRoutine
        End If
    End If

ExpectedErrorRoutine:
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lValidateReportPath", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub ValidateGLCurrencies()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim lRet As Long


    lRet = 0
    
    With moClass.moAppDB
        .SetInParam mlBatchKey
        .SetInParam mlHomeCurrency
        .SetInParam msCompanyID
        .SetOutParam lRet
        .ExecuteSP ("spCmValGLCurr")
        lRet = .GetOutParam(4)
        .ReleaseParams
    End With
     

    If lRet = -1 Then
        mPostError = 1
        miErrorsOccured = 1
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ValidateGLCurrencies", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub ValidateGainLoss()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim lRet As Long


    lRet = 0
    
    With moClass.moAppDB
        .SetInParam mlBatchKey
        .SetInParam moReport.SessionID
        .SetInParam msCompanyID
        .SetOutParam lRet
        .ExecuteSP ("sparValRealGain")
        lRet = .GetOutParam(4)
        .ReleaseParams
    End With
     

    If lRet = -1 Then
        mPostError = 1
        miErrorsOccured = 1
    End If
        
     

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ValidateGainLoss", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function bSetPostStatus(lBatchKey As Long, iPost As Integer, _
            iPrint As Integer, ByRef lLockID As Long, sBatch As String, _
            bTurnOn As Boolean) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*************************************************************************************
' Desc:  Sets or removes posting Status from batches chosen
' Parms: lBatchKey - Array of batch Keys
'        iPost     - Array Of posting Flags
'        bTurnOn   - Wheteher to turn on or of posting Status
'*************************************************************************************

Dim i           As Integer    'Looping Variable
Dim iResult     As Integer    'Stored procedure results
Dim lShared     As Long       'count of shared locks
Dim lExcl       As Long       'count of exclusive locks
Dim sEntity     As String       'entity name to be passed to logical lock routines
Dim iBatchType  As Integer      'batch type to be passed to logical lock routines
Dim lLock     As Long           'lock id for logical lock, out param for setting, in param for clearing

bSetPostStatus = True
    
  'Loop through posting array
    'For i = 0 To UBound(iPost())
        
            sEntity = kLockEntBatch & Format$(lBatchKey)
   
   'Don't allow them to print or post if already posting (turning Lock On)
        If bTurnOn Then
        
            gCountLogical moClass.moAppDB, sEntity, lShared, lExcl
            If lExcl > 0 Then
                'MsgBox "A batch that you have chosen to print or post is already posting."
                giSotaMsgBox Me, moClass.moSysSession, kmsgCIAlreadyPosting, _
                        sBatch
                bSetPostStatus = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
        
        End If
      
      'Post this batch
        If iPost = 1 Then
            
          'Turn on Lock
            If bTurnOn Then
                
                iResult = glLockLogical(moClass.moAppDB, sEntity, kLockTypeExclusive, lLock)
                
                Select Case iResult
                    Case kLockRetSuccess
                        lLockID = lLock
                      
                        'Get Status for batch make sure it is balanced or interrupted
                        'The IF condition is added by HC on 11-13-96. If Register-Only,
                        'the register should be able to run.
'                        If Me.optRegister(kPrintNoPost) = False Then
'                            With moClass.moAppDB
'                                .SetInParam lBatchKey
'                                .SetOutParam iResult
'                                .ExecuteSP ("spCIGetBatchPostStatus")
'                                iResult = .GetOutParam(2)
 '                               .ReleaseParams
'                            End With
                        
'                            If iResult = 0 Then
'                                giSotaMsgBox Nothing, moClass.moSysSession, kmsgCIBadStatus, _
'                                    sBatch
'                                bSetPostStatus = False
'                                Exit Function
'                            End If
'                        End If
                    Case kLockRetNoTemp
                        giSotaMsgBox Me, moClass.moSysSession, kmsgCantGetBatchLock, _
                            sBatch
                        bSetPostStatus = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                    Case kLockRetExclusiveAlready
                        giSotaMsgBox Me, moClass.moSysSession, kmsgCIAlreadyPosting, _
                            sBatch
                        bSetPostStatus = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                    Case kLockRetSharedAlready
                        giSotaMsgBox Me, moClass.moSysSession, kmsgCIInUseOrPosting, _
                            sBatch
                        bSetPostStatus = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                End Select
                
            Else
                If lLockID <> 0 Then
                    lLock = lLockID
                    iResult = glUnlockLogical(moClass.moAppDB, lLock)
                End If
            End If
        
        Else    'for shared lock on printing
            
            If bTurnOn Then
                
                iResult = glLockLogical(moClass.moAppDB, sEntity, kLockTypeShared, lLock)
                
                Select Case iResult
                    Case kLockRetSuccess
                        lLockID = lLock
                    Case kLockRetNoTemp
                        giSotaMsgBox Me, moClass.moSysSession, kmsgCantGetBatchLock, _
                            sBatch
                        bSetPostStatus = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                    Case kLockRetExclusiveAlready
                        giSotaMsgBox Me, moClass.moSysSession, kmsgCIAlreadyPosting, _
                            sBatch
                        bSetPostStatus = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                End Select
            Else
                
                If lLockID <> 0 Then
                    lLock = lLockID
                    iResult = glUnlockLogical(moClass.moAppDB, lLock)
                End If
                
            End If
        
        End If  'Post is turned on
    
    'Next i

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSetPostStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Public Property Get oReport() As clsReportEngine
'+++ VB/Rig Skip +++
    Set oReport = moReport
End Property

Public Property Set oReport(oClass As clsReportEngine)
'+++ VB/Rig Skip +++
    Set moReport = oClass
End Property

Public Sub SetRegPrintedFlag()

    If moClass.moAppDB.Lookup("RgstrPrinted", "tciBatchLog", "BatchKey = " & mlBatchKey) = False Then
        mbRegPrinted = False
    Else
        mbRegPrinted = True
    End If
    
End Sub

Private Sub EnablePost(bState As Boolean)
    
    '-- Enable post controls if not already enabled
    '-- and passed state is true
    '-- If passed state is false, disable controls
    If mbPostingAllowed Then
        If bState = True And chkPost.Enabled = False Then
            chkPost.Enabled = True
            chkPost.Value = vbChecked
            
        ElseIf bState = False And mbRegPrinted = False Then
            chkPost.Value = vbUnchecked
            chkPost.Enabled = False
            chkConfirm.Enabled = False
        End If
    End If
    
End Sub

Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Skip +++
    bLoadSuccess = mbLoadSuccess
End Property

Public Property Let bLoadSuccess(bNewLoadSuccess As Boolean)
'+++ VB/Rig Skip +++
    mbLoadSuccess = bNewLoadSuccess
End Property

Public Function lExecSP(spName As String, colInSPParams As Collection, ByRef colOutSPParams As Collection) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
        
Dim iLoop As Integer
Dim iNoOfInParams As Integer
Dim lRetVal As Long
'run register stored procedure to create work table and tglPosting table rows
  On Error GoTo ExpectedErrorRoutine

    iNoOfInParams = colInSPParams.Count
    
    With moClass.moAppDB
        ' set the "in" parameters from the "in" collection
        For iLoop = 1 To colInSPParams.Count
            .SetInParam colInSPParams.Item(iLoop)
        Next iLoop
        
        .SetOutParam lRetVal
        
        ' run the stored procedure
        .ExecuteSP (spName)
        
        While .IsExecuting = True
            DoEvents
        Wend
        
        'Next iLoop
        lRetVal = .GetOutParam(iNoOfInParams + 1)
        
        .ReleaseParams
    End With
    
    ' set the return value of this function to the last out param value
    lExecSP = lRetVal 'colOutSPParams.Item(colOutSPParams.Count)

'+++ VB/Rig Begin Pop +++
        Exit Function
        
ExpectedErrorRoutine:

    MsgBox Err.Description, vbOKOnly + vbCritical, "Stored Procedure: " & spName
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "lPostModuleTrans", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_Paint(Index As Integer)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If
#If CUSTOMIZER Then
Private Sub Form_Activate()

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyFormCust
        End If
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
    Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If

Private Sub txtMessageHeader_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtMessageHeader(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMessageHeader_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtMessageHeader(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMessageHeader_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtMessageHeader(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMessageHeader_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtMessageHeader(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub chkOutStdTrans_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkOutStdTrans, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkOutStdTrans_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkOutStdTrans_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkOutStdTrans, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkOutStdTrans_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkOutStdTrans_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkOutStdTrans, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkOutStdTrans_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkConfirm_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkConfirm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkConfirm_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkConfirm_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkConfirm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkConfirm_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkConfirm_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkConfirm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkConfirm_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPost_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkPost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPost_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPost_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkPost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPost_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkRegister_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkRegister, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkRegister_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkRegister_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkRegister, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkRegister_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkSummary_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkSummary, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkSummary_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkSummary_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkSummary, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkSummary_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkSummary_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkSummary, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkSummary_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboFormat_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboFormat, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboFormat_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboFormat_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboFormat, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboFormat_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboFormat_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboFormat, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboFormat_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboOutput_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboOutput, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboOutput_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboOutput_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboOutput, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboOutput_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboOutput_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboOutput, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboOutput_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboOutput_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboOutput, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboOutput_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboReportSettings_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboReportSettings, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboReportSettings_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboReportSettings_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboReportSettings, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboReportSettings_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
               Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If


















Public Property Get MyApp() As Object
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property







'SGS-JMM 1/31/2011
Public Function GetCypressQuery() As String
    Dim batchID As String

    On Error Resume Next
    batchID = moClass.moSysSession.AppDatabase.Lookup("BatchID", "tciBatchLog", "BatchKey = " & Me.CONTROLS(0).Parent.mlBatchKey)
    GetCypressQuery = "[Company ID] = """ & moClass.moSysSession.CompanyID & """ and [Batch ID] = """ & batchID & """"
End Function
'End SGS-JMM

