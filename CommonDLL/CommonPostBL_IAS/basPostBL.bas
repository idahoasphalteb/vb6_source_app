Attribute VB_Name = "basPostBL"
Option Explicit

Public Const PERIOD_END = "Period End"
Public Const CB_FINDSTRINGEXACT = &H158
Public Const kSessionID = "{tmcRevalRgstrWrk.SessionID}"
Public Const kBatchKey = "{tglPosting.BatchKey}"
Public Const kBatchKeyLog = "{tciErrorLog.BatchKey}"
Public Const RPT_MODULE = "MC" 'enter the two character module Name here
Public Const BACKSLASH = "\"

Public Const kACHErr = "ACH extract file cannot be generated."
'Logical Locking Entity Constants
Public Const kLockEntBatch = "BATCH"
Public Const kLockEntOptions = "OPT"
Public Const kLockEntVendPmt = "VENDPMT"
Public Const kvIMRANKlock = "IMRANK"
Public Const kLockShipment = "SHIPMENT"

Public Const kTxtFormatDefa     As Integer = 0
Public Const kTxtFormatName     As Integer = 1
Public Const kTxtFormatID       As Integer = 2
Public Const kTxtFormatBoth     As Integer = 3
Public Const kModuleNoAP        As Integer = 4
Public Const kModuleNoAR        As Integer = 5
Public Const kModuleNo          As String = "ModuleNo"
Public Const kParent            As String = "Parent"
Public Const kSortType          As String = "sortType"
Public Const kInvoiceCmnt       As String = "invcComments"
Public Const kLineItemCmnt      As String = "LineItemComments"
Public Const kPmntCmnt          As String = "paymentCmnt"
Public Const kRepType           As String = "repType"
Public Const kProject           As String = "ProjectAccounting"
Public Const kPaymentAppl       As String = "Payment Application Register"
Public Const kVoucherReg        As String = "Voucher Register"
Public Const kManualChk         As String = "Manual Check"
Public Const kSystemChk         As String = "System Payment"
Public Const kModule            As String = "module"
Public Const kRptParent         As String = "RptParent"
Public Const kAccsPayable       As String = "Accounts Payable"
Public Const kStandard          As Integer = 1
Public Const kTrans             As Integer = 2
Public Const kBegBal            As Integer = 3

Public Const kNumZeroPad = 1
Public Const kNumJustifyRight = 2
Public Const kTbProceed = "K18"
Public Const kTbClose = "K21"
Public Const kTbPreview = "K16"
Public Const kmsgapInvalidReportPath = 140362               ' Report Path: {0} is not a valid directory on this workstation.
Public Const kmsgCannotPostFatalErrors = 100073             ' {0} Register cannot post.  Invalid data encountered.Check for invalid accounts, closed periods and out-of-balance journals.
Public Const kmsgErrorGeneratingRegister = 100072           ' Errors were encountered generating the {0} Register.
Public Const kmsgPostingDidNotComplete = 100075             ' {0} Register posting did not complete.
Public Const kmsgFiscPerClosed = 130035                     ' The fiscal period for this post date is closed
Public Const kmsgPostRegister = 100074                      ' Do you want to post the {0} Register?
Public Const kretYes = 6                                     'Yes
Public Const kretNo = 7                                      'No
Public Const kretOK = 1                                      'OK
Public Const kTbPrint = "K14"
Public Const kmsgPostErrors = 140072                        ' Errors found during Posting.  Would you like to print the Error Log?
Public Const kmsgPostingSuccessful = 130261                 ' The posting process completed successfully.
Public Const kmsgNoBatches = 100076                         ' No {0} batches for posting.
Public Const kmsgGLYearNotSetup = 140475                    ' The necessary fiscal year for the company is not set up in GL. Click OK to have the system set up the fiscal year and post the batch. This may take a long time. Click Cancel to cancel posting. Use GL Setup to set up the fiscal year, then post the batch.
Public Const ksBSInUse = 151330                             ' In Use
Public Const ksBSOnHold = 151331                            ' On Hold

'Miscellaneous Constants needed for Reporting
'Would like to move towards using the standard constants for these
Public Const QUOTECON = """"
Public Const COLONCON = ":"
Public Const kmsgCIPostXXStatus = 140301                    ' Post {0} Batches processing has completed.    {1} Registers Selected for Printing        {2} Registers Printed    {3} Batches Selected for Posting        {4} Batches Posted        {5} Batches Not Posted due to Errors
Public Const kValidating = 1612                             ' Validating
Public Const kPrintedWarnings = 6551                        ' Printed-Warnings
Public Const kPrintError = 6507                             ' Print Error
Public Const kPrinting = 18263                              ' Printing
Public Const kPrintingCompleted = 18264                     ' Printing Completed
Public Const kPrintingRegister = 6502                       ' Printing Register
Public Const kRecurringJournal = 130005                     ' Recurring Journal
Public Const kRegisterPrinted = 6503                        ' Register Printed
Public Const kRegisterPrintedWarnings = 6552                ' Register Printed - Warnings
Public Const kPrinted = 6553                                ' Printed
Public Const kPostingToGL = 6554                            ' Posting to General Ledger
Public Const kPosted = 6555                                 ' Posted
Public Const kPostingCompleted = 6556                       ' Posting Completed
Public Const kErrors = 6557                                 ' Errors
Public Const kErrorsOccurred = 6558                         ' Error(s) occurred during validation
Public Const kstrRegPreprocEnded = 160304                   ' Preprocessing and registers completed
Public Const kstrRegPreprocStarted = 160303                 ' Preprocessing and registers started

Public Const kAllocationJournal = 130003                    ' Allocation Journal
Public Const kBudgetRevisionJournal = 130006                ' Budget Revision Journal
Public Const kDetail = 6471                                 ' Detail
Public Const kGeneralJournal = 130004                       ' General Journal
Public Const kJrnlBalanced = 654                            ' Balanced
Public Const kJrnlFatalErrors = 652                         ' Fatal Errors Exist
Public Const kJrnlNotBalanced = 653                         ' Out of Balance
Public Const kJrnlNotPrinted = 650                          ' Not Printed
Public Const kJrnlPosted = 656                              ' Posting Completed
Public Const kJrnlPosting = 655                             ' Posting
Public Const kJrnlPostingFailed = 657                       ' Posting Failed
Public Const kPostingRegister = 6714                        ' Posting Register
Public Const kSummary = 6483                                ' Summary
Public Const kPostingModule = 6505                          ' Posting Module
Public Const kModulePosted = 6506                           ' Module Posted
Public Const kModuleCleanup = 100197                        ' Module Cleanup
Public Const kGLPosting = 151424
Public Const kGLPostingComplete = 151425

Public Const kmsgIMFASAPINotAvailable = 166207              ' Warning: The Fixed Asset System is currently unavailable. Assets will not be created.
Public Const kmsgIMFASInitializeFailed = 166208             ' Warning: Unexpected error while initializing interface to the Fixed Asset Accounting System. Assets will not be created.
Public Const kFASImportStatusReport = 141198                ' FA Import Status Report
Public Const kmsgProc = 153223                              ' Unexpected Error running Stored Procedure {0}.

Public Const kmsgFlagNotFound = 140074                      ' Batch Key not found during print flag reset.
Public Const kmsgAPGenFileConnectErr = 140500               ' Unable to connect to the database. {0} extract file cannot be generated.
Public Const kmsgAPGenFileSuccess = 140498                  ' Successfully completed {0} process. Did it generate the proper file format?
Public Const kmsgAPGenFileUnknownErr = 140499               ' Unknown error. {0} extract file cannot be generated.
Public Const kmsgAPGenFileDirErr = 140501                   ' Invalid directory. {0} extract file cannot be generated.
Public Const kmsgAPACHBlankAcct = 140502                    ' Bank account number from [{0}] default Remit-To address cannot be blank. ACH extract file cannot be generated.
Public Const kmsgAPACHBlankRouting = 140503                 ' Bank routing number from [{0}] default Remit-To address cannot be blank. ACH extract file cannot be generated.
Public Const kmsgAPACHBlankOptions = 140504                 ' Unable to retrieve records in option screen. You must complete entries in Set Up ACH Options. ACH extract file cannot be generated.
Public Const kmsgAPACHNoChecks = 140505                     ' Batch does not contain any valid checks (ACH can only process standard checks). ACH extract file cannot be generated.
Public Const kmsgAPACHNextFileNo = 140506                   ' Unable to retrieve the next available Canadian ACH/AFT file number. Canadian ACH/AFT extract file cannot be generated.
Public Const kmsgAPACHBlankRtrn = 140507                    ' Canadian ACH/AFT return account number or return routing number cannot be blank. Canadian ACH/AFT extract file cannot be generated.
Public Const kmsgAPACHPrenotePost = 140515                  ' Warning: This batch cannot be posted because one or more vendors have a Prenotification ACH transaction code. Before you can post the batch, you must change the ACH transaction code in Maintain Vendors to an option other than Prenotification.'
Public Const kmsgAPACHMixedPrenoteBatch = 140516            ' The ACH file cannot be generated and the batch cannot be posted because the batch contains both prenotification and non-prenotification transactions. Change the ACH transaction code in Maintain Vendors to the same type for all vendors in this batch.'
Public Const kmsgAPACHAlphaNumRouting = 140517              ' Invalid Routing Number. {0} default Remit-To address routing number of {1} cannot contain characters that are not aplhanumeric. ACH extract file cannot be generated.
Public Const kProcessingRegister = 6501                     ' Processing Register

Public Const kVOJournalDesc = 6370                          ' Voucher Register
Public Const kCKJournalDesc = 6371                          ' Check Register
Public Const kMCJournalDesc = 6372                          ' Manual Check Register
Public Const kPAJournalDesc = 6373                          ' Payment Application Register

Public Const kmsgCIAlreadyPosting = 120088                  ' You may not print or post Batch {0}.  It is already posting.
Public Const kmsgCIBatchOnHold = 120092                     ' You may not post batch {0} it  is currently on hold.
Public Const kmsgCIInUseOrPosting = 120089                  ' You may not post batch {0}.  It is either in use or posting.
Public Const kmsgCIPrintRgstr = 120087                      ' The register has not been printed for Batch: {0}.  You must also select print to post this batch.
Public Const kmsgCIPrivateBatch = 120093                    ' Batch {0} is a private batch belonging to another user.  You may not print or post this batch.
Public Const kmsgUnexpectedConfirmUnloadRV = 100009         ' Unexpected Confirm Unload Return Value: {0}
Public Const kmsgUnexpectedSPReturnValue = 100071           ' Unexpected Stored Procedure Return Value: {0}
Public Const kmsgCIBadStatus = 120090                       ' You may not post batch {0}. It is not in a balanced or interrupted status.
Public Const kmsgCantGetBatchLock = 120098                  ' Error occurred while attempting to lock batch  {0}.
Public Const kmsgUnexpectedOperation = 100015               ' Unexpected Operation Value: {0}
Public Const kmsgAlreadyPosted = 153282                     ' The batch has been posted.
Public Const kVNoPost = 151281                              ' Batch {0}: Register cannot post.  Invalid data encountered.
Public Const kmsgFatalErrorPrePost = 140073                 ' Unexpected Error while Pre-Posting.
Public Const kVFatalErrorPrePost = 151266                   ' Batch {0}: Unexpected error while pre-posting.
Public Const kARZJA001ProcessingGLReg = 151129              ' Processing GL Register
Public Const kmsgExpError = 153450                          ' Error: "{0}" in {1}
Public Const kRevaluation = 170023                          ' Revaluation
Public Const kErrorLogTbl = 15242                           ' Error Log
Public Const kmsgPeriodEndRptSettingsMissin = 100200        ' There are no report settings named "Period End" for report "{1}".
Public Const kmsgBatchNotFoundError = 140033                ' The Accounts Payable batch you are attempting to access cannot be found at this time:Batch Key: {0}

'AvaTax Integration
Public Const kmsgtavConfigurationError = 100433 ' There was an unexpected error reading the AvaTax configuration table.   Table: tavConfiguration.  AvaTax will not be avabile during this process.
Public Const kmsgAvaObjectError = 100434 ' The AvaTax options are enabled for this company, but the client object AVZDADL1 was not found on this machine.  AvaTax will not be available during this process.
'AvaTax end

Public Type uLocalizedStrings
    sPrinted              As String
    sNotPrinted           As String
    sPrinting             As String
    sPrintingCompleted    As String
    sFatalErrors          As String
    sNotBalanced          As String
    sBalanced             As String
    sPosting              As String
    sPosted               As String
    sPostingFailed        As String
    sINJournalDesc        As String
    sCRJournalDesc        As String
    sVOJournalDesc        As String
    sCKJournalDesc        As String
    sMCJournalDesc        As String
    sPAJournalDesc        As String
    sARPAJournalDesc      As String
    sARRAJournalDesc      As String
    sCMJournalDesc        As String
    sJournalDesc          As String
    sGJJournalDesc        As String
    sRJJournalDesc        As String
    sAJJournalDesc        As String
    sBRJournalDesc        As String
    sDetail               As String
    sSummary              As String
    sCheckingBatch        As String
    sProcessingGLRegister As String
    sProcessingRegister   As String
    sPrintingRegister     As String
    sRegisterPrinted      As String
    sPostingCompleted     As String
    sPostingModule        As String
    sModulePosted         As String
    sPostingToGL          As String
    sValidating           As String
    sModuleCleanup        As String
    sPrintError           As String
    sPostingModule2       As String
    sModulePosted2        As String
    sGLPosted             As String
    sProcessingGLReg      As String
End Type

Public Type RegStatus
    iBatchStatus As Long
    iRecordsExist As Long
    iPrintStatus As Long
    iErrorStatus As Long
    iBalanceStatus As Long
End Type

Public Enum GroupingIndex   'The second index for GroupingParams()
    GroupName = 0           'Crystal report group header name, such as "GH1"
    SortDirection = 1       'Sort direction, 0 for ascending, 1 for descending (comes from CRAXDRT.CRSortDirection
    TableName = 2           'Name of table holding group column to group by
    ColumnName = 3          'Column name to group by
    HighestIndex = 3        'The highest index of the fields described above, used for dimensioning
End Enum

'   Private Enum to match that in Sage 500 ERP Statusbar control. Allows projects
'   without Sage 500 ERP Statusbar component to compile.
 Public Enum StatusType    '   Status Images
    SOTA_SB_NONE = 0
    SOTA_SB_START = 1
    SOTA_SB_ADD = 2
    SOTA_SB_EDIT = 3
    SOTA_SB_SAVE = 4
    SOTA_SB_LOCKED = 5
    SOTA_SB_INQUIRY = 6
    SOTA_SB_BUSY = 7
End Enum

Public Declare Function SendMessageByString Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Integer, ByVal lParam As String) As Long

Public Declare Sub Sleep Lib "kernel32" _
 (ByVal dwMilliseconds As Long)
 
 Public Const kARPrintingRegister = 151826                   ' Printing AR Register...
Public Const kARPostingRegister = 151827                    ' Posting AR Register...
Public Const kARPostingFailed = 151828                      ' AR Posting failed, AP Posting rolled back.
Public Const kARPostingCompleted = 151829                   ' AR Posting completed.
Public Const kRegisterPosted    As Long = 1
Public Const kRegisterNotPosted As Long = 0
Public Const kFileTypeDel = 1
Public Const kFileTypeFix = 2
Public Const kStrJustifyLeft = 1
Public Const kStrJustifyRight = 2
Public Const kDateYYMMDD = 1
Public Const kDateYYYYMMDD = 2
Public Const kDateMMDDYY = 3
Public Const kDateMMDDYYYY = 4
Public Const kDateJulian = 5
Public Const kDateDDMMYY = 6
Public Const kDateSlashMMDDYYYY = 7     'RKL DEJ 2016-06-08
Public Const kExtractNone = 1
Public Const kExtractACH = 2
Public Const kExtractPosPay = 3

'RKL DEJ 2016-06-08 (START)
Public Const kDecNA = -1
Public Const kDec0 = 0
Public Const kDec1 = 1
Public Const kDec2 = 2
Public Const kDec3 = 3
Public Const kDec4 = 4
Public Const kDec5 = 5
Public Const kDec6 = 6
Public Const kDec7 = 7
Public Const kDec8 = 8
Public Const kDec9 = 9
'RKL DEJ 2016-06-08 (STOP)

Public Const kvAcctRefUsageNotUsed = 0
Public mcSPInCreateTable        As New Collection
Public mcSPOutCreateTable       As New Collection
Public mcSPInDeleteTable        As New Collection
Public mcSPOutDeleteTable       As New Collection
Public mcSPInModulePost         As New Collection
Public mcSPOutModulePost        As New Collection
Public msSPCreateTable          As String
Public msSPDeleteTable          As String
Public msSPModulePost           As String
Public msWorkTable              As String
Public msReportFileName         As String
Public miSessionId              As Long
Public msCurrencyId             As String
Public msReportName             As String
Public miSourceCompUseMultCurr  As Integer
Public miSourceCompAcctRefUsage As Integer
Public msSourceCompanyID        As String
Public msSourceCompanyName      As String
Public mlSourceCompBatchKey     As Long
Public miUseMultCurr            As Integer
Public miJrnlType               As Integer
Public mlBatchType              As Long
Public mlSourceCompBatchType    As Long
Public mlSourceCompBatchType_NF As Long
Public msUserID                 As String
Public mlSessionID              As Long
Public miSessionIDItem          As Integer
Public miFullGL                 As Integer
Public msCompanyID              As String
Public miGLRgstrType            As Integer
Public lBatchKey                As Long
Public mlModuleNumber           As Long
Public miGLPostDetailFlag       As Integer          ' 1  None, 2 Summary,  Detail
Public msEntity                 As String           ' Holds the Batch Entity ID
Public mdPostDate               As Date
Public mlPostDate               As Date         ' Holds the post date
Public iBatchStatus             As Integer
Public miErrorsOccured          As Integer
Public miMode                   As Integer      ' 0 for register 1 for Post XX Batches
Public msBatchID                As String
Public mlLanguage               As Long
Public msBatchType              As String
Public msPrinterName            As String

Public sSortBy                  As String
Public GroupingParams()         As String      'Use GroupingIndex enumeration for second index specification
Public sAppendSort              As String
Public msCRWSelect              As String
Public msFormulas()             As String
Public moGLRegister             As Object
Public moAPRegister             As Object
Public moCommRegister           As Object
Public moARRegister             As Object
Public moMCRegister             As Object
Public mlTaskNumber             As Long
Public guLocalizedStrings       As uLocalizedStrings
Public ofrmRegister             As Object
Public ofrmPrintPost            As Object
Public ofrmProperties           As Object
Public mfrmMain                 As Object
Public frm                      As Object
Public msPrintButton            As String           ' Preview or Print
Public mbPostingAllowed         As Boolean
Public mbRegChecked             As Boolean
Public mbRegPrinted             As Boolean
Public gbPostBatchesFlag        As Boolean   ' Holds whether the invocation is from Post Im bates or not
Public moReport                 As clsReportEngine
Public mPostError               As Integer
Public mbProcessBatch           As Boolean
Public gbCancelPrint            As Boolean   ' Cancel Print Dialog from Post Batches. HBS.
Public gbSkipPrintDialog        As Boolean   ' don't show print dialog. HBS.
Public mbRegisterFlag           As Boolean   ' Holds a flag to indicate whether register is needed or not
Public mbPostFlag               As Boolean   ' Holds a flag to indicate whether posting is to be done or not.
Public mbConfirmFlag            As Boolean   ' Holds a flag for confirmation required.
Public sButton                  As String
Public miRepType                As Integer

Public frmReg                   As Object
Public frmPost                  As Object
Public muRegStat                As RegStatus
Public glSessionID              As Long
Public mlBatchKey               As Long
Public msCompanyName            As String 'Used for intercompany registers

'*******************************************************************************************
'*******************************************************************************************
'RKL DEJ 2/19/15 (START)
'This was added for uprade from 7.4 to 2014 - The object Apzjxdl1.clsProcessRegisterREG is
'no longer in use.  So moved code here
'*******************************************************************************************
'*******************************************************************************************
Public moCypHelper       As Object 'SGS DEJ 1/12/12 - Orig Customized in 7-3
Public mbPosPayAllowed   As Boolean
'*******************************************************************************************
'*******************************************************************************************
'RKL DEJ 2/19/15 (STOP)
'*******************************************************************************************
'*******************************************************************************************



Public Sub BuildLocalizedStrings(oAppDB As Object, _
                                 SysSession As Object, _
                                 guLocalizedStrings As uLocalizedStrings)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'   // LLJ: Change for module & register

    With guLocalizedStrings
        .sNotPrinted = gsBuildString(kJrnlNotPrinted, oAppDB, SysSession)  'Not Printed
        .sPrinting = gsBuildString(kPrinting, oAppDB, SysSession)  'Printing
        .sPrintingCompleted = gsBuildString(kPrintingCompleted, oAppDB, SysSession)   'Printing Completed
        .sFatalErrors = gsBuildString(kJrnlFatalErrors, oAppDB, SysSession)  'Fatal Errors Exist
        .sNotBalanced = gsBuildString(kJrnlNotBalanced, oAppDB, SysSession) 'Out of Balance
        .sBalanced = gsBuildString(kJrnlBalanced, oAppDB, SysSession) 'Balanced
        .sPosting = gsBuildString(kJrnlPosting, oAppDB, SysSession) 'Posting
        .sPosted = gsBuildString(kJrnlPosted, oAppDB, SysSession)
        .sPostingFailed = gsBuildString(kJrnlPostingFailed, oAppDB, SysSession)  'Posting Failed
        .sVOJournalDesc = gsBuildString(kVOJournalDesc, oAppDB, SysSession)           ' Voucher Register
        .sCKJournalDesc = gsBuildString(kCKJournalDesc, oAppDB, SysSession)           ' Check Register
        .sMCJournalDesc = gsBuildString(kMCJournalDesc, oAppDB, SysSession)           ' Manual Check Register
        .sPAJournalDesc = gsBuildString(kPAJournalDesc, oAppDB, SysSession)           ' Payment Application Register
        .sARRAJournalDesc = "Reverse Application Register"
        .sJournalDesc = Empty
        .sGJJournalDesc = gsBuildString(kGeneralJournal, oAppDB, SysSession)  'General Journal
        .sRJJournalDesc = gsBuildString(kRecurringJournal, oAppDB, SysSession)  'Recurring Journal
        .sAJJournalDesc = gsBuildString(kAllocationJournal, oAppDB, SysSession)  'Allocation Journal
        .sBRJournalDesc = gsBuildString(kBudgetRevisionJournal, oAppDB, SysSession)  'Budget Revision Journal
        .sPrintError = gsBuildString(kPrintError, oAppDB, SysSession)  'Print Error
        .sDetail = gsBuildString(kDetail, oAppDB, SysSession)  'Detail
        .sSummary = gsBuildString(kSummary, oAppDB, SysSession)  'Summary
        .sPostingModule2 = gsBuildString(kPostingModule, oAppDB, SysSession)
        .sPostingToGL = gsBuildString(kPostingToGL, oAppDB, SysSession)
        .sModulePosted2 = gsBuildString(kModulePosted, oAppDB, SysSession)
        .sGLPosted = gsBuildString(kGLPostingComplete, oAppDB, SysSession)
        .sModuleCleanup = gsBuildString(kModuleCleanup, oAppDB, SysSession)
        .sProcessingGLReg = gsBuildString(kARZJA001ProcessingGLReg, oAppDB, SysSession)
        .sRegisterPrinted = gsBuildString(kRegisterPrinted, oAppDB, SysSession)   'Register Printed
        .sPostingCompleted = gsBuildString(kPostingCompleted, oAppDB, SysSession) 'Posting Completed
    End With
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BuildLocalizedStrings", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
    sMyName = "basPostBL"
End Function
