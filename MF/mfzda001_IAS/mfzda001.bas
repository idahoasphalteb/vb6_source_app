Attribute VB_Name = "basmfzda001"
'*********************************************
'* NOTE: This module has NOT been VB/Rigged! *
'*********************************************

Option Explicit
'**********************************************************************
'     Name: basmfzda001
'     Desc: Main Module of this Object containing "Sub Main ()".
'     Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
'     Original: 11-10-1998
'     Mods: mm/dd/yy XXX
'**********************************************************************

'-- Security Event Constants
    '-- if any security events are defined for this task, here is
    '-- where you would define the constant for the event
    'Public Const ksecChangeStatus As String = "CICHGSTAT"

'-- Object Task Constants
    '-- if this maintenance program has an associated report/listing,
    '-- here is where you would define the report/listing's Task ID so
    '-- that it may be launched when the Print button is pressed on the
    '-- toolbar
    'Public Const ktskPrint = 9998
    'Form Constants
    Public Const kHeaderPage = 0
    Public Const kDetailPage = 1
'-- Static List Constants
' timItem | Status
    Public Const kvActive As Integer = 1                'Active                         Str=50028
    Public Const kvInactive As Integer = 2              'Inactive                       Str=50029
    Public Const kvDiscontinued As Integer = 3          'Discontinued                   Str=50387
    Public Const kvDeleted As Integer = 4               'Deleted                        Str=50033
    
    'Dialog forms' return values
    Public Const kDlgSuccess = 100
    Public Const kDlgFailure = 110
    Public Const kDlgCancel = 120
    
    Const VBRIG_MODULE_ID_STRING = "MFZDA001.BAS"

Public Sub Main()
'+++ VB/Rig Begin Push +++                                                                'MAS 500 Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'MAS 500 Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("mfzda001.clsmfzda001")
        StartAppInStandaloneMode oApp, Command$(), ktskMFRoutingEntry
    End If
'+++ VB/Rig Begin Pop +++                                                                 'MAS 500 Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'MAS 500 Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "basmfzda001", "Main", VBRIG_IS_MODULE                           'MAS 500 Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basmfzda001"
End Function

