Attribute VB_Name = "basmfzdz001"

Const VBRIG_MODULE_ID_STRING = "mfzdz001.BAS"
Public gbCancelPrint     As Boolean
'Public gbSkipPrintDialog As Boolean     '   Show print dialog
Public Type LineDetail
    
  sWorkCenterId As String
  sMachineId As String
  sOperationId As String
  sOperDesc As String
  dRunHrs As Double
  dRunCost As Double
  sStepId As String
  sType As String
  sMatItemId As String
  dQTYReqSTD As Double
  dScrapPcs As Double
  dSetupHrs As Double
  dSetCost As Double
  dMatCost As Double
  bReadOnly As Boolean
  lWorkOrderStepKey As Long
  lMatItemKey As Long
  lDetlWhseKey As Long
  lMatStockUOMKey As Long
  bKitDistrItem As Boolean
  lItemKey As Long
  sTranNo As String
  lWorkOrderKey As Long
  lDetlTransKey As Long
End Type
Public Function lStartReport(sButton As String, frm As Form, bPeriodEnd As Boolean, _
    sPerEndDate, iPerNo, sPerYear, Optional iFileType As Variant, Optional sFileName As Variant) As Long
    
Dim sWhereClause As String
Dim sTablesUsed As String
Dim sSelect As String
Dim sInsert As String
Dim lRetVal As Long
Dim bValid As Boolean
Dim iNumTablesUsed As Integer
Dim RptFileName As String
Dim ReportObj As clsReportEngine
Dim DBObj As Object

    On Error GoTo badexit

    lStartReport = kFailure
    
    If Not bPeriodEnd Then
        frm.MousePointer = vbHourglass
    End If

    
    Set ReportObj = frm.moReport
    Set DBObj = frm.oClass.moAppDB
    
    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
    
    sSelect = "IF  OBJECT_ID('tempdb..#tmfWorkOrdHeadWrk') IS NOT NULL" & vbCrLf
    sSelect = sSelect & "TRUNCATE table #tmfWorkOrdHeadWrk" & vbCrLf
    sSelect = sSelect & "ELSE" & vbCrLf
    sSelect = sSelect & "SELECT * INTO #tmfWorkOrdHeadWrk FROM tmfWorkOrdHeadWrk WHERE 1=2 " & vbCrLf

    DBObj.ExecuteSQL sSelect
    
     'CUSTOMIZE:  Build a string for selection of key field and session ID.
    sSelect = "SELECT DISTINCT tmfWorkOrdHead_HAI.WorkOrderKey, " & ReportObj.SessionID _
                & " FROM tmfWorkOrdHead_HAI WITH (NOLOCK) WHERE tmfWorkOrdHead_HAI.CompanyID = '" & frm.msCompanyID & kSQuote
                
    sSelect = sSelect & " and tmfWorkOrdHead_HAI.WorkOrderKey = " & gsQuoted(frm.mlWorkOrderKey)
    #If RPTDEBUG Then
        sInsert = "INSERT INTO tmfWorkOrdHeadWrk (WorkOrderKey, SessionID) " & sSelect
    #Else
        sInsert = "INSERT INTO #tmfWorkOrdHeadWrk (WorkOrderKey, SessionID) " & sSelect
    #End If
    
    On Error Resume Next
    DBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
    gClearSotaErr
    On Error GoTo badexit
    
       
    'CUSTOMIZE:  Set input and output parameters as needed and execute appropriate
    'custom stored procedure to populate remaining fields in primary work table and
    'insert records into secondary work tables.
    With DBObj
        .SetInParam (ReportObj.SessionID)
        .SetInParam frm.msCompanyID
        .SetOutParam lRetVal
        On Error Resume Next
        .ExecuteSP ("spmfReportProduction")
        If Err.Number <> 0 Then
            lRetVal = Err.Number
        Else
            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
            'errors after each attempt.  After storing all return values in local variables OR
            'encountering an error, release parameters.
            lRetVal = DBObj.GetOutParam(3)
        End If
    End With
     
    If lRetVal <> 0 Then
        ReportObj.ReportError kmsgProc, ""
        DBObj.ReleaseParams
        GoTo badexit
    End If
        
    DBObj.ReleaseParams
    gClearSotaErr
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)
     ReportObj.ReportFileName() = "mfzse003.RPT"
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    ReportObj.Orientation() = vbPRORPortrait
    
    'CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    'to gsBuildString with a VB constant defined in StrConst.bas. The subtitles should
    'not include the format selected by the user, i.e., "Detail" or "Summary".
    ReportObj.ReportTitle1() = "Production Report"   ' = gsBuildString(kCIFOBList, frm.oClass.moAppDB, frm.oClass.moSysSession)

    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    'using the "lbl" convention on formula field names so that label text will handled automatically
    ReportObj.UseSubTotalCaptions() = 1
    ReportObj.UseHeaderCaptions() = 1
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If

    
    '********* the following is specific to your report *************'
    Select Case RptFileName
        Case "XXZYY001.RPT"
        Case Else
    End Select
    '********* End of special processing *************'
    
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.AppendSort "tmfWorkOrdHeadWrk.WorkOrderNo"
    ReportObj.AppendSort "tmfWorkOrdDetlWrk.StepId"
    ReportObj.AppendSort "tmfWorkOrdTranWrk.TransactionNo"
    ReportObj.SetSQL
    
    
    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    ReportObj.SetReportCaptions
    
        
    'CUSTOMIZE:  If using work tables, restrict report data to current Session ID.  If using
    'real tables, might restrict report data to current company or other criteria.
    If (ReportObj.lRestrictBy("{tmfWorkOrdHeadWrk.SessionID} = " & ReportObj.SessionID) = kFailure) Then
        GoTo badexit
    End If
    
    Dim iUnitCostDecPlaces As Integer
    Dim iQtyDecPlaces As Integer
    
    iUnitCostDecPlaces = giGetValidInt(frm.oClass.moAppDB.Lookup("UnitCostDecPlaces", _
                         "tciOptions WITH (NOLOCK)", "CompanyID = " & gsQuoted(frm.msCompanyID)))
    iQtyDecPlaces = giGetValidInt(frm.oClass.moAppDB.Lookup("QtyDecPlaces", "tciOptions WITH (NOLOCK)", _
                    "CompanyID = " & gsQuoted(frm.msCompanyID)))
    
    ReportObj.bSetReportFormula "UnitCostDecPlaces", CInt(iUnitCostDecPlaces)
    ReportObj.bSetReportFormula "QtyDecPlaces", CInt(iQtyDecPlaces)

    If bPeriodEnd Then
        gbSkipPrintDialog = True    '   don't show print dialog
    End If

    ReportObj.ProcessReport frm, sButton, iFileType, sFileName
    gbSkipPrintDialog = True    '   if multiple rpts, show print dialog one time only.
            
            
    If Not bPeriodEnd Then
        frm.MousePointer = vbDefault
    End If
    
    lStartReport = kSuccess
    
    Set ReportObj = Nothing
    Set DBObj = Nothing
    
    Exit Function
    
badexit:
    ReportObj.CleanupWorkTables
    If Not bPeriodEnd Then
        frm.sbrMain.Status = SOTA_SB_START
        frm.MousePointer = vbDefault
    End If
   
    Set ReportObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
    Exit Function
'+++ VB/Rig Begin Pop +++                                                                 'MAS 500 Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'MAS 500 Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'MAS 500 Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'MAS 500 Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lStartReport", VBRIG_IS_MODULE                   'MAS 500 Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
End Function
Public Sub Main()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("mfzdz001.clsmfzdz001")
'        StartAppInStandaloneMode oApp, Command$(), ktskMFProductionEntry
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Main", VBRIG_IS_MODULE                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basmfzdz001"
End Function


