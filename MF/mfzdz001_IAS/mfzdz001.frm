VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#157.0#0"; "EntryLookupControls.ocx"
Object = "{9504980C-B928-4BF5-A5D0-13E1F649AECB}#35.0#0"; "SOTAVM.ocx"
Object = "{BC90D6A3-491E-451B-ADED-8FABA0B8EE36}#47.0#0"; "SOTADropDown.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.ocx"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#84.0#0"; "LookupView.ocx"
Begin VB.Form frmmfzdz001 
   ClientHeight    =   6750
   ClientLeft      =   855
   ClientTop       =   975
   ClientWidth     =   9480
   HelpContextID   =   38159
   Icon            =   "mfzdz001.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6750
   ScaleWidth      =   9480
   Begin Threed.SSFrame fraLaborEntry 
      Height          =   2370
      Left            =   75
      TabIndex        =   37
      Top             =   3780
      Width           =   9270
      _Version        =   65536
      _ExtentX        =   16351
      _ExtentY        =   4180
      _StockProps     =   14
      Caption         =   "Production Detail Information"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin FPSpreadADO.fpSpread grdMain 
         Height          =   1950
         Left            =   180
         TabIndex        =   38
         Top             =   315
         WhatsThisHelpID =   104255
         Width           =   8850
         _Version        =   524288
         _ExtentX        =   15610
         _ExtentY        =   3440
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         SpreadDesigner  =   "mfzdz001.frx":23D2
         AppearanceStyle =   0
      End
   End
   Begin VB.Frame FraProduction 
      Caption         =   "Production Entry"
      Height          =   3180
      Left            =   60
      TabIndex        =   1
      Top             =   465
      Width           =   9270
      Begin VB.CheckBox chkPhantomAsSubAss 
         Caption         =   "Treat Phantoms as Sub-assemblies"
         Height          =   255
         Left            =   5955
         TabIndex        =   27
         Top             =   315
         WhatsThisHelpID =   17777139
         Width           =   3090
      End
      Begin Threed.SSFrame fraReport 
         Height          =   1155
         Left            =   6840
         TabIndex        =   32
         Top             =   585
         Width           =   2205
         _Version        =   65536
         _ExtentX        =   3889
         _ExtentY        =   2037
         _StockProps     =   14
         Caption         =   "Production Report"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.CheckBox chkPrintProduction 
            Alignment       =   1  'Right Justify
            Caption         =   "Print"
            Height          =   285
            Left            =   95
            TabIndex        =   33
            Top             =   330
            WhatsThisHelpID =   17375843
            Width           =   900
         End
         Begin SOTADropDownControl.SOTADropDown ddnOutput 
            Height          =   315
            Left            =   780
            TabIndex        =   35
            Top             =   690
            WhatsThisHelpID =   100280
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   556
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Text            =   "ddnOutput"
         End
         Begin VB.Label lblOutput 
            Caption         =   "Output"
            Height          =   225
            Left            =   95
            TabIndex        =   34
            Top             =   735
            Width           =   630
         End
      End
      Begin VB.CommandButton cmdDetail 
         Caption         =   "D&etail..."
         Height          =   375
         Left            =   7365
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   2730
         WhatsThisHelpID =   17375845
         Width           =   1650
      End
      Begin VB.CommandButton cmdLotSerialBin 
         Caption         =   "&Dist..."
         Height          =   375
         Left            =   7365
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   2265
         WhatsThisHelpID =   104254
         Width           =   1650
      End
      Begin VB.CommandButton cmdValidProd 
         Caption         =   "&Validate Production"
         Height          =   375
         Left            =   7365
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   1815
         WhatsThisHelpID =   104253
         Width           =   1650
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtWhseDesc 
         Height          =   315
         Left            =   2850
         TabIndex        =   15
         Top             =   1545
         WhatsThisHelpID =   104247
         Width           =   2430
         _Version        =   65536
         _ExtentX        =   4286
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtItemDesc 
         Height          =   315
         Left            =   4095
         TabIndex        =   12
         Top             =   1125
         WhatsThisHelpID =   104245
         Width           =   2655
         _Version        =   65536
         _ExtentX        =   4683
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
      End
      Begin EntryLookupControls.TextLookup lkuItemId 
         Height          =   315
         Left            =   1260
         TabIndex        =   11
         Top             =   1125
         WhatsThisHelpID =   104244
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   556
         ForeColor       =   -2147483640
         IsSurrogateKey  =   -1  'True
         LookupID        =   "Item"
         ParentIDColumn  =   "ItemID"
         ParentKeyColumn =   "ItemKey"
         ParentTable     =   "timItem"
         BoundColumn     =   "ItemKey"
         BoundTable      =   "timItem"
         sSQLReturnCols  =   "ItemID,lkuItemId,;ShortDesc,txtItemDesc,;ItemKey,,;"
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtEmployeeName 
         Height          =   315
         Left            =   3795
         TabIndex        =   9
         Top             =   705
         WhatsThisHelpID =   104243
         Width           =   2955
         _Version        =   65536
         _ExtentX        =   5212
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
      End
      Begin EntryLookupControls.TextLookup lkuEmployee 
         Height          =   315
         Left            =   1260
         TabIndex        =   7
         Top             =   705
         WhatsThisHelpID =   104242
         Width           =   1830
         _ExtentX        =   3228
         _ExtentY        =   556
         ForeColor       =   -2147483640
         IsSurrogateKey  =   -1  'True
         LookupID        =   "MFEmplo_HAI "
         ParentIDColumn  =   "EmployeeId"
         ParentKeyColumn =   "EmployeeKey"
         ParentTable     =   "tmfEmployee_HAI"
         BoundColumn     =   "EmployeeKey"
         BoundTable      =   "tmfEmployee_HAI"
         IsForeignKey    =   -1  'True
         sSQLReturnCols  =   "EmployeeId,lkuEmployee,;EmployeeName,txtEmployeeName,;"
      End
      Begin EntryLookupControls.TextLookup lkuProdEntryNo 
         Height          =   315
         Left            =   1260
         TabIndex        =   3
         Top             =   315
         WhatsThisHelpID =   104240
         Width           =   2145
         _ExtentX        =   3784
         _ExtentY        =   556
         ForeColor       =   -2147483640
         LookupMode      =   0
         FillOnJump      =   -1  'True
         WildCardChar    =   "?"
         LookupID        =   "workorder"
         BoundColumn     =   "WorkOrderNo"
         BoundTable      =   "tmfWorkOrdHead_HAI"
         sSQLReturnCols  =   "WorkOrderNo,lkuProdEntryNo,;RoutingId,,;VersionId,,;PriorityCode,,;CustID,,;CustPONo,,;"
      End
      Begin SOTACalendarControl.SOTACalendar calEntryDate 
         Height          =   315
         Left            =   4440
         TabIndex        =   5
         Top             =   270
         WhatsThisHelpID =   104241
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaskedText      =   "  /  /    "
         Text            =   "  /  /    "
      End
      Begin EntryLookupControls.TextLookup lkuWhse 
         Height          =   315
         Left            =   1260
         TabIndex        =   14
         Top             =   1545
         WhatsThisHelpID =   104246
         Width           =   1545
         _ExtentX        =   2725
         _ExtentY        =   556
         ForeColor       =   -2147483640
         IsSurrogateKey  =   -1  'True
         LookupID        =   "Warehouse"
         ParentIDColumn  =   "WhseID"
         ParentKeyColumn =   "WhseKey"
         ParentTable     =   "timWarehouse"
         BoundColumn     =   "WhseKey"
         BoundTable      =   "tmfWorkOrdHead_HAI"
         IsForeignKey    =   -1  'True
         sSQLReturnCols  =   "WhseID,lkuWhse,;Description,txtWhseDesc,;"
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtVersionId 
         Height          =   315
         Left            =   5040
         TabIndex        =   20
         Top             =   1950
         WhatsThisHelpID =   104249
         Width           =   705
         _Version        =   65536
         _ExtentX        =   1235
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin LookupViewControl.LookupView navRoutingId 
         Height          =   285
         Left            =   5760
         TabIndex        =   18
         TabStop         =   0   'False
         ToolTipText     =   "Lookup"
         Top             =   1980
         WhatsThisHelpID =   17371181
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   503
         LookupMode      =   1
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtRoutingId 
         Height          =   315
         Left            =   1260
         TabIndex        =   17
         Top             =   1950
         WhatsThisHelpID =   104248
         Width           =   2880
         _Version        =   65536
         _ExtentX        =   5080
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         lMaxLength      =   30
      End
      Begin NEWSOTALib.SOTANumber numQuantity 
         Height          =   315
         Left            =   1260
         TabIndex        =   22
         Top             =   2355
         WhatsThisHelpID =   104250
         Width           =   1605
         _Version        =   65536
         _ExtentX        =   2831
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>##|,###|,###|,##<ILp0>#<IRp0>|.####"
         text            =   "          0.0000"
         dMaxValue       =   99999999999.9999
         sIntegralPlaces =   11
         sDecimalPlaces  =   4
      End
      Begin NEWSOTALib.SOTANumber numScrapPcs 
         Height          =   315
         Left            =   3795
         TabIndex        =   24
         Top             =   2355
         WhatsThisHelpID =   104251
         Width           =   1200
         _Version        =   65536
         _ExtentX        =   2117
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>###|,##<ILp0>#<IRp0>|.##"
         text            =   "     0.00"
         dMinValue       =   -9999
         dMaxValue       =   999999
         sIntegralPlaces =   6
         sDecimalPlaces  =   2
         dValue          =   0
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtComment 
         Height          =   315
         Left            =   1260
         TabIndex        =   26
         Top             =   2760
         WhatsThisHelpID =   104252
         Width           =   5955
         _Version        =   65536
         _ExtentX        =   10504
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         lMaxLength      =   50
      End
      Begin EntryLookupControls.TextLookup lkuAutoDistBin 
         Height          =   315
         Left            =   5460
         TabIndex        =   30
         Top             =   2355
         WhatsThisHelpID =   17777141
         Width           =   1755
         _ExtentX        =   3096
         _ExtentY        =   556
         ForeColor       =   -2147483640
         LookupID        =   "DistBin"
         ParentIDColumn  =   "WhseBinID"
         ParentKeyColumn =   "WhseBinKey"
         ParentTable     =   "timWhseBin"
         BoundColumn     =   "WhseBinKey"
         BoundTable      =   "timWhseBin"
         sSQLReturnCols  =   "Bin,lkuAutoDistBin,;WhseBinKey,,;OnHand,,;Available,,;UOMKey,,;"
      End
      Begin VB.TextBox txtMultipleBin 
         Height          =   315
         Left            =   5460
         Locked          =   -1  'True
         TabIndex        =   51
         Text            =   "Multiple Bin"
         Top             =   2355
         WhatsThisHelpID =   17777142
         Width           =   1455
      End
      Begin VB.Label lblAutoDistLot 
         Height          =   255
         Left            =   6120
         TabIndex        =   52
         Top             =   2040
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label lblAutoDistBin 
         Caption         =   "Bin"
         Height          =   255
         Left            =   5130
         TabIndex        =   29
         Top             =   2400
         Width           =   270
      End
      Begin VB.Label lblComment 
         Caption         =   "Comment"
         Height          =   225
         Left            =   225
         TabIndex        =   25
         Top             =   2805
         Width           =   765
      End
      Begin VB.Label lblScrapPcs 
         AutoSize        =   -1  'True
         Caption         =   "Scrap Pcs"
         Height          =   195
         Left            =   3000
         TabIndex        =   23
         Top             =   2400
         Width           =   765
      End
      Begin VB.Label lblQuantity 
         Caption         =   "Quantity"
         Height          =   225
         Left            =   225
         TabIndex        =   21
         Top             =   2400
         Width           =   855
      End
      Begin VB.Label lblProdEntryNo 
         Caption         =   "Entry No."
         Height          =   315
         Left            =   225
         TabIndex        =   2
         Top             =   360
         Width           =   810
      End
      Begin VB.Label lblEntryDate 
         Caption         =   "Entry Date"
         Height          =   270
         Left            =   3540
         TabIndex        =   4
         Top             =   315
         Width           =   825
      End
      Begin VB.Label lblWhse 
         Caption         =   "Whse "
         Height          =   315
         Left            =   225
         TabIndex        =   13
         Top             =   1590
         Width           =   945
      End
      Begin VB.Label lblEmployee 
         Caption         =   "Employee"
         Height          =   285
         Left            =   225
         TabIndex        =   6
         Top             =   750
         Width           =   960
      End
      Begin VB.Label lblEmployeeName 
         Caption         =   "Name"
         Height          =   255
         Left            =   3225
         TabIndex        =   8
         Top             =   750
         Width           =   555
      End
      Begin VB.Label lblItem 
         Caption         =   "Item"
         Height          =   210
         Left            =   225
         TabIndex        =   10
         Top             =   1170
         Width           =   915
      End
      Begin VB.Label lblVersion 
         Caption         =   "Version "
         Height          =   255
         Left            =   4395
         TabIndex        =   19
         Top             =   1995
         Width           =   600
      End
      Begin VB.Label lblRouting 
         Caption         =   "Routing "
         Height          =   255
         Left            =   225
         TabIndex        =   16
         Top             =   1995
         Width           =   900
      End
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   42
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -10000
      TabIndex        =   41
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -10000
      TabIndex        =   40
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   48
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.ComboBox CustomCombo 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      ForeColor       =   &H80000012&
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   47
      TabStop         =   0   'False
      Text            =   "Combo1"
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   46
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin SOTAVM.SOTAValidationMgr valMgr 
      Left            =   -10000
      Top             =   180
      _ExtentX        =   741
      _ExtentY        =   661
      CtlsCount       =   0
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   6360
      WhatsThisHelpID =   73
      Width           =   9480
      _ExtentX        =   16722
      _ExtentY        =   688
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   9480
      _ExtentX        =   16722
      _ExtentY        =   741
      Style           =   4
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   43
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   44
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   45
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   49
      TabStop         =   0   'False
      Top             =   2880
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   615
      Index           =   0
      Left            =   -10000
      TabIndex        =   50
      TabStop         =   0   'False
      Top             =   720
      WhatsThisHelpID =   70
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1085
      _StockProps     =   0
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   39
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmmfzdz001"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************************************************************
'     Name: mfzdz001
'     Desc: MF Production Entry
'     Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
'     Original: 05-15-2001
'     Mods:
'Date           Project         SE      Description
'************************************************************************************************************************
'05-24-01       n/a             SVB     Change the message for not existing active routing
'06-08-01       20375           SVB     Copy the code from txtRoutingId_lostFocus to txtVersionId_lostFocus to do the same events
'06-11-01       20375           SVB     Mod code to able enter item from Routing with mult.parts produced add item validation by standard cost
'06-19-01       20711           DOM     setting form caption in form load
'07-06-01       20855           DOM     routing active and work order complete flags changed from char to smallint
'09-05-01       21238           DOM     fixing client errors for build
'03-19-02       23810           SVB     Added Public Function bGetRouting with setting MFItemClassKey on cmdValidProd_Click,
'                                       also added CompanyID restriction to navRoutingId and navVersionId
'11-11-03   MRD           BEJ            Decimal Precision Changes

Option Explicit

Private Declare Function AllowSetForegroundWindow Lib "user32" (ByVal dwProcessID As Long) As Boolean
Private Const ASFW_ANY = -1

Public moFormCust           As Object
Public moMapSrch            As New Collection
Private WithEvents moGM                As clsGridMgr
Attribute moGM.VB_VarHelpID = -1
Private moClass             As Object
Private mlRunMode           As Long
Private mbSaved             As Boolean
Private mbCancelShutDown    As Boolean
Private mbTempTblsCreated   As Boolean
Public moSotaObjects        As New Collection
Private miFilter            As Integer
Private moContextMenu       As clsContextMenu   '-- Right Click Context Menu Class
Private mbEnterAsTab        As Boolean
Public msCompanyID         As String
Public mlWorkOrderKey      As Long
Private mlRoutingKey        As Long
Private mlWorkCenterKey     As Long
Private mlOperationKey      As Long
Private mlMachineKey        As Long
Private msQTYPerDesc        As String
Private miActive            As Integer
Private msLastButton        As String
Private miTrack             As Integer
Private mlWorkOrderStepKey  As Long
Private mbProductionEntered As Boolean
Private msUserID            As String
Private mlLanguage          As Long
Private msNavRestrict       As String
Private miSecurityLevel     As Integer
Private miOldFormHeight As Long
Private miMinFormHeight As Long
Private miOldFormWidth As Long
Private miMinFormWidth As Long
Private mbLoadSuccess       As Boolean
Private mlBatchKey          As Long
Private mlBatchType         As Long

Private mbDoAutoDist        As Boolean
Private mbValidAutoDistBin  As Boolean
Private mbAutoDistBinClicked As Boolean
Private mbReturnToGrid      As Boolean
Private mbCheckSecurity     As Boolean


'Report Variables
Public msPrintButton            As String
Public msMFReportPath           As String
Public moReport                 As clsReportEngine
Public moDDData                 As clsDDData
Public moPrinter                As Printer
Private mbPeriodEnd             As Boolean
Public sRealTableCollection     As Collection
Public sWorkTableCollection     As Collection

'Labor Entry Variables
Private mdTotQuantity           As Double
Private mdQuantity              As Double
Private mdScrapPcs              As Double
Private mlTransKey              As Long
Private msTransNo               As String
Private mdRunHrs                As Double
Private mdRunCost               As Double
Private mdRunFix                As Double
Private mdRunVar                As Double
Private msOperCostToWip         As String
Private miShift                 As Integer
Private msTimeStamp             As String
Private msProgressStep          As String
Private moScrapDD               As Object
Private mbRange                 As Boolean
Private mbGridDirty             As Boolean
Private miNegQty                As Integer
Private mbLeaveCell             As Boolean
Public moScrapDist              As Object
Private mbScrapReason           As Boolean
Private miAllowDecQty           As Integer
Private mdScrapPcsSTD           As Double
Private mdDetlScrapPcsSTD       As Double
Private mdProgQTYReqSTD         As Double
Private mdQTYReqSTD             As Double
Private msStepID                As String
Private msTranNo                As String
Private mlKeyPressCnt           As Long

' Distribution Entry Variables for Header
Private moKitDist           As Object
Private mbInitKitDist       As Boolean
Private mbDistCreated       As Boolean
Private mbDistDirty         As Boolean
Private mdDistQty           As Double
Private miTranTypeNo        As Integer
Private mbLotControlled     As Boolean
Private miFormState         As Integer
Private mlStockUOMKey       As Long
Private mlTrackMeth         As Long

' Distribution Entry Variables for Grid
Private mlMatItemKey        As Long
Private mlDetlWhseKey       As Long
Private mbDistDetlDirty     As Boolean
Private mdMatQuantity       As Double
Private miMatTrackBins      As Integer
Private miMatTrackMethod    As Integer
Private mlMatStockUOMKey    As Long

'Variables for Storing Old Field Values
Private mdOldQTYReqSTD     As Double
Private mdOldRunHrs        As Double
Private mdOldSetupHrs      As Double
Private mdOldScrapPcs      As Double

'Deletions Const
Private Const kDeleteLineScrap = 0 'Delete Line Scrap record from Scrap temp table
Private Const kDeleteDetlScrap = 2 'Delete all Detail records from Scrap temp table

Private Const kColStepId = 1
Private Const kColOperationId = 2
Private Const kColOperationDesc1 = 3
Private Const kColOperationType = 4
Private Const kColMatItemId = 5
Private Const kColWorkCenterId = 6
Private Const kColMachineId = 7
Private Const kColQTYReqSTD = 8
'RKL DEJ (START)
'Renumber/Order columns from 8 on to insert these two fields
Private Const kColUOM = 9
Private Const kColQtyOnHand = 10
'RKL DEJ (STOP)
Private Const kColScrapPcsSTD = 11   'RKL DEJ Changed from 9 to 11
Private Const kColRunHrsSTD = 12   'RKL DEJ Changed from 10 to 12
Private Const kColSetHrsSTD = 13   'RKL DEJ Changed from 11 to 13
Private Const kColTotCostStd = 14   'RKL DEJ Changed from 12 to 14
Private Const kColTotCostTD = 15   'RKL DEJ Changed from 13 to 15
Private Const kColRunCostSTD = 16   'RKL DEJ Changed from 14 to 16
Private Const kColRunCostTD = 17   'RKL DEJ Changed from 15 to 17
Private Const kColSetCostSTD = 18   'RKL DEJ Changed from 16 to 18
Private Const kColSetCostTD = 19   'RKL DEJ Changed from 17 to 19
Private Const kColMatCostSTD = 20   'RKL DEJ Changed from 18 to 20
Private Const kColMatCostTD = 21   'RKL DEJ Changed from 19 to 21
Private Const kColQTYTD = 22   'RKL DEJ Changed from 20 to 22
Private Const kColScrapPcsTD = 23   'RKL DEJ Changed from 219 to 23
Private Const kColRunHrsTD = 24   'RKL DEJ Changed from 22 to 24
Private Const kColSetHrsTD = 25   'RKL DEJ Changed from 23 to 25
Private Const kColDownHrsSTD = 26   'RKL DEJ Changed from 24 to 26
Private Const kColProgressStep = 27   'RKL DEJ Changed from 25 to 27
Private Const kColWorkOrderKey = 28   'RKL DEJ Changed from 26 to 28
Private Const kColWorkOrderStepKey = 29   'RKL DEJ Changed from 27 to 29
Private Const kColBackFlush = 30   'RKL DEJ Changed from 28 to 30
Private Const kColWorkCenterKey = 31   'RKL DEJ Changed from 92 to 31
Private Const kColOperationKey = 32   'RKL DEJ Changed from 30 to 32
Private Const kColMachineKey = 33   'RKL DEJ Changed from 31 to 33
Private Const kColQTYPerDesc = 34   'RKL DEJ Changed from 32 to 34
Private Const kColOperCostToWip = 35   'RKL DEJ Changed from 33 to 35
Private Const kColWhseKey = 36   'RKL DEJ Changed from 34 to 36
Private Const kColMatItemKey = 37   'RKL DEJ Changed from 35 to 37
Private Const kColDetlTransKey = 38   'RKL DEJ Changed from 36 to 38
Private Const kColMatWhseId = 39   'RKL DEJ Changed from 37 to 39

Private Const kActive = 1

Private WithEvents moDmForm    As clsDmForm
Attribute moDmForm.VB_VarHelpID = -1

Public moDMGrid                As clsDmGrid
    
    'Decimal Precision Changes
    Public miPriceDecimal      As Integer          'for decimal values
    Public miCostDecimal       As Integer          'for decimal values
    Public miQtyDecimal        As Integer          'for decimal values
    Public miCurrDecPlaces     As Integer          'for decimal values
    Private moModOptions       As New clsModuleOptions
    
Const VBRIG_MODULE_ID_STRING = "frmmfzdz001.FRM"

Private Sub calEntryDate_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus calEntryDate, True
    #End If
'+++ End Customizer Code Push +++
    If Len(Trim(calEntryDate)) = 0 Then
        calEntryDate = Format(moClass.moSysSession.BusinessDate, "SHORT DATE")
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "calEntryDate_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub chkPhantomAsSubAss_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkPhantomAsSubAss, True
    #End If
'+++ End Customizer Code Push +++
    Dim sUser       As String
    Dim sID         As String
    Dim vPrompt     As Variant
    If mbCheckSecurity Then
        'Show security event form
        'Check the user id typed in can override (no cancel hit)
        sID = CStr(kMFSecPhantomAsSubAss)
        
        sUser = CStr(moClass.moSysSession.UserId)
        vPrompt = True
        If moClass.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt) = 0 Then
            'No Override reset Value
            mbCheckSecurity = False
            chkPhantomAsSubAss.Value = vbUnchecked
            mbCheckSecurity = True
            Exit Sub
        End If
   End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "chkPhantomAsSubAss_Click", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub cmdDetail_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdDetail, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
Dim lActiveRow As Long
Dim sWorkCenterId As String
Dim sMachineId As String
Dim sOperationId As String
Dim sOperDesc As String
Dim dRunHrs As Double
Dim dRunCost As Double
Dim sStepId As String
Dim sType As String
Dim sProgressStep As String
Dim sCurrentStep As String
Dim sBackFlush As String
Dim sMatItemId As String
Dim dQTYReqSTD As Double
Dim dScrapPcs As Double
Dim dSetupHrs As Double
Dim dSetCost As Double
Dim dMatCost As Double
Dim bReadOnly As Boolean
Dim iRange As Integer
Dim bKitDistrItem As Boolean
Dim dNewQTYReqSTD As Double
Dim dNewScrapPcs As Double
Dim dNewRunHrs As Double
Dim dNewSetupHrs As Double
Dim lDetlTransKey As Long

Dim uLineDetails As LineDetail
Static bDetailLoaded As Boolean
    
    Set frmMFLineDetail.oClass = moClass

    If Not bDetailLoaded Then
        Load frmMFLineDetail

        With frmMFLineDetail
            Set .oDist = moKitDist
            Set .oScrap = moScrapDist
        End With

        bDetailLoaded = True
    End If
    
    'Get the active row to see what data to pass into the Line Details form.
    lActiveRow = glGridGetActiveRow(grdMain)
    bReadOnly = False
    bKitDistrItem = False
    If tbrMain.SecurityLevel = sotaTB_DISPLAYONLY Then
        bReadOnly = True
    End If
     'Are we on a good grid row to go to the Line Details form?
    If lActiveRow > 0 Then
         'Read the grid line data to pass into the Line Details form.
        sWorkCenterId = gsGetValidStr(gsGridReadCell(grdMain, lActiveRow, kColWorkCenterId))
        sMachineId = gsGetValidStr(gsGridReadCell(grdMain, lActiveRow, kColMachineId))
        sOperationId = gsGetValidStr(gsGridReadCell(grdMain, lActiveRow, kColOperationId))
        sOperDesc = gsGetValidStr(gsGridReadCell(grdMain, lActiveRow, kColOperationDesc1))
        dRunHrs = gdGetValidDbl(gsGridReadCell(grdMain, lActiveRow, kColRunHrsSTD))
        dRunCost = gdGetValidDbl(gsGridReadCell(grdMain, lActiveRow, kColRunCostSTD))
        sStepId = gsGetValidStr(gsGridReadCell(grdMain, lActiveRow, kColStepId))
        sType = gsGetValidStr(gsGridReadCell(grdMain, lActiveRow, kColOperationType))
        sMatItemId = gsGetValidStr(gsGridReadCell(grdMain, lActiveRow, kColMatItemId))
        dQTYReqSTD = gdGetValidDbl(gsGridReadCell(grdMain, lActiveRow, kColQTYReqSTD))
        dScrapPcs = gdGetValidDbl(gsGridReadCell(grdMain, lActiveRow, kColScrapPcsSTD))
        dSetupHrs = gdGetValidDbl(gsGridReadCell(grdMain, lActiveRow, kColSetHrsSTD))
        dSetCost = gdGetValidDbl(gsGridReadCell(grdMain, lActiveRow, kColSetCostSTD))
        dMatCost = gdGetValidDbl(gsGridReadCell(grdMain, lActiveRow, kColMatCostSTD))
        sBackFlush = gsGetValidStr(gsGridReadCellText(grdMain, lActiveRow, kColBackFlush))
        sProgressStep = gsGridReadCell(grdMain, lActiveRow, kColProgressStep)
        lDetlTransKey = gsGridReadCell(grdMain, lActiveRow, kColWorkOrderStepKey)
        
        mdScrapPcsSTD = dScrapPcs
        mdQTYReqSTD = dQTYReqSTD
        If sBackFlush = "Y" Or sProgressStep = "Y" Then
            bReadOnly = True
        End If
        If sType <> "L" Then
            If bKitDistGridInfo(lActiveRow) Then
                bKitDistrItem = True
            End If
        End If
        
          'Populate the uLineDetails structure as per type definition.
            With uLineDetails
                .sWorkCenterId = sWorkCenterId
                .sMachineId = sMachineId
                .sOperationId = sOperationId
                .sOperDesc = sOperDesc
                .dRunHrs = dRunHrs
                .dRunCost = dRunCost
                .sStepId = sStepId
                .sType = sType
                .sMatItemId = sMatItemId
                .dQTYReqSTD = dQTYReqSTD
                .dScrapPcs = dScrapPcs
                .dSetupHrs = dSetupHrs
                .dSetCost = dSetCost
                .dMatCost = dMatCost
                .bReadOnly = bReadOnly
                .bKitDistrItem = bKitDistrItem
                .lWorkOrderStepKey = mlWorkOrderStepKey
                .lMatItemKey = mlMatItemKey
                .lDetlWhseKey = mlDetlWhseKey
                .lMatStockUOMKey = mlMatStockUOMKey
                .lItemKey = lkuItemId.KeyValue
                .lWorkOrderKey = mlWorkOrderKey
                .sTranNo = lkuProdEntryNo.Text
                .lDetlTransKey = lDetlTransKey
            End With
            
        frmMFLineDetail.ProcessLineDetails uLineDetails
        
        'Was some data changed on the Detail form?
            If frmMFLineDetail.IsDirty Then
                'Yes, some data WAS changed on the Detail form.
                'Populate line grid with uLineDetails data.
                With uLineDetails
                    dNewQTYReqSTD = .dQTYReqSTD
                    gGridUpdateCell grdMain, lActiveRow, kColQTYReqSTD, Format$(dNewQTYReqSTD)
                    moDMGrid.SetRowDirty lActiveRow
                    moDMGrid.Save
                    If dNewQTYReqSTD <> mdQTYReqSTD Then
                        If sType = "L" Then
                            iRange = 1
                            sCurrentStep = sStepId
                            WOCalc iRange, sCurrentStep
                            moDMGrid.Refresh
                            mbGridDirty = True
                        Else
                            iRange = 0
                            WOCalc2 mlWorkOrderStepKey, dNewQTYReqSTD
                            moDMGrid.Refresh
                        End If
                     End If
                     
                    dNewRunHrs = .dRunHrs
                    gGridUpdateCell grdMain, lActiveRow, kColRunHrsSTD, Format$(dNewRunHrs)
                    
                    
                    dNewScrapPcs = .dScrapPcs
                    gGridUpdateCell grdMain, lActiveRow, kColScrapPcsSTD, Format$(dNewScrapPcs)
                    If dNewScrapPcs <> mdScrapPcsSTD Then
                        CalculateScrap dNewScrapPcs, lActiveRow, msTranNo
                    End If
                    dNewSetupHrs = .dSetupHrs
                    gGridUpdateCell grdMain, lActiveRow, kColSetHrsSTD, Format$(dNewSetupHrs)
                    gGridUpdateCell grdMain, lActiveRow, kColDetlTransKey, CStr(.lDetlTransKey)
                    
                End With
                moDMGrid.SetRowDirty lActiveRow
                moDMGrid.Save
            End If
    
        'Now set all of the 'Old' values as everything was validated on the Detail Form.
        mdOldQTYReqSTD = gdGetValidDbl(gsGridReadCell(grdMain, lActiveRow, kColQTYReqSTD))
        mdOldRunHrs = gdGetValidDbl(gsGridReadCell(grdMain, lActiveRow, kColRunHrsSTD))
        mdOldSetupHrs = gdGetValidDbl(gsGridReadCell(grdMain, lActiveRow, kColSetHrsSTD))
        mdOldScrapPcs = gdGetValidDbl(gsGridReadCell(grdMain, lActiveRow, kColScrapPcsSTD))
    End If
    
    If mbReturnToGrid Then
        mbReturnToGrid = False
        gbSetFocus Me, grdMain
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cmdDetail_Click", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Sub


Private Sub cmdLotSerialBin_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdLotSerialBin, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    Dim dTranQty As Double
    Dim bInitKitDist As Boolean
    Dim lTransKey As Long
    Dim sSQL As String
    Dim rs As Object
    Dim bReadOnly As Boolean
    Dim iDelDist As Integer
    iDelDist = 3 ' 3 - Delete only Header record from temp table
    If tbrMain.SecurityLevel = sotaTB_DISPLAYONLY Then
        bReadOnly = True
    Else
        bReadOnly = False
    End If
    
    If mlTransKey = 0 Then
       bGetNextTransKey
    End If
    moKitDist.WarehouseKey = lkuWhse.KeyValue
    
    If bReadOnly Then
        PopulateDistTempTable mlTransKey, mlTransKey
    End If
    
    'If the user selected a bin to auto-distribute then clicked the Dist button,
    'do the auto-distribution now so they can see it when the dialog appears.
    If bReadOnly = False And mbDoAutoDist = True Then
        bAutoDistFromBinLku
    End If
    
    lTransKey = moKitDist.EditDistribution(9002, _
                CLng(lkuItemId.KeyValue), CLng(mlStockUOMKey), _
                CDbl(mdQuantity), dTranQty, _
                mlTransKey, , , , , bReadOnly, True)
                    
    If moKitDist.oError.Number <> 0 Then
            giSotaMsgBox Me, moClass.moSysSession, moKitDist.oError.MessageConstant
            mbDistCreated = False
            moKitDist.oError.Clear
            Exit Sub
    End If
    
    'Set the value of the auto-dist bin lookup based on what is saved during EditDistribution.
    GetAutoDistSavedInfo
    
    If Not moKitDist.CancelClicked Then
       
        mbDistDirty = True
        mdDistQty = gdGetValidDbl(dTranQty)

        ' If user has not given any value then over ride the quantity from distribution
        If numQuantity.Value = 0# Then
            numQuantity.Value = mdDistQty / 1
            numQuantity_LostFocus
        End If

        mbDistCreated = True
    Else
        If Not mbDistDirty Then
            If tbrMain.SecurityLevel <> sotaTB_DISPLAYONLY Then
                DeleteKitDist (iDelDist)
                lTransKey = 0
            End If
        End If
    End If
    If bReadOnly Then
        DeleteKitDist (iDelDist)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cmdLotSerialBin_Click", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub PopulateDistTempTable(lReadTransKey As Long, lSetTransKey As Long)
'+++ VB/Rig Begin Push +++                                                                'MAS 500 Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'MAS 500 Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
    Dim sSQL As String
    
    sSQL = "INSERT INTO #timInvtDistWrk (ItemKey ,TranIdentifier, LineNumber, BinID, CompanyID, " _
                   & " ExpirationDate, InvtLotBinKey, LotKey, SerialKey, LotNo, QtyAvailable, " _
                   & " SerialNo, StockQty, TranQty, UOMKey, WhseBinKey)"
    sSQL = sSQL & " SELECT PartItemKey ," & glGetValidLong(lSetTransKey) & ",LineNumber, BinID, CompanyID, " _
                   & " ExpirationDate, InvtLotBinKey, InvtLotKey, InvtSerialKey,  LotNo, QtyAvailable, " _
                   & " SerialNo, StockQty, TranQty, UnitMeasKey, WhseBinKey " _
                   & " FROM tmfWOTranDist_HAI  WITH (NOLOCK)" _
                   & " WHERE  CompanyID = " & gsQuoted(msCompanyID) _
                   & " AND TransactionKey = " & glGetValidLong(lReadTransKey)
    moClass.moAppDB.ExecuteSQL sSQL
    
 '+++ VB/Rig Begin Pop +++                                                                 'MAS 500 Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'MAS 500 Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "PopulateDistTempTable", VBRIG_IS_FORM                  'MAS 500 Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub cmdValidProd_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdValidProd, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
   Dim iActionCode As Integer
   Dim bInitKitDist As Boolean
   Dim sPrefWhseBinID As String
   Dim lPrefWhseBinKey As Long
   
    mbProductionEntered = False
    If bValidEntry(True) Then
        If bValidStep = True Then
            If Not gbPostDateCheck(Me, moClass, calEntryDate, lblEntryDate, 9002) Then
                calEntryDate.Protected = False
                gbSetFocus Me, calEntryDate
                Exit Sub
            End If
            iActionCode = moDmForm.Save(True)
            If iActionCode = kDmSuccess Then
                CopyMMedia
                DisableContr
                If (miTrack = 1 Or mbLotControlled = True) Then     ' Or mlTrackMeth = 3 Then
                    cmdLotSerialBin.Enabled = True
                Else
                    cmdLotSerialBin.Enabled = False
                End If
                
                lkuAutoDistBin.Enabled = moKitDist.bEnableBinLkuCtrl(lkuWhse.KeyValue, lkuItemId.KeyValue)
                'For put-away transactions or increase to inventory, we always default the preferred bin.
                If lkuAutoDistBin.Enabled = True Then
                    'Bring the bin lookup to the front.
                    lkuAutoDistBin.ZOrder 0
                     moKitDist.GetPrefBinForInvtItem lkuWhse.KeyValue, lkuItemId.KeyValue, 1, sPrefWhseBinID, lPrefWhseBinKey, miTranTypeNo
                     lkuAutoDistBin.SetTextAndKeyValue sPrefWhseBinID, lPrefWhseBinKey
                    'If item is non-track, see if the auto-dist flag can be set.
                    'If it is a lot-tracked, do not set flag because we don't know
                    'the lot info yet.  User has to select it from the auto-bin lookup
                    'or use the Dist UI.
                    If lkuAutoDistBin.Enabled = True And Len(Trim(lkuAutoDistBin.Text)) <> 0 Then
                        lkuAutoDistBin_Validate False
                        If mbValidAutoDistBin = True Then
                            lkuAutoDistBin.Tag = lkuAutoDistBin.Text
                            mbDoAutoDist = True
                        End If
                    End If
                End If
                    
                If Not bValidProduction Then
                    Exit Sub
                Else
                    CheckBackFlush
                    SetTags
                    tbrMain.ButtonEnabled(kTbProceed) = True
                    cmdDetail.Enabled = True
                End If
            End If
        End If
        mbProductionEntered = True
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cmdValidProd_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub WOCalc(iRange As Integer, sCurrentStep As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    With moClass.moAppDB
        .SetInParamStr msCompanyID
        .SetInParamLong mlWorkOrderKey
        .SetInParamInt 0
        .SetInParam iRange
        .SetInParam sCurrentStep
        .ExecuteSP "spmfWOCalc"
        .ReleaseParams
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WOCalc", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub WOCalc2(lWorkOrderStepKey As Long, dQTYReqSTD As Double)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    With moClass.moAppDB
        .SetInParamStr msCompanyID
        .SetInParamLong mlWorkOrderKey
        .SetInParamLong lWorkOrderStepKey
        .SetInParam dQTYReqSTD
        .SetInParam 0
        .SetInParam 0
        .ExecuteSP "spmfWOCalc2"
        .ReleaseParams
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WOCalc2", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Function bValidStep() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim lRetVal As Long
    Dim sStepId As String
    Dim sString As String
  
    bValidStep = False
    With moClass.moAppDB
        .SetInParamLong mlRoutingKey
        .SetInParamLong lkuWhse.KeyValue
        .SetInParamStr msCompanyID
        .SetInParamLong 0
        .SetOutParam lRetVal
        .SetOutParam sStepId
        .ExecuteSP "spmfValidateWorkOrder"
         lRetVal = .GetOutParam(5)
         sStepId = .GetOutParam(6)
        .ReleaseParams
    End With
    sString = "on Step " & sStepId & ""
    If lRetVal = 0 Then
        bValidStep = True
    Else
        Select Case lRetVal
            Case -1
                giSotaMsgBox Me, moClass.moSysSession, kmsgMFNoInvRecExists, msStepID
            Case -3
                giSotaMsgBox Me, moClass.moSysSession, kmsgMFBackflushNonTrackOnly, msStepID
            Case -4
                giSotaMsgBox Me, moClass.moSysSession, kmsgMFOutsideWCannotbeBlank, msStepID
            Case -5
                giSotaMsgBox Me, moClass.moSysSession, kmsgMFOutsideWC, msStepID
            Case -6
                giSotaMsgBox Me, moClass.moSysSession, kmsgMFOnlyOneProgressStep
            Case -7
                giSotaMsgBox Me, moClass.moSysSession, kmsgIMSInvtInvalidStatus2, sString
            Case -8
                giSotaMsgBox Me, moClass.moSysSession, 320161, "Machine", sString
            Case -9
                giSotaMsgBox Me, moClass.moSysSession, 320161, "Tool", sString
            Case -10
                giSotaMsgBox Me, moClass.moSysSession, kmsgMFStepBackflushBinTemp, sStepId
            Case -11
                giSotaMsgBox Me, moClass.moSysSession, kmsgMFStepBackflushMissingBin, sStepId
            Case -12
                giSotaMsgBox Me, moClass.moSysSession, kmsgMFStepBackflushBinNoInWhse, sStepId
            Case -13
                giSotaMsgBox Me, moClass.moSysSession, kmsgMFStepBackflushBinNotActiv, sStepId
            Case -14
                giSotaMsgBox Me, moClass.moSysSession, kmsgMFStepBackflushBinRandom, sStepId
        End Select
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bValidStep", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Private Function bWOCreateSteps() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sBackFlush As String
sBackFlush = kChBoxNo
 
bWOCreateSteps = False
    Screen.MousePointer = vbHourglass
    sbrMain.Status = SOTA_SB_BUSY
    sbrMain.Message = "Creating Steps...Please Wait..."
    With moClass.moAppDB
        .SetInParamStr msCompanyID
        .SetInParamLong mlWorkOrderKey
        .SetInParamLong mlRoutingKey
        .SetInParamInt 0 'Level
        .SetInParamInt chkPhantomAsSubAss.Value
        .SetInParamStr sBackFlush ''N'
        .SetInParamInt 1 'PhantomMaterReqPer
        .SetInParamLong 0 'PhantomItemKey
        .SetOutParam 0 'RowKey
        .SetInParamInt 0 'InclCommentOnlyMatItem
        .ExecuteSP "spmfWOCreateSteps"
        .ReleaseParams
    End With
    sbrMain.Message = ""
    sbrMain.Status = SOTA_SB_ADD
    Screen.MousePointer = vbDefault
bWOCreateSteps = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bWOCreateSteps", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub cmdDetail_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdDetail, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDetail_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDetail_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdDetail, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDetail_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdLotSerialBin_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdLotSerialBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdLotSerialBin_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdLotSerialBin_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdLotSerialBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdLotSerialBin_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdValidProd_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdValidProd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdValidProd_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdValidProd_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdValidProd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdValidProd_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub txtWhseDesc_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtWhseDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtWhseDesc_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtWhseDesc_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtWhseDesc, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtWhseDesc_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtWhseDesc_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtWhseDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtWhseDesc_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtWhseDesc_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtWhseDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtWhseDesc_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemDesc_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtItemDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemDesc_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemDesc_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtItemDesc, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemDesc_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemDesc_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtItemDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemDesc_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemDesc_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtItemDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemDesc_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtEmployeeName_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtEmployeeName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtEmployeeName_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtEmployeeName_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtEmployeeName, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtEmployeeName_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtEmployeeName_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtEmployeeName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtEmployeeName_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtEmployeeName_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtEmployeeName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtEmployeeName_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtVersionId_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtVersionId, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtVersionId_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtVersionId_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtVersionId, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtVersionId_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtVersionId_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtVersionId, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtVersionId_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRoutingId_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtRoutingId, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRoutingId_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRoutingId_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtRoutingId, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRoutingId_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRoutingId_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtRoutingId, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRoutingId_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtComment_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtComment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtComment_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtComment_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtComment, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtComment_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtComment_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtComment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtComment_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMultipleBin_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtMultipleBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMultipleBin_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMultipleBin_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtMultipleBin, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMultipleBin_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMultipleBin_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtMultipleBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMultipleBin_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMultipleBin_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtMultipleBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMultipleBin_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItemId_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuItemId, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItemId_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItemId_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuItemId, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItemId_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItemId_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuItemId, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItemId_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItemId_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuItemId, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItemId_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItemId_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuItemId, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItemId_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuEmployee_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuEmployee, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuEmployee_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuEmployee_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuEmployee, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuEmployee_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuEmployee_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuEmployee, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuEmployee_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuEmployee_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuEmployee, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuEmployee_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuEmployee_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuEmployee, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuEmployee_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProdEntryNo_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuProdEntryNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProdEntryNo_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProdEntryNo_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuProdEntryNo, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProdEntryNo_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProdEntryNo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuProdEntryNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProdEntryNo_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProdEntryNo_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuProdEntryNo, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProdEntryNo_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProdEntryNo_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuProdEntryNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProdEntryNo_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhse_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhse_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhse_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuWhse, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhse_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhse_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhse_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhse_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuWhse, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhse_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhse_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhse_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuAutoDistBin_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuAutoDistBin_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuAutoDistBin_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuAutoDistBin, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuAutoDistBin_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuAutoDistBin_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuAutoDistBin_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuAutoDistBin_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuAutoDistBin_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub numQuantity_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numQuantity, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numQuantity_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numQuantity_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numQuantity, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numQuantity_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numScrapPcs_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numScrapPcs, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numScrapPcs_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numScrapPcs_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numScrapPcs, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numScrapPcs_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numScrapPcs_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numScrapPcs, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numScrapPcs_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPhantomAsSubAss_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkPhantomAsSubAss, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPhantomAsSubAss_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPhantomAsSubAss_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkPhantomAsSubAss, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPhantomAsSubAss_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPrintProduction_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkPrintProduction, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPrintProduction_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPrintProduction_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkPrintProduction, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPrintProduction_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPrintProduction_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkPrintProduction, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPrintProduction_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnOutput_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnOutput, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnOutput_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnOutput_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnOutput, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnOutput_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnOutput_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnOutput, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnOutput_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnOutput_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnOutput, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnOutput_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calEntryDate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange calEntryDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calEntryDate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calEntryDate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress calEntryDate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calEntryDate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calEntryDate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus calEntryDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calEntryDate_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomSpin_SpinDown(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomSpin_SpinDown", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomSpin_SpinUp(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomSpin_SpinUp", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
If Me.WindowState = vbNormal Or Me.WindowState = vbMaximized Then
        gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, _
fraLaborEntry, grdMain

        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, _
FraProduction, fraLaborEntry, grdMain
        
End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub




Private Sub grdMain_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
If tbrMain.SecurityLevel = sotaTB_DISPLAYONLY Then
    cmdDetail.Enabled = True
End If
If grdMain.Lock = False Then
    mdScrapPcsSTD = gdGetValidDbl(gsGridReadCell(grdMain, Row, kColScrapPcsSTD))
    mdQTYReqSTD = gdGetValidDbl(gsGridReadCell(grdMain, Row, kColQTYReqSTD))
End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdMain_Click", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdMain_EditChange(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

If Col = kColQTYReqSTD Then
    moDMGrid.SetRowDirty Row
    mbLeaveCell = True
End If
If Col = kColScrapPcsSTD Then
    moDMGrid.SetRowDirty Row
'    cmdScrapReason.Enabled = True
End If
If Col = kColSetHrsSTD Then
    moDMGrid.SetRowDirty Row
End If
If Col = kColRunHrsSTD Then
    moDMGrid.SetRowDirty Row
End If
If Col = kColSetHrsSTD Then
    moDMGrid.SetRowDirty Row
End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdMain_EditChange", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub grdMain_KeyDown(KeyCode As Integer, Shift As Integer)

Dim lRow As Long
Dim lCol As Long

'mlKeyPressCnt is reset to 0 each time user leaves a cell.
If mlKeyPressCnt = 0 Then
    lRow = glGridGetActiveRow(grdMain)
    lCol = glGridGetActiveCol(grdMain)
    If grdMain.Lock = False Then
        mdScrapPcsSTD = gdGetValidDbl(gsGridReadCell(grdMain, lRow, kColScrapPcsSTD))
        mdQTYReqSTD = gdGetValidDbl(gsGridReadCell(grdMain, lRow, kColQTYReqSTD))
    End If
    'Add to the mlKeyPressCnt so the value in variable that hold the old values will not be set again.
    'each time the user presses a key.
    mlKeyPressCnt = mlKeyPressCnt + 1
End If
End Sub

Private Sub grdMain_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGM.Grid_LeaveCell Col, Row, NewCol, NewRow
Dim sOperationType      As String
Dim sCurrentStep        As String
Dim iRange              As Integer
Dim dQTYReqSTD          As Double
Dim lWorkOrderStepKey   As Long
Dim dScrapPcs           As Double
Dim sStepId             As String
Dim sTranNo             As String
Dim dCellScrapPcs       As Double
Dim iDelAll             As Integer



mlKeyPressCnt = 0
If Col = kColQTYReqSTD Then
    dQTYReqSTD = gdGetValidDbl(gsGridReadCell(grdMain, Row, kColQTYReqSTD))
    If mdQTYReqSTD = dQTYReqSTD Then Exit Sub
    If grdMain.Lock = False Then
        If dQTYReqSTD < 0 Then
            Cancel = True
            dQTYReqSTD = 0
            moDMGrid.SetCellValue Row, Col, SQL_DECIMAL, dQTYReqSTD
            gGridSetActiveCell grdMain, Row, kColQTYReqSTD
        End If
        moDMGrid.Save
        lWorkOrderStepKey = glGetValidLong(gsGridReadCell(grdMain, Row, kColWorkOrderStepKey))
        
        If Not bValidProductionGridEntry(lWorkOrderStepKey) Then
            Cancel = True
            mbLeaveCell = False
               
        Else
            sOperationType = gsGridReadCellText(grdMain, Row, kColOperationType)
            If sOperationType = "L" Then
                iRange = 1
                sCurrentStep = gsGridReadCellText(grdMain, Row, kColStepId)
                WOCalc iRange, sCurrentStep
                moDMGrid.Refresh
                mbGridDirty = True
            Else
                iRange = 0
                WOCalc2 lWorkOrderStepKey, dQTYReqSTD
                moDMGrid.Refresh
            End If
            
            'SGS PTC IA (Resolves buggy active cell stuff)
            If NewRow = -1 Then
                gGridSetActiveCell grdMain, Row, kColQTYReqSTD
            Else
                gGridSetActiveCell grdMain, NewRow, kColQTYReqSTD
            End If
            mdQTYReqSTD = dQTYReqSTD
            'SGS PTC IA (End)
       End If
    
    End If
End If

If Col = kColScrapPcsSTD Then
    dScrapPcs = gdGetValidDbl(gsGridReadCell(grdMain, Row, kColScrapPcsSTD))
    If mdScrapPcsSTD = dScrapPcs Then Exit Sub
    If grdMain.Lock = False Then
        lWorkOrderStepKey = glGetValidLong(gsGridReadCell(grdMain, Row, kColWorkOrderStepKey))
        sStepId = gsGetValidStr(gsGridReadCell(grdMain, Row, kColStepId))
        sTranNo = (lkuProdEntryNo.Text + "-" + sStepId)
            If dScrapPcs < 0 Then
                Cancel = True
                dScrapPcs = 0
                moDMGrid.SetCellValue Row, Col, SQL_DECIMAL, dScrapPcs
            End If
            moDMGrid.Save
            If Not bValidProductionGridEntry(lWorkOrderStepKey) Then
                Cancel = True
                mbLeaveCell = False
            Else
                CalculateScrap dScrapPcs, Row, sTranNo
                gGridSetSelectRow grdMain, Row
            End If
    End If
 End If
 
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdMain_LeaveCell", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub CalculateScrap(dScrapPcs As Double, Row As Long, sTranNo As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

Dim dScrap As Double
Dim sCurrentStep As String
Dim dQTYReqSTD   As Double
Dim iRange       As Integer
Dim iDelAll      As Integer

iDelAll = kDeleteLineScrap
iRange = 1


If mdScrapPcsSTD <> dScrapPcs Then
    dQTYReqSTD = gdGetValidDbl(gsGridReadCell(grdMain, Row, kColQTYReqSTD))
    If dScrapPcs <> 0 Then
        dScrap = dScrapPcs - mdScrapPcsSTD
        gGridUpdateCell grdMain, Row, kColQTYReqSTD, (dQTYReqSTD + dScrap)
        moDMGrid.SetRowDirty Row
        moDMGrid.Save
        sCurrentStep = gsGridReadCellText(grdMain, Row, kColStepId)
        WOCalc iRange, sCurrentStep
        moDMGrid.Refresh
        mdScrapPcsSTD = dScrapPcs
        mbGridDirty = True
    Else
        dScrap = mdScrapPcsSTD - dScrapPcs
        gGridUpdateCell grdMain, Row, kColQTYReqSTD, (dQTYReqSTD - dScrap)
        moDMGrid.SetRowDirty Row
        moDMGrid.Save
        sCurrentStep = gsGridReadCellText(grdMain, Row, kColStepId)
        WOCalc iRange, sCurrentStep
        moDMGrid.Refresh
        mdScrapPcsSTD = dScrapPcs
        mbGridDirty = True
        DeleteScrap sTranNo, iDelAll
    End If
End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CalculateScrap", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdMain_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
    If Me.ActiveControl Is cmdDetail Then
        mbReturnToGrid = True
    Else
        mbReturnToGrid = False
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdMain_LostFocus", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

End Sub

Private Sub lkuAutoDistBin_BeforeValidate(Text As String, iReturn As EntryLookupControls.ENTRY_LKU_CTRL_ValidateTypes)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Validate the lookup control myself through local routine.
    iReturn = EL_CTRL_VALID
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuAutoDistBin_BeforeValidate", VBRIG_IS_FORM          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuAutoDistBin_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuAutoDistBin, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

    'Set flag use to determine if user has selected a value from the lookup section grid.
    'Flag is reset to False after auto-distribution is saved.
    If colSQLReturnVal.Count <> 0 Then
        mbAutoDistBinClicked = True
        'If the item is lot tracked and the user selected the same bin but a different lot
        'from the lookup, set the tag to blank so the new lot/bin can be validated.
        If mlTrackMeth = 1 And LCase(Trim(lkuAutoDistBin.Text)) = LCase(Trim(colSQLReturnVal(1))) Then
            If LCase(Trim(lblAutoDistLot.Caption)) <> LCase(Trim(colSQLReturnVal(3))) Then
                lkuAutoDistBin.Tag = ""
            End If
        End If
    Else
        mbAutoDistBinClicked = False
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuAutoDistBin_BeforeLookupReturn", VBRIG_IS_FORM      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuAutoDistBin_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuAutoDistBin, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    
    Dim lItemKey As Long
    Dim lUOMKey As Long
    Dim lInvtTranKey As Long
    Dim dTranQty As Double
    
    lItemKey = lkuItemId.KeyValue
    lUOMKey = mlStockUOMKey
    lInvtTranKey = 0
    dTranQty = mdQuantity

    moKitDist.bAutoDistBinLkuSetupClick lkuAutoDistBin, lkuWhse.KeyValue, lItemKey, 9002, lUOMKey, dTranQty, lInvtTranKey

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuAutoDistBin_LookupClick", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuAutoDistBin_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

    'Bring the bin lookup to the front.
    lkuAutoDistBin.ZOrder 0
    
    If mbValidAutoDistBin = True And lkuAutoDistBin.IsValid = True Then
        lkuAutoDistBin.Tag = lkuAutoDistBin.Text
        mbDoAutoDist = True
    Else
        mbDoAutoDist = False
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuAutoDistBin_LostFocus", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuAutoDistBin_Validate(Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim colAutoDistBinRetCol As Collection
    Dim sAutoDistBinID As String
    Dim dAutoDistBinQtyAvail As Double
    Dim lAutoDistBinUOMKey As Long
    Dim sAutoDistLotNo As String
    Dim sAutoDistLotExpDate As String
    Dim bCanAutoDist As Boolean
    Dim lRow As Long

    'This validation routine is designed to tell if the bin can be used
    'to auto-distribute the transaction.  Based on the tracking method,
    'it will try to see if there is enough information to do the auto-
    'distribution.  For example: Lot-tracked items require a lot number.
    
    'We will try to use the return column collection if we detect the user
    'selected a value by clicking the lookup, otherwise we will use the bin
    'itself to get the distribution info.  If the info is lacking, we will
    'raise an error stating the bin is invalid.
    
    If Me.ActiveControl <> numQuantity And _
       Me.ActiveControl <> lkuAutoDistBin And Me.ActiveControl <> cmdValidProd Then
        Exit Sub
    End If
    
    mbValidAutoDistBin = False
    
    'Check if the bin's value changed OR if the user changed the quantity / UOM
    '(signified by the Me.ActiveControl.Name not being equal the bin lookup control).
    If Len(lkuAutoDistBin.Text) <> 0 And ((Me.ActiveControl.Name <> "lkuAutoDistBin") Or _
       (LCase(Trim(lkuAutoDistBin.Text)) <> LCase(Trim(lkuAutoDistBin.Tag)))) Then

        If mbAutoDistBinClicked = True Then
            If lkuAutoDistBin.ReturnColumnValues.Count <> 0 Then
                bCanAutoDist = True
                Set colAutoDistBinRetCol = lkuAutoDistBin.ReturnColumnValues
                moKitDist.GetAutoDistInfoFromRetCol lkuItemId.KeyValue, colAutoDistBinRetCol, sAutoDistBinID, _
                                                    dAutoDistBinQtyAvail, lAutoDistBinUOMKey, _
                                                    sAutoDistLotNo, sAutoDistLotExpDate
                
                'Check if we can auto-distribute against the bin.
                bCanAutoDist = moKitDist.bIsValidBinForAutoDist(lkuWhse.KeyValue, lkuItemId.KeyValue, lkuAutoDistBin.Text, _
                                                 9002, gdGetValidDbl(mdQuantity), _
                                                 mlStockUOMKey, moClass.moSysSession.BusinessDate, _
                                                 0, dAutoDistBinQtyAvail, lAutoDistBinUOMKey, _
                                                 sAutoDistLotNo, sAutoDistLotExpDate)

                If bCanAutoDist Then
                    'If a lot number exist,
                    lblAutoDistLot.Caption = sAutoDistLotNo
                End If

            End If
        Else
            'User likely typed in the bin.
            sAutoDistBinID = Trim(lkuAutoDistBin.Text)
            sAutoDistLotNo = lblAutoDistLot.Caption
            
            'Check if we can auto-distribute against the bin.
            bCanAutoDist = moKitDist.bIsValidBinForAutoDist(lkuWhse.KeyValue, lkuItemId.KeyValue, lkuAutoDistBin.Text, _
                                             9002, gdGetValidDbl(mdQuantity), _
                                             mlStockUOMKey, moClass.moSysSession.BusinessDate, _
                                             0, dAutoDistBinQtyAvail, lAutoDistBinUOMKey, _
                                             sAutoDistLotNo, sAutoDistLotExpDate)
        End If
        
        'Check if we can do an auto-distribution.
        If bCanAutoDist = False And Cancel = False Then
            mbValidAutoDistBin = False
            lkuAutoDistBin.ClearData
            lkuAutoDistBin.SetFocus
            lkuAutoDistBin.Tag = ""
            lblAutoDistLot.Caption = ""
        Else
            mbValidAutoDistBin = True
        End If
        
        'Must reset to false in case user decides to type a value after using the lookup.  If it is not reset,
        'it will use the lku's return columns collection which will have the wrong values.
        mbAutoDistBinClicked = False
    End If
   
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuAutoDistBin_Validate", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuEmployee_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuEmployee, True
    #End If
'+++ End Customizer Code Push +++
Dim iEmplStatus As Integer
If lkuEmployee.KeyValue <> 0 Then
    iEmplStatus = giGetValidInt(moClass.moAppDB.Lookup("EmployeeStatus", "tmfEmployee_HAI WITH (NOLOCK)", "EmployeeKey = " & glGetValidLong(lkuEmployee.KeyValue) & " AND CompanyID = " & gsQuoted(msCompanyID)))
    If iEmplStatus <> 1 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgMFInactiveEmployee, lkuEmployee.Text
        lkuEmployee.Text = ""
        gbSetFocus Me, lkuEmployee
        Exit Sub
    Else
        txtEmployeeName = gsGetValidStr(moClass.moAppDB.Lookup("EmployeeName", "tmfEmployee_HAI WITH (NOLOCK)", "EmployeeKey = " & glGetValidLong(lkuEmployee.KeyValue) & " AND CompanyID = " & gsQuoted(msCompanyID)))
    End If
End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuEmployee_LostFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuEmployee_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuEmployee, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    lkuEmployee.RestrictClause = "tmfEmployee_HAI.EmployeeStatus = 1" _
& " AND CompanyID = " & gsQuoted(msCompanyID)

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuEmployee_LookupClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuItemId_BeforeValidate(Text As String, iReturn As EntryLookupControls.ENTRY_LKU_CTRL_ValidateTypes)
lkuItemId.Text = Text
End Sub

Private Sub lkuItemId_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuItemId, True
    #End If
'+++ End Customizer Code Push +++
    If lkuItemId.Protected Then
        Exit Sub
    End If
    
    If Len(Trim(lkuItemId)) <> 0 Then
        If Not bValidEntry(False) Then
            gbSetFocus Me, lkuItemId
        Else
            miAllowDecQty = giGetValidInt(moClass.moAppDB.Lookup("AllowDecimalQty", "timItem WITH (NOLOCK)", _
                            "ItemKey = " & Str(lkuItemId.KeyValue) _
                          & " AND CompanyID = " & gsQuoted(msCompanyID)))
        End If
    Else
        txtItemDesc = ""
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuItemId_LostFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuItemId_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuItemId, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    Dim sRestrictClause As String

    sRestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    sRestrictClause = sRestrictClause & " AND ItemKey IN (SELECT a.ItemKey FROM timInventory a WITH (NOLOCK)"
    sRestrictClause = sRestrictClause & " INNER JOIN timItem b WITH (NOLOCK) ON a.ItemKey = b.ItemKey"
    sRestrictClause = sRestrictClause & " WHERE b.CompanyID = " & gsQuoted(msCompanyID) & ")"

    If Len(Trim(txtRoutingId)) = 0 Then
        sRestrictClause = sRestrictClause & " AND ItemKey IN (SELECT c.ItemKey FROM tmfRoutProd_HAI c WITH (NOLOCK)"
        sRestrictClause = sRestrictClause & " INNER JOIN tmfRoutHead_HAI d WITH (NOLOCK) ON c.RoutingKey = d.RoutingKey"
        sRestrictClause = sRestrictClause & " WHERE d.Active = " & kRoutActive
        sRestrictClause = sRestrictClause & " AND d.CompanyID = " & gsQuoted(msCompanyID) & ")"
        sRestrictClause = sRestrictClause & " AND Status = " & kActive
    Else
        sRestrictClause = sRestrictClause & " AND ItemKey IN (SELECT c.ItemKey FROM tmfRoutProd_HAI c WITH (NOLOCK)"
        sRestrictClause = sRestrictClause & " INNER JOIN tmfRoutHead_HAI d WITH (NOLOCK) ON c.RoutingKey = d.RoutingKey"
        sRestrictClause = sRestrictClause & " WHERE d.RoutingKey = " & glGetValidLong(mlRoutingKey)
        sRestrictClause = sRestrictClause & " AND d.Active = " & kRoutActive
        sRestrictClause = sRestrictClause & " AND d.CompanyID = " & gsQuoted(msCompanyID) & ")"
        sRestrictClause = sRestrictClause & " AND Status = " & kActive
    End If

    lkuItemId.RestrictClause = sRestrictClause

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuItemId_LookupClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuProdEntryNo_BeforeValidate(Text As String, iReturn As EntryLookupControls.ENTRY_LKU_CTRL_ValidateTypes)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    If Not lkuProdEntryNo.EnabledText Then
        
            valMgr_KeyChange
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuProdEntryNo_BeforeValidate", VBRIG_IS_FORM          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub lkuProdEntryNo_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuProdEntryNo, True
    #End If
'+++ End Customizer Code Push +++
    If lkuProdEntryNo.Protected = False Then
        If IsNumeric(lkuProdEntryNo) Then
            lkuProdEntryNo = gsMFZeroFill(lkuProdEntryNo, 8)
        End If
        
        valMgr_KeyChange
        
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuProdEntryNo_LostFocus", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuProdEntryNo_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuProdEntryNo, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = Not bConfirmUnload(0, True)
    lkuProdEntryNo.RestrictClause = msNavRestrict
                                    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuProdEntryNo_LookupClick", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuProdEntryNo_Validate(Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    If lkuProdEntryNo.Text <> "" Then
    
        If Not bIsValidProductionWorkOrder Then
        
            Cancel = True
            lkuProdEntryNo.Text = ""
            gbSetFocus Me, lkuProdEntryNo
        
        End If
    
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuProdEntryNo_Validate", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuWhse_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuWhse, True
    #End If
'+++ End Customizer Code Push +++
    If lkuWhse.Protected Then
        Exit Sub
    End If
    
    If Len(Trim(lkuItemId)) <> 0 Then
        If Not bValidEntry(False) Then
            gbSetFocus Me, lkuWhse
        Else
            If txtRoutingId.Text = "" Then
                GetStdRout
            End If
        End If
    End If
    
    If Len(Trim(lkuWhse)) = 0 Then
        txtWhseDesc.Text = ""
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuWhse_LostFocus", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuWhse_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuWhse, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    If Len(Trim(lkuItemId.Text)) <> 0 Then
        If lkuItemId.KeyValue <> 0 Then
            lkuWhse.RestrictClause = "WhseKey IN (" _
                                   & " SELECT i.WhseKey FROM timInventory i WITH (NOLOCK)" _
                                   & " WHERE i.ItemKey = " & lkuItemId.KeyValue _
                                   & " AND i.Status = " & kActive & ")" _
                                   & " AND CompanyID = " & gsQuoted(msCompanyID) _
                                   & " AND Transit = 0 "
    
        End If
     Else
        lkuWhse.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID) & " AND Transit = 0 "
     End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuWhse_LookupClick", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmForm_DMDataDisplayed(oChild As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
If lkuProdEntryNo <> "" Then
    moDmForm.State = kDmStateLocked
    mlWorkOrderKey = glGetValidLong(moDmForm.GetColumnValue("WorkOrderKey"))
    moDMGrid.Where = "CompanyId = " & gsQuoted(msCompanyID) & " And WorkOrderKey = " & glGetValidLong(mlWorkOrderKey)
    GetItem
    GetLaborEntry
    DisableContr
'    bValidEntry (False)
    If (miTrack = 1 Or mbLotControlled = True) Then     ' Or mlTrackMeth = 3 Then
        cmdLotSerialBin.Enabled = True
        lkuAutoDistBin.Enabled = moKitDist.bEnableBinLkuCtrl(lkuWhse.KeyValue, lkuItemId.KeyValue)
'        mbDistInit = False
    Else
        cmdLotSerialBin.Enabled = False
        lkuAutoDistBin.Enabled = False
    End If

    cmdValidProd.Enabled = False
    
End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmForm_DMDataDisplayed", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmForm_DMStateChange(iOldState As Integer, iNewState As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
 tbrMain.SetState (iNewState)
    If iNewState = kDmStateLocked Then
        tbrMain.SecurityLevel = sotaTB_DISPLAYONLY
        tbrMain.ButtonEnabled(kTbProceed) = False
        tbrMain.ButtonEnabled(kTbNextNumber) = False
        tbrMain.ButtonEnabled(kTbPrint) = True
        tbrMain.ButtonEnabled(kTbPreview) = True
        fraReport.Enabled = False
    Else
        tbrMain.SecurityLevel = sotaTB_NORMAL
        tbrMain.ButtonEnabled(kTbNextNumber) = True
        tbrMain.ButtonEnabled(kTbPrint) = False
        tbrMain.ButtonEnabled(kTbPreview) = False
        fraReport.Enabled = True
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmForm_DMStateChange", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub navRoutingId_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Len(Trim(lkuItemId.Text)) > 0 Then
        navRoutingId.RestrictClause = "Active = " & gsQuoted(kRoutActive) _
& " AND CompanyID = " & gsQuoted(msCompanyID) _
& " AND ItemKey = " & glGetValidLong(lkuItemId.KeyValue)
    Else
         navRoutingId.RestrictClause = "Active = " & gsQuoted(kRoutActive) _
& " AND CompanyID = " & gsQuoted(msCompanyID)
    End If
    
    gcLookupClick Me, navRoutingId, txtRoutingId, "RoutingId"
    If navRoutingId.ReturnColumnValues.Count <> 0 Then
        txtRoutingId.Text = navRoutingId.ReturnColumnValues("RoutingId")
        txtVersionId.Text = navRoutingId.ReturnColumnValues("VersionId")
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "navRoutingId_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub numQuantity_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numQuantity, True
    #End If
'+++ End Customizer Code Push +++

    numQuantity.Tag = numQuantity.Value
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "numQuantity_GotFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub numQuantity_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numQuantity, True
    #End If
'+++ End Customizer Code Push +++

    If gdGetValidDbl(numQuantity.Tag) <> gdGetValidDbl(numQuantity.Text) Then
        If numQuantity < 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, "Quantity"
            numQuantity = 0
            gbSetFocus Me, numQuantity
        End If
        
        If miAllowDecQty = 0 Then
            If Int(numQuantity) <> numQuantity Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgInvalid, " Quantity, Item does not allow decimal values"
                gbSetFocus Me, numQuantity
            End If
        End If
                
        tbrMain.ButtonEnabled(kTbProceed) = False
        cmdLotSerialBin.Enabled = False
        lkuAutoDistBin.Enabled = False
        lkuAutoDistBin.SetTextAndKeyValue "", 0
        lkuAutoDistBin.Tag = ""
        lblAutoDistLot.Caption = ""

    End If
    mdQuantity = numQuantity
    numQuantity.Tag = numQuantity.Value
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "numQuantity_LostFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub numScrapPcs_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numScrapPcs, True
    #End If
'+++ End Customizer Code Push +++

    Dim bRetCode             As Boolean
    Dim iNegQty              As Integer
    Dim lRetVal              As Long
    Dim sToolBar             As String
    Dim dScrapPcs            As Double
    Dim iDelAll              As Integer

    iDelAll = kDeleteLineScrap
    
    If numScrapPcs.Tag <> numScrapPcs.Text Then
        mbScrapReason = False
    End If
    
    If mbScrapReason = True Then Exit Sub
    
    dScrapPcs = 0
    
    If tbrMain.SecurityLevel = sotaTB_NORMAL Then
        If numScrapPcs <> 0 Then
            mbScrapReason = False
            dScrapPcs = numScrapPcs
            If numScrapPcs < 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, "Scrap Pcs"
                numScrapPcs = ""
                gbSetFocus Me, numScrapPcs
                Exit Sub
            End If
            If miAllowDecQty = 0 Then
                If Int(numScrapPcs) <> numScrapPcs Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgInvalid, " Scrap Pcs, Item does not allow decimal values"
                    gbSetFocus Me, numScrapPcs
                    Exit Sub
                End If
            End If

            If moScrapDist Is Nothing Then Exit Sub
    
            bRetCode = moScrapDist.bLaborScrap(lkuProdEntryNo.Text, msCompanyID, _
                                                kWOProduction, dScrapPcs, _
                                                mlWorkOrderKey, mlWorkOrderStepKey, _
                                                sToolBar, True, _
                                                miAllowDecQty)
                                               
            If sToolBar = kTbCancelExit Then
                numScrapPcs = 0
                dScrapPcs = 0
            End If
        Else
            DeleteScrap lkuProdEntryNo.Text, iDelAll
        End If
        
        If numScrapPcs <> dScrapPcs Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgMFAllocatedScrap
            gbSetFocus Me, numScrapPcs
            Exit Sub
        Else
            mdScrapPcs = dScrapPcs
'            numScrapPcs.Tag = numScrapPcs.Text
            mbScrapReason = True
        End If

            tbrMain.ButtonEnabled(kTbProceed) = False
            cmdLotSerialBin.Enabled = False
            lkuAutoDistBin.Enabled = False
            
    End If
       
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "numScrapPcs_LostFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Function bValidQuantity() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim lRetVal As Long
    Dim msStepID As String
    Dim lRow As Long
    Dim iCheckAll As Integer
    Dim sItemsWithBadStatus As String
    
    iCheckAll = 1           'Check all steps regardless of Backflush or not
    bValidQuantity = True
    
    If bProgressStep(lRow) Then
        mdTotQuantity = mdQuantity + mdScrapPcs
        With moClass.moAppDB
            .SetInParamLong mlWorkOrderStepKey
            .SetInParamStr msCompanyID
            .SetInParam mdTotQuantity
            .SetInParam mlWorkOrderKey
            .SetInParam iCheckAll
            .SetOutParam lRetVal
            .SetOutParam msStepID
            .SetOutParam sItemsWithBadStatus
            .ExecuteSP "spmfCheckBackFlushQty"
             lRetVal = glGetValidLong(.GetOutParam(6))
             msStepID = gsGetValidStr(.GetOutParam(7))
             sItemsWithBadStatus = gsGetValidStr(.GetOutParam(8))
            .ReleaseParams
        End With
        
        'If any backflushed items have a bad status, let the user know.
        If sItemsWithBadStatus <> "" Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgItemsAreInactiveOrDeleted, sItemsWithBadStatus
            
            'If there are no other issues, then clean up and exit.  If there are other issues, let
            'processsing fall through and let the other code clean up.
            If lRetVal = 0 Then
                numScrapPcs = 0
                numQuantity = 0
                mdScrapPcs = 0
                mdQuantity = 0
                'Set focus to the step ID.  This is done because it is unknown which number control called this function, and
                'to reduce the chances of this function getting called repeatedly from a number control's lost focus event even
                'when there was no quantity change.
                gbSetFocus Me, lkuItemId
                bValidQuantity = False
            End If
        End If
        
        Select Case lRetVal
            Case -1         'Insufficient Qty to backflush
                If miNegQty = 0 Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgMFInsfQtyOnHandForStep, msStepID
                    numScrapPcs = 0
                    numQuantity = 0
                    mdScrapPcs = 0
                    mdQuantity = 0
                    gbSetFocus Me, numQuantity
                    bValidQuantity = False
                End If
                
            Case -2         'Arithmetic Overflow (too much requested for database columns)
                giSotaMsgBox Me, moClass.moSysSession, kmsgMFBFlushOverFlow, msStepID
                numScrapPcs = 0
                numQuantity = 0
                mdScrapPcs = 0
                mdQuantity = 0
                gbSetFocus Me, numQuantity
                bValidQuantity = False
        End Select
        
        If gdGetValidDbl(numScrapPcs.Text) <> gdGetValidDbl(numScrapPcs.Tag) Then
            GetDetlSTD
            If mdScrapPcs <> 0 Then
                gGridUpdateCell grdMain, lRow, kColScrapPcsSTD, Format$(mdDetlScrapPcsSTD + mdScrapPcs)
                gGridUpdateCell grdMain, lRow, kColQTYReqSTD, Format$(mdProgQTYReqSTD)
            Else
                gGridUpdateCell grdMain, lRow, kColScrapPcsSTD, Format$(mdDetlScrapPcsSTD)
                gGridUpdateCell grdMain, lRow, kColQTYReqSTD, Format$(mdProgQTYReqSTD)
            End If
            moDMGrid.SetRowDirty lRow
            moDMGrid.Save
            numScrapPcs.Tag = numScrapPcs.Text
        End If
   
        If miAllowDecQty = 0 Then
            If Int(mdTotQuantity) <> mdTotQuantity Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgInvalid, " Quantity, Item does not allow decimal values"
                numQuantity = 0
                mdQuantity = 0
                gbSetFocus Me, numQuantity
                bValidQuantity = False
            End If
        End If
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bValidQuantity", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Private Sub GetDetlSTD()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
mdDetlScrapPcsSTD = gdGetValidDbl(moClass.moAppDB.Lookup("ScrapPcsSTD", "tmfWorkOrdDetl_HAI WITH (NOLOCK)", "WorkOrderKey = " & glGetValidLong(mlWorkOrderKey) _
                                    & " AND WorkOrderStepKey = " & glGetValidLong(mlWorkOrderStepKey) _
                                    & " And CompanyID = " & gsQuoted(msCompanyID)))
mdProgQTYReqSTD = gdGetValidDbl(moClass.moAppDB.Lookup("QTYReqSTD", "tmfWorkOrdDetl_HAI WITH (NOLOCK)", "WorkOrderKey = " & glGetValidLong(mlWorkOrderKey) _
                                    & " AND WorkOrderStepKey = " & glGetValidLong(mlWorkOrderStepKey) _
                                    & " And CompanyID = " & gsQuoted(msCompanyID)))
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetDetlSTD", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function bValidScrap() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bValidScrap = True
    If numScrapPcs <> mdScrapPcs / 1 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgMFAllocatedScrap
        numScrapPcs_LostFocus
        bValidScrap = False
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bValidScrap", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Public Sub DeleteScrap(sTranNo As String, iDelAll As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sMode As String
    Dim sSQL  As String
   
    
    If (moDmForm.State = kDmStateAdd Or moDmForm.State = kDmStateEdit) And Not moScrapDist Is Nothing Then
        moScrapDist.DeleteScrap sTranNo, msCompanyID, kWOProduction, iDelAll
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DeleteScrap", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_MouseDown", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_MouseMove", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_MouseUp", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_Paint", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "frmmfzdz001"
End Function

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'*******************************************************
' Description:
'    This routine will bind Right-Click context menus to
'    the form and controls.
'*******************************************************

    Set moContextMenu = New clsContextMenu
    With moContextMenu
        Set .Form = frmmfzdz001
        
        .Bind "MFROUTE", txtRoutingId.hwnd, kEntTypeMFRouting
        
        ' Entities will be bound in CMAppendContextMenu
        .Bind "*DYNAMIC", lkuItemId.hwnd
        .Bind "*DYNAMIC", lkuWhse.hwnd
        
        .Init
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function CMAppendContextMenu(Ctl As Control, hmenu As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************************
'      Desc: Called to append menu items to right mouse popup menu.
'            Called because form_load did a .Bind *APPEND.
'     Parms: Ctl:   The control that received the right mouse button down message.
'            hmenu: The handle to the popup menu
'   Returns: True if menu item added; False if menu item not added.
'************************************************************************************
    CMAppendContextMenu = False

    Dim bItemHasValue   As Boolean
    Dim bWhseHasValue   As Boolean
    
    ' Determine if controls have value
    bItemHasValue = (Trim(lkuItemId.Text) <> "")
    bWhseHasValue = (Trim(lkuWhse.Text) <> "")

    If (Ctl Is lkuItemId) Then
        
        ' Point control to different context menu
        If (bItemHasValue) And (bWhseHasValue) Then
            moContextMenu.AppendDrillMenu hmenu, "IMINVEN", _
                                          Ctl, Me, False, kEntTypeIMInventory
        Else
            moContextMenu.AppendDrillMenu hmenu, "IMITEM", _
                                          Ctl, Me, False, kEntTypeIMItem
        End If
        
    End If

    If (Ctl Is lkuWhse) Then
        
        ' Point control to different context menu
        If (bItemHasValue) And (bWhseHasValue) Then
            moContextMenu.AppendDrillMenu hmenu, "IMINVEN", _
                                          Ctl, Me, False, kEntTypeIMInventory
        Else
            moContextMenu.AppendDrillMenu hmenu, "IMWHSE", _
                                          Ctl, Me, False, kEntTypeIMWarehouse
        End If
    End If

    CMAppendContextMenu = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CMAppendContextMenu", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Function ETWhereClause(ByVal Ctl As Object, _
                              ByVal taskID As Long, _
                              ByVal EntityType As Integer, _
                              ByVal naturalKeyCols As String, _
                              ByVal TaskGroup As EntityTask_Groups) As String
'+++ VB/Rig Skip +++
'*******************************************************************************
'   Description: OPTIONAL
'                Entity Task Where Clause. This routine allows the application
'                to specify the where clause to be used against the
'                Host Data View for the specified entity.
'
'                If a where clause is not specified, the where clause will be
'                built using the Natural Key Columns specified for the entity.
'
'                Surrogate Keys as natural keys and other criteria make it
'                impossible without supporting metadata to get these values from
'                the application.
'
'   Parameters:
'                ctl <in> - Control that selected the task
'                taskID <in> - Task ID for the Entity Task
'                entityType <in> - Entity Type bound to the control that selected the task
'                naturalKeyCols <in> - Key columns used to specify where clause (FYI)
'                TaskGroup <in> - Task Group 100-500 (FYI)
'
'   Returns:
'                A specified where clause WITHOUT the 'WHERE' verb. (i.e. "1 = 2")
'*******************************************************************************
On Error Resume Next
    
    Dim bItemHasValue   As Boolean
    Dim bWhseHasValue   As Boolean
    
    ' Determine if controls have value
    bItemHasValue = (Trim(lkuItemId.Text) <> "")
    bWhseHasValue = (Trim(lkuWhse.Text) <> "")
    
    Select Case True
        Case Ctl Is lkuWhse
            
            If (bItemHasValue) And (bWhseHasValue) Then
                ETWhereClause = "WhseKey = " & CStr(glGetValidLong(lkuWhse.KeyValue)) & " AND " & _
                                "ItemKey = " & CStr(glGetValidLong(lkuItemId.KeyValue))
            Else
                ETWhereClause = "WhseKey = " & CStr(glGetValidLong(lkuWhse.KeyValue))
            End If

        Case Ctl Is lkuItemId
            
            If (bItemHasValue) And (bWhseHasValue) Then
                ETWhereClause = "WhseKey = " & CStr(glGetValidLong(lkuWhse.KeyValue)) & " AND " & _
                                "ItemKey = " & CStr(glGetValidLong(lkuItemId.KeyValue))
            Else
                ETWhereClause = "ItemKey = " & CStr(glGetValidLong(lkuItemId.KeyValue))
            End If
        
        Case Ctl Is txtRoutingId
        
            ETWhereClause = "RoutingKey = " & CStr(glGetValidLong(moClass.moAppDB.Lookup("RoutingKey", "tmfRoutHead_HAI WITH (NOLOCK)", "RoutingId = " & gsQuoted(txtRoutingId.Text) _
                            & " AND VersionId = " & gsQuoted(txtVersionId.Text) _
                            & " AND CompanyID = " & gsQuoted(msCompanyID))))
        
        Case Else
            ETWhereClause = ""
    End Select
    
    Err.Clear
    
End Function


Private Sub BindForm()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************
' Description:
'    This routine will bind fields on the form to fields in the database.
'************************************************************************
    '-- create a new data Manager Form class
    Set moDmForm = New clsDmForm
    Set moDMGrid = New clsDmGrid

    With moDmForm
        Set .Form = frmmfzdz001
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB


        .AppName = Me.Caption
        .Table = "tmfWorkOrdHead_HAI"

        Set .ValidationMgr = valMgr

        .OrderBy = "WorkOrderNo"
        .UniqueKey = "WorkOrderNo,CompanyID"
        .Bind Nothing, "CompanyID", SQL_CHAR

        .Bind lkuProdEntryNo, "WorkOrderNo", SQL_CHAR
        .Bind Nothing, "WorkOrderKey", SQL_INTEGER
        .Bind calEntryDate, "EntryDate", SQL_DATE, kDmSetNull
        .BindLookup lkuWhse
        
        
        .Bind Nothing, "Quantity", SQL_DECIMAL
        .Bind Nothing, "Complete", SQL_SMALLINT
        .Bind Nothing, "RoutingKey", SQL_INTEGER
        .Bind Nothing, "UpdateCounter", SQL_INTEGER
        .Bind Nothing, "NotesKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "CustKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "QTYCycle", SQL_DECIMAL
        .Bind Nothing, "ItemClassKey", SQL_INTEGER
        .Bind Nothing, "RequiredDate", SQL_DATE
        .Bind Nothing, "ReleaseDate", SQL_DATE
        .Bind Nothing, "CustPONo", SQL_CHAR
        .Bind Nothing, "MFGCommitDate", SQL_DATE
        .Bind Nothing, "Priority", SQL_SMALLINT
        .Bind Nothing, "PriorityCode", SQL_INTEGER
        .Bind Nothing, "WorkOrderType", SQL_SMALLINT
        
        .LinkSource "tmfRoutHead_HAI", "RoutingKey=<<RoutingKey>>"
        .Link txtRoutingId, "RoutingId", SQL_CHAR
        .Link txtVersionId, "VersionId", SQL_CHAR


        .Init
    End With
    
    With moDMGrid
        Set .Form = frmmfzdz001
        Set .Session = moClass.moSysSession
        Set .Parent = moDmForm
        Set .Database = moClass.moAppDB
            .OrderBy = "WorkOrderKey, StepID"
            .UniqueKey = "WorkOrderKey, StepID"
            .Where = "CompanyID = " & gsQuoted(msCompanyID) & " And WorkOrderKey = " & glGetValidLong(mlWorkOrderKey)
     Set .Grid = grdMain
        .Table = "tmfWorkOrdDetl_HAI"
        
        .BindColumn "WorkOrderKey", kColWorkOrderKey, SQL_INTEGER
        .BindColumn "WorkOrderStepKey", kColWorkOrderStepKey, SQL_INTEGER
        .BindColumn "StepID", kColStepId, SQL_CHAR
        .BindColumn "MatItemKey", kColMatItemKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "OperationKey", kColOperationKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "OperationDesc1", kColOperationDesc1, SQL_CHAR
        .BindColumn "OperationType", kColOperationType, SQL_CHAR
        .BindColumn "WorkCenterKey", kColWorkCenterKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "QTYReqSTD", kColQTYReqSTD, SQL_DECIMAL
        .BindColumn "QTYTD", kColQTYTD, SQL_DECIMAL
        .BindColumn "SetHrsSTD", kColSetHrsSTD, SQL_DECIMAL
        .BindColumn "SetHrsTD", kColSetHrsTD, SQL_DECIMAL
        .BindColumn "SetCostSTD", kColSetCostSTD, SQL_DECIMAL
        .BindColumn "SetCostTD", kColSetCostTD, SQL_DECIMAL
        .BindColumn "RunHrsSTD", kColRunHrsSTD, SQL_DECIMAL
        .BindColumn "RunHrsTD", kColRunHrsTD, SQL_DECIMAL
        .BindColumn "RunCostSTD", kColRunCostSTD, SQL_DECIMAL
        .BindColumn "RunCostTD", kColRunCostTD, SQL_DECIMAL
        .BindColumn "MatCostSTD", kColMatCostSTD, SQL_DECIMAL
        .BindColumn "MatCostTD", kColMatCostTD, SQL_DECIMAL
        .BindColumn "ScrapPcsSTD", kColScrapPcsSTD, SQL_DECIMAL
        .BindColumn "ScrapPcsTD", kColScrapPcsTD, SQL_DECIMAL
        .BindColumn "DownHrsSTD", kColDownHrsSTD, SQL_DECIMAL
        .BindColumn "OperCostToWip", kColOperCostToWip, SQL_CHAR
        .BindColumn "ProgressStep", kColProgressStep, SQL_CHAR
        .BindColumn "BackFlush", kColBackFlush, SQL_CHAR
        .BindColumn "QTYPerDesc", kColQTYPerDesc, SQL_VARCHAR
        .BindColumn "MachineKey", kColMachineKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "WhseKey", kColWhseKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "UpdateCounter", Nothing, SQL_INTEGER
        
        .LinkSource "timItem", "ItemKey=<<MatItemKey>>"
        .Link kColMatItemId, "ItemID"
        
        .LinkSource "timWarehouse", "WhseKey=<<WhseKey>>"
        .Link kColMatWhseId, "WhseID"
        
        .LinkSource "tmfOperation_HAI", "OperationKey=<<OperationKey>>"
        .Link kColOperationId, "OperationID"

        .LinkSource "tmfWorkCenter_HAI ", "WorkCenterKey=<<WorkCenterKey>>"
        .Link kColWorkCenterId, "WorkCenterID"
        
        .LinkSource "tmfMachine_HAI ", "MachineKey=<<MachineKey>>"
        .Link kColMachineId, "MachineID"
        
'********************************************************
'RKL DEJ 1/27/14 (START)
'********************************************************
        .LinkSource "vdvStockStatus", "WhseKey=<<WhseKey>> and ItemKey=<<MatItemKey>>"
        .Link kColUOM, "UnitMeasID"
        .Link kColQtyOnHand, "QtyOnHand"
'********************************************************
'RKL DEJ 1/27/14 (STOP)
'********************************************************
        
    End With
    
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindForm", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub BindGM()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Bind the grid manager
    Set moGM = New clsGridMgr

    With moGM
        Set .Grid = grdMain
            .GridType = kGridDataSheet
        Set .DM = moDMGrid
        Set .Form = frmmfzdz001
        grdMain.ScrollBars = ScrollBarsBoth
        .Init
    End With

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindGM", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If
'********************************************************************
' Description:
'    This routine is the central routine for processing events when a
'    button on the Toolbar is clicked.
'********************************************************************
    Dim iActionCode As Integer
    Dim iConfirmUnload As Integer
    Dim lRet As Long
    Dim sNewKey As String
    Dim vParseRet As Variant
    Dim sSQL As String
    Dim iDelAll As Integer
    Dim iDelDist As Integer
    Dim bUpdateDistHead As Boolean
    
    iDelAll = kDeleteLineScrap
    iDelDist = 1
    bUpdateDistHead = False
    iActionCode = kDmFailure
    
    If (Me.Visible And Me.Enabled) Then
        Me.SetFocus
        DoEvents
    End If
    
    Select Case sKey
        Case kTbProceed
            If Not gbPostDateCheck(Me, moClass, calEntryDate, lblEntryDate, 9002) Then
                calEntryDate.Protected = False
                gbSetFocus Me, calEntryDate
                Exit Sub
            End If
            
            If mbProductionEntered Then
                If mbLotControlled = True Or miTrack = 1 Then
                'Add Lot Control/Serial/Bin Check here
                    If Not bEditDist Then
                        Exit Sub
                    End If
                End If
                
                If mbGridDirty Then
                    If bValidEntry(False) = True Then
                        iActionCode = moDMGrid.Save
                    Else
                        Exit Sub
                    End If
                End If
                
                ' Insert the Work Order Transaction and Parts Produced Records
                ' to temp tables
                bInsertTransaction
                
                UpdWOTranProd
                
                UpdateMFDistribution
                
                tbrMain.Enabled = False
                ProcessLaborTrans
                                                                        
                DeleteKitDist (iDelDist)
                ClearAll
                
                DoEvents
                tbrMain.Enabled = True
                
                iActionCode = moDmForm.Action(kDmCancel)
                
                Screen.MousePointer = vbDefault
                gbSetFocus Me, lkuProdEntryNo
            End If
            
        Case kTbCancel
            If Not bTransExist Then
                If Len(Trim(lkuProdEntryNo)) <> 0 Then
                    If bIsValidProductionWorkOrder(True) Then
                        DeleteKitDist (iDelDist)
                        UpdWOComplete               ' This will remove replenishment demand
                        DeleteWorkOrder
                        DeleteScrap lkuProdEntryNo.Text, iDelAll
                    End If
                    ClearAll
                    iActionCode = moDmForm.Action(kDmCancel)
                End If
                gbSetFocus Me, lkuProdEntryNo
            Else
                If bIsValidProductionWorkOrder(True) Then
                    DeleteKitDist (iDelDist)
                End If
                ClearAll
                iActionCode = moDmForm.Action(kDmCancel)
                tbrMain.ButtonEnabled(kTbProceed) = False
                cmdDetail.Enabled = False
                gbSetFocus Me, lkuProdEntryNo
            End If
            
            
        Case kTbDelete
            iActionCode = moDmForm.Action(kDmDelete)

        Case kTbCopyFrom
            '-- this is a future enhancement to Data Manager
            'iActionCode = moDmForm.CopyFrom

        Case kTbNextNumber
            GetNextNo
            mbProductionEntered = False
            cmdDetail.Enabled = False
            cmdLotSerialBin.Enabled = False
            
       Case kTbPrint, kTbPreview
            If bSetUpReport Then
                lStartReport sKey, Me, False, "", "", ""
            End If
            
        Case kTbHelp
            gDisplayFormLevelHelp Me
        
        Case Else
            tbrMain.GenericHandler sKey, Me, moDmForm, moClass
            
    End Select

    Select Case iActionCode
        Case kDmSuccess
            moClass.lUIActive = kChildObjectInactive
            
        Case kDmFailure, kDmError
            Exit Sub
             
    End Select
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mbCancelShutDown = False
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
    End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'*********************************************************************
' Description:
'    Form_KeyPress traps the keys pressed when the KeyPreview property
'    of the form is set to True.
'*********************************************************************
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
               SendKeys "{TAB}"
               KeyAscii = 0
            End If
        
        Case Else
    
    End Select
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    '-- assign system object properties to module-level variables
    Dim lRetVal As Long             'Captures Long function return value(s).
    Dim iRetVal As Integer          'Captures Integer function return value(s).
    Dim sRptProgramName As String   'Stores base filename for identification purposes.
    Dim sModule As String           'The report's module: AR, AP, SM, etc.
    Dim sDefaultReport As String    'The default .RPT file name
    
    
    mbLoadSuccess = False
    msCompanyID = moClass.moSysSession.CompanyId
    msUserID = moClass.moSysSession.UserId
    mbEnterAsTab = moClass.moSysSession.EnterAsTab
    mlLanguage = moClass.moSysSession.Language
        
    miOldFormHeight = Me.Height
    miMinFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormWidth = Me.Width
    
    SetModOptions                           ' Set up Module Options
    
    '--Set the form caption
    Me.Caption = "Production Entry"
    MapControls
    
    '-- setup and bind right click context menus to controls
    BindContextMenu
    '-- setup navigator restrict clause

    msNavRestrict = "WorkOrderType = " & gsQuoted(kWOProduction) _
                  & " AND CompanyID = " & gsQuoted(msCompanyID)
    
    gbLookupInit navRoutingId, moClass, moClass.moAppDB, "Routing", "CompanyID = " & gsQuoted(msCompanyID)
    SetupLookups
    SetupDropDowns
 
    On Error Resume Next
    
    '--adjust the tool bar
    With tbrMain
        .RemoveButton kTbMemo
        .RemoveButton kTbClose
        .RemoveButton kTbHelp
        .RemoveSeparator kTbCustomizer
        .AddButton kTbNextNumber
        .AddButton kTbCancel
        .AddButton kTbPreview
        .AddButton kTbPrint
        .AddSeparator kTbPreview
        .AddButton kTbHelp
        .AddSeparator kTbHelp
        .LocaleID = mlLanguage
    End With
    
    On Error GoTo VBRigErrorRoutine
    
    '-- bind form fields to database fields
    bSetupGrid
    BindForm
    BindGM
     'Set up Scrap Object
    If moScrapDist Is Nothing Then
         Set moScrapDist = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
kclsLaborScrapEntry, ktskMFLaborScrapEntry, _
kDDRunFlags, kContextAOF)
    
         If Not moScrapDist.InitScrapDist(moClass.moAppDB, moClass.moAppDB) Then
             Set moScrapDist = Nothing
         End If
     End If
            
    SetDecVal                               ' Set Form controls decimal values
    
     'Set up Distribution Object
    miTranTypeNo = 9001
    mbInitKitDist = False
    Set moKitDist = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
kclsIMDistribution, 117833828, _
kAOFRunFlags, kContextAOF)
                         
    'If Dist Task has no permissions, then unload the TranEntry form
    If moKitDist Is Nothing Then
        Debug.Print "Distribution Not Instantiated"
        If Not moClass.moFramework Is Nothing Then
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
        End If
        'Unload Me
        Exit Sub
    Else
        mbInitKitDist = moKitDist.InitDistribution(moClass.moAppDB, moClass.moAppDB, lkuWhse.KeyValue)
        If Not mbInitKitDist Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, "Error Loading Kit Dist Object", "Whse ID LostFocus"
            gClearSotaErr
            Exit Sub
        End If
    
    End If
      
    ClearAll
    'status bar settings
    sbrMain.MessageVisible = False
    Set sbrMain.Framework = oClass.moFramework
    Set moDmForm.SOTAStatusBar = sbrMain

    '-- initialize security level
    miSecurityLevel = giSetAppSecurity(moClass, tbrMain, moDmForm)
    
    '-- initialize browse filter tracking
    miFilter = 0
    
    moDmForm.SetUIStatusNone
    mbGridDirty = False
    mbCheckSecurity = True
    '--check the AllowNegQtyOnHand 1 - allow, 0 - not
    miNegQty = giGetValidInt(moClass.moAppDB.Lookup("AllowNegQtyOnHand", "timOptions WITH (NOLOCK)", "CompanyID = " & gsQuoted(msCompanyID)))
    mbLeaveCell = True
    mbLoadSuccess = True
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
    Dim iConfirmUnload As Integer
   
    '-- reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

    If moClass.mlError = 0 Then

        '-- if the form is dirty, prompt the user to save the record
        moDmForm.SetDirty False
        bConfirmUnload iConfirmUnload, True
        
        Select Case iConfirmUnload
            Case kDmSuccess
                HandleToolbarClick (kTbCancel)
                
            Case kDmFailure
                GoTo CancelShutDown
                
            Case kDmError
                GoTo CancelShutDown
                
            Case Else
                giSotaMsgBox Nothing, moClass.moSysSession, kmsgUnexpectedConfirmUnloadRV, iConfirmUnload
                
        End Select
      
        '-- Check all other forms that may have been loaded from this main form.
        '-- If there are any Visible forms, then this means the form is Active.
        '-- Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then GoTo CancelShutDown
    
        Select Case UnloadMode
            Case vbFormCode
                'Do Nothing
            
            Case Else
                '-- Most likely the user has requested to shut down the form.
                '-- If the context is normal or Drill-Around, have the object unload itself.
                Select Case mlRunMode
                    Case Else
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                        
                End Select
        End Select
    End If
    
    '-- If execution gets to this point, the form and class object of the form
    '-- will be shut down. Perform all operations necessary for a clean shutdown.
    PerformCleanShutDown
        
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
            
        Case Else
            'Do Nothing
            
    End Select

    Exit Sub
    
CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If
    If Not moDmForm Is Nothing Then
        moDmForm.UnloadSelf
        Set moDmForm = Nothing
    End If
    
     '-- Unload all forms loaded from this main form
    gUnloadChildForms Me

    '-- Remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    TerminateControls Me
    
    '-- If this form loads any other modal objects
    Set moSotaObjects = Nothing
    Set moContextMenu = Nothing
    Set moScrapDD = Nothing
    Set moReport = Nothing
    
    'Terminate Scrap Distribution
    If Not moScrapDist Is Nothing Then
        moScrapDist.Terminate
    End If
    Set moScrapDist = Nothing
    
    'Terminate the Distribution
    If Not moKitDist Is Nothing Then
        moKitDist.Terminate
        Set moKitDist = Nothing
    End If
    If Not moDMGrid Is Nothing Then
        moDMGrid.UnloadSelf
        Set moDMGrid = Nothing
    End If

        Set moModOptions = Nothing

    valMgr.Terminate
   
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    '-- If this form loads any other modal objects
'Terminate the Distribution
    If Not moKitDist Is Nothing Then
        moKitDist.Terminate
    End If
    Set moKitDist = Nothing
    
    Set moClass = Nothing
'    Set moContextMenu = Nothing
    Set moMapSrch = Nothing
    Set moDMGrid = Nothing
    Set moGM = Nothing
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmForm_DMBeforeInsert(bValid As Boolean)
'********************************************************************
' Description:
'    This routine will be called by Data Manager just before an
'    Insert. We need to get the next surrogate key for the table.
'********************************************************************
Dim lKey As Long
    On Error GoTo CancelInsert
    bValid = True

    With moClass.moAppDB
        .SetInParam "tmfWorkOrdHead_HAI"
        .SetOutParam mlWorkOrderKey
        .ExecuteSP ("spGetNextSurrogateKey")
        mlWorkOrderKey = .GetOutParam(2)
        .ReleaseParams
    End With
   
    moDmForm.SetColumnValue "WorkOrderKey", mlWorkOrderKey
    moDmForm.SetColumnValue "WorkOrderType", kWOProduction
    
    bValid = True
    Exit Sub
CancelInsert:
    bValid = False
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmForm_DMBeforeInsert", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmForm_DMValidate(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
bValid = False
bValid = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmForm_DMValidate", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub sbrMain_ButtonClick(sButton As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lRet            As Long
Dim sNewKey         As String
Dim vParseRet       As Variant
Dim iConfirmUnload  As Integer
    If sButton = kTbFilter Then Exit Sub
    bConfirmUnload iConfirmUnload, True
    If Not iConfirmUnload = kDmSuccess Then Exit Sub

    If sbrMain.Filtered Then
        miFilter = RSID_FILTERED
    Else
        miFilter = RSID_UNFILTERED
    End If
    
        lRet = glLookupBrowse(lkuProdEntryNo, sButton, miFilter, sNewKey)
           
   Select Case lRet
        Case MS_SUCCESS
            vParseRet = gvParseLookupReturn(sNewKey)
            If IsNull(vParseRet) Then Exit Sub
                lkuProdEntryNo.Text = vParseRet(1)
                valMgr_KeyChange
        Case Else
            gLookupBrowseError lRet, Me, moClass
    End Select
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "sbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub txtComment_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtComment, True
    #End If
'+++ End Customizer Code Push +++
    If txtComment.Tag <> txtComment.Text Then
        tbrMain.ButtonEnabled(kTbProceed) = False
        cmdLotSerialBin.Enabled = False
        lkuAutoDistBin.Enabled = False
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtComment_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub txtRoutingId_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtRoutingId, True
    #End If
'+++ End Customizer Code Push +++
Dim sSQL As String
Dim rs As Object

    If Len(Trim(txtRoutingId)) <> 0 And Len(Trim(txtVersionId)) <> 0 Then
        mlRoutingKey = glGetValidLong(moClass.moAppDB.Lookup("RoutingKey", "tmfRoutHead_HAI WITH (NOLOCK)", "RoutingId = " & gsQuoted(txtRoutingId) _
                            & " And VersionId = " & gsQuoted(txtVersionId) _
                            & " AND CompanyID = " & gsQuoted(msCompanyID)))
        If mlRoutingKey <> 0 Then
            If Len(Trim(lkuItemId)) = 0 Or Len(Trim(lkuWhse)) = 0 Then
                sSQL = "SELECT *  FROM tmfRoutHead_HAI WITH (NOLOCK) WHERE tmfRoutHead_HAI.RoutingKey = " & glGetValidLong(mlRoutingKey) _
                        & " AND tmfRoutHead_HAI.RollUpFlag = 1 AND tmfRoutHead_HAI.Active = " & (kRoutActive) _
                        & " AND CompanyID = " & gsQuoted(msCompanyID)

                Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

                If Not rs.IsEmpty Then
                    lkuItemId.KeyValue = glGetValidLong(rs.Field("ItemKey"))
                    txtItemDesc = gsGetValidStr(moClass.moAppDB.Lookup("ShortDesc", "timItemDescription WITH (NOLOCK)", _
                                "ItemKey = " & glGetValidLong(lkuItemId.KeyValue)))
                    lkuWhse.KeyValue = glGetValidLong(rs.Field("WhseKey"))
                    txtWhseDesc = gsGetValidStr(moClass.moAppDB.Lookup("Description", "timWarehouse WITH (NOLOCK)", _
                                "WhseKey = " & glGetValidLong(lkuWhse.KeyValue) _
                                & " AND CompanyID = " & gsQuoted(msCompanyID)))
                    moDmForm.SetColumnValue "RoutingKey", mlRoutingKey
                    If glGetValidLong(rs.Field("NotesKey")) <> 0 Then
                        moDmForm.SetColumnValue "NotesKey", glGetValidLong(rs.Field("NotesKey"))
                    End If
                 
                    moDmForm.SetColumnValue "ItemClassKey", glGetValidLong(rs.Field("ItemClassKey"))
                    
                    If glGetValidLong(rs.Field("CustKey")) <> 0 Then
                        moDmForm.SetColumnValue "CustKey", glGetValidLong(rs.Field("CustKey"))
                    End If
                    
                    moDmForm.SetColumnValue "QTYCycle", 1
                    moDmForm.SetColumnValue "RequiredDate", Format(moClass.moSysSession.BusinessDate, "SHORT DATE")
                    moDmForm.SetColumnValue "ReleaseDate", Format(moClass.moSysSession.BusinessDate, "SHORT DATE")
                    moDmForm.SetColumnValue "MFGCommitDate", Format(moClass.moSysSession.BusinessDate, "SHORT DATE")
                    
                    miAllowDecQty = giGetValidInt(moClass.moAppDB.Lookup("AllowDecimalQty", "timItem WITH (NOLOCK)", _
                            "ItemKey = " & Str(lkuItemId.KeyValue) _
                          & " AND CompanyID = " & gsQuoted(msCompanyID)))
                    
                   
                Else
                    sSQL = "SELECT *  FROM tmfRoutHead_HAI WITH (NOLOCK) WHERE tmfRoutHead_HAI.RoutingKey = " & glGetValidLong(mlRoutingKey) _
                                    & " AND CompanyID = " & gsQuoted(msCompanyID)

                    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
                    
                   
                    miActive = gdGetValidDbl(rs.Field("Active"))
                    
                    If miActive = kRoutInActive Then
                        giSotaMsgBox Me, moClass.moSysSession, kmsgMFNotActiveRouting
                        txtRoutingId = ""
                        txtVersionId = ""
                        gbSetFocus Me, txtRoutingId
                        Exit Sub
                    End If
                    moDmForm.SetColumnValue "RoutingKey", mlRoutingKey
                End If
                    
            Else
                If Not bGetRouting(mlRoutingKey) Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, gsStripChar(lblRouting, kAmpersand) & "/" & gsStripChar(lblVersion, kAmpersand)
                    txtRoutingId = ""
                    txtVersionId = ""
                    gbSetFocus Me, txtRoutingId
                    Exit Sub
                End If
                moDmForm.SetColumnValue "RoutingKey", mlRoutingKey
            End If
        Else
            Beep
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, gsStripChar(lblRouting, kAmpersand) & "/" & gsStripChar(lblVersion, kAmpersand)
            txtVersionId = ""
            gbSetFocus Me, txtVersionId
            Exit Sub
        End If
    Else
        mlRoutingKey = 0
        txtVersionId = ""
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtRoutingId_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Function bGetRouting(mlRoutingKey As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim rs As Object
    Dim sSQL As String
        bGetRouting = False
        If Len(Trim(lkuItemId)) <> 0 Then
            
            sSQL = "SELECT *  FROM tmfRoutHead_HAI rh WITH (NOLOCK) ,tmfRoutProd_HAI rp WITH (NOLOCK) WHERE rp.ItemKey = " & glGetValidLong(lkuItemId.KeyValue) _
                        & " AND rh.RoutingKey = " & glGetValidLong(mlRoutingKey) _
                        & " AND rp.RoutingKey = rh.RoutingKey" _
                        & " AND rh.Active = " & (kRoutActive) _
                        & " AND rh.CompanyID = " & gsQuoted(msCompanyID)


    
            Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
            If Not rs.IsEmpty Then
                
                If glGetValidLong(rs.Field("NotesKey")) <> 0 Then
                    moDmForm.SetColumnValue "NotesKey", glGetValidLong(rs.Field("NotesKey"))
                End If
                
                moDmForm.SetColumnValue "ItemClassKey", glGetValidLong(rs.Field("ItemClassKey"))
                
                If glGetValidLong(rs.Field("CustKey")) <> 0 Then
                    moDmForm.SetColumnValue "CustKey", glGetValidLong(rs.Field("CustKey"))
                End If
                
                moDmForm.SetColumnValue "QTYCycle", 1
                moDmForm.SetColumnValue "RequiredDate", Format(moClass.moSysSession.BusinessDate, "SHORT DATE")
                moDmForm.SetColumnValue "ReleaseDate", Format(moClass.moSysSession.BusinessDate, "SHORT DATE")
                moDmForm.SetColumnValue "MFGCommitDate", Format(moClass.moSysSession.BusinessDate, "SHORT DATE")
                
                miActive = giGetValidInt(rs.Field("Active"))
                
            Else
                txtRoutingId.Text = ""
                txtVersionId.Text = ""
                mlRoutingKey = 0
                Exit Function
            End If
        Else
            sSQL = "SELECT * FROM tmfRoutHead_HAI WITH (NOLOCK) WHERE RoutingKey = " _
                            & glGetValidLong(mlRoutingKey) _
                            & " AND VersionId = " & gsQuoted(txtVersionId) _
                            & " AND CompanyID = " _
                            & gsQuoted(msCompanyID)
                Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
                If Not rs.IsEmpty Then
                    If glGetValidLong(rs.Field("NotesKey")) <> 0 Then
                        moDmForm.SetColumnValue "NotesKey", glGetValidLong(rs.Field("NotesKey"))
                    End If
                   
                    moDmForm.SetColumnValue "ItemClassKey", glGetValidLong(rs.Field("ItemClassKey"))
                    
                    If glGetValidLong(rs.Field("CustKey")) <> 0 Then
                        moDmForm.SetColumnValue "CustKey", glGetValidLong(rs.Field("CustKey"))
                    
                    End If
                    
                    moDmForm.SetColumnValue "QTYCycle", 1
                    moDmForm.SetColumnValue "RequiredDate", Format(moClass.moSysSession.BusinessDate, "SHORT DATE")
                    moDmForm.SetColumnValue "ReleaseDate", Format(moClass.moSysSession.BusinessDate, "SHORT DATE")
                    moDmForm.SetColumnValue "MFGCommitDate", Format(moClass.moSysSession.BusinessDate, "SHORT DATE")
                    
                    miActive = giGetValidInt(rs.Field("Active"))
                    
                Else
                    Exit Function
                End If
        End If
        rs.Close
        bGetRouting = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetRouting", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function



Private Sub txtVersionId_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtVersionId, True
    #End If
'+++ End Customizer Code Push +++
    Dim sSQL As String
    Dim rs As Object
    If Len(Trim(txtRoutingId)) <> 0 And Len(Trim(txtVersionId)) <> 0 Then
        mlRoutingKey = glGetValidLong(moClass.moAppDB.Lookup("RoutingKey", "tmfRoutHead_HAI WITH (NOLOCK)", "RoutingId = " & gsQuoted(txtRoutingId) _
                            & " And VersionId = " & gsQuoted(txtVersionId) _
                            & " AND CompanyID = " & gsQuoted(msCompanyID)))
        If mlRoutingKey <> 0 Then
            If Len(Trim(lkuItemId)) = 0 Or Len(Trim(lkuWhse)) = 0 Then
                sSQL = "SELECT *  FROM tmfRoutHead_HAI WITH (NOLOCK) WHERE tmfRoutHead_HAI.RoutingKey = " & glGetValidLong(mlRoutingKey) _
                                & " AND tmfRoutHead_HAI.RollUpFlag = 1 AND tmfRoutHead_HAI.Active = " & gsQuoted(kRoutActive) _
                                & " AND CompanyID = " & gsQuoted(msCompanyID)

                Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

                If Not rs.IsEmpty Then
                    lkuItemId.KeyValue = glGetValidLong(rs.Field("ItemKey"))
                    txtItemDesc = gsGetValidStr(moClass.moAppDB.Lookup("ShortDesc", "timItemDescription WITH (NOLOCK)", _
                                    "ItemKey = " & glGetValidLong(lkuItemId.KeyValue)))
                    lkuWhse.KeyValue = glGetValidLong(rs.Field("WhseKey"))
                    txtWhseDesc = gsGetValidStr(moClass.moAppDB.Lookup("Description", "timWarehouse WITH (NOLOCK)", _
                                        "WhseKey = " & glGetValidLong(lkuWhse.KeyValue) _
                                        & " AND CompanyID = " & gsQuoted(msCompanyID)))
                    moDmForm.SetColumnValue "RoutingKey", mlRoutingKey
                    If glGetValidLong(rs.Field("NotesKey")) <> 0 Then
                        moDmForm.SetColumnValue "NotesKey", glGetValidLong(rs.Field("NotesKey"))
                    End If
                   
                    moDmForm.SetColumnValue "ItemClassKey", glGetValidLong(rs.Field("ItemClassKey"))
                    
                    If glGetValidLong(rs.Field("CustKey")) <> 0 Then
                        moDmForm.SetColumnValue "CustKey", glGetValidLong(rs.Field("CustKey"))
                    End If
                    
                    moDmForm.SetColumnValue "QTYCycle", 1
                    moDmForm.SetColumnValue "RequiredDate", Format(moClass.moSysSession.BusinessDate, "SHORT DATE")
                    moDmForm.SetColumnValue "ReleaseDate", Format(moClass.moSysSession.BusinessDate, "SHORT DATE")
                    moDmForm.SetColumnValue "MFGCommitDate", Format(moClass.moSysSession.BusinessDate, "SHORT DATE")
                    
                    miAllowDecQty = giGetValidInt(moClass.moAppDB.Lookup("AllowDecimalQty", "timItem WITH (NOLOCK)", _
                            "ItemKey = " & Str(lkuItemId.KeyValue) _
                          & " AND CompanyID = " & gsQuoted(msCompanyID)))
                    
                   
                Else
                    sSQL = "SELECT *  FROM tmfRoutHead_HAI WITH (NOLOCK) WHERE tmfRoutHead_HAI.RoutingKey = " & glGetValidLong(mlRoutingKey) _
                                & " AND CompanyID = " & gsQuoted(msCompanyID)

                    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
                    
                    miActive = gdGetValidDbl(rs.Field("Active"))
                    
                    If miActive = kRoutInActive Then
                        giSotaMsgBox Me, moClass.moSysSession, kmsgMFNotActiveRouting
                        txtRoutingId = ""
                        txtVersionId = ""
                        gbSetFocus Me, txtRoutingId
                        Exit Sub
                    End If
                    moDmForm.SetColumnValue "RoutingKey", mlRoutingKey
                End If
            Else
                If Not bGetRouting(mlRoutingKey) Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, gsStripChar(lblRouting, kAmpersand) & "/" & gsStripChar(lblVersion, kAmpersand)
                    txtRoutingId = ""
                    txtVersionId = ""
                    gbSetFocus Me, txtRoutingId
                    Exit Sub
                End If
                moDmForm.SetColumnValue "RoutingKey", mlRoutingKey
            End If
        Else
            Beep
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, gsStripChar(lblRouting, kAmpersand) & "/" & gsStripChar(lblVersion, kAmpersand)
            txtVersionId = ""
            gbSetFocus Me, txtVersionId
        End If
    Else
        If Len(Trim(txtRoutingId)) = 0 And Len(Trim(txtVersionId)) <> 0 Then
            Beep
            giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeBlank, "Routing ID"
            txtRoutingId = ""
            gbSetFocus Me, txtRoutingId
        End If
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtVersionId_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub valMgr_KeyChange()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lWorkOrderKey As Long
    
    If Len(Trim(lkuProdEntryNo)) <> 0 Then
        moDmForm.SetColumnValue "CompanyID", msCompanyID
        moDmForm.SetColumnValue "WorkOrderType", kWOProduction
        If lkuProdEntryNo.Protected = False Then
            If IsNumeric(lkuProdEntryNo) Then
                lkuProdEntryNo = gsMFZeroFill(lkuProdEntryNo, 8)
            End If
        End If
        lWorkOrderKey = glGetValidLong(moClass.moAppDB.Lookup("WorkOrderKey", "tmfWorkOrdHead_HAI WITH (NOLOCK)", "WorkOrderNo = " & gsQuoted(lkuProdEntryNo.Text) _
& " And CompanyID = " & gsQuoted(msCompanyID)))
        If lWorkOrderKey <> 0 Then
            tbrMain.SecurityLevel = sotaTB_DISPLAYONLY
            moDmForm.State = kDmStateLocked
        Else
            tbrMain.SecurityLevel = sotaTB_NORMAL
            moDmForm.State = kDmStateAdd
            gbSetFocus Me, calEntryDate
        End If
        moDmForm.KeyChange
    End If
    
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "valMgr_KeyChange", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
 msLastButton = Button
 HandleToolbarClick Button
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}


    With valMgr
        Set .Framework = moClass.moFramework
    .Keys.Add lkuProdEntryNo
        .Init
    End With

    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyDataBindings moDmForm
                moFormCust.ApplyFormCust
        End If
    End If

    Exit Sub


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
    '-- the four-letter abbreviation used below is built as follows:
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> +
    '-- "Z" + <one-letter mnemonic of program type, e.g., "M"=Maintenance>
    FormHelpPrefix = "MFZ"         '       Place your prefix here
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
    '-- the three-letter abbreviation used below is built as follows:
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> + "Z"
    WhatHelpPrefix = "MFZ"         '       Place your prefix here
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'***********************************************************************
' Description:
'    oClass contains the reference to the parent class object. The form
'    needs this reference to use the public variables created within the
'    class object.
'***********************************************************************
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'********************************************************************
' Description:
'    lRunMode contains the user-defined run mode context in which the
'    class object was Initialzed. The run mode is extracted from the
'    context passed in through the InitializeObject.
'********************************************************************
    lRunMode = mlRunMode
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lRunMode", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlRunMode = lNewRunMode
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lRunMode", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'**********************************************************************
' Description:
'    bCancelShutDown tells the framework whether the form has requested
'    the shutdown process to be cancelled.
'**********************************************************************
    bCancelShutDown = mbCancelShutDown
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bCancelShutDown", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Private Sub SetupLookups()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sRestrictClause As String

    With lkuProdEntryNo
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB
        
        .RestrictClause = msNavRestrict
    End With
    
    With lkuEmployee
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB
        
        .RestrictClause = "tmfEmployee_HAI.CompanyID = " & gsQuoted(msCompanyID)
    End With
    
    With lkuWhse
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB
        
        .RestrictClause = "CompanyID = " & gsQuoted(msCompanyID) & " AND Transit = 0 "
    End With
    
    With lkuItemId
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB
        
        sRestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
        sRestrictClause = sRestrictClause & " AND ItemKey IN (SELECT a.ItemKey FROM timInventory a WITH (NOLOCK)"
        sRestrictClause = sRestrictClause & " INNER JOIN timItem b WITH (NOLOCK) ON a.ItemKey = b.ItemKey"
        sRestrictClause = sRestrictClause & " WHERE b.CompanyID = " & gsQuoted(msCompanyID) & ")"

        .RestrictClause = sRestrictClause
    End With
   
    With lkuAutoDistBin
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB
    End With
   
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupLookups", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bConfirmUnload(iConfirmUnload As Integer, Optional ByVal bNoClear As Boolean = False) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim bValid As Boolean
    
    bConfirmUnload = False
    
    If Not valMgr.ValidateForm Then Exit Function
    
    iConfirmUnload = moDmForm.ConfirmUnload(bNoClear)

    If (iConfirmUnload = kDmSuccess) Then
        bConfirmUnload = True
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bConfirmUnload", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Skip +++
    bLoadSuccess = mbLoadSuccess
End Property

Public Property Let bLoadSuccess(bNewLoadSuccess As Boolean)
'+++ VB/Rig Skip +++
    mbLoadSuccess = bNewLoadSuccess
End Property

Private Sub SetupDropDowns()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

      
    ' setup dropdown combo
    ddnOutput.AddItem gsBuildString(kIMPrinter, moClass.moAppDB, moClass.moSysSession), 0
    ddnOutput.ItemData(0) = kIMPrinter
    ddnOutput.AddItem gsBuildString(kImScreen, moClass.moAppDB, moClass.moSysSession), 1
    ddnOutput.ItemData(1) = kImScreen
    ddnOutput.ListIndex = 0


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupDropDowns", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ProcessCancel()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moDmForm.Action kDmCancel
    '-- Set the detail controls as valid so the old values are correct
    valMgr.Reset
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ProcessCancel", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Sub GetNextNo()
'+++ VB/Rig Begin Push +++                                                                'MAS 500 Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'MAS 500 Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
    Dim tmpStatus As Integer
    Dim stmpPENo As String
    
    With moClass.moAppDB
        .SetInParam msCompanyID
        .SetOutParam stmpPENo
        .SetOutParam tmpStatus
        .ExecuteSP "spmfGetNextWO"
        stmpPENo = .GetOutParam(2)
        tmpStatus = .GetOutParam(3)
        .ReleaseParams
    End With
    
    If tmpStatus = 0 Then
        stmpPENo = gsMFZeroFill(stmpPENo, 8)
        lkuProdEntryNo = stmpPENo
              
        valMgr_KeyChange
        
    Else
        giSotaMsgBox Me, moClass.moSysSession, kmsgMFOptionNotFound
    End If
     
'+++ VB/Rig Begin Pop +++                                                                 'MAS 500 Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'MAS 500 Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmmfzdz001", "GetNextNo", VBRIG_IS_FORM                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
End Sub
Public Sub RestoreNextNo()
'+++ VB/Rig Begin Push +++                                                                'MAS 500 Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'MAS 500 Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
    Dim sProdEntryNo As String
    sProdEntryNo = lkuProdEntryNo.Text
'    If mlWorkOrderKey <> 0 Then
        With moClass.moAppDB
            .SetInParam msCompanyID
            .SetInParam sProdEntryNo
            .ExecuteSP "spmfRestoreNextWO"
            .ReleaseParams
        End With
'    End If
'+++ VB/Rig Begin Pop +++                                                                 'MAS 500 Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'MAS 500 Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmmfzdz001", "RestoreNextNo", VBRIG_IS_FORM                    'MAS 500 Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub CopyMMedia()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String
Dim sRoutTable As String
Dim sWOTable As String
sRoutTable = "tmfRoutHead_HAI"
sWOTable = "tmfWorkOrdHead_HAI"

 sSQL = "INSERT INTO tmfFileName_HAI(TableName,TableKey,ApplicationType,FileName,CompanyId)" _
            & " SELECT " & gsQuoted(sWOTable) & "," & Str(mlWorkOrderKey) & "," _
            & " a.ApplicationType,a.FileName, a.CompanyId" _
            & " FROM tmfFileName_HAI a WITH (NOLOCK) " _
            & " WHERE TableName  = " & gsQuoted(sRoutTable) _
            & " AND   TableKey  =  " & gsGetValidStr(mlRoutingKey)
            
 moClass.moAppDB.ExecuteSQL (sSQL)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CopyMMedia", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Sub DeleteWorkOrder()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sSQL As String
    If mlWorkOrderKey <> 0 Then
        sSQL = "DELETE tmfWorkOrdHead_HAI WHERE WorkOrderKey = " & mlWorkOrderKey _
                        & " AND CompanyID = " & gsQuoted(msCompanyID)
        moClass.moAppDB.ExecuteSQL (sSQL)
        sSQL = "DELETE tmfWorkOrdDetl_HAI WHERE WorkOrderKey = " & mlWorkOrderKey _
                        & " AND CompanyID = " & gsQuoted(msCompanyID)
        moClass.moAppDB.ExecuteSQL (sSQL)
        sSQL = "DELETE tmfWorkOrdProd_HAI WHERE WorkOrderKey = " & mlWorkOrderKey _
                        & " AND CompanyID = " & gsQuoted(msCompanyID)
        moClass.moAppDB.ExecuteSQL (sSQL)
        sSQL = "DELETE tmfFileName_HAI WHERE TableName  = " & gsQuoted("tmfWorkOrdHead_HAI") _
                 & " AND   TableKey  =  " & gsGetValidStr(mlWorkOrderKey)
        moClass.moAppDB.ExecuteSQL (sSQL)
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DeleteWorkOrder", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function bValidEntry(bButtonClick As Boolean) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim lInvItemKey As Long
    Dim sSQL As String
    Dim rs As Object
    Dim lRow As Long
    Dim sOperationType As String
    Dim dQTYReqSTD As Double
    Dim dScrapPcsSTD As Double
    Dim lWorkOrderStepKey As Long
    Dim bInitKitDist As Boolean

    bValidEntry = False
            
    If Len(Trim(lkuItemId)) <> 0 And lkuItemId.Text <> lkuItemId.Tag Then
        If lkuItemId.KeyValue <> 0 Then
            sSQL = "SELECT *  FROM tmfRoutHead_HAI rh WITH (NOLOCK) ,tmfRoutProd_HAI rp WITH (NOLOCK) WHERE rp.ItemKey = " & glGetValidLong(lkuItemId.KeyValue) _
                                & " AND rp.RoutingKey = rh.RoutingKey" _
                                & " AND rh.Active = " & kRoutActive _
                                & " AND rh.CompanyID = " & gsQuoted(msCompanyID)

            Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

            If Not rs.IsEmpty Then
                txtItemDesc = gsGetValidStr(moClass.moAppDB.Lookup("ShortDesc", "timItemDescription WITH (NOLOCK)", _
                                "ItemKey = " & glGetValidLong(lkuItemId.KeyValue)))
                lkuItemId.Tag = lkuItemId.Text
                                     
            Else
               giSotaMsgBox Me, moClass.moSysSession, kmsgMFActiveRouting, lkuItemId.Text
               lkuItemId.Text = ""
               gbSetFocus Me, lkuItemId
            End If
        End If
    End If

    If Len(Trim(lkuWhse)) <> 0 Then
        If lkuItemId.KeyValue <> 0 Then
            If lkuWhse.KeyValue <> 0 Then
                lInvItemKey = glGetValidLong(moClass.moAppDB.Lookup("ItemKey", "timInventory WITH (NOLOCK)", _
                              "ItemKey = " & glGetValidLong(lkuItemId.KeyValue) _
                            & " AND WhseKey = " & glGetValidLong(lkuWhse.KeyValue)))
                If lInvItemKey = 0 Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgIMSNoInvtRec, lkuItemId, lkuWhse
                    lkuWhse.Text = ""
                    txtWhseDesc = ""
                    Exit Function
                Else

                    txtWhseDesc = gsGetValidStr(moClass.moAppDB.Lookup("Description", "timWarehouse WITH (NOLOCK)", _
                                        "WhseKey = " & glGetValidLong(lkuWhse.KeyValue) _
                                        & " AND CompanyID = " & gsQuoted(msCompanyID)))
                    mlTrackMeth = 0
                    miTrack = 0
                    mlTrackMeth = glGetValidLong(moClass.moAppDB.Lookup("TrackMeth", "timItem WITH (NOLOCK)", _
                                        "ItemKey = " & glGetValidLong(lInvItemKey)))
                    mlStockUOMKey = glGetValidLong(moClass.moAppDB.Lookup("StockUnitMeasKey", "timItem WITH (NOLOCK)", _
                                        "ItemKey = " & glGetValidLong(lInvItemKey)))
                    miTrack = giGetValidInt(moClass.moAppDB.Lookup("TrackQtyAtBin", "timWarehouse WITH (NOLOCK)", _
                                        "WhseKey = " & glGetValidLong(lkuWhse.KeyValue) & " AND CompanyID = " & gsQuoted(msCompanyID)))
                    If mlTrackMeth = 0 Then
                        mbLotControlled = False
                    Else
                        mbLotControlled = True
                    End If
                    
               End If
              
               lkuWhse.Tag = lkuWhse.Text

            End If
        End If
    End If
    
    If (txtRoutingId <> "") And (Not bGetRouting(mlRoutingKey)) Then Exit Function
       
    If bButtonClick Then
        If Len(Trim(lkuEmployee)) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeBlank, "Employee "
            gbSetFocus Me, lkuEmployee
            Exit Function
        End If
        
        If Len(Trim(lkuItemId)) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeBlank, "Item "
            gbSetFocus Me, lkuItemId
            Exit Function
        End If

        If Len(Trim(lkuWhse)) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeBlank, "Warehouse "
            gbSetFocus Me, lkuWhse
            Exit Function
        End If

        If numQuantity = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalid, "Quantity, must be > 0"
            gbSetFocus Me, numQuantity
            Exit Function
        Else
            CalcQuantity
        End If
        
        If Len(Trim(lkuItemId)) <> 0 And Len(Trim(lkuWhse)) <> 0 Then
            If Not (bValidInvStatus(lkuItemId.KeyValue, lkuWhse.KeyValue)) Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgIMInvtStatusNotActive, lkuItemId.Text
                lkuItemId.SetFocus
                Exit Function
            End If
'            If Not bStItemCost Then
'                giSotaMsgBox Me, moClass.moSysSession, kmsgMFStItemCost, lkuItemId.Text
'                Exit Function
'            End If
        End If
    End If

    ' Validate Entry for grid
    If tbrMain.SecurityLevel <> sotaTB_DISPLAYONLY Then
        If Not bButtonClick Then
            mbLeaveCell = True
            If grdMain.DataRowCnt <> 0 Then
                ' Validation for AllowDecimals and NegQty
                For lRow = 1 To grdMain.DataRowCnt
                    dQTYReqSTD = gdGetValidDbl(gsGridReadCell(grdMain, lRow, kColQTYReqSTD))
                    If grdMain.Lock = False Then
                        moDMGrid.Save
                        lWorkOrderStepKey = glGetValidLong(gsGridReadCell(grdMain, lRow, kColWorkOrderStepKey))
                        If Not bValidProductionGridEntry(lWorkOrderStepKey) Then
                            bValidEntry = False
                            mbLeaveCell = False
                            Exit Function
                        End If
                    End If
                Next lRow
            End If
            'Validate ScrapAlloc
            If grdMain.DataRowCnt <> 0 Then
                For lRow = 1 To grdMain.DataRowCnt
                   dScrapPcsSTD = gdGetValidDbl(gsGridReadCell(grdMain, lRow, kColScrapPcsSTD))
                    If grdMain.Lock = False Then
                        If dScrapPcsSTD > 0 Then
                            If Not bScrapAllocated(lRow) Then
                                bValidEntry = False
                                gGridSetActiveCell grdMain, lRow, kColScrapPcsSTD
                                gGridSetSelectRow grdMain, lRow
                                cmdDetail.SetFocus
                                mbLeaveCell = False
                                Exit Function
                            End If
                        End If
                    End If
                Next lRow
            End If
               ' Validate Lot/Serial/Bin Items
            If grdMain.DataRowCnt <> 0 Then
                For lRow = 1 To grdMain.DataRowCnt
                    sOperationType = gsGetValidStr(gsGridReadCell(grdMain, lRow, kColOperationType))
                    If sOperationType <> "L" Then
                        If bKitDistGridInfo(lRow) Then
                            If Not bEditDetlDist Then
                                bValidEntry = False
                                gGridSetSelectRow grdMain, lRow
                                cmdDetail.SetFocus
                                Exit Function
                            End If
                        End If
                    End If
                Next lRow
            End If
        End If
    End If
     
    bValidEntry = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bValidEntry", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Private Function bScrapAllocated(Row As Long) As Boolean
Dim sSQL As String
Dim rs As Object
bScrapAllocated = False
msStepID = gsGetValidStr(gsGridReadCell(grdMain, Row, kColStepId))
msTranNo = LTrim(RTrim(lkuProdEntryNo.Text)) + "-" + LTrim(RTrim(msStepID))
    'If there are no scrap for this  line, there will be no records in #tmfScrpRsnDistWrk  for the line.
    sSQL = "SELECT * FROM #tmfScrpRsnDistWrk WITH (NOLOCK) WHERE TransactionNo = " & gsQuoted(msTranNo) & " AND CompanyID = " & gsQuoted(msCompanyID) & " AND TranDistType = " & kWOProduction
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If Not rs.IsEmpty Then
        bScrapAllocated = True
    Else
        If mbLeaveCell Then
            mbLeaveCell = False
            giSotaMsgBox Me, moClass.moSysSession, kmsgMFAllocatedScrap, msStepID
        End If
    End If
End Function
Private Function bValidProductionGridEntry(lWorkOrderStepKey As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'MAS 500 Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'MAS 500 Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
Dim msStepID As String
Dim lRetVal As Long

bValidProductionGridEntry = False
    With moClass.moAppDB
        .SetInParamLong lWorkOrderStepKey
        .SetInParamInt miNegQty
        .SetInParamLong lkuItemId.KeyValue
        .SetInParamStr msCompanyID
        .SetOutParam lRetVal
        .SetOutParam msStepID
        .ExecuteSP "spmfValidateProductionDetail"
         lRetVal = .GetOutParam(5)
         msStepID = .GetOutParam(6)
        .ReleaseParams
    End With
    Select Case lRetVal
    Case 0
        bValidProductionGridEntry = True
        mbLeaveCell = True
    Case -1
        If mbLeaveCell Then
            mbLeaveCell = False
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidIncompatibleFields, " Quantity, Item does not allow decimal values for Step", msStepID
            gGridSetActiveCell grdMain, grdMain.ActiveRow, kColQTYReqSTD
            bValidProductionGridEntry = False
        End If
    Case -2
        If mbLeaveCell Then
            mbLeaveCell = False
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidIncompatibleFields, "  Scrap Pcs, Item does not allow decimal values", msStepID
            gGridSetActiveCell grdMain, grdMain.ActiveRow, kColScrapPcsSTD
            bValidProductionGridEntry = False
        End If
    Case -3
        If mbLeaveCell Then
            mbLeaveCell = False
            giSotaMsgBox Me, moClass.moSysSession, kmsgMFInsfQtyOnHandForStep, msStepID
            gGridSetActiveCell grdMain, grdMain.ActiveRow, kColQTYReqSTD
            bValidProductionGridEntry = False
        End If
        
    End Select
'+++ VB/Rig Begin Pop +++                                                                 'MAS 500 Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'MAS 500 Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmmfzdz001", "bValidProductionGridEntry", VBRIG_IS_FORM                      'MAS 500 Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub ClearAll()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    lkuEmployee = ""
    txtEmployeeName = ""
    lkuItemId = ""
    lkuItemId.Tag = ""
    txtItemDesc = ""
    lkuWhse = ""
    lkuWhse.Tag = ""
    txtWhseDesc = ""
    txtRoutingId = ""
    txtVersionId = ""
    numQuantity = 0
    numQuantity.Tag = "0.00"
    numScrapPcs = 0
    numScrapPcs.Tag = "0.00"
    txtComment = ""
    txtComment.Tag = ""
    calEntryDate = ""
    lkuProdEntryNo.Protected = False
    calEntryDate.Protected = False
    lkuEmployee.Protected = False
    lkuEmployee.EnabledLookup = True
    lkuItemId.Protected = False
    lkuItemId.EnabledLookup = True
    lkuWhse.Protected = False
    lkuWhse.EnabledLookup = True
    txtRoutingId.Protected = False
    navRoutingId.Enabled = True
    txtVersionId.Protected = False
    numQuantity.Protected = False
    numScrapPcs.Protected = False
    txtComment.Protected = False
    cmdValidProd.Enabled = True
    cmdLotSerialBin.Enabled = False
    lkuAutoDistBin.SetTextAndKeyValue "", 0
    lkuAutoDistBin.Tag = ""
    lblAutoDistLot.Caption = ""
    'Bring the bin lookup to the front.
    lkuAutoDistBin.ZOrder 0
    lkuAutoDistBin.Enabled = False
    tbrMain.ButtonEnabled(kTbProceed) = False
    cmdDetail.Enabled = False
    mlTransKey = 0
    mbInitKitDist = False
    mbDistCreated = False
    mbDistDirty = False
    mdDistQty = 0
    mbProductionEntered = False
    mbLeaveCell = True
    mdQTYReqSTD = 0
    mdScrapPcsSTD = 0
    mbDistDetlDirty = False
    mdScrapPcs = 0
    chkPrintProduction = vbUnchecked
    sbrMain.Message = ""
    chkPhantomAsSubAss.Value = 0
    chkPhantomAsSubAss.Enabled = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ClearAll", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub GetStdRout()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String
Dim rs As Object
Dim sRoutingId      As String
Dim sVersionId      As String


    sSQL = "SELECT *  FROM tmfRoutHead_HAI rh WITH (NOLOCK) ,tmfRoutProd_HAI rp WITH (NOLOCK) WHERE rp.ItemKey = " & glGetValidLong(lkuItemId.KeyValue) _
         & " AND rp.RoutingKey = rh.RoutingKey" _
         & " AND rh.RollUpFlag = 1 AND rh.Active = " & gsQuoted(kRoutActive) _
         & " AND rh.CompanyID = " & gsQuoted(msCompanyID) _
         & " AND COALESCE(rh.WhseKey, 0) = " & glGetValidLong(lkuWhse.KeyValue)

    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

    If Not rs.IsEmpty Then
        mlRoutingKey = glGetValidLong(rs.Field("RoutingKey"))
        sRoutingId = gsGetValidStr(moClass.moAppDB.Lookup("RoutingId", "tmfRoutHead_HAI WITH (NOLOCK)", "RoutingKey = " & glGetValidLong(mlRoutingKey) _
                   & " AND CompanyID = " & gsQuoted(msCompanyID)))
        txtRoutingId = sRoutingId
        sVersionId = gsGetValidStr(moClass.moAppDB.Lookup("VersionId", "tmfRoutHead_HAI WITH (NOLOCK)", "RoutingKey = " & glGetValidLong(mlRoutingKey) _
                   & " AND CompanyID = " & gsQuoted(msCompanyID)))
        txtVersionId = sVersionId
        If glGetValidLong(rs.Field("CustKey")) <> 0 Then
            moDmForm.SetColumnValue "CustKey", glGetValidLong(rs.Field("CustKey"))
        End If
    Else
        rs.Close
        sSQL = ""
        sSQL = "SELECT *  FROM tmfRoutHead_HAI rh WITH (NOLOCK) ,tmfRoutProd_HAI rp WITH (NOLOCK) WHERE rp.RoutingKey = rh.RoutingKey" _
             & " AND rp.ItemKey = " & glGetValidLong(lkuItemId.KeyValue) _
             & " AND rh.RollUpFlag = 1" _
             & " AND rh.Active = " & gsQuoted(kRoutActive) _
             & " AND rh.CompanyID = " & gsQuoted(msCompanyID) _
             & " AND COALESCE(rh.WhseKey, 0) = 0"
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        If rs.IsEmpty Then
            txtRoutingId = ""
            txtVersionId = ""
        Else
            mlRoutingKey = glGetValidLong(rs.Field("RoutingKey"))
            sRoutingId = gsGetValidStr(moClass.moAppDB.Lookup("RoutingId", "tmfRoutHead_HAI WITH (NOLOCK)", "RoutingKey = " & glGetValidLong(mlRoutingKey) _
                       & " AND CompanyID = " & gsQuoted(msCompanyID)))
            txtRoutingId = sRoutingId
            sVersionId = gsGetValidStr(moClass.moAppDB.Lookup("VersionId", "tmfRoutHead_HAI WITH (NOLOCK)", "RoutingKey = " & glGetValidLong(mlRoutingKey) _
                       & " AND CompanyID = " & gsQuoted(msCompanyID)))
            txtVersionId = sVersionId
            If glGetValidLong(rs.Field("CustKey")) <> 0 Then
                moDmForm.SetColumnValue "CustKey", glGetValidLong(rs.Field("CustKey"))
            End If
        End If
    End If
    
        
        
        
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetStdRout", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CalcQuantity()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mdTotQuantity = mdQuantity + mdScrapPcs
    moDmForm.SetColumnValue "Quantity", mdTotQuantity
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CalcQuantity", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bGetNextTransKey() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bGetNextTransKey = False
    
    If mlTransKey = 0 Then
        With moClass.moAppDB
            .SetInParam "tmfWorkOrdTran_HAI"
            .SetOutParam mlTransKey
            .ExecuteSP ("spGetNextSurrogateKey")
            mlTransKey = .GetOutParam(2)
            .ReleaseParams
        End With
    End If
    
    bGetNextTransKey = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bGetNextTransKey", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub DisableContr()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    calEntryDate.Protected = True
    lkuEmployee.Protected = True
    lkuEmployee.EnabledLookup = False
    lkuItemId.Protected = True
    lkuItemId.EnabledLookup = False
    lkuWhse.Protected = True
    lkuWhse.EnabledLookup = False
    txtRoutingId.Protected = True
    navRoutingId.Enabled = False
    txtVersionId.Protected = True
    If lkuEmployee.Text = "" Then
        txtEmployeeName.Text = ""
    End If
    cmdDetail.Enabled = False
    chkPhantomAsSubAss.Enabled = False
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DisableContr", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Function bSetupGrid() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'***********************************************************
' Called from Form_Load
'***********************************************************
    
    '   Initialize Grid
' Add code here to format column headings, widths etc...

    bSetupGrid = False
     
    gGridSetProperties grdMain, 39, kGridDataSheet  'RKL DEJ Changed from 37 to 39
    gGridSetColors grdMain
    gGridSetMaxCols grdMain, 39 'RKL DEJ Changed from 37 to 39
    gGridFreezeCols grdMain, 3
    gGridSetColumnType grdMain, kColWorkOrderKey, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColWorkOrderKey, "-"
    gGridHideColumn grdMain, kColWorkOrderKey
    gGridSetColumnType grdMain, kColWorkOrderStepKey, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColWorkOrderStepKey, "-"
    gGridHideColumn grdMain, kColWorkOrderStepKey
    gGridSetColumnType grdMain, kColStepId, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColStepId, "Step"
    gGridSetColumnWidth grdMain, kColStepId, "5"
    gGridSetColumnType grdMain, kColOperationDesc1, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColOperationDesc1, "Operation Description "
    gGridSetColumnWidth grdMain, kColOperationDesc1, "16"
    gGridSetColumnType grdMain, kColOperationType, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColOperationType, "Type"
    gGridSetColumnWidth grdMain, kColOperationType, "5"
    gGridSetColumnType grdMain, kColQTYReqSTD, SS_CELL_TYPE_FLOAT, miQtyDecimal, 10
    gGridSetHeader grdMain, kColQTYReqSTD, "QTY Req"
    gGridSetColumnWidth grdMain, kColQTYReqSTD, "13"
    
    gGridSetColumnType grdMain, kColQTYTD, SS_CELL_TYPE_FLOAT, miQtyDecimal, 10
    gGridSetHeader grdMain, kColQTYTD, "QTY TD"
    gGridSetColumnWidth grdMain, kColQTYTD, "13"
    'gGridHideColumn grdMain, kColQTYTD
    
    gGridSetColumnType grdMain, kColOperationId, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColOperationId, "Operation"
    gGridSetColumnWidth grdMain, kColOperationId, "8"
    gGridSetColumnType grdMain, kColWorkCenterId, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColWorkCenterId, "Work Center"
    gGridSetColumnWidth grdMain, kColWorkCenterId, "8"
    
    gGridSetColumnType grdMain, kColMatItemId, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColMatItemId, "Material Item"
    gGridSetColumnWidth grdMain, kColMatItemId, "8"
    
    gGridSetColumnType grdMain, kColMatWhseId, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColMatWhseId, "Warehouse"
    gGridSetColumnWidth grdMain, kColMatWhseId, "8"
    
    gGridSetHeader grdMain, kColMachineId, "Machine"
    gGridSetColumnWidth grdMain, kColMachineId, "8"
    
    gGridSetColumnType grdMain, kColRunHrsSTD, SS_CELL_TYPE_FLOAT, 2, 8
    gGridSetHeader grdMain, kColRunHrsSTD, "Run Hrs"
    gGridSetColumnWidth grdMain, kColRunHrsSTD, "8"
    
    gGridSetColumnType grdMain, kColSetCostSTD, SS_CELL_TYPE_FLOAT, miCurrDecPlaces, 10
    gGridSetHeader grdMain, kColSetCostSTD, "Set Cost"
    gGridSetColumnWidth grdMain, kColSetCostSTD, "13"
    gGridSetColumnType grdMain, kColSetCostTD, SS_CELL_TYPE_FLOAT, miCurrDecPlaces, 10
    gGridSetHeader grdMain, kColSetCostTD, "Set Cost TD"
    gGridSetColumnWidth grdMain, kColSetCostTD, "13"
    gGridHideColumn grdMain, kColSetCostTD
    
    gGridSetColumnType grdMain, kColRunHrsTD, SS_CELL_TYPE_FLOAT, 2, 8
    gGridSetHeader grdMain, kColRunHrsTD, "Run Hrs TD"
    gGridSetColumnWidth grdMain, kColRunHrsTD, "13"
    'gGridHideColumn grdMain, kColRunHrsTD
    
    gGridSetColumnType grdMain, kColRunCostSTD, SS_CELL_TYPE_FLOAT, miCurrDecPlaces, 10
    gGridSetHeader grdMain, kColRunCostSTD, "Run Cost"
    gGridSetColumnWidth grdMain, kColRunCostSTD, "13"
    gGridSetColumnType grdMain, kColRunCostTD, SS_CELL_TYPE_FLOAT, miCurrDecPlaces, 10
    gGridSetHeader grdMain, kColRunCostTD, "Run Cost TD"
    gGridSetColumnWidth grdMain, kColRunCostTD, "13"
    gGridHideColumn grdMain, kColRunCostTD
    gGridSetColumnType grdMain, kColMatCostSTD, SS_CELL_TYPE_FLOAT, miCurrDecPlaces, 10
    gGridSetHeader grdMain, kColMatCostSTD, "Mat Cost"
    gGridSetColumnWidth grdMain, kColMatCostSTD, "13"
    gGridSetColumnType grdMain, kColMatCostTD, SS_CELL_TYPE_FLOAT, miCurrDecPlaces, 10
    gGridSetHeader grdMain, kColMatCostTD, "Mat Cost TD"
    gGridSetColumnWidth grdMain, kColMatCostTD, "10"
    gGridHideColumn grdMain, kColMatCostTD
    gGridSetColumnType grdMain, kColScrapPcsSTD, SS_CELL_TYPE_FLOAT, miQtyDecimal, 10
    gGridSetHeader grdMain, kColScrapPcsSTD, "Scrap Pcs"
    gGridSetColumnWidth grdMain, kColScrapPcsSTD, "10"
    
    gGridSetColumnType grdMain, kColScrapPcsTD, SS_CELL_TYPE_FLOAT, miQtyDecimal, 10
    gGridSetHeader grdMain, kColScrapPcsTD, "Scrap Pcs TD"
    gGridSetColumnWidth grdMain, kColScrapPcsTD, "10"
    'gGridHideColumn grdMain, kColScrapPcsTD
    
    gGridSetColumnWidth grdMain, kColSetHrsSTD, "10"
    gGridSetColumnType grdMain, kColSetHrsSTD, SS_CELL_TYPE_FLOAT, 2, 8
    gGridSetHeader grdMain, kColSetHrsSTD, "Setup Hrs"
    
    gGridSetColumnWidth grdMain, kColSetHrsTD, "10"
    gGridSetColumnType grdMain, kColSetHrsTD, SS_CELL_TYPE_FLOAT, 2, 8
    gGridSetHeader grdMain, kColSetHrsTD, "Set Hrs TD"
'    gGridHideColumn grdMain, kColSetHrsTD

    gGridSetColumnType grdMain, kColDownHrsSTD, SS_CELL_TYPE_FLOAT, 2, 8
    gGridSetHeader grdMain, kColDownHrsSTD, "Down Hrs STD"
    gGridHideColumn grdMain, kColDownHrsSTD
    
    gGridSetColumnType grdMain, kColProgressStep, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColProgressStep, "-"
    gGridHideColumn grdMain, kColProgressStep
    gGridSetColumnType grdMain, kColTotCostStd, SS_CELL_TYPE_FLOAT, miCurrDecPlaces, 10
    gGridSetHeader grdMain, kColTotCostStd, "Total Cost Std"
    gGridHideColumn grdMain, kColTotCostStd
    gGridSetColumnType grdMain, kColTotCostTD, SS_CELL_TYPE_FLOAT, miCurrDecPlaces, 10
    gGridSetHeader grdMain, kColTotCostTD, "Total Cost TD"
    gGridSetColumnType grdMain, kColWhseKey, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColWhseKey, "-"
    gGridHideColumn grdMain, kColWhseKey
    gGridSetColumnType grdMain, kColMatItemKey, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColMatItemKey, "-"
    
    gGridHideColumn grdMain, kColMatItemKey
    gGridHideColumn grdMain, kColTotCostTD
    gGridHideColumn grdMain, kColBackFlush
    gGridHideColumn grdMain, kColOperCostToWip
    gGridHideColumn grdMain, kColQTYPerDesc
    gGridHideColumn grdMain, kColMachineKey
    gGridHideColumn grdMain, kColOperationKey
    gGridHideColumn grdMain, kColWorkCenterKey
    gGridHideColumn grdMain, kColDetlTransKey
    gGridLockColumn grdMain, kColSetHrsSTD
    gGridLockColumn grdMain, kColSetHrsTD
    gGridLockColumn grdMain, kColStepId
    gGridLockColumn grdMain, kColMachineId
    gGridLockColumn grdMain, kColOperationDesc1
    gGridLockColumn grdMain, kColOperationType
    gGridLockColumn grdMain, kColQTYTD
    gGridLockColumn grdMain, kColOperationId
    gGridLockColumn grdMain, kColWorkCenterId
    gGridLockColumn grdMain, kColMatItemId
    gGridLockColumn grdMain, kColMatWhseId
    gGridLockColumn grdMain, kColRunHrsSTD
    gGridLockColumn grdMain, kColRunCostSTD
    gGridLockColumn grdMain, kColRunHrsTD
    gGridLockColumn grdMain, kColSetCostSTD
    gGridLockColumn grdMain, kColRunCostSTD
    gGridLockColumn grdMain, kColMatCostSTD
    gGridLockColumn grdMain, kColSetCostTD
    gGridLockColumn grdMain, kColMatCostTD
    gGridLockColumn grdMain, kColScrapPcsSTD
    gGridLockColumn grdMain, kColScrapPcsTD
    gGridLockColumn grdMain, kColDownHrsSTD
    gGridLockColumn grdMain, kColTotCostStd
    gGridLockColumn grdMain, kColTotCostTD
    gGridLockColumn grdMain, kColQTYTD
    gGridLockColumn grdMain, kColQTYReqSTD
   
'RKL DEJ (START)
    gGridSetColumnType grdMain, kColUOM, SS_CELL_TYPE_EDIT
    gGridSetHeader grdMain, kColUOM, "Unit Of Measure ID"
    gGridSetColumnWidth grdMain, kColUOM, "10"
    
    
    gGridSetColumnType grdMain, kColQtyOnHand, SS_CELL_TYPE_FLOAT, miQtyDecimal, 10
    gGridSetHeader grdMain, kColQtyOnHand, "Qty On Hand"
    gGridSetColumnWidth grdMain, kColQtyOnHand, "10"
    
    gGridLockColumn grdMain, kColUOM
    gGridLockColumn grdMain, kColQtyOnHand
    
    gGridHideColumn grdMain, kColRunHrsSTD
    gGridHideColumn grdMain, kColRunCostSTD
    gGridHideColumn grdMain, kColSetHrsSTD
    gGridHideColumn grdMain, kColSetCostSTD
    
'RKL DEJ (STOP)
   
   bSetupGrid = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bSetupGrid", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bValidProduction() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim iRange         As Integer
    Dim iDelAll        As Integer
    Dim iDelDist       As Integer
    Dim sResultingItemList As String
    
    iDelDist = 2  ' Flag to delete just a Detail
    iDelAll = kDeleteDetlScrap ' 2 - Delete any Detail records from temp table
    bValidProduction = False
    
    If grdMain.MaxRows = 1 Then
        InsertItem
        bWOCreateSteps
        moDMGrid.Where = "CompanyID = " & gsQuoted(msCompanyID) & " And WorkOrderKey = " & glGetValidLong(mlWorkOrderKey)
        moDMGrid.Init
    Else
        DeleteScrap lkuProdEntryNo.Text, iDelAll
        DeleteKitDist (iDelDist)
        iRange = 0
        WOCalc iRange, ""
    End If
    
    If Not bValidBackFlushPrefBinExistForWO(moClass, mlWorkOrderKey, sResultingItemList) Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgMFMissingPrefBinList, sResultingItemList
        numScrapPcs = 0
        numQuantity = 0
        mdScrapPcs = 0
        mdQuantity = 0
        Exit Function
    End If
    
    If Not bValidQuantity Then
        Exit Function
    Else
        moDMGrid.Refresh
        mdScrapPcsSTD = 0
    End If
   
    bValidProduction = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bValidProduction", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Public Sub DMGridRowLoaded(DMO As Object, Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sOperType As String
Dim sBackFlush As String
Dim sProgressStep As String
If moDmForm.State = kDmStateLocked Then Exit Sub

sOperType = gsGridReadCellText(grdMain, Row, kColOperationType)
sBackFlush = gsGridReadCellText(grdMain, Row, kColBackFlush)
sProgressStep = gsGridReadCell(grdMain, Row, kColProgressStep)
If sProgressStep = "N" Then
    If sBackFlush = "N" Then
        gGridUnlockCell grdMain, kColQTYReqSTD, Row
        moDMGrid.SetRowDirty Row
        mbGridDirty = True
        If sOperType = "L" Then
            gGridUnlockCell grdMain, kColSetHrsSTD, Row
            gGridUnlockCell grdMain, kColRunHrsSTD, Row
            gGridUnlockCell grdMain, kColScrapPcsSTD, Row
            moDMGrid.SetRowDirty Row
            mbGridDirty = True
        End If
    End If
End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DMGridRowLoaded", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Function bTransExist() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sSQL As String
    Dim rs As Object
    If Len(Trim(lkuProdEntryNo)) <> 0 Then
    If mlWorkOrderKey <> 0 Then
        sSQL = "SELECT WorkOrderKey FROM tmfWorkOrdTran_HAI WITH (NOLOCK)" _
             & " WHERE WorkOrderKey = " & glGetValidLong(mlWorkOrderKey) _
             & " AND CompanyID = " & gsQuoted(msCompanyID)
               
                                             
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        If rs.IsEmpty Then
            bTransExist = False
        Else
            bTransExist = True
        End If
    End If
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bTransExist", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Sub InsertItem()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String
sSQL = "INSERT INTO tmfWorkOrdProd_HAI (WorkOrderKey, ItemKey, QTYCycle, QTYTotal, QTYToDate, CompanyID, UpdateCounter)" _
                                & " VALUES (" & glGetValidLong(mlWorkOrderKey) & "," _
                                & glGetValidLong(lkuItemId.KeyValue) & "," _
                                & 1 & "," _
                                & gdGetValidDbl(mdTotQuantity) & "," _
                                & 0 & "," _
                                & gsQuoted(msCompanyID) & "," _
                                & 0 & ")"
 moClass.moAppDB.ExecuteSQL (sSQL)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                               'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "InsertItem", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub GetItem()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

Dim sSQL As String
Dim rs As Object
    
    If mlWorkOrderKey <> 0 Then
        sSQL = "SELECT ItemKey FROM tmfWorkOrdProd_HAI WITH (NOLOCK)" _
             & " WHERE WorkOrderKey = " & glGetValidLong(mlWorkOrderKey) _
             & " AND CompanyID = " & gsQuoted(msCompanyID)
               
                                             
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        If rs.IsEmpty Then
            lkuItemId.Text = ""
            Exit Sub
        Else
            lkuItemId.KeyValue = glGetValidLong(rs.Field("ItemKey"))
            txtItemDesc = gsGetValidStr(moClass.moAppDB.Lookup("ShortDesc", "timItemDescription WITH (NOLOCK)", _
"ItemKey = " & glGetValidLong(lkuItemId.KeyValue)))
        End If
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetItem", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bKitDistGridInfo(lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim bInitKitDist  As Boolean
    Dim iDelDist As Integer
    
    Dim bCanBeBackFlushedFromABin As Boolean
    Dim sBackFlush As String
    Dim lBackFlushWhseBinKey As Long
    Dim iBackflushFromPrefBin As Integer
    Dim sSQL As String
    Dim rs As Object

    iDelDist = 2 ' delete detail record if qty = 0
    bKitDistGridInfo = False
    mlWorkOrderStepKey = glGetValidLong(gsGridReadCell(grdMain, lRow, kColWorkOrderStepKey))
    mlMatItemKey = glGetValidLong(gsGridReadCell(grdMain, lRow, kColMatItemKey))
    mlDetlWhseKey = glGetValidLong(gsGridReadCell(grdMain, lRow, kColWhseKey))
    mdMatQuantity = gdGetValidDbl(gsGridReadCell(grdMain, lRow, kColQTYReqSTD))
    miMatTrackMethod = 0
    miMatTrackBins = 0
    bCanBeBackFlushedFromABin = False
    
    miMatTrackBins = giGetValidInt(moClass.moAppDB.Lookup("TrackQtyAtBin", "timWarehouse WITH (NOLOCK)", _
                     "WhseKey = " & glGetValidLong(mlDetlWhseKey) & " AND CompanyID = " & gsQuoted(msCompanyID)))
    
    sSQL = "SELECT TrackMeth, StockUnitMeasKey FROM timItem WITH (NOLOCK) WHERE ItemKey = " & glGetValidLong(mlMatItemKey)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEmpty Then
        miMatTrackMethod = giGetValidInt(rs.Field("TrackMeth"))
        mlMatStockUOMKey = glGetValidLong(rs.Field("StockUnitMeasKey"))
    End If
    
    If miMatTrackBins = 1 Then
        'Close the rs so it can be reused.
        rs.Close
        
        'Since the warehouse tracks quantity at bin, see if the line is marked as back flush and if so, make sure
        'we can determine the bin to use to perform the distributions.
        sSQL = "SELECT BackFlush, BackflushFromPrefBin, BackFlushWhseBinKey FROM tmfWorkOrdDetl_HAI WITH (NOLOCK) " & _
               "WHERE WorkOrderStepKey = " & glGetValidLong(mlWorkOrderStepKey)
    
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
        If Not rs.IsEmpty Then
            sBackFlush = UCase(Trim(gsGetValidStr(rs.Field("BackFlush"))))
            lBackFlushWhseBinKey = glGetValidLong(rs.Field("BackFlushWhseBinKey"))
            iBackflushFromPrefBin = giGetValidInt(rs.Field("BackflushFromPrefBin"))
            
            If sBackFlush = "Y" And (lBackFlushWhseBinKey <> 0 Or iBackflushFromPrefBin = 1) Then
                bCanBeBackFlushedFromABin = True
            Else
                bCanBeBackFlushedFromABin = False
            End If
        End If
    End If
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    If (miMatTrackBins = 1 Or miMatTrackMethod <> 0) And bCanBeBackFlushedFromABin = False Then
        bKitDistGridInfo = True
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                             'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bKitDistGridInfo", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Private Function bInsertTransaction() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sSQL As String
    Dim tmpStatus As Integer
    Dim sTimeStamp As String
    Dim dDate As String
    Dim sComment As String
    Dim lRow As Long
    
    bInsertTransaction = False
    
    If bProgressStep(lRow) Then
        If bGetLaborInf Then
            With moClass.moAppDB
                .SetInParam msCompanyID
                .SetOutParam msTransNo
                .SetOutParam tmpStatus
                .ExecuteSP "spmfGetNextTr"
                msTransNo = .GetOutParam(2)
                tmpStatus = .GetOutParam(3)
                .ReleaseParams
            End With
            
            If tmpStatus = 0 Then
                msTransNo = gsMFZeroFill(msTransNo, 8)
            Else
                giSotaMsgBox Me, moClass.moSysSession, kmsgMFOptionNotFound
                Exit Function
            End If
        
            If mlTransKey = 0 Then
               bGetNextTransKey
            End If
        
            dDate = gsFormatDateToDB(calEntryDate)
            sComment = txtComment.Text
            
            sSQL = "if  object_id('tempdb..#tmfWorkOrdTran_HAI') is null" & vbCrLf
            sSQL = sSQL & "SELECT * INTO #tmfWorkOrdTran_HAI FROM tmfWorkOrdTran_HAI WHERE 1 = 2 " & vbCrLf
            sSQL = sSQL & "ELSE TRUNCATE TABLE #tmfWorkOrdTran_HAI "
            moClass.moAppDB.ExecuteSQL (sSQL)
            
            sSQL = " INSERT INTO #tmfWorkOrdTran_HAI (TransactionNo, TransDate, Quantity, ScrapPcs," _
                                & " LaborType, StartTime, EndTime, StartTimeMin, EndTimeMin," _
                                & " StartTimeMeridian, EndTimeMeridian, StepComplete, Comment, Type," _
                                & " TOTHrs, QTYPerDesc, OperationType, TransactionKey, CompanyID," _
                                & " WorkCenterKey, MachineKey, EmployeeKey, WorkOrderKey, WorkOrderStepKey," _
                                & " OperationKey, WhseKey, ItemKey, ProduceItemKey, DownTimeKey, SetHrs, SetCost," _
                                & " SetFix, SetVar, RunHrs, RunCost, RunFix, RunVar, DownHrs, MatOutOtherCost," _
                                & " OUT, Mat, Other, ReverseFlag, Shift, QTYCycle, DayTimeStamp, EndDate," _
                                & " RefTransNum, NotesKey," _
                                & " UpdateCounter, BatchKey, CreateType)" _
                    & " VALUES (" & gsQuoted(msTransNo) & "," & gsQuoted(dDate) & "," & gdGetValidDbl(mdQuantity) & "," & gdGetValidDbl(mdScrapPcs) & "," _
                                & "'R', 0, 0, '', '', " _
                                & "'AM', 'AM', 'Y', " & gsQuoted(sComment) & ", 'L', " _
                                & "0 ," & gsQuoted(msQTYPerDesc) & ", 'L', " & glGetValidLong(mlTransKey) & ", " & gsQuoted(msCompanyID) & ", " _
                                & glGetValidLong(mlWorkCenterKey) & ", " & IIf(glGetValidLong(mlMachineKey) = 0, "NULL", glGetValidLong(mlMachineKey)) & "," _
                                & glGetValidLong(lkuEmployee.KeyValue) & "," & glGetValidLong(mlWorkOrderKey) & "," & glGetValidLong(mlWorkOrderStepKey) & "," _
                                & glGetValidLong(mlOperationKey) & "," & glGetValidLong(lkuWhse.KeyValue) & ", NULL," & glGetValidLong(lkuItemId.KeyValue) & ", NULL, 0, 0, " _
                                & "0 , 0, " & gdGetValidDbl(mdRunHrs) & "," & gdGetValidDbl(mdRunCost) & "," & gdGetValidDbl(mdRunFix) & "," & gdGetValidDbl(mdRunVar) & ", 0, 0, " _
                                & "0, 0, 0, 'N', " & giGetValidInt(miShift) & ", 1," & gsQuoted(gsFormatDateToDB(msTimeStamp)) & "," & gsQuoted(gsFormatDateToDB(Now)) & ", " _
                                & "NULL, NULL, " _
                                & " 0, " & glGetValidLong(mlBatchKey) & ", 1)"
               
            moClass.moAppDB.ExecuteSQL (sSQL)
            
        End If
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bInsertTransaction", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bProgressStep(lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    bProgressStep = False
    msProgressStep = ""
    
    For lRow = 1 To grdMain.MaxRows - 1
        msProgressStep = gsGridReadCell(grdMain, lRow, kColProgressStep)
        If msProgressStep = "Y" Then
            mlWorkOrderStepKey = glGetValidLong(gsGridReadCell(grdMain, lRow, kColWorkOrderStepKey))
            mlWorkCenterKey = glGetValidLong(gsGridReadCell(grdMain, lRow, kColWorkCenterKey))
            mlMachineKey = glGetValidLong(gsGridReadCell(grdMain, lRow, kColMachineKey))
            mlOperationKey = glGetValidLong(gsGridReadCell(grdMain, lRow, kColOperationKey))
            msQTYPerDesc = gsGetValidStr(gsGridReadCell(grdMain, lRow, kColQTYPerDesc))
            mdRunHrs = gdGetValidDbl(gsGridReadCell(grdMain, lRow, kColRunHrsSTD))
            mdRunCost = gdGetValidDbl(gsGridReadCell(grdMain, lRow, kColRunCostSTD))
            msOperCostToWip = gsGetValidStr(gsGridReadCell(grdMain, lRow, kColOperCostToWip))
            msStepID = gsGetValidStr(gsGridReadCell(grdMain, lRow, kColStepId))
            msTranNo = (lkuProdEntryNo.Text + "-" + msStepID)
            Exit For
        End If
    Next
    
    If msProgressStep = "" Then
        Exit Function
    End If
    
    bProgressStep = True


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bProgressStep", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Function bGetLaborInf() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim rs As Object
    Dim sSQL As String
    Dim sBatchNo As String
    Dim lPoststatus As Long
    Dim mdEmplRate As Double

    bGetLaborInf = False
    
    ' Get Employee Rate
    sSQL = "SELECT * FROM tmfEmployee_HAI WITH (NOLOCK) WHERE " _
         & "EmployeeKey = " & glGetValidLong(lkuEmployee.KeyValue)
           
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If Not rs.IsEmpty Then
        mdEmplRate = gdGetValidDbl(rs.Field("EmployeeRate"))
        miShift = giGetValidInt(rs.Field("EmployeeShift"))
    End If
    rs.Close
    
    ' Calc RunFix and RunVar paste on WorkCenter and Employee information
    sSQL = "SELECT * FROM tmfWorkCenter_HAI WITH (NOLOCK) WHERE " _
         & "WorkCenterKey = " & glGetValidLong(mlWorkCenterKey)
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEmpty Then
        mdRunCost = mdRunHrs * (gdGetValidDbl(rs.Field("WrkCntrEstRunRate")) + mdEmplRate)
        
        If msOperCostToWip <> "Y" Then
            mdRunFix = 0
            mdRunVar = 0
        Else
            mdRunFix = mdRunHrs * gdGetValidDbl(rs.Field("WrkCntrFixedRunRate"))
            mdRunVar = mdRunHrs * (gdGetValidDbl(rs.Field("WrkCntrVarRunRate")) + mdEmplRate)
        End If
    End If
    rs.Close
    
    ' Get Time Stamp
    With moClass.moAppDB
        .SetOutParam msTimeStamp
        .ExecuteSP "spmfGetDateAndTime"
        msTimeStamp = .GetOutParam(1)
        .ReleaseParams
    End With
    
    mlBatchKey = glGetWipBatchNo("L", frmmfzdz001, moClass, msCompanyID)
    
    If mlBatchKey = 0 Then
        GoTo badexit
    End If
    
    bGetLaborInf = True

    Exit Function

badexit:
    giSotaMsgBox Me, moClass.moSysSession, kmsgGetSurrogateKeyFailure, "Batch"

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bGetLaborInf", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub GetLaborEntry()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String
Dim rs As Object
    
    If mlWorkOrderKey <> 0 Then
        sSQL = "SELECT * " _
             & "  FROM tmfWorkOrdTran_HAI wot WITH (NOLOCK) , tmfWorkOrdDetl_HAI wod WITH (NOLOCK) " _
             & " WHERE wot.WorkOrderKey = " & glGetValidLong(mlWorkOrderKey) _
             & "   AND wot.CompanyID = " & gsQuoted(msCompanyID) _
             & "   AND wot.WorkOrderKey = wod.WorkOrderKey " _
             & "   AND wot.WorkOrderStepKey = wod.WorkOrderStepKey " _
             & "   AND wod.ProgressStep = 'Y' " _
             & "   AND wot.Type = 'L'"
                                             
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
        If rs.IsEmpty Then
            lkuEmployee.Text = ""
            numQuantity = 0
            numScrapPcs = 0
            txtComment.Text = ""
            Exit Sub
        Else
            lkuEmployee.KeyValue = glGetValidLong(rs.Field("EmployeeKey"))
            txtEmployeeName = gsGetValidStr(moClass.moAppDB.Lookup("EmployeeName", "tmfEmployee_HAI WITH (NOLOCK)", "EmployeeKey = " & gsQuoted(lkuEmployee.KeyValue) & " AND CompanyID = " & gsQuoted(msCompanyID)))
            numQuantity = gdGetValidDbl(rs.Field("Quantity"))
            numScrapPcs = gdGetValidDbl(rs.Field("ScrapPcs"))
            txtComment = gsGetValidStr(rs.Field("Comment"))
            txtWhseDesc = gsGetValidStr(moClass.moAppDB.Lookup("Description", "timWarehouse WITH (NOLOCK)", _
                          "WhseKey = " & glGetValidLong(lkuWhse.KeyValue)))
                          
            mlTransKey = glGetValidLong(rs.Field("TransactionKey"))
            numQuantity.Protected = True
            numScrapPcs.Protected = True
            txtComment.Protected = True
        End If
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetLaborEntry", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub UpdWOTranProd()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sSQL   As String
    
    sSQL = "if  object_id('tempdb..#tmfWOTranProd_HAI') is null" & vbCrLf
    sSQL = sSQL & "SELECT * INTO #tmfWOTranProd_HAI FROM tmfWOTranProd_HAI WHERE 1 = 2 " & vbCrLf
    sSQL = sSQL & "ELSE TRUNCATE TABLE #tmfWOTranProd_HAI "
    
    moClass.moAppDB.ExecuteSQL (sSQL)
            
    sSQL = " INSERT INTO #tmfWOTranProd_HAI (TransactionKey, PartItemKey, PartQuantity, PartScrapPcs," _
         & " CompanyID, UpdateCounter)" _
         & " VALUES (" & glGetValidLong(mlTransKey) & "," & glGetValidLong(lkuItemId.KeyValue) & "," _
         & gdGetValidDbl(mdQuantity) & "," & gdGetValidDbl(mdScrapPcs) & "," _
         & gsQuoted(msCompanyID) & "," & 0 & ")"
    
    moClass.moAppDB.ExecuteSQL (sSQL)
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "UpdWOTranProd", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function lInitializeReport(ModuleID As String, DefaultReport As String, ProjectName As String) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
   lInitializeReport = -1
    
    Set moPrinter = Printer
    Set moSotaObjects = New Collection
    Set moReport = New clsReportEngine
    Set moDDData = New clsDDData
    
        
    If mbPeriodEnd Then
       moReport.UI = False
    End If
   
    If Not moDDData.lInitDDData(sRealTableCollection, moClass.moAppDB, moClass.moAppDB, msCompanyID) = kSuccess Then
        lInitializeReport = kmsgCantLoadDDData
        Exit Function
    End If

    If Not moDDData.lDDBuildData(App.EXEName) = kSuccess Then
        lInitializeReport = kmsgCantLoadDDData
        Exit Function
    End If
    
    moReport.AppOrSysDB = kAppDB
    
    If (moReport.lInitReport(ModuleID, DefaultReport, Me, moDDData) = kFailure) Then
        lInitializeReport = kmsgFatalReportInit
        Exit Function
    End If
   
      
    lInitializeReport = 0
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lInitializeReport", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub UpdWOComplete()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String
        
    sSQL = "Update tmfWorkOrdHead_HAI SET Complete = " & gsQuoted(kWOComplete) & "," _
         & " CompleteDate = " & kSQuote & gsFormatDateToDB(moClass.moSysSession.BusinessDate) & kSQuote _
         & " WHERE WorkOrderKey= " & mlWorkOrderKey
            
    moClass.moAppDB.ExecuteSQL (sSQL)
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "UpdWOComplete", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub UpdateCloseout()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    With moClass.moAppDB
        .SetInParam mlWorkOrderKey
        .SetInParam msCompanyID
        .ExecuteSP "spmfUpdtCloseoutTrans"
        .ReleaseParams
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "UpdateCloseout", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bEditDist() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bEditDist = False
    
    If Abs(Val(numQuantity)) = 0 Then
        bEditDist = True
        Exit Function
    End If
    
    If mbDoAutoDist = True Then
        If Not bAutoDistFromBinLku = True Then
            Exit Function
        End If
    End If
    
    If (Not mbDistDirty Or Abs(gdGetValidDbl(mdDistQty)) <> Abs(gdGetValidDbl(numQuantity))) And cmdLotSerialBin.Enabled Then
'        If moDmForm.State = kDmStateAdd Then
            If Abs(gdGetValidDbl(mdDistQty)) <> Abs(gdGetValidDbl(Val(numQuantity))) Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgTranQtyDistQtyDifferent
            Else
                giSotaMsgBox Me, moClass.moSysSession, kmsgNoDist
            End If
            gbSetFocus Me, cmdLotSerialBin
            Exit Function
'        End If
    End If

    bEditDist = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bEditDist", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Private Function bEditDetlDist() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String
Dim rs As Object
Dim dTranQty As Double
    bEditDetlDist = False
    
    If gdGetValidDbl(mdMatQuantity) = 0 Then
        bEditDetlDist = True
        Exit Function
    End If
    
    sSQL = "SELECT * FROM  #timInvtDistWrk  WITH (NOLOCK) WHERE TranIdentifier = " & glGetValidLong(mlWorkOrderStepKey) & " AND CompanyID = " & gsQuoted(msCompanyID)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEmpty Then
        If Abs(gdGetValidDbl(moClass.moAppDB.Lookup("SUM(TranQty)", "#timInvtDistWrk", "TranIdentifier = " & _
                                                glGetValidLong(mlWorkOrderStepKey)))) <> Abs(gdGetValidDbl(mdMatQuantity)) Then

            giSotaMsgBox Me, moClass.moSysSession, kmsgTranQtyDistQtyDifferent
            Exit Function
        End If
    Else
        giSotaMsgBox Me, moClass.moSysSession, kmsgNoDist
        Exit Function
    End If
        
    bEditDetlDist = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bEditDetlDis", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub SetTags()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
numQuantity.Tag = numQuantity.Text
numScrapPcs.Tag = numScrapPcs.Text
txtComment.Tag = txtComment.Text
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetTags", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Sub CheckBackFlush()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lRowStep As Long
Dim sBackFlush As String
Dim lBFStep As Long
Dim lRowCount As Long
Dim sProgressStep As String
   'Check if it any steps are not BackFlush
    For lRowStep = 1 To grdMain.MaxRows - 1
        sBackFlush = gsGridReadCellText(grdMain, lRowStep, kColBackFlush)
        sProgressStep = gsGridReadCell(grdMain, lRowStep, kColProgressStep)
        If sProgressStep = "N" Then
            If sBackFlush = "N" Then
                lBFStep = 1
                lRowCount = lRowCount + lBFStep
            End If
        End If
       
    Next

    If lRowCount > 0 Then
        moDMGrid.SetDirty True
        mbGridDirty = True
        Exit Sub
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CheckBackFlush", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub DeleteKitDist(iDelDist As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lRow As Long
Dim sOperationType As String
Dim sSQL As String
If moKitDist Is Nothing Then Exit Sub

Select Case iDelDist
    Case 1 'Delete Header and Detail distribution records from temp table
        sSQL = "IF object_id('tempdb..#timInvtDistWrk') IS NOT NULL TRUNCATE TABLE #timInvtDistWrk"
        moClass.moAppDB.ExecuteSQL (sSQL)
    Case 2 'Delete only Detail records from temp table
        If grdMain.DataRowCnt <> 0 Then
        For lRow = 1 To grdMain.DataRowCnt
            sOperationType = gsGetValidStr(gsGridReadCell(grdMain, lRow, kColOperationType))
            If sOperationType <> "L" Then
                If bKitDistGridInfo(lRow) Then
                    moKitDist.Cancel mlWorkOrderStepKey
                End If
            End If
        Next lRow
        End If
    Case 3 'Delete only Header record from temp table
        moKitDist.Cancel mlTransKey
End Select
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DeleteKitDist", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Sub PrintProduction()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sPrintButton As String

    If ddnOutput.ListIndex = 0 Then
        sPrintButton = kTbPrint
    Else
        sPrintButton = kTbPreview
    End If
    
    sbrMain.Message = "Setting Report...Please Wait..."
    
    'gbSkipPrintDialog = False                'show print dialog one time
    
    lStartReport sPrintButton, Me, False, "", "", ""
    
    sbrMain.Message = ""
        
    If sPrintButton = kTbPreview Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgPrintingSuccess
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "PrintProduction", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub MapControls()
'+++ VB/Rig Begin Push +++                                                                'MAS 500 Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'MAS 500 Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
'*********************************************************************
'   Description:
'       MapControls will map the controls on the form to the buttons
'       that have Function key equivalents.  The general procedure
'       giCollectionAdd is used to add the button control as
'       an object Item to the collection and use the mapped
'       entry control's window handle as the key into the collection.
'*********************************************************************
    'Map Search buttons
    giCollectionAdd moMapSrch, navRoutingId, txtRoutingId.hwnd
    giCollectionAdd moMapSrch, navRoutingId, txtVersionId.hwnd
            
'+++ VB/Rig Begin Pop +++                                                                 'MAS 500 Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'MAS 500 Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MapControls", VBRIG_IS_FORM                      'MAS 500 Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
End Sub
Public Function bValidInvStatus(lItemKey As Long, lWhseKey As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iStatus As Integer
bValidInvStatus = False
iStatus = giGetValidInt(moClass.moAppDB.Lookup _
("Status", "timInventory WITH (NOLOCK)", "ItemKey = " & gsQuoted(lItemKey) & " AND WhseKey = " & _
gsQuoted(lWhseKey)))
If iStatus <> kActive Then
    Exit Function
End If
bValidInvStatus = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bValidInvStatus", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Sub SetModOptions()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sCurrID As String
    
    'Activate the Module Options
    Set moModOptions.oAppDB = moClass.moAppDB
    moModOptions.sCompanyID = msCompanyID
    
    'Set Decimal Values
    miPriceDecimal = moModOptions.CI("UnitPriceDecPlaces")
    miCostDecimal = moModOptions.CI("UnitCostDecPlaces")
    miQtyDecimal = moModOptions.CI("QtyDecPlaces")
        
    sCurrID = moClass.moAppDB.Lookup("CurrID", "tsmCompany WITH (NOLOCK)", "CompanyID = " & gsQuoted(msCompanyID))
    miCurrDecPlaces = giGetValidInt(moClass.moAppDB.Lookup("DigitsAfterDecimal", _
                      "tmcCurrency WITH (NOLOCK)", "CurrID = " & gsQuoted(sCurrID)))
    
'+++ VB/Rig Begin Pop +++                                                                 'MAS 500 Repository Error Rig  {1.1.1.0.0}
Exit Sub                                                                              'MAS 500 Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetModOptions", VBRIG_IS_FORM                  'MAS 500 Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub SetDecVal()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    numQuantity.DecimalPlaces = miQtyDecimal
    numScrapPcs.DecimalPlaces = miQtyDecimal
    
'+++ VB/Rig Begin Pop +++                                                                 'MAS 500 Repository Error Rig  {1.1.1.0.0}
Exit Sub                                                                              'MAS 500 Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetDecVal", VBRIG_IS_FORM                  'MAS 500 Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Sub


Private Sub grdMain_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    moGM.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop

'+++ VB/Rig Begin Pop +++                                                                 'MAS 500 Repository Error Rig  {1.1.1.0.0}
Exit Sub                                                                              'MAS 500 Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdMain_TopLeftChange", VBRIG_IS_FORM                  'MAS 500 Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Sub

Private Sub GetAutoDistSavedInfo()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim lItemKey As Long
    Dim lInvtTranKey As Long
    Dim sWhseBinID As String
    Dim lWhseBinKey As Long
    Dim bMultipleBin As Boolean
    Dim sLotNo As String

    'Lookup control does not allow any value except those that are available
    'for selection.  If we are dealing with multiple bins, bring the multiple
    'bin text box to the front and set the lookup value to an empty string.

    lItemKey = lkuItemId.KeyValue
    lInvtTranKey = 0
    
    Call moKitDist.sGetBinLkuCtrlSavedDistValues(lItemKey, mlTransKey, lInvtTranKey, bMultipleBin, sWhseBinID, lWhseBinKey, sLotNo)
    
    If bMultipleBin = True Then
        'Bring the multiple bin text box to the front.
        txtMultipleBin.ZOrder 0
        lkuAutoDistBin.SetTextAndKeyValue sWhseBinID, lWhseBinKey
        lkuAutoDistBin.Tag = ""
    Else
        'Bring the bin lookup to the front.
        lkuAutoDistBin.ZOrder 0
        'Set the values back to the bin lookup control and the grid.
        lkuAutoDistBin.SetTextAndKeyValue sWhseBinID, lWhseBinKey
        lkuAutoDistBin.Tag = sWhseBinID
    End If

    lblAutoDistLot.Caption = sLotNo
        
    If lkuAutoDistBin.Enabled = True Then
        'Set to false so it will not auto distribution on save.
        mbDoAutoDist = False
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetAutoDistSavedInfo", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bAutoDistFromBinLku() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim lInvtTranKey As Long
    Dim lItemKey As Long
    Dim lWhseKey As Long
    Dim lUOMKey As Long
    Dim dQtyToPick As Double
    Dim ocolBinRetColVals As Collection
    Dim sDoAutoDist As String

    bAutoDistFromBinLku = False
    
    lInvtTranKey = 0
    
    'Get values for auto dist method.
    Set ocolBinRetColVals = lkuAutoDistBin.ReturnColumnValues
    lItemKey = lkuItemId.KeyValue
    lWhseKey = lkuWhse.KeyValue
    lUOMKey = mlStockUOMKey
    dQtyToPick = mdQuantity
    If mlTransKey = 0 Then
       bGetNextTransKey
    End If

    'Attempt to auto distribute the bin selection.
    'If successful, this routine will assign a TranIdentifier.
    If moKitDist.bAutoDistBinLkuSelection(mlTransKey, lWhseKey, lItemKey, _
                                           9002, lUOMKey, dQtyToPick, _
                                           lkuAutoDistBin.Text, ocolBinRetColVals, lInvtTranKey) Then

        If mlTransKey > 0 Then
            'Set to false so we don't auto dist again.
            mbDoAutoDist = False
            '.ClearData will clear the data plus the return columns collection.  Must clear the
            'collection in case user decides to type a value after using the lookup.  If it is not
            'cleared, it will use the collection and distribute against the wrong set of data.
            lkuAutoDistBin.ClearData
            lkuAutoDistBin.Text = lkuAutoDistBin.Tag
            mbDistCreated = True
        Else
            Exit Function
        End If
    End If
    
    If Not mbLotControlled Then                 ' Only set the dist qty if not lot/serial tracked
        mdDistQty = gdGetValidDbl(numQuantity)  ' Bin tracked only
        mbDistDirty = True
    Else
        mdDistQty = 0                           ' Need to re-dist the lot/Serial numbers
        mbDistDirty = False
    End If
    
    bAutoDistFromBinLku = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bAutoDistFromBinLku", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Sub ProcessLaborTrans()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim oBatchLabor As Object
    Dim sTranType As String
    Dim sSQL As String
    Dim rsMFTran As DASRowSet
    Dim rsMFProd As DASRowSet
    Dim rsMFScrap As DASRowSet
    Dim rsMFDist As DASRowSet
    Dim rsIMDist As DASRowSet
    Dim lMFTranCount As Long
    Dim lMFProdCount As Long
    Dim lMFScrapCount As Long
    Dim lMFDistCount As Long
    Dim lIMDistCount As Long
    
    'Create Transaction Rowset
   ' Get data into rowset

    With moClass.moAppDB
        ' Query to pull data from table
        sSQL = "SELECT * FROM #tmfWorkOrdTran_HAI WITH (NOLOCK)"
        ' Put into Rowset
        .BeginTrans
        Set rsMFTran = .OpenRowset(sSQL, kSnapshot, kOptionNone)
        .CommitTrans
        ' Count of records in rowset (Rowset does not have recordcount prop)
        .BeginTrans
        lMFTranCount = .Lookup("Count(*)", "#tmfWorkOrdTran_HAI", "")
        .CommitTrans
    End With
    
'    'Create Parts Produced Recordset
    With moClass.moAppDB
        ' Query to pull data from table
        sSQL = "IF object_id('tempdb..#tmfWOTranProd_HAI') IS NULL SELECT * INTO #tmfWOTranProd_HAI FROM tmfWOTranProd_HAI WHERE 1=2"
        moClass.moAppDB.ExecuteSQL sSQL
        
        sSQL = "SELECT * FROM #tmfWOTranProd_HAI WITH (NOLOCK)"
        ' Put into Rowset
        .BeginTrans
        Set rsMFProd = .OpenRowset(sSQL, kSnapshot, kOptionNone)
        .CommitTrans
        
        If Not rsMFProd Is Nothing Then
        ' Count of records in rowset (Rowset does not have recordcount prop)
            .BeginTrans
            lMFProdCount = .Lookup("Count(*)", "#tmfWOTranProd_HAI", "")
            .CommitTrans
        End If
    End With

'    'Create Scrap Recordset
    With moClass.moAppDB
        ' Query to pull data from table
        sSQL = "IF object_id('tempdb..#tmfScrpRsnDistWrk') IS NULL CREATE TABLE #tmfScrpRsnDistWrk(TransactionNo VARCHAR(22) NOT NULL, CompanyID VARCHAR(3) NOT NULL, TranDistType SMALLINT NOT NULL, WorkOrderKey INTEGER NULL, WorkOrderStepKey INTEGER NULL, ScrapReasonKey INTEGER NULL, ScrapComment VARCHAR(40) NULL, ScrapPcs DECIMAL(16,8) NULL)"
        moClass.moAppDB.ExecuteSQL sSQL
        
        sSQL = "SELECT * FROM #tmfScrpRsnDistWrk WITH (NOLOCK)"
        ' Put into Rowset
        .BeginTrans
        Set rsMFScrap = .OpenRowset(sSQL, kSnapshot, kOptionNone)
        .CommitTrans
        ' Count of records in rowset (Rowset does not have recordcount prop)
        If Not rsMFScrap Is Nothing Then
            .BeginTrans
            lMFScrapCount = .Lookup("Count(*)", "#tmfScrpRsnDistWrk", "")
            .CommitTrans
        End If
    End With

    'Create MF Distribution Recordset
    With moClass.moAppDB
        ' Query to pull data from table
        sSQL = "IF object_id('tempdb..#tmfWOTranDist_HAI') IS NULL SELECT * INTO #tmfWOTranDist_HAI FROM tmfWOTranDist_HAI WHERE 1=2"
        moClass.moAppDB.ExecuteSQL sSQL
        
        sSQL = "SELECT * FROM #tmfWOTranDist_HAI WITH (NOLOCK)"
        ' Put into Rowset
        .BeginTrans
        Set rsMFDist = .OpenRowset(sSQL, kSnapshot, kOptionNone)
        .CommitTrans
                
        If Not rsMFDist Is Nothing Then
        ' Count of records in rowset (Rowset does not have recordcount prop)
            .BeginTrans
            lMFDistCount = .Lookup("Count(*)", "#tmfWOTranDist_HAI", "")
            .CommitTrans
        End If
    End With

    'Create IM Distribution Recordset
    With moClass.moAppDB
        ' Query to pull data from table
        sSQL = "IF object_id('tempdb..#timInvtDistwrk') IS NULL SELECT * INTO #timInvtDistwrk FROM timInvtDistwrk WHERE 1=2"
        moClass.moAppDB.ExecuteSQL sSQL
        
        sSQL = "SELECT * FROM #timInvtDistwrk"
        ' Put into Rowset
        .BeginTrans
        Set rsIMDist = .OpenRowset(sSQL, kSnapshot, kOptionNone)
        .CommitTrans
        
        If Not rsIMDist Is Nothing Then
        ' Count of records in rowset (Rowset does not have recordcount prop)
            .BeginTrans
            lIMDistCount = .Lookup("Count(*)", "#timInvtDistwrk", "")
            .CommitTrans
        End If
    End With
    
    sTranType = "P"
    
    On Error Resume Next
    ' For Vista OS (to ensure task comes to foreground)
    Dim bRetVal As Boolean
    bRetVal = AllowSetForegroundWindow(ASFW_ANY)
    Err.Clear
    
    #If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
    #End If
    
    Set oBatchLabor = moClass.moFramework.LoadSOTAObject(ktskMFProductionEntry, _
                                        EFW_TF_AUTOSHUTDN _
                                        + EFW_TF_MANUALLOADUI _
                                        + EFW_TF_MANUALDISPLAYUI _
                                        + EFW_TF_STANDALONE, 0)
    DoEvents
    oBatchLabor.ProcessBatch mlTransKey, sTranType, _
                             rsMFTran, rsMFProd, rsMFScrap, rsMFDist, rsIMDist, _
                             lMFTranCount, lMFProdCount, lMFScrapCount, lMFDistCount, lIMDistCount, _
                             "Y", msProgressStep, False, 0, 0, 0, chkPrintProduction.Value, ddnOutput.ListIndex
                             
    Set oBatchLabor = Nothing
    Set rsMFTran = Nothing
    Set rsMFProd = Nothing
    Set rsMFScrap = Nothing
    Set rsMFDist = Nothing
    Set rsIMDist = Nothing
        
    gbSetFocus Me, lkuProdEntryNo
    DoEvents
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ProcessLaborTrans", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Sub UpdateMFDistribution()
'+++ VB/Rig Begin Push +++                                                                'MAS 500 Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'MAS 500 Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
    ' Expand all Serial Ranges if entered.
    If Not moKitDist Is Nothing Then
        moKitDist.bExpandAllSerialRanges
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'MAS 500 Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'MAS 500 Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmmfzdz001", "UpdateMFDistribution", VBRIG_IS_FORM                  'MAS 500 Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Sub


Public Function bSetUpReport() As Boolean
'+++ VB/Rig Begin Push +++                                                                'MAS 500 Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'MAS 500 Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}

    Dim lRetVal As Long
    Dim sModule As String
    Dim sRptProgramName As String
    Dim sDefaultReport As String
    
    bSetUpReport = False
    
    'CUSTOMIZE: assign defaults for this project
    sRptProgramName = "MFZDD001" 'such as rpt001
    sModule = "MF"
    sDefaultReport = "MFZDD001.RPT" 'such as rpt001

    ' Getting the report paths
    msMFReportPath = moClass.moSysSession.ModuleReportPath("MF")

    'Set local flag to value of class module's bPeriodEnd flag.
'    mbPeriodEnd = moClass.bPeriodEnd

    Set sRealTableCollection = New Collection
    'CUSTOMIZE:  Invoke Add method of sRealTableCollection for each table which
    'will provide raw data for the report.  Primary table should appear first.
    With sRealTableCollection
        .Add "tmfWorkOrdHead_HAI"
        .Add "tmfRoutHead_HAI"

    End With

    Set sWorkTableCollection = New Collection
    'CUSTOMIZE:  Invoke Add method of sWorkTableCollection for each table which
    'will serve as a work table for the report.
    With sWorkTableCollection
        .Add "tmfWorkOrdHeadWrk"
        .Add "tmfWorkOrdDetlWrk"
        .Add "tmfRoutHeadWrk"
        .Add "tmfWorkOrdTranWrk"
    End With

    lRetVal = lInitializeReport(sModule, sDefaultReport, sRptProgramName)
    If lRetVal <> 0 Then
        moReport.ReportError lRetVal
    Else
        bSetUpReport = True
    End If
'+++ VB/Rig Begin Pop +++                                                                 'MAS 500 Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                              'MAS 500 Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmmfzdz001", "bSetUpReport", VBRIG_IS_FORM                     'MAS 500 Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Function


Private Function bIsValidProductionWorkOrder(Optional bSupressErrorMessage As Boolean = False) As Boolean
'+++ VB/Rig Begin Push +++                                                                'MAS 500 Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'MAS 500 Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}

    '*******************************************************************************
    '* This function determines if the value in the Entry No is valid for Production
    '* Entry.  A new work order is valid for Produciton Entry.  If the work order
    '* already exists, it needs to have a type for Production Entry.  A work order
    '* created in Advanced Manufacutring via the Create Work Order task is not valid
    '* for Production Entry.
    '*
    '* Since this task will delete a work order it processes if no transactions are
    '* processed (such as when the Production Entry is cancelled), we need to
    '* prevent an Advanced Manufacturing based work order from even being loaded.
    '******************************************************************************

    Dim lWorkOrderKey As Long
    Dim iWorkOrderType As Integer
    Dim oRS As DASRecordSet
    Dim sSQL As String
    Dim sEntryNo As String
    
    'Build a padded version of the work order number.
    sEntryNo = Trim(lkuProdEntryNo.Text)
    
    If IsNumeric(sEntryNo) Then
        sEntryNo = gsMFZeroFill(sEntryNo, 8)
    End If
    
    'Query to see if the work order exists already, regardless of work order type.
    sSQL = "SELECT WorkOrderKey, WorkOrderType FROM tmfWorkOrdHead_HAI WITH (NOLOCK) " & _
        " WHERE CompanyID = " & gsQuoted(msCompanyID) & " AND WorkOrderNo = " & gsQuoted(sEntryNo)
        
    Set oRS = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    'Check to see if work order exists.
    If Not oRS.IsEmpty Then
    
        'Work order exists, check the type of an existing work order.
        lWorkOrderKey = oRS.IntField("WorkOrderKey")
        iWorkOrderType = oRS.IntField("WorkOrderType")
        
        If lWorkOrderKey >= 0 Then
    
            If iWorkOrderType = kWOProduction Then
                bIsValidProductionWorkOrder = True
            Else
        
                If Not bSupressErrorMessage Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgMFInvldWOTypeForProdEntry, sEntryNo
                End If
        
            End If
            
        End If
    
    Else
        
        'Work order does not exist.  The work order will be new and created by Production Entry, so consider it valid.
        bIsValidProductionWorkOrder = True

    End If
    
    'Clean up recordset object.
    If Not oRS Is Nothing Then
        oRS.Close
        Set oRS = Nothing
    End If
        
'+++ VB/Rig Begin Pop +++                                                                 'MAS 500 Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'MAS 500 Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmmfzdz001", "bIsValidProductionWorkOrder", VBRIG_IS_FORM      'MAS 500 Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Function

