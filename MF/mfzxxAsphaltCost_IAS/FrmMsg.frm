VERSION 5.00
Begin VB.Form FrmMsg 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Calculating Costs"
   ClientHeight    =   3150
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3150
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   300
      Left            =   1800
      Top             =   1320
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "Ok"
      Enabled         =   0   'False
      Height          =   375
      Left            =   1920
      TabIndex        =   1
      Top             =   2640
      Width           =   1095
   End
   Begin VB.TextBox txtMsg 
      Height          =   2535
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      Top             =   0
      Width           =   4455
   End
End
Attribute VB_Name = "FrmMsg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdOk_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    On Error GoTo Error
    txtMsg.Text = "Please wait while the costs are calculated." & vbCrLf & "This may take a few minutes..."
    DoEvents
    
    Timer1.Enabled = True
    
    Exit Sub
Error:
    MsgBox Me.Name & ".Form_Load)()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "SAGE 500"
    
    Err.Clear
    cmdOk.Enabled = True
    
End Sub


Private Sub Timer1_Timer()
    On Error GoTo Error
    Timer1.Enabled = False
    
    Dim ErrMsg As String
    
    DoEvents
    
    'Run the sproc to get the Cost Rollup
    Call frmAsphaltCost.RunCalculations(ErrMsg)
    
    txtMsg.Text = ErrMsg
    
    cmdOk.Enabled = True

    
    Exit Sub
Error:
    MsgBox Me.Name & ".Timer1_Timer)()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "SAGE 500"
    
    Err.Clear
    cmdOk.Enabled = True

End Sub
