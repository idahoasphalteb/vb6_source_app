Attribute VB_Name = "basMFBSReport"
Option Explicit


Const VBRIG_MODULE_ID_STRING = "mfzxxBS_Rpt.BAS"

Public gbSkipPrintDialog As Boolean     '   Show print dialog


Public Enum BatchSheetRpt
    eProdByPlant = 0
    eProdDtlByPlant = 1
    eProdAvgByItemByPlant = 2
    eOutOfRangePH = 3
    eOutOfRangeTemps = 4
    eOutOfRangeResidue = 5
End Enum


Public Sub Main()
    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("mfzxxBS_Rpt.clsSOGPPReport")
        StartAppInStandaloneMode oApp, Command$()    ' ** The third parameter MUST be the
                                                     '    TaskID for this project in order for the
                                                     '    Standalone method to be activated for ActiveX Exe's
    End If
End Sub

Function PrepSelect(SessionID As String) As String
    Dim sSelect As String
    
    sSelect = _
    "Select " & SessionID & "'SessionID', " & _
    "vlu_BatchSheet_Place_Holder_RKL.CompanyID, vlu_BatchSheet_Place_Holder_RKL.CompanyName, vlu_BatchSheet_Place_Holder_RKL.GUIBatchSheetHeaderKey, vlu_BatchSheet_Place_Holder_RKL.BatchSheetID, vlu_BatchSheet_Place_Holder_RKL.DailyBatchNumber, vlu_BatchSheet_Place_Holder_RKL.ProductionDate, vlu_BatchSheet_Place_Holder_RKL.WhseKey, vlu_BatchSheet_Place_Holder_RKL.WhseID, vlu_BatchSheet_Place_Holder_RKL.WhseDesc, vlu_BatchSheet_Place_Holder_RKL.RoutingKey, vlu_BatchSheet_Place_Holder_RKL.RoutingId, vlu_BatchSheet_Place_Holder_RKL.RoutingVersionID, vlu_BatchSheet_Place_Holder_RKL.EmulsionItemKey, vlu_BatchSheet_Place_Holder_RKL.EmulsionItem, vlu_BatchSheet_Place_Holder_RKL.EmulsionItemID, vlu_BatchSheet_Place_Holder_RKL.EmulsionItemDesc, vlu_BatchSheet_Place_Holder_RKL.PlannedTons " & _
    "From vlu_BatchSheet_Place_Holder_RKL "
    

    PrepSelect = sSelect
End Function

Function PrepProdByPlant(SessionID As String) As String
    Dim sSelect As String
    
    sSelect = _
    "Select " & SessionID & "'SessionID', " & _
    "vluMF_BatchSheet_DataEntry_Rpting_RKL.CompanyID, vluMF_BatchSheet_DataEntry_Rpting_RKL.CompanyName, vluMF_BatchSheet_DataEntry_Rpting_RKL.GUIBatchSheetHeaderKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.BatchSheetID, vluMF_BatchSheet_DataEntry_Rpting_RKL.DailyBatchNumber, vluMF_BatchSheet_DataEntry_Rpting_RKL.ProductionDate, vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseID, vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseDesc, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingId, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingVersionID, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItem, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemID, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemDesc, vluMF_BatchSheet_DataEntry_Rpting_RKL.PlannedTons " & _
    "From vluMF_BatchSheet_DataEntry_Rpting_RKL "
    

    PrepProdByPlant = sSelect
End Function

Function PrepProdDtlByPlant(SessionID As String) As String
    Dim sSelect As String
    
    sSelect = "Select " & SessionID & "'SessionID' "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.CompanyID, vluMF_BatchSheet_DataEntry_Rpting_RKL.CompanyName "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.BatchSheetID, vluMF_BatchSheet_DataEntry_Rpting_RKL.DailyBatchNumber "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.ProductionDate "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseID, vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseDesc "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingId, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingVersionID "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItem, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemID, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemDesc "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.PlannedTons "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.PlannedCalcGallons01 "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.StepID "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.FormulaItemNbr "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.BOMComponentItemKey, vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.FormulaItem, vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.BOMComponentItemID, vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.BOMComponentItemShortDesc "
    
    sSelect = sSelect & ", (vluMF_BatchSheet_DataEntry_Rpting_RKL.PlannedTons * vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.PercentOfTotalBatch * 2000) 'QtyInLbs' "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.FormulaNetGallons 'QtyInGallons' "
    sSelect = sSelect & ", (vluMF_BatchSheet_DataEntry_Rpting_RKL.PlannedTons * vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.PercentOfTotalBatch) 'QtyInTons' "
    
    
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.PercentOfTotalBatch * 100 'PercentOfTotalBatch' "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.FormulaBatchTotalsByUOM 'QtyInRoutingUoM' "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.FormulaUOM 'RoutingUoM' "

    sSelect = sSelect & ", vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.FormulaNetGallons, vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.RoutDtlType "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.BOMItemClassKey, vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.BOMItemClassID, vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.BOMItemClassName "
    
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.GUIBatchSheetHeaderKey "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.GUIBatchSheetDetailKey "
    
    sSelect = sSelect & "From vluMF_BatchSheet_DataEntry_Rpting_RKL with(ReadUncommitted) "
    sSelect = sSelect & "Inner Join vluMF_BatchSheet_DataEntryDtl_Rpting_RKL with(REadUncommitted) "
    sSelect = sSelect & "    on vluMF_BatchSheet_DataEntry_Rpting_RKL.GUIBatchSheetHeaderKey = vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.GUIBatchSheetHeaderKey "
    

    PrepProdDtlByPlant = sSelect
End Function

Function PrepProdAvgByPlant(SessionID As String) As String
    Dim sSelect As String
    
    sSelect = "Select " & SessionID & "'SessionID' "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.CompanyID, vluMF_BatchSheet_DataEntry_Rpting_RKL.CompanyName "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseID, vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseDesc "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingId, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingVersionID "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItem, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemID, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemDesc "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.BOMComponentItemKey, vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.FormulaItem, vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.BOMComponentItemID, vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.BOMComponentItemShortDesc "
    sSelect = sSelect & ", Min(vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.PercentOfTotalBatch) * 100 'Min_PercentOfTotalBatch' "
    sSelect = sSelect & ", Max(vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.PercentOfTotalBatch) * 100 'Max_PercentOfTotalBatch' "
    sSelect = sSelect & ", Avg(vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.PercentOfTotalBatch) * 100 'Avg_PercentOfTotalBatch' "
    sSelect = sSelect & ", Count(vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemKey) 'Count_EmulsionItem' "
    sSelect = sSelect & ", Count(vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.BOMComponentItemKey) 'Count_ComponentItem' "
    sSelect = sSelect & "From vluMF_BatchSheet_DataEntry_Rpting_RKL with(ReadUncommitted) "
    sSelect = sSelect & "Inner Join vluMF_BatchSheet_DataEntryDtl_Rpting_RKL with(REadUncommitted) "
    sSelect = sSelect & "    on vluMF_BatchSheet_DataEntry_Rpting_RKL.GUIBatchSheetHeaderKey = vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.GUIBatchSheetHeaderKey "


    PrepProdAvgByPlant = sSelect
End Function

Function PrepOutOfRngPH(SessionID As String) As String
    Dim sSelect As String
    
    sSelect = "Select " & SessionID & "'SessionID' "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.CompanyID, vluMF_BatchSheet_DataEntry_Rpting_RKL.CompanyName "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.BatchSheetID, vluMF_BatchSheet_DataEntry_Rpting_RKL.DailyBatchNumber "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.ProductionDate "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseID, vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseDesc "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingId, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingVersionID "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItem, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemID, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemDesc "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.PlannedTons, vluMF_BatchSheet_DataEntry_Rpting_RKL.GUIBatchSheetHeaderKey "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.SolutionFinalpHReading, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutxSolutionPHMin, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutxSolutionPHMax, vluMF_BatchSheet_DataEntry_Rpting_RKL.SolutionFinalpHReadingIsInRange "
    sSelect = sSelect & "From vluMF_BatchSheet_DataEntry_Rpting_RKL with(ReadUncommitted) "
    

    PrepOutOfRngPH = sSelect
End Function

Function PrepOutOfRngTemps(SessionID As String) As String
    Dim sSelect As String
    
    sSelect = "Select " & SessionID & "'SessionID' "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.CompanyID, vluMF_BatchSheet_DataEntry_Rpting_RKL.CompanyName "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.BatchSheetID, vluMF_BatchSheet_DataEntry_Rpting_RKL.DailyBatchNumber "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.ProductionDate "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseID, vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseDesc "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingId, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingVersionID "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItem, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemID, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemDesc "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.PlannedTons, vluMF_BatchSheet_DataEntry_Rpting_RKL.GUIBatchSheetHeaderKey "

    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.BaseTargetTemperature, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionActualReadingsACTemp, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutxBaseAsphaltTempMin, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutxBaseAsphaltTempMax, vluMF_BatchSheet_DataEntry_Rpting_RKL.AsphaltTempRange "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.BaseTargetTemperatureIsInRange, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionActualReadingsACTempIsInRange "

    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.SolutionTemperature, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionActualReadingsSolutionTemp, vluMF_BatchSheet_DataEntry_Rpting_RKL.TargetSolutionTempMin, vluMF_BatchSheet_DataEntry_Rpting_RKL.TargetSolutionTempMax, SolutionTempRange "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.SolutionTemperatureIsInRange, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionActualReadingsSolutionTempIsInRange "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.SolutionTargetTemperature "

    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.EstMillTemperature, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionActualReadingsMillOutletTemp, vluMF_BatchSheet_DataEntry_Rpting_RKL.TargetMillTempMin, vluMF_BatchSheet_DataEntry_Rpting_RKL.TargetMillTempMax, vluMF_BatchSheet_DataEntry_Rpting_RKL.MillTempRange "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.EstMillTemperatureIsInRange, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionActualReadingsMillOutletTempIsInRange "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionActualReadingsFinalTemp "

    sSelect = sSelect & "From vluMF_BatchSheet_DataEntry_Rpting_RKL with(ReadUncommitted) "
    

    PrepOutOfRngTemps = sSelect
End Function

Function PrepOutOfRngRsdue(SessionID As String) As String
    Dim sSelect As String
    
    sSelect = "Select " & SessionID & "'SessionID' "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.CompanyID, vluMF_BatchSheet_DataEntry_Rpting_RKL.CompanyName "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.BatchSheetID, vluMF_BatchSheet_DataEntry_Rpting_RKL.DailyBatchNumber "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.ProductionDate "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseID, vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseDesc "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingId, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingVersionID "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItem, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemID, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemDesc "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.PlannedTons, vluMF_BatchSheet_DataEntry_Rpting_RKL.GUIBatchSheetHeaderKey "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionActualReadingsAvgResidue, EmulsionActualReadingsAvgResidueValue, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutxResidueMin, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutxResidueMax, vluMF_BatchSheet_DataEntry_Rpting_RKL.ResidueRange, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutxResidueAvg "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionActualReadingsAvgResidueIsInRange "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.EstResidue "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionActualReadingsAvgViscosity, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutxViscosityMin, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutxViscosityMax, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutxViscosityAvg "
    sSelect = sSelect & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionActualReadingsAvgViscosityIsInRange "
    sSelect = sSelect & "From vluMF_BatchSheet_DataEntry_Rpting_RKL with(ReadUncommitted) "

    

    PrepOutOfRngRsdue = sSelect
End Function

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basMFBSReport"
End Function

Public Function lStartReport(sButton As String, frm As Form, StartDate As Date, EndDate As Date, _
    bPeriodEnd As Boolean, sPerEndDate, iPerNo, sPerYear, Optional iFileType As Variant, Optional sFileName As Variant) As Long
    
    Dim sWhereClause As String
    Dim sTablesUsed As String
    Dim sSelect As String
    Dim sInsert As String
    Dim lRetval As Long
    Dim bValid As Boolean
    Dim iNumTablesUsed As Integer
    Dim RptFileName As String
    Dim lBadRow As Long
    Dim SelectObj As clsSelection
    Dim ReportObj As clsReportEngine
    Dim SortObj As clsSort
    Dim DBObj As Object
    Dim SettingsObj As clsSettings
    Dim InsColumns As String
    
    Dim sPermissions As String
    
    On Error GoTo badexit

    lStartReport = kFailure
    
    If Not bPeriodEnd Then ShowStatusBusy frm

    Set SelectObj = frm.moSelect
    Set ReportObj = frm.moReport
    Set SortObj = frm.moSort

    Set DBObj = frm.oClass.moAppDB
    Set SettingsObj = frm.moSettings
   
    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
    
    'Correct bad user input in the Selection grid.
    'lBadRow contains the failed row if the parameter passed is False
    lBadRow = SelectObj.lValidateGrid(True)
    
    'Create the WHERE clause based on user input in the Selection grid & Session ID.
    bValid = SelectObj.bGetWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True)

    
    SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.ProductionDate >= " & gsQuoted(StartDate)
    SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.ProductionDate <= " & gsQuoted(EndDate)
    
'    'Append criteria to restrict data returned to the current company.
'    'RKL DEJ 2018-08-24 Added If Statement
'    If frm.chFilterByCompany.Value = vbChecked Then
'        SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.CompanyID=" & gsQuoted(frm.msCompanyID)
'    End If
    
    
    'CUSTOMIZE:  Add join specifications to WHERE clause based on appearance of a given table
    'in the sTablesUsed string (built as a result of call to bGetWhereClause above).
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tciAddress", "tarCustomer.PrimaryAddrKey=tciAddress.AddrKey"
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustAddr", "tarCustomer.CustKey=tarCustAddr.CustKey"
    
'    'SGS DEJ Commented out because we do not need the sintax of 'Where'
'    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
'    If Not SelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
'        GoTo badexit
'    End If
    
    
    
    'Get User data/whse Permissions
    sPermissions = " vlu_BatchSheet_Place_Holder_RKL.WhseID in( " & _
    "    select distinct wp.WhseID " & _
    "    From tsmUserCompanyGrp g with(NoLock) " & _
    "    Inner Join tsmUserGroupWhseExt_SGS wp with(NoLock) " & _
    "        on g.UserGroupID = wp.UserGroupID " & _
    "    where g.UserID = " & gsQuoted(frm.msUserID) & " "
'    If frm.chFilterByCompany.Value = vbChecked Then
'        sPermissions = sPermissions & "    and g.CompanyID = " & gsQuoted(frm.msCompanyID) & " "
'    End If
    sPermissions = sPermissions & ") "

    SelectObj.AppendToWhereClause sWhereClause, sPermissions

    'CUSTOMIZE:  Build a string for selection of key field and session ID.
'    sSelect = "SELECT DISTINCT timItem., " & ReportObj.SessionID & " FROM " & sTablesUsed
'    sSelect = sSelect & " " & sWhereClause

    'RKL DEJ 2016-11-22 added Insert Columns
    'RKL DEJ 2018-08-24 Added JobState
    InsColumns = "( SessionID, CompanyID, CompanyName, GUIBatchSheetHeaderKey, BatchSheetID, DailyBatchNumber, ProductionDate, WhseKey, WhseID, WhseDesc, RoutingKey, RoutingId, RoutingVersionID, EmulsionItemKey, EmulsionItem, EmulsionItemID, EmulsionItemDesc, PlannedTons )"
    
Debug.Print sWhereClause
    #If RPTDEBUG Then
        sInsert = "INSERT INTO tmf_BatchSheet_ProductionSummary_Rpt_Wrk_RKL " & InsColumns & PrepSelect(ReportObj.SessionID) & " Where " & sWhereClause '& sPermissions
    #Else
        sInsert = "INSERT INTO #tmf_BatchSheet_ProductionSummary_Rpt_Wrk_RKL " & InsColumns & PrepSelect(ReportObj.SessionID) & " Where " & sWhereClause '& sPermissions
    #End If
Debug.Print sInsert
    
    On Error Resume Next
    
Debug.Print sInsert
    
    
    DBObj.ExecuteSQL sInsert
    
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
    gClearSotaErr
    On Error GoTo badexit
    
    'Check whether any records were inserted; if not, go to error handler.
    If IsMissing(iFileType) Then
        #If RPTDEBUG Then
            If Not SelectObj.bRecsToPrintExist(frm.sWorkTableCollection(1), "SessionID", ReportObj.SessionID, True) Then
        #Else
            If Not SelectObj.bRecsToPrintExist("#" & frm.sWorkTableCollection(1), "SessionID", ReportObj.SessionID, True) Then
        #End If
            GoTo badexit
        End If
    End If
   
'    'CUSTOMIZE:  Set input and output parameters as needed and execute appropriate
'    'custom stored procedure to populate remaining fields in primary work table and
'    'insert records into secondary work tables.
'    With DBObj
'        .SetInParam (ReportObj.SessionID)
'        .SetOutParam lRetval
'        On Error Resume Next
'        .ExecuteSP ("spSGSReportTest")
'        If Err.Number <> 0 Then
'            lRetval = Err.Number
'        Else
'            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
'            'errors after each attempt.  After storing all return values in local variables OR
'            'encountering an error, release parameters.
'            lRetval = DBObj.GetOutParam(2)
'        End If
'    End With
'
'    If lRetval <> 0 Then
'        ReportObj.ReportError kmsgProc, ""
'        DBObj.ReleaseParams
'        GoTo badexit
'    End If
        
    DBObj.ReleaseParams
    gClearSotaErr
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)
    ReportObj.ReportFileName() = "mf_BatchSheet_ProductionByPlant.rpt"
    
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    ReportObj.Orientation() = vbPRORPortrait
    
    'CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    'to gsBuildString with a VB constant defined in LocalizationConst.bas. The subtitles should
    'not include the format selected by the user, i.e., "Detail" or "Summary".
    'ReportObj.ReportTitle2() = ""
    ReportObj.ReportTitle1 = "Batch Sheet Production By Plant"
    
    ReportObj.ReportTitle2 = "Production Start Date: " & StartDate & " End Date: " & EndDate
    
    ReportObj.MessageLines = "Line One " & vbCrLf & "LineTwo " & vbCrLf & "Line Three"

    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    'using the "lbl" convention on formula field names so that label text will handled automatically
    ReportObj.UseSubTotalCaptions() = 1
    ReportObj.UseHeaderCaptions() = 5
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If

    'Set sort order in .RPT file according to user selections in the Sort grid.
'    If (ReportObj.lSetSortCriteria(frm.moSort, 0) = kFailure) Then
'        GoTo badexit
'    End If
    
    '********* the following is specific to your report *************'
    Select Case RptFileName
        Case "mf_BatchSheet_ProductionByPlant.RPT"
        Case Else
    End Select
    '********* End of special processing *************'
    
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.SetSQL

    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    ReportObj.SetReportCaptions
    
    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
    ReportObj.SelectString = SelectObj.sGetUserReadableWhereClause(kLenPortrait)
    
    'If user has chosen to print report settings, set text for summary section.
    If (ReportObj.lSetSummarySection(frm.chkSummary.Value, frm.moOptions) = kFailure) Then
        GoTo badexit
    End If
    
    'CUSTOMIZE:  If using work tables, restrict report data to current Session ID.  If using
    'real tables, might restrict report data to current company or other criteria.
'    If (ReportObj.lRestrictBy("{.SessionID} = " & ReportObj.SessionID) = kFailure) Then
'SGS DEJ change to map to the actual table.
    If (ReportObj.lRestrictBy("{" & frm.sWorkTableCollection(1) & ".SessionID} = " & ReportObj.SessionID) = kFailure) Then

    'frm.sWorkTableCollection(1)
        GoTo badexit
    End If
    
    If bPeriodEnd Then
        gbSkipPrintDialog = True    '   don't show print dialog
    End If
    
    ReportObj.ProcessReport frm, sButton, iFileType, sFileName
    gbSkipPrintDialog = True    '   if multiple rpts, show print dialog one time only.
            
            
    If Not bPeriodEnd Then ShowStatusNone frm
    
    lStartReport = kSuccess
    
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    
    Exit Function
    
badexit:
    ReportObj.CleanupWorkTables
    If Not bPeriodEnd Then ShowStatusNone frm
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
    Exit Function
End Function

Public Function ProdByPlant(sButton As String, frm As Form, StartDate As Date, EndDate As Date, _
    bPeriodEnd As Boolean, sPerEndDate, iPerNo, sPerYear, Optional iFileType As Variant, Optional sFileName As Variant) As Long

    Dim sWhereClause As String
    Dim sTablesUsed As String
    Dim sSelect As String
    Dim sInsert As String
    Dim lRetval As Long
    Dim bValid As Boolean
    Dim iNumTablesUsed As Integer
    Dim RptFileName As String
    Dim lBadRow As Long
    Dim SelectObj As clsSelection
    Dim ReportObj As clsReportEngine
    Dim SortObj As clsSort
    Dim DBObj As Object
    Dim SettingsObj As clsSettings
    Dim InsColumns As String
    
    Dim sPermissions As String
    
    On Error GoTo badexit

    ProdByPlant = kFailure
    
    If Not bPeriodEnd Then ShowStatusBusy frm

    Set SelectObj = frm.moSelect
    Set ReportObj = frm.moReport
    Set SortObj = frm.moSort

    Set DBObj = frm.oClass.moAppDB
    Set SettingsObj = frm.moSettings
   
    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
    
    'Correct bad user input in the Selection grid.
    'lBadRow contains the failed row if the parameter passed is False
    lBadRow = SelectObj.lValidateGrid(True)
    
    'Create the WHERE clause based on user input in the Selection grid & Session ID.
    bValid = SelectObj.bGetWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True)

    
    SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.ProductionDate >= " & gsQuoted(StartDate)
    SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.ProductionDate <= " & gsQuoted(EndDate)
    
'    'Append criteria to restrict data returned to the current company.
'    'RKL DEJ 2018-08-24 Added If Statement
'    If frm.chFilterByCompany.Value = vbChecked Then
'        SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.CompanyID=" & gsQuoted(frm.msCompanyID)
'    End If
    
    
    'CUSTOMIZE:  Add join specifications to WHERE clause based on appearance of a given table
    'in the sTablesUsed string (built as a result of call to bGetWhereClause above).
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tciAddress", "tarCustomer.PrimaryAddrKey=tciAddress.AddrKey"
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustAddr", "tarCustomer.CustKey=tarCustAddr.CustKey"
    
'    'SGS DEJ Commented out because we do not need the sintax of 'Where'
'    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
'    If Not SelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
'        GoTo badexit
'    End If
    
    
    
    'Get User data/whse Permissions
    sPermissions = " vlu_BatchSheet_Place_Holder_RKL.WhseID in( " & _
    "    select distinct wp.WhseID " & _
    "    From tsmUserCompanyGrp g with(NoLock) " & _
    "    Inner Join tsmUserGroupWhseExt_SGS wp with(NoLock) " & _
    "        on g.UserGroupID = wp.UserGroupID " & _
    "    where g.UserID = " & gsQuoted(frm.msUserID) & " "
'    If frm.chFilterByCompany.Value = vbChecked Then
'        sPermissions = sPermissions & "    and g.CompanyID = " & gsQuoted(frm.msCompanyID) & " "
'    End If
    sPermissions = sPermissions & ") "


    SelectObj.AppendToWhereClause sWhereClause, sPermissions

    'CUSTOMIZE:  Build a string for selection of key field and session ID.
'    sSelect = "SELECT DISTINCT timItem., " & ReportObj.SessionID & " FROM " & sTablesUsed
'    sSelect = sSelect & " " & sWhereClause

    'RKL DEJ 2016-11-22 added Insert Columns
    'RKL DEJ 2018-08-24 Added JobState
    InsColumns = "( SessionID, CompanyID, CompanyName, GUIBatchSheetHeaderKey, BatchSheetID, DailyBatchNumber, ProductionDate, WhseKey, WhseID, WhseDesc, RoutingKey, RoutingId, RoutingVersionID, EmulsionItemKey, EmulsionItem, EmulsionItemID, EmulsionItemDesc, PlannedTons )"
    
Debug.Print sWhereClause
    #If RPTDEBUG Then
        sInsert = "INSERT INTO tmf_BatchSheet_ProductionSummary_Rpt_Wrk_RKL " & InsColumns & PrepProdByPlant(ReportObj.SessionID) & " Where " & sWhereClause '& sPermissions
    #Else
        sInsert = "INSERT INTO #tmf_BatchSheet_ProductionSummary_Rpt_Wrk_RKL " & InsColumns & PrepProdByPlant(ReportObj.SessionID) & " Where " & sWhereClause '& sPermissions
    #End If
Debug.Print sInsert
    
    On Error Resume Next
    
    'apply the correct View
    sInsert = Replace(UCase(sInsert), UCase("vlu_BatchSheet_Place_Holder_RKL"), UCase("vluMF_BatchSheet_DataEntry_Rpting_RKL"))
    
Debug.Print sInsert
    
    
    DBObj.ExecuteSQL sInsert
    
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
    gClearSotaErr
    On Error GoTo badexit
    
    'Check whether any records were inserted; if not, go to error handler.
    If IsMissing(iFileType) Then
        #If RPTDEBUG Then
            If Not SelectObj.bRecsToPrintExist(frm.sWorkTableCollection(1), "SessionID", ReportObj.SessionID, True) Then
        #Else
            If Not SelectObj.bRecsToPrintExist("#" & frm.sWorkTableCollection(1), "SessionID", ReportObj.SessionID, True) Then
        #End If
            GoTo badexit
        End If
    End If
   
'    'CUSTOMIZE:  Set input and output parameters as needed and execute appropriate
'    'custom stored procedure to populate remaining fields in primary work table and
'    'insert records into secondary work tables.
'    With DBObj
'        .SetInParam (ReportObj.SessionID)
'        .SetOutParam lRetval
'        On Error Resume Next
'        .ExecuteSP ("spSGSReportTest")
'        If Err.Number <> 0 Then
'            lRetval = Err.Number
'        Else
'            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
'            'errors after each attempt.  After storing all return values in local variables OR
'            'encountering an error, release parameters.
'            lRetval = DBObj.GetOutParam(2)
'        End If
'    End With
'
'    If lRetval <> 0 Then
'        ReportObj.ReportError kmsgProc, ""
'        DBObj.ReleaseParams
'        GoTo badexit
'    End If
        
    DBObj.ReleaseParams
    gClearSotaErr
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)
    ReportObj.ReportFileName() = "mf_BatchSheet_ProductionByPlant.rpt"
    
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    ReportObj.Orientation() = vbPRORPortrait
    
    'CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    'to gsBuildString with a VB constant defined in LocalizationConst.bas. The subtitles should
    'not include the format selected by the user, i.e., "Detail" or "Summary".
    'ReportObj.ReportTitle2() = ""
    ReportObj.ReportTitle1 = "Batch Sheet Production By Plant"
    
    ReportObj.ReportTitle2 = "Production Start Date: " & StartDate & " End Date: " & EndDate
    
    ReportObj.MessageLines = "Line One " & vbCrLf & "LineTwo " & vbCrLf & "Line Three"

    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    'using the "lbl" convention on formula field names so that label text will handled automatically
    ReportObj.UseSubTotalCaptions() = 1
    ReportObj.UseHeaderCaptions() = 5
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If

    'Set sort order in .RPT file according to user selections in the Sort grid.
'    If (ReportObj.lSetSortCriteria(frm.moSort, 0) = kFailure) Then
'        GoTo badexit
'    End If
    
    '********* the following is specific to your report *************'
    Select Case RptFileName
        Case "mf_BatchSheet_ProductionByPlant.RPT"
        Case Else
    End Select
    '********* End of special processing *************'
    
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.SetSQL

    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    ReportObj.SetReportCaptions
    
    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
    ReportObj.SelectString = SelectObj.sGetUserReadableWhereClause(kLenPortrait)
    
    'If user has chosen to print report settings, set text for summary section.
    If (ReportObj.lSetSummarySection(frm.chkSummary.Value, frm.moOptions) = kFailure) Then
        GoTo badexit
    End If
    
    'CUSTOMIZE:  If using work tables, restrict report data to current Session ID.  If using
    'real tables, might restrict report data to current company or other criteria.
'    If (ReportObj.lRestrictBy("{.SessionID} = " & ReportObj.SessionID) = kFailure) Then
'SGS DEJ change to map to the actual table.
    If (ReportObj.lRestrictBy("{" & frm.sWorkTableCollection(1) & ".SessionID} = " & ReportObj.SessionID) = kFailure) Then

    'frm.sWorkTableCollection(1)
        GoTo badexit
    End If
    
    If bPeriodEnd Then
        gbSkipPrintDialog = True    '   don't show print dialog
    End If
    
    ReportObj.ProcessReport frm, sButton, iFileType, sFileName
    gbSkipPrintDialog = True    '   if multiple rpts, show print dialog one time only.
            
            
    If Not bPeriodEnd Then ShowStatusNone frm
    
    ProdByPlant = kSuccess
    
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    
    Exit Function
    
badexit:
    ReportObj.CleanupWorkTables
    If Not bPeriodEnd Then ShowStatusNone frm
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
    Exit Function
End Function


Public Function ProdDtlByPlant(sButton As String, frm As Form, StartDate As Date, EndDate As Date, _
    bPeriodEnd As Boolean, sPerEndDate, iPerNo, sPerYear, Optional iFileType As Variant, Optional sFileName As Variant) As Long
    
    Dim sWhereClause As String
    Dim sTablesUsed As String
    Dim sSelect As String
    Dim sInsert As String
    Dim lRetval As Long
    Dim bValid As Boolean
    Dim iNumTablesUsed As Integer
    Dim RptFileName As String
    Dim lBadRow As Long
    Dim SelectObj As clsSelection
    Dim ReportObj As clsReportEngine
    Dim SortObj As clsSort
    Dim DBObj As Object
    Dim SettingsObj As clsSettings
    Dim InsColumns As String
    
    Dim sPermissions As String
    
    On Error GoTo badexit

    ProdDtlByPlant = kFailure
    
    If Not bPeriodEnd Then ShowStatusBusy frm

    Set SelectObj = frm.moSelect
    Set ReportObj = frm.moReport
    Set SortObj = frm.moSort

    Set DBObj = frm.oClass.moAppDB
    Set SettingsObj = frm.moSettings
   
    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
    
    'Correct bad user input in the Selection grid.
    'lBadRow contains the failed row if the parameter passed is False
    lBadRow = SelectObj.lValidateGrid(True)
    
    'Create the WHERE clause based on user input in the Selection grid & Session ID.
    bValid = SelectObj.bGetWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True)

    
    SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.ProductionDate >= " & gsQuoted(StartDate)
    SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.ProductionDate <= " & gsQuoted(EndDate)
    
'    'Append criteria to restrict data returned to the current company.
'    'RKL DEJ 2018-08-24 Added If Statement
'    If frm.chFilterByCompany.Value = vbChecked Then
'        SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.CompanyID=" & gsQuoted(frm.msCompanyID)
'    End If
    
    
    'CUSTOMIZE:  Add join specifications to WHERE clause based on appearance of a given table
    'in the sTablesUsed string (built as a result of call to bGetWhereClause above).
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tciAddress", "tarCustomer.PrimaryAddrKey=tciAddress.AddrKey"
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustAddr", "tarCustomer.CustKey=tarCustAddr.CustKey"
    
'    'SGS DEJ Commented out because we do not need the sintax of 'Where'
'    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
'    If Not SelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
'        GoTo badexit
'    End If
    
    
    
    'Get User data/whse Permissions
    sPermissions = " vlu_BatchSheet_Place_Holder_RKL.WhseID in( " & _
    "    select distinct wp.WhseID " & _
    "    From tsmUserCompanyGrp g with(NoLock) " & _
    "    Inner Join tsmUserGroupWhseExt_SGS wp with(NoLock) " & _
    "        on g.UserGroupID = wp.UserGroupID " & _
    "    where g.UserID = " & gsQuoted(frm.msUserID) & " "
'    If frm.chFilterByCompany.Value = vbChecked Then
'        sPermissions = sPermissions & "    and g.CompanyID = " & gsQuoted(frm.msCompanyID) & " "
'    End If
    sPermissions = sPermissions & ") "


    SelectObj.AppendToWhereClause sWhereClause, sPermissions

    'CUSTOMIZE:  Build a string for selection of key field and session ID.
'    sSelect = "SELECT DISTINCT timItem., " & ReportObj.SessionID & " FROM " & sTablesUsed
'    sSelect = sSelect & " " & sWhereClause

    'RKL DEJ 2016-11-22 added Insert Columns
    'RKL DEJ 2018-08-24 Added JobState
    InsColumns = "( SessionID, CompanyID, CompanyName, BatchSheetID, DailyBatchNumber, ProductionDate, WhseKey, WhseID, WhseDesc, RoutingKey, RoutingId, RoutingVersionID, EmulsionItemKey, EmulsionItem, EmulsionItemID, EmulsionItemDesc, PlannedTons, BatchSizeGallons, StepID, FormulaItemNbr, BOMComponentItemKey, FormulaItem, BOMComponentItemID, BOMComponentItemShortDesc, QtyInLbs, QtyInGallons, QtyInTons, PercentOfTotalBatch, QtyInRoutingUoM, RoutingUoM, FormulaNetGallons, RoutDtlType, BOMItemClassKey, BOMItemClassID, BOMItemClassName, GUIBatchSheetHeaderKey, GUIBatchSheetDetailKey )"
    
Debug.Print sWhereClause
    #If RPTDEBUG Then
        sInsert = "INSERT INTO tmf_BatchSheet_ProductionDetail_Rpt_Wrk_RKL " & InsColumns & PrepProdDtlByPlant(ReportObj.SessionID) & " Where " & sWhereClause '& sPermissions
    #Else
        sInsert = "INSERT INTO #tmf_BatchSheet_ProductionDetail_Rpt_Wrk_RKL " & InsColumns & PrepProdDtlByPlant(ReportObj.SessionID) & " Where " & sWhereClause '& sPermissions
    #End If
Debug.Print sInsert
    
    On Error Resume Next
    
    'apply the correct View
    sInsert = Replace(UCase(sInsert), UCase("vlu_BatchSheet_Place_Holder_RKL"), UCase("vluMF_BatchSheet_DataEntry_Rpting_RKL"))
    
Debug.Print sInsert
    
    DBObj.ExecuteSQL sInsert
    
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
    gClearSotaErr
    On Error GoTo badexit
    
    'Check whether any records were inserted; if not, go to error handler.
    If IsMissing(iFileType) Then
        #If RPTDEBUG Then
            If Not SelectObj.bRecsToPrintExist("tmf_BatchSheet_ProductionDetail_Rpt_Wrk_RKL", "SessionID", ReportObj.SessionID, True) Then
        #Else
            If Not SelectObj.bRecsToPrintExist("#tmf_BatchSheet_ProductionDetail_Rpt_Wrk_RKL", "SessionID", ReportObj.SessionID, True) Then
        #End If
            GoTo badexit
        End If
    End If
   
'    'CUSTOMIZE:  Set input and output parameters as needed and execute appropriate
'    'custom stored procedure to populate remaining fields in primary work table and
'    'insert records into secondary work tables.
'    With DBObj
'        .SetInParam (ReportObj.SessionID)
'        .SetOutParam lRetval
'        On Error Resume Next
'        .ExecuteSP ("spSGSReportTest")
'        If Err.Number <> 0 Then
'            lRetval = Err.Number
'        Else
'            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
'            'errors after each attempt.  After storing all return values in local variables OR
'            'encountering an error, release parameters.
'            lRetval = DBObj.GetOutParam(2)
'        End If
'    End With
'
'    If lRetval <> 0 Then
'        ReportObj.ReportError kmsgProc, ""
'        DBObj.ReleaseParams
'        GoTo badexit
'    End If
        
    DBObj.ReleaseParams
    gClearSotaErr
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)
    ReportObj.ReportFileName() = "mf_BatchSheet_ProductionDetailByPlant.rpt"
    
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    ReportObj.Orientation() = vbPRORPortrait
    
    'CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    'to gsBuildString with a VB constant defined in LocalizationConst.bas. The subtitles should
    'not include the format selected by the user, i.e., "Detail" or "Summary".
    'ReportObj.ReportTitle2() = ""
    ReportObj.ReportTitle1 = "Batch Sheet Production Detail By Plant"
    
    ReportObj.ReportTitle2 = "Production Start Date: " & StartDate & " End Date: " & EndDate
    
    ReportObj.MessageLines = "Line One " & vbCrLf & "LineTwo " & vbCrLf & "Line Three"

    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    'using the "lbl" convention on formula field names so that label text will handled automatically
    ReportObj.UseSubTotalCaptions() = 1
    ReportObj.UseHeaderCaptions() = 5
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If

    'Set sort order in .RPT file according to user selections in the Sort grid.
'    If (ReportObj.lSetSortCriteria(frm.moSort, 0) = kFailure) Then
'        GoTo badexit
'    End If
    
    '********* the following is specific to your report *************'
    Select Case RptFileName
        Case "mf_BatchSheet_ProductionDetailByPlant.RPT"
        Case Else
    End Select
    '********* End of special processing *************'
    
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.SetSQL

    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    ReportObj.SetReportCaptions
    
    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
    ReportObj.SelectString = SelectObj.sGetUserReadableWhereClause(kLenPortrait)
    
    'If user has chosen to print report settings, set text for summary section.
    If (ReportObj.lSetSummarySection(frm.chkSummary.Value, frm.moOptions) = kFailure) Then
        GoTo badexit
    End If
    
    'CUSTOMIZE:  If using work tables, restrict report data to current Session ID.  If using
    'real tables, might restrict report data to current company or other criteria.
'    If (ReportObj.lRestrictBy("{.SessionID} = " & ReportObj.SessionID) = kFailure) Then
'SGS DEJ change to map to the actual table.
    If (ReportObj.lRestrictBy("{" & "tmf_BatchSheet_ProductionDetail_Rpt_Wrk_RKL" & ".SessionID} = " & ReportObj.SessionID) = kFailure) Then

    'frm.sWorkTableCollection(1)
        GoTo badexit
    End If
    
    If bPeriodEnd Then
        gbSkipPrintDialog = True    '   don't show print dialog
    End If
    
    ReportObj.ProcessReport frm, sButton, iFileType, sFileName
    gbSkipPrintDialog = True    '   if multiple rpts, show print dialog one time only.
            
            
    If Not bPeriodEnd Then ShowStatusNone frm
    
    ProdDtlByPlant = kSuccess
    
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    
    Exit Function
    
badexit:
    ReportObj.CleanupWorkTables
    If Not bPeriodEnd Then ShowStatusNone frm
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
    Exit Function
End Function


Public Function ProdAvgByItmByPlant(sButton As String, frm As Form, StartDate As Date, EndDate As Date, _
    bPeriodEnd As Boolean, sPerEndDate, iPerNo, sPerYear, Optional iFileType As Variant, Optional sFileName As Variant) As Long
    
    Dim sWhereClause As String
    Dim sTablesUsed As String
    Dim sSelect As String
    Dim sGroupBy As String
    Dim sInsert As String
    Dim lRetval As Long
    Dim bValid As Boolean
    Dim iNumTablesUsed As Integer
    Dim RptFileName As String
    Dim lBadRow As Long
    Dim SelectObj As clsSelection
    Dim ReportObj As clsReportEngine
    Dim SortObj As clsSort
    Dim DBObj As Object
    Dim SettingsObj As clsSettings
    Dim InsColumns As String
    
    Dim sPermissions As String
    
    On Error GoTo badexit

    ProdAvgByItmByPlant = kFailure
    
    If Not bPeriodEnd Then ShowStatusBusy frm

    Set SelectObj = frm.moSelect
    Set ReportObj = frm.moReport
    Set SortObj = frm.moSort

    Set DBObj = frm.oClass.moAppDB
    Set SettingsObj = frm.moSettings
   
    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
    
    'Correct bad user input in the Selection grid.
    'lBadRow contains the failed row if the parameter passed is False
    lBadRow = SelectObj.lValidateGrid(True)
    
    'Create the WHERE clause based on user input in the Selection grid & Session ID.
    bValid = SelectObj.bGetWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True)

    
    SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.ProductionDate >= " & gsQuoted(StartDate)
    SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.ProductionDate <= " & gsQuoted(EndDate)
    
'    'Append criteria to restrict data returned to the current company.
'    'RKL DEJ 2018-08-24 Added If Statement
'    If frm.chFilterByCompany.Value = vbChecked Then
'        SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.CompanyID=" & gsQuoted(frm.msCompanyID)
'    End If
    
    
    'CUSTOMIZE:  Add join specifications to WHERE clause based on appearance of a given table
    'in the sTablesUsed string (built as a result of call to bGetWhereClause above).
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tciAddress", "tarCustomer.PrimaryAddrKey=tciAddress.AddrKey"
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustAddr", "tarCustomer.CustKey=tarCustAddr.CustKey"
    
'    'SGS DEJ Commented out because we do not need the sintax of 'Where'
'    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
'    If Not SelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
'        GoTo badexit
'    End If
    
    
    
    'Get User data/whse Permissions
    sPermissions = " vlu_BatchSheet_Place_Holder_RKL.WhseID in( " & _
    "    select distinct wp.WhseID " & _
    "    From tsmUserCompanyGrp g with(NoLock) " & _
    "    Inner Join tsmUserGroupWhseExt_SGS wp with(NoLock) " & _
    "        on g.UserGroupID = wp.UserGroupID " & _
    "    where g.UserID = " & gsQuoted(frm.msUserID) & " "
'    If frm.chFilterByCompany.Value = vbChecked Then
'        sPermissions = sPermissions & "    and g.CompanyID = " & gsQuoted(frm.msCompanyID) & " "
'    End If
    sPermissions = sPermissions & ") "


    SelectObj.AppendToWhereClause sWhereClause, sPermissions

    'CUSTOMIZE:  Build a string for selection of key field and session ID.
'    sSelect = "SELECT DISTINCT timItem., " & ReportObj.SessionID & " FROM " & sTablesUsed
'    sSelect = sSelect & " " & sWhereClause
    
    'Set Group By
    sGroupBy = " Group By vluMF_BatchSheet_DataEntry_Rpting_RKL.CompanyID, vluMF_BatchSheet_DataEntry_Rpting_RKL.CompanyName "
    sGroupBy = sGroupBy & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseID, vluMF_BatchSheet_DataEntry_Rpting_RKL.WhseDesc "
    sGroupBy = sGroupBy & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingId, vluMF_BatchSheet_DataEntry_Rpting_RKL.RoutingVersionID "
    sGroupBy = sGroupBy & ", vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemKey, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItem, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemID, vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionItemDesc "
    sGroupBy = sGroupBy & ", vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.BOMComponentItemKey, vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.FormulaItem, vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.BOMComponentItemID, vluMF_BatchSheet_DataEntryDtl_Rpting_RKL.BOMComponentItemShortDesc "

    
    'RKL DEJ 2016-11-22 added Insert Columns
    'RKL DEJ 2018-08-24 Added JobState
    InsColumns = "( SessionID, CompanyID, CompanyName, WhseKey, WhseID, WhseDesc, RoutingKey, RoutingId, RoutingVersionID, EmulsionItemKey, EmulsionItem, EmulsionItemID, EmulsionItemDesc, BOMComponentItemKey, FormulaItem, BOMComponentItemID, BOMComponentItemShortDesc, Min_PercentOfTotalBatch, Max_PercentOfTotalBatch, Avg_PercentOfTotalBatch, Count_EmulsionItem, Count_ComponentItem )"
    
Debug.Print sWhereClause
    #If RPTDEBUG Then
        sInsert = "INSERT INTO tmf_BatchSheet_ProductionAverage_Rpt_Wrk_RKL " & InsColumns & PrepProdAvgByPlant(ReportObj.SessionID) & " Where " & sWhereClause & sGroupBy
    #Else
        sInsert = "INSERT INTO #tmf_BatchSheet_ProductionAverage_Rpt_Wrk_RKL " & InsColumns & PrepProdAvgByPlant(ReportObj.SessionID) & " Where " & sWhereClause & sGroupBy
    #End If
Debug.Print sInsert
    
    On Error Resume Next
    
    sInsert = Replace(UCase(sInsert), UCase("vlu_BatchSheet_Place_Holder_RKL"), UCase("vluMF_BatchSheet_DataEntry_Rpting_RKL"))
    
Debug.Print sInsert
    
    
    DBObj.ExecuteSQL sInsert
    
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
    gClearSotaErr
    On Error GoTo badexit
    
    'Check whether any records were inserted; if not, go to error handler.
    If IsMissing(iFileType) Then
        #If RPTDEBUG Then
            If Not SelectObj.bRecsToPrintExist("tmf_BatchSheet_ProductionAverage_Rpt_Wrk_RKL", "SessionID", ReportObj.SessionID, True) Then
        #Else
            If Not SelectObj.bRecsToPrintExist("#" & "tmf_BatchSheet_ProductionAverage_Rpt_Wrk_RKL", "SessionID", ReportObj.SessionID, True) Then
        #End If
            GoTo badexit
        End If
    End If
   
'    'CUSTOMIZE:  Set input and output parameters as needed and execute appropriate
'    'custom stored procedure to populate remaining fields in primary work table and
'    'insert records into secondary work tables.
'    With DBObj
'        .SetInParam (ReportObj.SessionID)
'        .SetOutParam lRetval
'        On Error Resume Next
'        .ExecuteSP ("spSGSReportTest")
'        If Err.Number <> 0 Then
'            lRetval = Err.Number
'        Else
'            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
'            'errors after each attempt.  After storing all return values in local variables OR
'            'encountering an error, release parameters.
'            lRetval = DBObj.GetOutParam(2)
'        End If
'    End With
'
'    If lRetval <> 0 Then
'        ReportObj.ReportError kmsgProc, ""
'        DBObj.ReleaseParams
'        GoTo badexit
'    End If
        
    DBObj.ReleaseParams
    gClearSotaErr
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)
    ReportObj.ReportFileName() = "mf_BatchSheet_ProductionAverageByPlant.rpt"
    
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    ReportObj.Orientation() = vbPRORPortrait
    
    'CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    'to gsBuildString with a VB constant defined in LocalizationConst.bas. The subtitles should
    'not include the format selected by the user, i.e., "Detail" or "Summary".
    'ReportObj.ReportTitle2() = ""
    ReportObj.ReportTitle1 = "Batch Sheet Production Average By Product By Plant"
    
    ReportObj.ReportTitle2 = "Production Start Date: " & StartDate & " End Date: " & EndDate
    
    ReportObj.MessageLines = "Line One " & vbCrLf & "LineTwo " & vbCrLf & "Line Three"

    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    'using the "lbl" convention on formula field names so that label text will handled automatically
    ReportObj.UseSubTotalCaptions() = 1
    ReportObj.UseHeaderCaptions() = 5
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If

    'Set sort order in .RPT file according to user selections in the Sort grid.
'    If (ReportObj.lSetSortCriteria(frm.moSort, 0) = kFailure) Then
'        GoTo badexit
'    End If
    
    '********* the following is specific to your report *************'
    Select Case RptFileName
        Case "mf_BatchSheet_ProductionAverageByPlant.rpt"
        Case Else
    End Select
    '********* End of special processing *************'
    
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.SetSQL

    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    ReportObj.SetReportCaptions
    
    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
    ReportObj.SelectString = SelectObj.sGetUserReadableWhereClause(kLenPortrait)
    
    'If user has chosen to print report settings, set text for summary section.
    If (ReportObj.lSetSummarySection(frm.chkSummary.Value, frm.moOptions) = kFailure) Then
        GoTo badexit
    End If
    
    'CUSTOMIZE:  If using work tables, restrict report data to current Session ID.  If using
    'real tables, might restrict report data to current company or other criteria.
'    If (ReportObj.lRestrictBy("{.SessionID} = " & ReportObj.SessionID) = kFailure) Then
'SGS DEJ change to map to the actual table.
    If (ReportObj.lRestrictBy("{" & "tmf_BatchSheet_ProductionAverage_Rpt_Wrk_RKL" & ".SessionID} = " & ReportObj.SessionID) = kFailure) Then

    'frm.sWorkTableCollection(1)
        GoTo badexit
    End If
    
    If bPeriodEnd Then
        gbSkipPrintDialog = True    '   don't show print dialog
    End If
    
    ReportObj.ProcessReport frm, sButton, iFileType, sFileName
    gbSkipPrintDialog = True    '   if multiple rpts, show print dialog one time only.
            
            
    If Not bPeriodEnd Then ShowStatusNone frm
    
    ProdAvgByItmByPlant = kSuccess
    
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    
    Exit Function
    
badexit:
    ReportObj.CleanupWorkTables
    If Not bPeriodEnd Then ShowStatusNone frm
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
    Exit Function
End Function


Public Function OutOfRangePH(sButton As String, frm As Form, StartDate As Date, EndDate As Date, _
    bPeriodEnd As Boolean, sPerEndDate, iPerNo, sPerYear, Optional iFileType As Variant, Optional sFileName As Variant) As Long
    
    Dim sWhereClause As String
    Dim sTablesUsed As String
    Dim sSelect As String
    Dim sGroupBy As String
    Dim sInsert As String
    Dim lRetval As Long
    Dim bValid As Boolean
    Dim iNumTablesUsed As Integer
    Dim RptFileName As String
    Dim lBadRow As Long
    Dim SelectObj As clsSelection
    Dim ReportObj As clsReportEngine
    Dim SortObj As clsSort
    Dim DBObj As Object
    Dim SettingsObj As clsSettings
    Dim InsColumns As String
    
    Dim sPermissions As String
    
    On Error GoTo badexit

    OutOfRangePH = kFailure
    
    If Not bPeriodEnd Then ShowStatusBusy frm

    Set SelectObj = frm.moSelect
    Set ReportObj = frm.moReport
    Set SortObj = frm.moSort

    Set DBObj = frm.oClass.moAppDB
    Set SettingsObj = frm.moSettings
   
    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
    
    'Correct bad user input in the Selection grid.
    'lBadRow contains the failed row if the parameter passed is False
    lBadRow = SelectObj.lValidateGrid(True)
    
    'Create the WHERE clause based on user input in the Selection grid & Session ID.
    bValid = SelectObj.bGetWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True)

    
    SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.ProductionDate >= " & gsQuoted(StartDate)
    SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.ProductionDate <= " & gsQuoted(EndDate)
    
'    'Append criteria to restrict data returned to the current company.
'    'RKL DEJ 2018-08-24 Added If Statement
'    If frm.chFilterByCompany.Value = vbChecked Then
'        SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.CompanyID=" & gsQuoted(frm.msCompanyID)
'    End If
    
    
    'CUSTOMIZE:  Add join specifications to WHERE clause based on appearance of a given table
    'in the sTablesUsed string (built as a result of call to bGetWhereClause above).
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tciAddress", "tarCustomer.PrimaryAddrKey=tciAddress.AddrKey"
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustAddr", "tarCustomer.CustKey=tarCustAddr.CustKey"
    
'    'SGS DEJ Commented out because we do not need the sintax of 'Where'
'    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
'    If Not SelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
'        GoTo badexit
'    End If
    
    
    
    'Get User data/whse Permissions
    sPermissions = " vlu_BatchSheet_Place_Holder_RKL.WhseID in( " & _
    "    select distinct wp.WhseID " & _
    "    From tsmUserCompanyGrp g with(NoLock) " & _
    "    Inner Join tsmUserGroupWhseExt_SGS wp with(NoLock) " & _
    "        on g.UserGroupID = wp.UserGroupID " & _
    "    where g.UserID = " & gsQuoted(frm.msUserID) & " "
'    If frm.chFilterByCompany.Value = vbChecked Then
'        sPermissions = sPermissions & "    and g.CompanyID = " & gsQuoted(frm.msCompanyID) & " "
'    End If
    sPermissions = sPermissions & ") "


    SelectObj.AppendToWhereClause sWhereClause, sPermissions
    
    SelectObj.AppendToWhereClause sWhereClause, " vluMF_BatchSheet_DataEntry_Rpting_RKL.SolutionFinalpHReadingIsInRange = 0 "

    'CUSTOMIZE:  Build a string for selection of key field and session ID.
'    sSelect = "SELECT DISTINCT timItem., " & ReportObj.SessionID & " FROM " & sTablesUsed
'    sSelect = sSelect & " " & sWhereClause
    
    
    'RKL DEJ 2016-11-22 added Insert Columns
    'RKL DEJ 2018-08-24 Added JobState
    InsColumns = "( SessionID, CompanyID, CompanyName, BatchSheetID, DailyBatchNumber, ProductionDate, WhseKey, WhseID, WhseDesc, RoutingKey, RoutingId, RoutingVersionID, EmulsionItemKey, EmulsionItem, EmulsionItemID, EmulsionItemDesc, PlannedTons, GUIBatchSheetHeaderKey, SolutionFinalpHReading, RoutxSolutionPHMin, RoutxSolutionPHMax, SolutionFinalpHReadingIsInRange )"
    
Debug.Print sWhereClause
    #If RPTDEBUG Then
        sInsert = "INSERT INTO tmf_BatchSheet_OutOfRangePH_Rpt_Wrk_RKL " & InsColumns & PrepOutOfRngPH(ReportObj.SessionID) & " Where " & sWhereClause
    #Else
        sInsert = "INSERT INTO #tmf_BatchSheet_OutOfRangePH_Rpt_Wrk_RKL " & InsColumns & PrepOutOfRngPH(ReportObj.SessionID) & " Where " & sWhereClause
    #End If
Debug.Print sInsert
    
    On Error Resume Next
    
    sInsert = Replace(UCase(sInsert), UCase("vlu_BatchSheet_Place_Holder_RKL"), UCase("vluMF_BatchSheet_DataEntry_Rpting_RKL"))
    
Debug.Print sInsert
    
    
    DBObj.ExecuteSQL sInsert
    
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
    gClearSotaErr
    On Error GoTo badexit
    
    'Check whether any records were inserted; if not, go to error handler.
    If IsMissing(iFileType) Then
        #If RPTDEBUG Then
            If Not SelectObj.bRecsToPrintExist("tmf_BatchSheet_OutOfRangePH_Rpt_Wrk_RKL", "SessionID", ReportObj.SessionID, True) Then
        #Else
            If Not SelectObj.bRecsToPrintExist("#" & "tmf_BatchSheet_OutOfRangePH_Rpt_Wrk_RKL", "SessionID", ReportObj.SessionID, True) Then
        #End If
            GoTo badexit
        End If
    End If
   
'    'CUSTOMIZE:  Set input and output parameters as needed and execute appropriate
'    'custom stored procedure to populate remaining fields in primary work table and
'    'insert records into secondary work tables.
'    With DBObj
'        .SetInParam (ReportObj.SessionID)
'        .SetOutParam lRetval
'        On Error Resume Next
'        .ExecuteSP ("spSGSReportTest")
'        If Err.Number <> 0 Then
'            lRetval = Err.Number
'        Else
'            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
'            'errors after each attempt.  After storing all return values in local variables OR
'            'encountering an error, release parameters.
'            lRetval = DBObj.GetOutParam(2)
'        End If
'    End With
'
'    If lRetval <> 0 Then
'        ReportObj.ReportError kmsgProc, ""
'        DBObj.ReleaseParams
'        GoTo badexit
'    End If
        
    DBObj.ReleaseParams
    gClearSotaErr
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)
    ReportObj.ReportFileName() = "mf_BatchSheet_OutOfRangePH.rpt"
    
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    ReportObj.Orientation() = vbPRORPortrait
    
    'CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    'to gsBuildString with a VB constant defined in LocalizationConst.bas. The subtitles should
    'not include the format selected by the user, i.e., "Detail" or "Summary".
    'ReportObj.ReportTitle2() = ""
    ReportObj.ReportTitle1 = "Batch Sheet Out Of Range - PH"
    
    ReportObj.ReportTitle2 = "Production Start Date: " & StartDate & " End Date: " & EndDate
    
    ReportObj.MessageLines = "Line One " & vbCrLf & "LineTwo " & vbCrLf & "Line Three"

    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    'using the "lbl" convention on formula field names so that label text will handled automatically
    ReportObj.UseSubTotalCaptions() = 1
    ReportObj.UseHeaderCaptions() = 5
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If

    'Set sort order in .RPT file according to user selections in the Sort grid.
'    If (ReportObj.lSetSortCriteria(frm.moSort, 0) = kFailure) Then
'        GoTo badexit
'    End If
    
    '********* the following is specific to your report *************'
    Select Case RptFileName
        Case "mf_BatchSheet_OutOfRangePH.rpt"
        Case Else
    End Select
    '********* End of special processing *************'
    
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.SetSQL

    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    ReportObj.SetReportCaptions
    
    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
    ReportObj.SelectString = SelectObj.sGetUserReadableWhereClause(kLenPortrait)
    
    'If user has chosen to print report settings, set text for summary section.
    If (ReportObj.lSetSummarySection(frm.chkSummary.Value, frm.moOptions) = kFailure) Then
        GoTo badexit
    End If
    
    'CUSTOMIZE:  If using work tables, restrict report data to current Session ID.  If using
    'real tables, might restrict report data to current company or other criteria.
'    If (ReportObj.lRestrictBy("{.SessionID} = " & ReportObj.SessionID) = kFailure) Then
'SGS DEJ change to map to the actual table.
    If (ReportObj.lRestrictBy("{" & "tmf_BatchSheet_OutOfRangePH_Rpt_Wrk_RKL" & ".SessionID} = " & ReportObj.SessionID) = kFailure) Then

    'frm.sWorkTableCollection(1)
        GoTo badexit
    End If
    
    If bPeriodEnd Then
        gbSkipPrintDialog = True    '   don't show print dialog
    End If
    
    ReportObj.ProcessReport frm, sButton, iFileType, sFileName
    gbSkipPrintDialog = True    '   if multiple rpts, show print dialog one time only.
            
            
    If Not bPeriodEnd Then ShowStatusNone frm
    
    OutOfRangePH = kSuccess
    
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    
    Exit Function
    
badexit:
    ReportObj.CleanupWorkTables
    If Not bPeriodEnd Then ShowStatusNone frm
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
    Exit Function
End Function


Public Function OutOfRangeTemps(sButton As String, frm As Form, StartDate As Date, EndDate As Date, _
    bPeriodEnd As Boolean, sPerEndDate, iPerNo, sPerYear, Optional iFileType As Variant, Optional sFileName As Variant) As Long
    
    Dim sWhereClause As String
    Dim sTablesUsed As String
    Dim sSelect As String
    Dim sOutOfRangeFlter As String
    Dim sInsert As String
    Dim lRetval As Long
    Dim bValid As Boolean
    Dim iNumTablesUsed As Integer
    Dim RptFileName As String
    Dim lBadRow As Long
    Dim SelectObj As clsSelection
    Dim ReportObj As clsReportEngine
    Dim SortObj As clsSort
    Dim DBObj As Object
    Dim SettingsObj As clsSettings
    Dim InsColumns As String
    
    Dim sPermissions As String
    
    On Error GoTo badexit

    OutOfRangeTemps = kFailure
    
    If Not bPeriodEnd Then ShowStatusBusy frm

    Set SelectObj = frm.moSelect
    Set ReportObj = frm.moReport
    Set SortObj = frm.moSort

    Set DBObj = frm.oClass.moAppDB
    Set SettingsObj = frm.moSettings
   
    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
    
    'Correct bad user input in the Selection grid.
    'lBadRow contains the failed row if the parameter passed is False
    lBadRow = SelectObj.lValidateGrid(True)
    
    'Create the WHERE clause based on user input in the Selection grid & Session ID.
    bValid = SelectObj.bGetWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True)

    
    SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.ProductionDate >= " & gsQuoted(StartDate)
    SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.ProductionDate <= " & gsQuoted(EndDate)
    
'    'Append criteria to restrict data returned to the current company.
'    'RKL DEJ 2018-08-24 Added If Statement
'    If frm.chFilterByCompany.Value = vbChecked Then
'        SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.CompanyID=" & gsQuoted(frm.msCompanyID)
'    End If
    
    
    'CUSTOMIZE:  Add join specifications to WHERE clause based on appearance of a given table
    'in the sTablesUsed string (built as a result of call to bGetWhereClause above).
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tciAddress", "tarCustomer.PrimaryAddrKey=tciAddress.AddrKey"
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustAddr", "tarCustomer.CustKey=tarCustAddr.CustKey"
    
'    'SGS DEJ Commented out because we do not need the sintax of 'Where'
'    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
'    If Not SelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
'        GoTo badexit
'    End If
    
    
    
    'Get User data/whse Permissions
    sPermissions = " vlu_BatchSheet_Place_Holder_RKL.WhseID in( " & _
    "    select distinct wp.WhseID " & _
    "    From tsmUserCompanyGrp g with(NoLock) " & _
    "    Inner Join tsmUserGroupWhseExt_SGS wp with(NoLock) " & _
    "        on g.UserGroupID = wp.UserGroupID " & _
    "    where g.UserID = " & gsQuoted(frm.msUserID) & " "
'    If frm.chFilterByCompany.Value = vbChecked Then
'        sPermissions = sPermissions & "    and g.CompanyID = " & gsQuoted(frm.msCompanyID) & " "
'    End If
    sPermissions = sPermissions & ") "


    SelectObj.AppendToWhereClause sWhereClause, sPermissions
    
    sOutOfRangeFlter = " ("
    sOutOfRangeFlter = sOutOfRangeFlter & "vluMF_BatchSheet_DataEntry_Rpting_RKL.BaseTargetTemperatureIsInRange = 0  "
    sOutOfRangeFlter = sOutOfRangeFlter & "or vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionActualReadingsACTempIsInRange = 0 "
    sOutOfRangeFlter = sOutOfRangeFlter & "Or vluMF_BatchSheet_DataEntry_Rpting_RKL.SolutionTemperatureIsInRange = 0 "
    sOutOfRangeFlter = sOutOfRangeFlter & "or vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionActualReadingsSolutionTempIsInRange = 0 "
    sOutOfRangeFlter = sOutOfRangeFlter & "or vluMF_BatchSheet_DataEntry_Rpting_RKL.EstMillTemperatureIsInRange = 0 "
    sOutOfRangeFlter = sOutOfRangeFlter & "or vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionActualReadingsMillOutletTempIsInRange = 0 "
    sOutOfRangeFlter = sOutOfRangeFlter & ") "
    
    SelectObj.AppendToWhereClause sWhereClause, sOutOfRangeFlter

    'CUSTOMIZE:  Build a string for selection of key field and session ID.
'    sSelect = "SELECT DISTINCT timItem., " & ReportObj.SessionID & " FROM " & sTablesUsed
'    sSelect = sSelect & " " & sWhereClause
    
    
    'RKL DEJ 2016-11-22 added Insert Columns
    'RKL DEJ 2018-08-24 Added JobState
    InsColumns = "( SessionID, CompanyID, CompanyName, BatchSheetID, DailyBatchNumber, ProductionDate, WhseKey, WhseID, WhseDesc, RoutingKey, RoutingId, RoutingVersionID, EmulsionItemKey, EmulsionItem, EmulsionItemID, EmulsionItemDesc, PlannedTons, GUIBatchSheetHeaderKey, BaseTargetTemperature, EmulsionActualReadingsACTemp, RoutxBaseAsphaltTempMin, RoutxBaseAsphaltTempMax, AsphaltTempRange, BaseTargetTemperatureIsInRange, EmulsionActualReadingsACTempIsInRange, SolutionTemperature, EmulsionActualReadingsSolutionTemp, TargetSolutionTempMin, TargetSolutionTempMax, SolutionTempRange, SolutionTemperatureIsInRange, EmulsionActualReadingsSolutionTempIsInRange, SolutionTargetTemperature, EstMillTemperature, EmulsionActualReadingsMillOutletTemp, TargetMillTempMin, TargetMillTempMax, MillTempRange, EstMillTemperatureIsInRange, EmulsionActualReadingsMillOutletTempIsInRange, EmulsionActualReadingsFinalTemp )"
    
Debug.Print sWhereClause
    #If RPTDEBUG Then
        sInsert = "INSERT INTO tmf_BatchSheet_OutOfRangeTemps_Rpt_Wrk_RKL " & InsColumns & PrepOutOfRngTemps(ReportObj.SessionID) & " Where " & sWhereClause
    #Else
        sInsert = "INSERT INTO #tmf_BatchSheet_OutOfRangeTemps_Rpt_Wrk_RKL " & InsColumns & PrepOutOfRngTemps(ReportObj.SessionID) & " Where " & sWhereClause
    #End If
Debug.Print sInsert
    
    On Error Resume Next
    
    sInsert = Replace(UCase(sInsert), UCase("vlu_BatchSheet_Place_Holder_RKL"), UCase("vluMF_BatchSheet_DataEntry_Rpting_RKL"))
    
Debug.Print sInsert
    
    
    DBObj.ExecuteSQL sInsert
    
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
    gClearSotaErr
    On Error GoTo badexit
    
    'Check whether any records were inserted; if not, go to error handler.
    If IsMissing(iFileType) Then
        #If RPTDEBUG Then
            If Not SelectObj.bRecsToPrintExist("tmf_BatchSheet_OutOfRangeTemps_Rpt_Wrk_RKL", "SessionID", ReportObj.SessionID, True) Then
        #Else
            If Not SelectObj.bRecsToPrintExist("#" & "tmf_BatchSheet_OutOfRangeTemps_Rpt_Wrk_RKL", "SessionID", ReportObj.SessionID, True) Then
        #End If
            GoTo badexit
        End If
    End If
   
'    'CUSTOMIZE:  Set input and output parameters as needed and execute appropriate
'    'custom stored procedure to populate remaining fields in primary work table and
'    'insert records into secondary work tables.
'    With DBObj
'        .SetInParam (ReportObj.SessionID)
'        .SetOutParam lRetval
'        On Error Resume Next
'        .ExecuteSP ("spSGSReportTest")
'        If Err.Number <> 0 Then
'            lRetval = Err.Number
'        Else
'            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
'            'errors after each attempt.  After storing all return values in local variables OR
'            'encountering an error, release parameters.
'            lRetval = DBObj.GetOutParam(2)
'        End If
'    End With
'
'    If lRetval <> 0 Then
'        ReportObj.ReportError kmsgProc, ""
'        DBObj.ReleaseParams
'        GoTo badexit
'    End If
        
    DBObj.ReleaseParams
    gClearSotaErr
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)
    ReportObj.ReportFileName() = "mf_BatchSheet_OutOfRangeTemps.rpt"
    
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    ReportObj.Orientation() = vbPRORPortrait
    
    'CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    'to gsBuildString with a VB constant defined in LocalizationConst.bas. The subtitles should
    'not include the format selected by the user, i.e., "Detail" or "Summary".
    'ReportObj.ReportTitle2() = ""
    ReportObj.ReportTitle1 = "Batch Sheet Out Of Range - Temps"
    
    ReportObj.ReportTitle2 = "Production Start Date: " & StartDate & " End Date: " & EndDate
    
    ReportObj.MessageLines = "Line One " & vbCrLf & "LineTwo " & vbCrLf & "Line Three"

    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    'using the "lbl" convention on formula field names so that label text will handled automatically
    ReportObj.UseSubTotalCaptions() = 1
    ReportObj.UseHeaderCaptions() = 5
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If

    'Set sort order in .RPT file according to user selections in the Sort grid.
'    If (ReportObj.lSetSortCriteria(frm.moSort, 0) = kFailure) Then
'        GoTo badexit
'    End If
    
    '********* the following is specific to your report *************'
    Select Case RptFileName
        Case "mf_BatchSheet_OutOfRangeTemps.rpt"
        Case Else
    End Select
    '********* End of special processing *************'
    
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.SetSQL

    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    ReportObj.SetReportCaptions
    
    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
    ReportObj.SelectString = SelectObj.sGetUserReadableWhereClause(kLenPortrait)
    
    'If user has chosen to print report settings, set text for summary section.
    If (ReportObj.lSetSummarySection(frm.chkSummary.Value, frm.moOptions) = kFailure) Then
        GoTo badexit
    End If
    
    'CUSTOMIZE:  If using work tables, restrict report data to current Session ID.  If using
    'real tables, might restrict report data to current company or other criteria.
'    If (ReportObj.lRestrictBy("{.SessionID} = " & ReportObj.SessionID) = kFailure) Then
'SGS DEJ change to map to the actual table.
    If (ReportObj.lRestrictBy("{" & "tmf_BatchSheet_OutOfRangeTemps_Rpt_Wrk_RKL" & ".SessionID} = " & ReportObj.SessionID) = kFailure) Then

    'frm.sWorkTableCollection(1)
        GoTo badexit
    End If
    
    If bPeriodEnd Then
        gbSkipPrintDialog = True    '   don't show print dialog
    End If
    
    ReportObj.ProcessReport frm, sButton, iFileType, sFileName
    gbSkipPrintDialog = True    '   if multiple rpts, show print dialog one time only.
            
            
    If Not bPeriodEnd Then ShowStatusNone frm
    
    OutOfRangeTemps = kSuccess
    
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    
    Exit Function
    
badexit:
    ReportObj.CleanupWorkTables
    If Not bPeriodEnd Then ShowStatusNone frm
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
    Exit Function
End Function


Public Function OutOfRangeResidue(sButton As String, frm As Form, StartDate As Date, EndDate As Date, _
    bPeriodEnd As Boolean, sPerEndDate, iPerNo, sPerYear, Optional iFileType As Variant, Optional sFileName As Variant) As Long
    
    Dim sWhereClause As String
    Dim sTablesUsed As String
    Dim sSelect As String
    Dim sOutOfRangeFlter As String
    Dim sInsert As String
    Dim lRetval As Long
    Dim bValid As Boolean
    Dim iNumTablesUsed As Integer
    Dim RptFileName As String
    Dim lBadRow As Long
    Dim SelectObj As clsSelection
    Dim ReportObj As clsReportEngine
    Dim SortObj As clsSort
    Dim DBObj As Object
    Dim SettingsObj As clsSettings
    Dim InsColumns As String
    
    Dim sPermissions As String
    
    On Error GoTo badexit

    OutOfRangeResidue = kFailure
    
    If Not bPeriodEnd Then ShowStatusBusy frm

    Set SelectObj = frm.moSelect
    Set ReportObj = frm.moReport
    Set SortObj = frm.moSort

    Set DBObj = frm.oClass.moAppDB
    Set SettingsObj = frm.moSettings
   
    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
    
    'Correct bad user input in the Selection grid.
    'lBadRow contains the failed row if the parameter passed is False
    lBadRow = SelectObj.lValidateGrid(True)
    
    'Create the WHERE clause based on user input in the Selection grid & Session ID.
    bValid = SelectObj.bGetWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True)

    
    SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.ProductionDate >= " & gsQuoted(StartDate)
    SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.ProductionDate <= " & gsQuoted(EndDate)
    
'    'Append criteria to restrict data returned to the current company.
'    'RKL DEJ 2018-08-24 Added If Statement
'    If frm.chFilterByCompany.Value = vbChecked Then
'        SelectObj.AppendToWhereClause sWhereClause, "vlu_BatchSheet_Place_Holder_RKL.CompanyID=" & gsQuoted(frm.msCompanyID)
'    End If
    
    
    'CUSTOMIZE:  Add join specifications to WHERE clause based on appearance of a given table
    'in the sTablesUsed string (built as a result of call to bGetWhereClause above).
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tciAddress", "tarCustomer.PrimaryAddrKey=tciAddress.AddrKey"
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustAddr", "tarCustomer.CustKey=tarCustAddr.CustKey"
    
'    'SGS DEJ Commented out because we do not need the sintax of 'Where'
'    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
'    If Not SelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
'        GoTo badexit
'    End If
    
    
    
    'Get User data/whse Permissions
    sPermissions = " vlu_BatchSheet_Place_Holder_RKL.WhseID in( " & _
    "    select distinct wp.WhseID " & _
    "    From tsmUserCompanyGrp g with(NoLock) " & _
    "    Inner Join tsmUserGroupWhseExt_SGS wp with(NoLock) " & _
    "        on g.UserGroupID = wp.UserGroupID " & _
    "    where g.UserID = " & gsQuoted(frm.msUserID) & " "
'    If frm.chFilterByCompany.Value = vbChecked Then
'        sPermissions = sPermissions & "    and g.CompanyID = " & gsQuoted(frm.msCompanyID) & " "
'    End If
    sPermissions = sPermissions & ") "


    SelectObj.AppendToWhereClause sWhereClause, sPermissions
    
    sOutOfRangeFlter = " ("
    sOutOfRangeFlter = sOutOfRangeFlter & "vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionActualReadingsAvgResidueIsInRange = 0  "
    sOutOfRangeFlter = sOutOfRangeFlter & "or vluMF_BatchSheet_DataEntry_Rpting_RKL.EmulsionActualReadingsAvgViscosityIsInRange = 0 "
    sOutOfRangeFlter = sOutOfRangeFlter & ") "
    
    SelectObj.AppendToWhereClause sWhereClause, sOutOfRangeFlter


    'CUSTOMIZE:  Build a string for selection of key field and session ID.
'    sSelect = "SELECT DISTINCT timItem., " & ReportObj.SessionID & " FROM " & sTablesUsed
'    sSelect = sSelect & " " & sWhereClause
    
    
    'RKL DEJ 2016-11-22 added Insert Columns
    'RKL DEJ 2018-08-24 Added JobState
    InsColumns = "( SessionID, CompanyID, CompanyName, BatchSheetID, DailyBatchNumber, ProductionDate, WhseKey, WhseID, WhseDesc, RoutingKey, RoutingId, RoutingVersionID, EmulsionItemKey, EmulsionItem, EmulsionItemID, EmulsionItemDesc, PlannedTons, GUIBatchSheetHeaderKey, EmulsionActualReadingsAvgResidue, EmulsionActualReadingsAvgResidueValue, RoutxResidueMin, RoutxResidueMax, ResidueRange, RoutxResidueAvg, EmulsionActualReadingsAvgResidueIsInRange, EstResidue, EmulsionActualReadingsAvgViscosity, RoutxViscosityMin, RoutxViscosityMax, RoutxViscosityAvg, EmulsionActualReadingsAvgViscosityIsInRange )"
    
Debug.Print sWhereClause
    #If RPTDEBUG Then
        sInsert = "INSERT INTO tmf_BatchSheet_OutOfRangeResidue_Rpt_Wrk_RKL " & InsColumns & PrepOutOfRngRsdue(ReportObj.SessionID) & " Where " & sWhereClause
    #Else
        sInsert = "INSERT INTO #tmf_BatchSheet_OutOfRangeResidue_Rpt_Wrk_RKL " & InsColumns & PrepOutOfRngRsdue(ReportObj.SessionID) & " Where " & sWhereClause
    #End If
Debug.Print sInsert
    
    On Error Resume Next
    
    sInsert = Replace(UCase(sInsert), UCase("vlu_BatchSheet_Place_Holder_RKL"), UCase("vluMF_BatchSheet_DataEntry_Rpting_RKL"))
    
Debug.Print sInsert
    
    
    DBObj.ExecuteSQL sInsert
    
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
    gClearSotaErr
    On Error GoTo badexit
    
    'Check whether any records were inserted; if not, go to error handler.
    If IsMissing(iFileType) Then
        #If RPTDEBUG Then
            If Not SelectObj.bRecsToPrintExist("tmf_BatchSheet_OutOfRangeResidue_Rpt_Wrk_RKL", "SessionID", ReportObj.SessionID, True) Then
        #Else
            If Not SelectObj.bRecsToPrintExist("#" & "tmf_BatchSheet_OutOfRangeResidue_Rpt_Wrk_RKL", "SessionID", ReportObj.SessionID, True) Then
        #End If
            GoTo badexit
        End If
    End If
   
'    'CUSTOMIZE:  Set input and output parameters as needed and execute appropriate
'    'custom stored procedure to populate remaining fields in primary work table and
'    'insert records into secondary work tables.
'    With DBObj
'        .SetInParam (ReportObj.SessionID)
'        .SetOutParam lRetval
'        On Error Resume Next
'        .ExecuteSP ("spSGSReportTest")
'        If Err.Number <> 0 Then
'            lRetval = Err.Number
'        Else
'            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
'            'errors after each attempt.  After storing all return values in local variables OR
'            'encountering an error, release parameters.
'            lRetval = DBObj.GetOutParam(2)
'        End If
'    End With
'
'    If lRetval <> 0 Then
'        ReportObj.ReportError kmsgProc, ""
'        DBObj.ReleaseParams
'        GoTo badexit
'    End If
        
    DBObj.ReleaseParams
    gClearSotaErr
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)
    ReportObj.ReportFileName() = "mf_BatchSheet_OutOfRangeResidue.rpt"
    
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    ReportObj.Orientation() = vbPRORPortrait
    
    'CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    'to gsBuildString with a VB constant defined in LocalizationConst.bas. The subtitles should
    'not include the format selected by the user, i.e., "Detail" or "Summary".
    'ReportObj.ReportTitle2() = ""
    ReportObj.ReportTitle1 = "Batch Sheet Out Of Range - Residue and Viscosity"
    
    ReportObj.ReportTitle2 = "Production Start Date: " & StartDate & " End Date: " & EndDate
    
    ReportObj.MessageLines = "Line One " & vbCrLf & "LineTwo " & vbCrLf & "Line Three"

    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    'using the "lbl" convention on formula field names so that label text will handled automatically
    ReportObj.UseSubTotalCaptions() = 1
    ReportObj.UseHeaderCaptions() = 5
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If

    'Set sort order in .RPT file according to user selections in the Sort grid.
'    If (ReportObj.lSetSortCriteria(frm.moSort, 0) = kFailure) Then
'        GoTo badexit
'    End If
    
    '********* the following is specific to your report *************'
    Select Case RptFileName
        Case "mf_BatchSheet_OutOfRangeResidue.rpt"
        Case Else
    End Select
    '********* End of special processing *************'
    
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.SetSQL

    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    ReportObj.SetReportCaptions
    
    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
    ReportObj.SelectString = SelectObj.sGetUserReadableWhereClause(kLenPortrait)
    
    'If user has chosen to print report settings, set text for summary section.
    If (ReportObj.lSetSummarySection(frm.chkSummary.Value, frm.moOptions) = kFailure) Then
        GoTo badexit
    End If
    
    'CUSTOMIZE:  If using work tables, restrict report data to current Session ID.  If using
    'real tables, might restrict report data to current company or other criteria.
'    If (ReportObj.lRestrictBy("{.SessionID} = " & ReportObj.SessionID) = kFailure) Then
'SGS DEJ change to map to the actual table.
    If (ReportObj.lRestrictBy("{" & "tmf_BatchSheet_OutOfRangeResidue_Rpt_Wrk_RKL" & ".SessionID} = " & ReportObj.SessionID) = kFailure) Then

    'frm.sWorkTableCollection(1)
        GoTo badexit
    End If
    
    If bPeriodEnd Then
        gbSkipPrintDialog = True    '   don't show print dialog
    End If
    
    ReportObj.ProcessReport frm, sButton, iFileType, sFileName
    gbSkipPrintDialog = True    '   if multiple rpts, show print dialog one time only.
            
            
    If Not bPeriodEnd Then ShowStatusNone frm
    
    OutOfRangeResidue = kSuccess
    
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    
    Exit Function
    
badexit:
    ReportObj.CleanupWorkTables
    If Not bPeriodEnd Then ShowStatusNone frm
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
    Exit Function
End Function














