VERSION 1.0 CLASS
BEGIN
  MultiUse = 0   'False
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsmfzse001"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Public moFramework          As Object

Public mlContext            As Long

Public moDasSession         As Object

Public moAppDB              As Object


Public moSysSession         As Object

Public miShutDownRequester  As Integer

Public mlError              As Long

Private mlRunFlags          As Long

Private mlUIActive          As Long

Public mfrmMain            As Object

Const VBRIG_MODULE_ID_STRING = "mfzse001.CLS"

Public mbPeriodEnd          As Boolean


Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "clsmfzse001"
End Function

Private Sub Class_Initialize()
'************************************************************************
' Description:
'    Class_Initialize re-dimensions the Hide and Minimize Form Arrays
'    to 0.  The purpose of this is to ensure that the UBound function
'    doesn't error if the ShowUI or RestoreUI are called out of order.
'
'    Class_Initialize also sets the ShutDownRequester variable to say
'    that if anything goes wrong initially within this class, that the
'    framework will automatically attempt to shut down this class object.
'************************************************************************
    mlUIActive = kChildObjectInactive
    miShutDownRequester = kFrameworkShutDown
End Sub

Public Function InitializeObject(ByVal oFramework As Object, ByVal lContext As Long) As Long
'***********************************************************************
'     Description:
'       InitalizeObject performs setup required for all applications.
'
'       InitalizeObject sets the context in which this class was invoked.
'       The context contains information from both the framework and
'       application on how this class was invoked.
'       The available user-defined application contexts that can be derived
'       from the lContext argument for clsWarehouseBinMnt are:
'           1) normal           (kContextNormal)
'           2) Add-on-the-fly   (kContextAOF)
'           3) Drill Around     (kContextDA)
'
'       InitalizeObject also sets the Non-UI objects required by all
'       applications.  These Non-UI objects are:
'           1) system Manager Session Object, which provides our applications
'              with user specific session information such as the
'              current CompanyID, Business date, etc and more importantly
'              Application data source Name, and system data source Name.
'           2) DAS Session Object, which supplies an instance of
'              the SOTA DAS so our projects can use the methods/Services
'              required for data Access.
'           3) Once we have the DAS Session Object, Application DSN, and
'              system DSN we can open the Application and system Databases,
'              required for data manipulation.
'
'       InitializeObject is called for Sota Inteferface 1, Sota Interface 2
'       and Sota Interface 3.
'
'    Parameters:
'       oFramework <in> - reference to the framework event object which
'                         gives this object access to the public
'                         framework properties and methods.
'       lContext <in>   - The context contains information from both the
'                         framework and application on how this class was
'                         invoked.
'
'   Return Values:
'       kSuccess    - if the function executed its code without error.
'       kFailure    - if any of the code executed in this
'                     function produced an error or invalid result.
'                     In case of kFailure, the Framework will execute
'                     the TerminateObject method of this class and shut
'                     down this object.
'***********************************************************************
    InitializeObject = kFailure

    DefaultInitializeObject Me, oFramework, lContext, App.ProductName, App.Title

    InitializeObject = kSuccess     ' return success
End Function

Public Function LoadUI(ByVal lContext As Long) As Long
'***********************************************************************
'     Description:
'       LoadUI is responsible for loading the main form(s) required for
'       user interface of this class.  LoadUI performs the following
'       tasks:
'           1) Instantiate (or set a pointer to) the main form(s) of
'              the class.
'           2) In order for the form to use the class' public
'              variables, a reference back to the class is required.
'              This is done using the form's oClass Property Set and setting
'              the object property to Me (this class instance).
'           3) For the application to use the form in a variety of ways,
'              (Maintenance, Add-on-the-Fly, Drill Around), the Form needs
'              to know the user-defined context in which it was invoked.
'              The form contains a property to Hold this Value which it
'              refers to as the Run Mode.  The Run Mode can
'              be extracted by performing a bit-wise comparison of the
'              context originally passed in through the InitializeObject method.
'
'       LoadUI is called automatically by the framework for SotaInterface 2,3 and
'       the AutoLoadUI run flag is set.
'
'    Parameters:
'           lContext <in>   - Context of the LoadUI or how it is being
'                             invoked by the framework
'   Return Values:
'       kSuccess    - if the function executed its code without error.
'       kFailure    - if any of the code executed in this
'                     function produced an error or invalid result.
'                     In case of kFailure, the Framework will proceed
'                     to shut down this object starting with the
'                     QueryShutDown method, proceeding to UnloadUI and
'                     TerminateObject.
'***********************************************************************
    LoadUI = kFailure

    Set mfrmMain = frmmfzse001

    Set mfrmMain.oClass = Me
    mfrmMain.lRunMode = mlContext And kRunModeMask
    
    Load frmmfzse001
    If mlError Then Err.Raise mlError
    
    'determine if the form load was successful
    If mfrmMain.bLoadSuccess Then
        LoadUI = kSuccess
    Else
        LoadUI = kFailure
    End If

End Function

Public Function GetUIHandle(ByVal lContext As Long) As Long
    If Not mfrmMain Is Nothing Then
        GetUIHandle = mfrmMain.hwnd
    End If
End Function

Public Function DisplayUI(ByVal lContext As Long) As Long
'*********************************************************************
'   Description:
'       The framework will call this method when the class object is
'       initially invoked and the RunFlags are are set to AutoDisplayUI.
'
'       DisplayUI is called automatically by the framework for SotaInterface 2,3 and
'       the AutoDisplayUI run flag is set.
'
'    Parameters:
'           lContext <in>   - Context of the DisplayUI or how it is being
'                             invoked by the framework.
'   Return Values:
'       kSuccess    - if the function executed its code without error.
'       kFailure    - if any of the code executed in this
'                     function produced an error or invalid result.
'                     In case of kFailure, the Framework will proceed
'                     to shut down this object starting with the
'                     QueryShutDown method, proceeding to UnloadUI and
'                     TerminateObject.
'*********************************************************************
#If InProc = 0 Then
    DisplayUI = kFailure
    If mfrmMain Is Nothing Then Exit Function

    mfrmMain.Show
    DoEvents
    If mlError Then Err.Raise mlError
    
    mfrmMain.SetFocus
    DisplayUI = kSuccess
#Else
    DisplayUI = EFW_CT_MODALEXIT
#End If
End Function

Public Function ShowUI(ByVal lContext As Long) As Long
'*********************************************************************
'   Description:
'       The framework will call this method when
'       the UI of this object had been hidden in an
'       explicit HideUI because the framework was minimized.
'       When the framework is restored, it requests the UI
'       of its objects to show themselves.
'
'       ShowUI is called automatically by the framework for SotaInterface 2,3 and
'
'    Parameters:
'           lContext <in>   - Context of the ShowUI or how it is being
'                             invoked by the framework.
'   Return Values:
'       kSuccess    - if the function executed its code without error.
'       kFailure    - if any of the code executed in this
'                     function produced an error or invalid result.
'                     Currently the framework ignores a failure.
'*********************************************************************
#If InProc = 0 Then
    ShowUI = kFailure

    If Not mfrmMain Is Nothing Then
        mfrmMain.Show
        DoEvents
    End If
#End If

    ShowUI = kSuccess
End Function

Public Function MinimizeUI(ByVal lContext As Long) As Long
'*********************************************************************
'   Description:
'       MinimizeUI minimizes the object's main form and hides all other forms
'       launched directly from the main form or from the class.
'       Forms already minimized or already hidden are ignored.
'       Forms launched from loaded objects are also ignored.
'
'
'    Parameters:
'           lContext <in>   - Context of the MinimizeUI or how it is being
'                             invoked by the framework.
'   Return Values:
'       kSuccess    - if the function executed its code without error.
'       kFailure    - if any of the code executed in this
'                     function produced an error or invalid result.
'                     The framework currently ignores a failure.
'*********************************************************************
    MinimizeUI = kFailure

    If Not mfrmMain Is Nothing Then
#If InProc = 0 Then
        If gbActiveChildObjects(mfrmMain.moSotaObjects) Then Exit Function
#End If
        mfrmMain.WindowState = vbMinimized
    End If

    MinimizeUI = kSuccess
End Function

Public Function RestoreUI(ByVal lContext As Long) As Long
'*********************************************************************
'   Description:
'       Calls the general procedure which restores the object's main
'       form and all other forms explicitly minimized in a previous
'       MinimizeUI.
'
'    Parameters:
'           lContext <in>   - Context of the RestoreUI or how it is being
'                             invoked by the framework.
'   Return Values:
'       kSuccess    - if the function executed its code without error.
'       kFailure    - if any of the code executed in this
'                     function produced an error or invalid result.
'                     The framework currently ignores a failure.
'*********************************************************************
    RestoreUI = kFailure

    If Not mfrmMain Is Nothing Then
        mfrmMain.WindowState = vbNormal
    End If

    RestoreUI = kSuccess
End Function

Public Function HideUI(ByVal lContext As Long) As Long
'*********************************************************************
'   Description:
'       If any child objects are Active, then this form is the parent
'       and it cannot hide itself.
'
'    Parameters:
'           lContext <in>   - Context of the HIdeUI or how it is being
'                             invoked by the framework.
'   Return Values:
'       kSuccess    - if the function executed its code without error.
'       kFailure    - if any of the code executed in this
'                     function produced an error or invalid result.
'                     The framework currently ignores a failure.
'*********************************************************************
#If InProc = 0 Then
    HideUI = kFailure

    If Not mfrmMain Is Nothing Then
        If gbActiveChildObjects(mfrmMain.moSotaObjects) Then Exit Function
        mfrmMain.Hide
    End If
#End If

    HideUI = kSuccess
End Function

'***********************************************************************
'                   Class Object ShutDown Process
'                   -----------------------------
'
'   SOTA Interface 3:
'       If InitializeObject returns kFailure, then the TerminateObject
'       method is called immediately and this object is guaranteed to
'       shutdown whether or not the TerminateObject returned kSuccess
'       or kFailure.
'
'       If LoadUI or DisplayUI returned kFailure, the QueryShutDown method is
'       called, followed by UnloadUI and TerminateObject.  The framework will
'       ignore failures from the QueryShutDown, UnloadUI, and Terminate object,
'       because the object is in a failure.
'
'       If the Framework requests shutdown or the UnloadSelf call requests
'       shutdown, then the QueryShutDown method is invoked.
'       If a successful QueryShutDown is performed, the UnloadUI method is
'       called. And if a successful UnloadUI is performed, TerminateObject
'       method is called.  Once the TerminateObject method is called, the
'       object will be shut down whether or not the TerminateObject returned
'       success or failure.
'
'       Special NOTE:
'           The QueryShutDown is designed to check for Active child objects
'           and return failure to the framework if any have been found.  If
'           there are any Active Child Objects remaining after a successful
'           QueryShutDown, the framework will shut them down prior to the
'           UnloadUI method call.
'
'           The UnloadUI is designed to unload its UI and within that it will unload
'           all its child objects (SOTA children).  If there are
'           any SOTA child objects remaining after a successful UnloadUI,
'           the framework will shut them down prior to the TerminateObject
'           method call.
'
'           The TerminateObject is designed to unload/remove all Non-UI
'           object references.  If there are any child Non-UI (No Sota
'           Interface or Sota Interface 1 (?)) remaining after the
'           TerminateObject call, then the framework will remove or
'           shut them down.
'***********************************************************************
Public Function QueryShutDown(lContext As Long) As Long
'***********************************************************************
'   Description:
'       QueryShutDown method to determine if this form can be
'       shutdown.  This is done by checking all child objects (created
'       via a LoadSotaObject) for the lUIActive property set to
'       kChildObjectActive.
'
'       QueryShutDown is called automatically by the framework for
'       Sota Interface 1, 2, 3.
'
'    Parameters:
'           lContext <in>   - Context of the QueryShutDown or how it is being
'                             invoked by the framework.
'   Return Values:
'       kSuccess    - if the function executed its code without error.
'                     Proceed with the ShutDown
'       kFailure    - if any of the code executed in this
'                     function produced an error or invalid result.
'                     Do not proceed with the ShutDown because there is
'                     an Active child object.
'***********************************************************************
    QueryShutDown = kFailure

    If Not mfrmMain Is Nothing Then
        If gbActiveChildObjects(mfrmMain.moSotaObjects) Then Exit Function
    End If

    QueryShutDown = kSuccess
End Function

Public Function UnloadUI(ByVal lContext As Long) As Long
'***********************************************************************
'   Description:
'       UnloadUI method attempts to Unload the UI or any forms loaded
'       within the life of this class object.  During the Unload of
'       the form, all SOTA child Objects (Sota Interface 2,3) must
'       be unloaded.  This is currently coded in the form's Query_Unload
'       event.
'
'       UnLoadUI is called automatically by the framework for SotaInterface 2,3
'
'    Parameters:
'           lContext <in>   - Context of the UnloadUI or how it is being
'                             invoked by the framework.
'   Return Values:
'       kSuccess    - if the function executed its code without error.
'                     Proceed with the ShutDown
'       kFailure    - if any of the code executed in this
'                     function produced an error or invalid result.
'                     Do not proceed with the ShutDown because there is
'                     a problem or the user has cancelled the framwork
'                     shutdown request.  This can be done if a dirty
'                     record is up an the form asks to save changes.
'                     If the user presses the cancel button, then the
'                     user is requesting the entire shutdown of the
'                     application to cancel its shutdown.
'***********************************************************************
    UnloadUI = kFailure

    If Not mfrmMain Is Nothing Then
        If miShutDownRequester = kFrameworkShutDown Then
            Unload mfrmMain
            If mfrmMain.bCancelShutDown Then Exit Function
            Set mfrmMain = Nothing
        Else
          If mlError Then mfrmMain.PerformCleanShutDown
        End If
    End If

    UnloadUI = kSuccess
End Function

Public Function TerminateObject(ByVal lContext As Long) As Long
'***********************************************************************
'   Description:
'       TerminateObject method removes all Non-UI object references
'       created in through the InitializeObject method.  If the framework
'       requested to shutdown of this object, then setting the framework
'       reference to Nothing is executed.  Otherwise, the form requested
'       an UnloadSelf with its QueryUnload.  After this TerminatObject
'       method has finished executing, control is relinquished
'       back to the that form event.  At that point, it is okay to set
'       the framework reference to nothing.
'
'    Parameters:
'           lContext <in>   - Context of the TerminateObject or how it is being
'                             invoked by the framework.
'   Return Values:
'       kSuccess    - if the function executed its code without error.
'                     Proceed with the ShutDown
'       kFailure    - if any of the code executed in this
'                     function produced an error or invalid result.
'                     Failure is ignored and the Object is guaranteed
'                     to shutdown at this point.
'***********************************************************************
    TerminateObject = kFailure

    DefaultTerminateObject Me
    
    Set mfrmMain = Nothing
    
    TerminateObject = kSuccess
End Function

Public Property Get lUIActive() As Long
'*************************************************************************
' Description:
'    lUIActive Property Get informs the parent object of its Active state.
'    A class is Active if it is in the middle of processing a record.
'    This property is **Required** for all SOTA Interface 3 classes.
'*************************************************************************
    lUIActive = mlUIActive
End Property
Public Property Let lUIActive(lNewActive As Long)
'*************************************************************************
' Description:
'    lUIActive Property Let is contains the new Active state of the class.
'    This is set internally within the form or method within this class.
'    This property is **Required** for all SOTA Interface 3 classes.
'*************************************************************************
    mlUIActive = lNewActive
End Property

Public Property Get lRunFlags() As Long
'**************************************************************************
' Description:
'    lRunFlags Property Get can be used to inform other objects or modules.
'    of how this object was invoked by the framework or parent object.
'    This property is **Required** for all SOTA Interface 3 classes.
'**************************************************************************
    lRunFlags = mlRunFlags
End Property
Public Property Let lRunFlags(ltRunFlags As Long)
'*******************************************************************
' Description:
'    lRunFlags Property Let is set by the Framework no matter which
'    interface Type is being used.  This property is set before any
'    other method call of this class.
'    This property is **Required** for all SOTA Interface 3 classes.
'*******************************************************************
    mlRunFlags = ltRunFlags
End Property

Public Property Get bPeriodEnd() As Boolean
'+++ VB/Rig Skip +++
    bPeriodEnd = mbPeriodEnd
End Property

Public Property Let bPeriodEnd(bSetPeriodEnd As Boolean)
'+++ VB/Rig Skip +++
    mbPeriodEnd = bSetPeriodEnd
End Property

Public Function lGetSettings(ByRef sSetting, ByRef sSelect, Optional oResponse As Variant) As Long
'************************************************************************************
'      Desc: Retrieve the report settings as a string.
'     Parms: sSetting - Output; String of saved setting id/setting key pairs.
'            sSelect - Output; String of saved setting key/select info pairs.
'            oResponse as Variant - Optional object used for debugging
'   Returns: kSuccess; kFailure
'            sSetting - Each setting is a string and a key. Each string and key is
'            separated by a tab (vbTab). Each setting is delimited by a CRLF (vbCrLf).
'            The first setting in the list is the most recently used by the user.
'            If no settings class exists, then empty string " is returned.
'            If error, then empty string " is returned.
'            sSelect - Each setting is a key and selection row information.
'            Each row is separated by a semicolon. Values within a row
'            are separated by commas.  Each row contains setting key,
'            caption, operator, start value, end value.  E.g.,
'            <setting key>, <caption>, <opertor>,<start value>, <end value>;
'            <setting key>, <caption>, <opertor>,<start value>, <end value>.
'            If no settings, then empty string (") is returned.
'   Assumes: Settings class proc Initialize has been run.
'            Selection control proc bInitSelect has been run.
'************************************************************************************
    lGetSettings = kFailure

    On Error Resume Next
    sSetting = mfrmMain.moSettings.sGetSettings()
    If Err <> 0 Then
        sSetting = ""
    End If
    
    sSelect = mfrmMain.moSelect.sGetSettings()
    If Err <> 0 Then
        sSelect = ""
    End If
    
    lGetSettings = kSuccess
End Function

Public Sub GenerateReport(iFileType As Integer, sFileName As String, _
    Optional lSettingKey As Variant, Optional sSelect As Variant, _
    Optional oResponse As Variant)
'************************************************************************************
'      Desc: Generate web report file by calling HandleToolbarClick.
'     Parms: iFileType as Integer - File type to create (0 = HTML, 1 = RPT)
'            sFileName as String - File name to create (including path)
'            lSettingKey as Variant - Optional; setting selected by user
'            sSelect as Variant - Optional; selection grid values
'            oResponse as Variant - Optional object used for debugging
'************************************************************************************
    '    Get the select, sort, and options for given setting.
    If Not IsMissing(lSettingKey) Then
        If lSettingKey <> 0 Then
            mfrmMain.moSettings.bLoadSettingsByKey (CLng(lSettingKey))
        End If
    End If
    
    '   Put Web Client Selection Grid values on Web Server Selection Grid
    If Not IsMissing(sSelect) Then
        On Error Resume Next    '   if selection grid does not exist
        mfrmMain.moSelect.lPutSelectGrid sSelect
        On Error GoTo 0     '   normal error processing
    End If
    
    mfrmMain.HandleToolbarClick kTbPreview, iFileType, sFileName
End Sub


Public Function sGetSelection(Optional oResponse As Variant) As String
'************************************************************************************
'      Desc: Retrieve the report selection grid information as a string.
'     Parms: oResponse as Variant - Optional object used for debugging
'   Returns: String of selection grid information.
'            Assumes: Selection Grid's proc bInitSelect has been run.
'************************************************************************************
    On Error Resume Next
    sGetSelection = mfrmMain.moSelect.sGetSelection()
    If Err <> 0 Then
        sGetSelection = ""
    End If
End Function


