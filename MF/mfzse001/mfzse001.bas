Attribute VB_Name = "basmfzse001"

Const VBRIG_MODULE_ID_STRING = "mfzse001.BAS"

Public gbSkipPrintDialog As Boolean     '   Show print dialog



Public Sub Main()

    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("mfzse001.clsmfzse001")
        StartAppInStandaloneMode oApp, Command$()    ' ** The third parameter MUST be the Enterprise
                                                     '    TaskID for this project in order for the
                                                     '    Standalone method to be activated for ActiveX Exe's
    End If

End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basmfzse001"
End Function

Public Function lStartReport(sButton As String, frm As Form, bPeriodEnd As Boolean, _
    sPerEndDate, iPerNo, sPerYear, Optional iFileType As Variant, Optional sFileName As Variant) As Long
    
Dim sWhereClause As String
Dim sTablesUsed As String
Dim sSelect As String
Dim sInsert As String
Dim lRetval As Long
Dim bValid As Boolean
Dim iNumTablesUsed As Integer
Dim RptFileName As String
Dim lBadRow As Long
Dim SelectObj As clsSelection
Dim ReportObj As clsReportEngine
Dim SortObj As clsSort
Dim DBObj As Object
Dim IRptSort As Integer
Dim sDetail As String

    On Error GoTo badexit

    lStartReport = kFailure
    
    If Not bValidEntries(frm, iFileType) Then
        Exit Function
    End If
    
    If Not bPeriodEnd Then
        frm.sbrMain.Status = SOTA_SB_BUSY
        frm.MousePointer = vbHourglass
    End If

    If Trim(gsGridReadCellText(frm.grdSort, 1, 1)) <> "Item" Then
        IRptSort = 1        'Production Report by Production Date
    Else
        IRptSort = 0        'Production Report by Item
    End If

    Set SelectObj = frm.moSelect
    Set ReportObj = frm.moReport
    Set SortObj = frm.moSort

    Set DBObj = frm.oClass.moAppDB
   
    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
    
    'Correct bad user input in the Selection grid.
    'lBadRow contains the failed row if the parameter passed is False
    lBadRow = SelectObj.lValidateGrid(True)
    
    'Create the WHERE clause based on user input in the Selection grid & Session ID.
    bValid = SelectObj.bGetWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True)

    'If the Employee table is present, the Transaction table is needed as well
    If InStr(sTablesUsed, "tmfEmployee_HAI") <> 0 Then
        sTablesUsed = sTablesUsed & ", tmfWorkOrdTran_HAI WITH (NOLOCK)"
    End If
    
    ' The Work Order table is needed for Company restrictions
    If Not (InStr(sTablesUsed, "tmfWorkOrdHead_HAI") <> 0) Then
        sTablesUsed = sTablesUsed & ", tmfWorkOrdHead_HAI WITH (NOLOCK)"
    End If
    
'    Append criteria to restrict data returned to the current company.
' & " AND tmfWorkOrdHead_HAI.WorkOrderType=1 " _

    SelectObj.AppendToWhereClause sWhereClause, "tmfWorkOrdHead_HAI.CompanyID='" & frm.msCompanyID & kSQuote _
                                              & " AND (tmfWorkOrdHead_HAI.EntryDate <= " & gsQuoted(frm.calEndDate.SQLDate) _
                                              & " AND tmfWorkOrdHead_HAI.EntryDate >= " & gsQuoted(frm.calStartDate.SQLDate) & ")"
    
'    SelectObj.AppendToWhereClause sWhereClause, "tmfWorkOrdHead_HAI.CompanyID='" & frm.msCompanyID & kSQuote
    
    
    'CUSTOMIZE:  Add join specifications to WHERE clause based on appearance of a given table
    'in the sTablesUsed string (built as a result of call to bGetWhereClause above).
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tciAddress", "tarCustomer.PrimaryAddrKey=tciAddress.AddrKey"
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustAddr", "tarCustomer.CustKey=tarCustAddr.CustKey"
    SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "timItem", "timItem.ItemKey = tmfWorkOrdProd_HAI.ItemKey"
    SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tmfWorkOrdHead_HAI", "tmfWorkOrdHead_HAI.WorkOrderKey=tmfWorkOrdProd_HAI.WorkOrderKey"
    SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tmfEmployee_HAI", "tmfWorkOrdHead_HAI.WorkOrderKey = tmfWorkOrdTran_HAI.WorkOrderKey " _
                                                                             & " AND tmfWorkOrdTran_HAI.EmployeeKey = tmfEmployee_HAI.EmployeeKey"
    
    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
    If Not SelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
        GoTo badexit
    End If

    'CUSTOMIZE:  Build a string for selection of key field and session ID.
    sSelect = "SELECT DISTINCT tmfWorkOrdProd_HAI.WorkOrderKey, " & ReportObj.SessionID & " FROM " & sTablesUsed
    sSelect = sSelect & " " & sWhereClause
    
    #If RPTDEBUG Then
            sInsert = "INSERT INTO " & frm.sWorkTableCollection(1) & " (WorkOrderKey, SessionID) " & sSelect
    #Else
            sInsert = "INSERT INTO #" & frm.sWorkTableCollection(1) & " (WorkOrderKey, SessionID) " & sSelect
    #End If
    
    On Error Resume Next
    DBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
    gClearSotaErr
    On Error GoTo badexit
    
    'Check whether any records were inserted; if not, go to error handler.
    If IsMissing(iFileType) Then
        #If RPTDEBUG Then
            If Not SelectObj.bRecsToPrintExist(frm.sWorkTableCollection(1), "SessionID", ReportObj.SessionID, True) Then
        #Else
            If Not SelectObj.bRecsToPrintExist("#" & Left(frm.sWorkTableCollection(1), 19), "SessionID", ReportObj.SessionID, True) Then
        #End If
            GoTo badexit
        End If
    End If
   
    'CUSTOMIZE:  Set input and output parameters as needed and execute appropriate
    'custom stored procedure to populate remaining fields in primary work table and
    'insert records into secondary work tables.
    With DBObj
        .SetInParam (ReportObj.SessionID)
        .SetOutParam lRetval
        On Error Resume Next
        .ExecuteSP ("spmfReportProductionEntry")
        If Err.Number <> 0 Then
            lRetval = Err.Number
        Else
            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
            'errors after each attempt.  After storing all return values in local variables OR
            'encountering an error, release parameters.
            lRetval = DBObj.GetOutParam(2)
        End If
    End With
     
    If lRetval <> 0 Then
        ReportObj.ReportError kmsgProc, ""
        DBObj.ReleaseParams
        GoTo badexit
    End If
        
    DBObj.ReleaseParams
    gClearSotaErr
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)

    If frm.optFormat(0) Then                                    'Summary
        If IRptSort = 0 Then
            ReportObj.ReportFileName() = "mfzse001.rpt"         'Item Sort
        End If
        If IRptSort = 1 Then
            ReportObj.ReportFileName() = "mfzse002.rpt"         'Prod Date Sort
        End If
    End If

    If frm.optFormat(1) Then                                    'Detail
        If IRptSort = 0 Then
            ReportObj.ReportFileName() = "mfzse001.rpt"         'Item Sort
        End If
        If IRptSort = 1 Then
            ReportObj.ReportFileName() = "mfzse002.rpt"         'Prod date Sort
        End If
    End If

    If frm.optFormat(2) Then                                    'Prod Entry Detail
        ReportObj.ReportFileName() = "mfzse003.rpt"
    End If
    
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    ReportObj.Orientation() = vbPRORLandscape
    
    'CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    'to gsBuildString with a VB constant defined in StrConst.bas. The subtitles should
    'not include the format selected by the user, i.e., "Detail" or "Summary".
    'ReportObj.ReportTitle2() = ""

    ReportObj.ReportTitle1() = "Production Entry Report"    ' = gsBuildString(kCIFOBList, frm.oClass.moAppDB, frm.oClass.moSysSession)
    
    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    'using the "lbl" convention on formula field names so that label text will handled automatically
    ReportObj.UseSubTotalCaptions() = 1
    ReportObj.UseHeaderCaptions() = 1
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If

    'Set sort order in .RPT file according to user selections in the Sort grid.

    If (ReportObj.lSetSortCriteria(frm.moSort, 3) = kFailure) Then
            GoTo badexit
    End If
    
    
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.SetSQL

    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    ReportObj.SetReportCaptions
    
    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
    ReportObj.SelectString = SelectObj.sGetUserReadableWhereClause(kLenPortrait)
    
    'If user has chosen to print report settings, set text for summary section.
    If (ReportObj.lSetSummarySection(frm.chkSummary.Value, frm.moOptions) = kFailure) Then
        GoTo badexit
    End If
    
    Dim iUnitCostDecPlaces As Integer
    Dim iQtyDecPlaces As Integer
    
    iUnitCostDecPlaces = giGetValidInt(frm.oClass.moAppDB.Lookup("UnitCostDecPlaces", _
                         "tciOptions", "CompanyID = " & gsQuoted(frm.msCompanyID)))
    iQtyDecPlaces = giGetValidInt(frm.oClass.moAppDB.Lookup("QtyDecPlaces", "tciOptions", _
                    "CompanyID = " & gsQuoted(frm.msCompanyID)))
    
    ReportObj.bSetReportFormula "UnitCostDecPlaces", CInt(iUnitCostDecPlaces)
    ReportObj.bSetReportFormula "QtyDecPlaces", CInt(iQtyDecPlaces)
    
    'CUSTOMIZE:  If using work tables, restrict report data to current Session ID.  If using
    'real tables, might restrict report data to current company or other criteria.
    
    If (ReportObj.lRestrictBy("{" & frm.sWorkTableCollection(1) & ".sessionid } =" & ReportObj.SessionID) = kFailure) Then
        GoTo badexit
    End If
     
    'Format Options (Print Detail) ??
    If Not frm.optFormat(2) Then
        If ReportObj.bSetReportFormula("PrintDetail", gsQuoted(gsGetValidStr(frm.optFormat(1).Value))) = False Then
            GoTo badexit
        End If
    End If
    
    'If Format is Summary, then hide Detail Band
    'Format Options (PrintProd Detail) ??
    If frm.optFormat(0) Then
        sDetail = "False"
    Else
        sDetail = "True"
    End If
    
    If Not frm.optFormat(2) Then
        If ReportObj.bSetReportFormula("PrintProdDetails", QUOTECON & sDetail & QUOTECON) = False Then
            GoTo badexit
        End If
    End If
    
    If bPeriodEnd Then
        gbSkipPrintDialog = True    '   don't show print dialog
    End If
    
    ReportObj.ProcessReport frm, sButton, iFileType, sFileName
    gbSkipPrintDialog = True    '   if multiple rpts, show print dialog one time only.
            
            
    If Not bPeriodEnd Then
        frm.sbrMain.Status = SOTA_SB_START
        frm.MousePointer = vbDefault
    End If
    
    lStartReport = kSuccess
    
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    
    Exit Function
    
badexit:
    ReportObj.CleanupWorkTables
    If Not bPeriodEnd Then
        frm.sbrMain.Status = SOTA_SB_START
        frm.MousePointer = vbDefault
    End If
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
    Exit Function
End Function


Public Function bValidEntries(frm As Form, Optional iFileType As Variant) As Boolean
    
    On Error GoTo badexit
    
    bValidEntries = False
    
    If frm.calStartDate.Text = "" Then
        If IsMissing(iFileType) Then
            Call giSotaMsgBox(Nothing, frm.oClass.moSysSession, kmsgTransactionDateRequired)
            frm.tabReport.Tab = 1
            frm.calStartDate.SetFocus
            Exit Function
        Else
            frm.calStartDate.SQLDate = gsFormatDateToDB(frm.oClass.moSysSession.BusinessDate)
        End If
    End If
'-- Check End Date can't be blank
    If frm.calEndDate.Text = "" Then
        If IsMissing(iFileType) Then
            Call giSotaMsgBox(Nothing, frm.oClass.moSysSession, kmsgTransactionDateRequired)
            frm.tabReport.Tab = 1
            frm.calEndDate.SetFocus
            Exit Function
        Else
            frm.calEndDate.SQLDate = gsFormatDateToDB(frm.oClass.moSysSession.BusinessDate)
        End If
    End If
    '-- Check Start date is valid or not
    If Not frm.calStartDate.IsValid Then
        Call giSotaMsgBox(Nothing, frm.oClass.moSysSession, kmsgInDate)
        frm.tabReport.Tab = 1
        frm.calStartDate.SetFocus
        Exit Function
    End If
    '-- Check End date is valid or not
    If Not frm.calEndDate.IsValid Then
        Call giSotaMsgBox(Nothing, frm.oClass.moSysSession, kmsgInDate)
        frm.tabReport.Tab = 1
        frm.calEndDate.SetFocus
        Exit Function
    End If
    
    If CDate(frm.calEndDate.Text) < CDate(frm.calStartDate.Text) Then
        Call giSotaMsgBox(Nothing, frm.oClass.moSysSession, kmsgToNotLessThanFrom, frm.lblEndingDate.Caption, frm.lblStartDate.Caption)
        Exit Function
    End If

    bValidEntries = True
    
    
badexit:


End Function
