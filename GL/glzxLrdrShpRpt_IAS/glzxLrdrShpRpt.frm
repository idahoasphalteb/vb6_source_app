VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#113.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#48.0#0"; "sotasbar.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#50.0#0"; "SOTACalendar.ocx"
Object = "{9504980C-B928-4BF5-A5D0-13E1F649AECB}#31.0#0"; "SOTAVM.ocx"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#75.0#0"; "LookupView.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmLeaderShipReport 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Leadership Report"
   ClientHeight    =   6840
   ClientLeft      =   3060
   ClientTop       =   2115
   ClientWidth     =   9450
   HelpContextID   =   17370001
   Icon            =   "glzxLrdrShpRpt.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6840
   ScaleWidth      =   9450
   Begin TabDlg.SSTab tabReport 
      Height          =   5460
      Left            =   0
      TabIndex        =   7
      Top             =   960
      WhatsThisHelpID =   17370004
      Width           =   9375
      _ExtentX        =   16536
      _ExtentY        =   9631
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   4
      TabHeight       =   529
      TabCaption(0)   =   "&Main"
      TabPicture(0)   =   "glzxLrdrShpRpt.frx":23D2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label2"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "txtFiscPeriod"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "txtFiscYear"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "SSTab1"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "&Sales Tons Revenues"
      TabPicture(1)   =   "glzxLrdrShpRpt.frx":23EE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "SSTab2"
      Tab(1).ControlCount=   1
      Begin TabDlg.SSTab SSTab1 
         Height          =   4335
         Left            =   0
         TabIndex        =   29
         Top             =   1080
         Width           =   9375
         _ExtentX        =   16536
         _ExtentY        =   7646
         _Version        =   393216
         TabsPerRow      =   4
         TabHeight       =   520
         TabCaption(0)   =   "Pages to Include"
         TabPicture(0)   =   "glzxLrdrShpRpt.frx":240A
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "chRptPages(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "chRptPages(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "chRptPages(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "chRptPages(4)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "chRptPages(5)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "chRptPages(6)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "chRptPages(7)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "chRptPages(8)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "chRptPages(9)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "chRptPages(10)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "chRptPages(11)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "chRptPages(12)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "chRptPages(13)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "chRptPages(14)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "chRptPages(15)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "chRptPages(16)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "chRptPages(17)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "chRptPages(18)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "chRptPages(19)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "cmdSelectAll"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "cmdUnSelectAll"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "cmdInvert"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).ControlCount=   22
         TabCaption(1)   =   "Trucking Miles"
         TabPicture(1)   =   "glzxLrdrShpRpt.frx":2426
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "Label3"
         Tab(1).Control(1)=   "Label4"
         Tab(1).Control(2)=   "Label5"
         Tab(1).Control(3)=   "Label6"
         Tab(1).Control(4)=   "Label7"
         Tab(1).Control(5)=   "Label8"
         Tab(1).Control(6)=   "txtMilesYTDYear"
         Tab(1).Control(7)=   "txtMilesYTDPlan"
         Tab(1).Control(8)=   "txtMilesYTDActual"
         Tab(1).Control(9)=   "txtMilesCurrMonthPlan"
         Tab(1).Control(10)=   "txtMilesCurrMonthActual"
         Tab(1).ControlCount=   11
         TabCaption(2)   =   "Escalation/De-Escalation"
         TabPicture(2)   =   "glzxLrdrShpRpt.frx":2442
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "Label9"
         Tab(2).Control(1)=   "Label10"
         Tab(2).Control(2)=   "Label11"
         Tab(2).Control(3)=   "Label12"
         Tab(2).Control(4)=   "Label13"
         Tab(2).Control(5)=   "Label14"
         Tab(2).Control(6)=   "txtEscaYTDPrior"
         Tab(2).Control(7)=   "txtEscaYTDPlan"
         Tab(2).Control(8)=   "txtEscaYTDActual"
         Tab(2).Control(9)=   "txtEscaCurrMonthPlan"
         Tab(2).Control(10)=   "txtEscaCurrMonthActual"
         Tab(2).ControlCount=   11
         Begin VB.CommandButton cmdInvert 
            Caption         =   "Invert Selection"
            Height          =   375
            Left            =   7800
            TabIndex        =   113
            Top             =   1560
            Width           =   1335
         End
         Begin VB.CommandButton cmdUnSelectAll 
            Caption         =   "Unselect All"
            Height          =   375
            Left            =   7800
            TabIndex        =   112
            Top             =   1080
            Width           =   1335
         End
         Begin VB.CommandButton cmdSelectAll 
            Caption         =   "Select All"
            Height          =   375
            Left            =   7800
            TabIndex        =   111
            Top             =   600
            Width           =   1335
         End
         Begin NEWSOTALib.SOTACurrency txtMilesCurrMonthActual 
            Height          =   255
            Left            =   -73080
            TabIndex        =   51
            Top             =   1680
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin VB.CheckBox chRptPages 
            Caption         =   "Woods Cross Plant"
            Height          =   375
            Index           =   19
            Left            =   4440
            TabIndex        =   48
            Top             =   3360
            Value           =   1  'Checked
            Width           =   3135
         End
         Begin VB.CheckBox chRptPages 
            Caption         =   "Rawlins Plant"
            Height          =   375
            Index           =   18
            Left            =   4440
            TabIndex        =   47
            Top             =   3000
            Value           =   1  'Checked
            Width           =   3135
         End
         Begin VB.CheckBox chRptPages 
            Caption         =   "Peak General and Administrative"
            Height          =   375
            Index           =   17
            Left            =   4440
            TabIndex        =   46
            Top             =   2640
            Value           =   1  'Checked
            Width           =   3135
         End
         Begin VB.CheckBox chRptPages 
            Caption         =   "Peak Sales"
            Height          =   375
            Index           =   16
            Left            =   4440
            TabIndex        =   45
            Top             =   2280
            Value           =   1  'Checked
            Width           =   3135
         End
         Begin VB.CheckBox chRptPages 
            Caption         =   "Peak Asphalt, LLC Consolidated"
            Height          =   375
            Index           =   15
            Left            =   4440
            TabIndex        =   44
            Top             =   1920
            Value           =   1  'Checked
            Width           =   3135
         End
         Begin VB.CheckBox chRptPages 
            Caption         =   "Johnny B. Transport, LLC - Shop"
            Height          =   375
            Index           =   14
            Left            =   4440
            TabIndex        =   43
            Top             =   1560
            Value           =   1  'Checked
            Width           =   3135
         End
         Begin VB.CheckBox chRptPages 
            Caption         =   "Johnny B. Transport, LLC"
            Height          =   375
            Index           =   13
            Left            =   4440
            TabIndex        =   42
            Top             =   1200
            Value           =   1  'Checked
            Width           =   3135
         End
         Begin VB.CheckBox chRptPages 
            Caption         =   "Johnny B. Transport, LLC Consolidated"
            Height          =   375
            Index           =   12
            Left            =   4440
            TabIndex        =   41
            Top             =   840
            Value           =   1  'Checked
            Width           =   3135
         End
         Begin VB.CheckBox chRptPages 
            Caption         =   "Laboratory"
            Height          =   375
            Index           =   11
            Left            =   4440
            TabIndex        =   40
            Top             =   480
            Value           =   1  'Checked
            Width           =   3135
         End
         Begin VB.CheckBox chRptPages 
            Caption         =   "Nampa Plant"
            Height          =   375
            Index           =   10
            Left            =   720
            TabIndex        =   39
            Top             =   3720
            Value           =   1  'Checked
            Width           =   3135
         End
         Begin VB.CheckBox chRptPages 
            Caption         =   "Hauser Plant"
            Height          =   375
            Index           =   9
            Left            =   720
            TabIndex        =   38
            Top             =   3360
            Value           =   1  'Checked
            Width           =   3135
         End
         Begin VB.CheckBox chRptPages 
            Caption         =   "Blackfoot Plant"
            Height          =   375
            Index           =   8
            Left            =   720
            TabIndex        =   37
            Top             =   3000
            Value           =   1  'Checked
            Width           =   3135
         End
         Begin VB.CheckBox chRptPages 
            Caption         =   "Engineering, Safety, and Environmental"
            Height          =   375
            Index           =   7
            Left            =   720
            TabIndex        =   36
            Top             =   2640
            Value           =   1  'Checked
            Width           =   3135
         End
         Begin VB.CheckBox chRptPages 
            Caption         =   "IAS Sales"
            Height          =   375
            Index           =   6
            Left            =   720
            TabIndex        =   35
            Top             =   2280
            Value           =   1  'Checked
            Width           =   3135
         End
         Begin VB.CheckBox chRptPages 
            Caption         =   "IAS General and Administrative"
            Height          =   375
            Index           =   5
            Left            =   720
            TabIndex        =   34
            Top             =   1920
            Value           =   1  'Checked
            Width           =   3135
         End
         Begin VB.CheckBox chRptPages 
            Caption         =   "Operations"
            Height          =   375
            Index           =   4
            Left            =   720
            TabIndex        =   33
            Top             =   1560
            Value           =   1  'Checked
            Width           =   3135
         End
         Begin VB.CheckBox chRptPages 
            Caption         =   "Consolidated Sales and Marketing"
            Height          =   375
            Index           =   3
            Left            =   720
            TabIndex        =   32
            Top             =   1200
            Value           =   1  'Checked
            Width           =   3135
         End
         Begin VB.CheckBox chRptPages 
            Caption         =   "Idaho Aphalt Supply, Inc."
            Height          =   375
            Index           =   2
            Left            =   720
            TabIndex        =   31
            Top             =   840
            Value           =   1  'Checked
            Width           =   3135
         End
         Begin VB.CheckBox chRptPages 
            Caption         =   "Idaho Aphalt Supply Consolidated"
            Height          =   375
            Index           =   1
            Left            =   720
            TabIndex        =   30
            Top             =   480
            Value           =   1  'Checked
            Width           =   3135
         End
         Begin NEWSOTALib.SOTACurrency txtMilesCurrMonthPlan 
            Height          =   255
            Left            =   -69240
            TabIndex        =   53
            Top             =   1680
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtMilesYTDActual 
            Height          =   255
            Left            =   -74400
            TabIndex        =   55
            Top             =   2640
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtMilesYTDPlan 
            Height          =   255
            Left            =   -71280
            TabIndex        =   57
            Top             =   2640
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtMilesYTDYear 
            Height          =   255
            Left            =   -68160
            TabIndex        =   59
            Top             =   2640
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtEscaCurrMonthActual 
            Height          =   255
            Left            =   -73080
            TabIndex        =   115
            Top             =   1680
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtEscaCurrMonthPlan 
            Height          =   255
            Left            =   -69240
            TabIndex        =   116
            Top             =   1680
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtEscaYTDActual 
            Height          =   255
            Left            =   -74400
            TabIndex        =   117
            Top             =   2640
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtEscaYTDPlan 
            Height          =   255
            Left            =   -71280
            TabIndex        =   118
            Top             =   2640
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtEscaYTDPrior 
            Height          =   255
            Left            =   -68160
            TabIndex        =   119
            Top             =   2640
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin VB.Label Label14 
            Caption         =   "Current Month Actual: "
            Height          =   255
            Left            =   -73080
            TabIndex        =   124
            Top             =   1440
            Width           =   1575
         End
         Begin VB.Label Label13 
            Caption         =   "Current Month Plan: "
            Height          =   255
            Left            =   -69240
            TabIndex        =   123
            Top             =   1440
            Width           =   1575
         End
         Begin VB.Label Label12 
            Caption         =   "YTD Actual: "
            Height          =   255
            Left            =   -74400
            TabIndex        =   122
            Top             =   2400
            Width           =   1215
         End
         Begin VB.Label Label11 
            Caption         =   "YTD Plan: "
            Height          =   255
            Left            =   -71280
            TabIndex        =   121
            Top             =   2400
            Width           =   1215
         End
         Begin VB.Label Label10 
            Caption         =   "YTD Prior Year: "
            Height          =   255
            Left            =   -68160
            TabIndex        =   120
            Top             =   2400
            Width           =   1215
         End
         Begin VB.Label Label9 
            Alignment       =   2  'Center
            AutoSize        =   -1  'True
            Caption         =   "Escalation/De-Escalation for IAS Consolidated"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   -73470
            TabIndex        =   114
            Top             =   600
            Width           =   5745
         End
         Begin VB.Label Label8 
            Caption         =   "YTD Prior Year: "
            Height          =   255
            Left            =   -68160
            TabIndex        =   58
            Top             =   2400
            Width           =   1215
         End
         Begin VB.Label Label7 
            Caption         =   "YTD Plan: "
            Height          =   255
            Left            =   -71280
            TabIndex        =   56
            Top             =   2400
            Width           =   1215
         End
         Begin VB.Label Label6 
            Caption         =   "YTD Actual: "
            Height          =   255
            Left            =   -74400
            TabIndex        =   54
            Top             =   2400
            Width           =   1215
         End
         Begin VB.Label Label5 
            Caption         =   "Current Month Plan: "
            Height          =   255
            Left            =   -69240
            TabIndex        =   52
            Top             =   1440
            Width           =   1575
         End
         Begin VB.Label Label4 
            Caption         =   "Current Month Actual: "
            Height          =   255
            Left            =   -73080
            TabIndex        =   50
            Top             =   1440
            Width           =   1575
         End
         Begin VB.Label Label3 
            Alignment       =   2  'Center
            AutoSize        =   -1  'True
            Caption         =   "Trucking Miles for Johnny B"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   -72075
            TabIndex        =   49
            Top             =   480
            Width           =   3525
         End
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtFiscYear 
         Height          =   255
         Left            =   1200
         TabIndex        =   17
         Top             =   600
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   450
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         lMaxLength      =   4
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtFiscPeriod 
         Height          =   255
         Left            =   3240
         TabIndex        =   19
         Top             =   600
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   450
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         lMaxLength      =   2
      End
      Begin TabDlg.SSTab SSTab2 
         Height          =   3855
         Left            =   -75000
         TabIndex        =   60
         Top             =   1200
         Width           =   9375
         _ExtentX        =   16536
         _ExtentY        =   6800
         _Version        =   393216
         Tabs            =   5
         TabsPerRow      =   5
         TabHeight       =   520
         TabCaption(0)   =   "Blackfoot Plant"
         TabPicture(0)   =   "glzxLrdrShpRpt.frx":245E
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblYTDPriorYear(8)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblYTDPlan(8)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblYTDActual(8)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblCurrentMonthPlan(8)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblCurrentMonthActual(8)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtYTDPriorYear(8)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtYTDPlan(8)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtYTDActual(8)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtCurrMonthPlan(8)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtCurrMonthActual(8)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Hauser Plant"
         TabPicture(1)   =   "glzxLrdrShpRpt.frx":247A
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "txtCurrMonthActual(9)"
         Tab(1).Control(1)=   "txtCurrMonthPlan(9)"
         Tab(1).Control(2)=   "txtYTDActual(9)"
         Tab(1).Control(3)=   "txtYTDPlan(9)"
         Tab(1).Control(4)=   "txtYTDPriorYear(9)"
         Tab(1).Control(5)=   "lblYTDPriorYear(9)"
         Tab(1).Control(6)=   "lblYTDPlan(9)"
         Tab(1).Control(7)=   "lblYTDActual(9)"
         Tab(1).Control(8)=   "lblCurrentMonthPlan(9)"
         Tab(1).Control(9)=   "lblCurrentMonthActual(9)"
         Tab(1).ControlCount=   10
         TabCaption(2)   =   "Nampa Plant"
         TabPicture(2)   =   "glzxLrdrShpRpt.frx":2496
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "txtCurrMonthActual(10)"
         Tab(2).Control(1)=   "txtCurrMonthPlan(10)"
         Tab(2).Control(2)=   "txtYTDActual(10)"
         Tab(2).Control(3)=   "txtYTDPlan(10)"
         Tab(2).Control(4)=   "txtYTDPriorYear(10)"
         Tab(2).Control(5)=   "lblYTDPriorYear(10)"
         Tab(2).Control(6)=   "lblYTDPlan(10)"
         Tab(2).Control(7)=   "lblYTDActual(10)"
         Tab(2).Control(8)=   "lblCurrentMonthPlan(10)"
         Tab(2).Control(9)=   "lblCurrentMonthActual(10)"
         Tab(2).ControlCount=   10
         TabCaption(3)   =   "Rawlins Plant"
         TabPicture(3)   =   "glzxLrdrShpRpt.frx":24B2
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "txtCurrMonthActual(18)"
         Tab(3).Control(1)=   "txtCurrMonthPlan(18)"
         Tab(3).Control(2)=   "txtYTDActual(18)"
         Tab(3).Control(3)=   "txtYTDPlan(18)"
         Tab(3).Control(4)=   "txtYTDPriorYear(18)"
         Tab(3).Control(5)=   "lblYTDPriorYear(18)"
         Tab(3).Control(6)=   "lblYTDPlan(18)"
         Tab(3).Control(7)=   "lblYTDActual(18)"
         Tab(3).Control(8)=   "lblCurrentMonthPlan(18)"
         Tab(3).Control(9)=   "lblCurrentMonthActual(18)"
         Tab(3).ControlCount=   10
         TabCaption(4)   =   "Woods Cross Plant"
         TabPicture(4)   =   "glzxLrdrShpRpt.frx":24CE
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "txtCurrMonthActual(19)"
         Tab(4).Control(1)=   "txtCurrMonthPlan(19)"
         Tab(4).Control(2)=   "txtYTDActual(19)"
         Tab(4).Control(3)=   "txtYTDPlan(19)"
         Tab(4).Control(4)=   "txtYTDPriorYear(19)"
         Tab(4).Control(5)=   "lblYTDPriorYear(19)"
         Tab(4).Control(6)=   "lblYTDPlan(19)"
         Tab(4).Control(7)=   "lblYTDActual(19)"
         Tab(4).Control(8)=   "lblCurrentMonthPlan(19)"
         Tab(4).Control(9)=   "lblCurrentMonthActual(19)"
         Tab(4).ControlCount=   10
         Begin NEWSOTALib.SOTACurrency txtCurrMonthActual 
            Height          =   255
            Index           =   8
            Left            =   2040
            TabIndex        =   61
            Top             =   1560
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtCurrMonthPlan 
            Height          =   255
            Index           =   8
            Left            =   5880
            TabIndex        =   62
            Top             =   1560
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtYTDActual 
            Height          =   255
            Index           =   8
            Left            =   720
            TabIndex        =   63
            Top             =   2520
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtYTDPlan 
            Height          =   255
            Index           =   8
            Left            =   3840
            TabIndex        =   64
            Top             =   2520
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtYTDPriorYear 
            Height          =   255
            Index           =   8
            Left            =   6960
            TabIndex        =   65
            Top             =   2520
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtCurrMonthActual 
            Height          =   255
            Index           =   9
            Left            =   -72960
            TabIndex        =   66
            Top             =   1560
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtCurrMonthPlan 
            Height          =   255
            Index           =   9
            Left            =   -69120
            TabIndex        =   67
            Top             =   1560
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtYTDActual 
            Height          =   255
            Index           =   9
            Left            =   -74280
            TabIndex        =   68
            Top             =   2520
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtYTDPlan 
            Height          =   255
            Index           =   9
            Left            =   -71160
            TabIndex        =   69
            Top             =   2520
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtYTDPriorYear 
            Height          =   255
            Index           =   9
            Left            =   -68040
            TabIndex        =   70
            Top             =   2520
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtCurrMonthActual 
            Height          =   255
            Index           =   10
            Left            =   -72960
            TabIndex        =   71
            Top             =   1560
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtCurrMonthPlan 
            Height          =   255
            Index           =   10
            Left            =   -69120
            TabIndex        =   72
            Top             =   1560
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtYTDActual 
            Height          =   255
            Index           =   10
            Left            =   -74280
            TabIndex        =   73
            Top             =   2520
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtYTDPlan 
            Height          =   255
            Index           =   10
            Left            =   -71160
            TabIndex        =   74
            Top             =   2520
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtYTDPriorYear 
            Height          =   255
            Index           =   10
            Left            =   -68040
            TabIndex        =   75
            Top             =   2520
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtCurrMonthActual 
            Height          =   255
            Index           =   18
            Left            =   -72960
            TabIndex        =   76
            Top             =   1560
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtCurrMonthPlan 
            Height          =   255
            Index           =   18
            Left            =   -69120
            TabIndex        =   77
            Top             =   1560
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtYTDActual 
            Height          =   255
            Index           =   18
            Left            =   -74280
            TabIndex        =   78
            Top             =   2520
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtYTDPlan 
            Height          =   255
            Index           =   18
            Left            =   -71160
            TabIndex        =   79
            Top             =   2520
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtYTDPriorYear 
            Height          =   255
            Index           =   18
            Left            =   -68040
            TabIndex        =   80
            Top             =   2520
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtCurrMonthActual 
            Height          =   255
            Index           =   19
            Left            =   -72960
            TabIndex        =   81
            Top             =   1560
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtCurrMonthPlan 
            Height          =   255
            Index           =   19
            Left            =   -69120
            TabIndex        =   82
            Top             =   1560
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtYTDActual 
            Height          =   255
            Index           =   19
            Left            =   -74280
            TabIndex        =   83
            Top             =   2520
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtYTDPlan 
            Height          =   255
            Index           =   19
            Left            =   -71160
            TabIndex        =   84
            Top             =   2520
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTACurrency txtYTDPriorYear 
            Height          =   255
            Index           =   19
            Left            =   -68040
            TabIndex        =   85
            Top             =   2520
            Width           =   1935
            _Version        =   65536
            _ExtentX        =   3413
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin VB.Label lblCurrentMonthActual 
            Caption         =   "Current Month Actual: "
            Height          =   255
            Index           =   8
            Left            =   2040
            TabIndex        =   110
            Top             =   1320
            Width           =   1575
         End
         Begin VB.Label lblCurrentMonthPlan 
            Caption         =   "Current Month Plan: "
            Height          =   255
            Index           =   8
            Left            =   5880
            TabIndex        =   109
            Top             =   1320
            Width           =   1575
         End
         Begin VB.Label lblYTDActual 
            Caption         =   "YTD Actual: "
            Height          =   255
            Index           =   8
            Left            =   720
            TabIndex        =   108
            Top             =   2280
            Width           =   1215
         End
         Begin VB.Label lblYTDPlan 
            Caption         =   "YTD Plan: "
            Height          =   255
            Index           =   8
            Left            =   3840
            TabIndex        =   107
            Top             =   2280
            Width           =   1215
         End
         Begin VB.Label lblYTDPriorYear 
            Caption         =   "YTD Prior Year: "
            Height          =   255
            Index           =   8
            Left            =   6960
            TabIndex        =   106
            Top             =   2280
            Width           =   1215
         End
         Begin VB.Label lblYTDPriorYear 
            Caption         =   "YTD Prior Year: "
            Height          =   255
            Index           =   9
            Left            =   -68040
            TabIndex        =   105
            Top             =   2280
            Width           =   1215
         End
         Begin VB.Label lblYTDPlan 
            Caption         =   "YTD Plan: "
            Height          =   255
            Index           =   9
            Left            =   -71160
            TabIndex        =   104
            Top             =   2280
            Width           =   1215
         End
         Begin VB.Label lblYTDActual 
            Caption         =   "YTD Actual: "
            Height          =   255
            Index           =   9
            Left            =   -74280
            TabIndex        =   103
            Top             =   2280
            Width           =   1215
         End
         Begin VB.Label lblCurrentMonthPlan 
            Caption         =   "Current Month Plan: "
            Height          =   255
            Index           =   9
            Left            =   -69120
            TabIndex        =   102
            Top             =   1320
            Width           =   1575
         End
         Begin VB.Label lblCurrentMonthActual 
            Caption         =   "Current Month Actual: "
            Height          =   255
            Index           =   9
            Left            =   -72960
            TabIndex        =   101
            Top             =   1320
            Width           =   1575
         End
         Begin VB.Label lblYTDPriorYear 
            Caption         =   "YTD Prior Year: "
            Height          =   255
            Index           =   10
            Left            =   -68040
            TabIndex        =   100
            Top             =   2280
            Width           =   1215
         End
         Begin VB.Label lblYTDPlan 
            Caption         =   "YTD Plan: "
            Height          =   255
            Index           =   10
            Left            =   -71160
            TabIndex        =   99
            Top             =   2280
            Width           =   1215
         End
         Begin VB.Label lblYTDActual 
            Caption         =   "YTD Actual: "
            Height          =   255
            Index           =   10
            Left            =   -74280
            TabIndex        =   98
            Top             =   2280
            Width           =   1215
         End
         Begin VB.Label lblCurrentMonthPlan 
            Caption         =   "Current Month Plan: "
            Height          =   255
            Index           =   10
            Left            =   -69120
            TabIndex        =   97
            Top             =   1320
            Width           =   1575
         End
         Begin VB.Label lblCurrentMonthActual 
            Caption         =   "Current Month Actual: "
            Height          =   255
            Index           =   10
            Left            =   -72960
            TabIndex        =   96
            Top             =   1320
            Width           =   1575
         End
         Begin VB.Label lblYTDPriorYear 
            Caption         =   "YTD Prior Year: "
            Height          =   255
            Index           =   18
            Left            =   -68040
            TabIndex        =   95
            Top             =   2280
            Width           =   1215
         End
         Begin VB.Label lblYTDPlan 
            Caption         =   "YTD Plan: "
            Height          =   255
            Index           =   18
            Left            =   -71160
            TabIndex        =   94
            Top             =   2280
            Width           =   1215
         End
         Begin VB.Label lblYTDActual 
            Caption         =   "YTD Actual: "
            Height          =   255
            Index           =   18
            Left            =   -74280
            TabIndex        =   93
            Top             =   2280
            Width           =   1215
         End
         Begin VB.Label lblCurrentMonthPlan 
            Caption         =   "Current Month Plan: "
            Height          =   255
            Index           =   18
            Left            =   -69120
            TabIndex        =   92
            Top             =   1320
            Width           =   1575
         End
         Begin VB.Label lblCurrentMonthActual 
            Caption         =   "Current Month Actual: "
            Height          =   255
            Index           =   18
            Left            =   -72960
            TabIndex        =   91
            Top             =   1320
            Width           =   1575
         End
         Begin VB.Label lblYTDPriorYear 
            Caption         =   "YTD Prior Year: "
            Height          =   255
            Index           =   19
            Left            =   -68040
            TabIndex        =   90
            Top             =   2280
            Width           =   1215
         End
         Begin VB.Label lblYTDPlan 
            Caption         =   "YTD Plan: "
            Height          =   255
            Index           =   19
            Left            =   -71160
            TabIndex        =   89
            Top             =   2280
            Width           =   1215
         End
         Begin VB.Label lblYTDActual 
            Caption         =   "YTD Actual: "
            Height          =   255
            Index           =   19
            Left            =   -74280
            TabIndex        =   88
            Top             =   2280
            Width           =   1215
         End
         Begin VB.Label lblCurrentMonthPlan 
            Caption         =   "Current Month Plan: "
            Height          =   255
            Index           =   19
            Left            =   -69120
            TabIndex        =   87
            Top             =   1320
            Width           =   1575
         End
         Begin VB.Label lblCurrentMonthActual 
            Caption         =   "Current Month Actual: "
            Height          =   255
            Index           =   19
            Left            =   -72960
            TabIndex        =   86
            Top             =   1320
            Width           =   1575
         End
      End
      Begin VB.Label Label2 
         Caption         =   "Fisc Period:"
         Height          =   255
         Left            =   2280
         TabIndex        =   20
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Fisc Year:"
         Height          =   255
         Left            =   360
         TabIndex        =   18
         Top             =   600
         Width           =   735
      End
   End
   Begin VB.Frame fraSort 
      Caption         =   "&Sort"
      Height          =   825
      Left            =   4200
      TabIndex        =   27
      Top             =   2280
      Visible         =   0   'False
      Width           =   1035
      Begin FPSpreadADO.fpSpread grdSort 
         Height          =   1545
         Left            =   90
         TabIndex        =   28
         Top             =   270
         WhatsThisHelpID =   17370010
         Width           =   9015
         _Version        =   524288
         _ExtentX        =   15901
         _ExtentY        =   2725
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         SpreadDesigner  =   "glzxLrdrShpRpt.frx":24EA
         AppearanceStyle =   0
      End
   End
   Begin VB.Frame fraSelect 
      Caption         =   "S&elect"
      Height          =   1020
      Left            =   4080
      TabIndex        =   21
      Top             =   3480
      Visible         =   0   'False
      Width           =   1155
      Begin FPSpreadADO.fpSpread grdSelection 
         Height          =   2175
         Left            =   90
         TabIndex        =   22
         Top             =   270
         WhatsThisHelpID =   17370015
         Width           =   9015
         _Version        =   524288
         _ExtentX        =   15901
         _ExtentY        =   3836
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         SpreadDesigner  =   "glzxLrdrShpRpt.frx":28F8
         AppearanceStyle =   0
      End
      Begin NEWSOTALib.SOTACurrency curControl 
         Height          =   285
         Left            =   1530
         TabIndex        =   23
         TabStop         =   0   'False
         Top             =   1035
         WhatsThisHelpID =   17370011
         Width           =   1950
         _Version        =   65536
         _ExtentX        =   3440
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         sBorder         =   1
         mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "           0.00"
         sDecimalPlaces  =   2
      End
      Begin NEWSOTALib.SOTANumber nbrControl 
         Height          =   285
         Left            =   1530
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   1440
         WhatsThisHelpID =   17370012
         Width           =   1950
         _Version        =   65536
         _ExtentX        =   3440
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         sBorder         =   1
         mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "           0.00"
         sDecimalPlaces  =   2
      End
      Begin NEWSOTALib.SOTAMaskedEdit mskControl 
         Height          =   240
         Left            =   1530
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   720
         WhatsThisHelpID =   17370013
         Width           =   1950
         _Version        =   65536
         _ExtentX        =   3440
         _ExtentY        =   423
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         sBorder         =   1
      End
      Begin LookupViewControl.LookupView luvControl 
         Height          =   285
         Index           =   0
         Left            =   3555
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   720
         WhatsThisHelpID =   17370014
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   503
         LookupMode      =   1
      End
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -10000
      TabIndex        =   3
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -10000
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.ComboBox CustomCombo 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      ForeColor       =   &H80000012&
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   11
      TabStop         =   0   'False
      Text            =   "Combo1"
      Top             =   1530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.CheckBox chkSummary 
      Caption         =   "Print &Report Settings"
      Height          =   285
      Left            =   6120
      TabIndex        =   6
      Top             =   495
      WhatsThisHelpID =   17370003
      Width           =   2445
   End
   Begin VB.ComboBox cboReportSettings 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   675
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   495
      WhatsThisHelpID =   17370002
      Width           =   4380
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      Width           =   9450
      _ExtentX        =   16669
      _ExtentY        =   741
      Style           =   3
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   6450
      Width           =   9450
      _ExtentX        =   16669
      _ExtentY        =   688
      BrowseVisible   =   0   'False
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   4
      Top             =   4320
      Visible         =   0   'False
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   2880
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   615
      Index           =   0
      Left            =   -10000
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   720
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1085
      _StockProps     =   0
   End
   Begin MSComDlg.CommonDialog dlgCreateRPTFile 
      Left            =   10620
      Top             =   1770
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComDlg.CommonDialog dlgPrintSetup 
      Left            =   9930
      Top             =   1770
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin SOTAVM.SOTAValidationMgr valMgr 
      Left            =   -10000
      Top             =   180
      _ExtentX        =   741
      _ExtentY        =   661
      CtlsCount       =   0
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   14
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Label lblSetting 
      AutoSize        =   -1  'True
      Caption         =   "Se&tting"
      Height          =   195
      Left            =   135
      TabIndex        =   1
      Top             =   540
      Width           =   495
   End
End
Attribute VB_Name = "frmLeaderShipReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mlRunMode               As Long

Private mbSaved                 As Boolean

Private mbCancelShutDown        As Boolean

Private mbLoading               As Boolean

Private mbPeriodEnd             As Boolean

Private mbLoadSuccess           As Boolean

Private mbEnterAsTab            As Boolean

Private miSecurityLevel         As Integer

Private moClass                 As Object

Private moContextMenu           As clsContextMenu

Public moSotaObjects            As New Collection

Public msCompanyID              As String

Public msUserID                 As String

Public mlLanguage               As Long

Public moReport                 As clsReportEngine

Public moSettings               As clsSettings

Public moSort                   As clsSort

Public moSelect                 As clsSelection

Public moDDData                 As clsDDData

Public moOptions                As clsOptions

Public moPrinter                As Printer

Public sRealTableCollection     As Collection

Public sWorkTableCollection     As Collection

Const VBRIG_MODULE_ID_STRING = "glzxLrdrShpRpt.FRM"

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

Private Sub cmdInvert_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim i As Integer
    
    For i = 1 To 19
        If chRptPages(i).Value = vbUnchecked Then
            chRptPages(i).Value = vbChecked
        Else
            chRptPages(i).Value = vbUnchecked
        End If
    Next

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cmdInvert_Click", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub cmdSelectAll_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim i As Integer
    
    For i = 1 To 19
        chRptPages(i).Value = vbChecked
    Next
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cmdSelectAll_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub cmdUnSelectAll_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim i As Integer
    
    For i = 1 To 19
        chRptPages(i).Value = vbUnchecked
    Next

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cmdUnSelectAll_Click", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

'********************** Selection Stuff ****************************
Private Sub grdSelection_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect.EditMode Col, Row, Mode, ChangeMade
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdSelection_EditMode", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdSelection_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect.SelectionGridGotFocus
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdSelection_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdSelection_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect.SelectionGridKeyDown keycode, Shift
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdSelection_KeyDown", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdSelection_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect.SelectionGridLeaveCell Col, Row, NewCol, NewRow, Cancel
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdSelection_LeaveCell", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdSelection_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect.SelectionGridTopLeftChange OldLeft, OldTop, NewLeft, NewTop
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdSelection_TopLeftChange", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub



Private Sub mskControl_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect.mskControlLostFocus
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "mskControl_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub nbrControl_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect.nbrControlLostFocus
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "nbrControl_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub curControl_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect.curControlLostFocus
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "curControl_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub luvControl_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect.LookupControlClick Index, ""
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "luvControl_GotFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
'******************** End Selection Stuff ****************************


'    ************************ Sort Stuff
'    ****************
'***************
Private Sub grdSort_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSort.GridLeaveCell Col, Row, NewCol, NewRow
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdSort_LeaveCell", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdSort_KeyUp(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSort.GridKey keycode, Shift
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdSort_KeyUp", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdSort_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSort.GridChange Col, Row
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdSort_Change", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdSort_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSort.GridButtonClicked Col, Row, ButtonDown
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdSort_ButtonClicked", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdSort_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSort.SortGridGotFocus
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdSort_GotFocus", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdSort_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSort.SortGridKeyDown keycode, Shift
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdSort_KeyDown", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
'*********************** End Sort Stuff *****************************

Public Sub HandleToolbarClick(sKey As String, Optional iFileType As Variant, Optional sFileName As Variant)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    If Me.Visible Then
        Me.SetFocus '   VB5
    End If

#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If
   
    Select Case sKey
        Case kTbProceed, kTbFinish, kTbSave
            moSettings.ReportSettingsSaveAs
        
        Case kTbCancel, kTbDelete
            moSettings.ReportSettingsDelete
        
        Case kTbDefer, kTbPreview, kTbPrint
                'Validate the input
                If bValidate() = False Then
                    Exit Sub
                End If
                
                gbSkipPrintDialog = False                         'show print dialog one time
                lStartReport sKey, Me, False, CLng(txtFiscYear.Text), CLng(txtFiscPeriod.Text), iFileType, sFileName
            
        Case kTbPrintSetup
            dlgPrintSetup.ShowPrinter
        
        Case kTbHelp
            gDisplayFormLevelHelp Me
    End Select
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function lInitializeReport(ModuleID As String, DefaultReport As String, ProjectName As String) As Long
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    lInitializeReport = -1
    
    Set moPrinter = Printer
    Set moSotaObjects = New Collection
    Set moReport = New clsReportEngine
    Set moSettings = New clsSettings
    Set moSort = New clsSort
    Set moSelect = New clsSelection
    Set moDDData = New clsDDData
    Set moOptions = New clsOptions
        
    If mbPeriodEnd Then
        moSelect.UI = False
        moReport.UI = False
    End If
    If Not moDDData.lInitDDData(sRealTableCollection, moClass.moAppDB, moClass.moAppDB, msCompanyID) = kSuccess Then
        lInitializeReport = kmsgCantLoadDDData
        Exit Function
    End If
    
    If Not moDDData.lDDBuildData(App.EXEName) = kSuccess Then
        lInitializeReport = kmsgCantLoadDDData
        Exit Function
    End If
    
    moReport.AppOrSysDB = kAppDB
    
    #If RPTDEBUG = 1 Then  'Do not use temp tables
        If (moReport.lInitReport(ModuleID, DefaultReport, Me, moDDData, False) = kFailure) Then
            lInitializeReport = kmsgFatalReportInit
    #Else                             ' Do use temp tables
        If (moReport.lInitReport(ModuleID, DefaultReport, Me, moDDData, True) = kFailure) Then
            lInitializeReport = kmsgFatalReportInit
    #End If
        Exit Function
    End If
   
    'CUSTOMIZE: set Groups and Sorts as required
    'note there are two optional parameters: iNumGroups,iNumSorts for lInitSort
    If (moSort.lInitSort(Me, moDDData) = kFailure) Then
        lInitializeReport = kmsgFataSortInit
        Exit Function
    End If

    With moSort

    End With

    If Not moSelect.bInitSelect(sRealTableCollection(1), mskControl, luvControl, nbrControl, _
            curControl, grdSelection, fraSelect, Me, moDDData, _
            19 * 120 + 60, 17 * 120, 18 * 120, 18 * 120, True) Then
        lInitializeReport = kmsgSetupSelectionGridFail
        Exit Function
    End If
    
    '   Add GL Account Segments to sort and/or selection grid. Examples:
    '   Dim oAppDB as DASDatabase
    '   Set oAppDB = moClass.moAppDB
    '   moSort.lAddGLAcctSeg oAppDB, "Segment1", "Segment2", "Segment3", "Segment4", "Segment5"
    '   moSelect.lAddGLAcctSeg oAppDB, "GLAcctNo", "Segment1", "Segment2", "Segment3", "Segment4", "Segment5"
    With moSelect

    End With
    moSelect.PopulateSelectionGrid
    
    lInitializeReport = 0
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lInitializeReport", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Sub tabReport_Click(PreviousTab As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim bEnabled As Boolean

    If tabReport.Tab = PreviousTab Then Exit Sub
    'POSSIBLE CUSTOMIZE: make sure form is saved with Main tab in front
    'this code is used to ensure tab order works correctly
    bEnabled = (tabReport.Tab = 1)

    fraSort.Enabled = Not bEnabled
    fraSelect.Enabled = Not bEnabled

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "tabReport_Click", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    HandleToolbarClick Button
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function

Private Sub cboReportSettings_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
If Not moSettings.gbSkipClick Then
    moSettings.bLoadReportSettings
End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cboReportSettings_Click", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub cboReportSettings_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Len(cboReportSettings.Text) > 40 Then
        Beep
        KeyAscii = 0
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cboReportSettings_KeyPress", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Skip +++
    mbCancelShutDown = False
    mbLoadSuccess = False
End Sub

Private Sub Form_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iLuvControl As Integer
#If CUSTOMIZER Then
    If (Shift = 4) Then
        gProcessFKeys Me, keycode, Shift
    End If
#End If

    If Shift = 0 Then
        Select Case keycode
            Case vbKeyF1
                gDisplayWhatsThisHelp Me, Me.ActiveControl
            Case vbKeyF5
                iLuvControl = moSelect.SelectionGridF5KeyDown(Me.ActiveControl)
                If iLuvControl > 0 Then
                    luvControl_GotFocus iLuvControl
                End If
        End Select
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Skip +++
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
              gProcessSendKeys "{TAB}"
               KeyAscii = 0
            End If
        
        Case Else
            'Other KeyAscii routines.
    End Select
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lRetval As Long             'Captures Long function return value(s).
    Dim iRetVal As Integer          'Captures Integer function return value(s).
    Dim sRptProgramName As String   'Stores base filename for identification purposes.
    Dim sModule As String           'The report's module: AR, AP, SM, etc.
    Dim sDefaultReport As String    'The default .RPT file name
    
    mbLoading = True
    mbLoadSuccess = False
           
    'Assign system object properties to module-level variables for easier access
    With moClass.moSysSession
        msCompanyID = .CompanyId
        msUserID = .UserId
        mbEnterAsTab = .EnterAsTab
        mlLanguage = .Language
    End With
    
    'CUSTOMIZE: assign defaults for this project
    sRptProgramName = "glzxLrdrShpRpt" 'such as rpt001
    sModule = "GL" '2 letter Module name
    sDefaultReport = "Leadership Report.rpt"
    
    'Set local flag to value of class module's bPeriodEnd flag.
    mbPeriodEnd = moClass.bPeriodEnd
    
    Set sRealTableCollection = New Collection
    'CUSTOMIZE:  Invoke Add method of sRealTableCollection for each table which
    'will provide raw data for the report.  Primary table should appear first.
    With sRealTableCollection
        .Add "vluLeadershipReportData_SGS"

    End With
    
    Set sWorkTableCollection = New Collection
    'CUSTOMIZE:  Invoke Add method of sWorkTableCollection for each table which
    'will serve as a work table for the report.
    With sWorkTableCollection
        .Add "tglLeadershipReportDataTmp_SGS"

    End With
    
    lRetval = lInitializeReport(sModule, sDefaultReport, sRptProgramName)
    If lRetval <> 0 Then
        moReport.ReportError lRetval
        GoTo badexit
    End If
    
    'Perform initialization of form and control properties only if you need to display UI.
    If Not mbPeriodEnd Then
        'Set form dimensions.
        Me.Height = 7200 'max height: 7200
        Me.Width = 9600  'max width: 9600
        
        'Initialize toolbar.
        tbrMain.Init sotaTB_REPORT, mlLanguage
        With moClass.moFramework
            tbrMain.SecurityLevel = .GetTaskPermission(.GetTaskID())
        End With
                    
        '   Initialize status bar.
        Set sbrMain.Framework = moClass.moFramework
        sbrMain.Status = SOTA_SB_START
        
        'Set form caption using localized text.
        'CUSTOMIZE:  Pass in appropriate string constant as first argument.
        Me.Caption = "Leadership Report"
        
        'Initialize context menu.
        'context-sensitive help.
        Set moContextMenu = New clsContextMenu
        With moContextMenu
            .Bind "*SELECTION", grdSelection.hwnd
            Set .Form = frmLeaderShipReport
            .Init
        End With
    End If
    
    'CUSTOMIZE:  Add all controls on Options page for which you will want to save settings
    'to the moOptions collection, using its Add method.
    With moOptions
        .Add chkSummary
    
    End With
    
    'Initialize the report settings combo box.
    moSettings.Initialize Me, cboReportSettings, moSort, moSelect, moOptions, , tbrMain, sbrMain
    
    'If the report is being invoked from the Period End Processing task, attempt to load the
    'setting named "Period End".  If such setting does not exist, or if not being invoked from
    'the Period End Processing task, load last used setting.
    If mbPeriodEnd Then
        moSettings.lLoadPeriodEndSettings
    End If
        
    'Update status flags to indicate successful load has completed.
    mbLoadSuccess = True
    mbLoading = False

    'Set the default Year and Perod (= current year and current month -1... uless current month is Jan.)
    If CInt(Month(Date)) - 1 = 0 Then
        txtFiscPeriod.Text = 12
        txtFiscYear.Text = CInt(Year(Date)) - 1
    Else
        txtFiscPeriod.Text = CInt(Month(Date)) - 1
        txtFiscYear.Text = CInt(Year(Date))
    End If
    
    Exit Sub
    
badexit:

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
    Dim iRet As Integer

    mbCancelShutDown = False
    
    moReport.CleanupWorkTables
    
    If Not mbPeriodEnd Then
        moSettings.SaveLastSettingKey
    End If
    
    If moClass.mlError = 0 Then
        'if a report preview window is active, query user to continue closing.
        If Not moReport.bShutdownEngine Then
            GoTo CancelShutDown
        End If
        
        If Not mbPeriodEnd And gbActiveChildForms(Me) Then
            GoTo CancelShutDown
        End If
    End If
    
    If UnloadMode <> vbFormCode Then
        moClass.miShutDownRequester = kUnloadSelfShutDown
    End If
    
    PerformCleanShutDown

    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
            
        Case Else 'kFrameworkShutDown
        'Do Nothing
        
    End Select
    
    Exit Sub
    
CancelShutDown:
    Cancel = True
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    gUnloadChildForms Me
    giCollectionDel moClass.moFramework, moSotaObjects, -1

    UnloadObjects

    TerminateControls Me

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Skip +++

    Set moClass = Nothing
End Sub

Private Sub UnloadObjects()
'+++ VB/Rig Skip +++
    Set moSotaObjects = Nothing
    Set moReport = Nothing
    Set moSettings = Nothing
    Set moSort = Nothing
    Set moContextMenu = Nothing
    Set moSelect = Nothing
    Set moDDData = Nothing
    Set moOptions = Nothing
    Set moPrinter = Nothing
    Set sRealTableCollection = Nothing
    Set sWorkTableCollection = Nothing
End Sub

Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

#If CUSTOMIZER Then
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyFormCust
        End If
    End If
#End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Skip +++
    WhatHelpPrefix = moReport.ModuleID() & "Z"
End Property

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Skip +++
    FormHelpPrefix = moReport.ModuleID() & "Z"
End Property

Public Property Get oClass() As Object
'+++ VB/Rig Skip +++
    Set oClass = moClass
End Property
Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Skip +++
    Set moClass = oNewClass
End Property

Public Property Get oreport() As clsReportEngine
'+++ VB/Rig Skip +++
    Set oreport = moReport
End Property
Public Property Set oreport(oClass As clsReportEngine)
'+++ VB/Rig Skip +++
    Set moReport = oClass
End Property

Public Property Get lRunMode() As Long
'+++ VB/Rig Skip +++
    lRunMode = mlRunMode
End Property
Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Skip +++
    mlRunMode = lNewRunMode
End Property

Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Skip +++
    bLoadSuccess = mbLoadSuccess
End Property

Public Property Let bLoadSuccess(bNewLoadSuccess As Boolean)
'+++ VB/Rig Skip +++
    mbLoadSuccess = bNewLoadSuccess
End Property

Public Property Get bCancelShutDown() As Boolean
'+++ VB/Rig Skip +++
  bCancelShutDown = mbCancelShutDown
End Property
Public Property Let bCancelShutDown(bCancel As Boolean)
'+++ VB/Rig Skip +++
    mbCancelShutDown = bCancel
End Property
'*** END PROPERTIES ***

Public Property Get MyApp() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyApp = App
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyApp", VBRIG_IS_FORM                                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get MyForms() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyForms = Forms
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyForms", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Function CMMultiSelect(ByVal Ctl As Object) As Object
'+++ VB/Rig Skip +++
'*******************************************************************************
'   Description: OPTIONAL
'                The form's selection object MUST be declared as PUBLIC.
'                However, array variables in a form can not be declared public.
'                Therefore, for those small handful of applications that have
'                their selection object declared as Private and/or declared
'                as an array, this mechanism will allow the form to pass back
'                the selection object to be worked on.
'
'                Therefore, if applications are having an issue, they must either
'                declare their selection object as public or implement this call back
'                method
'
'   Parameters:
'                Ctl <in> - Control that selected the task
'
'   Returns:
'                A selection object
'*******************************************************************************
On Error Resume Next
    
    ' moSelect is a private control array, therefore ...
    Set CMMultiSelect = moSelect(Ctl.Index)
    
    Err.Clear
    
End Function

Public Property Get UseHTMLHelp() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    UseHTMLHelp = True   ' Form uses HTML Help (True/False)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "UseHTMLHelp", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property


Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picdrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseDown Index, Button, Shift, x, y
    End If
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_MouseDown", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picdrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseMove Index, Button, Shift, x, y
    End If
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_MouseMove", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picdrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseUp Index, Button, Shift, x, y
    End If
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_MouseUp", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picdrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_Paint Index
    End If
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_Paint", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Function bValidate() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    'Default to failed:
    bValidate = False
    
    '****************************************************************************
    '****************************************************************************
    '****************************************************************************
    'Validate Fisc Year
    '****************************************************************************
    '****************************************************************************
    '****************************************************************************
    If Trim(txtFiscYear.Text) = Empty Then
        MsgBox "You must enter a valide Fisc Year (4 digit Numeric Value between 2000 and 2050)", vbExclamation, "MAS 500"
        Exit Function
    End If
    
    If IsNumeric(Trim(txtFiscYear.Text)) = False Then
        MsgBox "You must enter a valide Fisc Year (4 digit Numeric Value between 2000 and 2050)", vbExclamation, "MAS 500"
        Exit Function
    End If
    
    If CInt(txtFiscYear.Text) < 2000 Or CInt(txtFiscYear.Text) > 2050 Then
        MsgBox "You must enter a valide Fisc Year (4 digit Numeric Value between 2000 and 2050)", vbExclamation, "MAS 500"
        Exit Function
    End If
    
    
    '****************************************************************************
    '****************************************************************************
    '****************************************************************************
    'Validate Fisc Period
    '****************************************************************************
    '****************************************************************************
    '****************************************************************************
    If Trim(txtFiscPeriod.Text) = Empty Then
        MsgBox "You must enter a valide Fisc Period (Numeric Value between 1 and 12)", vbExclamation, "MAS 500"
        Exit Function
    End If
    
    If IsNumeric(Trim(txtFiscPeriod.Text)) = False Then
        MsgBox "You must enter a valide Fisc Period (Numeric Value between 1 and 12)", vbExclamation, "MAS 500"
        Exit Function
    End If
    
    If CInt(txtFiscPeriod.Text) < 1 Or CInt(txtFiscPeriod.Text) > 12 Then
        MsgBox "You must enter a valide Fisc Period (Numeric Value between 1 and 12)", vbExclamation, "MAS 500"
        Exit Function
    End If
    
    bValidate = True
    
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bValidate", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


